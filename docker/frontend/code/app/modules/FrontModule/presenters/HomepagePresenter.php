<?php

declare(strict_types=1);

namespace Cleevio\FrontModule\Presenters;

class HomepagePresenter extends BasePresenter
{

	public function startup(): void
	{
		parent::startup();

		$this->redirect(':Trips:Front:Trip:default');
	}

	/**
	 * @return string
	 */
	protected function getLayoutName(): string
	{
		return "layout";
	}
}
