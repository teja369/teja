<?php

declare(strict_types=1);

namespace Cleevio\FrontModule\Presenters;

use Cleevio\ApiClient\Exception\ApiException;
use Cleevio\Issues\Components\IssueFormModal\IIssueFormModalFactory;
use Cleevio\Issues\Components\IssueFormModal\IssueFormModal;
use Cleevio\Translations\Components\LanguageSelect\ILanguageSelectFactory;
use Cleevio\Trips\API\Requests\TripsGetRequest;
use Cleevio\Trips\Components\TripsCounter\ITripsCounterFactory;
use Cleevio\Trips\Components\TripsCounter\TripsCounter;
use Cleevio\Trips\Components\TripsSidebar\ITripsSidebarFactory;
use Cleevio\Trips\Components\TripsSidebar\TripsSidebar;
use Cleevio\Users\Components\UserInfoBox\IUserInfoBoxFactory;
use Cleevio\Users\Components\UserInfoBox\UserInfoBox;
use DateTime;
use Nette\Application\AbortException;
use Nette\Application\BadRequestException;

class DashboardPresenter extends BasePresenter
{

	/**
	 * @var TripsGetRequest
	 */
	protected $tripsGetRequest;


	public function __construct(TripsGetRequest $tripsGetRequest)
	{
		parent::__construct();

		$this->tripsGetRequest = $tripsGetRequest;
	}


	/**
	 * @throws AbortException
	 * @throws BadRequestException
	 */
	public function startup(): void
	{
		parent::startup();

		try {

			$currentRequest = clone $this->tripsGetRequest;
			$currentRequest->setPlanned();
			$currentRequest->setMaxStartDate((new DateTime)->format('Y-m-d'));
			$currentRequest->setMinEndDate((new DateTime)->format('Y-m-d'));
			$currentRequest->setLimit(1);
			$this->template->current = $currentRequest->execute();

			$plannedRequest = clone $this->tripsGetRequest;
			$plannedRequest->setPlanned();
			$plannedRequest->setMinStartDate((new DateTime)->modify("+1 days")->format('Y-m-d'));
			$plannedRequest->setOrderBy('start');
			$plannedRequest->setOrderDirection('asc');
			$plannedRequest->setLimit(1);
			$this->template->planned = $plannedRequest->execute();

			$draftsRequest = clone $this->tripsGetRequest;
			$draftsRequest->setLimit(2);
			$draftsRequest->setTripState('draft');
			$this->template->drafts = $draftsRequest->execute();

		} catch (ApiException $e) {
			$this->error($e->getMessage(), $e->getCode());
		}
	}

	/**
	 * @return string
	 */
	protected function getLayoutName(): string
	{
		return 'layout';
	}

	/**
	 * @param IUserInfoBoxFactory $factory
	 * @param ILanguageSelectFactory $languageFactory
	 * @return UserInfoBox
	 */
	protected function createComponentUserInfoBox(
		IUserInfoBoxFactory $factory,
		ILanguageSelectFactory $languageFactory
	): UserInfoBox
	{
		$control = $factory->create();

		$control->addComponent($languageFactory->create(), 'languageSelect');

		$control->onLoggedOut[] = function () {
			if ($this->ssoContext->getUri() !== null) {
				$this->redirectPermanent(':Users:Front:Logout:sso');
			} else {
				$this->redirectPermanent(':Front:Homepage:default');
			}
		};

		return $control;
	}

	/**
	 * @param ITripsCounterFactory $factory
	 * @return TripsCounter
	 */
	public function createComponentTripsCounter(ITripsCounterFactory $factory): TripsCounter
	{
		return $factory->create();
	}

	/**
	 * @param ITripsSidebarFactory $factory
	 * @return TripsSidebar
	 */
	public function createComponentTripsSidebar(ITripsSidebarFactory $factory): TripsSidebar
	{
		return $factory->create();
	}

	public function createComponentIssueFormModal(IIssueFormModalFactory $factory): IssueFormModal
	{
		$control = $factory->create();

		$control->onSuccess[] = function () {
			$this->payload->forceRedirect = true;
			$this->flashMessage('frontend.trips.dashboard.issue.message-sent.success', 'success');
			$this->redrawControl('flashes');
			$this->redirectPermanent(':Trips:Front:Trip:');
		};

		return $control;
	}
}
