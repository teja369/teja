<?php

declare(strict_types=1);

namespace Cleevio\FrontModule\Presenters;

use Cleevio\API\Context;
use Cleevio\ApiClient\DTO\Locale;
use Cleevio\Sso\SsoContext;
use Cleevio\Translations\API\Requests\LanguagesGetRequest;
use Cleevio\Translations\Components\LanguageSelect\ILanguageSelectFactory;
use Cleevio\Translations\Components\LanguageSelect\LanguageSelect;
use Cleevio\Trips\Components\PromptModal\IPromptModal;
use Cleevio\Trips\Components\PromptModal\PromptModal;
use Cleevio\Users\FrontModule\Presenters\LoginPresenter;
use Cleevio\Users\FrontModule\Presenters\LogoutPresenter;
use Contributte\Translation\LocalesResolvers\Session;
use Kdyby\Autowired\AutowireComponentFactories;
use Kdyby\Autowired\AutowireProperties;
use Nette\Application\AbortException;
use Nette\Application\UI\Presenter;
use Nette\Http\Request;
use Nette\Localization\ITranslator;

abstract class BasePresenter extends Presenter
{

	use AutowireProperties;
	use AutowireComponentFactories;

	/**
	 * @var ITranslator @inject
	 */
	public $translator;

	/**
	 * @var Session @inject
	 */
	public $translatorSessionResolver;

	/**
	 * @var Request @inject
	 */
	public $request;

	/**
	 * @var LanguagesGetRequest @inject
	 */
	public $languageRequest;

	/**
	 * @var Context
	 * @inject
	 */
	public $apiContext;

	/**
	 * @var SsoContext
	 * @inject
	 */
	public $ssoContext;

	/**
	 * @var \Nette\Http\Session
	 * @inject
	 */
	public $session;

	/**
	 * @var Locale|null
	 */
	protected $localeContext;


	/**
	 * @throws AbortException
	 * @throws \Exception
	 */
	public function startup(): void
	{
		parent::startup();
		$this->autoCanonicalize = false;

		// Handle language
		$this['languageSelect']->handleLang();

		if (($this->apiContext === null || $this->apiContext->getAuthorization() === null) && !$this->isAuthAction()) {
			if ($this->ssoContext->getUri() !== null) {
				$this->redirectUrl($this->ssoContext->getUri());
			} else {
				$this->redirect(':Users:Front:Login:default');
			}
		}

		if ($this->ssoContext->getUri() !== null && !$this->apiContext->getTimeout() && !$this->isAuthAction()) {
			$this->redirect(':Users:Front:Logout:sso');
		}

		$this->setLayout(__DIR__ . '/templates/@' . $this->getLayoutName() . ".latte");

		$this->localeContext = $this->apiContext->getLocale();
	}

	/**
	 * Determines if current uri is login/logout endpoint
	 * @return bool
	 */
	private function isAuthAction(): bool
	{
		return !($this->getPresenter() instanceof LoginPresenter === false && $this->getPresenter() instanceof LogoutPresenter === false);
	}

	/**
	 * @return string
	 */
	abstract protected function getLayoutName(): string;

	/**
	 * @param string|null $q
	 * @throws AbortException
	 */
	public function handleDatepickerMonths(?string $q): void
	{
		if ($q !== null && $q !== '') {
			$translatedMonths = [];

			for ($i = 1; $i <= 12; $i++) {
				$month = str_pad((string) $i, 2, (string) 0, STR_PAD_LEFT);
				$translatedMonths[$month] = $this->translator->translate(
					sprintf('frontend.dates.month.%s', $month)
				);
			}

			$searchedMonths = [];

			foreach ($translatedMonths as $key => $month) {
				if (strpos(strtolower($month), strtolower($q)) > -1) {
					$searchedMonths[$key] = $month;
				}
			}

			$result = [];

			foreach ($searchedMonths as $key => $month) {
				for ($i = 0; $i < 3; $i++) {
					$year = (int) date('Y') + $i;
					$result[] = [
						'value' => $year . '-' . $key, 'text' => $month . ' ' . $year,
					];
				}
			}
		}

		$this->sendJson($result ?? []);
	}

	/**
	 * @param IPromptModal $factory
	 * @return PromptModal
	 */
	public function createComponentPromptModal(IPromptModal $factory): PromptModal
	{
		return $factory->create();
	}

	/**
	 * @param ILanguageSelectFactory $factory
	 * @return LanguageSelect
	 */
	protected function createComponentLanguageSelect(ILanguageSelectFactory $factory): LanguageSelect
	{
		$control = $factory->create();

		$control->onSuccess[] = function () {
			if ($this->isAjax()) {
				$this->payload->forceRedirect = true;
				$this->redirect('this');
			} else {
				$this->redirect('this');
			}
		};

		return $control;
	}

}
