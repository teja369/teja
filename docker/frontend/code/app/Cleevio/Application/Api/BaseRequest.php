<?php

declare(strict_types=1);

namespace Cleevio\API;

use Cleevio\ApiClient\BaseRequest as ClientBaseRequest;

abstract class BaseRequest extends ClientBaseRequest
{
	protected function request(): array
	{
		$response = parent::request();

		return array_key_exists('data', $response)
			? $response['data']
			: $response;
	}

}
