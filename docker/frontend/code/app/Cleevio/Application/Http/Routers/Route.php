<?php

declare(strict_types=1);

namespace Cleevio\Application\Http\Routers;

use Nette\Application\Routers\Route as NetteRoute;
use Nette\Http\Url;
use Nette\Http\UrlScript;

class Route extends NetteRoute
{

	public function constructUrl(array $params, UrlScript $refUrl): ?string
	{
		$url = parent::constructUrl($params, $refUrl);

		if (isset($_SERVER['HTTP_X_FORWARDED_HOST'])) {
			$url = new Url($url);
			$url->setHost($_SERVER['HTTP_X_FORWARDED_HOST']);

			return $url->getAbsoluteUrl();
		}

		return $url;
	}
}
