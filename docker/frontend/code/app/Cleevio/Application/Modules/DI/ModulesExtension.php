<?php

declare(strict_types=1);

namespace Cleevio\Modules\DI;

use Cleevio\Modules\Providers\IPresenterMappingProvider;
use Cleevio\Modules\Providers\IRouterProvider;
use Cleevio\Modules\Providers\ITranslationProvider;
use Nette\DI\CompilerExtension;
use Nette\DI\ServiceDefinition;
use Nette\DI\Statement;
use Nette\Utils\Finder;

class ModulesExtension extends CompilerExtension
{

	public const TAG_ROUTER = 'cleevio.modules.router';

	public function loadConfiguration(): void
	{
		$builder = $this->getContainerBuilder();

		foreach ($this->compiler->getExtensions() as $extension) {
			if ($extension instanceof IPresenterMappingProvider) {
				$this->setupPresenterMapping($extension);
			}

			if ($extension instanceof ITranslationProvider) {
				$this->setupTranslation($extension);
			}

			if ($extension instanceof IRouterProvider) {
				$this->setupRouter($extension);
			}
		}

		$router = $builder->getDefinition('router');

		foreach ($builder->findByTag(self::TAG_ROUTER) as $serviceName => $priority) {
			if (is_bool($priority)) {
				$priority = 100;
			}

			$routerFactories[$priority][$serviceName] = $serviceName;
		}

		if (isset($routerFactories) && count($routerFactories) > 0) {
			krsort($routerFactories, \SORT_NUMERIC);

			foreach ($routerFactories as $priority => $items) {
				$routerFactories[$priority] = $items;
			}

			foreach ($routerFactories as $items) {
				foreach ($items as $serviceName) {
					$factory = new Statement(['@' . $serviceName, 'createRouter']);
					$router->addSetup('offsetSet', [null, $factory]);
				}
			}
		}
	}


	/**
	 * @param ITranslationProvider $extension
	 */
	private function setupTranslation(ITranslationProvider $extension): void
	{
		$builder = $this->getContainerBuilder();

		$translation = $builder->getDefinition('translation.translator');

		foreach (Finder::findFiles('*.*.neon')->from($extension->getTranslationLocation()) as $file) {
			/** @var \SplFileInfo $file */
			if (!preg_match('~^(?P<domain>.*?)\.(?P<locale>[^\.]+)\.(?P<format>[^\.]+)$~', $file->getFilename(), $m)) {
				continue;
			}

			$translation->addSetup('addResource', [$m['format'], $file->getPathname(), $m['locale'], $m['domain']]);
			$builder->addDependency($file->getPathname());
		}
	}


	/**
	 * @param IRouterProvider $extension
	 */
	private function setupRouter(IRouterProvider $extension): void
	{
		$builder = $this->getContainerBuilder();

		foreach ($extension->getRouterSettings() as $priority => $router) {
			$builder->addDefinition($this->prefix($extension->name . '.router.' . $priority))
				->setFactory($router)
				->addTag(self::TAG_ROUTER, $priority);
		}
	}


	/**
	 * @param IPresenterMappingProvider $extension
	 */
	private function setupPresenterMapping(IPresenterMappingProvider $extension): void
	{
		$builder = $this->getContainerBuilder();

		/** @var ServiceDefinition $presenterFactory */
		$presenterFactory = $builder->getDefinition('application.presenterFactory');
		$presenterFactory->addSetup('setMapping', [$extension->getPresenterMapping()]);
	}
}
