<?php

declare(strict_types=1);

namespace Cleevio\UI;

use Nette\Forms\Controls\MultiChoiceControl;
use Nette\Forms\Helpers;
use Nette\Utils\Arrays;
use Nette\Utils\Html;

class DynamicMultiSelect extends MultiChoiceControl
{
	/**
	 * @var array of option / optgroup
	 */
	private $options = [];

	/**
	 * @var array
	 */
	private $optionAttributes = [];


	public function __construct($label = null, ?array $items = null)
	{
		parent::__construct($label, $items);
		$this->setOption('type', 'select');
	}

	/**
	 * Sets options and option groups from which to choose.
	 * @param array $items
	 * @param bool $useKeys
	 * @return static
	 */
	public function setItems(array $items, bool $useKeys = true)
	{
		if (!$useKeys) {
			$res = [];

			foreach ($items as $key => $value) {
				unset($items[$key]);

				if (is_array($value)) {
					foreach ($value as $val) {
						$res[$key][(string) $val] = $val;
					}
				} else {
					$res[(string) $value] = $value;
				}
			}

			$items = $res;
		}

		$this->options = $items;

		return parent::setItems(Arrays::flatten($items, true));
	}


	/**
	 * Generates control's HTML element.
	 */
	public function getControl(): Html
	{
		$items = [];

		foreach ($this->options as $key => $value) {
			$items[is_array($value) ? $this->translate($key) : $key] = $this->translate($value);
		}

		return Helpers::createSelectBox(
			$items,
			[
				'disabled:' => is_array($this->disabled) ? $this->disabled : null,
			] + $this->optionAttributes,
			$this->value
		)->addAttributes(parent::getControl()->attrs)->multiple(true);
	}


	/**
	 * @return static
	 */
	public function addOptionAttributes(array $attributes)
	{
		$this->optionAttributes = $attributes + $this->optionAttributes;

		return $this;
	}


	public function getOptionAttributes(): array
	{
		return $this->optionAttributes;
	}

	/**
	 * Returns selected keys.
	 */
	public function getValue(): array
	{
		return $this->value;
	}
}
