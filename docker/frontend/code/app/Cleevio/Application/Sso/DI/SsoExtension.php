<?php

declare(strict_types=1);

namespace Cleevio\Sso\DI;

use Cleevio\Sso\SsoContext;
use Nette\DI\CompilerExtension;

class SsoExtension extends CompilerExtension
{

	public function loadConfiguration()
	{
		$container = $this->getContainerBuilder();

		$config = (array) $this->getConfig();

		$container->addDefinition($this->prefix('sso'))
			->setFactory(SsoContext::class)
			->setArguments([$config['uri'] !== '' ? $config['uri'] : null, $config['timeout']]);

	}
}
