<?php

declare(strict_types=1);

namespace Cleevio\Router\DI;

use Cleevio\Modules\Providers\IRouterProvider;
use Cleevio\Router\Router\RouterFactory;
use Nette\DI\CompilerExtension;

class RouterExtension extends CompilerExtension implements IRouterProvider
{

	public function getRouterSettings(): array
	{
		return [
			100 => RouterFactory::class,
		];
	}
}
