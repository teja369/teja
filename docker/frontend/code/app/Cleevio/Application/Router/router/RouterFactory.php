<?php

declare(strict_types=1);

namespace Cleevio\Router\Router;

use Nette;
use Nette\Application\Routers\Route;
use Nette\Application\Routers\RouteList;

class RouterFactory
{
	/**
	 * @return Nette\Application\IRouter
	 */
	public function createRouter()
	{
		$router = new RouteList;

		$router[] = $backModule = new RouteList('Back');

		$backModule[] = new Route('backend/<action>[/<id>]', [
			'presenter' => 'Homepage',
			'action' => 'default',
		]);

		return $router;
	}

}
