<?php

declare(strict_types=1);

namespace Cleevio\Registrations\API\Responses;

use Cleevio\ApiClient\IResponse;
use InvalidArgumentException;

class RegistrationResponse implements IResponse
{

	/**
	 * @var int
	 */
	private $id;

	/**
	 * @var string
	 */
	private $name;


	/**
	 * RegistrationResponse constructor.
	 * @param array $registration
	 */
	public function __construct(array $registration)
	{
		if (isset($registration['id'], $registration['name'])) {
			$this->id = $registration['id'];
			$this->name = $registration['name'];
		} else {
			throw new InvalidArgumentException;
		}
	}


	/**
	 * @return int
	 */
	public function getId(): int
	{
		return $this->id;
	}


	/**
	 * @return string
	 */
	public function getName(): string
	{
		return $this->name;
	}

}
