<?php

declare(strict_types=1);

namespace Cleevio\Registrations\API\Requests;

use Cleevio\API\BaseRequest;
use Cleevio\ApiClient\Exception\ApiException;
use Cleevio\Registrations\API\Responses\RegistrationsResponse;
use InvalidArgumentException;

class RegistrationsGetRequest extends BaseRequest implements IRegistrationsGetRequest
{

	/**
	 * @var int
	 */
	private $tripId;


	/**
	 * Endpoint path
	 * @return string
	 */
	protected function path(): string
	{
		if ($this->tripId === null) {
			throw new InvalidArgumentException;
		}

		return '/registrations?trip=' . $this->tripId;
	}


	/**
	 * Method
	 * @return string
	 */
	protected function method(): string
	{
		return 'GET';
	}


	/**
	 * Determines whether request requires authorization
	 * @return bool
	 */
	function requiresAuth(): bool
	{
		return true;
	}


	/**
	 * @param int $tripId
	 */
	public function setTripId(int $tripId): void
	{
		$this->tripId = $tripId;
	}


	/**
	 * Execute request and return a response
	 * @return RegistrationsResponse
	 * @throws ApiException
	 */
	function execute(): RegistrationsResponse
	{
		return new RegistrationsResponse($this->request());
	}
}
