<?php

declare(strict_types=1);

namespace Cleevio\Registrations\API\Responses;

use Cleevio\ApiClient\IResponse;

class RegistrationsResponse implements IResponse
{

	/**
	 * @var RegistrationResponse[]
	 */
	private $registrations = [];


	/**
	 * RegistrationsResponse constructor.
	 * @param array $registrations
	 */
	public function __construct(array $registrations)
	{
		$this->registrations = array_map(static function($registration) {
			return new RegistrationResponse($registration);
		}, $registrations['items']);
	}

	/**
	 * @return RegistrationResponse[]
	 */
	public function getRegistrations(): array
	{
		return $this->registrations;
	}

}
