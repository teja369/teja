<?php

declare(strict_types=1);

namespace Cleevio\Registrations\API\Requests;

interface IRegistrationsGetRequest
{

	/**
	 * @param int $tripId
	 */
	public function setTripId(int $tripId): void;
}
