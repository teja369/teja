<?php

declare(strict_types=1);

namespace Cleevio\Trips\API\Responses;

use Cleevio\ApiClient\DTO\PaginationResponse;

class TripsResponse extends PaginationResponse
{

	/**
	 * @var TripResponse[]
	 */
	private $items;

	/**
	 * TripsResponse constructor.
	 * @param array $response
	 */
	public function __construct(array $response)
	{
		parent::__construct($response['data']);

		$this->items = array_map(static function ($trip) {
			return new TripResponse($trip);
		}, $response['data']['items']);

	}

	/**
	 * @return TripResponse[]
	 */
	public function getItems(): array
	{
		return $this->items;
	}
}
