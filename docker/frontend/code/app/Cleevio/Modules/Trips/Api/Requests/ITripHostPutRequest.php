<?php

declare(strict_types=1);

namespace Cleevio\Trips\API\Requests;

interface ITripHostPutRequest
{

	/**
	 * @param int $id
	 */
	public function setTripId(int $id): void;


	/**
	 * @param int $id
	 */
	public function setHostId(int $id): void;
}
