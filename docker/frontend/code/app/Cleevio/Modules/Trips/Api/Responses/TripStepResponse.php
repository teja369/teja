<?php

declare(strict_types=1);

namespace Cleevio\Trips\API\Responses;

use Cleevio\ApiClient\IResponse;
use InvalidArgumentException;

class TripStepResponse implements IResponse
{

	/**
	 * @var string|null
	 */
	private $step;

	/**
	 * @var string|null
	 */
	private $error;

	/**
	 * @var array
	 */
	private $params;

	/**
	 * TripStepResponse constructor.
	 * @param array $trip
	 */
	public function __construct(array $trip)
	{
		if (array_key_exists('step', $trip) &&
			array_key_exists('error', $trip) &&
			array_key_exists('params', $trip)) {
			$this->step = $trip['step'];
			$this->error = $trip['error'];
			$this->params = (array) $trip['params'];
		} else {
			throw new InvalidArgumentException;
		}
	}

	public function getStep(): ?string
	{
		return $this->step;
	}

	public function getError(): ?string
	{
		return $this->error;
	}

	public function getParams(): array
	{
		return $this->params;
	}

}
