<?php

declare(strict_types=1);

namespace Cleevio\Trips\API\Requests;

use Cleevio\API\BaseRequest;
use Cleevio\ApiClient\Exception\ApiException;
use Cleevio\Trips\API\Responses\TripResponse;
use InvalidArgumentException;

class TripHostPutRequest extends BaseRequest implements ITripHostPutRequest
{

	/**
	 * @var int
	 */
	private $tripId;

	/**
	 * @var int
	 */
	private $hostId;


	/**
	 * Endpoint path
	 * @return string
	 */
	protected function path(): string
	{
		if ($this->tripId === null || $this->hostId === null) {
			throw new InvalidArgumentException;
		}

		return sprintf('/trips/%s/host/%s', $this->tripId, $this->hostId);
	}


	/**
	 * Method
	 * @return string
	 */
	protected function method(): string
	{
		return 'PUT';
	}


	/**
	 * Determines whether request requires authorization
	 * @return bool
	 */
	function requiresAuth(): bool
	{
		return true;
	}


	/**
	 * @param int $id
	 */
	function setTripId(int $id): void
	{
		$this->tripId = $id;
	}


	/**
	 * @param int $id
	 */
	function setHostId(int $id): void
	{
		$this->hostId = $id;
	}


	/**
	 * Execute request and return a response
	 * @return TripResponse
	 * @throws ApiException
	 */
	function execute(): TripResponse
	{
		return new TripResponse($this->request());
	}
}
