<?php

declare(strict_types=1);

namespace Cleevio\Trips\API\Requests;

interface ITripAnswersPutRequest
{

	/**
	 * @param int $tripId
	 */
	public function setTripId(int $tripId): void;
}
