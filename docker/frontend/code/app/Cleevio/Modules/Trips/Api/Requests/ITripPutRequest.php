<?php

declare(strict_types=1);

namespace Cleevio\Trips\API\Requests;

interface ITripPutRequest
{

	function setTripId(int $id): void;

}
