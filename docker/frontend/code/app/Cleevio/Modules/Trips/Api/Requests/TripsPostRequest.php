<?php

declare(strict_types=1);

namespace Cleevio\Trips\API\Requests;

use Cleevio\API\BaseRequest;
use Cleevio\ApiClient\Exception\ApiException;
use Cleevio\Trips\API\Responses\TripResponse;

class TripsPostRequest extends BaseRequest implements ITripsPostRequest
{

	/**
	 * Endpoint path
	 * @return string
	 */
	protected function path(): string
	{
		return '/trips';
	}


	/**
	 * Method
	 * @return string
	 */
	protected function method(): string
	{
		return 'POST';
	}


	/**
	 * Determines whether request requires authorization
	 * @return bool
	 */
	function requiresAuth(): bool
	{
		return true;
	}


	/**
	 * Execute request and return a response
	 * @return TripResponse
	 * @throws ApiException
	 */
	function execute(): TripResponse
	{
		return new TripResponse($this->request());
	}
}
