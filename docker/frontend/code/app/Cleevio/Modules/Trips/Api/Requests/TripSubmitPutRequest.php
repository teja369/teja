<?php

declare(strict_types=1);

namespace Cleevio\Trips\API\Requests;

use Cleevio\API\BaseRequest;
use Cleevio\ApiClient\DTO\EmptyResponse;
use Cleevio\ApiClient\Exception\ApiException;
use InvalidArgumentException;

class TripSubmitPutRequest extends BaseRequest implements ITripSubmitPutRequest
{

	/**
	 * @var int
	 */
	private $tripId;


	/**
	 * Endpoint path
	 * @return string
	 */
	protected function path(): string
	{
		if ($this->tripId === null) {
			throw new InvalidArgumentException;
		}

		return sprintf('/trips/%s/submit', $this->tripId);
	}


	/**
	 * Method
	 * @return string
	 */
	protected function method(): string
	{
		return 'PUT';
	}


	/**
	 * Determines whether request requires authorization
	 * @return bool
	 */
	function requiresAuth(): bool
	{
		return true;
	}


	/**
	 * @param int $tripId
	 */
	function setTripId(int $tripId): void
	{
		$this->tripId = $tripId;
	}


	/**
	 * @return EmptyResponse
	 * @throws ApiException
	 */
	function execute(): EmptyResponse
	{
		$this->request();

		return new EmptyResponse;
	}
}
