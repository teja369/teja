<?php

declare(strict_types=1);

namespace Cleevio\Trips\API\Requests;

use Cleevio\API\BaseRequest;
use Cleevio\ApiClient\Exception\ApiException;
use Cleevio\Trips\API\Responses\TripResponse;
use InvalidArgumentException;

class TripGetCopyRequest extends BaseRequest implements ITripGetCopyRequest
{
	/**
	 * @var int
	 */
	private $tripId;

	/**
	 * Endpoint path
	 * @return string
	 */
	protected function path(): string
	{
		if ($this->tripId === null) {
			throw new InvalidArgumentException;
		}

		return '/trips/' . $this->tripId . '/copy';
	}

	/**
	 * Method
	 * @return string
	 */
	protected function method(): string
	{
		return 'GET';
	}

	/**
	 * Determines whether request requires authorization
	 * @return bool
	 */
	function requiresAuth(): bool
	{
		return true;
	}

	/**
	 * @param int $id
	 */
	function setTripId(int $id): void
	{
		$this->tripId = $id;
	}

	/**
	 * Execute request and return a response
	 * @return TripResponse
	 * @throws ApiException
	 */
	function execute(): TripResponse
	{
		return new TripResponse($this->request());
	}
}
