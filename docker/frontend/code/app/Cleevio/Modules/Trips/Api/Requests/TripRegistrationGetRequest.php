<?php

declare(strict_types=1);

namespace Cleevio\Trips\API\Requests;

use Cleevio\API\BaseRequest;
use Cleevio\ApiClient\Exception\ApiException;
use Cleevio\Trips\API\Responses\TripResponse;

class TripRegistrationGetRequest extends BaseRequest implements ITripRegistrationGetRequest
{

	/**
	 * @var int
	 */
	private $tripId;


	/**
	 * Endpoint path
	 * @return string
	 */
	protected function path(): string
	{
		return sprintf('/trips/%s/registration', $this->tripId);
	}


	/**
	 * Method
	 * @return string
	 */
	protected function method(): string
	{
		return 'GET';
	}


	/**
	 * Determines whether request requires authorization
	 * @return bool
	 */
	function requiresAuth(): bool
	{
		return true;
	}


	public function setTripId(int $tripId): void
	{
		$this->tripId = $tripId;
	}


	/**
	 * Execute request and return a response
	 * @return TripResponse
	 * @throws ApiException
	 */
	function execute(): TripResponse
	{
		return new TripResponse($this->request());
	}
}
