<?php

declare(strict_types=1);

namespace Cleevio\Trips\API\Responses;

use Cleevio\ApiClient\IResponse;
use Cleevio\Countries\API\Responses\CountryResponse;
use Cleevio\Questions\API\Responses\QuestionResponse;
use InvalidArgumentException;

class TripAnswerResponse implements IResponse
{

	/**
	 * @var int
	 */
	private $id;

	/**
	 * @var QuestionResponse
	 */
	private $question;

	/**
	 * @var string|null
	 */
	private $text;

	/**
	 * @var bool|null
	 */
	private $bool;

	/**
	 * @var int|null
	 */
	private $number;

	/**
	 * @var array|null
	 */
	private $choice;

	/**
	 * @var string|null
	 */
	private $datetime;

	/**
	 * @var CountryResponse|null
	 */
	private $country;


	/**
	 * TripAnswersResponse constructor.
	 * @param array $answer
	 */
	public function __construct(array $answer)
	{
		if (array_key_exists('id', $answer) && array_key_exists('question', $answer)) {
			$this->id = $answer['id'];
			$this->question = new QuestionResponse($answer['question']);
			$this->text = $answer['text'] ?? null;
			$this->bool = $answer['bool'] ?? null;
			$this->number = $answer['number'] ?? null;
			$this->choice = $answer['choice'] ?? null;
			$this->datetime = $answer['datetime'] ?? null;
			$this->country = array_key_exists('country', $answer) && $answer['country'] !== null
				? new CountryResponse($answer['country'])
				: null;
		} else {
			throw new InvalidArgumentException;
		}
	}


	/**
	 * @return int
	 */
	public function getId(): int
	{
		return $this->id;
	}


	/**
	 * @return QuestionResponse
	 */
	public function getQuestion(): QuestionResponse
	{
		return $this->question;
	}


	/**
	 * @return string|null
	 */
	public function getText(): ?string
	{
		return $this->text;
	}


	/**
	 * @return bool|null
	 */
	public function getBool(): ?bool
	{
		return $this->bool;
	}


	/**
	 * @return int|null
	 */
	public function getNumber(): ?int
	{
		return $this->number;
	}


	/**
	 * @return array|null
	 */
	public function getChoice(): ?array
	{
		return $this->choice;
	}


	/**
	 * Todo: DateTime instead of string
	 * @return string|null
	 */
	public function getDatetime(): ?string
	{
		return $this->datetime;
	}


	/**
	 * @return CountryResponse|null
	 */
	public function getCountry(): ?CountryResponse
	{
		return $this->country;
	}

}
