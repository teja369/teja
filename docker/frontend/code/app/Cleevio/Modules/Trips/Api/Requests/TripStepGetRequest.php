<?php

declare(strict_types=1);

namespace Cleevio\Trips\API\Requests;

use Cleevio\API\BaseRequest;
use Cleevio\ApiClient\Exception\ApiException;
use Cleevio\Trips\API\Responses\TripStepResponse;
use InvalidArgumentException;

class TripStepGetRequest extends BaseRequest implements ITripStepGetRequest
{

	/**
	 * @var int
	 */
	private $tripId;


	/**
	 * Endpoint path
	 * @return string
	 */
	protected function path(): string
	{
		if ($this->tripId === null) {
			throw new InvalidArgumentException;
		}

		return sprintf('/trips/%s/step', $this->tripId);
	}


	/**
	 * Method
	 * @return string
	 */
	protected function method(): string
	{
		return 'GET';
	}


	/**
	 * Determines whether request requires authorization
	 * @return bool
	 */
	function requiresAuth(): bool
	{
		return true;
	}


	/**
	 * @param int $id
	 */
	function setTripId(int $id): void
	{
		$this->tripId = $id;
	}

	/**
	 * Execute request and return a response
	 * @return TripStepResponse
	 * @throws ApiException
	 */
	function execute(): TripStepResponse
	{
		return new TripStepResponse($this->request());
	}
}
