<?php

declare(strict_types=1);

namespace Cleevio\Trips\API\Requests;

interface ITripCancelPutRequest
{

	/**
	 * @param int $id
	 */
	public function setId(int $id): void;

}
