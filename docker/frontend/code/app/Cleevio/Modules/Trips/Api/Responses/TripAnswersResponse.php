<?php

declare(strict_types=1);

namespace Cleevio\Trips\API\Responses;

use Cleevio\ApiClient\IResponse;

class TripAnswersResponse implements IResponse
{

	/**
	 * @var TripAnswerResponse[]
	 */
	private $answers;


	/**
	 * TripAnswersResponse constructor.
	 * @param array $answers
	 */
	public function __construct(array $answers)
	{
		$this->answers = array_map(static function ($answer) {
			return new TripAnswerResponse($answer);
		}, $answers);

	}


	/**
	 * @return TripAnswerResponse[]
	 */
	public function getAnswers(): array
	{
		usort($this->answers, static function (TripAnswerResponse $a, TripAnswerResponse $b) {
			return $a->getQuestion()->getId() > $b->getQuestion()->getId();
		});

		return $this->answers;
	}

}
