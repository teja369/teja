<?php

declare(strict_types=1);

namespace Cleevio\Trips\API\Responses;

use Cleevio\ApiClient\Exception\BadRequestException;
use Cleevio\ApiClient\IResponse;
use Cleevio\Registrations\API\Responses\RegistrationResponse;
use DateTime;
use InvalidArgumentException;

class TripRegistrationResponse implements IResponse
{

	private const DATE_FORMAT = 'Y-m-d H:i:s';

	/**
	 * @var RegistrationResponse[]
	 */
	private $registrations;

	/**
	 * @var DateTime|null
	 */
	private $checked;


	/**
	 * TripResponse constructor.
	 * @param array $tripRegistration
	 * @throws BadRequestException
	 */
	public function __construct(array $tripRegistration)
	{
		if (array_key_exists('registrations', $tripRegistration) && array_key_exists('checked', $tripRegistration)) {
			$checked = $tripRegistration['checked'] === null ? null : DateTime::createFromFormat(self::DATE_FORMAT, $tripRegistration['checked']);

			if ($checked === false) {
				throw new BadRequestException('Invalid date format');
			}

			$this->registrations = array_map(static function ($registration) {
				return new RegistrationResponse($registration);
			}, $tripRegistration['registrations']);
			$this->checked = $checked;
		} else {
			throw new InvalidArgumentException;
		}
	}


	/**
	 * @return RegistrationResponse[]
	 */
	public function getRegistrations(): array
	{
		return $this->registrations;
	}


	/**
	 * @return DateTime|null
	 */
	public function getChecked(): ?DateTime
	{
		return $this->checked;
	}

}
