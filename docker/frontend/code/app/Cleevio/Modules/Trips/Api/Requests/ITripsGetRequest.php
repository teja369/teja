<?php

declare(strict_types=1);

namespace Cleevio\Trips\API\Requests;

interface ITripsGetRequest
{

	/**
	 * @param string|null $state
	 */
	public function setTripState(?string $state): void;


	/**
	 * @param string|null $minStartDate
	 */
	public function setMinStartDate(?string $minStartDate): void;


	/**
	 * @param string|null $maxStartDate
	 */
	public function setMaxStartDate(?string $maxStartDate): void;


	/**
	 * @param string|null $minEndDate
	 */
	public function setMinEndDate(?string $minEndDate): void;


	/**
	 * @param string|null $maxEndDate
	 */
	public function setMaxEndDate(?string $maxEndDate): void;


	/**
	 * @param string|null $code
	 */
	public function setCountry(?string $code): void;


	/**
	 * @param string|null $column
	 */
	public function setOrderBy(?string $column): void;


	/**
	 * @param string|null $direction
	 */
	public function setOrderDirection(?string $direction): void;


	/**
	 * @param int|null $id
	 */
	public function setHostId(?int $id): void;


	/**
	 * @param int|null $id
	 */
	public function setFavorite(?int $id): void;


	public function setSubmitted(): void;

}
