<?php

declare(strict_types=1);

namespace Cleevio\Trips\API\Requests;

interface ITripRegistrationGetRequest
{

	/**
	 * @param int $tripId
	 */
	public function setTripId(int $tripId): void;
}
