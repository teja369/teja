<?php

declare(strict_types=1);

namespace Cleevio\Trips\API\Requests;

interface ITripGetCopyRequest
{

	function setTripId(int $id): void;

}
