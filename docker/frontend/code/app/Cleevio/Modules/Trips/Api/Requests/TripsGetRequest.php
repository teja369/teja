<?php

declare(strict_types=1);

namespace Cleevio\Trips\API\Requests;

use Cleevio\ApiClient\Exception\ApiException;
use Cleevio\ApiClient\Requests\PaginationRequest;
use Cleevio\Trips\API\Responses\TripsResponse;

class TripsGetRequest extends PaginationRequest implements ITripsGetRequest
{

	public const TRIP_PLANNED = [
		'submitted',
		'processed',
	];

	public const TRIP_SUBMITTED = [
		'submitted',
		'processed',
		'on-hold',
		'cancelled',
	];

	/**
	 * @var array|null
	 */
	private $state;

	/**
	 * @var string|null
	 */
	private $minStartDate;

	/**
	 * @var string|null
	 */
	private $maxStartDate;

	/**
	 * @var string|null
	 */
	private $minEndDate;

	/**
	 * @var string|null
	 */
	private $maxEndDate;

	/**
	 * @var string|null
	 */
	private $country;

	/**
	 * @var string|null
	 */
	private $orderBy;

	/**
	 * @var string|null
	 */
	private $orderDirection;

	/**
	 * @var int|null
	 */
	private $hostId;

	/**
	 * @var int|null
	 */
	private $favorite;


	/**
	 * Endpoint path
	 * @return string
	 */
	protected function path(): string
	{
		$path = '/trips';

		$params = [
			'limit' => $this->getLimit(),
			'page' => $this->getPage(),
			'state' => $this->state === null
				? null
				: implode('|', $this->state),
			'minStartDate' => $this->minStartDate,
			'maxStartDate' => $this->maxStartDate,
			'minEndDate' => $this->minEndDate,
			'maxEndDate' => $this->maxEndDate,
			'country' => $this->country,
			'orderBy' => $this->orderBy,
			'orderDir' => $this->orderDirection,
			'host' => $this->hostId,
			'isFavorite' => $this->favorite,
		];

		$query = http_build_query($params);

		return strlen($query) > 0
			? $path . '?' . $query
			: $path;
	}


	/**
	 * Method
	 * @return string
	 */
	protected function method(): string
	{
		return 'GET';
	}


	/**
	 * Determines whether request requires authorization
	 * @return bool
	 */
	function requiresAuth(): bool
	{
		return true;
	}


	/**
	 * @param string $state
	 */
	public function setTripState(?string $state): void
	{
		$this->state[] = $state;
	}


	public function setSubmitted(): void
	{
		$this->state = self::TRIP_SUBMITTED;
	}

	public function setPlanned(): void
	{
		$this->state = self::TRIP_PLANNED;
	}


	public function getMinStartDate(): ?string
	{
		return $this->minStartDate;
	}


	/**
	 * @param string|null $minStartDate
	 */
	public function setMinStartDate(?string $minStartDate): void
	{
		$this->minStartDate = $minStartDate;
	}


	public function getMaxStartDate(): ?string
	{
		return $this->maxStartDate;
	}


	/**
	 * @param string|null $maxStartDate
	 */
	public function setMaxStartDate(?string $maxStartDate): void
	{
		$this->maxStartDate = $maxStartDate;
	}


	public function getMinEndDate(): ?string
	{
		return $this->minEndDate;
	}


	/**
	 * @param string|null $minEndDate
	 */
	public function setMinEndDate(?string $minEndDate): void
	{
		$this->minEndDate = $minEndDate;
	}


	public function getMaxEndDate(): ?string
	{
		return $this->maxEndDate;
	}


	/**
	 * @param string|null $maxEndDate
	 */
	public function setMaxEndDate(?string $maxEndDate): void
	{
		$this->maxEndDate = $maxEndDate;
	}


	/**
	 * @param string|null $code
	 */
	public function setCountry(?string $code): void
	{
		$this->country = $code;
	}


	/**
	 * @param string|null $column
	 */
	public function setOrderBy(?string $column): void
	{
		$this->orderBy = $column;
	}


	/**
	 * @param string|null $direction
	 */
	public function setOrderDirection(?string $direction): void
	{
		$this->orderDirection = $direction;
	}


	/**
	 * @param int|null $id
	 */
	public function setHostId(?int $id): void
	{
		$this->hostId = $id;
	}


	/**
	 * @param int|null $favorite
	 */
	public function setFavorite(?int $favorite): void
	{
		$this->favorite = $favorite;
	}


	/**
	 * Execute request and return a response
	 * @return TripsResponse
	 * @throws ApiException
	 */
	function execute(): TripsResponse
	{
		return new TripsResponse($this->request());
	}
}
