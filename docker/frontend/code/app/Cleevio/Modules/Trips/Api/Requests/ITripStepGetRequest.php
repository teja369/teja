<?php

declare(strict_types=1);

namespace Cleevio\Trips\API\Requests;

interface ITripStepGetRequest
{

	/**
	 * @param int $id
	 */
	public function setTripId(int $id): void;
}
