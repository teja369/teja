<?php

declare(strict_types=1);

namespace Cleevio\Trips\FrontModule\Presenters;

use Cleevio\ApiClient\Exception\ApiException;
use Cleevio\Countries\API\Requests\CountryHomeGetRequest;
use Cleevio\Fields\API\Requests\FieldsGetRequest;
use Cleevio\FrontModule\Presenters\BasePresenter;
use Cleevio\Hosts\API\Requests\HostsGetRequest;
use Cleevio\Questions\API\Requests\GroupsGetRequest;
use Cleevio\Questions\API\Requests\QuestionsGetRequest;
use Cleevio\Registrations\API\Requests\RegistrationsGetRequest;
use Cleevio\Trips\API\Requests\TripCancelPutRequest;
use Cleevio\Trips\API\Requests\TripDeleteRequest;
use Cleevio\Trips\API\Requests\TripFavoritePutRequest;
use Cleevio\Trips\API\Requests\TripGetCopyRequest;
use Cleevio\Trips\API\Requests\TripGetRequest;
use Cleevio\Trips\API\Requests\TripRegistrationGetRequest;
use Cleevio\Trips\API\Requests\TripsGetRequest;
use Cleevio\Trips\API\Requests\TripStepGetRequest;
use Cleevio\Trips\API\Requests\TripSubmitPutRequest;
use Cleevio\Trips\API\Responses\TripResponse;
use Cleevio\Trips\Components\CreateTripForm\CreateTripForm;
use Cleevio\Trips\Components\CreateTripForm\ICreateTripFormFactory;
use Cleevio\Trips\Components\CreateTripHostForm\CreateTripHostForm;
use Cleevio\Trips\Components\CreateTripHostForm\ICreateTripHostFormFactory;
use Cleevio\Trips\Components\CreateTripQuestionsForm\CreateTripQuestionsForm;
use Cleevio\Trips\Components\CreateTripQuestionsForm\ICreateTripQuestionsFormFactory;
use DateTimeZone;
use InvalidArgumentException;
use Nette\Application\AbortException;
use Nette\Application\UI\InvalidLinkException;

class TripCreatePresenter extends BasePresenter
{

	/**
	 * @var TripsGetRequest
	 */
	protected $tripsGetRequest;

	/**
	 * @var TripDeleteRequest
	 */
	protected $tripDeleteRequest;

	/**
	 * @var FieldsGetRequest
	 */
	protected $fieldsGetRequest;

	/**
	 * @var HostsGetRequest
	 */
	protected $hostsGetRequest;

	/**
	 * @var TripGetRequest
	 */
	private $tripGetRequest;

	/**
	 * @var QuestionsGetRequest
	 */
	private $questionsGetRequest;

	/**
	 * @var RegistrationsGetRequest
	 */
	private $registrationsGetRequest;

	/**
	 * @var TripRegistrationGetRequest
	 */
	private $tripRegistrationGetRequest;

	/**
	 * @var TripSubmitPutRequest
	 */
	private $tripSubmitPutRequest;

	/**
	 * @var TripStepGetRequest
	 */
	private $tripStepGetRequest;

	/**
	 * @var GroupsGetRequest
	 */
	private $groupsGetRequest;

	/**
	 * @var TripResponse
	 */
	private $trip;

	/**
	 * @var int|null
	 */
	private $group;

	/**
	 * @var TripFavoritePutRequest
	 */
	private $tripFavoritePutRequest;

	/**
	 * @var TripGetCopyRequest
	 */
	private $tripGetCopyRequest;

	/**
	 * @var CountryHomeGetRequest
	 */
	private $countryHomeGetRequest;

	/**
	 * @var TripCancelPutRequest
	 */
	private $tripCancelPutRequest;


	/**
	 * TripCreatePresenter constructor.
	 * @param TripsGetRequest $tripsGetRequest
	 * @param TripGetRequest $tripGetRequest
	 * @param TripDeleteRequest $tripDeleteRequest
	 * @param FieldsGetRequest $fieldsGetRequest
	 * @param HostsGetRequest $hostsGetRequest
	 * @param QuestionsGetRequest $questionsGetRequest
	 * @param GroupsGetRequest $groupsGetRequest
	 * @param RegistrationsGetRequest $registrationsGetRequest
	 * @param TripRegistrationGetRequest $tripRegistrationGetRequest
	 * @param TripSubmitPutRequest $tripSubmitPutRequest
	 * @param TripStepGetRequest $tripStepGetRequest
	 * @param TripFavoritePutRequest $tripFavoritePutRequest
	 * @param TripGetCopyRequest $tripGetCopyRequest
	 * @param CountryHomeGetRequest $countryHomeGetRequest
	 * @param TripCancelPutRequest $tripCancelPutRequest
	 */
	public function __construct(
		TripsGetRequest $tripsGetRequest,
		TripGetRequest $tripGetRequest,
		TripDeleteRequest $tripDeleteRequest,
		FieldsGetRequest $fieldsGetRequest,
		HostsGetRequest $hostsGetRequest,
		QuestionsGetRequest $questionsGetRequest,
		GroupsGetRequest $groupsGetRequest,
		RegistrationsGetRequest $registrationsGetRequest,
		TripRegistrationGetRequest $tripRegistrationGetRequest,
		TripSubmitPutRequest $tripSubmitPutRequest,
		TripStepGetRequest $tripStepGetRequest,
		TripFavoritePutRequest $tripFavoritePutRequest,
		TripGetCopyRequest $tripGetCopyRequest,
		CountryHomeGetRequest $countryHomeGetRequest,
		TripCancelPutRequest $tripCancelPutRequest
	)
	{
		parent::__construct();

		$this->tripsGetRequest = $tripsGetRequest;
		$this->tripDeleteRequest = $tripDeleteRequest;
		$this->fieldsGetRequest = $fieldsGetRequest;
		$this->hostsGetRequest = $hostsGetRequest;
		$this->tripGetRequest = $tripGetRequest;
		$this->questionsGetRequest = $questionsGetRequest;
		$this->registrationsGetRequest = $registrationsGetRequest;
		$this->tripRegistrationGetRequest = $tripRegistrationGetRequest;
		$this->tripSubmitPutRequest = $tripSubmitPutRequest;
		$this->groupsGetRequest = $groupsGetRequest;
		$this->tripStepGetRequest = $tripStepGetRequest;
		$this->tripFavoritePutRequest = $tripFavoritePutRequest;
		$this->tripGetCopyRequest = $tripGetCopyRequest;
		$this->countryHomeGetRequest = $countryHomeGetRequest;
		$this->tripCancelPutRequest = $tripCancelPutRequest;
	}


	/**
	 * @param int $id
	 * @throws AbortException
	 * @throws ApiException
	 */
	public function actionCreateJunction(int $id): void
	{
		$this->tripStepGetRequest->setTripId($id);
		$response = $this->tripStepGetRequest->execute();

		if ($this->isAjax()) {
			$this->payload->forceRedirect = true;
		}

		if ($response->getStep() === null) {
			$this->redirectPermanent(":Front:Homepage:default");
		}

		$group = array_key_exists('group', $response->getParams())
			? $response->getParams()['group']
			: null;

		switch ($response->getStep()) {
			case 'purpose':
				$this->redirectPermanent(":Trips:Front:TripCreate:create-purpose", ['id' => $id, 'groupId' => $group]);

				break;
			case 'host':
				$this->redirectPermanent(":Trips:Front:TripCreate:create-host", ['id' => $id]);

				break;
			case 'registration':
				$this->redirectPermanent(":Trips:Front:TripCreate:create-registration", ['id' => $id]);

				break;
			case 'details':
				$this->redirectPermanent(":Trips:Front:TripCreate:details", ['id' => $id, 'groupId' => $group]);

				break;
			case 'summary':
				$this->redirectPermanent(":Trips:Front:TripCreate:summary", ['id' => $id]);

				break;
			default:
				if ($response->getError() !== null) {
					$this->flashMessage($this->translator->translate('frontend.' . $response->getError(), $response->getParams()), 'danger');
				}

				$this->redirectPermanent(":Front:Homepage:default");

				break;
		}
	}


	/**
	 * @param int $id
	 * @throws AbortException
	 * @throws ApiException
	 */
	public function actionRepeat(int $id): void
	{
		$this->tripGetCopyRequest->setTripId($id);
		$trip = $this->tripGetCopyRequest->execute();

		if ($this->isAjax()) {
			$this->payload->forceRedirect = true;
		}

		$this->redirectPermanent(":Trips:Front:TripCreate:create", [
			'id' => $trip->getId(),
			'repeat' => true,
		]);
	}


	/**
	 * @param int|null $id
	 * @param string|null $country
	 * @param bool $repeat
	 * @throws ApiException
	 */
	public function actionCreate(?int $id, ?string $country = null, bool $repeat = false): void
	{
		$this['createTripForm']->setPrefilledCountry($country);

		if ($id !== null) {
			$this->tripGetRequest->setTripId($id);
			$trip = $this->tripGetRequest->execute();

			$this['createTripForm']->setTrip($trip);
		}

		if ($repeat) {
			$this['createTripForm']->setRepeat();
		}

		$this->template->trip = $trip ?? null;
		$this->template->step = 0;
		$this->template->prev = null;
	}


	/**
	 * @param int $id
	 * @param bool|null $prev
	 * @param int|null $groupId
	 * @throws AbortException
	 * @throws ApiException
	 * @throws InvalidLinkException
	 */
	public function actionCreatePurpose(int $id, ?bool $prev, ?int $groupId): void
	{
		$this->actionQuestions($id, $groupId, 1,
			$prev !== null && $prev === true,
			'purpose',
			':Trips:Front:TripCreate:create',
			':Trips:Front:TripCreate:create-host',
			':Trips:Front:TripCreate:create-purpose');
	}


	/**
	 * @param int $id
	 * @throws ApiException
	 * @throws InvalidLinkException
	 */
	public function actionCreateHost(int $id): void
	{
		$this->tripGetRequest->setTripId($id);
		$this->trip = $this->tripGetRequest->execute();

		$this['createTripHostForm']->setTripId($id);

		$this->template->trip = $this->trip;
		$this->template->step = 2;
		$this->template->prev = $this->link(':Trips:Front:TripCreate:create-purpose', ['id' => $id, 'prev' => true]);
	}


	/**
	 * @param int $id
	 * @throws ApiException
	 * @throws InvalidLinkException
	 */
	public function renderCreateRegistration(int $id): void
	{
		$this->registrationsGetRequest->setTripId($id);
		$registrations = $this->registrationsGetRequest->execute()->getRegistrations();

		$this->tripRegistrationGetRequest->setTripId($id);
		$trip = $this->tripRegistrationGetRequest->execute();

		$tripRegistration = [];

		foreach ($trip->getRegistration()->getRegistrations() as $registration) {
			$tripRegistration[] = $registration->getId();
		}

		$this->template->registrations = $registrations;
		$this->template->tripRegistration = $tripRegistration;
		$this->template->trip = $trip;
		$this->template->step = 3;
		$this->template->prev = $this->link(':Trips:Front:TripCreate:create-host', ['id' => $id, 'prev' => true]);
	}


	/**
	 * @param int $id
	 * @param bool|null $prev
	 * @param int|null $groupId
	 * @throws AbortException
	 * @throws ApiException
	 * @throws InvalidLinkException
	 */
	public function actionDetails(int $id, ?bool $prev, ?int $groupId): void
	{
		$this->actionQuestions($id, $groupId, 3,
			$prev !== null && $prev === true,
			'detail',
			':Trips:Front:TripCreate:create-registration',
			':Trips:Front:TripCreate:summary',
			':Trips:Front:TripCreate:details');
	}


	/**
	 * Prepare questions action
	 * @param int $id Trip ID
	 * @param int|null $groupId Question group ID
	 * @param int $step
	 * @param bool $goPrev
	 * @param string $type Question group type
	 * @param string $prevAction Previous action
	 * @param string $nextAction Next action
	 * @param string $currentAction Current action
	 * @throws AbortException
	 * @throws ApiException
	 * @throws InvalidLinkException
	 */
	private function actionQuestions(
		int $id,
		?int $groupId,
		int $step,
		bool $goPrev,
		string $type = '',
		string $prevAction = '',
		string $nextAction = '',
		string $currentAction = ''
	): void
	{
		$this->group = $groupId;

		// Get trip information
		$this->tripGetRequest->setTripId($id);
		$this->trip = $this->tripGetRequest->execute();

		// Get groups information
		$this->groupsGetRequest->setTripId($id);
		$this->groupsGetRequest->setType($type);
		$groups = $this->groupsGetRequest->execute()->getGroups();
		$prev = null;
		$next = null;
		$current = null;
		$currentOrder = 0;

		// Determine next group
		foreach ($groups as $group) {
			if ($group->getId() > $groupId) {
				$next = $group;

				break;
			} elseif ($group->getId() === $groupId) {
				$currentOrder++;
				$current = $group;
			} else {
				$currentOrder++;
				$prev = $group;
			}
		}

		if ($groupId === null && $next !== null) {
			// Redirect to next group
			$this->redirectPermanent($currentAction, ['id' => $id, 'groupId' => $next->getId()]);

		} elseif ($groupId === null && $next === null) {

			// There are no purpose question groups
			$this->redirectPermanent($goPrev ? $prevAction : $nextAction, ['id' => $id]);

		} elseif ($current !== null) {

			$this['createTripQuestionsForm']->setTripId($id);
			$this['createTripQuestionsForm']->setGroup($current);
			$this['createTripQuestionsForm']->setCurrentGroup($currentOrder);
			$this['createTripQuestionsForm']->setTotalGroups(count($groups));

			if ($next !== null) {
				$this['createTripQuestionsForm']->onSuccess[] = function (int $tripId) use ($next, $currentAction) {
					$this->redirectPermanent($currentAction, ['id' => $tripId, 'groupId' => $next->getId()]);
				};
			} else {
				$this['createTripQuestionsForm']->onSuccess[] = function (int $tripId) use ($prevAction, $nextAction, $goPrev) {
					$this->redirectPermanent($goPrev ? $prevAction : $nextAction, ['id' => $tripId]);
				};
			}

			$this->template->trip = $this->trip;
			$this->template->step = $step;
			$this->template->group = $current;
			$this->template->prev = $prev === null
				? $this->link($prevAction, ['id' => $id, 'prev' => true])
				: $this->link($currentAction, ['id' => $id, 'prev' => true, 'groupId' => $prev->getId()]);
		} else {

			// Redirect directly to next step
			$this->redirectPermanent($goPrev ? $prevAction : $nextAction, ['id' => $id]);
		}
	}


	/**
	 * @param int $id
	 * @throws AbortException
	 * @throws ApiException
	 */
	public function actionSubmit(int $id): void
	{
		$this->tripGetRequest->setTripId($id);
		$trip = $this->tripGetRequest->execute();
		$modal = $trip->getSubmittedAt() === null
			? 'created'
			: 'updated';

		$this->tripSubmitPutRequest->setTripId($id);
		$this->tripSubmitPutRequest->execute();
		$this->flashMessage('frontend.trips.create-trip.form.success.submitted', 'success');
		$this->redirectPermanent(':Trips:Front:TripCreate:summary', ['id' => $id, 'modal' => $modal]);
	}


	/**
	 * @param int $id
	 * @param string|null $modal
	 * @throws ApiException
	 * @throws InvalidLinkException
	 */
	public function renderSummary(int $id, ?string $modal = null): void
	{
		$this->tripGetRequest->setTripId($id);
		$trip = $this->tripGetRequest->execute();

		$this->template->trip = $trip;
		$this->template->step = 4;
		$this->template->home = $this->countryHomeGetRequest->execute();
		$this->template->prev = $this->link(':Trips:Front:TripCreate:details', ['id' => $id, 'prev' => true]);
		$this->template->modal = $modal;
	}


	/**
	 * @param int $id
	 * @throws AbortException
	 * @throws ApiException
	 */
	public function handleDelete(int $id): void
	{
		$this->tripDeleteRequest->setId($id);
		$this->tripDeleteRequest->execute();

		if ($this->isAjax()) {
			$this->payload->forceRedirect = true;
			$this->redirect(':Front:Homepage:default');
		} else {
			$this->redirect(':Front:Homepage:default');
		}
	}


	/**
	 * @param int $id
	 * @throws AbortException
	 * @throws ApiException
	 */
	public function handleAddFavorite(int $id): void
	{
		$this->tripFavoritePutRequest->setTripId($id);
		$this->tripFavoritePutRequest->execute();

		if ($this->isAjax()) {
			$this->payload->forceRedirect = true;
			$this->redirect('this');
		} else {
			$this->redirect('this');
		}
	}


	/**
	 * @param int $id
	 * @throws AbortException
	 * @throws ApiException
	 */
	public function handleCancel(int $id): void
	{
		$this->tripCancelPutRequest->setId($id);
		$this->tripCancelPutRequest->execute();

		if ($this->isAjax()) {
			$this->payload->forceRedirect = true;
			$this->redirect('this');
		} else {
			$this->redirect('this');
		}
	}


	/**
	 * @param int|null $id Trip ID
	 * @param string|null $start
	 * @param string|null $end
	 * @throws AbortException
	 * @throws ApiException
	 */
	public function handleDisableDates(?int $id, ?string $start, ?string $end): void
	{
		if ($start !== null && $end !== null) {
			$start = date('Y-m-d', (int) substr($start, 0, -3));
			$end = date('Y-m-d', (int) substr($end, 0, -3));

			$this->tripsGetRequest->setLimit(42);
			$this->tripsGetRequest->setMaxStartDate($end);
			$this->tripsGetRequest->setMinEndDate($start);
			$trips = $this->tripsGetRequest->execute();

			$disabledDates = [];

			/** @var TripResponse $trip */
			foreach ($trips->getItems() as $trip) {
				for ($i = $trip->getStart(); $i <= $trip->getEnd(); $i->modify('+1 day')) {
					$date = $i->setTimezone(new DateTimeZone('UTC'))->setTime(0, 0)->format('U') . '000';

					if ($id !== $trip->getId() && !in_array($date, $disabledDates, true)) {
						$disabledDates[] = $date;
					}
				}
			}

			$this->sendJson($disabledDates);
		}
	}


	/**
	 * @param ICreateTripFormFactory $factory
	 * @return CreateTripForm
	 */
	public function createComponentCreateTripForm(ICreateTripFormFactory $factory): CreateTripForm
	{
		$control = $factory->create();

		$control->onSuccess[] = function (int $id) {
			$this->redirectPermanent(':Trips:Front:TripCreate:createPurpose', ['id' => $id]);
		};

		return $control;
	}


	/**
	 * @param ICreateTripHostFormFactory $factory
	 * @return CreateTripHostForm
	 * @throws AbortException
	 */
	public function createComponentCreateTripHostForm(ICreateTripHostFormFactory $factory): CreateTripHostForm
	{
		try {
			$this->fieldsGetRequest->setTripId($this->trip->getId());
			$fields = $this->fieldsGetRequest->execute()->getHostFields();

			$control = $factory->create();
			$control->setFields($fields);
			$control->setTrip($this->trip);

			$control->onSuccess[] = function (int $tripId) {
				$this->redirectPermanent(':Trips:Front:TripCreate:createRegistration', ['id' => $tripId]);
			};

			return $control;
		} catch (ApiException $e) {
			$this->flashMessage($this->translator->translate('frontend.' . $e->getMessage(), $e->getParams()), 'danger');
			$this->redirectPermanent(":Front:Homepage:default");
		}
	}


	/**
	 * @param ICreateTripQuestionsFormFactory $factory
	 * @return CreateTripQuestionsForm
	 * @throws AbortException
	 */
	public function createComponentCreateTripQuestionsForm(ICreateTripQuestionsFormFactory $factory): CreateTripQuestionsForm
	{
		try {
			if ($this->group === null) {
				throw new InvalidArgumentException;
			}

			$this->questionsGetRequest->setTripId($this->trip->getId());
			$this->questionsGetRequest->setGroupId($this->group);
			$response = $this->questionsGetRequest->execute();

			$control = $factory->create();

			$control->setQuestions($response->getQuestions());
			$control->setTrip($this->trip);
			$control->setTripId($this->trip->getId());

			return $control;
		} catch (ApiException $e) {
			$this->flashMessage($this->translator->translate('frontend.' . $e->getMessage(), $e->getParams()), 'danger');
			$this->redirectPermanent(":Front:Homepage:default");
		}
	}


	/**
	 * @return string
	 */
	protected function getLayoutName(): string
	{
		return 'layout-trip';
	}
}
