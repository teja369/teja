<?php

declare(strict_types=1);

namespace Cleevio\Trips\FrontModule\Presenters;

use Cleevio\ApiClient\Exception\ApiException;
use Cleevio\Fields\API\Requests\FieldsGetRequest;
use Cleevio\FrontModule\Presenters\DashboardPresenter;
use Cleevio\Hosts\API\Requests\HostsGetRequest;
use Cleevio\Translations\Components\ReadableDate\IReadableDateFactory;
use Cleevio\Translations\Components\ReadableDate\ReadableDate;
use Cleevio\Trips\API\Requests\TripCancelPutRequest;
use Cleevio\Trips\API\Requests\TripDeleteRequest;
use Cleevio\Trips\API\Requests\TripFavoriteDeleteRequest;
use Cleevio\Trips\API\Requests\TripFavoritePutRequest;
use Cleevio\Trips\API\Requests\TripsGetRequest;
use Cleevio\Trips\Components\TripsTable\ITripsTableFactory;
use Cleevio\Trips\Components\TripsTable\TripsTable;
use Cleevio\UI\ApiPaginationControl\ApiPaginationControl;
use Cleevio\UI\ApiPaginationControl\IApiPaginationControlFactory;
use DateTime;
use Nette\Application\AbortException;
use Nette\Application\BadRequestException;

class TripPresenter extends DashboardPresenter
{

	/**
	 * @var TripDeleteRequest
	 */
	protected $tripDeleteRequest;

	/**
	 * @var FieldsGetRequest
	 */
	protected $fieldsGetRequest;

	/**
	 * @var HostsGetRequest
	 */
	protected $hostsGetRequest;

	/**
	 * @var TripFavoritePutRequest
	 */
	private $tripFavoritePutRequest;

	/**
	 * @var TripFavoriteDeleteRequest
	 */
	private $tripFavoriteDeleteRequest;

	/**
	 * @var TripCancelPutRequest
	 */
	private $tripCancelPutRequest;


	/**
	 * TripPresenter constructor.
	 * @param TripsGetRequest $tripsGetRequest
	 * @param TripDeleteRequest $tripDeleteRequest
	 * @param FieldsGetRequest $fieldsGetRequest
	 * @param HostsGetRequest $hostsGetRequest
	 * @param TripFavoritePutRequest $tripFavoritePutRequest
	 * @param TripFavoriteDeleteRequest $tripFavoriteDeleteRequest
	 * @param TripCancelPutRequest $tripCancelPutRequest
	 */
	public function __construct(
		TripsGetRequest $tripsGetRequest,
		TripDeleteRequest $tripDeleteRequest,
		FieldsGetRequest $fieldsGetRequest,
		HostsGetRequest $hostsGetRequest,
		TripFavoritePutRequest $tripFavoritePutRequest,
		TripFavoriteDeleteRequest $tripFavoriteDeleteRequest,
		TripCancelPutRequest $tripCancelPutRequest
	)
	{
		parent::__construct($tripsGetRequest);

		$this->tripDeleteRequest = $tripDeleteRequest;
		$this->fieldsGetRequest = $fieldsGetRequest;
		$this->hostsGetRequest = $hostsGetRequest;
		$this->tripFavoritePutRequest = $tripFavoritePutRequest;
		$this->tripFavoriteDeleteRequest = $tripFavoriteDeleteRequest;
		$this->tripCancelPutRequest = $tripCancelPutRequest;
	}


	/**
	 * @throws BadRequestException
	 */
	public function actionDefault(): void
	{
		try {
			$favoritesRequest = clone $this->tripsGetRequest;
			$favoritesRequest->setFavorite(1);
			$favoritesRequest->setLimit(2);
			$favorites = $favoritesRequest->execute();
			$this->template->favorites = $favorites;

			$recentTripsRequest = clone $this->tripsGetRequest;
			$recentTripsRequest->setSubmitted();
			$recentTripsRequest->setLimit(2);
			$recentTripsRequest->setMaxEndDate((new DateTime)->modify('- 1 day')->format('Y-m-d'));
			$recentTrips = $recentTripsRequest->execute();
			$this->template->recentTrips = $recentTrips;

			if ($this->isAjax()) {
				$this->redrawControl('dashboard');
			}
		} catch (ApiException $e) {
			$this->error($e->getMessage(), $e->getCode());
		}
	}


	public function actionDrafts(): void
	{
		$tripsGetRequest = clone $this->tripsGetRequest;
		$tripsGetRequest->setTripState('draft');

		$this['tripsTable']->setTripGetRequest($tripsGetRequest);
		$this['tripsTable']->showFilter();

		if ($this->isAjax()) {
			$this['tripsTable']->redrawControl('tripsCount');
			$this['tripsTable']->redrawControl('trips');
		} else {
			$this['tripsTable']->clearFilters();
		}
	}


	public function actionPlannedTrips(): void
	{
		$tripsGetRequest = clone $this->tripsGetRequest;

		$tripsGetRequest->setSubmitted();
		$tripsGetRequest->setMinStartDate((new DateTime)->modify('+ 1 day')->format('Y-m-d'));
		$tripsGetRequest->setMinEndDate((new DateTime)->modify('+ 1 day')->format('Y-m-d'));

		$this['tripsTable']->setTripGetRequest($tripsGetRequest);
		$this['tripsTable']->showFilter();

		if ($this->isAjax()) {
			$this['tripsTable']->redrawControl('tripsCount');
			$this['tripsTable']->redrawControl('trips');
		} else {
			$this['tripsTable']->clearFilters();
		}
	}


	public function actionPastTrips(): void
	{
		$tripsGetRequest = clone $this->tripsGetRequest;

		$tripsGetRequest->setSubmitted();
		$tripsGetRequest->setMaxEndDate((new DateTime)->modify('- 1 day')->format('Y-m-d'));
		$tripsGetRequest->setMaxStartDate((new DateTime)->modify('- 1 day')->format('Y-m-d'));

		$this['tripsTable']->setTripGetRequest($tripsGetRequest);
		$this['tripsTable']->showFilter();

		if ($this->isAjax()) {
			$this['tripsTable']->redrawControl('tripsCount');
			$this['tripsTable']->redrawControl('trips');
		} else {
			$this['tripsTable']->clearFilters();
		}
	}


	/**
	 * @throws BadRequestException
	 */
	public function actionFavorites(): void
	{
		try {
			$tripsGetRequest = clone $this->tripsGetRequest;
			$tripsGetRequest->setFavorite(1);
			$tripsGetRequest->execute();

			$this['tripsTable']->setTripGetRequest($tripsGetRequest);

			if ($this->isAjax()) {
				$this['tripsTable']->redrawControl('tripsCount');
				$this['tripsTable']->redrawControl('trips');
			} else {
				$this['tripsTable']->clearFilters();
			}
		} catch (ApiException $e) {
			$this->error($e->getMessage(), $e->getCode());
		}
	}


	/**
	 * @param int $id
	 * @throws AbortException
	 * @throws BadRequestException
	 */
	public function handleDelete(int $id): void
	{
		try {
			$this->tripDeleteRequest->setId($id);
			$this->tripDeleteRequest->execute();

			if ($this->isAjax()) {
				$this->payload->forceRedirect = true;
				$this->redirect('this');
			} else {
				$this->redirect('this');
			}
		} catch (ApiException $e) {
			$this->error($e->getMessage(), $e->getCode());
		}
	}


	/**
	 * @param int $id
	 * @throws AbortException
	 * @throws BadRequestException
	 */
	public function handleCancel(int $id): void
	{
		try {
			$this->tripCancelPutRequest->setId($id);
			$this->tripCancelPutRequest->execute();

			if ($this->isAjax()) {
				$this->payload->forceRedirect = true;
				$this->redirect('this');
			} else {
				$this->redirect('this');
			}
		} catch (ApiException $e) {
			$this->error($e->getMessage(), $e->getCode());
		}
	}


	/**
	 * @param int $id
	 * @throws AbortException
	 * @throws BadRequestException
	 */
	public function handleAddFavorite(int $id): void
	{
		try {
			$this->tripFavoritePutRequest->setTripId($id);
			$this->tripFavoritePutRequest->execute();

			if ($this->isAjax()) {
				$this->payload->forceRedirect = true;
				$this->redirect('this');
			} else {
				$this->redirect('this');
			}
		} catch (ApiException $e) {
			$this->error($e->getMessage(), $e->getCode());
		}
	}


	/**
	 * @param int $id
	 * @throws AbortException
	 * @throws BadRequestException
	 */
	public function handleRemoveFavorite(int $id): void
	{
		try {
			$this->tripFavoriteDeleteRequest->setTripId($id);
			$this->tripFavoriteDeleteRequest->execute();

			if ($this->isAjax()) {
				$this->payload->forceRedirect = true;
				$this->redirect('this');
			} else {
				$this->redirect('this');
			}
		} catch (ApiException $e) {
			$this->error($e->getMessage(), $e->getCode());
		}
	}


	/**
	 * @param ITripsTableFactory $tripsTableFactory
	 * @param IReadableDateFactory $readableDateFactory
	 * @param IApiPaginationControlFactory $apiPaginationControlFactory
	 * @return TripsTable
	 */
	public function createComponentTripsTable(
		ITripsTableFactory $tripsTableFactory,
		IReadableDateFactory $readableDateFactory,
		IApiPaginationControlFactory $apiPaginationControlFactory
	): TripsTable
	{
		$control = $tripsTableFactory->create();

		$control->addComponent($readableDateFactory->create(), 'readableDate');
		$control->addComponent($apiPaginationControlFactory->create(), 'apiPagination');

		return $control;
	}


	/**
	 * @param IReadableDateFactory $factory
	 * @return ReadableDate
	 */
	public function createComponentReadableDate(IReadableDateFactory $factory): ReadableDate
	{
		return $factory->create();
	}


	/**
	 * @param IApiPaginationControlFactory $factory
	 * @return ApiPaginationControl
	 */
	public function createComponentApiPagination(IApiPaginationControlFactory $factory): ApiPaginationControl
	{
		return $factory->create();
	}
}
