<?php

declare(strict_types=1);

namespace Cleevio\Trips\Router;

use Nette;
use Nette\Application\Routers\Route;
use Nette\Application\Routers\RouteList;

class RouterFactory
{
	/**
	 * @return Nette\Application\IRouter
	 */
	public function createRouter()
	{
		$router = new RouteList;

		$router[] = $tripsModule = new RouteList('Trips');

		$tripsModule[] = new Route('trips/<presenter>/<action>[/<id>]', [
			'presenter' => 'Trip',
			'action' => 'default',
			'module' => 'Front',
		]);

		return $router;
	}

}
