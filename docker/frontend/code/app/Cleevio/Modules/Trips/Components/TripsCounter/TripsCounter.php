<?php

declare(strict_types=1);

namespace Cleevio\Trips\Components\TripsCounter;

use Cleevio\Trips\API\Responses\TripsResponse;
use Cleevio\UI\BaseControl;

class TripsCounter extends BaseControl
{

	/**
	 * @param TripsResponse $trips
	 */
	public function render(TripsResponse $trips): void
	{
		$this->template->trips = $trips->getCurrentItemsCount();
		$this->template->render(__DIR__ . '/templates/default.latte');
	}

}

interface ITripsCounterFactory
{

	/**
	 * @return TripsCounter
	 */
	function create();
}
