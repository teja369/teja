<?php

declare(strict_types=1);

namespace Cleevio\Trips\Components\TripsTable;

use Cleevio\API\Context;
use Cleevio\Trips\API\Requests\TripsGetRequest;
use Cleevio\Trips\API\Responses\TripsResponse;
use Cleevio\UI\BaseControl;
use Cleevio\UI\BaseForm;
use Cleevio\UI\DynamicSelect;
use Nette\Http\Session;
use Nette\Http\SessionSection;
use Nette\Localization\ITranslator;

/**
 * Trips table component with filters
 *
 * @package Cleevio\Trips\Components\TripsTable
 */
class TripsTable extends BaseControl
{

	private const FORM_DESKTOP = 'Desktop';
	private const FORM_MOBILE = 'Mobile';

	/**
	 * Request to searh trips
	 * @var TripsGetRequest|null
	 */
	private $tripsGetRequest;

	/**
	 * @var ITranslator
	 */
	private $translator;

	/**
	 * @var bool
	 */
	private $showFilter = false;

	/**
	 * @var Context
	 */
	private $context;

	/**
	 * @var SessionSection
	 */
	private $session;

	/**
	 * @param ITranslator $translator
	 * @param Context $context
	 * @param Session $session
	 */
	public function __construct(ITranslator $translator, Context $context, Session $session)
	{
		$this->translator = $translator;
		$this->context = $context;
		$this->session = $session->getSection('tripsTable');
	}

	/**
	 * Remove all filters
	 */
	public function clearFilters(): void
	{
		unset($this->session->values);
	}

	/**
	 * Filter results
	 *
	 * @param array $values
	 */
	private function filter(array $values): void
	{
		if($this->tripsGetRequest === null){
			throw new \InvalidArgumentException;
		}

		$country = array_key_exists('country', $values) && $values['country'] !== ''
			? $values['country']
			: null;

		$company = array_key_exists('company', $values) && $values['company'] !== ''
			? $values['company']
			: null;

		if (array_key_exists('sortBy', $values)) {
			switch ($values['sortBy']) {
				case 'oldest':
					$this->tripsGetRequest->setOrderBy('start');
					$this->tripsGetRequest->setOrderDirection('desc');

					break;
				case 'newest':
					$this->tripsGetRequest->setOrderBy('start');
					$this->tripsGetRequest->setOrderDirection('asc');

					break;
			}
		}

		$startDate = !array_key_exists('tripStart', $values) || $values['tripStart'] === ''
			? $this->tripsGetRequest->getMinStartDate()
			: $values['tripStart'];

		$this->tripsGetRequest->setMinStartDate($startDate !== null ? date('Y-m-d', strtotime($startDate)) : null);
		$this->tripsGetRequest->setMinEndDate($startDate !== null ? date('Y-m-d', strtotime($startDate)) : null);

		$endDate = !array_key_exists('tripEnd', $values) || $values['tripEnd'] === ''
			? $this->tripsGetRequest->getMaxEndDate()
			: $values['tripEnd'];

		$this->tripsGetRequest->setMaxStartDate($endDate !== null ? date('Y-m-d', strtotime($endDate)) : null);
		$this->tripsGetRequest->setMaxEndDate($endDate !== null ? date('Y-m-d', strtotime($endDate)) : null);

		$this->tripsGetRequest->setCountry($country);
		$this->tripsGetRequest->setHostId($company);
	}

	public function render(?TripsResponse $tripsResponse = null): void
	{
		$this['apiPagination']->setAjax(true);

		if ($tripsResponse === null) {
			// Handle filters
			if ($this->session->offsetExists('values')) {
				$this->filter((array) $this->session->values);
			}

			$this['apiPagination']->setRequest($this->tripsGetRequest);
		}

		$trips = $tripsResponse ?? $this['apiPagination']->getItems();

		$this->template->trips = $trips->getItems();
		$this->template->tripsCount = $trips->getCurrentItemsCount();

		if ($this->tripsGetRequest !== null) {
			$this->template->startDateMin = $this->tripsGetRequest->getMinStartDate();
			$this->template->startDateMax = $this->tripsGetRequest->getMaxStartDate();

			$this->template->endDateMin = $this->tripsGetRequest->getMinEndDate();
			$this->template->endDateMax = $this->tripsGetRequest->getMaxEndDate();
		}

		$this->template->showFilter = $this->showFilter;
		$this->template->localeContext = $this->context->getLocale();
		$this->template->loc = $this->context->getLocale() !== null
			? $this->context->getLocale()->getCode()
			: null;
		$this->template->render(__DIR__ . '/templates/default.latte');
	}

	/**
	 * @return BaseForm
	 */
	protected function createComponentFormMobile(): BaseForm
	{
		$form = $this->handleCreateForm(self::FORM_MOBILE);

		$form
			->addHidden('reset')
			->setHtmlAttribute('hidden');

		$form->onSuccess[] = function (BaseForm $form) {
			$this->handleFormSubmit($form);
		};

		return $form;
	}

	/**
	 * @return BaseForm
	 */
	protected function createComponentFormDesktop(): BaseForm
	{
		$form = $this->handleCreateForm(self::FORM_DESKTOP);

		$form->onSuccess[] = function (BaseForm $form) {
			$this->handleFormSubmit($form);
		};

		return $form;
	}


	private function handleCreateForm(string $type): BaseForm
	{
		$form = new BaseForm;

		$form->setTranslator($this->translator);

		$form->addSelect('sortBy' . $type, 'frontend.trips.trip.past-trips.filter.label.sort-by', [
			null => 'frontend.trips.trip.past-trips.filter.placeholder.sort-by',
			'newest' => 'frontend.trips.trip.past-trips.filter.option.sort-by.newest',
			'oldest' => 'frontend.trips.trip.past-trips.filter.option.sort-by.oldest',
		]);

		$form->addSelect('show' . $type, 'frontend.trips.trip.past-trips.filter.label.show', [
			'all' => 'frontend.trips.trip.past-trips.filter.option.show.all',
			/*'favorites' => 'frontend.trips.trip.past-trips.filter.option.show.favorites',*/ // will be replaced with more relevant options
		]);

		$form['country' . $type] = new DynamicSelect('frontend.trips.trip.past-trips.filter.label.country');

		$form['company' . $type] = new DynamicSelect('frontend.trips.trip.past-trips.filter.label.company');

		$form->addText('tripStart' . $type, 'frontend.trips.trip.past-trips.filter.label.trip-start');

		$form->addText('tripEnd' . $type, 'frontend.trips.trip.past-trips.filter.label.trip-end');

		$form->addHidden('type' . $type, $type);

		$form->addSubmit('send' . $type)->setHtmlAttribute('hidden');

		return $form;
	}


	/**
	 * @param BaseForm $form
	 */
	private function handleFormSubmit(BaseForm $form): void
	{
		$values = $form->getValues();
		$filters = [];

		foreach ($values as $key => $value) {
			$key = str_replace([self::FORM_MOBILE, self::FORM_DESKTOP], '', (string) $key);

			$filters[$key] = $value;
		}

		$this->session->values = $filters;

		$this['apiPagination']->setRequest($this->tripsGetRequest);

		if ($this->isAjax()) {
			$trips = $this['apiPagination']->getItems();

			$this->template->trips = $trips->getItems();
			$this->template->tripsCount = $trips->getCurrentItemsCount();

			$this->redrawControl('tripsCount');
			$this->redrawControl('trips');
		}
	}


	public function showFilter(): void
	{
		$this->showFilter = true;
	}


	public function setTripGetRequest(TripsGetRequest $request): void
	{
		$this->tripsGetRequest = $request;
	}

}

interface ITripsTableFactory
{

	/**
	 * @return TripsTable
	 */
	function create();
}
