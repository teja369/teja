<?php

declare(strict_types=1);

namespace Cleevio\Trips\Components\CreateTripForm;

use Cleevio\ApiClient\Exception\ApiException;
use Cleevio\ApiClient\Exception\BadRequestException;
use Cleevio\ApiClient\IContext;
use Cleevio\Countries\API\Requests\CountriesGetRequest;
use Cleevio\Countries\API\Requests\CountryHomeGetRequest;
use Cleevio\Countries\API\Responses\CountryResponse;
use Cleevio\Trips\API\Requests\TripPutRequest;
use Cleevio\Trips\API\Requests\TripsPostRequest;
use Cleevio\Trips\API\Responses\TripResponse;
use Cleevio\UI\BaseControl;
use Cleevio\UI\BaseForm;
use Nette\Localization\ITranslator;
use Nette\Utils\Html;

class CreateTripForm extends BaseControl
{

	/**
	 * @var array
	 */
	public $onSuccess = [];

	/**
	 * @var ITranslator
	 */
	private $translator;

	/**
	 * @var TripsPostRequest
	 */
	private $tripsPostRequest;

	/**
	 * @var array
	 */
	protected $countries = [];

	/**
	 * @var array
	 */
	private $home;

	/**
	 * @var string|null
	 */
	private $prefilledCountry;

	/**
	 * @var IContext
	 */
	private $context;

	/**
	 * @var TripResponse|null
	 */
	private $trip;

	/**
	 * @var bool
	 */
	private $isRepeat = false;

	/**
	 * @var TripPutRequest
	 */
	private $tripPutRequest;


	/**
	 * @param ITranslator           $translator
	 * @param IContext              $context
	 * @param TripsPostRequest      $tripsPostRequest
	 * @param TripPutRequest        $tripPutRequest
	 * @param CountriesGetRequest   $countriesGetRequest
	 * @param CountryHomeGetRequest $countryHomeGetRequest
	 * @throws ApiException
	 */
	public function __construct(
		ITranslator $translator,
		IContext $context,
		TripsPostRequest $tripsPostRequest,
		TripPutRequest $tripPutRequest,
		CountriesGetRequest $countriesGetRequest,
		CountryHomeGetRequest $countryHomeGetRequest
	)
	{
		$this->translator = $translator;
		$this->tripsPostRequest = $tripsPostRequest;
		$this->context = $context;
		$this->tripPutRequest = $tripPutRequest;
		$this->home = $this->fromResponse($countryHomeGetRequest->execute());

		$countriesGetRequest->setLimit(1000);

		$this->countries = $this->getDestinationOptions($countriesGetRequest);

	}


	/**
	 * Create country select option from response
	 * @param CountryResponse $response
	 * @return array
	 */
	private function fromResponse(CountryResponse $response): array
	{
		return [
			'key' => $response->getName(),
			'icon' => $response->getCode(),
			'isHome' => $response->isHome(),
		];
	}


	/**
	 * @param CountriesGetRequest $countriesGetRequest
	 * @return array
	 * @throws ApiException
	 */
	private function getDestinationOptions(CountriesGetRequest $countriesGetRequest): array
	{
		$options = [];

		foreach ($countriesGetRequest->execute()->getItems() as $country) {
			if (!$country->isHome()) {
				$option = Html::el('option')
					->setAttribute('data-icon', 'flag-' . $country->getCode())
					->setAttribute('value', $country->getCode())
					->setText($country->getName());

				$options[$country->getCode()] = $option;
			}
		}

		return $options;
	}


	public function render(): void
	{
		$this->template->home = [$this->home];
		$this->template->countries = $this->countries;
		$this->template->prefilledCountry = $this->prefilledCountry;
		$this->template->trip = $this->trip;
		$this->template->localeContext = $this->context->getLocale();
		$this->template->loc = $this->context->getLocale() !== null
			? $this->context->getLocale()->getCode()
			: null;
		$this->template->render(__DIR__ . '/templates/default.latte');
	}


	protected function createComponentForm(): BaseForm
	{
		$form = new BaseForm;

		$form->setTranslator($this->translator);

		$form->addHidden('id', $this->trip !== null ? (string) $this->trip->getId() : null);

		$form->addSelect('home', 'frontend.trips.create-trip.form.label.home', array_map(static function (array $key) {
			return $key['key'];
		}, [$this->home]))
			->setDisabled(true);

		$form->addSelect('country', 'frontend.trips.create-trip.form.label.destination')
			->setItems($this->countries)
			->setDefaultValue(
				$this->trip !== null && !$this->trip->getCountry()->isHome()
					? $this->trip->getCountry()->getCode()
					: $this->prefilledCountry
			)
			->setRequired(true)
			->addRule($form::NOT_EQUAL, 'frontend.trips.create-trip.form.validation.selected.destination', 0)
			->addRule($form::LENGTH, 'frontend.trips.create-trip.form.validation.length.destination', 2);

		$form->addText('start', 'frontend.trips.create-trip.form.label.start')
			->setDefaultValue(
				$this->trip !== null && !$this->isRepeat
					? $this->trip->getStart()->format('d-m-Y')
					: null
			)
			->setRequired('frontend.trips.create-trip.form.required.start');

		$form->addText('end', 'frontend.trips.create-trip.form.label.end')
			->setDefaultValue(
				$this->trip !== null && !$this->isRepeat
					? $this->trip->getEnd()->format('d-m-Y')
					: null
			)
			->setRequired('frontend.trips.create-trip.form.required.end');

		$form->addSubmit('send', 'frontend.trips.create-trip.form.submit');

		$form->onSuccess[] = function (BaseForm $form) {
			$values = $form->getValues();

			$start = date('Y-m-d', strtotime($values->start));
			$end = date('Y-m-d', strtotime($values->end));

			try {
				if ($this->trip === null) {

					$this->tripsPostRequest->setData([
						'start' => $start,
						'end' => $end,
						'country' => $values->country,
					]);

					$response = $this->tripsPostRequest->execute();
					$this->onSuccess($response->getId());

				} else {

					$this->tripPutRequest->setTripId($this->trip->getId());
					$this->tripPutRequest->setData([
						'start' => $start,
						'end' => $end,
						'country' => $values->country,
					]);

					$response = $this->tripPutRequest->execute();

					$this->onSuccess($response->getId());
				}
			} catch (BadRequestException $e) {
				$this->flashMessage($this->translator->translate('frontend.' . $e->getMessage(), $e->getParams()), 'danger');
			} catch (ApiException $e) {
				$this->flashMessage($e->getMessage(), 'danger');
			}

			if ($this->isAjax()) {
				$this->redrawControl('flashes');
			}
		};

		return $form;
	}


	public function setTrip(?TripResponse $trip): void
	{
		$this->trip = $trip;
	}


	public function setRepeat(): void
	{
		$this->isRepeat = true;
	}


	/**
	 * @param string|null $country
	 */
	public function setPrefilledCountry(?string $country): void
	{
		$this->prefilledCountry = $country;
	}

}

interface ICreateTripFormFactory
{

	/**
	 * @return CreateTripForm
	 */
	function create();
}
