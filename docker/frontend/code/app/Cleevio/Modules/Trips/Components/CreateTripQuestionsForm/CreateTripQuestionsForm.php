<?php

declare(strict_types=1);

namespace Cleevio\Trips\Components\CreateTripQuestionsForm;

use Cleevio\API\Context;
use Cleevio\ApiClient\Exception\ApiException;
use Cleevio\ApiClient\Exception\BadRequestException;
use Cleevio\Questions\API\Responses\GroupResponse;
use Cleevio\Questions\API\Responses\QuestionResponse;
use Cleevio\Questions\Factories\IQuestionBuilderFactory;
use Cleevio\Questions\Factories\IQuestionParserFactory;
use Cleevio\Trips\API\Requests\TripAnswersPutRequest;
use Cleevio\Trips\API\Responses\TripResponse;
use Cleevio\UI\BaseControl;
use Cleevio\UI\BaseForm;
use Nette\Localization\ITranslator;

class CreateTripQuestionsForm extends BaseControl
{

	private const DROPDOWN_THRESHOLD_LOW = 10;

	private const DROPDOWN_THRESHOLD_HIGH = 100;

	/**
	 * @var array
	 */
	public $onSuccess = [];

	/**
	 * @var ITranslator
	 */
	private $translator;

	/**
	 * @var QuestionResponse[]
	 */
	private $questions;

	/**
	 * @var IQuestionBuilderFactory
	 */
	private $questionBuilderFactory;

	/**
	 * @var TripAnswersPutRequest
	 */
	private $tripAnswersPutRequest;

	/**
	 * @var IQuestionParserFactory
	 */
	private $questionParserFactory;

	/**
	 * @var TripResponse
	 */
	private $trip;

	/**
	 * @var int
	 */
	private $tripId;

	/**
	 * @var GroupResponse
	 */
	private $group;

	/**
	 * @var int
	 */
	private $currentGroup = 0;

	/**
	 * @var int
	 */
	private $totalGroups = 0;

	/**
	 * @var Context
	 */
	private $apiContext;


	/**
	 * @param ITranslator $translator
	 * @param IQuestionBuilderFactory $questionBuilderFactory
	 * @param TripAnswersPutRequest $tripAnswersPutRequest
	 * @param IQuestionParserFactory $questionParserFactory
	 * @param Context $apiContext
	 */
	public function __construct(
		ITranslator $translator,
		IQuestionBuilderFactory $questionBuilderFactory,
		TripAnswersPutRequest $tripAnswersPutRequest,
		IQuestionParserFactory $questionParserFactory,
		Context $apiContext
	)
	{
		$this->translator = $translator;
		$this->questionBuilderFactory = $questionBuilderFactory;
		$this->tripAnswersPutRequest = $tripAnswersPutRequest;
		$this->questionParserFactory = $questionParserFactory;
		$this->apiContext = $apiContext;
	}


	public function render(): void
	{
		$this->template->trip = $this->trip;
		$this->template->dropDownMin = self::DROPDOWN_THRESHOLD_LOW;
		$this->template->dropDownMax = self::DROPDOWN_THRESHOLD_HIGH;
		$this->template->currentGroup = $this->currentGroup;
		$this->template->totalGroups = $this->totalGroups;
		$this->template->group = $this->group;
		$this->template->questions = $this->questions;
		$this->template->localeContext = $this->apiContext->getLocale();
		$this->template->render(__DIR__ . '/templates/default.latte');
	}


	protected function createComponentForm(): BaseForm
	{
		$form = new BaseForm;

		$form->setTranslator($this->translator);

		$defaults = [];

		if ($this->trip->getAnswers() !== null) {
			foreach ($this->trip->getAnswers()->getAnswers() as $answer) {
				$defaults[$answer->getQuestion()->getId()] = $answer;
			}
		}

		// Build tree
		$grouped = [];
		$maxDepth = 0;

		while (count($grouped) !== count($this->questions)) {
			foreach ($this->questions as $question) {
				if (array_key_exists($question->getId(), $grouped)) {
					continue;
				}

				if ($question->getParent() !== null && !array_key_exists($question->getParent()->getId(), $grouped)) {
					continue;
				}

				$grouped[$question->getId()] = $question;
			}

			$maxDepth++;

			if ($maxDepth > 100) {
				break;
			}
		}

		foreach ($grouped as $question) {
			$this->questionBuilderFactory->getBuilder($question->getType())
				->build($form, $question, $defaults[$question->getId()] ?? null);
		}

		$form->addSubmit('send', 'frontend.trips.create-trip.purpose.form.submit');

		$form->onError[] = function (BaseForm $form) {
			foreach ($form->getErrors() as $error) {
				$this->flashMessage($error, 'danger');
			}
		};

		$form->onSuccess[] = function (BaseForm $form) {

			$data = array_map(function (QuestionResponse $question) use ($form) {
				return $this->questionParserFactory->getBuilder($question->getType())
					->parse($form, $question);
			}, $this->questions);

			try {
				$this->tripAnswersPutRequest->setTripId($this->trip->getId());
				$this->tripAnswersPutRequest->setData($data);
				$this->tripAnswersPutRequest->execute();

				$this->onSuccess($this->tripId);

			} catch (BadRequestException $e) {
				$this->flashMessage($this->translator->translate('frontend.' . $e->getMessage(), $e->getParams()), 'danger');
			} catch (ApiException $e) {
				$this->flashMessage('frontend.trips.create-trip.form.error.unknown', 'danger');
			}
		};

		return $form;
	}


	/**
	 * @param QuestionResponse[] $questions
	 */
	public function setQuestions(array $questions): void
	{
		$this->questions = $questions;
	}


	/**
	 * @param TripResponse $trip
	 */
	public function setTrip(TripResponse $trip): void
	{
		$this->trip = $trip;
	}


	/**
	 * @param int $tripId
	 */
	public function setTripId(int $tripId): void
	{
		$this->tripId = $tripId;
	}


	/**
	 * @param GroupResponse $group
	 */
	public function setGroup(GroupResponse $group): void
	{
		$this->group = $group;
	}


	/**
	 * @param int $totalGroups
	 */
	public function setTotalGroups(int $totalGroups): void
	{
		$this->totalGroups = $totalGroups;
	}


	/**
	 * @param int $currentGroup
	 */
	public function setCurrentGroup(int $currentGroup): void
	{
		$this->currentGroup = $currentGroup;
	}
}

interface ICreateTripQuestionsFormFactory
{

	/**
	 * @return CreateTripQuestionsForm
	 */
	function create();
}
