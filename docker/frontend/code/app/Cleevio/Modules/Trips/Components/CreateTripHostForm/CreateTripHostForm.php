<?php

declare(strict_types=1);

namespace Cleevio\Trips\Components\CreateTripHostForm;

use Cleevio\ApiClient\Exception\ApiException;
use Cleevio\ApiClient\Exception\BadRequestException;
use Cleevio\Fields\API\Responses\FieldResponse;
use Cleevio\Hosts\API\Requests\HostPutRequest;
use Cleevio\Hosts\API\Requests\HostsPostRequest;
use Cleevio\Hosts\Factories\IHostFieldBuilderFactory;
use Cleevio\Trips\API\Requests\TripHostDeleteRequest;
use Cleevio\Trips\API\Requests\TripHostPutRequest;
use Cleevio\Trips\API\Responses\TripResponse;
use Cleevio\UI\BaseControl;
use Cleevio\UI\BaseForm;
use Nette\Localization\ITranslator;

class CreateTripHostForm extends BaseControl
{

	private const HOST_ENTITY_TYPE_CLIENT = 'client';

	private const HOST_ENTITY_TYPE_NON_CLIENT = 'non-client';

	private const HOST_ENTITY_TYPE_NONE = 'none';

	private const MAX_LENGTH = 100;

	/**
	 * @var array
	 */
	public $onSuccess = [];

	/**
	 * @var ITranslator
	 */
	private $translator;

	/**
	 * @var TripResponse
	 */
	private $trip;

	/**
	 * @var FieldResponse[]
	 */
	private $fields;

	/**
	 * @var TripHostPutRequest
	 */
	private $tripHostPutRequest;

	/**
	 * @var HostsPostRequest
	 */
	private $hostsPostRequest;

	/**
	 * @var HostPutRequest
	 */
	private $hostPutRequest;

	/**
	 * @var int
	 */
	private $tripId;

	/**
	 * @var IHostFieldBuilderFactory
	 */
	private $hostFieldBuilderFactory;

	/**
	 * @var TripHostDeleteRequest
	 */
	private $tripHostDeleteRequest;


	public function __construct(
		ITranslator $translator,
		TripHostPutRequest $tripHostPutRequest,
		TripHostDeleteRequest $tripHostDeleteRequest,
		HostsPostRequest $hostsPostRequest,
		HostPutRequest $hostPutRequest,
		IHostFieldBuilderFactory $hostFieldBuilderFactory
	)
	{
		$this->translator = $translator;
		$this->tripHostPutRequest = $tripHostPutRequest;
		$this->hostsPostRequest = $hostsPostRequest;
		$this->hostPutRequest = $hostPutRequest;
		$this->hostFieldBuilderFactory = $hostFieldBuilderFactory;
		$this->tripHostDeleteRequest = $tripHostDeleteRequest;
	}


	public function render(): void
	{
		$this->template->fields = $this->fields;
		$this->template->trip = $this->trip;
		$this->template->defaultClient = $this->trip->getHost() ?? null;
		$this->template->render(__DIR__ . '/templates/default.latte');
	}


	protected function createComponentForm(): BaseForm
	{
		$form = new BaseForm;

		$form->setTranslator($this->translator);

		$defaultHost = null;

		if ($this->trip->getHost() !== null) {
			$defaultHost = in_array($this->trip->getHost()->getType(), [self::HOST_ENTITY_TYPE_NON_CLIENT, self::HOST_ENTITY_TYPE_CLIENT], true)
				? self::HOST_ENTITY_TYPE_CLIENT
				: self::HOST_ENTITY_TYPE_NONE;
		} elseif ($this->trip->getHostSet() !== null) {
			$defaultHost = 'none';
		}

		$form->addRadioList('toggler', 'toggler', [
			'client' => self::HOST_ENTITY_TYPE_CLIENT,
			'non-client' => self::HOST_ENTITY_TYPE_NON_CLIENT,
			'none' => self::HOST_ENTITY_TYPE_NONE])
			->setDefaultValue($this->trip->getHost() !== null ? $this->trip->getHost()->getType() : null)
			->addConditionOn($form['toggler'], $form::EQUAL, self::HOST_ENTITY_TYPE_NON_CLIENT)
			->toggle("non-client")
			->addRule($form::REQUIRED, 'name required')
			->elseCondition()
			->addConditionOn($form['toggler'], $form::EQUAL, self::HOST_ENTITY_TYPE_CLIENT)
			->toggle('clients')
			->endCondition();

		$form->addRadioList('hostType', 'frontend.trips.create-trip.host.form.label.host-type', [
			'client' => $this->translator->translate('frontend.trips.create-trip.host.form.option.client'),
			'none' => $this->translator->translate('frontend.trips.create-trip.host.form.option.none')])
			->setDefaultValue($defaultHost)
			->addRule($form::REQUIRED, $this->translator->translate('frontend.trips.create-trip.host.form.validation.host-type.required'))
			->addConditionOn($form['hostType'], $form::EQUAL, 'client')
			->toggle('client-non-client')
			->endCondition();


		$form->addSelect('client', 'frontend.trips.create-trip.host.form.label.clients')/*->addRule($form::REQUIRED, 'client required')*/
		;

		$form->addText('name', 'frontend.trips.create-trip.host.form.label.non-client.name')
			->setDefaultValue(
				$this->trip->getHost() !== null && $this->trip->getHost()->getType() === self::HOST_ENTITY_TYPE_NON_CLIENT
					? $this->trip->getHost()->getName()
					: null)
			->addCondition($form::FILLED)
			->addRule($form::MAX_LENGTH,
				$this->translator->translate('frontend.questions.validation.maxLength', [
					'name' => 'frontend.trips.create-trip.host.form.label.non-client.name',
					'count' => self::MAX_LENGTH,
				])
				, self::MAX_LENGTH);

		$defaults = [];

		if ($this->trip->getHost() !== null && $this->trip->getHost()->getParams() !== null) {
			foreach ($this->trip->getHost()->getParams() as $param) {
				$defaults[$param->getField()->getId()] = $param->getValue();
			}
		}

		foreach ($this->fields as $input) {
			$this->hostFieldBuilderFactory->getBuilder($input->getType())
				->build($form, $this->trip->getHost(), $input,
					array_key_exists($input->getId(), $defaults) ? $defaults[$input->getId()] : null);
		}

		$form->addSubmit('send', 'frontend.trips.create-trip.host.form.submit');

		$form->onSuccess[] = function (BaseForm $form) {
			$values = $form->getValues();
			$clients = array_key_exists('client', $form->getHttpData())
				? (int) $form->getHttpData()['client']
				: null;

			try {
				if ($values->hostType !== self::HOST_ENTITY_TYPE_NONE &&
					$values->toggler === self::HOST_ENTITY_TYPE_CLIENT &&
					$clients !== null) {
					$this->tripHostPutRequest->setTripId($this->tripId);
					$this->tripHostPutRequest->setHostId($clients);
					$this->tripHostPutRequest->execute();

					$this->onSuccess($this->tripId);
				} elseif ($values->hostType !== self::HOST_ENTITY_TYPE_NONE &&
					$values->toggler === self::HOST_ENTITY_TYPE_NON_CLIENT) {
					$data = [
						'name' => $values->name,
						'country' => $this->trip->getCountry()->getCode(),
						'params' => array_map(static function (FieldResponse $field) use ($values) {
							return ['id' => $field->getId(), 'value' => $values->offsetGet($field->getId())];
						}, $this->fields),
					];

					if ($this->trip->getHost() !== null && $this->trip->getHost()
							->getType() === self::HOST_ENTITY_TYPE_NON_CLIENT) {
						$this->hostPutRequest->setHostId($this->trip->getHost()->getId());
						$this->hostPutRequest->setData($data);
						$response = $this->hostPutRequest->execute();
					} else {
						$this->hostsPostRequest->setData($data);
						$response = $this->hostsPostRequest->execute();
					}

					$this->tripHostPutRequest->setTripId($this->tripId);
					$this->tripHostPutRequest->setHostId($response->getId());
					$this->tripHostPutRequest->execute();

					$this->onSuccess($this->tripId);
				} elseif ($values->hostType === self::HOST_ENTITY_TYPE_NONE) {
					$this->tripHostDeleteRequest->setTripId($this->tripId);
					$this->tripHostDeleteRequest->execute();

					$this->onSuccess($this->tripId);
				} else {
					$this->flashMessage('frontend.trips.create-trip.host.form.validation.host-type.required', 'danger');
				}
			} catch (BadRequestException $e) {
				$this->flashMessage($this->translator->translate('frontend.' . $e->getMessage(), $e->getParams()), 'danger');
			} catch (ApiException $e) {
				$this->flashMessage('frontend.trips.create-trip.form.error.unknown', 'danger');
			}

			if ($this->isAjax()) {
				$this->redrawControl('flashes');
			}
		};

		return $form;
	}


	/**
	 * @param TripResponse $trip
	 */
	public function setTrip(TripResponse $trip): void
	{
		$this->trip = $trip;
	}


	/**
	 * @param FieldResponse[] $fields
	 */
	public function setFields(array $fields): void
	{
		$this->fields = $fields;
	}


	/**
	 * @param int $tripId
	 */
	public function setTripId(int $tripId): void
	{
		$this->tripId = $tripId;
	}

}

interface ICreateTripHostFormFactory
{

	/**
	 * @return CreateTripHostForm
	 */
	function create();
}
