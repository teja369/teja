<?php

declare(strict_types=1);

namespace Cleevio\Trips\Components\TripsSidebar;

use Cleevio\Trips\API\Responses\TripsResponse;
use Cleevio\UI\BaseControl;

class TripsSidebar extends BaseControl
{

	public function render(TripsResponse $trips): void
	{
		$this->template->trips = $trips->getItems();
		$this->template->render(__DIR__ . '/templates/default.latte');
	}

}

interface ITripsSidebarFactory
{

	/**
	 * @return TripsSidebar
	 */
	function create();
}
