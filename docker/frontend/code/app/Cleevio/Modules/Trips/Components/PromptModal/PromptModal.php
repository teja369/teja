<?php

declare(strict_types=1);

namespace Cleevio\Trips\Components\PromptModal;

use Cleevio\UI\BaseControlModal;

/**
 * Class PromptModal
 * @package Cleevio\Trips\Components\DeleteModal
 */
class PromptModal extends BaseControlModal
{

	public function __construct()
	{
		parent::__construct();
		$this->setView('delete');
		$this->setTemplateDirectory(__DIR__ . '/templates');
	}


	public function handleShowModal(int $id, string $type): void
	{
		$this->template->id = $id;
		$this->setView($type);
	}
}

interface IPromptModal
{

	/**
	 * @return PromptModal
	 */
	function create();
}
