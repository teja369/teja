<?php

declare(strict_types=1);

namespace Cleevio\Translations\DI;

use Nette\DI\CompilerExtension;

class TranslationsExtension extends CompilerExtension
{

	public function loadConfiguration()
	{
		if (defined('NETTE_TESTER')) {
			$this->compiler->loadConfig(__DIR__ . '/services.test.neon');
		} else {
			$this->compiler->loadConfig(__DIR__ . '/services.neon');
		}
	}

}
