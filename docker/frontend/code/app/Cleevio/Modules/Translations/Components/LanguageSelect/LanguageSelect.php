<?php

declare(strict_types=1);

namespace Cleevio\Translations\Components\LanguageSelect;

use Cleevio\API\Context;
use Cleevio\ApiClient\DTO\Locale;
use Cleevio\ApiClient\Exception\ApiException;
use Cleevio\ApiClient\Exception\NotModifiedException;
use Cleevio\Translations\API\Requests\LanguagesGetRequest;
use Cleevio\Translations\API\Requests\TranslationGetRequest;
use Cleevio\Translations\API\Responses\LanguageResponse;
use Cleevio\UI\BaseControl;
use Cleevio\Users\Api\Requests\UserLanguagePutRequest;
use Contributte\Translation\LocalesResolvers\Session as SessionResolver;
use DateTime;
use Nette\Application\AbortException;
use Nette\Application\BadRequestException;
use Nette\Http\Request;
use Nette\Utils\Finder;

/**
 * @method onSuccess()
 */
class LanguageSelect extends BaseControl
{

	/**
	 * @var LanguageResponse[]
	 */
	private static $locales = [];

	/**
	 * @var array
	 */
	public $onSuccess = [];

	/**
	 * @var Context
	 */
	private $apiContext;

	/**
	 * @var Locale|null
	 */
	private $locale;

	/**
	 * @var LanguagesGetRequest
	 */
	private $languageRequest;

	/**
	 * @var Request
	 */
	private $request;

	/**
	 * @var SessionResolver
	 */
	private $translatorSessionResolver;

	/**
	 * @var UserLanguagePutRequest
	 */
	private $userLanguagePutRequest;

	/**
	 * @var TranslationGetRequest
	 */
	private $translationGetRequest;

	/**
	 * LanguageSelect constructor.
	 * @param Context $context
	 * @param LanguagesGetRequest $languagesRequest
	 * @param UserLanguagePutRequest $userLanguagePutRequest
	 * @param TranslationGetRequest $translationGetRequest
	 * @param Request $request
	 * @param SessionResolver $translatorSessionResolver
	 */
	public function __construct(
		Context $context,
		LanguagesGetRequest $languagesRequest,
		UserLanguagePutRequest $userLanguagePutRequest,
		TranslationGetRequest $translationGetRequest,
		Request $request,
		SessionResolver $translatorSessionResolver
	)
	{
		$this->request = $request;
		$this->apiContext = $context;
		$this->languageRequest = $languagesRequest;
		$this->translatorSessionResolver = $translatorSessionResolver;
		$this->userLanguagePutRequest = $userLanguagePutRequest;
		$this->translationGetRequest = $translationGetRequest;
	}


	public function render(string $type): void
	{
		$this->template->locales = self::$locales;
		$this->template->loc = $this->apiContext->getLocale();
		$this->template->type = $type;
		$this->template->render(__DIR__ . '/templates/default.latte');
	}


	/**
	 * Set language context
	 * Load support languages from API if necessary
	 *
	 * @throws BadRequestException
	 * @throws AbortException
	 */
	public function handleLang(): void
	{
		try {

			$languages = $this->languageRequest->execute()->getLanguages();

			$supported = array_map(static function (LanguageResponse $language) {
				return $language->getCode();
			}, $languages);

			$detected = $this->apiContext->getLocale() === null
				? $this->request->detectLanguage($supported)
				: $this->apiContext->getLocale()->getCode();

			$default = null;
			$locale = null;

			foreach ($languages as $language) {
				if ($language->isDefault()) {
					$default = $language;
				}

				if ($language->getCode() === $detected) {
					$locale = $language;
				}
			}

			if ($locale !== null) {
				$this->apiContext->setLocale($locale);
				$this->translatorSessionResolver->setLocale($locale->getCode());
			} elseif ($default !== null) {
				$this->apiContext->setLocale($default);
				$this->translatorSessionResolver->setLocale($default->getCode());
			}

			$this->locale = $this->apiContext->getLocale();
			self::$locales = $languages;

			$this->loadTranslations();

		} catch (ApiException $e) {
			throw new BadRequestException($e->getMessage(), $e->getCode());
		}
	}

	/**
	 * @throws ApiException
	 * @throws AbortException
	 */
	private function loadTranslations(): void
	{
		try {

			$request = clone $this->translationGetRequest;

			if ($this->locale !== null && defined('CACHE_DIR') && !defined('NETTE_TESTER')) {

				$fileName = CACHE_DIR . "/cache/lang/frontend." . $this->locale->getCode() . ".neon";

				$old = file_exists($fileName)
					? file_get_contents($fileName)
					: null;

				$request->setCode($this->locale->getCode());

				if ($old !== null) {
					$fileModified = filemtime($fileName);

					if($fileModified !== false) {
						$since = new DateTime("@". $fileModified);
						$request->setLastModifiedSince($since);
					}
				}

				$translations = $request->execute()->getTranslations();

				if (count($translations) > 0) {

					$contents = '';
					$i = 1;

					foreach ($translations as $translation) {
						$contents .= sprintf("%s='%s'", $translation->getKey(), $translation->getValue());
						$contents .= $i < count($translations)
							? "\n"
							: '';
						$i++;
					}

					foreach (Finder::findFiles('*nette.*/*.*', '*translation*/*.*', '*lang*/' . $this->locale->getCode() . '.neon')->from(CACHE_DIR . "/cache") as $file) {
						unlink($file->getPathname());
					}

					file_put_contents($fileName, $contents);
					$this->redirect('this');
				}
			}
		} catch (NotModifiedException $e) {
			// No need to translate
		}
	}

	/**
	 * @param string $lang
	 * @throws BadRequestException
	 */
	public function handleLangChange(string $lang): void
	{
		try {
			$languages = $this->languageRequest->execute()->getLanguages();
			$supported = array_map(static function (LanguageResponse $language) {
				return $language->getCode();
			}, $languages);

			if (!in_array($lang, $supported, true)) {
				throw new BadRequestException;
			}

			foreach ($languages as $language) {
				if ($language->getCode() === $lang) {

					$this->userLanguagePutRequest->setCode($lang);
					$this->userLanguagePutRequest->execute();

					$this->apiContext->setLocale($language);
					$this->translatorSessionResolver->setLocale($lang);
				}
			}

			$this->onSuccess();
		} catch (ApiException $e) {
			throw new BadRequestException;
		}
	}

}

interface ILanguageSelectFactory
{

	/**
	 * @return LanguageSelect
	 */
	function create();
}
