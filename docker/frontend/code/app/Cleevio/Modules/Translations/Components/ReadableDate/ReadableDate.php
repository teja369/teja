<?php

declare(strict_types=1);

namespace Cleevio\Translations\Components\ReadableDate;

use Cleevio\UI\BaseControl;
use DateTime;
use Exception;

class ReadableDate extends BaseControl
{

	private const YEAR = 'frontend.dates.readable.year';

	private const MONTH = 'frontend.dates.readable.month';

	private const DAY = 'frontend.dates.readable.day';

	private const HOUR = 'frontend.dates.readable.hour';

	private const MINUTE = 'frontend.dates.readable.minute';

	private const LESS_THAN_MINUTE = 'frontend.dates.readable.less-than-minute';

	/**
	 * @var string
	 */
	private $text;

	/**
	 * @var int|null
	 */
	private $value;


	/**
	 * @param DateTime $datetime
	 * @throws Exception
	 */
	public function render(DateTime $datetime): void
	{
		$this->handleGetReadableDate($datetime);

		$this->template->value = $this->value;
		$this->template->text = $this->text;
		$this->template->render(__DIR__ . '/templates/default.latte');
	}


	/**
	 * @param DateTime $datetime
	 * @throws Exception
	 */
	public function handleGetReadableDate(DateTime $datetime): void
	{
		$today = new DateTime;
		$interval = $today->diff($datetime);

		if ($interval->y > 0) {
			$this->value = $interval->y;
			$this->text = self::YEAR;
		} elseif ($interval->m > 0) {
			$this->value = $interval->m;
			$this->text = self::MONTH;
		} elseif ($interval->d > 0) {
			$this->value = $interval->d;
			$this->text = self::DAY;
		} elseif ($interval->h > 0) {
			$this->value = $interval->h;
			$this->text = self::HOUR;
		} elseif ($interval->i > 0) {
			$this->value = $interval->i;
			$this->text = self::MINUTE;
		} else {
			$this->text = self::LESS_THAN_MINUTE;
		}
	}

}

interface IReadableDateFactory
{

	/**
	 * @return ReadableDate
	 */
	function create();
}
