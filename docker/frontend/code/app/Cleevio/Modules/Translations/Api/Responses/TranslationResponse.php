<?php

declare(strict_types=1);

namespace Cleevio\Translations\API\Responses;

use Cleevio\ApiClient\IResponse;
use InvalidArgumentException;

class TranslationResponse implements IResponse
{

	/**
	 * @var string
	 */
	private $key;

	/**
	 * @var string
	 */
	private $value;

	/**
	 * TranslationResponse constructor.
	 * @param array $language
	 */
	public function __construct(array $language)
	{
		if (array_key_exists('key', $language) && array_key_exists('value', $language) ) {
			$this->key = $language['key'];
			$this->value = $language['value'];
		} else {
			throw new InvalidArgumentException;
		}
	}

	/**
	 * @return string
	 */
	public function getKey(): string
	{
		return $this->key;
	}

	/**
	 * @return string
	 */
	public function getValue(): string
	{
		return $this->value;
	}

}
