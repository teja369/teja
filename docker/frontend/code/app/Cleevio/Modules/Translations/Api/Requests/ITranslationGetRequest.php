<?php

declare(strict_types=1);

namespace Cleevio\Translations\API\Requests;

interface ITranslationGetRequest
{
	public function setCode(string $code): ITranslationGetRequest;
}
