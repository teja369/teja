<?php

declare(strict_types=1);

namespace Cleevio\Translations\API\Responses;

use Cleevio\ApiClient\IResponse;

class LanguagesResponse implements IResponse
{

	/**
	 * @var LanguageResponse[]
	 */
	private $languages = [];

	/**
	 * LanguagesResponse constructor.
	 * @param array $languages
	 */
	public function __construct(array $languages)
	{
		$this->languages = array_map(static function ($language) {
			return new LanguageResponse($language);
		}, $languages);
	}

	/**
	 * @return LanguageResponse[]
	 */
	public function getLanguages(): array
	{
		return $this->languages;
	}

}
