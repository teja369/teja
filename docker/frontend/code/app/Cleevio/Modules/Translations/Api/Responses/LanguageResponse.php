<?php

declare(strict_types=1);

namespace Cleevio\Translations\API\Responses;

use Cleevio\ApiClient\DTO\Locale;
use Cleevio\Countries\API\Responses\CountryResponse;
use InvalidArgumentException;

class LanguageResponse extends Locale
{

	/**
	 * @var string
	 */
	private $code;

	/**
	 * @var string
	 */
	private $name;
	/**
	 * @var bool
	 */
	private $isDefault;

	/**
	 * @var CountryResponse
	 */
	private $country;

	/**
	 * LanguagesResponse constructor.
	 * @param array $language
	 */
	public function __construct(array $language)
	{
		if (array_key_exists('code', $language) &&
			array_key_exists('name', $language) &&
			array_key_exists('country', $language)) {
			$this->code = $language['code'];
			$this->name = $language['name'];
			$this->country = new CountryResponse($language['country']);
			$this->isDefault = (bool) $language['isDefault'];
		} else {
			throw new InvalidArgumentException;
		}
	}

	/**
	 * @return string
	 */
	public function getCode(): string
	{
		return $this->code;
	}

	/**
	 * @return string
	 */
	public function getName(): string
	{
		return $this->name;
	}

	/**
	 * @return bool
	 */
	public function isDefault(): bool
	{
		return $this->isDefault;
	}

	/**
	 * @return CountryResponse
	 */
	public function getCountry(): CountryResponse
	{
		return $this->country;
	}

	public function getDateFormat(): string
	{
		return 'dd-mm-yyyy';
	}

	public function getDateTimeFormat(): string
	{
		return 'dd-mm-yyy H:i:s';
	}
}
