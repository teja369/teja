<?php

declare(strict_types=1);

namespace Cleevio\Translations\API\Requests;

use Cleevio\API\BaseRequest;
use Cleevio\ApiClient\Exception\ApiException;
use Cleevio\Translations\API\Responses\TranslationsResponse;

class TranslationGetRequest extends BaseRequest implements ITranslationGetRequest
{

	/**
	 * @var string
	 */
	private $code = 'en';

	/**
	 * @param string $code
	 * @return TranslationGetRequest
	 */
	public function setCode(string $code): ITranslationGetRequest
	{
		$this->code = $code;

		return $this;
	}

	/**
	 * Endpoint path
	 * @return string
	 */
	protected function path(): string
	{
		return '/translations/' . $this->code;
	}

	/**
	 * Method
	 * @return string
	 */
	protected function method(): string
	{
		return 'GET';
	}

	/**
	 * Determines whether request requires authorization
	 * @return bool
	 */
	function requiresAuth(): bool
	{
		return false;
	}

	/**
	 * Execute request and return a response
	 * @return TranslationsResponse
	 * @throws ApiException
	 */
	function execute(): TranslationsResponse
	{
		return new TranslationsResponse($this->request());
	}
}
