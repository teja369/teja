<?php

declare(strict_types=1);

namespace Cleevio\Issues\Components\IssueFormModal;

use Cleevio\ApiClient\Exception\ApiException;
use Cleevio\ApiClient\Exception\BadRequestException;
use Cleevio\Issues\API\Requests\IssuePostRequest;
use Cleevio\UI\BaseControlModal;
use Cleevio\UI\BaseForm;
use Nette\Localization\ITranslator;

class IssueFormModal extends BaseControlModal
{

	/**
	 * @var array
	 */
	public $onSuccess = [];

	/**
	 * @var array
	 */
	public $onError = [];

	/**
	 * @var ITranslator
	 */
	private $translator;

	/**
	 * @var IssuePostRequest
	 */
	private $issuePostRequest;


	/**
	 * IssueFormModal constructor.
	 * @param ITranslator      $translator
	 * @param IssuePostRequest $issuePostRequest
	 */
	public function __construct(
		ITranslator $translator,
		IssuePostRequest $issuePostRequest
	)
	{
		parent::__construct();
		$this->translator = $translator;
		$this->issuePostRequest = $issuePostRequest;
		$this->setTemplateDirectory(__DIR__ . '/templates');
	}


	protected function createComponentForm(): BaseForm
	{
		$form = new BaseForm;
		$form->setTranslator($this->translator);

		$form->addText('title', 'frontend.issues.modal.input.title.label')
			->setHtmlAttribute('placeholder', 'frontend.issues.modal.input.title.placeholder')
			->setRequired('frontend.issues.modal.input.title.required');

		$form->addTextArea('message', 'frontend.issues.modal.input.message.label')
			->setHtmlAttribute('placeholder', 'frontend.issues.modal.input.message.placeholder')
			->setRequired('frontend.issues.modal.input.message.required');

		$form->addSubmit('send', 'frontend.issues.modal.input.send.label');

		$form->onSuccess[] = function (BaseForm $form) {
			$values = $form->getValues();

			try {
				$this->issuePostRequest->setData([
					'title' => $values->title,
					'message' => $values->message,
				]);
				$this->issuePostRequest->execute();

				$this->onSuccess();

			} catch (BadRequestException $e) {
				$this->flashMessage($this->translator->translate('frontend.' . $e->getMessage(), $e->getParams()), 'danger');
			} catch (ApiException $e) {
				$this->flashMessage('frontend.issues.modal.form.error.unknown', 'danger');
			}

			if ($this->isAjax()) {
				$this->redrawControl('flashes');
			}
		};

		return $form;
	}


	public function handleShowModal(): void
	{
		$this->setView('default');
	}
}

interface IIssueFormModalFactory
{

	/**
	 * @return IssueFormModal
	 */
	function create();
}
