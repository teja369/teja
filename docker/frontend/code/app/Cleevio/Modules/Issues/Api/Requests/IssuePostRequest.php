<?php

declare(strict_types=1);

namespace Cleevio\Issues\API\Requests;

use Cleevio\API\BaseRequest;
use Cleevio\ApiClient\DTO\EmptyResponse;
use Cleevio\ApiClient\Exception\ApiException;

class IssuePostRequest extends BaseRequest implements IIssuePostRequest
{

	/**
	 * Endpoint path
	 * @return string
	 */
	protected function path(): string
	{
		return '/issues';
	}


	/**
	 * Method
	 * @return string
	 */
	protected function method(): string
	{
		return 'POST';
	}


	/**
	 * Determines whether request requires authorization
	 * @return bool
	 */
	function requiresAuth(): bool
	{
		return true;
	}


	/**
	 * @return EmptyResponse
	 * @throws ApiException
	 */
	function execute(): EmptyResponse
	{
		$this->request();

		return new EmptyResponse;
	}
}
