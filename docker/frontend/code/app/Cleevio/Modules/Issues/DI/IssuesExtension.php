<?php

declare(strict_types=1);

namespace Cleevio\Isssues\DI;

use Nette\DI\CompilerExtension;

class IssuesExtension extends CompilerExtension
{

	public function loadConfiguration()
	{
		if (defined('NETTE_TESTER')) {
			$this->compiler->loadConfig(__DIR__ . '/services.test.neon');
		} else {
			$this->compiler->loadConfig(__DIR__ . '/services.neon');
		}
	}
}
