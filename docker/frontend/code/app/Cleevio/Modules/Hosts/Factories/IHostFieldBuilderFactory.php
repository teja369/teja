<?php

declare(strict_types=1);

namespace Cleevio\Hosts\Factories;

use Cleevio\Hosts\Builders\IHostFieldFormBuilder;

interface IHostFieldBuilderFactory
{

	/**
	 * @param string $type
	 * @return IHostFieldFormBuilder
	 */
	public function getBuilder(string $type): IHostFieldFormBuilder;
}
