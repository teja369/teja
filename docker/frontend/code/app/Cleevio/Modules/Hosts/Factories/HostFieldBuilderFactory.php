<?php

declare(strict_types=1);

namespace Cleevio\Hosts\Factories;

use Cleevio\Hosts\Builders\IHostFieldFormBuilder;
use Cleevio\Hosts\Builders\NumberHostFieldFormBuilder;
use Cleevio\Hosts\Builders\TextHostFieldFormBuilder;
use InvalidArgumentException;

class HostFieldBuilderFactory implements IHostFieldBuilderFactory
{

	private const HOST_FIELD_TYPE_TEXT = 'text';
	private const HOST_FIELD_TYPE_NUMBER = 'number';

	/**
	 * @param string $type
	 * @return IHostFieldFormBuilder
	 */
	public function getBuilder(string $type): IHostFieldFormBuilder
	{
		switch ($type) {
			case self::HOST_FIELD_TYPE_TEXT:
				return new TextHostFieldFormBuilder;
			case self::HOST_FIELD_TYPE_NUMBER:
				return new NumberHostFieldFormBuilder;
		}

		throw new InvalidArgumentException;
	}
}
