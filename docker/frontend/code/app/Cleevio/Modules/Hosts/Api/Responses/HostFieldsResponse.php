<?php

declare(strict_types=1);

namespace Cleevio\Hosts\API\Responses;

use Cleevio\ApiClient\IResponse;

class HostFieldsResponse implements IResponse
{

	/**
	 * @var HostFieldResponse[]
	 */
	private $hostFields = [];

	/**
	 * HostsResponse constructor.
	 * @param array $hosts
	 */
	public function __construct(array $hosts)
	{
		$this->hostFields = array_map(static function ($host) {
			return new HostFieldResponse($host);
		}, $hosts);
	}

	public function getHostFields(): array
	{
		return $this->hostFields;
	}

}
