<?php

declare(strict_types=1);

namespace Cleevio\Hosts\API\Requests;

use Cleevio\ApiClient\Exception\ApiException;
use Cleevio\ApiClient\Requests\PaginationRequest;
use Cleevio\Hosts\API\Responses\HostsResponse;

class HostsGetRequest extends PaginationRequest implements IHostsGetRequest
{

	/**
	 * @var int
	 */
	private $tripId;

	/**
	 * @var string|null
	 */
	private $search;


	/**
	 * Endpoint path
	 * @return string
	 */
	protected function path(): string
	{
		$path = '/hosts';

		$params = [
			'limit' => $this->getLimit(),
			'page' => $this->getPage(),
			'trip' => $this->tripId,
			'type' => 'client',
			'search' => $this->search,
		];

		$query = http_build_query($params);

		return strlen($query) > 0
			? $path . '?' . $query
			: $path;
	}


	/**
	 * Method
	 * @return string
	 */
	protected function method(): string
	{
		return 'GET';
	}


	/**
	 * Determines whether request requires authorization
	 * @return bool
	 */
	function requiresAuth(): bool
	{
		return true;
	}


	/**
	 * @param int $tripId
	 */
	public function setTripId(int $tripId): void
	{
		$this->tripId = $tripId;
	}


	/**
	 * @param string|null $search
	 */
	public function setSearch(?string $search): void
	{
		$this->search = $search;
	}


	/**
	 * Execute request and return a response
	 * @return HostsResponse
	 * @throws ApiException
	 */
	function execute(): HostsResponse
	{
		return new HostsResponse($this->request());
	}

}
