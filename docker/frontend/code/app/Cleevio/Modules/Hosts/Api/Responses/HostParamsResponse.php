<?php

declare(strict_types=1);

namespace Cleevio\Hosts\API\Responses;

use Cleevio\ApiClient\IResponse;

class HostParamsResponse implements IResponse
{

	/**
	 * @var HostParamResponse[]
	 */
	private $params;


	/**
	 * HostParamsResponse constructor.
	 * @param array $params
	 */
	public function __construct(array $params)
	{
		$this->params = array_map(static function ($param) {
			return new HostParamResponse($param);
		}, $params);
	}


	/**
	 * @return HostParamResponse[]
	 */
	public function getParam(): array
	{
		return $this->params;
	}

}
