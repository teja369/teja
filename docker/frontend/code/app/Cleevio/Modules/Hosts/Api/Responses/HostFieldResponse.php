<?php

declare(strict_types=1);

namespace Cleevio\Hosts\API\Responses;

use Cleevio\ApiClient\IResponse;
use Cleevio\Fields\API\Responses\FieldResponse;
use InvalidArgumentException;

class HostFieldResponse implements IResponse
{

	/**
	 * @var int
	 */
	private $id;

	/**
	 * @var FieldResponse
	 */
	private $field;

	/**
	 * DestinationResponse constructor.
	 * @param array $host
	 */
	public function __construct(array $host)
	{
		if (isset($host['id']) && isset($host['field'])) {
			$this->id = $host['id'];
			$this->field = new FieldResponse($host['field']);
		} else {
			throw new InvalidArgumentException;
		}
	}


	/**
	 * @return int
	 */
	public function getId(): int
	{
		return $this->id;
	}

	public function getField(): FieldResponse
	{
		return $this->field;
	}

}
