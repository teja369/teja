<?php

declare(strict_types=1);

namespace Cleevio\Hosts\API\Responses;

use Cleevio\ApiClient\IResponse;
use InvalidArgumentException;

class HostResponse implements IResponse
{

	/**
	 * @var int
	 */
	private $id;

	/**
	 * @var string
	 */
	private $name;

	/**
	 * @var string
	 */
	private $type;

	/**
	 * @var string
	 */
	private $address;

	/**
	 * @var HostParamResponse[]|null
	 */
	private $params;

	/**
	 * DestinationResponse constructor.
	 * @param array $host
	 */
	public function __construct(array $host)
	{
		if (array_key_exists('id', $host) &&
			array_key_exists('name', $host) &&
			array_key_exists('type', $host) &&
			array_key_exists('address', $host)) {
			$this->id = $host['id'];
			$this->name = $host['name'];
			$this->type = $host['type'];
			$this->address = $host['address'];
			$this->params = $host['params'] === null
				? null
				: array_map(static function ($param) {
					return new HostParamResponse($param);
				}, $host['params']);
		} else {
			throw new InvalidArgumentException;
		}
	}


	/**
	 * @return int
	 */
	public function getId(): int
	{
		return $this->id;
	}


	/**
	 * @return string
	 */
	public function getName(): string
	{
		return $this->name;
	}


	/**
	 * @return string
	 */
	public function getType(): string
	{
		return $this->type;
	}

	/**
	 * @return string|null
	 */
	public function getAddress(): ?string
	{
		return $this->address;
	}

	/**
	 * @return HostParamResponse[]|null
	 */
	public function getParams(): ?array
	{
		return $this->params;
	}

}
