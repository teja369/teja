<?php

declare(strict_types=1);

namespace Cleevio\Hosts\API\Requests;

interface IHostsGetRequest
{

	/**
	 * @param int $id
	 */
	public function setTripId(int $id): void;


	/**
	 * @param string|null $search
	 */
	public function setSearch(?string $search): void;
}
