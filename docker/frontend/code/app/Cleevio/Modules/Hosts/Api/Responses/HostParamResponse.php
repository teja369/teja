<?php

declare(strict_types=1);

namespace Cleevio\Hosts\API\Responses;

use Cleevio\ApiClient\IResponse;
use Cleevio\Fields\API\Responses\FieldResponse;
use InvalidArgumentException;

class HostParamResponse implements IResponse
{

	/**
	 * @var int
	 */
	private $id;

	/**
	 * @var FieldResponse
	 */
	private $field;

	/**
	 * @var string|null
	 */
	private $value;

	/**
	 * HostParamsResponse constructor.
	 * @param array $param
	 */
	public function __construct(array $param)
	{
		if (array_key_exists('id', $param) && array_key_exists('field', $param) && array_key_exists('value', $param)) {
			$this->id = $param['id'];
			$this->field = new FieldResponse($param['field']);
			$this->value = $param['value'];
		} else {
			throw new InvalidArgumentException;
		}
	}


	/**
	 * @return int
	 */
	public function getId(): int
	{
		return $this->id;
	}


	/**
	 * @return FieldResponse
	 */
	public function getField(): FieldResponse
	{
		return $this->field;
	}


	/**
	 * @return string|null
	 */
	public function getValue(): ?string
	{
		return $this->value;
	}

}
