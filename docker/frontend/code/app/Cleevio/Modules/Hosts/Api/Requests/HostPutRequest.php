<?php

declare(strict_types=1);

namespace Cleevio\Hosts\API\Requests;

use Cleevio\API\BaseRequest;
use Cleevio\ApiClient\Exception\ApiException;
use Cleevio\Hosts\API\Responses\HostResponse;
use InvalidArgumentException;

class HostPutRequest extends BaseRequest implements IHostPutRequest
{

	/**
	 * @var int
	 */
	private $hostId;


	/**
	 * Endpoint path
	 * @return string
	 */
	protected function path(): string
	{
		if ($this->hostId === null) {
			throw new InvalidArgumentException;
		}

		return '/host/' . $this->hostId;
	}


	/**
	 * Method
	 * @return string
	 */
	protected function method(): string
	{
		return 'PUT';
	}


	/**
	 * Determines whether request requires authorization
	 * @return bool
	 */
	function requiresAuth(): bool
	{
		return true;
	}


	/**
	 * @param int $hostId
	 */
	public function setHostId(int $hostId): void
	{
		$this->hostId = $hostId;
	}


	/**
	 * Execute request and return a response
	 * @return HostResponse
	 * @throws ApiException
	 */
	function execute(): HostResponse
	{
		return new HostResponse($this->request());
	}

}
