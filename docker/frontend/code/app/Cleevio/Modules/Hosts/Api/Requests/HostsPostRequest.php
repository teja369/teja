<?php

declare(strict_types=1);

namespace Cleevio\Hosts\API\Requests;

use Cleevio\API\BaseRequest;
use Cleevio\ApiClient\Exception\ApiException;
use Cleevio\Hosts\API\Responses\HostResponse;

class HostsPostRequest extends BaseRequest implements IHostsPostRequest
{

	/**
	 * Endpoint path
	 * @return string
	 */
	protected function path(): string
	{
		return '/hosts';
	}


	/**
	 * Method
	 * @return string
	 */
	protected function method(): string
	{
		return 'POST';
	}


	/**
	 * Determines whether request requires authorization
	 * @return bool
	 */
	function requiresAuth(): bool
	{
		return true;
	}


	/**
	 * @return HostResponse
	 * @throws ApiException
	 */
	function execute(): HostResponse
	{
		return new HostResponse($this->request());
	}

}
