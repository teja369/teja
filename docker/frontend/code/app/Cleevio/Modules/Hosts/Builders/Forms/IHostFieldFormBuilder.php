<?php

declare(strict_types=1);

namespace Cleevio\Hosts\Builders;

use Cleevio\Fields\API\Responses\FieldResponse;
use Cleevio\Hosts\API\Responses\HostResponse;
use Cleevio\UI\BaseForm;

interface IHostFieldFormBuilder
{

	/**
	 * @param BaseForm $form
	 * @param HostResponse|null $host
	 * @param FieldResponse $input
	 * @param string|null $defaultValue
	 * @return BaseForm
	 */
	public function build(
		BaseForm $form,
		?HostResponse $host,
		FieldResponse $input,
		?string $defaultValue
	): BaseForm;

}
