<?php

declare(strict_types=1);

namespace Cleevio\Hosts\Builders;

use Cleevio\Fields\API\Responses\FieldResponse;
use Cleevio\Hosts\API\Responses\HostResponse;
use Cleevio\UI\BaseForm;

class TextHostFieldFormBuilder implements IHostFieldFormBuilder
{

	public const HOST_ENTITY_TYPE_NON_CLIENT = 'non-client';

	public const MAX_LENGTH = 100;

	/**
	 * @param BaseForm $form
	 * @param HostResponse|null $host
	 * @param FieldResponse $input
	 * @param string|null $defaultValue
	 * @return BaseForm
	 */
	public function build(
		BaseForm $form,
		?HostResponse $host,
		FieldResponse $input,
		?string $defaultValue
	): BaseForm
	{
		$inputId = (string) $input->getId();
		$translator = $form->getTranslator();

		if ($translator === null) {
			throw new \InvalidArgumentException;
		}

		$inputText = $form->addText($inputId, $input->getName())
			->setDefaultValue(
				$defaultValue === null || $host === null || $host->getType() !== self::HOST_ENTITY_TYPE_NON_CLIENT
					? null
					: $defaultValue
			);

		if ($input->getPlaceholder() !== null) {
			$form[$inputId]->setHtmlAttribute('placeholder', $input->getPlaceholder());
		}

		if ($input->getValidation() !== null) {
			$inputText
				->addConditionOn($form['toggler'], $form::EQUAL, self::HOST_ENTITY_TYPE_NON_CLIENT)
				->addRule($form::PATTERN, $translator->translate('frontend.trips.create-trip.host.form.validation.field.pattern', [
					"name" => $input->getName(),
				]), $input->getValidation());
		}

		if ($input->isRequired()) {
			$inputText
				->addConditionOn($form['toggler'], $form::EQUAL, self::HOST_ENTITY_TYPE_NON_CLIENT)
				->addRule($form::REQUIRED, $translator->translate('frontend.trips.create-trip.host.form.validation.field.required', [
					"name" => $input->getName(),
				])
				);
		}

		$form[$inputId]->addCondition($form::FILLED)
			->addRule($form::MAX_LENGTH,
				$translator->translate('frontend.questions.validation.maxLength', [
					'name' => $input->getName(),
					'count' => self::MAX_LENGTH,
				])
				, self::MAX_LENGTH);

		return $form;
	}
}
