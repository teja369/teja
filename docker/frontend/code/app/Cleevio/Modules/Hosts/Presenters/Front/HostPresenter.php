<?php

declare(strict_types=1);

namespace Cleevio\Hosts\FrontModule\Presenters;

use Cleevio\ApiClient\Exception\ApiException;
use Cleevio\FrontModule\Presenters\BasePresenter;
use Cleevio\Hosts\API\Requests\HostsGetRequest;
use Cleevio\Hosts\API\Responses\HostResponse;
use Nette\Application\AbortException;

class HostPresenter extends BasePresenter
{

	/**
	 * @var HostsGetRequest
	 */
	private $hostsGetRequest;


	public function __construct(HostsGetRequest $hostsGetRequest)
	{
		parent::__construct();
		$this->hostsGetRequest = $hostsGetRequest;
	}


	/**
	 * @param string|null $q
	 * @param int $tripId
	 * @param int $p
	 * @throws AbortException
	 * @throws ApiException
	 */
	public function actionSearchHost(?string $q, ?int $tripId, int $p = 1): void
	{
		if ($q !== null && $this->isAjax()) {
			$this->hostsGetRequest->setSearch($q);

			if ($tripId !== null) {
				$this->hostsGetRequest->setTripId($tripId);
			}

			$this->hostsGetRequest->setLimit(100);
			$this->hostsGetRequest->setPage($p-1);

			$results = $this->hostsGetRequest->execute();
			$result = array_map(static function (HostResponse $result) {
				return [
					'text' => $result->getName(),
					'value' => $result->getId(),
				];
			}, $results->getItems());

			$this->sendJson($result);
		}
	}


	/**
	 * @return string
	 */
	protected function getLayoutName(): string
	{
		return 'layout-trip';
	}
}
