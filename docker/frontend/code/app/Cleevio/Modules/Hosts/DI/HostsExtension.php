<?php

declare(strict_types=1);

namespace Cleevio\Hosts\DI;

use Cleevio\Hosts\Router\RouterFactory;
use Cleevio\Modules\Providers\IPresenterMappingProvider;
use Cleevio\Modules\Providers\IRouterProvider;
use Nette\DI\CompilerExtension;

class HostsExtension extends CompilerExtension implements IPresenterMappingProvider, IRouterProvider
{

	public function loadConfiguration()
	{
		if (defined('NETTE_TESTER')) {
			$this->compiler->loadConfig(__DIR__ . '/services.test.neon');
		} else {
			$this->compiler->loadConfig(__DIR__ . '/services.neon');
		}
	}

	public function getPresenterMapping(): array
	{
		return ['Hosts' => 'Cleevio\\Hosts\\*Module\\Presenters\\*Presenter'];
	}


	public function getRouterSettings(): array
	{
		return [100 => RouterFactory::class];
	}
}
