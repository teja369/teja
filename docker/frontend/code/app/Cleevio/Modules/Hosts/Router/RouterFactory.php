<?php

declare(strict_types=1);

namespace Cleevio\Hosts\Router;

use Nette;
use Nette\Application\Routers\Route;
use Nette\Application\Routers\RouteList;

class RouterFactory
{
	/**
	 * @return Nette\Application\IRouter
	 */
	public function createRouter()
	{
		$router = new RouteList;

		$router[] = $hostsModule = new RouteList('Hosts');

		$hostsModule[] = new Route('hosts/<presenter>/<action>[/<id>]', [
			'presenter' => 'Host',
			'action' => 'default',
			'module' => 'Front',
		]);

		return $router;
	}

}
