<?php

declare(strict_types=1);

namespace Cleevio\Questions\Router;

use Nette;
use Nette\Application\Routers\Route;
use Nette\Application\Routers\RouteList;

class RouterFactory
{
	/**
	 * @return Nette\Application\IRouter
	 */
	public function createRouter()
	{
		$router = new RouteList;

		$router[] = $questionsModule = new RouteList('Questions');

		$questionsModule[] = new Route('questions/<presenter>/<action>[/<id>]', [
			'presenter' => 'Question',
			'action' => 'default',
			'module' => 'Front',
		]);

		return $router;
	}

}
