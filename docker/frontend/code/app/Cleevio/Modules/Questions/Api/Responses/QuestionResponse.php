<?php

declare(strict_types=1);

namespace Cleevio\Questions\API\Responses;

use Cleevio\ApiClient\IResponse;
use InvalidArgumentException;

class QuestionResponse implements IResponse
{

	/**
	 * @var int
	 */
	private $id;

	/**
	 * @var string
	 */
	private $type;

	/**
	 * @var string
	 */
	private $question;

	/**
	 * @var string|null
	 */
	private $validation;

	/**
	 * @var TextQuestionResponse|null
	 */
	private $text;

	/**
	 * @var array|null
	 */
	private $bool;

	/**
	 * @var NumberQuestionResponse|null
	 */
	private $number;

	/**
	 * @var ChoiceQuestionResponse|null
	 */
	private $choice;

	/**
	 * @var DateTimeQuestionResponse|null
	 */
	private $datetime;

	/**
	 * @var bool
	 */
	private $required;

	/**
	 * @var QuestionResponse|null
	 */
	private $parent;

	/**
	 * @var string|null
	 */
	private $parentRule;

	/**
	 * @var GroupResponse
	 */
	private $group;

	/**
	 * @var string|null
	 */
	private $help;

	/**
	 * @var int
	 */
	private $order;

	/**
	 * @var string|null
	 */
	private $placeholder;

	/**
	 * @var string|null
	 */
	private $validationHelp;

	/**
	 * @var CountryQuestionResponse|null
	 */
	private $country;


	/**
	 * QuestionResponse constructor.
	 * @param array $question
	 */
	public function __construct(array $question)
	{
		if (array_key_exists('id', $question) &&
			array_key_exists('type', $question) &&
			array_key_exists('question', $question) &&
			array_key_exists('validation', $question) &&
			array_key_exists('required', $question)
		) {
			$this->id = $question['id'];
			$this->type = $question['type'];
			$this->parent = $question['parent'] !== null
				? new QuestionResponse($question['parent'])
				: null;
			$this->parentRule = $question['parentRule'];
			$this->question = $question['question'];
			$this->group = new GroupResponse($question['group']);
			$this->required = $question['required'];
			$this->validation = $question['validation'];
			$this->validationHelp = $question['validationHelp'];
			$this->order = $question['order'];
			$this->help = $question['help'];
			$this->placeholder = $question['placeholder'];
			$this->text = array_key_exists('text', $question) && $question['text'] !== null
				? new TextQuestionResponse($question['text'])
				: null;
			$this->bool = array_key_exists('bool', $question) && $question['bool'] !== null
				? $question['bool']
				: null;
			$this->number = array_key_exists('number', $question) && $question['number'] !== null
				? new NumberQuestionResponse($question['number'])
				: null;
			$this->choice = array_key_exists('choice', $question) && $question['choice'] !== null
				? new ChoiceQuestionResponse($question['choice'])
				: null;
			$this->datetime = array_key_exists('datetime', $question) && $question['datetime'] !== null
				? new DateTimeQuestionResponse($question['datetime'])
				: null;
			$this->country = array_key_exists('country', $question) && $question['country'] !== null
				? new CountryQuestionResponse($question['country'])
				: null;
		} else {
			throw new InvalidArgumentException;
		}
	}


	/**
	 * @return int
	 */
	public function getId(): int
	{
		return $this->id;
	}


	/**
	 * @return string
	 */
	public function getType(): string
	{
		return $this->type;
	}


	/**
	 * @return string
	 */
	public function getQuestion(): string
	{
		return $this->question;
	}


	/**
	 * @return string|null
	 */
	public function getValidation(): ?string
	{
		return $this->validation;
	}


	/**
	 * @return GroupResponse
	 */
	public function getGroup(): GroupResponse
	{
		return $this->group;
	}


	/**
	 * @return TextQuestionResponse|null
	 */
	public function getText(): ?TextQuestionResponse
	{
		return $this->text;
	}


	/**
	 * @return array|null
	 */
	public function getBool(): ?array
	{
		return $this->bool;
	}


	/**
	 * @return NumberQuestionResponse|null
	 */
	public function getNumber(): ?NumberQuestionResponse
	{
		return $this->number;
	}


	/**
	 * @return ChoiceQuestionResponse|null
	 */
	public function getChoice(): ?ChoiceQuestionResponse
	{
		return $this->choice;
	}


	/**
	 * @return DateTimeQuestionResponse|null
	 */
	public function getDatetime(): ?DateTimeQuestionResponse
	{
		return $this->datetime;
	}


	/**
	 * @return bool
	 */
	public function getRequired(): bool
	{
		return $this->required;
	}


	/**
	 * @return QuestionResponse|null
	 */
	public function getParent(): ?QuestionResponse
	{
		return $this->parent;
	}


	/**
	 * @return string|null
	 */
	public function getParentRule(): ?string
	{
		return $this->parentRule;
	}


	/**
	 * @return string|null
	 */
	public function getValidationHelp(): ?string
	{
		return $this->validationHelp;
	}


	/**
	 * @return string|null
	 */
	public function getHelp(): ?string
	{
		return $this->help;
	}


	/**
	 * @return int
	 */
	public function getOrder(): int
	{
		return $this->order;
	}


	/**
	 * @return string|null
	 */
	public function getPlaceholder(): ?string
	{
		return $this->placeholder;
	}


	/**
	 * @return CountryQuestionResponse|null
	 */
	public function getCountry(): ?CountryQuestionResponse
	{
		return $this->country;
	}

}
