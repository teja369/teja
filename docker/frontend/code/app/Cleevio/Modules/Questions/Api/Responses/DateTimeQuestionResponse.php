<?php

declare(strict_types=1);

namespace Cleevio\Questions\API\Responses;

use Cleevio\ApiClient\IResponse;
use InvalidArgumentException;

class DateTimeQuestionResponse implements IResponse
{

	/**
	 * @var string|null
	 */
	private $variant;


	/**
	 * DateTimeQuestionResponse constructor.
	 * @param array $dateTimeQuestion
	 */
	public function __construct(array $dateTimeQuestion)
	{
		if (array_key_exists('variant', $dateTimeQuestion)) {
			$this->variant = $dateTimeQuestion['variant'];
		} else {
			throw new InvalidArgumentException;
		}
	}

	public function getVariant(): ?string
	{
		return $this->variant;
	}

}
