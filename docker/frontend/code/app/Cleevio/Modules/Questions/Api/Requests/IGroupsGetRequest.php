<?php

declare(strict_types=1);

namespace Cleevio\Questions\API\Requests;

interface IGroupsGetRequest
{

	/**
	 * @param int $tripId
	 */
	public function setTripId(int $tripId): void;

	/**
	 * @param string $type
	 */
	public function setType(string $type): void;
}
