<?php

declare(strict_types=1);

namespace Cleevio\Questions\API\Responses;

use Cleevio\ApiClient\IResponse;

class QuestionsResponse implements IResponse
{

	/**
	 * @var QuestionResponse[]
	 */
	private $questions = [];


	/**
	 * QuestionsResponse constructor.
	 * @param array $questions
	 */
	public function __construct(array $questions)
	{
		$this->questions = array_map(static function ($question) {
			return new QuestionResponse($question);
		}, $questions);
	}


	/**
	 * @return QuestionResponse[]
	 */
	public function getQuestions(): array
	{
		return $this->questions;
	}

}
