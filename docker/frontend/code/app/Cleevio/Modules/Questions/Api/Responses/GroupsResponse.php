<?php

declare(strict_types=1);

namespace Cleevio\Questions\API\Responses;

use Cleevio\ApiClient\IResponse;

class GroupsResponse implements IResponse
{

	/**
	 * @var GroupResponse[]
	 */
	private $groups = [];


	/**
	 * GroupsResponse constructor.
	 * @param array $groups
	 */
	public function __construct(array $groups)
	{
		$this->groups = array_map(static function ($group) {
			return new GroupResponse($group);
		}, $groups);
	}

	/**
	 * @return GroupResponse[]
	 */
	public function getGroups(): array
	{
		return $this->groups;
	}

}
