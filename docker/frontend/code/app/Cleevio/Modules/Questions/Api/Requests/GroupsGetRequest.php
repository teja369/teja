<?php

declare(strict_types=1);

namespace Cleevio\Questions\API\Requests;

use Cleevio\API\BaseRequest;
use Cleevio\ApiClient\Exception\ApiException;
use Cleevio\Questions\API\Responses\GroupsResponse;
use InvalidArgumentException;

class GroupsGetRequest extends BaseRequest implements IGroupsGetRequest
{

	/**
	 * @var string|null
	 */
	private $type = null;

	/**
	 * @var int
	 */
	private $tripId;

	/**
	 * Endpoint path
	 * @return string
	 */
	protected function path(): string
	{
		if ($this->tripId === null) {
			throw new InvalidArgumentException;
		}

		return '/questions/groups?trip=' . $this->tripId . '&type=' . $this->type;
	}


	/**
	 * Method
	 * @return string
	 */
	protected function method(): string
	{
		return 'GET';
	}

	/**
	 * Determines whether request requires authorization
	 * @return bool
	 */
	function requiresAuth(): bool
	{
		return true;
	}

	/**
	 * @param string|null $type
	 */
	public function setType(?string $type): void
	{
		$this->type = $type;
	}

	/**
	 * Execute request and return a response
	 * @return GroupsResponse
	 * @throws ApiException
	 */
	function execute(): GroupsResponse
	{
		return new GroupsResponse($this->request());
	}

	/**
	 * @param int $tripId
	 */
	public function setTripId(int $tripId): void
	{
		$this->tripId = $tripId;
	}
}
