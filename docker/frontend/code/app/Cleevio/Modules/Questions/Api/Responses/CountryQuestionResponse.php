<?php

declare(strict_types=1);

namespace Cleevio\Questions\API\Responses;

use Cleevio\ApiClient\IResponse;

class CountryQuestionResponse implements IResponse
{

	/**
	 * CountryQuestionResponse constructor.
	 * @param array $countryQuestion
	 */
	public function __construct(array $countryQuestion)
	{
		unset($countryQuestion);
	}

}
