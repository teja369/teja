<?php

declare(strict_types=1);

namespace Cleevio\Questions\API\Requests;

use Cleevio\ApiClient\Exception\ApiException;
use Cleevio\ApiClient\Requests\PaginationRequest;
use Cleevio\Questions\API\Responses\QuestionOptionsPaginatedResponse;

class QuestionOptionGetRequest extends PaginationRequest implements IQuestionOptionGetRequest
{

	/**
	 * @var int|null
	 */
	private $tripId = null;

	/**
	 * @var int
	 */
	private $questionId;

	/**
	 * @var string|null
	 */
	private $search;


	/**
	 * Endpoint path
	 * @return string
	 */
	protected function path(): string
	{
		$path = sprintf('/questions/%s/option', $this->questionId);

		$params = [
			'limit' => $this->getLimit(),
			'page' => $this->getPage(),
			'trip' => $this->tripId,
			'search' => $this->search,
		];

		$query = http_build_query($params);

		return strlen($query) > 0
			? $path . '?' . $query
			: $path;
	}


	/**
	 * Method
	 * @return string
	 */
	protected function method(): string
	{
		return 'GET';
	}


	/**
	 * Determines whether request requires authorization
	 * @return bool
	 */
	function requiresAuth(): bool
	{
		return true;
	}


	/**
	 * @param int $tripId
	 */
	public function setTripId(int $tripId): void
	{
		$this->tripId = $tripId;
	}


	/**
	 * @param int $questionId
	 */
	public function setQuestionId(int $questionId): void
	{
		$this->questionId = $questionId;
	}


	/**
	 * @param string $search
	 */
	public function setSearch(string $search): void
	{
		$this->search = $search;
	}


	/**
	 * Execute request and return a response
	 * @return QuestionOptionsPaginatedResponse
	 * @throws ApiException
	 */
	function execute(): QuestionOptionsPaginatedResponse
	{
		return new QuestionOptionsPaginatedResponse($this->request());
	}
}
