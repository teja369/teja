<?php

declare(strict_types=1);

namespace Cleevio\Questions\API\Responses;

use Cleevio\ApiClient\DTO\PaginationResponse;

class QuestionOptionsPaginatedResponse extends PaginationResponse
{

	/**
	 * @var OptionResponse[]
	 */
	private $items = [];


	/**
	 * QuestionResponse constructor.
	 * @param array $response
	 */
	public function __construct(array $response)
	{
		parent::__construct($response['data']);

		$this->items = array_map(static function($option) {
			return new OptionResponse($option);
		}, $response['data']['items']);
	}


	public function getItems(): array
	{
		return $this->items;
	}

}
