<?php

declare(strict_types=1);

namespace Cleevio\Questions\API\Requests;

interface IQuestionOptionGetRequest
{

	/**
	 * @param int $tripId
	 */
	public function setTripId(int $tripId): void;


	/**
	 * @param int $questionId
	 */
	public function setQuestionId(int $questionId): void;


	/**
	 * @param string $search
	 */
	public function setSearch(string $search): void;
}
