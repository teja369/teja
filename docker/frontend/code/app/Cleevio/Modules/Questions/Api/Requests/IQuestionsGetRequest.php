<?php

declare(strict_types=1);

namespace Cleevio\Questions\API\Requests;

interface IQuestionsGetRequest
{

	/**
	 * @param int $tripId
	 */
	public function setTripId(int $tripId): void;

	/**
	 * @param int $groupId
	 */
	public function setGroupId(int $groupId): void;
}
