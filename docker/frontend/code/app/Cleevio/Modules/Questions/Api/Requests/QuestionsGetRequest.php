<?php

declare(strict_types=1);

namespace Cleevio\Questions\API\Requests;

use Cleevio\API\BaseRequest;
use Cleevio\ApiClient\Exception\ApiException;
use Cleevio\Questions\API\Responses\QuestionsResponse;
use InvalidArgumentException;

class QuestionsGetRequest extends BaseRequest implements IQuestionsGetRequest
{

	/**
	 * @var int|null
	 */
	private $tripId = null;

	/**
	 * @var int|null
	 */
	private $groupId = null;

	/**
	 * Endpoint path
	 * @return string
	 */
	protected function path(): string
	{
		if ($this->tripId === null || $this->groupId === null) {
			throw new InvalidArgumentException;
		}

		return '/questions/groups/' . $this->groupId . '/questions?trip=' . $this->tripId;
	}


	/**
	 * Method
	 * @return string
	 */
	protected function method(): string
	{
		return 'GET';
	}


	/**
	 * Determines whether request requires authorization
	 * @return bool
	 */
	function requiresAuth(): bool
	{
		return true;
	}


	/**
	 * @param int $tripId
	 */
	public function setTripId(int $tripId): void
	{
		$this->tripId = $tripId;
	}

	/**
	 * @param int $groupId
	 */
	public function setGroupId(int $groupId): void
	{
		$this->groupId = $groupId;
	}

	/**
	 * Execute request and return a response
	 * @return QuestionsResponse
	 * @throws ApiException
	 */
	function execute(): QuestionsResponse
	{
		return new QuestionsResponse($this->request());
	}
}
