<?php

declare(strict_types=1);

namespace Cleevio\Questions\Builders;

use Cleevio\Questions\API\Responses\QuestionResponse;
use Cleevio\Trips\API\Responses\TripAnswerResponse;
use Cleevio\UI\BaseForm;
use InvalidArgumentException;

class DateTimeQuestionFormBuilder extends BaseFormBuilder implements IDateTimeQuestionFormBuilder
{

	/**
	 * @param BaseForm                $form
	 * @param QuestionResponse        $question
	 * @param TripAnswerResponse|null $answer
	 * @return BaseForm
	 */
	public function build(
		BaseForm $form,
		QuestionResponse $question,
		?TripAnswerResponse $answer
	): BaseForm
	{
		if ($question->getDatetime() === null) {
			throw new InvalidArgumentException;
		}

		$questionId = (string) $question->getId();
		$translator = $form->getTranslator();

		if ($translator === null) {
			throw new \InvalidArgumentException;
		}

		$defaultValue = null;

		if ($answer !== null && $answer->getDatetime() !== null) {
			switch ($question->getDatetime()->getVariant()) {
				case 'datetime':
					$datetime = explode(', ', $answer->getDatetime());

					$date = strtotime($datetime[0]);
					$time = strtotime($datetime[1]);

					if ($date === false || $time === false) {
						throw new InvalidArgumentException;
					}

					$date = date('d-m-Y', $date);
					$time = date('h:i A', $time);

					$defaultValue = implode(' ', [$date, $time]);

					break;
				case 'date':
					$date = strtotime($answer->getDatetime());

					if ($date === false) {
						throw new InvalidArgumentException;
					}

					$defaultValue = date('d-m-Y', $date);

					break;
				case 'time':

					$time = strtotime($answer->getDatetime());

					if ($time === false) {
						throw new InvalidArgumentException;
					}

					$defaultValue = date('h:i A', $time);

					break;
			}
		}

		$form->addText($questionId, $question->getQuestion())
			->setDefaultValue($defaultValue);

		$form = $this->addParentRule($form, $question);

		if ($question->getPlaceholder() !== null) {
			$form[$questionId]->setHtmlAttribute('placeholder', $question->getPlaceholder());
		}

		if ($question->getValidation() !== null) {
			$form[$questionId]
				->addCondition($form::FILLED)
				->addRule($form::PATTERN,
					$translator->translate('frontend.questions.validation.pattern', [
						'name' => $question->getQuestion(),
					]),
					$question->getValidation());
		}

		return $form;
	}
}
