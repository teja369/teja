<?php

declare(strict_types=1);

namespace Cleevio\Questions\Builders;

use Cleevio\Questions\API\Responses\QuestionResponse;
use Cleevio\Trips\API\Responses\TripAnswerResponse;
use Cleevio\UI\BaseForm;
use InvalidArgumentException;

class TextQuestionFormBuilder extends BaseFormBuilder implements ITextQuestionFormBuilder
{

	private const MAX_LENGTH = 100;


	/**
	 * @param BaseForm                $form
	 * @param QuestionResponse        $question
	 * @param TripAnswerResponse|null $answer
	 * @return BaseForm
	 */
	public function build(
		BaseForm $form,
		QuestionResponse $question,
		?TripAnswerResponse $answer
	): BaseForm
	{
		if ($question->getText() === null) {
			throw new InvalidArgumentException;
		}

		$questionId = (string) $question->getId();
		$translator = $form->getTranslator();

		if ($translator === null) {
			throw new \InvalidArgumentException;
		}

		$form->addText($questionId, $question->getQuestion())
			->setDefaultValue($answer === null ? null : $answer->getText());

		$form = $this->addParentRule($form, $question);

		if ($question->getPlaceholder() !== null) {
			$form[$questionId]->setHtmlAttribute('placeholder', $question->getPlaceholder());
		}

		if ($question->getText()->getMinLength() !== null) {
			$form[$questionId]
				->addCondition($form::FILLED)
				->addRule($form::MIN_LENGTH,
					$translator->translate('frontend.questions.validation.minLength', [
						'name' => $question->getQuestion(),
						'count' => $question->getText()->getMinLength(),
					])
					, $question->getText()->getMinLength());
		}

		$maxLength = $question->getText()->getMaxLength() !== null
			? min([self::MAX_LENGTH, $question->getText()->getMaxLength()])
			: self::MAX_LENGTH;

		$form[$questionId]
			->addCondition($form::FILLED)
			->addRule($form::MAX_LENGTH,
				$translator->translate('frontend.questions.validation.maxLength', [
					'name' => $question->getQuestion(),
					'count' => $maxLength,
				])
				, $maxLength);

		if ($question->getValidation() !== null) {
			$form[$questionId]
				->addCondition($form::FILLED)
				->addRule($form::PATTERN,
					$translator->translate('frontend.questions.validation.pattern', [
						'name' => $question->getQuestion(),
					]),
					$question->getValidation());
		}

		return $form;
	}
}
