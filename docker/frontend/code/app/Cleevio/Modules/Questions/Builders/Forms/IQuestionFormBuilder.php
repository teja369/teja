<?php

declare(strict_types=1);

namespace Cleevio\Questions\Builders;

use Cleevio\Questions\API\Responses\QuestionResponse;
use Cleevio\Trips\API\Responses\TripAnswerResponse;
use Cleevio\UI\BaseForm;

interface IQuestionFormBuilder
{

	/**
	 * @param BaseForm                      $form
	 * @param QuestionResponse              $question
	 * @param TripAnswerResponse|null       $answer
	 * @return BaseForm
	 */
	public function build(
		BaseForm $form,
		QuestionResponse $question,
		?TripAnswerResponse $answer
	): BaseForm;
}
