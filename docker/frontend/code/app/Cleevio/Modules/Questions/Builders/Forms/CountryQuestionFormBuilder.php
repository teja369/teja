<?php

declare(strict_types=1);

namespace Cleevio\Questions\Builders;

use Cleevio\Questions\API\Responses\QuestionResponse;
use Cleevio\Trips\API\Responses\TripAnswerResponse;
use Cleevio\UI\BaseForm;
use Cleevio\UI\DynamicSelect;
use InvalidArgumentException;
use Nette\Utils\Html;

class CountryQuestionFormBuilder extends BaseFormBuilder implements ICountryQuestionFormBuilder
{

	/**
	 * @param BaseForm                $form
	 * @param QuestionResponse        $question
	 * @param TripAnswerResponse|null $answer
	 * @return BaseForm
	 */
	public function build(
		BaseForm $form,
		QuestionResponse $question,
		?TripAnswerResponse $answer
	): BaseForm
	{
		if ($question->getCountry() === null) {
			throw new InvalidArgumentException;
		}

		$questionId = (string) $question->getId();
		$translator = $form->getTranslator();

		if ($translator === null) {
			throw new \InvalidArgumentException;
		}

		$form[$questionId] = (new DynamicSelect($question->getQuestion()));

		if ($answer !== null && $answer->getCountry() !== null) {
			$item = Html::el('option')
				->setAttribute('data-icon', 'flag-' . $answer->getCountry()->getCode())
				->setAttribute('value', $answer->getCountry()->getCode())
				->setText($answer->getCountry()->getName());

			$form[$questionId]
				->setItems([$answer->getCountry()->getCode() => $item])
				->setDefaultValue($answer->getCountry()->getCode());
		}

		$form = $this->addParentRule($form, $question);

		if ($question->getPlaceholder() !== null) {
			$form[$questionId]->setHtmlAttribute('placeholder', $question->getPlaceholder());
		}

		if ($question->getValidation() !== null) {
			$form[$questionId]
				->addCondition($form::FILLED)
				->addRule($form::PATTERN,
					$translator->translate('frontend.questions.validation.pattern', [
						'name' => $question->getQuestion(),
					]),
					$question->getValidation());
		}

		return $form;
	}
}
