<?php

declare(strict_types=1);

namespace Cleevio\Questions\Builders;

use Cleevio\Questions\API\Responses\QuestionResponse;
use Cleevio\Trips\API\Responses\TripAnswerResponse;
use Cleevio\UI\BaseForm;

class BoolQuestionFormBuilder extends BaseFormBuilder implements IBoolQuestionFormBuilder
{

	/**
	 * @param BaseForm $form
	 * @param QuestionResponse $question
	 * @param TripAnswerResponse|null $answer
	 * @return BaseForm
	 */
	public function build(
		BaseForm $form,
		QuestionResponse $question,
		?TripAnswerResponse $answer
	): BaseForm
	{
		$translator = $form->getTranslator();

		if ($translator === null) {
			throw new \InvalidArgumentException;
		}

		$form->addRadioList((string) $question->getId(), $question->getQuestion(), [
			true => 'frontend.questions.bool-question.option.true',
			false => 'frontend.questions.bool-question.option.false',
		])->setDefaultValue($answer === null ? null : (int) $answer->getBool());

		return $this->addParentRule($form, $question);
	}
}
