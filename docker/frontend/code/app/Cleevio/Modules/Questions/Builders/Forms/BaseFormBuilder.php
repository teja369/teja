<?php

declare(strict_types=1);

namespace Cleevio\Questions\Builders;

use Cleevio\Questions\API\Responses\QuestionResponse;
use Cleevio\UI\BaseForm;

class BaseFormBuilder
{

	/**
	 * @param BaseForm         $form
	 * @param QuestionResponse $question
	 * @return BaseForm
	 */
	public function addParentRule(BaseForm $form, QuestionResponse $question): BaseForm
	{
		$questionId = (string) $question->getId();

		$translator = $form->getTranslator();

		if ($translator === null) {
			throw new \InvalidArgumentException;
		}

		if ($question->getParent() !== null) {

			$parent = (string) $question->getParent()->getId();

			// If question has parent set as required only if parent answers matches the rule

			if ($question->getParentRule() === null) {

				$form[$parent]
					->addConditionOn($form[$parent], $form::FILLED)
					->toggle($question->getType() . '-' . $questionId);

				if ($question->getRequired()) {
					$form[$questionId]
						->addConditionOn($form[$parent], $form::FILLED)
						->addRule($form::REQUIRED, $translator->translate('frontend.questions.validation.required', [
							'name' => $question->getQuestion(),
						]));
				}
			} else {

				$form[$parent]
					->addConditionOn($form[$parent], $form::FILLED)
					->addConditionOn($form[$parent], $form::PATTERN, $question->getParentRule())
					->toggle($question->getType() . '-' . $questionId);

				if ($question->getRequired()) {
					$form[$questionId]
						->addConditionOn($form[$parent], $form::FILLED)
						->addConditionOn($form[$parent], $form::PATTERN, $question->getParentRule())
						->addRule($form::REQUIRED, $translator->translate('frontend.questions.validation.required', [
							'name' => $question->getQuestion(),
						]));
				}
			}
		} elseif ($question->getRequired()) {

			//For questions without a parent set required because it is always visible

			$form[$questionId]->addRule($form::REQUIRED, $translator->translate('frontend.questions.validation.required', [
				'name' => $question->getQuestion(),
			]));

		}

		return $form;
	}
}
