<?php

declare(strict_types=1);

namespace Cleevio\Questions\Builders;

use Cleevio\Questions\API\Responses\QuestionResponse;
use Cleevio\Trips\API\Responses\TripAnswerResponse;
use Cleevio\UI\BaseForm;
use InvalidArgumentException;

class NumberQuestionFormBuilder extends BaseFormBuilder implements INumberQuestionFormBuilder
{

	/**
	 * @param BaseForm $form
	 * @param QuestionResponse $question
	 * @param TripAnswerResponse|null $answer
	 * @return BaseForm
	 */
	public function build(
		BaseForm $form,
		QuestionResponse $question,
		?TripAnswerResponse $answer
	): BaseForm
	{
		if ($question->getNumber() === null) {
			throw new InvalidArgumentException;
		}

		$questionId = (string) $question->getId();
		$translator = $form->getTranslator();

		if ($translator === null) {
			throw new \InvalidArgumentException;
		}

		$form->addText($questionId, $question->getQuestion()) /// addInteger cannot be nullable?
			->setDefaultValue($answer === null ? null : $answer->getNumber())
			->addCondition($form::FILLED)
			->addRule($form::INTEGER);

		$form = $this->addParentRule($form, $question);

		if ($question->getPlaceholder() !== null) {
			$form[$questionId]->setHtmlAttribute('placeholder', $question->getPlaceholder());
		}

		if ($question->getNumber()->getMinValue() !== null) {
			$form[$questionId]
				->addCondition($form::FILLED)
				->addRule($form::MIN,
				$translator->translate('frontend.questions.validation.minValue', [
					'name' => $question->getQuestion(),
					'count' => $question->getNumber()->getMinValue(),
				])
				, $question->getNumber()->getMinValue());
		}

		if ($question->getNumber()->getMaxValue() !== null) {
			$form[$questionId]
				->addCondition($form::FILLED)
				->addRule($form::MAX,
				$translator->translate('frontend.questions.validation.maxValue', [
					'name' => $question->getQuestion(),
					'count' => $question->getNumber()->getMaxValue(),
				])
				, $question->getNumber()->getMaxValue());
		}

		if ($question->getValidation() !== null) {
			$form[$questionId]
				->addCondition($form::FILLED)
				->addRule($form::PATTERN,
				$translator->translate('frontend.questions.validation.pattern', [
					'name' => $question->getQuestion(),
				]),
				$question->getValidation());
		}

		return $form;
	}
}
