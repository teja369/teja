<?php

declare(strict_types=1);

namespace Cleevio\Questions\Builders;

use Cleevio\ApiClient\Exception\ApiException;
use Cleevio\Questions\API\Requests\QuestionOptionGetRequest;
use Cleevio\Questions\API\Responses\OptionResponse;
use Cleevio\Questions\API\Responses\QuestionResponse;
use Cleevio\Trips\API\Responses\TripAnswerResponse;
use Cleevio\UI\BaseForm;
use Cleevio\UI\DynamicMultiSelect;
use Cleevio\UI\DynamicSelect;
use InvalidArgumentException;

class ChoiceQuestionFormBuilder extends BaseFormBuilder implements IChoiceQuestionFormBuilder
{

	private const DROPDOWN_THRESHOLD_LOW = 10;

	private const DROPDOWN_THRESHOLD_HIGH = 100;

	/**
	 * @var QuestionOptionGetRequest
	 */
	private $optionGetRequest;


	public function __construct(QuestionOptionGetRequest $optionGetRequest)
	{
		$this->optionGetRequest = $optionGetRequest;
	}


	/**
	 * @param BaseForm                $form
	 * @param QuestionResponse        $question
	 * @param TripAnswerResponse|null $answer
	 * @return BaseForm
	 * @throws ApiException
	 */
	public function build(
		BaseForm $form,
		QuestionResponse $question,
		?TripAnswerResponse $answer
	): BaseForm
	{
		if ($question->getChoice() === null) {
			throw new InvalidArgumentException;
		}

		$questionId = (string) $question->getId();
		$translator = $form->getTranslator();

		if ($translator === null) {
			throw new \InvalidArgumentException;
		}

		$this->optionGetRequest->setQuestionId($question->getId());
		$this->optionGetRequest->setLimit(self::DROPDOWN_THRESHOLD_HIGH);
		$response = $this->optionGetRequest->execute();

		$count = $response->getCurrentItemsCount();

		$options = [];

		/** @var OptionResponse $option */
		foreach ($response->getItems() as $option) {
			$options[$option->getId()] = $option->getText();
		}

		if ($question->getChoice()->isMultiChoice()) {
			if ($count <= self::DROPDOWN_THRESHOLD_LOW) {
				$form->addCheckboxList($questionId, $question->getQuestion(), $options);
			} elseif ($count > self::DROPDOWN_THRESHOLD_LOW && $count < self::DROPDOWN_THRESHOLD_HIGH) {
				$form->addMultiSelect($questionId, $question->getQuestion(), $options);
			} else {
				if ($answer !== null && $answer->getChoice() !== null) {
					$selected = [];

					/** @var int $option */
					foreach ($answer->getChoice() as $option) {
						$selected[$option] = $options[$option];
					}
				}

				$form[$questionId] = (new DynamicMultiSelect($question->getQuestion(), $selected ?? null))
					->setHtmlAttribute('data-ajax-search');
			}

			$form[$questionId]->setDefaultValue($answer === null
				? null
				: $answer->getChoice()
			);

		} else {
			if ($count <= self::DROPDOWN_THRESHOLD_LOW) {
				$form->addRadioList($questionId, $question->getQuestion(), $options);
			} elseif ($count > self::DROPDOWN_THRESHOLD_LOW && $count < self::DROPDOWN_THRESHOLD_HIGH) {
				$form[$questionId] = new DynamicSelect($question->getQuestion(), $options);
			} else {
				if ($answer !== null) {
					$selected = [];
					$selected[$answer->getChoice()[0]] = $options[$answer->getChoice()[0]];
				}

				$form[$questionId] = (new DynamicSelect($question->getQuestion(), $selected ?? null))
					->setHtmlAttribute('data-ajax-search');
			}

			$form[$questionId]->setDefaultValue($answer === null || !isset($answer->getChoice()[0])
				? null
				: $answer->getChoice()[0]
			);
		}

		$form = $this->addParentRule($form, $question);

		return $form;
	}
}
