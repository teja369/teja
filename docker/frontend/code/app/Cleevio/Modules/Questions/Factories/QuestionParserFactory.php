<?php

declare(strict_types=1);

namespace Cleevio\Questions\Factories;

use Cleevio\Questions\Parsers\BoolQuestionParser;
use Cleevio\Questions\Parsers\ChoiceQuestionParser;
use Cleevio\Questions\Parsers\CountryQuestionParser;
use Cleevio\Questions\Parsers\DateTimeQuestionParser;
use Cleevio\Questions\Parsers\IQuestionAnswerParser;
use Cleevio\Questions\Parsers\NumberQuestionParser;
use Cleevio\Questions\Parsers\TextQuestionParser;
use InvalidArgumentException;

class QuestionParserFactory implements IQuestionParserFactory
{

	/**
	 * @param string $type
	 * @return IQuestionAnswerParser
	 */
	public function getBuilder(string $type): IQuestionAnswerParser
	{
		switch ($type) {
			case 'text':
				return new TextQuestionParser;
			case 'number':
				return new NumberQuestionParser;
			case 'bool':
				return new BoolQuestionParser;
			case 'choice':
				return new ChoiceQuestionParser;
			case 'datetime':
				return new DateTimeQuestionParser;
			case 'country':
				return new CountryQuestionParser;
		}

		throw new InvalidArgumentException;
	}
}
