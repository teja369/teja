<?php

declare(strict_types=1);

namespace Cleevio\Questions\Factories;

use Cleevio\Questions\Parsers\IQuestionAnswerParser;

interface IQuestionParserFactory
{

	/**
	 * @param string $type
	 * @return IQuestionAnswerParser
	 */
	public function getBuilder(string $type): IQuestionAnswerParser;
}
