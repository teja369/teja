<?php

declare(strict_types=1);

namespace Cleevio\Questions\Factories;

use Cleevio\Questions\Builders\IBoolQuestionFormBuilder;
use Cleevio\Questions\Builders\IChoiceQuestionFormBuilder;
use Cleevio\Questions\Builders\ICountryQuestionFormBuilder;
use Cleevio\Questions\Builders\IDateTimeQuestionFormBuilder;
use Cleevio\Questions\Builders\INumberQuestionFormBuilder;
use Cleevio\Questions\Builders\IQuestionFormBuilder;
use Cleevio\Questions\Builders\ITextQuestionFormBuilder;
use InvalidArgumentException;

class QuestionBuilderFactory implements IQuestionBuilderFactory
{

	/**
	 * @var IBoolQuestionFormBuilder
	 */
	private $boolQuestionFormBuilder;

	/**
	 * @var IChoiceQuestionFormBuilder
	 */
	private $choiceQuestionFormBuilder;

	/**
	 * @var IDateTimeQuestionFormBuilder
	 */
	private $dateTimeQuestionFormBuilder;

	/**
	 * @var INumberQuestionFormBuilder
	 */
	private $numberQuestionFormBuilder;

	/**
	 * @var ITextQuestionFormBuilder
	 */
	private $textQuestionBuilder;

	/**
	 * @var ICountryQuestionFormBuilder
	 */
	private $countryQuestionFormBuilder;


	public function __construct(
		IBoolQuestionFormBuilder $boolQuestionFormBuilder,
		IChoiceQuestionFormBuilder $choiceQuestionFormBuilder,
		IDateTimeQuestionFormBuilder $dateTimeQuestionFormBuilder,
		INumberQuestionFormBuilder $numberQuestionFormBuilder,
		ITextQuestionFormBuilder $textQuestionBuilder,
		ICountryQuestionFormBuilder $countryQuestionFormBuilder
	)
	{
		$this->boolQuestionFormBuilder = $boolQuestionFormBuilder;
		$this->choiceQuestionFormBuilder = $choiceQuestionFormBuilder;
		$this->dateTimeQuestionFormBuilder = $dateTimeQuestionFormBuilder;
		$this->numberQuestionFormBuilder = $numberQuestionFormBuilder;
		$this->textQuestionBuilder = $textQuestionBuilder;
		$this->countryQuestionFormBuilder = $countryQuestionFormBuilder;
	}


	/**
	 * @param string $type
	 * @return IQuestionFormBuilder
	 */
	public function getBuilder(string $type): IQuestionFormBuilder
	{
		switch ($type) {
			case 'text':
				return $this->textQuestionBuilder;
			case 'number':
				return $this->numberQuestionFormBuilder;
			case 'bool':
				return $this->boolQuestionFormBuilder;
			case 'choice':
				return $this->choiceQuestionFormBuilder;
			case 'datetime':
				return $this->dateTimeQuestionFormBuilder;
			case 'country':
				return $this->countryQuestionFormBuilder;
		}

		throw new InvalidArgumentException;
	}
}
