<?php

declare(strict_types=1);

namespace Cleevio\Questions\Factories;

use Cleevio\Questions\Builders\IQuestionFormBuilder;

interface IQuestionBuilderFactory
{

	/**
	 * @param string $type
	 * @return IQuestionFormBuilder
	 */
	public function getBuilder(string $type): IQuestionFormBuilder;
}
