<?php

declare(strict_types=1);

namespace Cleevio\Questions\DI;

use Cleevio\Modules\Providers\IPresenterMappingProvider;
use Cleevio\Modules\Providers\IRouterProvider;
use Cleevio\Questions\Router\RouterFactory;
use Nette\DI\CompilerExtension;

class QuestionsExtension extends CompilerExtension implements IPresenterMappingProvider, IRouterProvider
{

	public function loadConfiguration()
	{
		if (defined('NETTE_TESTER')) {
			$this->compiler->loadConfig(__DIR__ . '/services.test.neon');
		} else {
			$this->compiler->loadConfig(__DIR__ . '/services.neon');
		}
	}

	public function getPresenterMapping(): array
	{
		return ['Questions' => 'Cleevio\\Questions\\*Module\\Presenters\\*Presenter'];
	}


	public function getRouterSettings(): array
	{
		return [100 => RouterFactory::class];
	}
}
