<?php

declare(strict_types=1);

namespace Cleevio\Questions\Parsers;

use Cleevio\Questions\API\Responses\QuestionResponse;
use Cleevio\UI\BaseForm;

class ChoiceQuestionParser implements IQuestionAnswerParser
{

	/**
	 * @param BaseForm         $form
	 * @param QuestionResponse $question
	 * @return array
	 */
	public function parse(BaseForm $form, QuestionResponse $question): array
	{
		$values = $form->getValues();

		if ($values->offsetGet($question->getId()) === null ||
			is_array($values->offsetGet($question->getId())) &&
			count($values->offsetGet($question->getId())) === 0
		) {
			return ['id' => $question->getId(), 'choice' => null];
		}

		return ['id' => $question->getId(), 'choice' => is_array($values->offsetGet($question->getId()))
			? $values->offsetGet($question->getId())
			: [$values->offsetGet($question->getId())],
		];
	}
}
