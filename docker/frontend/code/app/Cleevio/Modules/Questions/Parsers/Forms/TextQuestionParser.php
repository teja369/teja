<?php

declare(strict_types=1);

namespace Cleevio\Questions\Parsers;

use Cleevio\Questions\API\Responses\QuestionResponse;
use Cleevio\UI\BaseForm;

class TextQuestionParser implements IQuestionAnswerParser
{

	/**
	 * @param BaseForm         $form
	 * @param QuestionResponse $question
	 * @return array
	 */
	public function parse(BaseForm $form, QuestionResponse $question): array
	{
		$values = $form->getValues();

		if ($values->offsetGet($question->getId()) === null) {
			return ['id' => $question->getId(), 'text' => null];
		}

		return ['id' => $question->getId(), 'text' => $values->offsetGet($question->getId())];
	}
}
