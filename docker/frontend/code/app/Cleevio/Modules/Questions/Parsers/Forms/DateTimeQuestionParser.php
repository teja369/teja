<?php

declare(strict_types=1);

namespace Cleevio\Questions\Parsers;

use Cleevio\Questions\API\Responses\QuestionResponse;
use Cleevio\UI\BaseForm;
use InvalidArgumentException;

class DateTimeQuestionParser implements IQuestionAnswerParser
{

	/**
	 * @param BaseForm         $form
	 * @param QuestionResponse $question
	 * @return array
	 */
	public function parse(BaseForm $form, QuestionResponse $question): array
	{
		$values = $form->getValues();

		if ($values->offsetGet($question->getId()) === null || $values->offsetGet($question->getId()) === '') {
			return ['id' => $question->getId(), 'datetime' => null];
		}

		if ($question->getDatetime() === null) {
			throw new InvalidArgumentException;
		}

		switch ($question->getDatetime()->getVariant()) {
			case 'datetime':
				$datetime = date('Y-m-d H:i:s', strtotime($values->offsetGet($question->getId())));
				$datetime = implode(', ', explode(' ', $datetime));

				break;
			case 'date':
				$datetime = date('Y-m-d', strtotime($values->offsetGet($question->getId())));

				break;
			case 'time':
				$datetime = date('H:i:s', strtotime($values->offsetGet($question->getId())));

				break;
			default:
				$datetime = date('Y-m-d', strtotime($values->offsetGet($question->getId())));
		}

		return ['id' => $question->getId(), 'datetime' => $datetime ?? null];
	}
}
