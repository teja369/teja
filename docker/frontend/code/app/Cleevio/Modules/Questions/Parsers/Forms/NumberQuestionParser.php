<?php

declare(strict_types=1);

namespace Cleevio\Questions\Parsers;

use Cleevio\Questions\API\Responses\QuestionResponse;
use Cleevio\UI\BaseForm;

class NumberQuestionParser implements IQuestionAnswerParser
{

	/**
	 * @param BaseForm         $form
	 * @param QuestionResponse $question
	 * @return array
	 */
	public function parse(BaseForm $form, QuestionResponse $question): array
	{
		$values = $form->getValues();

		if ($values->offsetGet($question->getId()) === null || !is_numeric($values->offsetGet($question->getId()))) {
			return ['id' => $question->getId(), 'number' => null];
		}

		return ['id' => $question->getId(), 'number' => $values->offsetGet($question->getId())];
	}
}
