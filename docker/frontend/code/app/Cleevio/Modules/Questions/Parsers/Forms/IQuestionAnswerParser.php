<?php

declare(strict_types=1);

namespace Cleevio\Questions\Parsers;

use Cleevio\Questions\API\Responses\QuestionResponse;
use Cleevio\UI\BaseForm;

interface IQuestionAnswerParser
{

	/**
	 * @param BaseForm         $form
	 * @param QuestionResponse $question
	 * @return array
	 */
	public function parse(BaseForm $form, QuestionResponse $question): array;
}
