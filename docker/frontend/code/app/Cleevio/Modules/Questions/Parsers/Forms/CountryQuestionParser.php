<?php

declare(strict_types=1);

namespace Cleevio\Questions\Parsers;

use Cleevio\Questions\API\Responses\QuestionResponse;
use Cleevio\UI\BaseForm;

class CountryQuestionParser implements IQuestionAnswerParser
{

	/**
	 * @param BaseForm         $form
	 * @param QuestionResponse $question
	 * @return array
	 */
	public function parse(BaseForm $form, QuestionResponse $question): array
	{
		$values = $form->getValues();

		if ($values->offsetGet($question->getId()) === null || $values->offsetGet($question->getId()) === '') {
			return ['id' => $question->getId(), 'country' => null];
		}

		return ['id' => $question->getId(), 'country' => $values->offsetGet($question->getId())];
	}
}
