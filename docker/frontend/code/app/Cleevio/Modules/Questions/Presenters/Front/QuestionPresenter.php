<?php

declare(strict_types=1);

namespace Cleevio\Questions\FrontModule\Presenters;

use Cleevio\ApiClient\Exception\ApiException;
use Cleevio\FrontModule\Presenters\BasePresenter;
use Cleevio\Questions\API\Requests\QuestionOptionGetRequest;
use Cleevio\Questions\API\Responses\OptionResponse;
use Nette\Application\AbortException;

class QuestionPresenter extends BasePresenter
{

	/**
	 * @var QuestionOptionGetRequest
	 */
	private $optionsRequest;


	/**
	 * QuestionPresenter constructor.
	 * @param QuestionOptionGetRequest $optionsRequest
	 */
	public function __construct(QuestionOptionGetRequest $optionsRequest)
	{
		parent::__construct();

		$this->optionsRequest = $optionsRequest;
	}


	/**
	 * @param string $q
	 * @param int $questionId
	 * @param int $tripId
	 * @param int $p
	 * @throws AbortException
	 * @throws ApiException
	 */
	public function actionSearchOption(string $q, int $questionId, int $tripId, int $p = 1): void
	{
		$this->optionsRequest->setQuestionId($questionId);
		$this->optionsRequest->setTripId($tripId);

		if ($q !== '') {
			$this->optionsRequest->setSearch($q);
		}

		$this->optionsRequest->setLimit(100);
		$this->optionsRequest->setPage($p-1);
		$response = $this->optionsRequest->execute();

		$result = array_map(static function (OptionResponse $option) {
			return ['value' => $option->getId(), 'text' => $option->getText()];
		}, $response->getItems());


		$this->sendJson($result ?? []);
	}


	/**
	 * @return string
	 */
	protected function getLayoutName(): string
	{
		return 'layout-trip';
	}
}
