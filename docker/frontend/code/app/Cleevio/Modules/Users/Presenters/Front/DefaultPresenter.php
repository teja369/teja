<?php

declare(strict_types=1);

namespace Cleevio\Users\FrontModule\Presenters;

use Cleevio\FrontModule\Presenters\BasePresenter;

class DefaultPresenter extends BasePresenter
{
	/**
	 * @return string
	 */
	protected function getLayoutName(): string
	{
		return 'layout';
	}
}
