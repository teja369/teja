<?php

declare(strict_types=1);

namespace Cleevio\Users\FrontModule\Presenters;

use Cleevio\API\Context;
use Cleevio\ApiClient\Exception\ApiException;
use Cleevio\FrontModule\Presenters\BasePresenter;
use Cleevio\Sso\SsoContext;
use Cleevio\Users\Api\Requests\RefreshRequest;
use Cleevio\Users\Api\Requests\SsoLoginRequest;
use Cleevio\Users\Api\Requests\UserRequest;
use Cleevio\Users\Components\LoginForm\ILoginFormFactory;
use Cleevio\Users\Components\LoginForm\LoginForm;
use Nette\Application\AbortException;
use Nette\Security\AuthenticationException;

class LoginPresenter extends BasePresenter
{

	/**
	 * @var Context
	 */
	private $context;

	/**
	 * @var SsoLoginRequest
	 */
	private $ssoLoginRequest;

	/**
	 * @var UserRequest
	 */
	private $userRequest;
	/**
	 * @var RefreshRequest
	 */
	private $refreshRequest;

	/**
	 * @param SsoLoginRequest $ssoLoginRequest
	 * @param RefreshRequest $refreshRequest
	 * @param UserRequest $userRequest
	 * @param SsoContext $ssoContext
	 * @param Context $context
	 */
	public function __construct(SsoLoginRequest $ssoLoginRequest,
								RefreshRequest $refreshRequest,
								UserRequest $userRequest,
								SsoContext $ssoContext,
								Context $context)
	{
		parent::__construct();

		$this->ssoLoginRequest = $ssoLoginRequest;
		$this->context = $context;
		$this->userRequest = $userRequest;
		$this->ssoContext = $ssoContext;
		$this->refreshRequest = $refreshRequest;
	}

	/**
	 * @param string $accessToken
	 * @throws AbortException
	 * @throws AuthenticationException
	 */
	function actionSso(string $accessToken): void
	{
		try {
			$this->ssoLoginRequest->setHeader('Authorization', sprintf('Bearer %s', $accessToken));
			$authorization = $this->ssoLoginRequest->execute();

			$this->context->setAuthorization($authorization);

			// Retrieve user information
			$user = $this->userRequest->execute();
			$this->context->setSubject($user);

			if ($user->getLanguage() !== null) {
				$this->context->setLocale($user->getLanguage());
			}

			$this->getUser()->login($user->getUsername());

			$this->redirect(':Front:Homepage:default');

		} catch (ApiException $e) {
			if ($this->ssoContext->getUri() !== null) {
				$this->redirectUrl($this->ssoContext->getUri());
			}

			$this->flashMessage($e->getMessage(), "danger");
			$this->redirect(':Front:Homepage:default');
		}
	}

	/**
	 * @param string $refreshToken
	 * @throws AbortException
	 * @throws AuthenticationException
	 */
	function actionToken(string $refreshToken): void
	{
		try {
			$this->refreshRequest->setHeader('Authorization', sprintf('Bearer %s', $refreshToken));
			$authorization = $this->refreshRequest->execute();

			$this->context->setAuthorization($authorization);

			// Retrieve user information
			$user = $this->userRequest->execute();
			$this->context->setSubject($user);

			if ($user->getLanguage() !== null) {
				$this->context->setLocale($user->getLanguage());
			}

			$this->getUser()->login($user->getUsername());

			$this->redirect(':Front:Homepage:default');

		} catch (ApiException $e) {
			if ($this->ssoContext->getUri() !== null) {
				$this->redirectUrl($this->ssoContext->getUri());
			}

			$this->flashMessage($e->getMessage(), "danger");
			$this->redirect(':Front:Homepage:default');
		}
	}

	/**
	 * @param ILoginFormFactory $factory
	 * @return LoginForm
	 */
	protected function createComponentLoginForm(ILoginFormFactory $factory): LoginForm
	{
		$control = $factory->create();

		$control->onSuccess[] = function () {
			if ($this->isAjax()) {
				$this->payload->forceRedirect = true;
				$this->redirect(':Front:Homepage:default');
			} else {
				$this->redirect(':Front:Homepage:default');
			}
		};

		return $control;
	}

	/**
	 * @return string
	 */
	protected function getLayoutName(): string
	{
		return 'layout-blank';
	}
}
