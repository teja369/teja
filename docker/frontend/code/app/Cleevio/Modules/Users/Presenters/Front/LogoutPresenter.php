<?php

declare(strict_types=1);

namespace Cleevio\Users\FrontModule\Presenters;

use Cleevio\API\Context;
use Cleevio\FrontModule\Presenters\BasePresenter;
use Nette\Application\AbortException;

class LogoutPresenter extends BasePresenter
{

	/**
	 * @var Context
	 */
	private $context;

	/**
	 * @param Context $context
	 */
	public function __construct(Context $context)
	{
		parent::__construct();

		$this->context = $context;
	}

	/**
	 * @throws AbortException
	 */
	function actionSso(): void
	{
		$this->context->clear();

		$this->redirectUrl(sprintf("%s#LOGOUT", $this->ssoContext->getUri()));
	}

	/**
	 * @return string
	 */
	protected function getLayoutName(): string
	{
		return 'layout-blank';
	}
}
