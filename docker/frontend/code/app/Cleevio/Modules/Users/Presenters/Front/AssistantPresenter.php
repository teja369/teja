<?php

declare(strict_types=1);

namespace Cleevio\Users\FrontModule\Presenters;

use Cleevio\ApiClient\Exception\ApiException;
use Cleevio\FrontModule\Presenters\DashboardPresenter;
use Cleevio\Trips\API\Requests\TripsGetRequest;
use Cleevio\UI\ApiPaginationControl\IApiPaginationControlFactory;
use Cleevio\Users\Api\Requests\AssistantUsersGetRequest;
use Cleevio\Users\Api\Requests\UserAssistantsGetRequest;
use Cleevio\Users\Components\AssistantsTable\AssistantsTable;
use Cleevio\Users\Components\AssistantsTable\IAssistantsTable;
use Cleevio\Users\Components\RequestAssistantControl\IRequestAssistantControl;
use Cleevio\Users\Components\RequestAssistantControl\RequestAssistantControl;
use Nette\Application\BadRequestException;

class AssistantPresenter extends DashboardPresenter
{

	/**
	 * @var UserAssistantsGetRequest
	 */
	private $userAssistantsGetRequest;

	/**
	 * @var AssistantUsersGetRequest
	 */
	private $assistantUsersGetRequest;


	public function __construct(
		TripsGetRequest $tripsGetRequest,
		UserAssistantsGetRequest $userAssistantsGetRequest,
		AssistantUsersGetRequest $assistantUsersGetRequest
	)
	{
		parent::__construct($tripsGetRequest);
		$this->userAssistantsGetRequest = $userAssistantsGetRequest;
		$this->assistantUsersGetRequest = $assistantUsersGetRequest;
	}


	/**
	 * @throws BadRequestException
	 */
	public function actionDefault(): void
	{
		try {
			$this->userAssistantsGetRequest->setLimit(2);
			$this->assistantUsersGetRequest->setLimit(2);
			$assistants = $this->userAssistantsGetRequest->execute();
			$travellers = $this->assistantUsersGetRequest->execute();

			$this->template->assistants = $assistants->getItems();
			$this->template->travellers = $travellers->getItems();
			$this->template->requests = [];
		} catch (ApiException $e) {
			$this->error($e->getMessage(), $e->getCode());
		}
	}

	public function actionAssistants(): void
	{
		$this['assistantsTable']->setRequest($this->userAssistantsGetRequest);

		if ($this->isAjax()) {
			$this['assistantsTable']->redrawControl('users');
		}
	}

	public function actionTravellers(): void
	{
		$this['assistantsTable']->setRequest($this->assistantUsersGetRequest);

		if ($this->isAjax()) {
			$this['assistantsTable']->redrawControl('users');
		}
	}

	public function actionRequests(): void
	{
		$this['assistantsTable']->setRequest($this->assistantUsersGetRequest);

		if ($this->isAjax()) {
			$this['assistantsTable']->redrawControl('users');
		}
	}


	protected function createComponentRequestAssistantControl(IRequestAssistantControl $factory): RequestAssistantControl
	{
		return $factory->create();
	}

	protected function createComponentAssistantsTable(
		IAssistantsTable $factory,
		IApiPaginationControlFactory $apiPaginationControlFactory
	): AssistantsTable
	{
		$control = $factory->create();

		$control->addComponent($apiPaginationControlFactory->create(), 'apiPagination');

		return $control;
	}
}
