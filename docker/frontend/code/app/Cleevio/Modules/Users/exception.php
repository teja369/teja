<?php

declare(strict_types=1);

namespace Cleevio\Users;

use Exception;

class UserNotFound extends Exception
{

}

class InvalidPassword extends Exception
{

}
