<?php

declare(strict_types=1);

namespace Cleevio\Users\DI;

use Cleevio\Modules\Providers\IPresenterMappingProvider;
use Cleevio\Modules\Providers\IRouterProvider;
use Cleevio\Users\Router\RouterFactory;
use Nette\DI\CompilerExtension;

class UsersExtension extends CompilerExtension implements IPresenterMappingProvider, IRouterProvider
{

	public function loadConfiguration()
	{
		$this->compiler->loadConfig(__DIR__ . '/services.neon');
	}


	public function getPresenterMapping(): array
	{
		return ['Users' => 'Cleevio\\Users\\*Module\\Presenters\\*Presenter'];
	}

	public function getRouterSettings(): array
	{
		return [100 => RouterFactory::class];
	}
}
