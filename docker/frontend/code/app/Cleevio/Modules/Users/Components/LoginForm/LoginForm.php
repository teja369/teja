<?php

declare(strict_types=1);

namespace Cleevio\Users\Components\LoginForm;

use Cleevio\API\Context;
use Cleevio\ApiClient\Exception\ApiException;
use Cleevio\ApiClient\Exception\BadRequestException;
use Cleevio\ApiClient\Exception\UnauthorizedException;
use Cleevio\UI\BaseControl;
use Cleevio\UI\BaseForm;
use Cleevio\Users\Api\Requests\LoginRequest;
use Cleevio\Users\Api\Requests\UserRequest;
use Nette\Localization\ITranslator;
use Nette\Security\User;

class LoginForm extends BaseControl
{

	/**
	 * @var array
	 */
	public $onSuccess = [];


	/**
	 * @var ITranslator
	 */
	private $translator;

	/**
	 * @var LoginRequest
	 */
	private $loginRequest;

	/**
	 * @var UserRequest
	 */
	private $userRequest;

	/**
	 * @var Context
	 */
	private $context;

	/**
	 * @var User
	 */
	private $user;

	/**
	 * @param ITranslator $translator
	 * @param LoginRequest $loginRequest
	 * @param UserRequest $userRequest
	 * @param User $user
	 * @param Context $context
	 */
	public function __construct(
		ITranslator $translator,
		LoginRequest $loginRequest,
		UserRequest $userRequest,
		User $user,
		Context $context
	)
	{
		$this->translator = $translator;
		$this->loginRequest = $loginRequest;
		$this->userRequest = $userRequest;
		$this->context = $context;
		$this->user = $user;
	}


	public function render(): void
	{
		$this->template->render(__DIR__ . '/templates/default.latte');
	}


	protected function createComponentForm(): BaseForm
	{
		$form = new BaseForm;
		$form->setTranslator($this->translator);

		$form->addText('username', 'frontend.users.login.default.sign-in.form.label.username')
			->setRequired('frontend.users.login.default.sign-in.form.validation.required.username');

		$form->addPassword('password', 'frontend.users.login.default.sign-in.form.label.password')
			->setRequired('frontend.users.login.default.sign-in.form.validation.required.password');

		$form->addSubmit('send');

		$form->onSuccess[] = function (BaseForm $form) {
			$values = $form->getValues();

			try {

				// Login user
				$this->loginRequest->setBasicAuth($values->username, $values->password);
				$authorization = $this->loginRequest->execute();
				$this->context->setAuthorization($authorization);

				// Retrieve user information
				$user = $this->userRequest->execute();
				$this->context->setSubject($user);

				if ($user->getLanguage() !== null) {
					$this->context->setLocale($user->getLanguage());
				}

				$this->user->login($user->getUsername());

				$this->onSuccess();

			} catch (UnauthorizedException | BadRequestException $e) {
				$this->context->clear();
				$this->flashMessage($this->translator->translate('frontend.users.login.default.sign-in.form.error.bad-credentials'), 'danger');
			} catch (ApiException $e) {
				$this->context->clear();
				$this->flashMessage($this->translator->translate('frontend.users.login.default.sign-in.form.error.unknown'), 'danger');
			}

			if ($this->isAjax()) {
				$this->redrawControl('flashes');
			}
		};

		return $form;
	}
}

interface ILoginFormFactory
{

	/**
	 * @return LoginForm
	 */
	function create();
}
