<?php

declare(strict_types=1);

namespace Cleevio\Users\Components\UserInfoBox;

use Cleevio\API\Context;
use Cleevio\UI\BaseControl;

class UserInfoBox extends BaseControl
{

	/**
	 * @var array
	 */
	public $onLoggedOut = [];

	/**
	 * @var Context
	 */
	private $context;


	/**
	 * @param Context $context API context
	 */
	public function __construct(Context $context)
	{
		$this->context = $context;
	}


	public function render(string $type): void
	{
		$this->template->authorized = $this->context->getAuthorization() !== null;
		$this->template->user = $this->context->getSubject();
		$this->template->language = $this->context->getLocale();
		$this->template->type = $type;
		$this->template->render(sprintf(__DIR__ . '/templates/%s.latte', $type));
	}


	public function handleLogout(): void
	{
		$this->context->clear();
		$this->onLoggedOut();
	}

}

interface IUserInfoBoxFactory
{

	/**
	 * @return UserInfoBox
	 */
	function create();
}
