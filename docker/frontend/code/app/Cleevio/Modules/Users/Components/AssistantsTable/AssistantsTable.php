<?php

declare(strict_types=1);

namespace Cleevio\Users\Components\AssistantsTable;

use Cleevio\ApiClient\Requests\PaginationRequest;
use Cleevio\UI\BaseControl;

class AssistantsTable extends BaseControl
{

	/**
	 * @var array
	 */
	public $onSuccess = [];

	/**
	 * @var PaginationRequest
	 */
	private $request;


	public function render(?array $users = null): void
	{
		$this['apiPagination']->setAjax(true);

		if ($users === null) {
			$this['apiPagination']->setRequest($this->request);
		}

		$this->template->users = $users ?? $this['apiPagination']->getItems()->getItems();
		$this->template->render(__DIR__ . '/templates/default.latte');
	}


	public function setRequest(PaginationRequest $request): void
	{
		$this->request = $request;
	}
}

interface IAssistantsTable
{

	/**
	 * @return AssistantsTable
	 */
	function create();
}
