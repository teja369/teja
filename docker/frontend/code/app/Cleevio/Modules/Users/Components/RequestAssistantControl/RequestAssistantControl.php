<?php

declare(strict_types=1);

namespace Cleevio\Users\Components\RequestAssistantControl;

use Cleevio\UI\BaseControl;
use Cleevio\UI\BaseForm;
use Nette\Localization\ITranslator;

class RequestAssistantControl extends BaseControl
{

	/**
	 * @var array
	 */
	public $onSuccess = [];

	/**
	 * @var ITranslator
	 */
	private $translator;


	/**
	 * RequestAssistantControl constructor.
	 * @param ITranslator $translator
	 */
	public function __construct(ITranslator $translator)
	{
		$this->translator = $translator;
	}


	public function render(): void
	{
		$this->template->render(__DIR__ . '/templates/default.latte');
	}

	protected function createComponentRequestAssistantForm(): BaseForm
	{
		$form = new BaseForm;

		$form->setTranslator($this->translator);

		$form->addEmail('email', 'frontend.users.request-assistant.email.label')
			->setHtmlAttribute('placeholder', 'frontend.users.request-assistant.email.label')
			->setRequired();

		$form->addSubmit('send', 'frontend.users.request-assistant.submit.label');

		$form->onSuccess[] = function (BaseForm $form) {
			unset($form);
			$this->onSuccess();
		};

		return $form;
	}

}

interface IRequestAssistantControl
{

	/**
	 * @return RequestAssistantControl
	 */
	function create();
}
