<?php

declare(strict_types=1);

namespace Cleevio\Users\Api\Requests;

use Cleevio\ApiClient\Exception\ApiException;
use Cleevio\ApiClient\Requests\PaginationRequest;
use Cleevio\Users\Api\Responses\UsersResponse;

class UserAssistantsGetRequest extends PaginationRequest implements IUserAssistantsGetRequest
{
	/**
	 * Endpoint path
	 * @return string
	 */
	protected function path(): string
	{
		$path = '/user/assistant';

		$params = [
			'limit' => $this->getLimit(),
			'page' => $this->getPage(),
		];

		$query = http_build_query($params);

		return strlen($query) > 0
			? $path . '?' . $query
			: $path;
	}


	/**
	 * Method
	 * @return string
	 */
	protected function method(): string
	{
		return 'GET';
	}


	/**
	 * Determines whether request requires authorization
	 * @return bool
	 */
	function requiresAuth(): bool
	{
		return true;
	}


	/**
	 * Execute request and return a response
	 * @return UsersResponse
	 * @throws ApiException
	 */
	function execute(): UsersResponse
	{
		return new UsersResponse($this->request());
	}
}
