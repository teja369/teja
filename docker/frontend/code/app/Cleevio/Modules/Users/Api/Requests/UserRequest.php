<?php

declare(strict_types=1);

namespace Cleevio\Users\Api\Requests;

use Cleevio\API\BaseRequest;
use Cleevio\ApiClient\Exception\ApiException;
use Cleevio\Users\Api\Responses\UserResponse;

class UserRequest extends BaseRequest implements IUserRequest
{

	/**
	 * Endpoint path
	 * @return string
	 */
	protected function path(): string
	{
		return "/user";
	}

	/**
	 * Method
	 * @return string
	 */
	protected function method(): string
	{
		return "GET";
	}

	/**
	 * Determines whether request requires authorization
	 * @return bool
	 */
	function requiresAuth(): bool
	{
		return true;
	}

	/**
	 * Execute request and return a response
	 * @return UserResponse
	 * @throws ApiException
	 */
	function execute(): UserResponse
	{
		return new UserResponse($this->request());
	}
}
