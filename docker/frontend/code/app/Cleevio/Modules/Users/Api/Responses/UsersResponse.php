<?php

declare(strict_types=1);

namespace Cleevio\Users\Api\Responses;

use Cleevio\ApiClient\DTO\PaginationResponse;

class UsersResponse extends PaginationResponse
{

	/**
	 * @var UserResponse[]
	 */
	private $items;

	/**
	 * TripsResponse constructor.
	 * @param array $response
	 */
	public function __construct(array $response)
	{
		parent::__construct($response['data']);

		$this->items = array_map(static function ($user) {
			return new UserResponse($user);
		}, $response['data']['items']);

	}

	/**
	 * @return UserResponse[]
	 */
	public function getItems(): array
	{
		return $this->items;
	}
}
