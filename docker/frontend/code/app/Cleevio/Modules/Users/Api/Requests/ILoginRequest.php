<?php

declare(strict_types=1);

namespace Cleevio\Users\Api\Requests;

interface ILoginRequest
{
	/**
	 *  Set basic auth headers for login
	 * @param string $username
	 * @param string $password
	 */
	public function setBasicAuth(string $username, string $password): void;

}
