<?php

declare(strict_types=1);

namespace Cleevio\Users\Api\Requests;

use Cleevio\API\BaseRequest;
use Cleevio\ApiClient\DTO\Authorization;
use Cleevio\ApiClient\Exception\ApiException;
use Cleevio\Users\Api\Responses\LoginResponse;

class RefreshRequest extends BaseRequest implements IRefreshRequest
{

	/**
	 * Endpoint path
	 * @return string
	 */
	protected function path(): string
	{
		return "/auth/refresh";
	}

	/**
	 * Method
	 * @return string
	 */
	protected function method(): string
	{
		return "POST";
	}

	/**
	 * Determines whether request requires authorization
	 * @return bool
	 */
	function requiresAuth(): bool
	{
		return false;
	}

	/**
	 * Execute request and return a response
	 * @return LoginResponse
	 * @throws ApiException
	 */
	function execute(): Authorization
	{
		return new LoginResponse($this->request());
	}
}
