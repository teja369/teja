<?php

declare(strict_types=1);

namespace Cleevio\Users\Api\Responses;

use Cleevio\ApiClient\DTO\Subject;
use Cleevio\ApiClient\IResponse;
use Cleevio\Translations\API\Responses\LanguageResponse;

class UserResponse extends Subject implements IResponse
{
	/**
	 * @var int
	 */
	private $id;

	/**
	 * @var string
	 */
	private $username;

	/**
	 * @var string|null
	 */
	private $email;

	/**
	 * @var string|null
	 */
	private $name;

	/**
	 * @var LanguageResponse|null
	 */
	private $language;

	/**
	 * @param array $user
	 */
	public function __construct(array $user)
	{
		if (array_key_exists('id', $user) &&
			array_key_exists('username', $user) &&
			array_key_exists('name', $user) &&
			array_key_exists('email', $user) &&
			array_key_exists('role', $user) &&
			array_key_exists('language', $user)) {
			$this->id = $user['id'];
			$this->username = $user['username'];
			$this->name = $user['name'];
			$this->email = $user['email'];
			$this->language = $user['language'] !== null
				? new LanguageResponse($user['language'])
				: null;
		} else {
			throw new \InvalidArgumentException;
		}
	}

	public function getId(): int
	{
		return $this->id;
	}

	public function getUsername(): string
	{
		return $this->username;
	}

	public function getEmail(): ?string
	{
		return $this->email;
	}

	public function getName(): ?string
	{
		return $this->name;
	}

	public function getRole(): string
	{
		return "";
	}

	/**
	 * @return LanguageResponse|null
	 */
	public function getLanguage(): ?LanguageResponse
	{
		return $this->language;
	}

}
