<?php

declare(strict_types=1);

namespace Cleevio\Users\Api\Responses;

use Cleevio\ApiClient\DTO\Authorization;

class LoginResponse extends Authorization
{

	/**
	 * @param array $user
	 */
	public function __construct(array $user)
	{
		if (array_key_exists('accessToken', $user) &&
			array_key_exists('refreshToken', $user) &&
			array_key_exists('expires', $user)) {
			parent::__construct($user['accessToken'], $user['refreshToken'], (int) $user['expires']);
		} else {
			throw new \InvalidArgumentException;
		}
	}

}
