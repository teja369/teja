<?php

declare(strict_types=1);

namespace Cleevio\Users\Api\Requests;

use Cleevio\API\BaseRequest;
use Cleevio\ApiClient\DTO\Authorization;
use Cleevio\ApiClient\Exception\ApiException;
use Cleevio\Users\Api\Responses\LoginResponse;

class LoginRequest extends BaseRequest implements ILoginRequest
{

	/**
	 * Endpoint path
	 * @return string
	 */
	protected function path(): string
	{
		return "/auth/login";
	}

	/**
	 * Method
	 * @return string
	 */
	protected function method(): string
	{
		return "POST";
	}

	/**
	 * Determines whether request requires authorization
	 * @return bool
	 */
	function requiresAuth(): bool
	{
		return false;
	}
	
	/**
	 * @return bool
	 */
	protected function dispatchEvents()
	{
		return false;
	}

	/**
	 *  Set basic auth headers for login
	 * @param string $username
	 * @param string $password
	 */
	public function setBasicAuth(string $username, string $password): void
	{
		$this->setHeader("Authorization", "Basic " . base64_encode($username . ':' . $password));
	}

	/**
	 * Execute request and return a response
	 * @return LoginResponse
	 * @throws ApiException
	 */
	function execute(): Authorization
	{
		return new LoginResponse($this->request());
	}
}
