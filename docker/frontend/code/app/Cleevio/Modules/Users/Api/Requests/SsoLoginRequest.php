<?php

declare(strict_types=1);

namespace Cleevio\Users\Api\Requests;

use Cleevio\API\BaseRequest;
use Cleevio\ApiClient\DTO\Authorization;
use Cleevio\ApiClient\Exception\ApiException;
use Cleevio\Users\Api\Responses\LoginResponse;

class SsoLoginRequest extends BaseRequest implements ISsoLoginRequest
{

	/**
	 * Endpoint path
	 * @return string
	 */
	protected function path(): string
	{
		return "/sso/login";
	}

	/**
	 * Method
	 * @return string
	 */
	protected function method(): string
	{
		return "POST";
	}

	/**
	 * Determines whether request requires authorization
	 * @return bool
	 */
	function requiresAuth(): bool
	{
		return false;
	}

	/**
	 * @return bool
	 */
	protected function dispatchEvents()
	{
		return false;
	}

	/**
	 * Execute request and return a response
	 * @return LoginResponse
	 * @throws ApiException
	 */
	function execute(): Authorization
	{
		return new LoginResponse($this->request());
	}
}
