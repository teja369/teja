<?php

declare(strict_types=1);

namespace Cleevio\Countries\DI;

use Cleevio\Countries\Router\RouterFactory;
use Cleevio\Modules\Providers\IPresenterMappingProvider;
use Cleevio\Modules\Providers\IRouterProvider;
use Nette\DI\CompilerExtension;

class CountriesExtension extends CompilerExtension implements IPresenterMappingProvider, IRouterProvider
{

	public function loadConfiguration()
	{
		if (defined('NETTE_TESTER')) {
			$this->compiler->loadConfig(__DIR__ . '/services.test.neon');
		} else {
			$this->compiler->loadConfig(__DIR__ . '/services.neon');
		}
	}


	public function getPresenterMapping(): array
	{
		return ['Countries' => 'Cleevio\\Countries\\*Module\\Presenters\\*Presenter'];
	}


	public function getRouterSettings(): array
	{
		return [100 => RouterFactory::class];
	}
}
