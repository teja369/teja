<?php

declare(strict_types=1);

namespace Cleevio\Countries\API\Requests;

use Cleevio\ApiClient\Exception\ApiException;
use Cleevio\ApiClient\Requests\PaginationRequest;
use Cleevio\Countries\API\Responses\CountriesPagedResponse;

class CountriesGetRequest extends PaginationRequest implements ICountriesGetRequest
{

	/**
	 * @var string|null
	 */
	private $search;

	/**
	 * @var bool
	 */
	private $showDisabled = false;


	/**
	 * Endpoint path
	 * @return string
	 */
	protected function path(): string
	{

		$path = '/countries';

		$params = [
			'limit' => $this->getLimit(),
			'page' => $this->getPage(),
			'search' => $this->search,
		];

		if ($this->showDisabled) {
			$params['showDisabled'] = true;
		}

		$query = http_build_query($params);

		return strlen($query) > 0
			? $path . '?' . $query
			: $path;
	}


	/**
	 * Method
	 * @return string
	 */
	protected function method(): string
	{
		return 'GET';
	}


	/**
	 * Determines whether request requires authorization
	 * @return bool
	 */
	function requiresAuth(): bool
	{
		return true;
	}


	function setSearch(?string $search = null): void
	{
		$this->search = $search;
	}

	public function setShowDisabled(): void
	{
		$this->showDisabled = true;
	}


	/**
	 * Execute request and return a response
	 * @return CountriesPagedResponse
	 * @throws ApiException
	 */
	function execute(): CountriesPagedResponse
	{
		return new CountriesPagedResponse($this->request());
	}
}
