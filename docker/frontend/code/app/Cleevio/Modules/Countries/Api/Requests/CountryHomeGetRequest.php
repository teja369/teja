<?php

declare(strict_types=1);

namespace Cleevio\Countries\API\Requests;

use Cleevio\API\BaseRequest;
use Cleevio\ApiClient\Exception\ApiException;
use Cleevio\Countries\API\Responses\CountryResponse;

class CountryHomeGetRequest extends BaseRequest implements ICountryHomeGetRequest
{

	/**
	 * Endpoint path
	 * @return string
	 */
	protected function path(): string
	{
		return '/countries/home';
	}


	/**
	 * Method
	 * @return string
	 */
	protected function method(): string
	{
		return 'GET';
	}


	/**
	 * Determines whether request requires authorization
	 * @return bool
	 */
	function requiresAuth(): bool
	{
		return true;
	}

	/**
	 * Execute request and return a response
	 * @return CountryResponse
	 * @throws ApiException
	 */
	function execute(): CountryResponse
	{
		return new CountryResponse($this->request());
	}
}
