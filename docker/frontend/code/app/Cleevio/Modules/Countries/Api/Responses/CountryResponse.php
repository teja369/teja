<?php

declare(strict_types=1);

namespace Cleevio\Countries\API\Responses;

use Cleevio\ApiClient\IResponse;
use InvalidArgumentException;

class CountryResponse implements IResponse
{

	/**
	 * @var string
	 */
	private $code;

	/**
	 * @var string
	 */
	private $name;

	/**
	 * @var bool
	 */
	private $isHome;

	/**
	 * DestinationResponse constructor.
	 * @param array $country
	 */
	public function __construct(array $country)
	{
		if (isset($country['code'], $country['name'], $country['isHome'])) {
			$this->code = $country['code'];
			$this->name = $country['name'];
			$this->isHome = (bool) $country['isHome'];
		} else {
			throw new InvalidArgumentException;
		}
	}

	/**
	 * @return string
	 */
	public function getCode(): string
	{
		return $this->code;
	}


	/**
	 * @return string
	 */
	public function getName(): string
	{
		return $this->name;
	}

	/**
	 * @return bool
	 */
	public function isHome(): bool
	{
		return $this->isHome;
	}
}
