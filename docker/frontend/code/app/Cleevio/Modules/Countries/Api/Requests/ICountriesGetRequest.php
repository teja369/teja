<?php

declare(strict_types=1);

namespace Cleevio\Countries\API\Requests;

interface ICountriesGetRequest
{

	/**
	 * @param string|null $search
	 */
	public function setSearch(?string $search = null): void;


	public function setShowDisabled(): void;
}
