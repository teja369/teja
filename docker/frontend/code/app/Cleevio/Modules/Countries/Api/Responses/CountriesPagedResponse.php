<?php

declare(strict_types=1);

namespace Cleevio\Countries\API\Responses;

use Cleevio\ApiClient\DTO\PaginationResponse;

class CountriesPagedResponse extends PaginationResponse
{
	/**
	 * @var CountryResponse[]
	 */
	private $items = [];


	/**
	 * CountriesPagedResponse constructor.
	 * @param array $response
	 */
	public function __construct(array $response)
	{
		parent::__construct($response['data']);

		$this->items = array_map(static function ($host) {
			return new CountryResponse($host);
		}, $response['data']['items']);
	}

	/**
	 * @return CountryResponse[]
	 */
	public function getItems(): array
	{
		return $this->items;
	}

}
