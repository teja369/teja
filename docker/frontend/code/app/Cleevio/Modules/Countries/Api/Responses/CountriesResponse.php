<?php

declare(strict_types=1);

namespace Cleevio\Countries\API\Responses;

use Cleevio\ApiClient\IResponse;

class CountriesResponse implements IResponse
{
	/**
	 * @var CountryResponse[]
	 */
	private $countries = [];

	/**
	 * CountriesResponse constructor.
	 * @param array $countries
	 */
	public function __construct(array $countries)
	{
		$this->countries = array_map(static function($country) {
			return new CountryResponse($country);
		}, $countries);
	}

	/**
	 * @return CountryResponse[]
	 */
	public function getCountries(): array
	{
		return $this->countries;
	}

}
