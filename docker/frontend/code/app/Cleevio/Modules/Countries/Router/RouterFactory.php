<?php

declare(strict_types=1);

namespace Cleevio\Countries\Router;

use Nette;
use Nette\Application\Routers\Route;
use Nette\Application\Routers\RouteList;

class RouterFactory
{
	/**
	 * @return Nette\Application\IRouter
	 */
	public function createRouter()
	{
		$router = new RouteList;

		$router[] = $countriesModule = new RouteList('Countries');

		$countriesModule[] = new Route('countries/<presenter>/<action>[/<id>]', [
			'presenter' => 'Country',
			'action' => 'default',
			'module' => 'Front',
		]);

		return $router;
	}

}
