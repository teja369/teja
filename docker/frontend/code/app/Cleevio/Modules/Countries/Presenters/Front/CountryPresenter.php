<?php

declare(strict_types=1);

namespace Cleevio\Countries\FrontModule\Presenters;

use Cleevio\ApiClient\Exception\ApiException;
use Cleevio\Countries\API\Requests\CountriesGetRequest;
use Cleevio\FrontModule\Presenters\BasePresenter;
use Nette\Application\AbortException;
use Nette\Application\UI\InvalidLinkException;

class CountryPresenter extends BasePresenter
{

	/**
	 * @var CountriesGetRequest
	 */
	private $countriesGetRequest;


	public function __construct(CountriesGetRequest $countriesGetRequest)
	{
		parent::__construct();
		$this->countriesGetRequest = $countriesGetRequest;
	}


	/**
	 * @param string $q
	 * @param string $type
	 * @param int    $p
	 * @throws AbortException
	 * @throws ApiException
	 * @throws InvalidLinkException
	 */
	public function actionCountrySearch(string $q, string $type, int $p = 1): void
	{
		if ($q !== '') {
			$this->countriesGetRequest->setSearch($q);
		}

		if ($type === 'question') {
			$this->countriesGetRequest->setShowDisabled();
		}

		$this->countriesGetRequest->setLimit(500);
		$this->countriesGetRequest->setPage($p - 1);
		$countries = $this->countriesGetRequest->execute();

		$result = [];

		foreach ($countries->getItems() as $country) {
			if ($type === 'create') {
				if ($country->isHome()) {
					continue;
				}

				$result[] = [
					'text' => $country->getName(),
					'url' => $this->link(':Trips:Front:TripCreate:create', ['country' => $country->getCode()]),
					'icon' => 'flag-' . $country->getCode(),
				];
			} else {
				if ($type === 'filter' && $country->isHome()) {
					continue;
				}

				$result[] = [
					'value' => $country->getCode(),
					'text' => $country->getName(),
					'icon' => 'flag-' . $country->getCode(),
				];
			}
		}

		$this->sendJson($result);
	}


	/**
	 * @return string
	 */
	protected function getLayoutName(): string
	{
		return 'layout-trip';
	}
}
