<?php

declare(strict_types=1);

namespace Cleevio\Fields\API\Requests;

use Cleevio\API\BaseRequest;
use Cleevio\ApiClient\Exception\ApiException;
use Cleevio\Fields\API\Responses\FieldsResponse;
use InvalidArgumentException;

class FieldsGetRequest extends BaseRequest implements IFieldsGetRequest
{

	/**
	 * @var int
	 */
	private $tripId;


	/**
	 * Endpoint path
	 * @return string
	 */
	protected function path(): string
	{
		if ($this->tripId === null) {
			throw new InvalidArgumentException;
		}

		return '/fields?trip=' . $this->tripId;
	}


	/**
	 * Method
	 * @return string
	 */
	protected function method(): string
	{
		return 'GET';
	}


	/**
	 * Determines whether request requires authorization
	 * @return bool
	 */
	function requiresAuth(): bool
	{
		return true;
	}


	/**
	 * @return FieldsResponse
	 * @throws ApiException
	 */
	function execute(): FieldsResponse
	{
		return new FieldsResponse($this->request());
	}


	/**
	 * @param int $id
	 */
	function setTripId(int $id): void
	{
		$this->tripId = $id;
	}
}
