<?php

declare(strict_types=1);

namespace Cleevio\Fields\API\Responses;

use Cleevio\ApiClient\IResponse;

class FieldsResponse implements IResponse
{
	/**
	 * @var FieldResponse[]
	 */
	private $hostFields = [];


	/**
	 * HostFieldsResponse constructor.
	 * @param array $hostFields
	 */
	public function __construct(array $hostFields)
	{
		$this->hostFields = array_map(static function ($hostField) {
			return new FieldResponse($hostField);
		}, $hostFields);
	}

	/**
	 * @return FieldResponse[]
	 */
	public function getHostFields(): array
	{
		return $this->hostFields;
	}

}
