<?php

declare(strict_types=1);

namespace Cleevio\Fields\API\Responses;

use Cleevio\ApiClient\IResponse;
use InvalidArgumentException;

class FieldResponse implements IResponse
{

	/**
	 * @var int
	 */
	private $id;

	/**
	 * @var string
	 */
	private $name;

	/**
	 * @var string
	 */
	private $type;

	/**
	 * @var string|null
	 */
	private $placeholder;

	/**
	 * @var bool
	 */
	private $required;

	/**
	 * @var string|null
	 */
	private $validation;

	/**
	 * FieldResponse constructor.
	 * @param array $field
	 */
	public function __construct(array $field)
	{
		if (array_key_exists('id', $field) &&
			array_key_exists('name', $field) &&
			array_key_exists('type', $field) &&
			array_key_exists('required', $field) &&
			array_key_exists('validation', $field)) {
			$this->id = $field['id'];
			$this->name = $field['name'];
			$this->type = $field['type'];
			$this->required = (bool) $field['required'];
			$this->validation = $field['validation'];
			$this->placeholder = $field['placeholder'];
		} else {
			throw new InvalidArgumentException;
		}
	}


	/**
	 * @return int
	 */
	public function getId(): int
	{
		return $this->id;
	}


	/**
	 * @return string
	 */
	public function getName(): string
	{
		return $this->name;
	}


	/**
	 * @return string
	 */
	public function getType(): string
	{
		return $this->type;
	}


	/**
	 * @return string|null
	 */
	public function getPlaceholder(): ?string
	{
		return $this->placeholder;
	}


	/**
	 * @return bool
	 */
	public function isRequired(): bool
	{
		return $this->required;
	}

	/**
	 * @return string|null
	 */
	public function getValidation(): ?string
	{
		return $this->validation;
	}
}
