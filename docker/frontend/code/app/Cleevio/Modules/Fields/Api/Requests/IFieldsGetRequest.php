<?php

declare(strict_types=1);

namespace Cleevio\Fields\API\Requests;

interface IFieldsGetRequest
{
	function setTripId(int $id): void;
}
