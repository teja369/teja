// Dependencies
import './js/vendor/jquery';
import './js/vendor/bootstrap';
import './js/vendor/bootstrap-datepicker.min';
import './js/vendor/bootstrap-datepicker.en-GB.min';
import './js/vendor/parsley.min';
import './js/vendor/simplebar.min';
import './js/vendor/swiper.min';
import './js/vendor/bootstrap-select.min'
import 'vanilla-lazyload';
//import 'live-form-validation';
// import 'bootstrap';

// Application dependencies
import './js/naja';
import './js/lazyLoad';

// JS
import './js/app'
import './js/custom'
import './js/extensions/ajax.modal';
import './js/extensions/ajax.dimmer';

// Styles
import './scss/app.scss'
