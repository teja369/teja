$(document).ready(function () {

    $('input[type="radio"].language-select')
        .on('click', function () {
            $(this).next('a')[0].click();
        });

    $('#mobile-next, #desktop-next')
        .on('click', function (e) {
            e.preventDefault();
            if (!$('#submit').hasClass('disabled')) {
                if(
                    !$(this).hasClass('disabled') &&
                    $('input[name="start"]').val() !== '' &&
                    $('input[name="end"]').val() !== '' &&
                    $('select[name="country"]').val() !== null
                ) {
                    $(this).addClass('btn-loading');
                    $(this).addClass('disabled');
                    $(this).append('<div class="loading"><div></div></div>');
                }
                $('#submit')[0].click();
            }
        });

    $('#clients, #non-clients, .child-question').removeClass('hidden');

    $('#frm-createTripHostForm-form-hostType-client').on('click', function () {
        $('#clients').removeClass('hidden');
    });

    $('#frm-createTripHostForm-form-hostType-none').on('click', function () {
        $('#clients').addClass('hidden');
    });


    $('#addHostButton').on('click', function () {
        $("#frm-createTripHostForm-form-toggler-non-client").click();

        var defaultOption = $('select[name="client"] option[selected="selected"]');
        defaultOption.removeAttr('selected');
        $('select[name="client"]').val(null);
        $('.filter-option-inner-inner').text('');
    });


    $(document).on('click', '#frm-createTripHostForm-form .dropdown-item', function () {
        $("#frm-createTripHostForm-form-toggler-client").click();
    });

    var formTimeOut = null;

    $('#frm-tripsTable-formDesktop, #frm-tripsTable-formMobile').on('change', function (e) {

        const that = $(this);

        if (formTimeOut !== null) {
            return;
        }

        formTimeOut = setTimeout(function () {
                formTimeOut = null;
            that.find(".submit").click();
        }, 400);
    });

});

