import $ from 'jquery'
import naja from 'naja';

/**
 * Dimmer Extension
 *
 * @param naja
 * @returns {DimmerExtension}
 * @constructor
 */
function DimmerExtension(naja) {

	naja.addEventListener('interaction', function (event) {
		if (!event.element) {
			return;
		}

		this.element = event.element;
		this.closestForm = this.element.closest('button, a.btn');
	}.bind(this));

	naja.addEventListener('start', showLoader.bind(this));
	naja.addEventListener('success', hideLoader.bind(this));
	naja.addEventListener('error', hideLoaderError.bind(this));

	function showLoader() {
		this.text = $(this.closestForm).text();
		$(this.closestForm).append('<div class="loading"><div></div></div>');
		$(this.closestForm).addClass('btn-loading');
		$(this.closestForm).addClass('disabled');
	}

	function hideLoader() {
		$(this.closestForm).html(this.text);
		$(this.closestForm).removeClass('disabled');
		$(this.closestForm).removeClass('btn-loading');

	}

	function hideLoaderError() {
		//$(this.closestForm).find('div.dimmer').html('<div class="ajax-error"><i class="fal fa-exclamation-circle fa-2x red text"></i><br><div>Error Occured. Please try again later.</div></div>');
		//const overlay = this.closestForm;

		//setTimeout(function () {
		//	$(overlay).ajaxDimmer('hide');
		//}, 3000);
	}

	return this;
}

naja.registerExtension(DimmerExtension);
