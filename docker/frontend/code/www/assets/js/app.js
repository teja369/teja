var app = {
	cache: {
		disabledDates: {},
	},
    init: function () {
        app.initNavigation();
        app.initTimeoutHide();
        app.initArrowDropdowns();
        app.initButtonRadios();
        app.initRadioClear();
        app.initTabs();
        app.initSearch();
        app.initDropdown();
        app.initMultiselect();
        app.initDropdownSearch();
        app.initDatepickerInputs();
        app.initDatepickers();
        app.initTimepickers();
        app.initDatetimePickers();
        app.initTooltips();
        app.initInputTogglers();
        app.initTogglers();
        app.initSwipers();
        app.initInputClear();
        app.initNewTrip();
        app.initModals();
        app.initAlerts();
		app.initFilterCollapse();
		app.initResetButtons();
		app.initLoadingButtons();
    },
    initArrowDropdowns: function () {
        $('.dropdown-menu.dropdown-menu-arrow').on("click.bs.dropdown", function (e) {
            e.stopPropagation();
        });
    },
    initButtonRadios: function () {
        $('.btn-flag-radio').click(function () {
            var radio = $(this).find('input[type="radio"]');
            if (radio.is(':checked')) {
                radio.prop('checked', false);
            } else {
                radio.prop('checked', true);
            }
            radio.trigger('change');
        });
    },
	initRadioClear: function () {
		$('.radio-clear').each(function(){
			var container = $(this);
			var text = container.data('clear-text');
			var btn = $('<button type="button" class="btn-default ml-2">' + text + '</button>');
			if(container.find('input[type=radio]').is(':checked')){
				container.append(btn);
			}
			container.find('input[type=radio]').change(function(){
				if($(this).is(':checked')){
					container.append(btn);
					btn.click(function(){
						$(this).closest('.radio-clear').find('input[type=radio]').prop('checked', false);
						$(this).remove();
					});
					container.siblings('.radio-clear').each(function(){
						$(this).find('.btn-default').remove();
					})
				}
			});
			btn.click(function(){
				$(this).closest('.radio-clear').find('input[type=radio]').prop('checked', false);
				$(this).remove();
			})
		});
	},
    initSearch: function () {
        if ($('.search').length) {
            $('.search').each(function () {
                var search = $(this);
                var source = search.data('source');
                var input = search.find('input[type=text]');
                var items = search.find('.search-whisperer-items');
                var loading = search.find('.loading');
                var empty = search.find('.empty');
                var timeout = search.data('typing-interval');
                var timer;
                input.click(function () {
                    search.addClass('active');
                    empty.hide();
                    if (input.val()) {
                        loading.hide();
                    } else {
                        if (!items.length) {
                            loading.show();
                            $.get(source, function (result) {
                                loading.hide();
                                empty.hide();
                                items.html('');
                                if (result.length) {
                                    $.each(result, function (idx, obj) {
                                        items.append('<div class="search-whisperer-item"><span class="' + obj.icon + ' flag-sm"></span><span class="country">' + obj.text + '</span></div>');
                                    });
                                } else {
                                    empty.show();
                                }
                            });
                        } else {
                            loading.hide();
                        }
                    }
                });
                input.on('input', function () {
                    if ($(this).val()) {
                        search.addClass('typing');
                    } else {
                        search.removeClass('typing');
                    }
                });
                input.on('keyup', function () {
                    var query = $(this).val();
                    clearTimeout(timer);
                    timer = setTimeout(function () {
                        items.html('');
                        empty.hide();
                        loading.show();
                        // $.get(source, {q: $(this).val()}, function(result){
                        jQuery.ajax({
                            url: source,
                            data: {q: query},
                            success: function (result) {
                                loading.hide();
                                empty.hide();
                                items.html('');
                                if (result.length) {
                                    $.each(result, function (idx, obj) {
                                        items.append('<a href="' + obj.url + '" class="search-whisperer-item"><span class="' + obj.icon + ' flag-sm"></span><span class="country">' + obj.text + '</span></a>');
                                    });
                                } else {
                                    empty.show();
                                }
                            }
                        });
                    }, timeout);
                });
                input.on('keydown', function () {
                    clearTimeout(timer);
                });
                search.find('.icon-clear').click(function () {
                    input.val('');
                    items.html('');
                    search.removeClass('typing active');
                });
            });
            app.closeOnClickOutside('.search');
        }
    },
    initDropdown: function () {
        $('[data-toggle="dropdown"]').selectpicker();
    },
	initMultiselect: function () {
		$('[data-toggle="select-multiple"]').each(function () {
			var placeholder = $(this).data('placeholder');
			var placeholderIcon = $(this).data('placeholderIcon');

            if(!!placeholder) {
                $(this).prepend('<option value="" selected disabled>' + placeholder + '</option>');
            }

            var typingInterval = $(this).data('typingInterval');
			var source = $(this).data('source');
			var enableGroups = $(this).data('enable-groups');
			var generate = $(this).data('generate');
			var clone = $(this).clone();
			clone.find('option:selected:not(:disabled)').remove();
			var options = clone.html();
			var name = '__select__' + $(this).attr('name');
			var id = $(this).attr('id');
			var targetId = app.generateUniqueId();

			if(!placeholderIcon){
				placeholderIcon = '';
			}

			var dropdown = $('<div class="select-multiple-' + generate + '"><div class="' + generate + '"><select name="' + name + '"class="form-control"data-toggle="search-dropdown"data-live-search="true"data-style="btn-dropdown"data-placeholder="' + placeholder + '"data-placeholder-icon="' + placeholderIcon + '"data-typing-interval="' + typingInterval + '"data-source="' + source + '"data-enable-groups="' + enableGroups + '"data-multiple="#' + id + '"data-multiple-target="#' + targetId + '">' + options + '</select></div><div id="' + targetId + '" class="mt-3"></div></div>');

			dropdown.insertAfter($(this));
		});
	},
    initDropdownSearch: function () {
        $('[data-toggle="search-dropdown"]').each(function () {
            var s = $(this);
            var type = s.data('type');
            var enableGroups = s.data('enable-groups');
            var multiple = s.data('multiple');
            var placeholder = s.data('placeholder');
            var placeholderIcon = s.data('placeholder-icon');
			var enableAjaxOnShow = s.data('ajax-on-show');
            var source = s.data('source');
            var sourceQuery = s.data('source-query');
            var timeout = s.data('typing-interval');
            var timer;

            if(!!placeholder) {
                $(this).prepend('<option value="" disabled '+($(this).find('option[selected]').length === 0 ? 'selected' : '')+'>' + placeholder + '</option>');
            }

			s.selectpicker({
	            liveSearch: true,
	            noneResultsText: 'No results found',
	        });

            var input = s.siblings('.dropdown-menu').find('.bs-searchbox input');
            var searchbox = s.siblings('.dropdown-menu').find('.bs-searchbox');
			var dropdown = s.parent('.dropdown');

            s.on('loaded.bs.select', function () {
                input.attr('placeholder', placeholder);
            });

            s.on('show.bs.select', function () {
                // var firstChild = s.siblings('.dropdown-menu').find('ul.dropdown-menu li:first-child');
                // if (firstChild.length && !firstChild.hasClass('dropdown-header') && placeholder && firstChild.text().trim() == placeholder.trim()) {
                    // firstChild.hide();
                // }
                searchbox.find('.icon-clear').remove();

				if(enableAjaxOnShow){
					s.data('page', 1);
					app.handleDropdownSearchAjaxRequest(s, true);
					dropdown.find('.dropdown-clear').remove();
				}

            });

			s.on('hide.bs.select', function () {
			    s.trigger('change');
			});

			s.on('shown.bs.select', function (){
				if(enableAjaxOnShow){
					s.siblings('.dropdown-menu').find('div.inner.show').on('scroll', function () {
						if($(this).scrollTop() + $(this).innerHeight() >= $(this)[0].scrollHeight) {
							app.handleDropdownSearchAjaxRequest(s, false);
				        }
					});
				}
			});

            s.on('changed.bs.select', function (e) {
                if (multiple) {
					var value = s.find(':selected').attr('value');
					if(!$(multiple).find('option[value="' + value + '"]:selected').length){
						var m = $(s.data('multiple-target'));
						var icon = s.find(':selected').data('icon');
						if(icon){
							icon = '<span class="' + icon + ' flag-sm"></span>';
						} else {
							icon = '';
						}
						var text = s.find(':selected').text();
						var el = '<div class="multiple-selected-wrapper">'
							+ '<div class="multiple-selected">'
							+ icon
							+ text
							+ '</div>'
							+ '<button type="button" class="btn-default-lg btn-danger" data-value="' + value + '">'
							+ '<span class="icon-delete"></span>'
							+ '</button>'
							+ '</div>';
						m.append(el);
						$(multiple).append('<option value="' + value + '" selected>' + text + '</option>').trigger('change');
						$('.multiple-selected-wrapper button').click(function () {
							$(this).parent('.multiple-selected-wrapper').remove();
							$(multiple).find('option[value="' + $(this).data('value') + '"]').remove().trigger('change');
						});
					}
					s.val('');
					s.selectpicker('refresh');
                } else {
					if(placeholder && s.val()){
						if(!dropdown.find('.dropdown-clear.icon-clear').length){
							var clear = $('<span class="dropdown-clear icon-clear"></span>');
							dropdown.prepend(clear);
							clear.click(function(){
								$(this).remove();
								dropdown.find('.dropdown-toggle').addClass('bs-placeholder');
								if(placeholderIcon){
									s.prepend('<option value="" data-icon="' + placeholderIcon + '" disabled>' + placeholder + '</option>');
								}
								s.val('');
								s.selectpicker('refresh');
								s.trigger('change');
							});
						}
					}
				}
            });

			if(!!s.val() && placeholder){
				var clear = $('<span class="dropdown-clear icon-clear"></span>');
				dropdown.prepend(clear);
				clear.click(function(){
					$(this).remove();
					if(placeholderIcon){
						s.prepend('<option value="" data-icon="' + placeholderIcon + '" disabled>' + placeholder + '</option>');
					}
					s.val('');
					s.selectpicker('refresh');
					s.trigger('change');
				});
			}

			if (multiple) {
				var m = $(s.data('multiple-target'));
				$(multiple + ' :selected').each(function(){
					if(!$(this).is(':disabled')){
						var icon = $(this).data('icon');
						if(icon){
							icon = '<span class="' + icon + ' flag-sm"></span>';
						} else {
							icon = '';
						}
						var text = $(this).text();
						var value = $(this).attr('value');
						var el = '<div class="multiple-selected-wrapper">'
							+ '<div class="multiple-selected">'
							+ icon
							+ text
							+ '</div>'
							+ '<button type="button" class="btn-default-lg btn-danger" data-value="' + value + '">'
							+ '<span class="icon-delete"></span>'
							+ '</button>'
							+ '</div>';
						m.append(el);
						$('.multiple-selected-wrapper button').click(function () {
							$(this).parent('.multiple-selected-wrapper').remove();
							$(multiple).find('option[value="' + $(this).data('value') + '"]').remove().trigger('change');
						});
					}
				});
			}

            if (source) {

                input.on('input', function () {
                    $('.no-results').text('Loading...');
                });

                input.on('keyup', function () {

                    clearTimeout(timer);

                    if ($(this).val()) {
                        if (!searchbox.find('.icon-clear').length) {
                            searchbox.append('<span class="icon-clear"></span>');
                            searchbox.find('.icon-clear').click(function (e) {
                                e.stopPropagation();
                                input.val('').trigger('change');
                                $(this).remove();
								s.trigger('change');
                            })
                        }
                    } else {
                        searchbox.find('.icon-clear').remove();
                    }

                    timer = setTimeout(function () {
						app.handleDropdownSearchAjaxRequest(s, true);
                    }, timeout);
                });
            }
        });
    },
	handleDropdownSearchAjaxRequest: function (s, empty) {
		var placeholder = s.data('placeholder');
		var placeholderIcon = s.data('placeholder-icon');
		var enableGroups = s.data('enable-groups');
		var source = s.data('source');
		var sourceQuery = s.data('source-query');
		var input = s.siblings('.dropdown-menu').find('.bs-searchbox input');
		var records = s.data('records');
		var data = {q: input.val()};

		if(!empty) {
			s.data('page', s.data('page') + 1);
		}

		var page = s.data('page');

		if(page) {
			data['p'] = page;
		}

		if(records) {
			data['r'] = records;
		}

		if (sourceQuery) {
		    data = $.extend({}, data, sourceQuery);
		}

		var request = $.ajax({
		    type: 'GET',
		    url: source,
		    data: data,
		});

		request.done(function (data) {

			if(empty){
				s.empty();
			}

			var icon = '';

			if(placeholderIcon){
				icon = 'data-icon="' + placeholderIcon + '"';
			}

		    var html = '<option value="" class="d-none"' + icon + '>' + placeholder + '</option>';

		    if (!data.length) {
		        $('.no-results').text('No results found');
		    } else {

			    if (enableGroups) {

			        for (var i = 0; i < data.length; i++) {

			            html += '<optgroup label="' + data[i].label + '">';

			            for (var j = 0; j < data[i].data.length; j++) {

			                var d = data[i].data[j];
			                var cls = '';
			                var icon = '';
			                var subtext = '';

			                if (d.hidden) {
			                    cls = ' class="d-none"';
			                }

			                if (d.icon) {
			                    icon = 'data-icon="' + d.icon + '"';
			                }

			                if (d.subtext) {
			                    subtext = ' data-subtext="' + d.subtext + '"';
			                }

			                html += '<option value="' + d.value + '"' + icon + cls + subtext + '>' + d.text + '</option>';

			            }
			            ;

			            html += '</optgroup>';
			        }

			    } else {

			        for (var j = 0; j < data.length; j++) {

			            var d = data[j];
			            var cls = '';
			            var icon = '';
			            var subtext = '';

			            if (d.hidden) {
			                cls = ' class="d-none"';
			            }

			            if (d.icon) {
			                icon = ' data-icon="' + d.icon + '"';
			            }

			            if (d.subtext) {
			                subtext = ' data-subtext="' + d.subtext + '"';
			            }

			            html += '<option value="' + d.value + '"' + icon + cls + subtext + '>' + d.text + '</option>';

			        }
			        ;
			    }
			}
		    s.append(html);
		    s.selectpicker('refresh');
		});
		// s.trigger('change');
	},
    initTabs: function () {
        $('[data-toggle="tab"]').click(function () {
            var target = $($(this).data('target'));
            target.siblings('.tab').removeClass('active');
            target.addClass('active');
        });
    },
    initDatepickers: function () {
        if (app.isMobile()) {
            $('.datepicker').each(function () {
                $(this).attr('type', 'date');
            });
        } else {
            $('.datepicker-external').each(function () {
                var input = $($(this).data('input'));
                var datepicker = $(this);
                var controls = $(this).siblings('.datepicker-controls');
                var selectMonth = controls.find('select.select-month');
				var isStart = datepicker.data('type') == 'start';
                var connected = $($(this).data('connect'));
                var connectedInput = $(connected.data('input'));
                var d = datepicker.datepicker({
                    todayHighlight: true,
                    maxViewMode: 'years',
                    templates: {
                        leftArrow: '<span class="icon-back"></span>',
                        rightArrow: '<span class="icon-next"></span>'
                    },
                }).on('changeDate', function (e) {
                    var date = e.date;
                    input.val(datepicker.datepicker('getFormattedDate'));
                    input.trigger('change');
                    if (input.data('connect')) {
                        var con = $(input.data('connect'));
                        con.val(datepicker.datepicker('getFormattedDate'));
                        con.trigger('change');
                    }
                    if (date) {
                        app.datepickerSelectMonth(selectMonth, date);
                        if (connected.length) {
                            var cInput = $(connected.data('input'));
                            var cSelectMonth = connected.siblings('.datepicker-controls').find('select.select-month');
                            if (!cInput.val()) {
                                app.datepickerSelectMonth(cSelectMonth, date);
                                connected.datepicker('updateViewMonthYear', date.getMonth(), date.getFullYear(), date);
                            }

							var start = datepicker.find('.datepicker-days > table > tbody > tr > td.active').data('date');
				            var end = connected.find('.datepicker-days > table > tbody > tr > td.active').data('date');
							if(start && end){
								if(start > end){
									var h = start;
									start = end;
									end = h;
								}
								var url = datepicker.data('disabled-url');
								$.get(url, {start: start, end: end}, function (result) {
					                if (!result.length) {
										app.selectRange(datepicker, connected, false);
									} else {
										if(isStart){
											connectedInput.val('');
											connected.val('').datepicker('update');
										} else {
											input.val('');
											datepicker.val('').datepicker('update');
											connected.datepicker('update', connected.datepicker('getFormattedDate'));
										}
										app.disableDates(connected);
										app.disableDates(datepicker);
									}
								});
							}
                        }

                        if (datepicker.closest('.datepicker-input-container').length) {
                            var c = datepicker.closest('.datepicker-input-container');
                            c.removeClass('active');
                            c.find('.datepicker').val(input.val());
                        }

						app.disableDates(connected);
                        app.disableDates(datepicker);
                    }

				}).on('clearDate', function(){
					if (input.data('connect')) {
                        var con = $(input.data('connect'));
                        con.val(datepicker.datepicker('getFormattedDate'));
                        con.trigger('change');
                    }
				});

				if(input.val()){
					d.datepicker('update', input.val());
					app.datepickerSelectMonth(selectMonth, d.datepicker('getDate'));
				}

                input.on('input', function () {
                    var date = input.val();
					if(!date){
						d.datepicker('update');
						input.trigger('change');
					} else {
	                    var dateReg = /^\s*(3[01]|[12][0-9]|0?[1-9])\-(1[012]|0?[1-9])\-((?:19|20)\d{2})\s*$/
	                    if (date.match(dateReg)) {
	                        d = date.split('-');
	                        datepicker.datepicker('setDate', date);
	                        date = d[2] + '-' + d[1] + '-' + d[0];
	                        date = new Date(date);
	                        app.datepickerSelectMonth(selectMonth, date, false);
	                    }
	                }
                });

                controls.find('.next-month').click(function () {
                    var newDate = datepicker.datepicker('updateViewNextMonth');
                    app.datepickerSelectMonth(selectMonth, newDate);
                    app.disableDates(datepicker);
                    app.selectRange(datepicker, connected, true);
                });

                controls.find('.prev-month').click(function () {
                    var newDate = datepicker.datepicker('updateViewPrevMonth');
                    app.datepickerSelectMonth(selectMonth, newDate);
                    app.disableDates(datepicker);
                    app.selectRange(datepicker, connected, true);
                });

                controls.find('.select-month').change(function () {
                    if ($(this).val()) {
                        var date = $(this).val().split('-');
						var month = parseInt(date[1]) - 1;
                        datepicker.datepicker('updateViewMonthYear', month, date[0]);
                        app.disableDates(datepicker);
                        app.selectRange(datepicker, connected, true);
                    }
                });

                app.disableDates(datepicker);
                app.disableDates(connected);
                app.selectRange(datepicker, connected, false);
            });
        }
    },
    selectRange: function (datepicker, connected, updateView) {
        // var second = connected.datepicker("getUTCDate"),
        //     first = datepicker.datepicker("getUTCDate"),
        var second = connected.datepicker("getDate"),
            first = datepicker.datepicker("getDate"),
            isStart = datepicker.data('type') == 'start',
            isEnd = datepicker.data('type') == 'end',
            diff,
            elStart,
            elEnd,
            nextStart,
            prevEnd;

        if (second) {

            diff = Math.round((second - first) / (1000 * 60 * 60 * 24));

            if (diff < 0) {
                diff = diff * -1;
            }

            datepicker.find('.day').removeClass('selected selected-start selected-end selected-last selected-first');
            if (!updateView) {
                connected.find('.day').removeClass('selected selected-start selected-end selected-last selected-first');
            }

            if ((isStart && first <= second) || (isEnd && first >= second)) {

                if (datepicker.find('.day.active').length) {

                    if (isEnd) {

                        elStart = connected.find('.day.active');
                        elEnd = datepicker.find('.day.active');

                    } else {

                        elStart = datepicker.find('.day.active');
                        elEnd = connected.find('.day.active');

                    }

                    var i = 0;
                    for (i = 0; i < diff; i++) {

                        nextStart = elStart.next();
                        if (!nextStart.length) {
                            nextStart = elStart.parent().next().find('.day:first-child');
                            nextStart.addClass('selected-first');
                            elStart.addClass('selected-last');
                        }
                        elStart = nextStart;
                        elStart.addClass('selected selected-start');
                        if (i + 1 == diff) {
                            elStart.addClass('selected-last');
                        }

                        prevEnd = elEnd.prev();
                        if (!prevEnd.length) {
                            prevEnd = elEnd.parent().prev().find('.day:last-child');
                            prevEnd.addClass('selected-first');
                            elEnd.addClass('selected-last');
                        }
                        elEnd = prevEnd;
                        elEnd.addClass('selected selected-end');
                        if (i + 1 == diff) {
                            elEnd.addClass('selected-last');
                        }

                    }
                } else {
                    var viewDate = datepicker.datepicker('getViewDate');
                    // var vMonth = parseInt(viewDate.getUTCFullYear() + viewDate.getUTCMonth().toString().padStart(2, '0'));
                    // var fMonth = parseInt(first.getUTCFullYear() + first.getUTCMonth().toString().padStart(2, '0'));
                    // var sMonth = parseInt(second.getUTCFullYear() + second.getUTCMonth().toString().padStart(2, '0'));
					// var day = second.getUTCDate();
                    var vMonth = parseInt(viewDate.getFullYear() + viewDate.getMonth().toString().padStart(2, '0'));
                    var fMonth = parseInt(first.getFullYear() + first.getMonth().toString().padStart(2, '0'));
                    var sMonth = parseInt(second.getFullYear() + second.getMonth().toString().padStart(2, '0'));
                    var day = second.getDate();
                    if (isStart) {
                        if (vMonth > fMonth && vMonth == sMonth) {
                            var found = true;
                            datepicker.find('.day').each(function () {
                                if (found) {
                                    if ($(this).text() == day) {
                                        found = false;
                                    }
                                    $(this).addClass('selected selected-start');
                                    if ($(this).is(':first-child')) {
                                        $(this).addClass('selected-first');
                                    }
                                    if ($(this).is(':last-child')) {
                                        $(this).addClass('selected-last');
                                    }
                                    if (!found) {
                                        $(this).addClass('selected-last');
                                    }
                                }
                            });
                        }
                        if (vMonth > fMonth && vMonth < sMonth) {
                            datepicker.find('.day').each(function () {
                                $(this).addClass('selected selected-start');
                                if ($(this).is(':first-child')) {
                                    $(this).addClass('selected-first');
                                }
                                if ($(this).is(':last-child')) {
                                    $(this).addClass('selected-last');
                                }
                            });
                        }
                    } else {
                        if (vMonth < fMonth && vMonth == sMonth) {
                            var found = false;
                            var foundDay = 0;
                            datepicker.find('.day').each(function () {
                                if ($(this).text() == day) {
                                    found = true;
                                }
                                if (found) {
                                    foundDay++;
                                    $(this).addClass('selected selected-end');
                                    if (foundDay == 1) {
                                        $(this).addClass('selected-last');
                                    }
                                    if ($(this).is(':first-child')) {
                                        $(this).addClass('selected-last');
                                    }
                                    if ($(this).is(':last-child')) {
                                        $(this).addClass('selected-first');
                                    }
                                }

                            });
                        }
                        if (vMonth < fMonth && vMonth > sMonth) {
                            datepicker.find('.day').each(function () {
                                $(this).addClass('selected selected-end');
                                if ($(this).is(':first-child')) {
                                    $(this).addClass('selected-last');
                                }
                                if ($(this).is(':last-child')) {
                                    $(this).addClass('selected-first');
                                }
                            });
                        }
                    }
                }
            } else {
                if (!updateView) {
                    connected.datepicker('clearDates');
                    app.disableDates(datepicker);
                    app.disableDates(connected);
                }
            }
        }
    },
    disableDates: function (datepicker) {
		var viewDate = datepicker.datepicker('getViewDate');
		if(viewDate instanceof Date){
			// var key = viewDate.getUTCMonth() + '_' + viewDate.getUTCFullYear();
			var key = viewDate.getMonth() + '_' + viewDate.getFullYear();
	        var url = datepicker.data('disabled-url');
	        if (url && !app.cache.disabledDates.hasOwnProperty(key)) {
		        var start = datepicker.find('.datepicker-days > table > tbody > tr:first-child > td:first-child').data('date');
		        var end = datepicker.find('.datepicker-days > table > tbody > tr:last-child > td:last-child').data('date');
		        $.get(url, {start: start, end: end}, function (result) {
		            if (result && result.length) {
						app.cache.disabledDates[key] = result;
						app.disableDatesFromCache(datepicker, key);
		            }
		        });
	        }
			app.disableDatesFromCache(datepicker, key);
		}
    },
	disableDatesFromCache: function(datepicker, key) {
		if(app.cache.disabledDates.hasOwnProperty(key)){
			$.each(app.cache.disabledDates[key], function (idx, timestamp) {
				var item = datepicker.find('[data-date="' + timestamp + '"]');
				if (item.length) {
					item.addClass('disabled disabled-date');
				}
			});
		}
	},
    initDatepickerInputs: function () {
        $('.datepicker-input-container').each(function () {
            var container = $(this);
            var input = $(this).find('input[type=text]');
            var id = container.attr('id');
			var containerInput = container.find('input[type=text]');
			containerInput.val(input.val());
            input.focus(function () {
                container.addClass('active');
            });
            app.closeOnClickOutside('#' + id);
        });
    },
    initTooltips: function () {
        $('[data-toggle="tooltip"]').tooltip();
    },
    initInputTogglers: function () {
        $('[data-input-show]').change(function () {
            if ($(this).is(':checked')) {
                $($(this).data('input-show')).addClass('active');
            }
        })
        $('[data-input-hide]').change(function () {
            if ($(this).is(':checked')) {
                $($(this).data('input-hide')).removeClass('active');
            }
        })
    },
    initTogglers: function () {
        $('[data-toggle="toggle"]').click(function () {
            var target = $($(this).data('target'));
            if (target.is(':visible')) {
                $(this).text($(this).data('text-show'));
            } else {
                $(this).text($(this).data('text-hide'));
            }
            target.toggle();
        });
    },
    initNavigation: function () {
        $('[data-toggle="navigation"]').click(function () {
            var nav = $('#navigation');
            if (nav.hasClass('active')) {
                nav.removeClass('active');
            } else {
                nav.addClass('active');
            }

        });
        $('[data-toggle="navigation-filter"]').click(function () {
            var nav = $('#navigation');
            if (nav.hasClass('active')) {
                nav.removeClass('active');
            } else {
                $('#modal-filter').modal('hide');
                nav.addClass('active');
            }

        });
    },
    initSwipers: function () {
        var swiper = new Swiper('.swiper-container', {
            // loop: true,
            slidesPerView: 'auto',
            spaceBetween: 10,
        });
    },
    initInputClear: function () {
        $('.input-clear').each(function () {
            var e = $(this);
            var input = $(this).find('.form-control');
            var clear = $(this).find('.icon-clear');
            if (input.val()) {
                e.addClass('filled');
            }
            input.on('input change', function () {
                if ($(this).val()) {
                    e.addClass('filled');
                } else {
                    e.removeClass('filled');
                }
            })
            clear.click(function () {
                input.val('');
                input.trigger('change');
                e.removeClass('filled');
                // if($(this).closest('.datepicker-input-container').length){
                // 	$(this).closest('.datepicker-input-container').removeClass('active');
                // }
            })
        })
    },
    initTimeoutHide: function () {

        $('[data-timeout-hide]').each(function () {

            var el = $(this);
            var delay = $(this).data('timeout-hide')

            setTimeout(function () {
                el.fadeOut();
            }, delay);

        });

    },
    initNewTrip: function () {
        // if($('.new-trip-content').length){
        // 	var initialHeight = $('.new-trip-content .simplebar-content-wrapper').height();
        // 	if(app.isMobile()){
        // 		$('.new-trip-content .simplebar-content-wrapper').scroll(function(){
        // 			var offset = $('body').height() * 0.1;
        // 			if(offset > $(this).scrollTop()){
        // 				// $('.new-trip-footer').removeClass('position-relative');
        // 			} else {
        // 				$('.new-trip-footer').addClass('position-relative');
        // 			}
        // 		});
        // 	}
        // }
    },
    closeOnClickOutside: function (selector) {
        window.addEventListener('click', function (e) {
            var target = $(e.target);
            if (target.closest(selector).length && $(selector).is(":visible")) {
            } else {
                $(selector).removeClass('active typing open');
            }
        });
    },
    isMobile: function () {
        return /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent);
    },
    datepickerSelectMonth: function (element, date) {
        if (date) {
            var locale = element.data('locale');
            var monthText = date.toLocaleString(locale, {month: 'long'});
            monthText = monthText.charAt(0).toUpperCase() + monthText.slice(1)
            var month = date.getMonth() + 1;
            month = month.toString().padStart(2, '0');
            var year = date.getFullYear();
            element.html('<option value="" selected>' + monthText + ' ' + year + '</option>')
            element.selectpicker('refresh');
        }
    },
    initTimepickers: function () {
        $('[data-toggle="timepicker"]').each(function () {
            var input = $(this);
			var target = input.data('target');
            var mStep = parseInt($(this).data('step-minutes'));
			if(target){
	            var timepicker = $('<div class="timepicker-container detached"><input type="text" placeholder="hh:mm" value="12:00 AM" class="form-control"><div class="timepicker-controls"><div class="row"><div class="col-6 d-flex align-items-center"><div class="timepicker-controls-container"><button type="button" class="btn-default hours-up"><span class="icon-chevron-up"></span></button><div class="timepicker-hours">12</div><button type="button" class="btn-default hours-down"><span class="icon-chevron-down"></span></button></div><div class="timepicker-dots">:</div><div class="timepicker-controls-container"><button type="button" class="btn-default minutes-up"><span class="icon-chevron-up"></span></button><div class="timepicker-minutes">00</div><button type="button" class="btn-default minutes-down"><span class="icon-chevron-down"></span></button></div></div><div class="col-6 d-flex align-items-center justify-content-center"><button type="button" class="btn-default timepicker-am active">AM</button><button type="button" class="btn-default timepicker-pm">PM</button></div></div></div></div>');
				$(target).append(timepicker);
			} else {
	            var timepicker = $('<div class="timepicker-container input-clear"><span class="icon-clear"></span><input type="text" placeholder="hh:mm" value="12:00 AM" class="form-control"><div class="timepicker-controls"><div class="row"><div class="col-6 d-flex align-items-center"><div class="timepicker-controls-container"><button type="button" class="btn-default hours-up"><span class="icon-chevron-up"></span></button><div class="timepicker-hours">12</div><button type="button" class="btn-default hours-down"><span class="icon-chevron-down"></span></button></div><div class="timepicker-dots">:</div><div class="timepicker-controls-container"><button type="button" class="btn-default minutes-up"><span class="icon-chevron-up"></span></button><div class="timepicker-minutes">00</div><button type="button" class="btn-default minutes-down"><span class="icon-chevron-down"></span></button></div></div><div class="col-6 d-flex align-items-center justify-content-center"><button type="button" class="btn-default timepicker-am active">AM</button><button type="button" class="btn-default timepicker-pm">PM</button></div></div></div></div>');
				timepicker.insertAfter(input);
			}
            var hUp = timepicker.find('.hours-up');
            var hDown = timepicker.find('.hours-down');
            var h = timepicker.find('.timepicker-hours');
            var mUp = timepicker.find('.minutes-up');
            var mDown = timepicker.find('.minutes-down');
            var m = timepicker.find('.timepicker-minutes');
            var tInput = timepicker.find('input[type=text]');
            var am = timepicker.find('.timepicker-am');
            var pm = timepicker.find('.timepicker-pm');
            input.mouseup(function () {
				$(this).val(tInput.val());
				$(this).trigger('change');
                setTimeout(function () {
                    timepicker.addClass('active');
                }, 100);
            });
            tInput.keyup(function () {
                var reg = /(0[1-9]:[0-5][0-9]((\ ){0,1})((AM)|(PM)|(am)|(pm)))|([1-9]:[0-5][0-9]((\ ){0,1})((AM)|(PM)|(am)|(pm)))|(1[0-2]:[0-5][0-9]((\ ){0,1})((AM)|(PM)|(am)|(pm)))/;
                if (tInput.val().match(reg)) {
                    tInput.val(tInput.val().toUpperCase());
                    var s = tInput.val().split(' ');
                    var t = s[0].split(':');
                    h.text(t[0]);
                    m.text(t[1]);
                    if (s[1] == 'AM') {
                        pm.removeClass('active');
                        am.addClass('active');
                    } else {
                        am.removeClass('active');
                        pm.addClass('active');
                    }
                }
            });
            app.closeOnClickOutside('.timepicker-container');
            if (input.val()) {
                tInput.val(input.val());
				tInput.trigger('change');
            }
            tInput.change(function () {
                input.val($(this).val());
				input.trigger('change');
            });
            hUp.click(function () {
                var val = parseInt(h.text());
                if (val < 12) {
                    val++;
                } else {
                    val = 1;
                }
                h.text(val.toString().padStart(2, '0'));
                app.timepickerSetValue(timepicker);
            });
            hDown.click(function () {
                var val = parseInt(h.text());
                if (val > 1) {
                    val--;
                } else {
                    val = 12;
                }
                h.text(val.toString().padStart(2, '0'));
                app.timepickerSetValue(timepicker);
            });
            mUp.click(function () {
                var val = parseInt(m.text());
                val = val + mStep;
                if (val > 59) {
                    val = 0;
                }
                m.text(val.toString().padStart(2, '0'));
                app.timepickerSetValue(timepicker);
            });
            mDown.click(function () {
                var val = parseInt(m.text());
                val = val - mStep;
                if (val < 0) {
                    val = 60 - mStep;
                }
                m.text(val.toString().padStart(2, '0'));
                app.timepickerSetValue(timepicker);
            });
            am.click(function () {
                if (!am.hasClass('active')) {
                    pm.removeClass('active');
                    am.addClass('active');
                    app.timepickerSetValue(timepicker);
                }
            })
            pm.click(function () {
                if (!pm.hasClass('active')) {
                    am.removeClass('active');
                    pm.addClass('active');
                    app.timepickerSetValue(timepicker);
                }
            })

        });
    },
    timepickerGetValue: function (timepicker, get) {
        var val = timepicker.find('input[type=text]').val();
    },
    timepickerSetValue: function (timepicker) {
        var input = timepicker.find('input[type=text]');
        var h = timepicker.find('.timepicker-hours').text();
        var m = timepicker.find('.timepicker-minutes').text();
        var am = timepicker.find('.timepicker-am');
        var mer = 'AM';
        if (!am.hasClass('active')) {
            mer = 'PM';
        }
        input.val(h + ':' + m + ' ' + mer).trigger('change');
    },
	initDatetimePickers: function () {
		$('.datetimepicker-input-container').each(function () {
            var container = $(this);
            var datepickerInput = $(this).find('.datepicker');
            var timepickerInput = $(this).find('.timepicker');
			var input = $(this).find('.datepicker-input-value');
			var value = input.val();
            var id = container.attr('id');
			var containerInput = container.find('input[type=text]');
			datepickerInput.val(value.substr(0,value.indexOf(' ')));
			timepickerInput.val(value.substr(value.indexOf(' ')));
            datepickerInput.focus(function () {
                container.addClass('active');
            });
			timepickerInput.change(function () {
				input.val(datepickerInput.val() + ' ' + timepickerInput.val())
					.trigger('change');
			});
			datepickerInput.change(function () {
				input.val(datepickerInput.val() + ' ' + timepickerInput.val())
					.trigger('change');
			});
            app.closeOnClickOutside('#' + id);
        });
		$('.datetimepicker-input-container .datetimepicker-wrapper .timepicker').click(function(){
			$(this).closest('.datetimepicker-input-container').removeClass('active').addClass('open');
		});
		app.closeOnClickOutside('.datetimepicker-input-container');
	},
    initModals: function () {
        $('[data-toggle="modal-no-backdrop"]').click(function () {
            var target = $($(this).data('target'));
            if (target.is(':visible')) {
                target.modal('hide');
            } else {
                target.modal('show');
                $('.dashboard-navigation').removeClass('active');
                $('.modal-backdrop').hide();
            }
        })
    },
	initResetButtons: function () {
		$('[data-toggle="reset"]').click(function(){
			var target = $($(this).data('target'));
			target.find('[data-toggle="search-dropdown"]').each(function(){
				var placeholder = $(this).data('placeholder');
				var placeholderIcon = $(this).data('placeholder-icon');
				$(this).parent('.dropdown').find('.dropdown-clear').remove();
				if(placeholderIcon){
					$(this).prepend('<option value="" data-icon="' + placeholderIcon + '" disabled>' + placeholder + '</option>');
				}
				$(this).val('');
				$(this).selectpicker('refresh');
				$(this).trigger('change');
			});
			target.find('[data-toggle="dropdown"]').each(function(){
				var emptyValueFound = false;
				var first = true;
				var firstValue;
				$(this).find('option').each(function(){
					if(first){
						first = false;
						firstValue = $(this).attr('value');
					}
					if($(this).attr('value') == ''){
						emptyValueFound = true;
						return false;
					}
				});
				if(emptyValueFound){
					$(this).val('');
				} else {
					$(this).val(firstValue);
				}
				$(this).selectpicker('refresh');
				$(this).trigger('change');
			});
			target.trigger('reset');
		});
	},
	initLoadingButtons: function () {
		$('.btn-loading').append('<div class="loading"><div></div></div>');
	},
	initFilterCollapse: function () {
		$('[data-toggle="collapse-filter"]').each(function(){
			var collapseBtn = $(this);
			var filter = $($(this).data('target'));
			var resetBtn = $($(this).data('reset-btn'))
			collapseBtn.click(function(){
				filter.collapse('toggle');
				collapseBtn.toggleClass('active');
				resetBtn.toggleClass('collapse collapsed');
			})
		});
	},
	initAlerts: function () {
		setTimeout(function(){
			$('.alert-top').fadeOut();
		}, 3000);

	},
	generateUniqueId: function () {
		return '_' + Math.random().toString(36).substr(2, 9);
	},
};
$("document").ready(function () {
    app.init();
});

// Extend datepicker
$.fn.datepicker.Constructor.prototype.getViewDate = function () {
    var viewDate = this.viewDate;
    // viewDate.setMinutes(viewDate.getMinutes() + viewDate.getTimezoneOffset());
    return viewDate;
}

$.fn.datepicker.Constructor.prototype.updateViewMonthYear = function (month, year, date) {
	this.viewDate.setUTCMonth(month);
    this.viewDate.setUTCFullYear(year);
    this.fill();
    return this;
}

$.fn.datepicker.Constructor.prototype.updateViewNextMonth = function () {
    var month = this.viewDate.getMonth(),
        year = this.viewDate.getFullYear();

    if (month == 11) {
        this.viewDate.setUTCMonth(0);
        this.viewDate.setUTCFullYear(year + 1);
    } else {
        this.viewDate.setUTCMonth(month + 1);
    }
    this.fill();
    return this.viewDate;
}

$.fn.datepicker.Constructor.prototype.updateViewPrevMonth = function () {
	var month = this.viewDate.getMonth(),
        year = this.viewDate.getFullYear();

    if (month == 0) {
        this.viewDate.setUTCMonth(11);
        this.viewDate.setUTCFullYear(year - 1);
    } else {
        this.viewDate.setUTCMonth(month - 1);
    }
    this.fill();
    return this.viewDate;
}

// fix missing padStart
if (!String.prototype.padStart) {
    String.prototype.padStart = function padStart(targetLength, padString) {
        targetLength = targetLength >> 0; //truncate if number or convert non-number to 0;
        padString = String((typeof padString !== 'undefined' ? padString : ' '));
        if (this.length > targetLength) {
            return String(this);
        } else {
            targetLength = targetLength - this.length;
            if (targetLength > padString.length) {
                padString += padString.repeat(targetLength / padString.length); //append to original to ensure we are longer than needed
            }
            return padString.slice(0, targetLength) + String(this);
        }
    };
}