<?php
declare(strict_types=1);

namespace Test\Cleevio\Users\Front;

$container = require __DIR__ . '/../../../bootstrap.php';

use Nette;
use Tester\Assert;
use Tester\DomQuery;
use Tester\TestCase;

class LoginPresenter extends TestCase
{

	/**
	 * @var object|Nette\Application\IPresenterFactory|null
	 */
	private $presenterFactory;

	/**
	 * LoginPresenter constructor.
	 * @param Nette\DI\Container $container
	 */
	function __construct(Nette\DI\Container $container)
	{
		$this->presenterFactory = $container->getByType('Nette\Application\IPresenterFactory');
	}

	function testDefault()
	{
		$presenter = $this->presenterFactory->createPresenter('Users:Front:Login');
		$presenter->autoCanonicalize = false;

		$request = new Nette\Application\Request('Login', 'GET', array('action' => 'default'));
		$response = $presenter->run($request);

		Assert::type('Nette\Application\Responses\TextResponse', $response);
		Assert::type('Nette\Bridges\ApplicationLatte\Template', $response->getSource());

		$html = (string)$response->getSource();
		$dom = DomQuery::fromHtml($html);

		Assert::true($dom->has('input[name="username"]'));
		Assert::true($dom->has('input[name="password"]'));

	}
}

$test = new LoginPresenter($container);
$test->run();
