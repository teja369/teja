<?php
declare(strict_types=1);

namespace Test\Cleevio\Users\Back;

$container = require __DIR__ . '/../../../bootstrap.php';

use Cleevio\ApiClient\DTO\Authorization;
use Cleevio\ApiClient\IContext;
use Nette;
use Tester\Assert;
use Tester\TestCase;

class DefaultPresenter extends TestCase
{

    /**
     * @var object|Nette\Application\IPresenterFactory|null
     */
    private $presenterFactory;

    /**
     * @var Nette\Security\User|object|null
     */
    private $user;

    /**
     * @var IContext|object|null
     */
    private $context;

    /**
     * LoginPresenter constructor.
     * @param Nette\DI\Container $container
     */
    function __construct(Nette\DI\Container $container)
    {
        $this->user = $container->getByType(Nette\Security\User::class);
        $this->presenterFactory = $container->getByType('Nette\Application\IPresenterFactory');
        $this->context = $container->getByType(IContext::class);
    }

    function testDefault()
    {
        $presenter = $this->presenterFactory->createPresenter('Users:Front:Default');
        $presenter->autoCanonicalize = false;

        $request = new Nette\Application\Request('Default', 'GET', array('action' => 'default'));
        $response = $presenter->run($request);

        Assert::type('Nette\Application\Responses\TextResponse', $response);
        Assert::type('Nette\Bridges\ApplicationLatte\Template', $response->getSource());
    }

    /**
     */
    protected function setUp()
    {
        parent::setUp();

        // Set authorized user credentials
        $this->context->setAuthorization(new Authorization('', '', time() + 3600));

    }
}

$test = new DefaultPresenter($container);
$test->run();
