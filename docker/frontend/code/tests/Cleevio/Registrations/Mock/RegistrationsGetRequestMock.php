<?php

declare(strict_types=1);

namespace Tests\Cleevio\Registrations\API\Requests;

use Cleevio\Registrations\API\Requests\IRegistrationsGetRequest;
use Cleevio\Registrations\API\Requests\RegistrationsGetRequest;
use Cleevio\Registrations\API\Responses\RegistrationsResponse;
use Test\Cleevio\Registrations\Front\Registration;

class RegistrationsGetRequestMock extends RegistrationsGetRequest implements IRegistrationsGetRequest
{

    /**
     * Execute request and return a response
     * @return RegistrationsResponse
     */
    function execute(): RegistrationsResponse
    {
        return new RegistrationsResponse(['items' => Registration::multiple(10)]);
    }
}
