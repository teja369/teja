<?php

declare(strict_types=1);

namespace Test\Cleevio\Trips\Front;

use Cleevio\Translations\API\Requests\TranslationGetRequest;
use Cleevio\Translations\API\Responses\TranslationsResponse;
use Test\Cleevio\Translations\Front\Translation;

class TranslationGetRequestMock extends TranslationGetRequest
{

    /**
     * @return TranslationsResponse
     */
    function execute(): TranslationsResponse
    {
        return new TranslationsResponse(Translation::multiple(1));
    }
}
