<?php

declare(strict_types=1);

namespace Test\Cleevio\Trips\Front;

use Cleevio\ApiClient\Exception\BadRequestException;
use Cleevio\Translations\API\Requests\LanguagesGetRequest;
use Cleevio\Translations\API\Responses\LanguagesResponse;
use Cleevio\Trips\API\Responses\TripResponse;
use Cleevio\Trips\API\Requests\TripGetRequest;
use Test\Cleevio\Translations\Front\Language;

class LanguagesGetRequestMock extends LanguagesGetRequest
{

    /**
     * @return LanguagesResponse
     */
    function execute(): LanguagesResponse
    {
        return new LanguagesResponse(Language::multiple(1));
    }
}
