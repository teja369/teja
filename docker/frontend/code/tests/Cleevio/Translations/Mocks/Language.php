<?php

declare(strict_types=1);

namespace Test\Cleevio\Translations\Front;

use Test\Cleevio\Countries\Front\Country;

class Language
{

    /**
     * Generate single host
     *
     * @param int $id
     * @return array
     */
    public static function generate(int $id = 1): array
    {
        return [
            'id' => $id,
            'code' => 'en',
            'name' => 'English',
            'country' => Country::generate(),
        ];
    }

    public static function multiple(int $count = 1): array
    {
        $output = [];

        for ($i = 0; $i < $count; $i++) {
            $output[] = self::generate($i);
        }

        return $output;
    }
}