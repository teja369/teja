<?php

declare(strict_types=1);

namespace Tests\Cleevio\Questions\API\Requests;

use Cleevio\Questions\API\Requests\IQuestionsGetRequest;
use Cleevio\Questions\API\Requests\QuestionsGetRequest;
use Cleevio\Questions\API\Responses\QuestionsResponse;
use Test\Cleevio\Questions\Front\Question;

class QuestionsGetRequestMock extends QuestionsGetRequest implements IQuestionsGetRequest
{

    /**
     * Execute request and return a response
     * @return QuestionsResponse
     */
    function execute(): QuestionsResponse
    {
        return new QuestionsResponse(Question::multiple(10));
    }
}
