<?php

declare(strict_types=1);

namespace Tests\Cleevio\Questions\API\Requests;

use Cleevio\Questions\API\Requests\GroupsGetRequest;
use Cleevio\Questions\API\Requests\IGroupsGetRequest;
use Cleevio\Questions\API\Responses\GroupsResponse;
use Test\Cleevio\Questions\Front\Group;

class GroupsGetRequestMock extends GroupsGetRequest implements IGroupsGetRequest
{

    /**
     * Execute request and return a response
     * @return GroupsResponse
     */
    function execute(): GroupsResponse
    {
        return new GroupsResponse(Group::multiple(10));
    }

}
