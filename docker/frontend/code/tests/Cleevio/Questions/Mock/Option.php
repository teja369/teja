<?php

declare(strict_types=1);

namespace Test\Cleevio\Questions\Front;

class Option
{

    /**
     * Generate single host
     *
     * @param int $id
     * @return array
     */
    public static function generate(int $id = 1): array
    {
        return [
            'id' => $id,
            'type' => 'text',
            'name' => '',
        ];
    }

    public static function multiple(int $count = 1): array
    {
        $output = [];

        for ($i = 0; $i < $count; $i++) {
            $output[] = self::generate($i);
        }

        return $output;
    }
}