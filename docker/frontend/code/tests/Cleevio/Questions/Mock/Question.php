<?php

declare(strict_types=1);

namespace Test\Cleevio\Questions\Front;

class Question
{

    /**
     * Generate single host
     *
     * @param int $id
     * @return array
     */
    public static function generate(int $id = 1): array
    {
        return [
            'id' => $id,
            'type' => 'text',
            'parent' => null,
            'parentRule' => null,
            'question' => sprintf('question_%s', $id),
            'group' => Group::generate(),
            'validation' => null,
            'required' => true,
            'placeholder' => '',
            'text' => [
                'minLength' => 10,
                'maxLength' => null,
            ],
            'bool' => null,
            'number' => null,
        ];
    }

    public static function multiple(int $count = 1): array
    {
        $output = [];

        for ($i = 0; $i < $count; $i++) {
            $output[] = self::generate($i);
        }

        return $output;
    }
}