<?php

declare(strict_types=1);

namespace Tests\Cleevio\Questions\API\Requests;

use Cleevio\Questions\API\Requests\GroupsGetRequest;
use Cleevio\Questions\API\Requests\IGroupsGetRequest;
use Cleevio\Questions\API\Requests\IQuestionOptionGetRequest;
use Cleevio\Questions\API\Requests\QuestionOptionGetRequest;
use Cleevio\Questions\API\Responses\GroupsResponse;
use Cleevio\Questions\API\Responses\QuestionOptionsPaginatedResponse;
use Test\Cleevio\Questions\Front\Option;

class QuestionOptionGetRequestMock extends QuestionOptionGetRequest implements IQuestionOptionGetRequest
{

    /**
     * @return QuestionOptionsPaginatedResponse
     */
    function execute(): QuestionOptionsPaginatedResponse
    {
        return new QuestionOptionsPaginatedResponse([
            'items' => Option::multiple(10)
        ]);
    }

}
