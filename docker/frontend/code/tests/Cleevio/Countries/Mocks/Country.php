<?php

declare(strict_types=1);

namespace Test\Cleevio\Countries\Front;

class Country
{

    /**
     * Generate single field
     *
     * @return array
     */
    public static function generate(): array
    {
        return [
            'code' => substr(uniqid(), 0, 2),
            'name' => uniqid(),
            "isEnabled" => true,
            "isHome" => false];
    }

    public static function multiple(int $count = 1): array
    {
        $output = [];

        for ($i = 0; $i < $count; $i++) {
            $output[] = self::generate();
        }

        return $output;
    }
}