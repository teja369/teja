<?php

declare(strict_types=1);

namespace Test\Cleevio\Trips\Front;

use Cleevio\Countries\API\Requests\CountriesGetRequest;
use Cleevio\Countries\API\Requests\ICountriesGetRequest;
use Cleevio\Countries\API\Responses\CountriesPagedResponse;
use Test\Cleevio\Countries\Front\Country;

class CountriesGetRequestMock extends CountriesGetRequest implements ICountriesGetRequest
{

    /**
     * Execute request and return a response
     * @return CountriesPagedResponse
     */
    function execute(): CountriesPagedResponse
    {
        return new CountriesPagedResponse(["data" => [
            'items' => Country::multiple(10),
        ]]);
    }
}
