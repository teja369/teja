<?php

declare(strict_types=1);

namespace Test\Cleevio\Trips\Front;

use Cleevio\Countries\API\Requests\CountriesGetRequest;
use Cleevio\Countries\API\Requests\CountryHomeGetRequest;
use Cleevio\Countries\API\Requests\ICountriesGetRequest;
use Cleevio\Countries\API\Requests\ICountryHomeGetRequest;
use Cleevio\Countries\API\Responses\CountriesPagedResponse;
use Cleevio\Countries\API\Responses\CountriesResponse;
use Cleevio\Countries\API\Responses\CountryResponse;
use Test\Cleevio\Countries\Front\Country;

class CountryHomeGetRequestMock extends CountryHomeGetRequest implements ICountryHomeGetRequest
{

    /**
     * Execute request and return a response
     * @return CountryResponse
     */
    function execute(): CountryResponse
    {
        return new CountryResponse(Country::generate());
    }
}
