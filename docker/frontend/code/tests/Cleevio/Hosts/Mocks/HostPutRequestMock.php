<?php

declare(strict_types=1);

namespace Test\Cleevio\Hosts\Front;

use Cleevio\Hosts\API\Requests\HostPutRequest;
use Cleevio\Hosts\API\Requests\IHostPutRequest;
use Cleevio\Hosts\API\Responses\HostResponse;

class HostPutRequestMock extends HostPutRequest implements IHostPutRequest
{

    /**
     * @return HostResponse
     */
    function execute(): HostResponse
    {
        return new HostResponse(Host::generate());
    }
}
