<?php

declare(strict_types=1);

namespace Test\Cleevio\Hosts\Front;

use Test\Cleevio\Fields\Front\Field;

class Host
{

    /**
     * Generate single host
     *
     * @param int $id
     * @return array
     */
    public static function generate(int $id = 1): array
    {
        return [
            'id' => $id,
            'type' => 'client',
            'name' => sprintf('name_%s', $id),
            'address' => sprintf('address_%s', $id),
            'params' => [
                [
                    'id' => rand(),
                    'field' => Field::generate($id),
                    'value' => 'Street 589',
                ]
            ],
        ];
    }

    public static function multiple(int $count = 1): array
    {
        $output = [];

        for ($i = 0; $i < $count; $i++) {
            $output[] = self::generate($i);
        }

        return $output;
    }
}