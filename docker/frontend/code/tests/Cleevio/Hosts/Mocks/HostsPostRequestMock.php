<?php

declare(strict_types=1);

namespace Test\Cleevio\Hosts\Front;

use Cleevio\Countries\API\Responses\CountriesResponse;
use Cleevio\Hosts\API\Requests\HostsGetRequest;
use Cleevio\Hosts\API\Requests\HostsPostRequest;
use Cleevio\Hosts\API\Requests\IHostsGetRequest;
use Cleevio\Hosts\API\Requests\IHostsPostRequest;
use Cleevio\Hosts\API\Responses\HostResponse;
use Cleevio\Hosts\API\Responses\HostsResponse;

class HostsPostRequestMock extends HostsPostRequest implements IHostsPostRequest
{

    /**
     * @return HostResponse
     */
    function execute(): HostResponse
    {
        return new HostResponse(Host::generate());
    }
}
