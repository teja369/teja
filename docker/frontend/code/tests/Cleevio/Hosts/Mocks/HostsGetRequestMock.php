<?php

declare(strict_types=1);

namespace Test\Cleevio\Hosts\Front;

use Cleevio\Hosts\API\Requests\HostsGetRequest;
use Cleevio\Hosts\API\Requests\IHostsGetRequest;
use Cleevio\Hosts\API\Responses\HostsResponse;

class HostsGetRequestMock extends HostsGetRequest implements IHostsGetRequest
{

    /**
     * Execute request and return a response
     * @return HostsResponse
     */
    function execute(): HostsResponse
    {
        return new HostsResponse(['items' => Host::multiple(10)]);
    }
}
