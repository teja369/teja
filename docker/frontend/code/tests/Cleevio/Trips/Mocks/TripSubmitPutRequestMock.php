<?php

declare(strict_types=1);

namespace Test\Cleevio\Trips\Front;

use Cleevio\ApiClient\DTO\EmptyResponse;
use Cleevio\Trips\API\Requests\ITripDeleteRequest;
use Cleevio\Trips\API\Requests\ITripSubmitPutRequest;
use Cleevio\Trips\API\Requests\TripDeleteRequest;
use Cleevio\Trips\API\Requests\TripSubmitPutRequest;

class TripSubmitPutRequestMock extends TripSubmitPutRequest implements ITripSubmitPutRequest
{

	/**
	 * @return EmptyResponse
	 */
	function execute(): EmptyResponse
	{
		return new EmptyResponse;
	}
}
