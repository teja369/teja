<?php

declare(strict_types=1);

namespace Test\Cleevio\Trips\Front;

use Cleevio\ApiClient\Exception\BadRequestException;
use Cleevio\Trips\API\Requests\ITripCancelPutRequest;
use Cleevio\Trips\API\Requests\ITripFavoritePutRequest;
use Cleevio\Trips\API\Requests\TripCancelPutRequest;
use Cleevio\Trips\API\Requests\TripFavoritePutRequest;
use Cleevio\Trips\API\Responses\TripResponse;
use Cleevio\Trips\API\Requests\TripGetRequest;
use Test\Cleevio\Hosts\Front\Host;

class TripCancelPutRequestMock extends TripCancelPutRequest implements ITripCancelPutRequest
{

	/**
	 * @return TripResponse
	 * @throws BadRequestException
	 */
	function execute(): TripResponse
	{
		return new TripResponse(Trip::generate());
	}
}
