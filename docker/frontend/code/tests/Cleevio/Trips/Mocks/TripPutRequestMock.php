<?php

declare(strict_types=1);

namespace Test\Cleevio\Trips\Front;

use Cleevio\ApiClient\Exception\BadRequestException;
use Cleevio\Trips\API\Requests\TripPutRequest;
use Cleevio\Trips\API\Responses\TripResponse;
use Cleevio\Trips\API\Requests\TripGetRequest;
use Test\Cleevio\Hosts\Front\Host;

class TripPutRequestMock extends TripPutRequest
{

    /**
     * @return TripResponse
     * @throws BadRequestException
     */
    function execute(): TripResponse
    {
        return new TripResponse(Trip::generate());
    }
}
