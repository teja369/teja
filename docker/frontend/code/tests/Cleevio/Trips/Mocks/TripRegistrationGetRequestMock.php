<?php

declare(strict_types=1);

namespace Test\Cleevio\Trips\Front;

use Cleevio\Trips\API\Requests\ITripRegistrationGetRequest;
use Cleevio\Trips\API\Requests\TripRegistrationGetRequest;
use Cleevio\Trips\API\Responses\TripResponse;
use Cleevio\ApiClient\Exception\BadRequestException;

class TripRegistrationGetRequestMock extends TripRegistrationGetRequest implements ITripRegistrationGetRequest
{

	/**
	 * @return TripResponse
	 * @throws BadRequestException
	 */
	function execute(): TripResponse
	{
		return new TripResponse(Trip::generate());
	}
}
