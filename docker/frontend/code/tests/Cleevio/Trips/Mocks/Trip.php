<?php

declare(strict_types=1);

namespace Test\Cleevio\Trips\Front;

use Test\Cleevio\Countries\Front\Country;
use Test\Cleevio\Hosts\Front\Host;
use Test\Cleevio\Questions\Front\Question;

class Trip
{

    /**
     * Generate single host
     *
     * @param int $id
     * @return array
     */
    public static function generate(int $id = 1): array
    {
        return [
            'id' => $id,
            'start' => '2019-01-12',
            'end' => '2019-02-12',
            'country' => Country::generate(),
            'host' => Host::generate(),
            'registration' => [
                'registrations' => [
                    ['id' => 1, 'name' => 'PWD'],
                ],
                'checked' => '2019-12-02 15:43:07',
            ],
            'isFavorite' => true,
            'answers' => [
                [
                    'id' => 1,
                    'question' => Question::generate($id),
                    'text' => 'I am fine',
                    'bool' => null,
                    'number' => null,
                    'choice' => null,
                ],
            ],
            'state' => 'draft',
            'createdAt' => '2019-12-12 20:00:00',
            'updatedAt' => '2019-12-12 20:00:00',
        ];
    }

    public static function multiple(int $count = 1): array
    {
        $output = [];

        for ($i = 0; $i < $count; $i++) {
            $output[] = self::generate($i);
        }

        return $output;
    }
}