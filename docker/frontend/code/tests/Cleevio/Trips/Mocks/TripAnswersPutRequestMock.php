<?php

declare(strict_types=1);

namespace Test\Cleevio\Trips\Front;

use Cleevio\ApiClient\Exception\BadRequestException;
use Cleevio\Trips\API\Requests\ITripAnswersPutRequest;
use Cleevio\Trips\API\Requests\TripAnswersPutRequest;
use Cleevio\Trips\API\Responses\TripResponse;
use Cleevio\Trips\API\Requests\TripGetRequest;
use Test\Cleevio\Hosts\Front\Host;

class TripAnswersPutRequestMock extends TripAnswersPutRequest implements ITripAnswersPutRequest
{

    /**
     * @return TripResponse
     * @throws BadRequestException
     */
    function execute(): TripResponse
    {
        return new TripResponse(Trip::generate());
    }
}
