<?php

declare(strict_types=1);

namespace Test\Cleevio\Trips\Front;

use Cleevio\ApiClient\Exception\BadRequestException;
use Cleevio\Trips\API\Requests\TripStepGetRequest;
use Cleevio\Trips\API\Responses\TripResponse;
use Cleevio\Trips\API\Requests\TripGetRequest;
use Cleevio\Trips\API\Responses\TripStepResponse;

class TripStepGetRequestMock extends TripStepGetRequest
{

    /**
     * @return TripStepResponse
     */
    function execute(): TripStepResponse
    {
        return new TripStepResponse(
            [
                'step' => null,
                'params' => []
            ]
        );
    }
}
