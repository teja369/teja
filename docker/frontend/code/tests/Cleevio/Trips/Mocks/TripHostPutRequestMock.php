<?php

declare(strict_types=1);

namespace Test\Cleevio\Trips\Front;

use Cleevio\Trips\API\Requests\ITripHostPutRequest;
use Cleevio\Trips\API\Requests\TripHostPutRequest;
use Cleevio\Trips\API\Responses\TripResponse;
use Cleevio\ApiClient\Exception\BadRequestException;
use Test\Cleevio\Hosts\Front\Host;

class TripHostPutRequestMock extends TripHostPutRequest implements ITripHostPutRequest
{

	/**
	 * @return TripResponse
	 * @throws BadRequestException
	 */
	function execute(): TripResponse
	{
		return new TripResponse(Trip::generate());
	}
}
