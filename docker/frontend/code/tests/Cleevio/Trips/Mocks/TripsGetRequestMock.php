<?php

declare(strict_types=1);

namespace Test\Cleevio\Trips\Front;

use Cleevio\Trips\API\Requests\TripsGetRequest;
use Cleevio\Trips\API\Responses\TripsResponse;
use Cleevio\ApiClient\Exception\BadRequestException;
use Test\Cleevio\Hosts\Front\Host;

class TripsGetRequestMock extends TripsGetRequest
{

    /**
     * @return TripsResponse
     */
    function execute(): TripsResponse
    {
        return new TripsResponse(['data' => [
            "total_items" => 10,
            "current_items_count" => 10,
            "total_pages" => 1,
            "page" => 1,
            "items_per_page" => 1,
            "next_link" => "api/v1/trips?page=1",
            "previous_link" => "api/v1/trips?page=1",
            'items' => Trip::multiple(10),
        ]]);
    }
}
