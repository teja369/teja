<?php

declare(strict_types=1);

namespace Test\Cleevio\Trips\Front;

use Cleevio\ApiClient\DTO\EmptyResponse;
use Cleevio\Trips\API\Requests\ITripDeleteRequest;
use Cleevio\Trips\API\Requests\TripDeleteRequest;

class TripDeleteRequestMock extends TripDeleteRequest implements ITripDeleteRequest
{

	/**
	 * @return EmptyResponse
	 */
	function execute(): EmptyResponse
	{
		return new EmptyResponse;
	}
}
