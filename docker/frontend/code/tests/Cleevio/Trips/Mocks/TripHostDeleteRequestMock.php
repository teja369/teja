<?php

declare(strict_types=1);

namespace Test\Cleevio\Trips\Front;

use Cleevio\ApiClient\Exception\BadRequestException;
use Cleevio\Trips\API\Requests\ITripHostDeleteRequest;
use Cleevio\Trips\API\Requests\TripHostDeleteRequest;
use Cleevio\Trips\API\Responses\TripResponse;
use Test\Cleevio\Hosts\Front\Host;

class TripHostDeleteRequestMock extends TripHostDeleteRequest implements ITripHostDeleteRequest
{

    /**
     * @return TripResponse
     * @throws BadRequestException
     */
    function execute(): TripResponse
    {
        return new TripResponse(Trip::generate());
    }
}
