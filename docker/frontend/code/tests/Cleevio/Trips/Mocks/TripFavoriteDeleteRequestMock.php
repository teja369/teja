<?php

declare(strict_types=1);

namespace Test\Cleevio\Trips\Front;

use Cleevio\ApiClient\Exception\BadRequestException;
use Cleevio\Trips\API\Requests\ITripFavoriteDeleteRequest;
use Cleevio\Trips\API\Requests\TripFavoriteDeleteRequest;
use Cleevio\Trips\API\Responses\TripResponse;
use Cleevio\Trips\API\Requests\TripGetRequest;
use Test\Cleevio\Hosts\Front\Host;

class TripFavoriteDeleteRequestMock extends TripFavoriteDeleteRequest implements ITripFavoriteDeleteRequest
{

	/**
	 * @return TripResponse
	 * @throws BadRequestException
	 */
	function execute(): TripResponse
	{
		return new TripResponse(
			[
				'id' => 1,
				'start' => '2019-01-12',
				'end' => '2019-02-12',
				'country' => [
					'id' => 3,
					'code' => 'de',
                    'name' => 'Germany',
                    'icon' => 'icon',
                    "isEnabled" => true,
                    "isHome" => true
				],
				'purposes' => [['id' => 1, 'name' => 'Business trip']],
				'state' => 'draft',
				'host' => Host::generate(),
				'registration' => [
					'registrations' => [
						['id' => 2, 'name' => 'A1'],
					],
					'checked' => '2019-12-02 15:43:07',
				],
				'isFavorite' => true,
				'answers' => [
					[
						'id' => 1,
						'question' => [
							'id' => 2,
							'type' => 'text',
							'question' => 'How are you?',
                            'group' => [
                                'id' => 1,
                                'type' => 'text',
                                'name' => '',
                            ],
							'validation' => '\d.*\d',
							'required' => true,
							'text' => [
								'minLength' => 10,
								'maxLength' => null,
							],
							'bool' => null,
							'number' => null,
						],
						'text' => 'I am fine',
						'bool' => null,
						'number' => null,
						'choice' => null,
					],
					[
						'id' => 3,
						'question' => [
							'id' => 3,
							'type' => 'bool',
							'question' => 'Testing A1 bool question',
                            'group' => [
                                'id' => 1,
                                'type' => 'text',
                                'name' => '',
                            ],
							'validation' => null,
							'required' => true,
							'text' => null,
							'bool' => null,
							'number' => null,
						],
						'text' => null,
						'bool' => true,
						'number' => null,
						'choice' => null,
					],
				],
				'createdAt' => '2019-12-12 20:00:00',
				'updatedAt' => '2019-12-12 20:00:00',
			]
		);
	}
}
