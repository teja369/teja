<?php
declare(strict_types=1);

namespace Test\Cleevio\Trips\Front;

$container = require __DIR__ . '/../../../bootstrap.php';

use Cleevio\API\Context;
use Cleevio\ApiClient\DTO\Authorization;
use Cleevio\ApiClient\DTO\Locale;
use Cleevio\ApiClient\IContext;
use Cleevio\Countries\API\Requests\ICountriesGetRequest;
use Cleevio\Countries\API\Responses\CountryResponse;
use Cleevio\Fields\API\Requests\IFieldsGetRequest;
use Cleevio\Fields\API\Responses\FieldResponse;
use Cleevio\Questions\API\Requests\IQuestionsGetRequest;
use Cleevio\Questions\API\Requests\QuestionsGetRequest;
use Cleevio\Questions\API\Responses\QuestionResponse;
use Cleevio\Registrations\API\Requests\IRegistrationsGetRequest;
use Cleevio\Translations\API\Responses\LanguageResponse;
use Cleevio\Trips\API\Requests\ITripRegistrationGetRequest;
use Cleevio\Trips\API\Requests\TripGetRequest;
use Cleevio\Trips\API\Responses\TripResponse;
use Nette;
use Test\Cleevio\Translations\Front\Language;
use Tester\Assert;
use Tester\DomQuery;
use Tester\TestCase;

class TripCreatePresenter extends TestCase
{

    /**
     * @var object|Nette\Application\IPresenterFactory|null
     */
    private $presenterFactory;

    /**
     * @var ICountriesGetRequest
     */
    private $countriesGetRequest;

    /**
     * @var CountryResponse[]
     */
    private $options;

    /**
     * @var Context
     */
    private $context;

    /**
     * @var FieldResponse[]
     */
    private $fields;

    /**
     * @var IFieldsGetRequest|object|null
     */
    private $fieldsGetRequest;

    /**
     * @var TripGetRequest
     */
    private $tripGetRequest;

    /**
     * @var TripResponse
     */
    private $tripResponse;

    /**
     * @var ITripRegistrationGetRequest
     */
    private $tripRegistrationGetRequest;

    /**
     * @var TripResponse
     */
    private $registrationResponse;

    /**
     * @var IRegistrationsGetRequest|object|null
     */
    private $registrationGetRequest;

    /**
     * @var IQuestionsGetRequest|object|null
     */
    private $questionsGetRequest;

    /**
     * @var QuestionResponse[]
     */
    private $questionsResponse;



	/**
     * TripPresenter constructor.
     * @param Nette\DI\Container $container
     */
    function __construct(
        Nette\DI\Container $container
    )
    {
        $this->presenterFactory = $container->getByType('Nette\Application\IPresenterFactory');
        $this->countriesGetRequest = $container->getByType(ICountriesGetRequest::class);
        $this->fieldsGetRequest = $container->getByType(IFieldsGetRequest::class);
        $this->tripGetRequest = $container->getByType(TripGetRequest::class);
        $this->tripRegistrationGetRequest = $container->getByType(ITripRegistrationGetRequest::class);
        $this->registrationGetRequest = $container->getByType(IRegistrationsGetRequest::class);
        $this->questionsGetRequest = $container->getByType(QuestionsGetRequest::class);
        $this->context = $container->getByType(IContext::class);
    }


    function testCreate()
    {
        $presenter = $this->presenterFactory->createPresenter('Trips:Front:TripCreate');
        $presenter->autoCanonicalize = false;

        $request = new Nette\Application\Request('TripCreate', 'GET', ['action' => 'create']);
        $response = $presenter->run($request);


        Assert::type('Nette\Application\Responses\TextResponse', $response);
        Assert::type('Nette\Bridges\ApplicationLatte\Template', $response->getSource());

        $html = (string)$response->getSource();
        $dom = DomQuery::fromHtml($html);

        Assert::true($dom->has('select[name="country"]'));
        Assert::true($dom->has('input[name="start"]'));
        Assert::true($dom->has('input[name="end"]'));

        /** @var CountryResponse $option */
        foreach ($this->options as $option) {
            if ($option->isHome()) {
                Assert::false($dom->has(sprintf('option[value="%s"]', $option->getCode())));
            } else {
                Assert::true($dom->has(sprintf('option[value="%s"]', $option->getCode())));
            }
        }
    }

    function testCreateHost()
    {
        $presenter = $this->presenterFactory->createPresenter('Trips:Front:TripCreate');
        $presenter->autoCanonicalize = false;

        $request = new Nette\Application\Request('TripCreate', 'GET', [
            'action' => 'createHost',
            'id' => 1,
        ]);

        $response = $presenter->run($request);

        Assert::type('Nette\Application\Responses\TextResponse', $response);
        Assert::type('Nette\Bridges\ApplicationLatte\Template', $response->getSource());

        $html = (string)$response->getSource();
        $dom = DomQuery::fromHtml($html);

        Assert::true($dom->has('input[name="hostType"]'));
        Assert::true($dom->has('input[value="client"]'));
        Assert::true($dom->has('input[value="none"]'));
        Assert::true($dom->has('select[name="client"]'));
        Assert::true($dom->has('input[name="send"]'));
        Assert::true($dom->has(sprintf('input[name="hostType"][checked][value="%s"]', $this->tripResponse->getHost()
            ->getType())));
        Assert::true($dom->has(sprintf('option[selected][value="%s"]', $this->tripResponse->getHost()->getId())));

        foreach ($this->fields as $field) {
            Assert::true($dom->has(sprintf('input[name="%s"]', '_' . $field->getId())));

            $placeholder = (string)$dom->find(sprintf('input[name="%s"]', '_' . $field
                    ->getId()))[0]->attributes()['placeholder'];
            Assert::same($placeholder, $field->getPlaceholder());
        }
    }


    function testCreateRegistration()
    {
        $presenter = $this->presenterFactory->createPresenter('Trips:Front:TripCreate');
        $presenter->autoCanonicalize = false;

        $request = new Nette\Application\Request('TripCreate', 'GET', [
            'action' => 'createRegistration',
            'id' => 1,
        ]);
        $response = $presenter->run($request);

        Assert::type('Nette\Application\Responses\TextResponse', $response);
        Assert::type('Nette\Bridges\ApplicationLatte\Template', $response->getSource());

        $html = (string)$response->getSource();
        $dom = DomQuery::fromHtml($html);

        Assert::true($dom->has('a[href="/trips/trip-create/details/1"]'));

        // Todo fix for general registration
        /*Assert::same((int)$dom->find('.registration')[0]->attributes()['data-id'], $this->registrationResponse->getRegistration()
            ->getRegistrations()[0]->getId());*/
    }


    function testCreateTripQuestionsForm()
    {
        $presenter = $this->presenterFactory->createPresenter('Trips:Front:TripCreate');
        $presenter->autoCanonicalize = false;

        $request = new Nette\Application\Request('TripCreate', 'GET', [
            'action' => 'details',
            'id' => 1,
            'groupId' => 1,
        ]);

        $response = $presenter->run($request);

        Assert::type('Nette\Application\Responses\TextResponse', $response);
        Assert::type('Nette\Bridges\ApplicationLatte\Template', $response->getSource());

        $html = (string)$response->getSource();
        $dom = DomQuery::fromHtml($html);

        /** @var QuestionResponse $question */
        foreach ($this->questionsResponse->getQuestions() as $question) {
            Assert::true($dom->has(sprintf('input[name="%s"]', '_' . $question->getId())));

            if ($question->getType() === 'text') {
                $textRules = (string)$dom->find('input[name="_' . $question->getId() . '"]')[0]->attributes()['data-nette-rules'];
                $placeholder = (string)$dom->find('input[name="_' . $question->getId() . '"]')[0]->attributes()['placeholder'];

                Assert::notNull($question->getText()->getMinLength());
                Assert::true(strpos($textRules, sprintf('"arg":%s', $question->getText()->getMinLength())) !== false);
                Assert::equal($placeholder, $question->getPlaceholder());
            } elseif ($question->getType() === 'number') {
                $placeholder = (string)$dom->find('input[name="_' . $question->getId() . '"]')[0]->attributes()['placeholder'];
                Assert::equal($placeholder, $question->getPlaceholder());
            }

        }

    }


    protected function setUp()
    {
        parent::setUp();

        // Set authorized user credentials
        $this->context->setAuthorization(new Authorization('', '', time() + 3600));
        $this->context->setLocale(new LanguageResponse(Language::generate(1)));

        $this->options = $this->countriesGetRequest->execute()->getItems();
        $this->fields = $this->fieldsGetRequest->execute();
        $this->tripResponse = $this->tripGetRequest->execute();
        $this->tripRegistrationGetRequest->setTripId(1);
        $this->registrationResponse = $this->tripRegistrationGetRequest->execute();
        $this->questionsGetRequest->setTripId(1);
        $this->questionsResponse = $this->questionsGetRequest->execute();
    }

}

$test = new TripCreatePresenter($container);
$test->run();
