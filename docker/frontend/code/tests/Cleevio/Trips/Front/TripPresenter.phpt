<?php
declare(strict_types=1);

namespace Test\Cleevio\Trips\Front;

$container = require __DIR__ . '/../../../bootstrap.php';

use Cleevio\API\Context;
use Cleevio\ApiClient\DTO\Authorization;
use Cleevio\ApiClient\IContext;
use Cleevio\Translations\API\Responses\LanguageResponse;
use Nette;
use Test\Cleevio\Translations\Front\Language;
use Tester\Assert;
use Tester\DomQuery;
use Tester\TestCase;

class TripPresenter extends TestCase
{

	/**
	 * @var object|Nette\Application\IPresenterFactory|null
	 */
	private $presenterFactory;

	/**
	 * @var Context
	 */
	private $context;


	/**
	 * TripPresenter constructor.
	 * @param Nette\DI\Container $container
	 */
	function __construct(
		Nette\DI\Container $container
	)
	{
		$this->presenterFactory = $container->getByType('Nette\Application\IPresenterFactory');
		$this->context = $container->getByType(IContext::class);
	}


	function testDefault()
	{
		$presenter = $this->presenterFactory->createPresenter('Trips:Front:Trip');
		$presenter->autoCanonicalize = false;

		$request = new Nette\Application\Request('Trip', 'GET', ['action' => 'default']);
		$response = $presenter->run($request);

		Assert::type('Nette\Application\Responses\TextResponse', $response);
		Assert::type('Nette\Bridges\ApplicationLatte\Template', $response->getSource());

		$html = (string) $response->getSource();
		$dom = DomQuery::fromHtml($html);

		Assert::true($dom->has('a[href="/trips/trip-create/create"]'));
		Assert::true($dom->has('a[href="/trips/trip/drafts"]'));
	}


	function testDrafts()
	{
		$presenter = $this->presenterFactory->createPresenter('Trips:Front:Trip');
		$presenter->autoCanonicalize = false;

		$request = new Nette\Application\Request('Trip', 'GET', ['action' => 'drafts']);
		$response = $presenter->run($request);

		Assert::type('Nette\Application\Responses\TextResponse', $response);
		Assert::type('Nette\Bridges\ApplicationLatte\Template', $response->getSource());

		$html = (string) $response->getSource();
		$dom = DomQuery::fromHtml($html);
	}


	protected function setUp()
	{
		parent::setUp();

		// Set authorized user credentials
		$this->context->setAuthorization(new Authorization('', '', time() + 3600));
		$this->context->setLocale(new LanguageResponse(Language::generate(1)));
	}

}

$test = new TripPresenter($container);
$test->run();
