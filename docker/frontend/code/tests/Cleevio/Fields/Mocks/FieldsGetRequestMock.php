<?php

declare(strict_types=1);

namespace Tests\Cleevio\Hosts\API\Requests;

use Cleevio\Fields\API\Requests\FieldsGetRequest;
use Cleevio\Fields\API\Responses\FieldsResponse;
use Test\Cleevio\Fields\Front\Field;

class FieldsGetRequestMock extends FieldsGetRequest
{

    /**
     * Execute request and return a response
     * @return FieldsResponse
     */
    function execute(): FieldsResponse
    {
        return new FieldsResponse(Field::multiple(10));
    }
}
