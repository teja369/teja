<?php

declare(strict_types=1);

namespace Test\Cleevio\Fields\Front;

class Field
{

    /**
     * Generate single field
     *
     * @param int $id
     * @return array
     */
    public static function generate(int $id = 1): array
    {
        return [
            'id' => $id,
            'name' => sprintf('name_%s', $id),
            'type' => 'text',
            'placeholder' => 'Example',
            'required' => true,
            "validation" => null
        ];
    }

    public static function multiple(int $count = 1): array
    {
        $output = [];

        for ($i = 0; $i < $count; $i++) {
            $output[] = self::generate($i);
        }

        return $output;
    }
}