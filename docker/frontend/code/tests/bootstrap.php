<?php

error_reporting(E_ALL ^ (E_NOTICE | E_WARNING));

require __DIR__ . '/../vendor/autoload.php';

$configurator = new Nette\Configurator;

/**
 * Load environment variables, it MUST be set.
 * Take a look on .env.example, edit and save it as .env in application root.
 */
$environtmentLoader = (new \Dotenv\Dotenv(__DIR__ . '/..'))->load();

$configurator->setDebugMode(true);
$configurator->setTempDirectory(__DIR__ . '/../temp');

$configurator->createRobotLoader()
    ->addDirectory(__DIR__ . '/../app')
    ->addDirectory(__DIR__ . '/../tests')
    ->register();

$configurator->addParameters([
    'appDir' => __DIR__ . '/../app',
    'wwwDir' => __DIR__ . '/../www',
]);

$configurator->addConfig(__DIR__ . '/../app/config/config.neon');
$configurator->addConfig(__DIR__ . '/../app/config/config.test.neon');

$configurator->addParameters([
    'PROXY_ADDRESS' => null,
    'SSO_URI' => env("SSO_URI"),
    'SSO_TIMEOUT' => (int)getenv("SSO_TIMEOUT"),
    'SMTP_HOST' => env("SMTP_HOST"),
    'SMTP_PORT' => (int)getenv("SMTP_PORT"),
    'SMTP_SECURE' => getenv("SMTP_SECURE") === "" ? null : getenv("SMTP_SECURE"),
    'SMTP_USERNAME' => getenv("SMTP_USERNAME"),
    'SMTP_PASSWORD' => getenv("SMTP_PASSWORD"),
    'EMAIL_ADDRESS_SYSTEM' => getenv("EMAIL_ADDRESS_SYSTEM"),
    'LOGGER_SENTRY_DSN' => getenv("LOGGER_SENTRY_DSN"),
    'LOGGER_SENTRY_ENABLED' => false,
    'LOGGER_MAILER_ENABLED' => false,
	'LOGGER_MAILER_ADDRESS' => getenv("LOGGER_MAILER_ADDRESS"),
]);

define('WWW_DIR', __DIR__ . '/../www');
define('CACHE_DIR', __DIR__ . '/../temp');
define('NETTE_TESTER', true);

if (!file_exists(CACHE_DIR . "/cache/lang")) {
    mkdir(CACHE_DIR . "/cache/lang", 0777, true);
}

return $configurator->createContainer();
