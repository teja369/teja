<?php

declare(strict_types=1);

namespace Cleevio\Sso\Router;

use Nette;
use Nette\Application\Routers\Route;
use Nette\Application\Routers\RouteList;

class RouterFactory
{
	/**
	 * @return Nette\Application\IRouter
	 */
	public function createRouter()
	{
		$router = new RouteList;

		$router[] = $ssoModule = new RouteList('Sso');

		$ssoModule[] = new Route('sso/<presenter>/<action>[/<id>]', [
			'presenter' => 'Login',
			'action' => 'default',
			'module' => 'Back',
		]);

		return $router;
	}

}
