<?php

declare(strict_types=1);

namespace Cleevio\Sso\BackModule\API\Requests;

use Cleevio\ApiClient\BaseRequest;
use Cleevio\ApiClient\Exception\ApiException;
use Cleevio\Sso\BackModule\API\Responses\SamlResponse;

class SamlPostRequest extends BaseRequest implements ISamlPostRequest
{

	/**
	 * Endpoint path
	 * @return string
	 */
	protected function path(): string
	{
		return '/saml/login';
	}

	/**
	 * Method
	 * @return string
	 */
	protected function method(): string
	{
		return 'POST';
	}

	/**
	 * Determines whether request requires authorization
	 * @return bool
	 */
	function requiresAuth(): bool
	{
		return false;
	}

	/**
	 * Execute request and return a response
	 * @return SamlResponse
	 * @throws ApiException
	 */
	function execute(): SamlResponse
	{
		return new SamlResponse($this->request());
	}
}
