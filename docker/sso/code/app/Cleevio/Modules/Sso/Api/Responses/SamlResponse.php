<?php

declare(strict_types=1);

namespace Cleevio\Sso\BackModule\API\Responses;

use Cleevio\ApiClient\IResponse;
use InvalidArgumentException;

class SamlResponse implements IResponse
{

	/**
	 * @var string
	 */
	private $accessToken;

	/**
	 * @var string
	 */
	private $email;

	/**
	 * @var string
	 */
	private $endpoint;

	/**
	 * DestinationResponse constructor.
	 * @param array $response
	 */
	public function __construct(array $response)
	{
		if (array_key_exists('email', $response) &&
			array_key_exists('accessToken', $response) &&
			array_key_exists('endpoint', $response)) {
			$this->email = $response['email'];
			$this->accessToken = $response['accessToken'];
			$this->endpoint = $response['endpoint'];
		} else {
			throw new InvalidArgumentException;
		}
	}

	public function getEmail(): string
	{
		return $this->email;
	}

	public function getAccessToken(): string
	{
		return $this->accessToken;
	}

	public function getEndpoint(): string
	{
		return $this->endpoint;
	}
}
