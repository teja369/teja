<?php

declare(strict_types=1);

namespace Cleevio\Sso\BackModule;

use Cleevio\Organizations\Organization;

interface ISsoService
{

	/**
	 * Extract organization ID from SAML response
	 * @param string $saml
	 * @return Organization|null
	 */
	public function getOrganization(string $saml): ?Organization;

}
