<?php

declare(strict_types=1);

namespace Cleevio\Sso\DI;

use Cleevio\Modules\Providers\IPresenterMappingProvider;
use Cleevio\Modules\Providers\IRouterProvider;
use Cleevio\Sso\Router\RouterFactory;
use Nette\DI\CompilerExtension;

class SsoExtension extends CompilerExtension implements IPresenterMappingProvider, IRouterProvider
{

	public function loadConfiguration()
	{
		$this->compiler->loadConfig(__DIR__ . '/services.neon');
	}


	public function getPresenterMapping(): array
	{
		return ['Sso' => 'Cleevio\\Sso\\*Module\\Presenters\\*Presenter'];
	}


	public function getRouterSettings(): array
	{
		return [100 => RouterFactory::class];
	}
}
