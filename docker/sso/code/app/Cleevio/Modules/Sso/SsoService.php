<?php

declare(strict_types=1);

namespace Cleevio\Sso\BackModule;

use Cleevio\Organizations\Organization;
use Cleevio\Organizations\OrganizationsFactory;
use LightSaml\Model\Context\DeserializationContext;
use LightSaml\Model\Protocol\Response;

class SsoService implements ISsoService
{
	protected const ORGANIZATION_PARAM = 'PortalOrgID';

	/**
	 * @var OrganizationsFactory
	 */
	private $organizationsFactory;

	/**
	 * @param OrganizationsFactory $organizationsFactory
	 */
	public function __construct(OrganizationsFactory $organizationsFactory)
	{
		$this->organizationsFactory = $organizationsFactory;
	}

	/**
	 * Extract organization ID from SAML response
	 * @param string $saml
	 * @return Organization|null
	 */
	public function getOrganization(string $saml): ?Organization
	{
		$deserializationContext = new DeserializationContext;
		$deserializationContext->getDocument()->loadXML($saml);

		$response = new Response;
		$response->deserialize($deserializationContext->getDocument()->firstChild, $deserializationContext);

		// Extract organization ID
		$assertion = $response->getFirstAssertion();

		if ($assertion !== null && $assertion->getFirstAttributeStatement() !== null) {
			foreach ($assertion->getFirstAttributeStatement()->getAllAttributes() as $attribute) {
				if ($attribute->getName() === self::ORGANIZATION_PARAM && $attribute->getFirstAttributeValue() !== null) {
					return $this->organizationsFactory->getOrganization((int) $attribute->getFirstAttributeValue());
				}
			}
		}

		return null;
	}
}
