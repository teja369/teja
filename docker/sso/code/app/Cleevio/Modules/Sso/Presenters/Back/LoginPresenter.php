<?php

declare(strict_types=1);

namespace Cleevio\Sso\BackModule\Presenters;

use Cleevio\ApiClient\Exception\ApiException;
use Cleevio\BackModule\Presenters\BasePresenter;
use Cleevio\Sso\BackModule\API\Requests\SamlPostRequest;
use Cleevio\Sso\BackModule\ISsoService;
use Nette\Application\AbortException;
use Nette\Application\BadRequestException;
use Nette\Http\IResponse;

class LoginPresenter extends BasePresenter
{
	private const SAML_POST_ATTRIBUTE = 'SAMLResponse';

	/**
	 * @var ISsoService
	 */
	private $ssoService;

	/**
	 * @var SamlPostRequest
	 */
	private $samlPostRequest;

	/**
	 * @param ISsoService $ssoService
	 * @param SamlPostRequest $samlPostRequest
	 */
	public function __construct(ISsoService $ssoService,
								SamlPostRequest $samlPostRequest)
	{
		parent::__construct();

		$this->ssoService = $ssoService;
		$this->samlPostRequest = $samlPostRequest;
	}

	/**
	 * @throws BadRequestException
	 * @throws AbortException
	 */
	public function actionDefault(): void
	{
		$saml = $this->getHttpRequest()->getPost(self::SAML_POST_ATTRIBUTE);

		if ($saml === null) {
			$this->error('Missing SAML response', IResponse::S400_BAD_REQUEST);
		}

		$xml = str_replace("^", "", $saml);
		$xml = rawurldecode($xml);
		$xml = base64_decode($xml, true);

		if ($xml === false) {
			$this->error('Invalid SAML response', IResponse::S400_BAD_REQUEST);
		}

		$organization = $this->ssoService->getOrganization($xml);

		if ($organization === null) {
			$this->error(sprintf('Organization attribute was not found'), IResponse::S400_BAD_REQUEST);
		}

		try {
			$this->samlPostRequest->setEndpoint($organization->getApiEndpoint());
			$this->samlPostRequest->setHeader("Authorization", 'Bearer ' . $saml);
			$response = $this->samlPostRequest->execute();

			$endpoint = $response->getEndpoint() === 'admin'
				? $organization->getAdminEndpoint()
				: $organization->getTravellerEndpoint();

			$this->redirectUrl(sprintf("%s?accessToken=%s", $endpoint, $response->getAccessToken()));
		} catch (ApiException $e) {
			$this->error($e->getMessage(), $e->getCode());
		}
	}

}
