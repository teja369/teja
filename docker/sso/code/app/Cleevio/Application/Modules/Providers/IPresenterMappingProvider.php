<?php

declare(strict_types=1);

namespace Cleevio\Modules\Providers;

interface IPresenterMappingProvider
{

	public function getPresenterMapping(): array;
}
