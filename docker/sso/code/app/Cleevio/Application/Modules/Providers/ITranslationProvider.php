<?php

declare(strict_types=1);

namespace Cleevio\Modules\Providers;

interface ITranslationProvider
{

	public function getTranslationLocation(): string;
}
