<?php

declare(strict_types=1);

namespace Cleevio\Organizations;

use Doctrine\Common\Collections\Criteria;
use Nettrine\ORM\EntityManagerDecorator;

class OrganizationsFactory
{
	/**
	 * @var EntityManagerDecorator
	 */
	private $em;

	/**
	 * @param EntityManagerDecorator $em
	 */
	public function __construct(EntityManagerDecorator $em)
	{
		$this->em = $em;
	}

	/**
	 * Get organization by id
	 *
	 * @param int $id
	 * @return Organization|null
	 */
	public function getOrganization(int $id): ?Organization
	{
		$organization = $this->em->getRepository(Organization::class)
			->createQueryBuilder("t")
			->addCriteria(Criteria::create()->andWhere(Criteria::expr()->eq("id", $id)))
			->setMaxResults(1)
			->getQuery()->execute();

		return $organization !== null && count($organization) > 0
			? $organization[0]
			: null;
	}
}
