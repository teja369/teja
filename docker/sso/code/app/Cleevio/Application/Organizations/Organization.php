<?php

declare(strict_types=1);

namespace Cleevio\Organizations;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 * @ORM\Table(name="organizations")
 */
class Organization
{

	/**
	 * Organization identification
	 *
	 * @ORM\Id
	 * @ORM\Column(type="integer")
	 * @ORM\GeneratedValue
	 * @var string
	 */
	private $id;

	/**
	 * Api endpoint
	 *
	 * @ORM\Column(type="string", nullable=false)
	 * @var string
	 */
	private $apiEndpoint;

	/**
	 * Traveller endpoint
	 *
	 * @ORM\Column(type="string", nullable=false)
	 * @var string
	 */
	private $travellerEndpoint;

	/**
	 * Admin endpoint
	 *
	 * @ORM\Column(type="string", nullable=false)
	 * @var string
	 */
	private $adminEndpoint;

	/**
	 * @param string $id
	 * @param string $apiEndpoint
	 * @param string $travellerEndpoint
	 * @param string $adminEndpoint
	 */
	public function __construct(string $id, string $apiEndpoint, string $travellerEndpoint, string $adminEndpoint)
	{
		$this->id = $id;
		$this->apiEndpoint = $apiEndpoint;
		$this->travellerEndpoint = $travellerEndpoint;
		$this->adminEndpoint = $adminEndpoint;
	}

	public function getId(): string
	{
		return $this->id;
	}

	public function getApiEndpoint(): string
	{
		return $this->apiEndpoint;
	}

	public function getTravellerEndpoint(): string
	{
		return $this->travellerEndpoint;
	}

	public function getAdminEndpoint(): string
	{
		return $this->adminEndpoint;
	}

}
