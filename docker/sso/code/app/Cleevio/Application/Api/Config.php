<?php

declare(strict_types=1);

namespace Cleevio\API;

use Cleevio\ApiClient\IApiConfig;

class Config implements IApiConfig
{

	/**
	 * Api base URI
	 *
	 * @example http://api.example.com/api/v1
	 * @return string
	 */
	public function getUri(): string
	{
		return "";
	}
}
