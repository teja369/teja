<?php

declare(strict_types=1);

namespace Cleevio\API;

use Cleevio\ApiClient\DTO\Authorization;
use Cleevio\ApiClient\DTO\Locale;
use Cleevio\ApiClient\DTO\Subject;
use Cleevio\ApiClient\IContext;
use Nette\Http\Session;
use Nette\Http\SessionSection;

class Context implements IContext
{
	/**
	 * @var SessionSection
	 */
	private $session;

	/**
	 * @param Session $session
	 */
	public function __construct(
		Session $session
	)
	{
		$this->session = $session->getSection('auth');
	}

	/**
	 * Get stored authorization credentials
	 *
	 * @return Authorization
	 */
	public function getAuthorization(): ?Authorization
	{
		if (!$this->session->offsetExists('authorization')) {
			return null;
		}

		$auth = @unserialize($this->session->authorization);

		return $auth === false
			? null
			: $auth;
	}

	/**
	 * Set new authorization credentials
	 *
	 * @param Authorization $authorization
	 * @return void
	 */
	public function setAuthorization(Authorization $authorization): void
	{
		$this->session->authorization = serialize($authorization);
	}

	/**
	 * Get current language
	 * @return Locale|null
	 */
	public function getLocale(): ?Locale
	{
		if (!$this->session->offsetExists('locale')) {
			return null;
		}

		$locale = @unserialize($this->session->locale);

		return $locale === false
			? null
			: $locale;
	}

	/**
	 * Set language for translation
	 * @param Locale $locale
	 */
	public function setLocale(Locale $locale): void
	{
		$this->session->locale = serialize($locale);
	}

	/**
	 * @return Subject|null
	 */
	public function getSubject(): ?Subject
	{
		if (!$this->session->offsetExists('user')) {
			return null;
		}

		$user = @unserialize($this->session->user);

		return $user === false
			? null
			: $user;
	}

	/**
	 * Set subject
	 * @param Subject $subject
	 */
	public function setSubject(Subject $subject): void
	{
		$this->session->user = serialize($subject);
	}

	/**
	 * Send locale parameter to API
	 * @return bool
	 */
	public function sendLocale(): bool
	{
		return false;
	}

	/**
	 * Clear context data
	 */
	public function clear(): void
	{
		unset($this->session->authorization);
		unset($this->session->user);
	}
}
