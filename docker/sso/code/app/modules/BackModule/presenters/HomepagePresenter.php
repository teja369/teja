<?php

declare(strict_types=1);

namespace Cleevio\BackModule\Presenters;

class HomepagePresenter extends BasePresenter
{

	public function startup(): void
	{
		parent::startup();
	}
}
