<?php

declare(strict_types=1);

namespace Cleevio\BackModule\Presenters;

use Kdyby\Autowired\AutowireComponentFactories;
use Kdyby\Autowired\AutowireProperties;
use Nette\Application\UI\Presenter;

abstract class BasePresenter extends Presenter
{

	use AutowireProperties;
	use AutowireComponentFactories;

	public function startup(): void
	{
		parent::startup();
		$this->autoCanonicalize = false;

		$this->setLayout(__DIR__ . '/templates/@layout.latte');
	}

}
