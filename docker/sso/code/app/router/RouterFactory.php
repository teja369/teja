<?php

declare(strict_types=1);

namespace App;

use Nette;
use Nette\Application\Routers\RouteList;

class RouterFactory
{

	use Nette\StaticClass;

	/**
	 * @return Nette\Application\IRouter
	 */
	public static function createRouter()
	{
		$router = new RouteList;

		$router[] = $backModule = new RouteList('Back');

		$backModule[] = new Nette\Application\Routers\Route('/', 'Homepage:default');

		return $router;
	}

}
