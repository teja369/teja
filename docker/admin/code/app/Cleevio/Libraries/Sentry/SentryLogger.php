<?php

declare(strict_types=1);

namespace Cleevio\Sentry;

use Nette\Application\Application;
use Tracy\Debugger;
use function Sentry\captureException;

class SentryLogger
{

	/**
	 * @var bool
	 */
	private $enabled;


	/**
	 * @param bool $enabled
	 * @param Application $application
	 */
	public function __construct($enabled, Application $application)
	{
		$this->enabled = $enabled;

		Debugger::$onFatalError[] = [$this, 'logTracyFatalError'];
		$application->onError[] = [$this, 'logApplicationError'];
	}


	/**
	 * @param \Throwable $e
	 * @return void
	 */
	public function logTracyFatalError(\Throwable $e)
	{
		$this->logException($e);
	}


	/**
	 * @param Application $application
	 * @param \Throwable $e
	 * @return void
	 */
	public function logApplicationError(Application $application, \Throwable $e)
	{
		unset($application);
		$this->logException($e);
	}


	/**
	 * @param \Throwable $e
	 * @return void
	 */
	public function logException(\Throwable $e)
	{
		if (!$this->enabled) {
			return;
		}

		captureException($e);
	}
}
