<?php

declare(strict_types=1);

namespace Cleevio\Sentry\DI;

use Cleevio\Sentry\SentryLogger;
use Nette;
use function Sentry\init;

class SentryLoggerExtension extends Nette\DI\CompilerExtension
{

	/**
	 * @var array
	 */
	public $defaults = [
		'dsn' => '',
		'enabled' => true, // !$container->parameters['debugMode']
	];

	public function loadConfiguration()
	{
		$container = $this->getContainerBuilder();

		/**
		 * Default value: Turn Sentry logger on/off depending on application debugMode
		 */
		$this->defaults['enabled'] = !$container->parameters['debugMode'];

		$config = $this->getConfig();

		init([
			'dsn' => env('SENTRY_DSN'),
			'environment' => env('ENVIRONMENT_NAME', 'undefined'),
			'release' => gitRelease(),
		]);

		$container->addDefinition($this->prefix('logger'))
			->setFactory(SentryLogger::class)
			->setArguments([$config['enabled']]);
	}
}
