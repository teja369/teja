<?php

declare(strict_types=1);

namespace Cleevio\Registrations\Components\SetRegistrationFormModal;

use Cleevio\Registrations\API\Requests\RegistrationsGetRequest;
use Cleevio\UI\SetRelationFormModal\SetRelationFormModal;
use Nette\Localization\ITranslator;

class SetRegistrationFormModal extends SetRelationFormModal
{
	/**
	 * @param ITranslator $translator
	 * @param RegistrationsGetRequest $registrationsGetRequest
	 */
	public function __construct(ITranslator $translator, RegistrationsGetRequest $registrationsGetRequest)
	{
		parent::__construct($translator, $registrationsGetRequest);
	}

	protected function translationHeadline(): string
	{
		return "back.registrations.modal.headline";
	}
}

interface ISetRegistrationFormModalFactory
{

	/**
	 * @return SetRegistrationFormModal
	 */
	public function create(): SetRegistrationFormModal;
}
