<?php

declare(strict_types=1);

namespace Cleevio\Registrations\Components\SetRegistrationFormModal;

use Cleevio\ApiClient\IRequest;

interface ISetRegistrationRequest extends IRequest
{

	public function setId(int $id): void;
}
