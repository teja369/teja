<?php

declare(strict_types=1);

namespace Cleevio\Registrations\Components\SetRegistrationFormModal;

use Cleevio\ApiClient\IRequest;
use Cleevio\Questions\API\Responses\QuestionRegistrationsResponse;

interface IGetRegistrationRequest extends IRequest
{

	/**
	 * Execute request and return a response
	 * @return QuestionRegistrationsResponse
	 */
	function execute(): QuestionRegistrationsResponse;


	public function setId(int $id): void;

}
