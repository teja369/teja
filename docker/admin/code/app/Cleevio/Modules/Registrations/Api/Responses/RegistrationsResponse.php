<?php

declare(strict_types=1);

namespace Cleevio\Hosts\API\Responses;

use Cleevio\ApiClient\DTO\PaginationResponse;
use Cleevio\Registrations\API\Responses\RegistrationResponse;

class RegistrationsResponse extends PaginationResponse
{

	/**
	 * @var RegistrationResponse[]
	 */
	private $items = [];


	/**
	 * RegistrationsResponse constructor.
	 * @param array $request
	 */
	public function __construct(array $request)
	{
		parent::__construct($request);

		$this->items = array_map(static function ($host) {
			return new RegistrationResponse($host);
		}, $request['items']);
	}


	/**
	 * @return RegistrationResponse[]
	 */
	public function getItems(): array
	{
		return $this->items;
	}

}
