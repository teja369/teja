<?php

declare(strict_types=1);

namespace Cleevio\Registrations\API\Responses;

use Cleevio\UI\ApiFilterForm\IFilterResponse;
use Cleevio\UI\SetFormModal\IRelationResponse;
use InvalidArgumentException;

class RegistrationResponse implements IRelationResponse, IFilterResponse
{

	/**
	 * @var int
	 */
	private $id;

	/**
	 * @var string
	 */
	private $name;

	/**
	 * FieldResponse constructor.
	 * @param array $field
	 */
	public function __construct(array $field)
	{
		if (array_key_exists('id', $field) &&
			array_key_exists('name', $field)) {
			$this->id = $field['id'];
			$this->name = $field['name'];
		} else {
			throw new InvalidArgumentException;
		}
	}


	/**
	 * @return int
	 */
	public function getId(): int
	{
		return $this->id;
	}

	/**
	 * @return string
	 */
	public function getValue(): string
	{
		return (string) $this->id;
	}

	/**
	 * @return string
	 */
	public function getName(): string
	{
		return $this->name;
	}

}
