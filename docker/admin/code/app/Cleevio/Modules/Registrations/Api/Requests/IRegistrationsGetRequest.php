<?php

declare(strict_types=1);

namespace Cleevio\Registrations\API\Requests;

use Cleevio\UI\SearchForm\ISearchRequest;

interface IRegistrationsGetRequest extends ISearchRequest
{

}
