<?php

declare(strict_types=1);

namespace Cleevio\Users\Components\LoginForm;

use Cleevio\API\Context;
use Cleevio\ApiClient\Exception\ApiException;
use Cleevio\UI\BaseControl;
use Cleevio\UI\BaseForm;
use Cleevio\Users\Api\Requests\LoginRequest;
use Cleevio\Users\Api\Requests\UserRequest;
use Nette\Localization\ITranslator;
use Nette\Security\User;

class LoginForm extends BaseControl
{

	/**
	 * @var array
	 */
	public $onSuccess = [];


	/**
	 * @var LoginRequest
	 */
	private $loginRequest;

	/**
	 * @var Context
	 */
	private $context;

	/**
	 * @var ITranslator
	 */
	private $translator;

	/**
	 * @var User
	 */
	private $user;

	/**
	 * @var UserRequest
	 */
	private $userRequest;


	public function __construct(
		ITranslator $translator,
		LoginRequest $loginRequest,
		UserRequest $userRequest,
		Context $context,
		User $user
	)
	{
		$this->loginRequest = $loginRequest;
		$this->context = $context;
		$this->translator = $translator;
		$this->user = $user;
		$this->userRequest = $userRequest;
	}


	public function render(): void
	{
		$this->template->render(__DIR__ . '/templates/default.latte');
	}


	protected function createComponentForm(): BaseForm
	{
		$form = new BaseForm;
		$form->setTranslator($this->translator);

		$form->addText('username', 'back.users.login.default.sign-in.form.label.username')
			->setRequired('back.users.login.default.sign-in.form.validation.required.username');

		$form->addPassword('password', 'back.users.login.default.sign-in.form.label.password')
			->setRequired('back.users.login.default.sign-in.form.validation.required.password');


		$form->addSubmit('send', 'back.users.login.default.sign-in.form.label.submit');

		$form->onSuccess[] = function (BaseForm $form) {
			$values = $form->getValues();

			try {
				// Login user
				$this->loginRequest->setBasicAuth($values->username, $values->password);
				$authorization = $this->loginRequest->execute();
				$this->context->setAuthorization($authorization);

				$user = $this->userRequest->execute();
				$this->context->setSubject($user);

				if($user->getLanguage() !== null) {
					$this->context->setLocale($user->getLanguage());
				}

				$this->user->login($user->getUsername());
				$this->onSuccess();
			} catch (ApiException $e) {
				$this->flashMessage($e->getMessage(), 'danger');
			}

			if ($this->isAjax()) {
				$this->redrawControl('flashes');
			}
		};

		return $form;
	}
}

interface ILoginFormFactory
{

	/**
	 * @return LoginForm
	 */
	function create();
}
