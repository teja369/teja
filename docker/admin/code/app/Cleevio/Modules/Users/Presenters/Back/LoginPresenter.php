<?php

declare(strict_types=1);

namespace Cleevio\Users\BackModule\Presenters;

use Cleevio\API\Context;
use Cleevio\ApiClient\Exception\ApiException;
use Cleevio\BackModule\Presenters\BasePresenter;
use Cleevio\Sso\SsoContext;
use Cleevio\Users\Api\Requests\SsoLoginRequest;
use Cleevio\Users\Api\Requests\UserRequest;
use Cleevio\Users\Components\LoginForm\ILoginFormFactory;
use Cleevio\Users\Components\LoginForm\LoginForm;
use Nette\Application\AbortException;
use Nette\Security\AuthenticationException;

class LoginPresenter extends BasePresenter
{

	/**
	 * @var Context
	 */
	private $apiContext;

	/**
	 * @var SsoLoginRequest
	 */
	private $ssoLoginRequest;

	/**
	 * @var UserRequest
	 */
	private $userRequest;

	/**
	 * @param SsoLoginRequest $ssoLoginRequest
	 * @param Context $apiContext
	 * @param SsoContext $ssoContext
	 * @param UserRequest $userRequest
	 */
	public function __construct(SsoLoginRequest $ssoLoginRequest,
								Context $apiContext,
								SsoContext $ssoContext,
								UserRequest $userRequest)
	{
		parent::__construct();

		$this->ssoLoginRequest = $ssoLoginRequest;
		$this->userRequest = $userRequest;
		$this->apiContext = $apiContext;
		$this->ssoContext = $ssoContext;
	}

	/**
	 * @param string $accessToken
	 * @throws AbortException
	 * @throws AuthenticationException
	 */
	function actionSso(string $accessToken): void
	{
		try {
			$this->ssoLoginRequest->setHeader('Authorization', sprintf('Bearer %s', $accessToken));
			$authorization = $this->ssoLoginRequest->execute();

			$this->apiContext->setAuthorization($authorization);

			// Retrieve user information
			$user = $this->userRequest->execute();
			$this->apiContext->setSubject($user);

			if ($user->getLanguage() !== null) {
				$this->apiContext->setLocale($user->getLanguage());
			}

			$this->getUser()->login($user->getUsername());

			$this->redirect(':Back:Homepage:default');

		} catch (ApiException $e) {
			if ($this->ssoContext->getUri() !== null) {
				$this->redirectUrl($this->ssoContext->getUri());
			}

			$this->flashMessage($e->getMessage(), "danger");
			$this->redirect(':Back:Homepage:default');
		}
	}

	protected function createComponentLoginForm(ILoginFormFactory $factory): LoginForm
	{
		$control = $factory->create();

		$control->onSuccess[] = function () {
			if ($this->isAjax()) {
				$this->payload->forceRedirect = true;
				$this->redirect(':Back:Homepage:default');
			} else {
				$this->redirect(':Back:Homepage:default');
			}
		};

		return $control;
	}

}
