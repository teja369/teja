<?php

declare(strict_types=1);

namespace Cleevio\Users\BackModule\Presenters;

use Cleevio\API\Context;
use Cleevio\BackModule\Presenters\BasePresenter;
use Nette\Application\AbortException;

class LogoutPresenter extends BasePresenter
{

	/**
	 * @var Context
	 */
	private $apiContext;

	/**
	 * @param Context $apiContext
	 */
	public function __construct(Context $apiContext)
	{
		parent::__construct();

		$this->apiContext = $apiContext;
	}

	/**
	 * @throws AbortException
	 */
	function actionSso(): void
	{
		$this->apiContext->clear();

		$this->redirectUrl(sprintf("%s#LOGOUT", $this->ssoContext->getUri()));

	}

}
