<?php

declare(strict_types=1);

namespace Cleevio\Users\Router;

use Nette;
use Nette\Application\Routers\Route;
use Nette\Application\Routers\RouteList;

class RouterFactory
{
	/**
	 * @return Nette\Application\IRouter
	 */
	public function createRouter()
	{
		$router = new RouteList;

		$router[] = $usersModule = new RouteList('Users');

		$usersModule[] = new Route('users/<presenter>/<action>[/<id>]', [
			'presenter' => 'Homepage',
			'action' => 'default',
			'module' => 'Back',
		]);

		return $router;
	}

}
