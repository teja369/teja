<?php

declare(strict_types=1);

namespace Cleevio\Users\Api\Requests;

use Cleevio\Acl\Models\IRole;
use Cleevio\Api\BaseRequest;
use Cleevio\ApiClient\Exception\ApiException;
use Cleevio\Trips\API\Responses\RolesResponse;

class RolesRequest extends BaseRequest implements IRolesRequest
{

	/**
	 * Endpoint path
	 * @return string
	 */
	protected function path(): string
	{
		return "/user/role";
	}

	/**
	 * Method
	 * @return string
	 */
	protected function method(): string
	{
		return "GET";
	}

	/**
	 * Determines whether request requires authorization
	 * @return bool
	 */
	function requiresAuth(): bool
	{
		return true;
	}

	/**
	 * Execute request and return a response
	 * @return RolesResponse
	 * @throws ApiException
	 */
	function execute(): RolesResponse
	{
		return new RolesResponse($this->request());
	}

	/**
	 * @return IRole[]
	 * @throws ApiException
	 */
	function getRoles(): array
	{
		return $this->execute()->getItems();
	}
}
