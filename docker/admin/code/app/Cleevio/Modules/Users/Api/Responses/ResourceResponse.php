<?php

declare(strict_types=1);

namespace Cleevio\Users\Api\Responses;

use Cleevio\Acl\Models\IModel;
use Cleevio\Acl\Models\IResource;
use Cleevio\ApiClient\IResponse;

class ResourceResponse implements IResponse, IResource
{

	/**
	 * @var string
	 */
	private $name;

	/**
	 * @var ResourceResponse|null
	 */
	private $parent;

	/**
	 * @param array $user
	 */
	public function __construct(array $user)
	{
		if (array_key_exists('name', $user) &&
			array_key_exists('parent', $user)) {
			$this->name = $user['name'];
			$this->parent = $user['parent'] !== null
				? new ResourceResponse($user['parent'])
				: null;
		} else {
			throw new \InvalidArgumentException;
		}
	}

	public function getName(): string
	{
		return $this->name;
	}

	/**
	 * Another resource that this resource inherits from
	 * @return IResource|null
	 */
	function getParent(): ?IModel
	{
		return $this->parent;
	}
}
