<?php

declare(strict_types=1);

namespace Cleevio\Users\Api\Requests;

use Cleevio\Acl\Models\IResource;
use Cleevio\Api\BaseRequest;
use Cleevio\ApiClient\Exception\ApiException;
use Cleevio\Trips\API\Responses\ResourcesResponse;

class ResourcesRequest extends BaseRequest implements IResourcesRequest
{

	/**
	 * Endpoint path
	 * @return string
	 */
	protected function path(): string
	{
		return "/user/resource";
	}

	/**
	 * Method
	 * @return string
	 */
	protected function method(): string
	{
		return "GET";
	}

	/**
	 * Determines whether request requires authorization
	 * @return bool
	 */
	function requiresAuth(): bool
	{
		return true;
	}

	/**
	 * Execute request and return a response
	 * @return ResourcesResponse
	 * @throws ApiException
	 */
	function execute(): ResourcesResponse
	{
		return new ResourcesResponse($this->request());
	}

	/**
	 * @return IResource[]
	 * @throws ApiException
	 */
	function getResources(): array
	{
		return $this->execute()->getItems();
	}
}
