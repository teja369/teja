<?php

declare(strict_types=1);

namespace Cleevio\Users\Api\Requests;

use Cleevio\Api\BaseRequest;
use Cleevio\ApiClient\Exception\ApiException;
use Cleevio\Users\Api\Responses\UserResponse;

class UserLanguagePutRequest extends BaseRequest implements IUserLanguagePutRequestRequest
{
	/**
	 * @var string
	 */
	private $code;

	/**
	 * Endpoint path
	 * @return string
	 */
	protected function path(): string
	{
		return sprintf("/user/language/%s", $this->code);
	}

	/**
	 * Method
	 * @return string
	 */
	protected function method(): string
	{
		return "PUT";
	}

	/**
	 * Determines whether request requires authorization
	 * @return bool
	 */
	function requiresAuth(): bool
	{
		return true;
	}

	/**
	 * Execute request and return a response
	 * @return UserResponse
	 * @throws ApiException
	 */
	function execute(): UserResponse
	{
		return new UserResponse($this->request());
	}

	/**
	 * Set language code
	 * @param string $code
	 */
	function setCode(string $code): void
	{
		$this->code = $code;
	}
}
