<?php

declare(strict_types=1);

namespace Cleevio\Users\Api\Requests;

use Cleevio\Acl\Providers\IRolesProvider;

interface IRolesRequest extends IRolesProvider
{

}
