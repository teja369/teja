<?php

declare(strict_types=1);

namespace Cleevio\Users\Api\Requests;

interface IUserLanguagePutRequestRequest
{

	/**
	 * Set language code
	 * @param string $code
	 */
	function setCode(string $code): void;

}
