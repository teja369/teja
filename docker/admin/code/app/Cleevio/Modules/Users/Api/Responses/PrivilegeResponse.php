<?php

declare(strict_types=1);

namespace Cleevio\Users\Api\Responses;

use Cleevio\Acl\Models\IPrivilege;
use Cleevio\Acl\Models\IResource;
use Cleevio\ApiClient\IResponse;
use InvalidArgumentException;

class PrivilegeResponse implements IResponse, IPrivilege
{
	/**
	 * @var string
	 */
	private $privilege;

	/**
	 * @var ResourceResponse
	 */
	private $resource;

	/**
	 * @var bool
	 */
	private $allow;

	/**
	 * @param array $privilege
	 */
	public function __construct(array $privilege)
	{
		if (array_key_exists('privilege', $privilege) &&
			array_key_exists('resource', $privilege) &&
			array_key_exists('allow', $privilege)) {
			$this->privilege = $privilege['privilege'];
			$this->resource = new ResourceResponse($privilege['resource']);
			$this->allow = (bool) $privilege['allow'];

		} else {
			throw new InvalidArgumentException;
		}
	}

	/**
	 * @return IResource
	 */
	function getResource(): IResource
	{
		return $this->resource;
	}

	/**
	 * @return string|null
	 */
	function getPrivilege(): ?string
	{
		return $this->privilege;
	}

	/**
	 * Allow or deny access
	 * @return bool
	 */
	function allow(): bool
	{
		return $this->allow;
	}
}
