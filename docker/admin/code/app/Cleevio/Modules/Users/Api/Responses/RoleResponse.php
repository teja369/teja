<?php

declare(strict_types=1);

namespace Cleevio\Users\Api\Responses;

use Cleevio\Acl\Models\IModel;
use Cleevio\Acl\Models\IPrivilege;
use Cleevio\Acl\Models\IRole;
use Cleevio\ApiClient\IResponse;

class RoleResponse implements IResponse, IRole
{

	/**
	 * @var string
	 */
	private $name;

	/**
	 * @var RoleResponse|null
	 */
	private $parent;

	/**
	 * @var PrivilegeResponse[]
	 */
	private $privileges;

	/**
	 * @param array $role
	 */
	public function __construct(array $role)
	{
		if (array_key_exists('name', $role) &&
			array_key_exists('privileges', $role) &&
			array_key_exists('parent', $role)) {
			$this->name = $role['name'];
			$this->privileges = array_map(static function (array $privilege) {
				return new PrivilegeResponse($privilege);
			}, $role['privileges']);
			$this->parent = $role['parent'] !== null
				? new RoleResponse($role['parent'])
				: null;
		} else {
			throw new \InvalidArgumentException;
		}
	}

	public function getName(): string
	{
		return $this->name;
	}

	/**
	 * @return IPrivilege[]
	 */
	function getPrivileges(): array
	{
		return $this->privileges;
	}

	/**
	 * Another role that this role inherits from
	 * @return IRole|null
	 */
	function getParent(): ?IModel
	{
		return $this->parent;
	}
}
