<?php

declare(strict_types=1);

namespace Cleevio\Trips\API\Responses;

use Cleevio\ApiClient\IResponse;
use Cleevio\Users\Api\Responses\RoleResponse;

class RolesResponse implements IResponse
{

	/**
	 * @var RoleResponse[]
	 */
	private $items;

	/**
	 * RolesResponse constructor.
	 * @param array $response
	 */
	public function __construct(array $response)
	{
		$this->items = array_map(static function ($role) {
			return new RoleResponse($role);
		}, $response);

	}

	/**
	 * @return RoleResponse[]
	 */
	public function getItems(): array
	{
		return $this->items;
	}
}
