<?php

declare(strict_types=1);

namespace Cleevio\Trips\API\Responses;

use Cleevio\ApiClient\IResponse;
use Cleevio\Users\Api\Responses\ResourceResponse;

class ResourcesResponse implements IResponse
{

	/**
	 * @var ResourceResponse[]
	 */
	private $items;

	/**
	 * ResourcesResponse constructor.
	 * @param array $response
	 */
	public function __construct(array $response)
	{
		$this->items = array_map(static function ($role) {
			return new ResourceResponse($role);
		}, $response);

	}

	/**
	 * @return ResourceResponse[]
	 */
	public function getItems(): array
	{
		return $this->items;
	}
}
