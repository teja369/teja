<?php

declare(strict_types=1);

namespace Cleevio\Users;

use Exception;

class EmailAlreadyExists extends Exception
{

}

class UserNotFound extends Exception
{

}

class VerificationCodeNotValid extends Exception
{

}

class InvalidPassword extends Exception
{

}

class PasswordResetTimeLimit extends Exception
{

}
