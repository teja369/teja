<?php

declare(strict_types=1);

namespace Cleevio\Trips\DI;

use Cleevio\Modules\Providers\IPresenterMappingProvider;
use Cleevio\Modules\Providers\IRouterProvider;
use Cleevio\Trips\Router\RouterFactory;
use Nette\DI\CompilerExtension;

class TripsExtension extends CompilerExtension implements IPresenterMappingProvider, IRouterProvider
{

	public function loadConfiguration()
	{
		if (defined('NETTE_TESTER')) {
			$this->compiler->loadConfig(__DIR__ . '/services.test.neon');
		} else {
			$this->compiler->loadConfig(__DIR__ . '/services.neon');
		}
	}


	public function getPresenterMapping(): array
	{
		return ['Trips' => 'Cleevio\\Trips\\*Module\\Presenters\\*Presenter'];
	}


	public function getRouterSettings(): array
	{
		return [100 => RouterFactory::class];
	}
}
