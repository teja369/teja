<?php

declare(strict_types=1);

namespace Cleevio\Trips\Components\FilterForm;

use Cleevio\Countries\API\Requests\CountriesGetRequest;
use Cleevio\UI\ApiFilterForm\FilterForm as BaseFilterForm;
use Cleevio\UI\ApiFilterForm\IFilterInput;
use Cleevio\UI\ApiFilterForm\Inputs\DateInput;
use Cleevio\UI\ApiFilterForm\Inputs\RemoteInput;
use Cleevio\UI\ApiFilterForm\Inputs\SelectInput;
use Cleevio\UI\ApiFilterForm\Inputs\TextInput;
use Nette\Localization\ITranslator;

class FilterForm extends BaseFilterForm
{
	/**
	 * @var CountriesGetRequest
	 */
	private $countriesGetRequest;


	/**
	 * @param ITranslator             $translator
	 * @param CountriesGetRequest     $countriesGetRequest
	 */
	public function __construct(
		ITranslator $translator,
		CountriesGetRequest $countriesGetRequest
	)
	{
		parent::__construct($translator);

		$this->countriesGetRequest = $countriesGetRequest;
	}


	/**
	 * @return IFilterInput[]
	 */
	protected function getInputs(): array
	{
		return [
			new RemoteInput("back.trips.filter.country", "country", $this->countriesGetRequest, 2),
			new SelectInput("back.trips.filter.state", "state", [
				"submitted" => "back.trips.states.submitted",
				"processed" => "back.trips.states.processed",
				"on-hold" => "back.trips.states.on-hold",
			], 2),
			new DateInput("back.trips.filter.dates.start", "start", 2),
			new DateInput("back.trips.filter.dates.end", "end", 2),
			new TextInput("back.trips.filter.search", "search", 2),
		];
	}
}

interface IFilterFormFactory
{

	/**
	 * @return FilterForm
	 */
	function create();
}
