<?php

declare(strict_types=1);

namespace Cleevio\Trips\Components\StateMessageFormModal;

use Cleevio\ApiClient\Exception\ApiException;
use Cleevio\ApiClient\Exception\BadRequestException;
use Cleevio\Trips\API\Requests\TripOnHoldPutRequest;
use Cleevio\UI\BaseControlModal;
use Cleevio\UI\BaseForm;
use Nette\Localization\ITranslator;

class StateMessageFormModal extends BaseControlModal
{

	/**
	 * @var array
	 */
	public $onSuccess = [];

	/**
	 * @var TripOnHoldPutRequest
	 */
	private $tripOnHoldPutRequest;

	/**
	 * @var ITranslator
	 */
	private $translator;


	public function __construct(
		TripOnHoldPutRequest $tripOnHoldPutRequest,
		ITranslator $translator
	)
	{
		parent::__construct();
		$this->tripOnHoldPutRequest = $tripOnHoldPutRequest;
		$this->translator = $translator;
		$this->setTemplateDirectory(__DIR__ . '/templates');
	}


	/**
	 * @return BaseForm
	 */
	protected function createComponentForm(): BaseForm
	{
		$form = new BaseForm;

		$form->setTranslator($this->translator);

		$form->addHidden('id');
		$form->addTextArea('message', 'back.trips.message-modal.label.message')
			->setRequired('back.trips.message-modal.required.message');
		$form->addSubmit('send', 'back.trips.message-modal.label.submit');

		$form->onSuccess[] = function (BaseForm $form) {
			$values = $form->getValues();

			try {
				$this->tripOnHoldPutRequest->setTripId((int) $values->id);
				$this->tripOnHoldPutRequest->setData(['message' => $values->message]);
				$this->tripOnHoldPutRequest->execute();

				$this->onSuccess();
			} catch (BadRequestException $e) {
				$this->flashMessage($this->translator->translate('back.' . $e->getMessage(), $e->getParams()), 'danger');
			} catch (ApiException $e) {
				$this->flashMessage('back.questions.form.error.unknown', 'danger');
			}
		};

		return $form;
	}


	/**
	 * @param int $id
	 */
	public function handleShowModal(int $id): void
	{
		$this['form']['id']->setDefaultValue($id);
		$this->setView('default');
	}

}

interface IStateMessageFormModal
{

	/**
	 * @return StateMessageFormModal
	 */
	function create();
}
