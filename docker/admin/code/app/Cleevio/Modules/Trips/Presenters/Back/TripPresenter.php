<?php

declare(strict_types=1);

namespace Cleevio\Trips\BackModule\Presenters;

use Cleevio\ApiClient\Exception\ApiException;
use Cleevio\Fields\API\Requests\HostParamsGetRequest;
use Cleevio\Trips\API\Requests\TripDeleteRequest;
use Cleevio\Trips\API\Requests\TripGetRequest;
use Cleevio\Trips\API\Requests\TripOnHoldDeleteRequest;
use Cleevio\Trips\API\Requests\TripProcessPutRequest;
use Cleevio\Trips\API\Requests\TripsGetRequest;
use Cleevio\Trips\Components\FilterForm\FilterForm;
use Cleevio\Trips\Components\FilterForm\IFilterFormFactory;
use Cleevio\Trips\Components\StateMessageFormModal\IStateMessageFormModal;
use Cleevio\Trips\Components\StateMessageFormModal\StateMessageFormModal;
use Cleevio\UI\ApiPaginationControl\ApiPaginationControl;
use Cleevio\UI\ApiPaginationControl\IApiPaginationControlFactory;
use Nette\Application\AbortException;
use Nette\Application\BadRequestException;

class TripPresenter extends BasePresenter
{

	/**
	 * @var TripsGetRequest
	 */
	private $tripsGetRequest;

	/**
	 * @var TripOnHoldDeleteRequest
	 */
	private $tripOnHoldDeleteRequest;

	/**
	 * @var TripGetRequest
	 */
	private $tripGetRequest;

	/**
	 * @var HostParamsGetRequest
	 */
	private $hostParamsGetRequest;

	/**
	 * @var TripDeleteRequest
	 */
	private $tripDeleteRequest;

	/**
	 * @var TripProcessPutRequest
	 */
	private $tripProcessPutRequest;


	/**
	 * @param TripsGetRequest $tripsGetRequest
	 * @param TripGetRequest $tripGetRequest
	 * @param HostParamsGetRequest $hostParamsGetRequest
	 * @param TripDeleteRequest $tripDeleteRequest
	 * @param TripOnHoldDeleteRequest $tripOnHoldDeleteRequest
	 * @param TripProcessPutRequest $tripProcessPutRequest
	 */
	public function __construct(
		TripsGetRequest $tripsGetRequest,
		TripGetRequest $tripGetRequest,
		HostParamsGetRequest $hostParamsGetRequest,
		TripDeleteRequest $tripDeleteRequest,
		TripOnHoldDeleteRequest $tripOnHoldDeleteRequest,
		TripProcessPutRequest $tripProcessPutRequest
	)
	{
		parent::__construct();

		$this->tripsGetRequest = $tripsGetRequest;
		$this->tripOnHoldDeleteRequest = $tripOnHoldDeleteRequest;
		$this->tripGetRequest = $tripGetRequest;
		$this->hostParamsGetRequest = $hostParamsGetRequest;
		$this->tripDeleteRequest = $tripDeleteRequest;
		$this->tripProcessPutRequest = $tripProcessPutRequest;
	}


	public function startup()
	{
		parent::startup();
	}


	/**
	 * @param string|null $country
	 * @param string|null $start
	 * @param string|null $end
	 * @param string|null $state
	 * @param string|null $search
	 */
	public function actionDefault(
		?string $country,
		?string $start,
		?string $end,
		?string $state,
		?string $search
	): void
	{
		try {
			$params = [
				'country' => $country,
				'state' => $state !== null ? [$state] : ['submitted', 'processed', 'on-hold'],
				'minStartDate' => $start,
				'maxEndDate' => $end,
				'search' => $search,
			];

			$this->tripsGetRequest->setParams($params);
			$this['apiPagination']->setRequest($this->tripsGetRequest);

			$this->template->trips = $this['apiPagination']->getItems()->getItems();

		$this['filterForm']->setDefaults([
			'country' => $country,
			'state' => $state,
			'start' => $start,
			'end' => $end,
			'search' => $search,
		]);

			$this['filterForm']->onSuccess[] = function ($params) {
				$this->redirect(':Trips:Back:Trip:default', $params);
			};
		} catch (ApiException $ex) {
			$this->error($this->translator->translate('back.' . $ex->getMessage()), $ex->getCode());
		}
	}


	/**
	 * @param int $id
	 * @throws BadRequestException
	 */
	public function actionDetail(int $id): void
	{
		try {
			$this->tripGetRequest->setId($id);
			$trip = $this->tripGetRequest->execute();

			if ($trip->getHost() !== null) {
				$this->hostParamsGetRequest->setHostId($trip->getHost()->getId());
				$hostPrams = $this->hostParamsGetRequest->execute();
			}

			$this->template->trip = $trip;
			$this->template->hostParams = $hostPrams ?? null;
		} catch (ApiException $ex) {
			$this->error($this->translator->translate('back.' . $ex->getMessage()), $ex->getCode());
		}
	}


	/**
	 * @param int $id
	 * @throws AbortException
	 * @throws BadRequestException
	 */
	public function handleDelete(int $id): void
	{
		try {
			$this->tripDeleteRequest->setId($id);
			$this->tripDeleteRequest->execute();

			$this->template->trips = $this['apiPagination']->getItems()->getItems();

			if ($this->isAjax()) {
				$this->redrawControl('flashes');
				$this->redrawControl('trips');
			} else {
				$this->redirect(':Trips:Back:Trip:');
			}
		} catch (ApiException $ex) {
			$this->error($this->translator->translate('back.' . $ex->getMessage()), $ex->getCode());
		}
	}


	/**
	 * @param int $id
	 * @throws AbortException
	 * @throws BadRequestException
	 */
	public function handleRelease(int $id): void
	{
		try {
			$this->tripOnHoldDeleteRequest->setTripId($id);
			$this->tripOnHoldDeleteRequest->execute();

			$this->template->trips = $this['apiPagination']->getItems()->getItems();

			$this->flashMessage('back.trips.message-modal.release.success.message', 'success');

			if ($this->isAjax()) {
				$this->redrawControl('flashes');
				$this->redrawControl('trips');
			} else {
				$this->redirectPermanent('this');
			}
		} catch (ApiException $ex) {
			$this->error($this->translator->translate('back.' . $ex->getMessage()), $ex->getCode());
		}
	}


	/**
	 * @param int $id
	 * @throws AbortException
	 * @throws BadRequestException
	 */
	public function handleProcess(int $id): void
	{
		try {
			$this->tripProcessPutRequest->setTripId($id);
			$this->tripProcessPutRequest->execute();

			$this->flashMessage('back.trips.process.success.message', 'success');

			$this->template->trips = $this['apiPagination']->getItems()->getItems();

			if ($this->isAjax()) {
				$this->redrawControl('flashes');
				$this->redrawControl('trips');
			} else {
				$this->redirectPermanent('this');
			}
		} catch (ApiException $ex) {
			$this->error($this->translator->translate('back.' . $ex->getMessage()), $ex->getCode());
		}
	}


	/**
	 * @param IApiPaginationControlFactory $factory
	 * @return ApiPaginationControl
	 */
	protected function createComponentApiPagination(IApiPaginationControlFactory $factory): ApiPaginationControl
	{
		return $factory->create();
	}


	/**
	 * @param IFilterFormFactory $filterFormFactory
	 * @return FilterForm
	 */
	protected function createComponentFilterForm(IFilterFormFactory $filterFormFactory): FilterForm
	{
		return $filterFormFactory->create();
	}


	/**
	 * @param IStateMessageFormModal $factory
	 * @return StateMessageFormModal
	 */
	protected function createComponentSetMessageFormModal(IStateMessageFormModal $factory): StateMessageFormModal
	{
		$control = $factory->create();

		$control->onSuccess[] = function () {
			$this->flashMessage('back.trips.message-modal.on-hold.success.message', 'success');
			$this->redirectPermanent(':Trips:Back:Trip:');
		};

		return $control;
	}
}
