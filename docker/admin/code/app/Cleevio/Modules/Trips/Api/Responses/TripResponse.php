<?php

declare(strict_types=1);

namespace Cleevio\Trips\API\Responses;

use Cleevio\ApiClient\Exception\BadRequestException;
use Cleevio\ApiClient\IResponse;
use Cleevio\Countries\API\Responses\CountryResponse;
use Cleevio\Hosts\API\Responses\HostResponse;
use Cleevio\Users\Api\Responses\UserResponse;
use DateTime;
use InvalidArgumentException;

class TripResponse implements IResponse
{

	private const DATE_FORMAT = 'Y-m-d';

	private const DATETIME_FORMAT = 'Y-m-d H:i:s';

	/**
	 * @var int
	 */
	private $id;

	/**
	 * @var DateTime
	 */
	private $start;

	/**
	 * @var DateTime
	 */
	private $end;

	/**
	 * @var CountryResponse
	 */
	private $country;

	/**
	 * @var string
	 */
	private $state;

	/**
	 * @var HostResponse|null
	 */
	private $host;

	/**
	 * @var DateTime|null
	 */
	private $hostSet;

	/**
	 * @var TripRegistrationResponse
	 */
	private $registration;

	/**
	 * @var TripAnswersResponse|null
	 */
	private $answers;

	/**
	 * @var bool
	 */
	private $isFavorite;

	/**
	 * @var DateTime
	 */
	private $createdAt;

	/**
	 * @var DateTime
	 */
	private $updatedAt;

	/**
	 * @var DateTime|null
	 */
	private $submittedAt;

	/**
	 * @var UserResponse
	 */
	private $user;

	/**
	 * @var string|null
	 */
	private $stateMessage;


	/**
	 * TripResponse constructor.
	 * @param array $trip
	 * @throws BadRequestException
	 */
	public function __construct(array $trip)
	{
		if (isset($trip['start'], $trip['end'], $trip['country'], $trip['id'], $trip['state'])) {
			$start = DateTime::createFromFormat(self::DATE_FORMAT, $trip['start']);
			$end = DateTime::createFromFormat(self::DATE_FORMAT, $trip['end']);
			$hostSet = $trip['hostSet'] === null ? null : DateTime::createFromFormat(self::DATETIME_FORMAT, $trip['hostSet']);
			$createdAt = DateTime::createFromFormat(self::DATETIME_FORMAT, $trip['createdAt']);
			$updatedAt = DateTime::createFromFormat(self::DATETIME_FORMAT, $trip['updatedAt']);
			$submittedAt = $trip['submittedAt'] !== null
				? DateTime::createFromFormat(self::DATETIME_FORMAT, $trip['submittedAt'])
				: null;

			if ($start === false || $end === false || $hostSet === false || $createdAt === false || $updatedAt === false || $submittedAt === false) {
				throw new BadRequestException('frontend.trips.trip.get.invalid-date-format');
			}

			$this->id = $trip['id'];
			$this->start = $start;
			$this->end = $end;
			$this->country = new CountryResponse($trip['country']);
			$this->state = $trip['state'];
			$this->stateMessage = $trip['stateMessage'];
			$this->host = isset($trip['host'])
				? new HostResponse($trip['host'])
				: null;
			$this->hostSet = $hostSet;
			$this->user = new UserResponse($trip['user']);
			$this->registration = new TripRegistrationResponse($trip['registration']);
			$this->answers = new TripAnswersResponse($trip['answers']);
			$this->isFavorite = $trip['isFavorite'];
			$this->createdAt = $createdAt;
			$this->updatedAt = $updatedAt;
			$this->submittedAt = $submittedAt;
		} else {
			throw new InvalidArgumentException;
		}
	}


	/**
	 * @return int
	 */
	public function getId(): int
	{
		return $this->id;
	}


	/**
	 * @return DateTime
	 */
	public function getStart(): DateTime
	{
		return $this->start;
	}


	/**
	 * @return DateTime
	 */
	public function getEnd(): DateTime
	{
		return $this->end;
	}


	/**
	 * @return CountryResponse
	 */
	public function getCountry(): CountryResponse
	{
		return $this->country;
	}


	/**
	 * @return string
	 */
	public function getState(): string
	{
		return $this->state;
	}


	/**
	 * @return HostResponse|null
	 */
	public function getHost(): ?HostResponse
	{
		return $this->host;
	}


	/**
	 * @return DateTime|null
	 */
	public function getHostSet(): ?DateTime
	{
		return $this->hostSet;
	}


	/**
	 * @return TripRegistrationResponse
	 */
	public function getRegistration(): TripRegistrationResponse
	{
		return $this->registration;
	}


	/**
	 * @return TripAnswersResponse|null
	 */
	public function getAnswers(): ?TripAnswersResponse
	{
		return $this->answers;
	}


	/**
	 * @return DateTime
	 */
	public function getCreatedAt(): DateTime
	{
		return $this->createdAt;
	}


	/**
	 * @return DateTime
	 */
	public function getUpdatedAt(): DateTime
	{
		return $this->updatedAt;
	}


	/**
	 * @return DateTime|null
	 */
	public function getSubmittedAt(): ?DateTime
	{
		return $this->submittedAt;
	}


	/**
	 * @return bool
	 */
	public function isFavorite(): bool
	{
		return $this->isFavorite;
	}


	/**
	 * @return UserResponse
	 */
	public function getUser(): UserResponse
	{
		return $this->user;
	}


	/**
	 * @return string|null
	 */
	public function getStateMessage(): ?string
	{
		return $this->stateMessage;
	}

}
