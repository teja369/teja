<?php

declare(strict_types=1);

namespace Cleevio\Trips\API\Requests;

interface ITripProcessPutRequest
{

	/**
	 * @param int $id
	 */
	public function setTripId(int $id): void;
}
