<?php

declare(strict_types=1);

namespace Cleevio\Trips\API\Requests;

interface ITripDeleteRequest
{

	function setId(int $id): void;

}
