<?php

declare(strict_types=1);

namespace Cleevio\Trips\API\Requests;

use Cleevio\ApiClient\Exception\ApiException;
use Cleevio\ApiClient\Requests\PaginationRequest;
use Cleevio\Trips\API\Responses\TripsResponse;

class TripsGetRequest extends PaginationRequest implements ITripsGetRequest
{

	/**
	 * @var array
	 */
	private $params = [];

	/**
	 * Endpoint path
	 * @return string
	 */
	protected function path(): string
	{
		$path = '/trips';

		$query = http_build_query($this->params + [
				'page' => $this->getPage(),
				'limit' => $this->getLimit(),
				'showAll' => true,
			]);

		return strlen($query) > 0
			? $path . '?' . $query
			: $path;
	}

	public function setParams(array $params): void
	{
		if (array_key_exists('state', $params) && $params['state'] !== null) {
			$params['state'] = implode('|', $params['state']);
		}

		$this->params = $params;
	}

	/**
	 * Method
	 * @return string
	 */
	protected function method(): string
	{
		return 'GET';
	}


	/**
	 * Determines whether request requires authorization
	 * @return bool
	 */
	function requiresAuth(): bool
	{
		return true;
	}


	/**
	 * Execute request and return a response
	 * @return TripsResponse
	 * @throws ApiException
	 */
	function execute(): TripsResponse
	{
		return new TripsResponse($this->request());
	}

}
