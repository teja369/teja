<?php

declare(strict_types=1);

namespace Cleevio\Trips\API\Requests;

use Cleevio\ApiClient\Exception\ApiException;
use Cleevio\ApiClient\Exception\BadRequestException;
use Cleevio\ApiClient\Requests\PaginationRequest;
use Cleevio\Trips\API\Responses\TripResponse;

class TripGetRequest extends PaginationRequest implements ITripGetRequest
{
	/**
	 * @var int
	 */
	private $id;

	/**
	 * Endpoint path
	 * @return string
	 */
	protected function path(): string
	{
		return sprintf('/trips/%s', $this->id);
	}

	/**
	 * Method
	 * @return string
	 */
	protected function method(): string
	{
		return 'GET';
	}


	/**
	 * Determines whether request requires authorization
	 * @return bool
	 */
	function requiresAuth(): bool
	{
		return true;
	}

	/**
	 * Execute request and return a response
	 * @return TripResponse
	 * @throws ApiException
	 * @throws BadRequestException
	 */
	function execute(): TripResponse
	{
		return new TripResponse($this->request());
	}

	/**
	 * @param int $id
	 */
	function setId(int $id): void
	{
		$this->id = $id;
	}
}
