<?php

declare(strict_types=1);

namespace Cleevio\Trips\API\Requests;

interface ITripGetRequest
{

	function setId(int $id): void;

}
