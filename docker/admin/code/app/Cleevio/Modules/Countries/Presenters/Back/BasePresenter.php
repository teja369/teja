<?php

declare(strict_types=1);

namespace Cleevio\Countries\BackModule\Presenters;

use Cleevio\BackModule\Presenters\BasePresenter as Presenter;

class BasePresenter extends Presenter
{

	public function startup()
	{
		parent::startup();

		if (!$this->getUser()->isAllowed('admin.countries')) {
			$this->redirect(':Back:Homepage:default');
		}
	}

}
