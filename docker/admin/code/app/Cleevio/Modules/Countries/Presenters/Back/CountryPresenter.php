<?php

declare(strict_types=1);

namespace Cleevio\Countries\BackModule\Presenters;

use Cleevio\ApiClient\Exception\ApiException;
use Cleevio\Countries\API\Requests\CountriesGetRequest;
use Cleevio\Countries\API\Requests\CountryDeleteRequest;
use Cleevio\Countries\API\Requests\CountryPutRequest;
use Cleevio\Questions\API\Requests\CountryTranslationGetRequest;
use Cleevio\Questions\API\Requests\CountryTranslationPutRequest;
use Cleevio\Translations\Components\SetTranslationFormModal\ISetTranslationFormModal;
use Cleevio\Translations\Components\SetTranslationFormModal\SetTranslationFormModal;
use Cleevio\UI\ApiPaginationControl\ApiPaginationControl;
use Cleevio\UI\ApiPaginationControl\IApiPaginationControlFactory;
use Nette\Application\AbortException;
use Nette\Application\BadRequestException;

class CountryPresenter extends BasePresenter
{

	/**
	 * @var CountriesGetRequest
	 */
	private $countriesGetRequest;

	/**
	 * @var CountryPutRequest
	 */
	private $countryPutRequest;

	/**
	 * @var CountryDeleteRequest
	 */
	private $countryDeleteRequest;
	/**
	 * @var CountryTranslationGetRequest
	 */
	private $countryTranslationGetRequest;
	/**
	 * @var CountryTranslationPutRequest
	 */
	private $countryTranslationPutRequest;


	/**
	 * CountryPresenter constructor.
	 * @param CountriesGetRequest $countriesGetRequest
	 * @param CountryPutRequest $countryPutRequest
	 * @param CountryDeleteRequest $countryDeleteRequest
	 * @param CountryTranslationGetRequest $countryTranslationGetRequest
	 * @param CountryTranslationPutRequest $countryTranslationPutRequest
	 */
	public function __construct(
		CountriesGetRequest $countriesGetRequest,
		CountryPutRequest $countryPutRequest,
		CountryDeleteRequest $countryDeleteRequest,
		CountryTranslationGetRequest $countryTranslationGetRequest,
		CountryTranslationPutRequest $countryTranslationPutRequest
	)
	{
		parent::__construct();
		$this->countriesGetRequest = $countriesGetRequest;
		$this->countryPutRequest = $countryPutRequest;
		$this->countryDeleteRequest = $countryDeleteRequest;
		$this->countryTranslationGetRequest = $countryTranslationGetRequest;
		$this->countryTranslationPutRequest = $countryTranslationPutRequest;
	}


	public function startup()
	{
		parent::startup();
	}


	public function renderDefault(): void
	{
		$this['apiPagination']->setLimit(200);
		$this->template->countries = $this['apiPagination']->getItems()->getItems();
	}


	/**
	 * @param string $code
	 * @throws AbortException
	 * @throws BadRequestException
	 */
	public function handleDisable(string $code): void
	{
		try {
			$this->countryDeleteRequest->setCountryCode($code);
			$this->countryDeleteRequest->execute();

			$this->template->countries = $this['apiPagination']->getItems()->getItems();

			if ($this->isAjax()) {
				$this->redrawControl('flashes');
				$this->redrawControl('countries');
			} else {
				$this->redirect(':Countries:Back:Country:');
			}
		} catch (ApiException $ex) {
			$this->error($this->translator->translate('back.' . $ex->getMessage()), $ex->getCode());
		}
	}


	/**
	 * @param string $code
	 * @throws AbortException
	 * @throws BadRequestException
	 */
	public function handleEnable(string $code): void
	{
		try {
			$this->countryPutRequest->setCountryCode($code);
			$this->countryPutRequest->execute();

			$this->template->countries = $this['apiPagination']->getItems()->getItems();

			if ($this->isAjax()) {
				$this->redrawControl('flashes');
				$this->redrawControl('countries');
			} else {
				$this->redirect(':Countries:Back:Country:');
			}
		} catch (ApiException $ex) {
			$this->error($this->translator->translate('back.' . $ex->getMessage()), $ex->getCode());
		}
	}

	protected function createComponentTranslationOptionModalForm(ISetTranslationFormModal $factory): SetTranslationFormModal
	{
		$control = $factory->create();
		$control->setGetRequest($this->countryTranslationGetRequest);
		$control->setPutRequest($this->countryTranslationPutRequest);

		return $control;
	}

	protected function createComponentApiPagination(IApiPaginationControlFactory $factory): ApiPaginationControl
	{
		$control = $factory->create();
		$control->setRequest($this->countriesGetRequest);

		return $control;
	}

}
