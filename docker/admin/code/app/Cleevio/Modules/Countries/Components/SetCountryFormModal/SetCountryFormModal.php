<?php

declare(strict_types=1);

namespace Cleevio\Countries\Components\SetCountryFormModal;

use Cleevio\Countries\API\Requests\CountriesGetRequest;
use Cleevio\UI\SetRelationFormModal\SetRelationFormModal;
use Nette\Localization\ITranslator;

class SetCountryFormModal extends SetRelationFormModal
{

	/**
	 * @param ITranslator $translator
	 * @param CountriesGetRequest $hostsGetRequest
	 */
	public function __construct(ITranslator $translator, CountriesGetRequest $hostsGetRequest)
	{
		parent::__construct($translator, $hostsGetRequest);
	}

	protected function translationHeadline(): string
	{
		return "back.hosts.fields.countries.modal.headline"; // Todo: Generalise key
	}
}

interface ISetCountryFormModalFactory
{

	/**
	 * @return SetCountryFormModal
	 */
	public function create(): SetCountryFormModal;
}
