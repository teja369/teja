<?php

declare(strict_types=1);

namespace Cleevio\Countries\API\Requests;

use Cleevio\Api\BaseRequest;
use Cleevio\ApiClient\Exception\ApiException;
use Cleevio\Countries\API\Responses\CountryDeleteResponse;
use InvalidArgumentException;

class CountryDeleteRequest extends BaseRequest implements ICountryDeleteRequest
{

	/**
	 * @var string
	 */
	private $countryCode;


	/**
	 * Endpoint path
	 * @return string
	 */
	protected function path(): string
	{
		if ($this->countryCode === null) {
			throw new InvalidArgumentException;
		}

		return sprintf('/countries/%s/enable', $this->countryCode);
	}


	/**
	 * Method
	 * @return string
	 */
	protected function method(): string
	{
		return 'DELETE';
	}


	/**
	 * Determines whether request requires authorization
	 * @return bool
	 */
	function requiresAuth(): bool
	{
		return true;
	}


	/**
	 * @param string $code
	 */
	public function setCountryCode(string $code): void
	{
		$this->countryCode = $code;
	}


	/**
	 * @return CountryDeleteResponse
	 * @throws ApiException
	 */
	function execute(): CountryDeleteResponse
	{
		$this->request();

		return new CountryDeleteResponse;
	}
}
