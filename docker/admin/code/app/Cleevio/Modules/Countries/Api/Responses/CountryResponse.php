<?php

declare(strict_types=1);

namespace Cleevio\Countries\API\Responses;

use Cleevio\UI\ApiFilterForm\IFilterResponse;
use Cleevio\UI\SetFormModal\IRelationResponse;
use InvalidArgumentException;

class CountryResponse implements IRelationResponse, IFilterResponse
{

	/**
	 * @var int
	 */
	private $id;

	/**
	 * @var string
	 */
	private $name;

	/**
	 * @var string
	 */
	private $code;

	/**
	 * @var bool
	 */
	private $enabled;


	/**
	 * @param array $country
	 */
	public function __construct(array $country)
	{
		if (array_key_exists('id', $country) &&
			array_key_exists('name', $country) &&
			array_key_exists('code', $country)) {
			$this->id = $country['id'];
			$this->name = $country['name'];
			$this->code = $country['code'];
			$this->enabled = $country['isEnabled'];
		} else {
			throw new InvalidArgumentException;
		}
	}


	/**
	 * @return int
	 */
	public function getId(): int
	{
		return $this->id;
	}


	/**
	 * @return string
	 */
	public function getName(): string
	{
		return $this->name;
	}

	/**
	 * @return string
	 */
	public function getCode(): string
	{
		return $this->code;
	}


	/**
	 * @return bool
	 */
	public function isEnabled(): bool
	{
		return $this->enabled;
	}

	/**
	 * @return string
	 */
	function getValue(): string
	{
		return $this->getCode();
	}
}
