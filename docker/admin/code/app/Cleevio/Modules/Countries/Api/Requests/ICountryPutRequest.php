<?php

declare(strict_types=1);

namespace Cleevio\Countries\API\Requests;

interface ICountryPutRequest
{

	/**
	 * @param string $code
	 */
	public function setCountryCode(string $code): void;
}
