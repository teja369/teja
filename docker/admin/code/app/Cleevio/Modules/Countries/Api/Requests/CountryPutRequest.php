<?php

declare(strict_types=1);

namespace Cleevio\Countries\API\Requests;

use Cleevio\Api\BaseRequest;
use Cleevio\ApiClient\Exception\ApiException;
use Cleevio\Countries\API\Responses\CountryPutResponse;
use InvalidArgumentException;

class CountryPutRequest extends BaseRequest implements ICountryPutRequest
{

	/**
	 * @var string
	 */
	private $countryCode;


	/**
	 * Endpoint path
	 * @return string
	 */
	protected function path(): string
	{
		if ($this->countryCode === null) {
			throw new InvalidArgumentException;
		}

		return sprintf('/countries/%s/enable', $this->countryCode);
	}


	/**
	 * Method
	 * @return string
	 */
	protected function method(): string
	{
		return 'PUT';
	}


	/**
	 * Determines whether request requires authorization
	 * @return bool
	 */
	function requiresAuth(): bool
	{
		return true;
	}


	/**
	 * @param string $code
	 */
	public function setCountryCode(string $code): void
	{
		$this->countryCode = $code;
	}


	/**
	 * @return CountryPutResponse
	 * @throws ApiException
	 */
	function execute(): CountryPutResponse
	{
		$this->request();

		return new CountryPutResponse;
	}
}
