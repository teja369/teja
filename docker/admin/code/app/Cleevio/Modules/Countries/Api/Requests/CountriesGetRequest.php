<?php

declare(strict_types=1);

namespace Cleevio\Countries\API\Requests;

use Cleevio\ApiClient\Exception\ApiException;
use Cleevio\Countries\API\Responses\CountriesPagedResponse;
use Cleevio\UI\ApiFilterForm\IFilterRequest;
use Cleevio\UI\SetFormModal\DataGetRequest;

class CountriesGetRequest extends DataGetRequest implements ICountriesGetRequest, IFilterRequest
{

	/**
	 * Endpoint path
	 * @return string
	 */
	protected function path(): string
	{
		$path = '/countries';

		$params = [
			'showDisabled' => 1,
			'limit' => $this->getLimit(),
			'page' => $this->getPage(),
		];

		$query = http_build_query($params);

		return strlen($query) > 0
			? $path . '?' . $query
			: $path;
	}


	/**
	 * Method
	 * @return string
	 */
	protected function method(): string
	{
		return 'GET';
	}


	/**
	 * Determines whether request requires authorization
	 * @return bool
	 */
	function requiresAuth(): bool
	{
		return true;
	}


	/**
	 * Execute request and return a response
	 * @return CountriesPagedResponse
	 * @throws ApiException
	 */
	function execute(): CountriesPagedResponse
	{
		return new CountriesPagedResponse($this->request());
	}
}
