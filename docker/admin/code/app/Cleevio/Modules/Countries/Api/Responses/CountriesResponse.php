<?php

declare(strict_types=1);

namespace Cleevio\Countries\API\Responses;

use Cleevio\ApiClient\IResponse;

class CountriesResponse implements IResponse
{

	/**
	 * @var CountryResponse[]
	 */
	private $countries = [];


	/**
	 * CountriesResponse constructor.
	 * @param array $request
	 */
	public function __construct(array $request)
	{
		$this->countries = array_map(static function ($host) {
			return new CountryResponse($host);
		}, $request);
	}

	/**
	 * @return CountryResponse[]
	 */
	public function getItems(): array
	{
		return $this->countries;
	}
}
