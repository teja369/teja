<?php

declare(strict_types=1);

namespace Cleevio\Questions\API\Requests;

use Cleevio\Api\BaseRequest;
use Cleevio\ApiClient\Exception\ApiException;
use Cleevio\Questions\API\Responses\CountryTranslationResponse;
use Cleevio\Translations\Components\SetTranslationFormModal\IPutTranslationRequest;
use Cleevio\Translations\Components\SetTranslationFormModal\TranslatableResponse;
use InvalidArgumentException;

class CountryTranslationPutRequest extends BaseRequest implements ICountryTranslationPutRequest, IPutTranslationRequest
{

	/**
	 * @var int
	 */
	private $countryId;

	/**
	 * @var string
	 */
	private $languageCode;


	/**
	 * Endpoint path
	 * @return string
	 */
	protected function path(): string
	{
		if ($this->countryId === null || $this->languageCode === null) {
			throw new InvalidArgumentException;
		}

		return sprintf('/countries/%s/translation/%s', $this->countryId, $this->languageCode);
	}


	/**
	 * Method
	 * @return string
	 */
	protected function method(): string
	{
		return 'PUT';
	}


	/**
	 * Determines whether request requires authorization
	 * @return bool
	 */
	function requiresAuth(): bool
	{
		return true;
	}


	/**
	 * Execute request and return a response
	 * @return CountryTranslationResponse
	 * @throws ApiException
	 */
	function execute(): TranslatableResponse
	{
		return new CountryTranslationResponse($this->request());
	}


	public function setLanguageCode(string $languageCode): void
	{
		$this->languageCode = $languageCode;
	}


	public function setId(int $id): void
	{
		$this->countryId = $id;
	}
}
