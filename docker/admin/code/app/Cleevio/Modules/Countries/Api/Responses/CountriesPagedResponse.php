<?php

declare(strict_types=1);

namespace Cleevio\Countries\API\Responses;

use Cleevio\ApiClient\DTO\PaginationResponse;

class CountriesPagedResponse extends PaginationResponse
{

	/**
	 * @var CountryResponse[]
	 */
	private $countries = [];


	/**
	 * CountriesResponse constructor.
	 * @param array $request
	 */
	public function __construct(array $request)
	{
		parent::__construct($request);

		$this->countries = array_map(static function ($host) {
			return new CountryResponse($host);
		}, $request['items']);
	}

	/**
	 * @return CountryResponse[]
	 */
	public function getItems(): array
	{
		return $this->countries;
	}
}
