<?php

declare(strict_types=1);

namespace Cleevio\Countries\Router;

use Nette;
use Nette\Application\Routers\Route;
use Nette\Application\Routers\RouteList;

class RouterFactory
{
	/**
	 * @return Nette\Application\IRouter
	 */
	public function createRouter()
	{
		$router = new RouteList;

		$router[] = $usersModule = new RouteList('Countries');

		$usersModule[] = new Route('countries/<presenter>/<action>[/<id>]', [
			'presenter' => 'Homepage',
			'action' => 'default',
			'module' => 'Back',
		]);

		return $router;
	}

}
