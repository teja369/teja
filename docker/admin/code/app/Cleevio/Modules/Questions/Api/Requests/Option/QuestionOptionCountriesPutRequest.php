<?php

declare(strict_types=1);

namespace Cleevio\Questions\API\Requests;

use Cleevio\Api\BaseRequest;
use Cleevio\ApiClient\Exception\ApiException;
use Cleevio\Countries\API\Responses\CountriesResponse;
use Cleevio\UI\SetFormModal\IRelationSetRequest;
use InvalidArgumentException;

class QuestionOptionCountriesPutRequest extends BaseRequest implements IQuestionOptionCountriesPutRequest, IRelationSetRequest
{
	/**
	 * @var int
	 */
	private $optionId;

	/**
	 * Endpoint path
	 * @return string
	 */
	protected function path(): string
	{
		if ($this->optionId === null) {
			throw new InvalidArgumentException;
		}

		return sprintf('/questions/option/%s/country', $this->optionId);
	}


	/**
	 * Method
	 * @return string
	 */
	protected function method(): string
	{
		return 'PUT';
	}

	/**
	 * Determines whether request requires authorization
	 * @return bool
	 */
	function requiresAuth(): bool
	{
		return true;
	}

	/**
	 * Execute request and return a response
	 * @return CountriesResponse
	 * @throws ApiException
	 */
	function execute(): CountriesResponse
	{
		return new CountriesResponse($this->request());
	}

	public function setId(int $id): void
	{
		$this->optionId = $id;
	}
}
