<?php

declare(strict_types=1);

namespace Cleevio\Questions\API\Requests;

use Cleevio\Api\BaseRequest;
use Cleevio\ApiClient\Exception\ApiException;
use Cleevio\Quesetions\API\Requests\IQuestionsGroupPutRequest;
use Cleevio\Questions\API\Responses\QuestionsGroupResponse;

class QuestionsGroupPutRequest extends BaseRequest implements IQuestionsGroupPutRequest
{

	/**
	 * @var int
	 */
	private $groupId;


	/**
	 * Endpoint path
	 * @return string
	 */
	protected function path(): string
	{
		return sprintf('/questions/groups/%s', $this->groupId);
	}


	/**
	 * Method
	 * @return string
	 */
	protected function method(): string
	{
		return 'PUT';
	}


	/**
	 * Determines whether request requires authorization
	 * @return bool
	 */
	function requiresAuth(): bool
	{
		return true;
	}


	public function setGroupId(int $id): void
	{
		$this->groupId = $id;
	}


	/**
	 * Execute request and return a response
	 * @return QuestionsGroupResponse
	 * @throws ApiException
	 */
	function execute(): QuestionsGroupResponse
	{
		return new QuestionsGroupResponse($this->request());
	}
}
