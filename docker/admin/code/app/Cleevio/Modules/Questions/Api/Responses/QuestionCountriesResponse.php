<?php

declare(strict_types=1);

namespace Cleevio\Questions\API\Responses;

use Cleevio\Countries\API\Responses\CountryResponse;
use Cleevio\Hosts\API\Responses\HostResponse;
use Cleevio\UI\SetFormModal\IRelationsResponse;

class QuestionCountriesResponse implements IRelationsResponse
{

	/**
	 * @var HostResponse[]
	 */
	private $items = [];


	/**
	 * HostsResponse constructor.
	 * @param array $request
	 */
	public function __construct(array $request)
	{
		$this->items = array_map(static function ($host) {
			return new CountryResponse($host);
		}, $request);
	}


	/**
	 * @return HostResponse[]
	 */
	public function getItems(): array
	{
		return $this->items;
	}

}
