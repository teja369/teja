<?php

declare(strict_types=1);

namespace Cleevio\Questions\API\Requests;

use Cleevio\ApiClient\Exception\ApiException;
use Cleevio\ApiClient\Requests\PaginationRequest;
use Cleevio\Questions\API\Responses\QuestionsResponse;

class QuestionsGetRequest extends PaginationRequest implements IQuestionsGetRequest
{

	/**
	 * @var int|null
	 */
	private $groupId = null;

	/**
	 * @var array
	 */
	private $params = [];


	/**
	 * Endpoint path
	 * @return string
	 */
	protected function path(): string
	{
		$path = '/questions';

		$query = http_build_query($this->params + [
				'limit' => $this->getLimit(),
				'page' => $this->getPage(),
				'group' => $this->groupId,
			]);

		return strlen($query) > 0
			? $path . '?' . $query
			: $path;
	}


	/**
	 * Method
	 * @return string
	 */
	protected function method(): string
	{
		return 'GET';
	}


	/**
	 * Determines whether request requires authorization
	 * @return bool
	 */
	function requiresAuth(): bool
	{
		return true;
	}


	/**
	 * Execute request and return a response
	 * @return QuestionsResponse
	 * @throws ApiException
	 */
	function execute(): QuestionsResponse
	{
		return new QuestionsResponse($this->request());
	}


	/**
	 * @param int|null $groupId
	 */
	public function setGroupId(?int $groupId): void
	{
		$this->groupId = $groupId;
	}

	public function setParams(array $params): void
	{
		$this->params = $params;
	}

}
