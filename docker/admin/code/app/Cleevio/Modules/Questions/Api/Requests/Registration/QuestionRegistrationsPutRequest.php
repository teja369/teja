<?php

declare(strict_types=1);

namespace Cleevio\Questions\API\Requests;

use Cleevio\Api\BaseRequest;
use Cleevio\ApiClient\DTO\EmptyResponse;
use Cleevio\ApiClient\Exception\ApiException;
use Cleevio\UI\SetFormModal\IRelationSetRequest;
use InvalidArgumentException;

class QuestionRegistrationsPutRequest extends BaseRequest implements IQuestionRegistrationsPutRequest, IRelationSetRequest
{

	/**
	 * @var int
	 */
	private $questionId;


	/**
	 * Endpoint path
	 * @return string
	 */
	protected function path(): string
	{
		if ($this->questionId === null) {
			throw new InvalidArgumentException;
		}

		return sprintf('/questions/%s/registration', $this->questionId);
	}


	/**
	 * Method
	 * @return string
	 */
	protected function method(): string
	{
		return 'PUT';
	}


	/**
	 * Determines whether request requires authorization
	 * @return bool
	 */
	function requiresAuth(): bool
	{
		return true;
	}


	/**
	 * Execute request and return a response
	 * @return EmptyResponse
	 * @throws ApiException
	 */
	function execute(): EmptyResponse
	{
		$this->request();

		return new EmptyResponse;
	}


	public function setQuestionId(int $id): void
	{
		$this->questionId = $id;
	}


	public function setId(int $id): void
	{
		$this->setQuestionId($id);
	}
}
