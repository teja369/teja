<?php

declare(strict_types=1);

namespace Cleevio\Questions\API\Requests;

use Cleevio\Api\BaseRequest;
use Cleevio\ApiClient\Exception\ApiException;
use Cleevio\Questions\API\Responses\QuestionCountriesResponse;
use Cleevio\UI\SetFormModal\IRelationGetRequest;
use Cleevio\UI\SetFormModal\IRelationsResponse;
use InvalidArgumentException;

class QuestionCountriesGetRequest extends BaseRequest implements IQuestionCountriesGetRequest, IRelationGetRequest
{
	/**
	 * @var int
	 */
	private $questionId;

	/**
	 * Endpoint path
	 * @return string
	 */
	protected function path(): string
	{
		if ($this->questionId === null) {
			throw new InvalidArgumentException;
		}

		return sprintf('/questions/%s/country', $this->questionId);
	}


	/**
	 * Method
	 * @return string
	 */
	protected function method(): string
	{
		return 'GET';
	}

	/**
	 * Determines whether request requires authorization
	 * @return bool
	 */
	function requiresAuth(): bool
	{
		return true;
	}

	/**
	 * Execute request and return a response
	 * @return QuestionCountriesResponse
	 * @throws ApiException
	 */
	function execute(): IRelationsResponse
	{
		return new QuestionCountriesResponse($this->request());
	}


	public function setId(int $id): void
	{
		$this->questionId = $id;
	}
}
