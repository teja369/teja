<?php

declare(strict_types=1);

namespace Cleevio\Questions\API\Requests;

use Cleevio\Api\BaseRequest;
use Cleevio\ApiClient\Exception\ApiException;
use Cleevio\Questions\API\Responses\OptionResponse;
use InvalidArgumentException;

class QuestionOptionGetRequest extends BaseRequest implements IQuestionOptionCountriesGetRequest
{
	/**
	 * @var int
	 */
	private $optionId;

	/**
	 * Endpoint path
	 * @return string
	 */
	protected function path(): string
	{
		if ($this->optionId === null) {
			throw new InvalidArgumentException;
		}

		return sprintf('/questions/option/%s', $this->optionId);
	}


	/**
	 * Method
	 * @return string
	 */
	protected function method(): string
	{
		return 'GET';
	}

	/**
	 * Determines whether request requires authorization
	 * @return bool
	 */
	function requiresAuth(): bool
	{
		return true;
	}

	/**
	 * Execute request and return a response
	 * @return OptionResponse
	 * @throws ApiException
	 */
	function execute(): OptionResponse
	{
		return new OptionResponse($this->request());
	}


	public function setId(int $id): void
	{
		$this->optionId = $id;
	}
}
