<?php

declare(strict_types=1);

namespace Cleevio\Questions\API\Requests;

use Cleevio\Api\BaseRequest;
use Cleevio\ApiClient\Exception\ApiException;
use Cleevio\Quesetions\API\Requests\IQuestionsGroupPostRequest;
use Cleevio\Questions\API\Responses\QuestionsGroupResponse;

class QuestionsGroupPostRequest extends BaseRequest implements IQuestionsGroupPostRequest
{

	/**
	 * Endpoint path
	 * @return string
	 */
	protected function path(): string
	{
		return '/questions/groups';
	}


	/**
	 * Method
	 * @return string
	 */
	protected function method(): string
	{
		return 'POST';
	}


	/**
	 * Determines whether request requires authorization
	 * @return bool
	 */
	function requiresAuth(): bool
	{
		return true;
	}


	/**
	 * Execute request and return a response
	 * @return QuestionsGroupResponse
	 * @throws ApiException
	 */
	function execute(): QuestionsGroupResponse
	{
		return new QuestionsGroupResponse($this->request());
	}
}
