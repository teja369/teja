<?php

declare(strict_types=1);

namespace Cleevio\Questions\API\Responses;

use Cleevio\Translations\Components\SetTranslationFormModal\TranslatableResponse;
use Cleevio\Translations\Components\SetTranslationFormModal\TranslationMapping;
use Cleevio\Translations\Components\SetTranslationFormModal\TranslationMappingField;
use InvalidArgumentException;

class QuestionOptionTranslationResponse extends TranslatableResponse
{

	/**
	 * @var string|null
	 */
	private $text;


	/**
	 * @param array $translation
	 */
	public function __construct(array $translation)
	{
		if (array_key_exists('text', $translation)) {
			$this->text = $translation['text'];
		} else {
			throw new InvalidArgumentException;
		}
	}

	public function getText(): ?string
	{
		return $this->text;
	}


	public function getMapping(): TranslationMapping
	{
		$mapping = new TranslationMapping;
		$mapping->addField(new TranslationMappingField('text', 'back.questions.options.form.label.text', $this->getText()));

		return $mapping;
	}
}
