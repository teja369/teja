<?php

declare(strict_types=1);

namespace Cleevio\Questions\API\Responses;

use Cleevio\ApiClient\IResponse;
use InvalidArgumentException;

class NumberQuestionResponse implements IResponse
{

	/**
	 * @var int|null
	 */
	private $minValue;

	/**
	 * @var int|null
	 */
	private $maxValue;


	/**
	 * NumberQuestionResponse constructor.
	 * @param array $numberQuestion
	 */
	public function __construct(array $numberQuestion)
	{
		if (array_key_exists('minValue', $numberQuestion) && array_key_exists('maxValue', $numberQuestion)) {
			$this->minValue = $numberQuestion['minValue'];
			$this->maxValue = $numberQuestion['maxValue'];
		} else {
			throw new InvalidArgumentException;
		}
	}


	/**
	 * @return int|null
	 */
	public function getMinValue(): ?int
	{
		return $this->minValue;
	}


	/**
	 * @return int|null
	 */
	public function getMaxValue(): ?int
	{
		return $this->maxValue;
	}

}
