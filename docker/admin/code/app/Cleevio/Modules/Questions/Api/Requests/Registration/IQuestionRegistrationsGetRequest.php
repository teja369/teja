<?php

declare(strict_types=1);

namespace Cleevio\Questions\API\Requests;

interface IQuestionRegistrationsGetRequest
{

	public function setQuestionId(int $id): void;
}
