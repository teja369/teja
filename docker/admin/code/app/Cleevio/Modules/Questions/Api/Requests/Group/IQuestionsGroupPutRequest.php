<?php

declare(strict_types=1);

namespace Cleevio\Quesetions\API\Requests;

interface IQuestionsGroupPutRequest
{
	public function setGroupId(int $id): void;
}
