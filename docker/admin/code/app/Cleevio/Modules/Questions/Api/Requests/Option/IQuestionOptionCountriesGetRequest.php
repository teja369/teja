<?php

declare(strict_types=1);

namespace Cleevio\Questions\API\Requests;

interface IQuestionOptionCountriesGetRequest
{

	public function setId(int $id): void;
}
