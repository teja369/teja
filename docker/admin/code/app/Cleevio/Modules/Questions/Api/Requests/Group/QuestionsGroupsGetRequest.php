<?php

declare(strict_types=1);

namespace Cleevio\Questions\API\Requests;

use Cleevio\Api\BaseRequest;
use Cleevio\ApiClient\Exception\ApiException;
use Cleevio\Quesetions\API\Requests\IQuestionsGroupsGetRequest;
use Cleevio\Questions\API\Responses\QuestionsGroupsResponse;

class QuestionsGroupsGetRequest extends BaseRequest implements IQuestionsGroupsGetRequest
{

	/**
	 * Endpoint path
	 * @return string
	 */
	protected function path(): string
	{
		return '/questions/groups';
	}


	/**
	 * Method
	 * @return string
	 */
	protected function method(): string
	{
		return 'GET';
	}


	/**
	 * Determines whether request requires authorization
	 * @return bool
	 */
	function requiresAuth(): bool
	{
		return true;
	}


	/**
	 * Execute request and return a response
	 * @return QuestionsGroupsResponse
	 * @throws ApiException
	 */
	function execute(): QuestionsGroupsResponse
	{
		return new QuestionsGroupsResponse($this->request());
	}
}
