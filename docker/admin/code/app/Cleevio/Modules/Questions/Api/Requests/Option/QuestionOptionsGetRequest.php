<?php

declare(strict_types=1);

namespace Cleevio\Questions\API\Requests;

use Cleevio\ApiClient\Exception\ApiException;
use Cleevio\ApiClient\Requests\PaginationRequest;
use Cleevio\Questions\API\Responses\QuestionOptionsResponse;
use InvalidArgumentException;

class QuestionOptionsGetRequest extends PaginationRequest implements IQuestionOptionsGetRequest
{

	/**
	 * @var int
	 */
	private $questionId;

	/**
	 * @var string|null
	 */
	private $search;

	/**
	 * @var int|null
	 */
	private $tripId;


	/**
	 * Endpoint path
	 * @return string
	 */
	protected function path(): string
	{
		if ($this->questionId === null) {
			throw new InvalidArgumentException;
		}

		$path = sprintf('/questions/%s/option', $this->questionId);

		$params = [
			'limit' => $this->getLimit(),
			'page' => $this->getPage(),
			'trip' => $this->tripId,
			'search' => $this->search,
		];

		$query = http_build_query($params);

		return strlen($query) > 0
			? $path . '?' . $query
			: $path;
	}


	/**
	 * Method
	 * @return string
	 */
	protected function method(): string
	{
		return 'GET';
	}


	/**
	 * Determines whether request requires authorization
	 * @return bool
	 */
	function requiresAuth(): bool
	{
		return true;
	}


	/**
	 * @return QuestionOptionsResponse
	 * @throws ApiException
	 */
	function execute(): QuestionOptionsResponse
	{
		return new QuestionOptionsResponse($this->request());
	}


	/**
	 * @param int $id
	 */
	public function setQuestionId(int $id): void
	{
		$this->questionId = $id;
	}


	/**
	 * @param string|null $search
	 */
	public function setSearch(?string $search): void
	{
		$this->search = $search;
	}


	/**
	 * @param int|null $id
	 */
	public function setTripId(?int $id): void
	{
		$this->tripId = $id;
	}

}
