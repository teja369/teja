<?php

declare(strict_types=1);

namespace Cleevio\Questions\API\Requests;

use Cleevio\Api\BaseRequest;
use Cleevio\ApiClient\Exception\ApiException;
use Cleevio\Questions\API\Responses\OptionCountriesResponse;
use Cleevio\UI\SetFormModal\IRelationGetRequest;
use Cleevio\UI\SetFormModal\IRelationsResponse;
use InvalidArgumentException;

class QuestionOptionCountriesGetRequest extends BaseRequest implements IQuestionOptionCountriesGetRequest, IRelationGetRequest
{
	/**
	 * @var int
	 */
	private $optionId;

	/**
	 * Endpoint path
	 * @return string
	 */
	protected function path(): string
	{
		if ($this->optionId === null) {
			throw new InvalidArgumentException;
		}

		return sprintf('/questions/option/%s/country', $this->optionId);
	}


	/**
	 * Method
	 * @return string
	 */
	protected function method(): string
	{
		return 'GET';
	}

	/**
	 * Determines whether request requires authorization
	 * @return bool
	 */
	function requiresAuth(): bool
	{
		return true;
	}

	/**
	 * Execute request and return a response
	 * @return OptionCountriesResponse
	 * @throws ApiException
	 */
	function execute(): IRelationsResponse
	{
		return new OptionCountriesResponse($this->request());
	}


	public function setId(int $id): void
	{
		$this->optionId = $id;
	}
}
