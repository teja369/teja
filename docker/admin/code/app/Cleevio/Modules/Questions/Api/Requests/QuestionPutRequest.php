<?php

declare(strict_types=1);

namespace Cleevio\Questions\API\Requests;

use Cleevio\Api\BaseRequest;
use Cleevio\ApiClient\Exception\ApiException;
use Cleevio\ApiClient\Exception\NotFoundException;
use Cleevio\Questions\API\Responses\QuestionResponse;
use InvalidArgumentException;

class QuestionPutRequest extends BaseRequest implements IQuestionGetRequest
{

	/**
	 * @var int
	 */
	private $questionId;


	/**
	 * Endpoint path
	 * @return string
	 */
	protected function path(): string
	{
		if ($this->questionId === null) {
			throw new InvalidArgumentException;
		}

		return sprintf('/questions/%s', $this->questionId);
	}


	/**
	 * Method
	 * @return string
	 */
	protected function method(): string
	{
		return 'PUT';
	}


	/**
	 * Determines whether request requires authorization
	 * @return bool
	 */
	function requiresAuth(): bool
	{
		return true;
	}


	/**
	 * Execute request and return a response
	 * @return QuestionResponse
	 * @throws ApiException
	 * @throws NotFoundException
	 */
	function execute(): QuestionResponse
	{
		return new QuestionResponse($this->request());
	}


	public function setQuestionId(int $id): void
	{
		$this->questionId = $id;
	}
}
