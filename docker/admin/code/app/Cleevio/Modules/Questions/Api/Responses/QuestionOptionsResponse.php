<?php

declare(strict_types=1);

namespace Cleevio\Questions\API\Responses;

use Cleevio\ApiClient\DTO\PaginationResponse;

class QuestionOptionsResponse extends PaginationResponse
{

	/**
	 * @var QuestionOptionResponse[]
	 */
	private $items;


	/**
	 * QuestionOptionsResponse constructor.
	 * @param array $request
	 */
	public function __construct(array $request)
	{
		parent::__construct($request['data']);

		$this->items = array_map(static function ($option) {
			return new QuestionOptionResponse($option);
		}, $request['data']['items']);
	}


	/**
	 * @return array
	 */
	public function getItems(): array
	{
		return $this->items;
	}
}
