<?php

declare(strict_types=1);

namespace Cleevio\Questions\API\Requests;

interface IQuestionVisibilityPutRequest
{

	public function setQuestionId(int $id): void;
}
