<?php

declare(strict_types=1);

namespace Cleevio\Questions\API\Requests;

use Cleevio\Api\BaseRequest;
use Cleevio\ApiClient\Exception\ApiException;
use Cleevio\Quesetions\API\Requests\IGroupQuestionsGetRequest;
use Cleevio\Questions\API\Responses\QuestionsResponse;

class GroupQuestionsGetRequest extends BaseRequest implements IGroupQuestionsGetRequest
{

	/**
	 * @var int
	 */
	private $groupId;


	/**
	 * Endpoint path
	 * @return string
	 */
	protected function path(): string
	{
		return sprintf('/questions/groups/%s/questions', $this->groupId);
	}


	/**
	 * Method
	 * @return string
	 */
	protected function method(): string
	{
		return 'GET';
	}


	/**
	 * Determines whether request requires authorization
	 * @return bool
	 */
	function requiresAuth(): bool
	{
		return true;
	}


	public function setGroupId(int $id): void
	{
		$this->groupId = $id;
	}


	/**
	 * Execute request and return a response
	 * @return QuestionsResponse
	 * @throws ApiException
	 */
	function execute(): QuestionsResponse
	{
		return new QuestionsResponse($this->request());
	}
}
