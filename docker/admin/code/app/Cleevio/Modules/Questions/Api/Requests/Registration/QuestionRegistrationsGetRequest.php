<?php

declare(strict_types=1);

namespace Cleevio\Questions\API\Requests;

use Cleevio\Api\BaseRequest;
use Cleevio\ApiClient\Exception\ApiException;
use Cleevio\Questions\API\Responses\QuestionRegistrationsResponse;
use Cleevio\UI\SetFormModal\IRelationGetRequest;
use Cleevio\UI\SetFormModal\IRelationsResponse;
use InvalidArgumentException;

class QuestionRegistrationsGetRequest extends BaseRequest implements IQuestionRegistrationsGetRequest, IRelationGetRequest
{

	/**
	 * @var int
	 */
	private $questionId;


	/**
	 * Endpoint path
	 * @return string
	 */
	protected function path(): string
	{
		if ($this->questionId === null) {
			throw new InvalidArgumentException;
		}

		return sprintf('/questions/%s/registration', $this->questionId);
	}


	/**
	 * Method
	 * @return string
	 */
	protected function method(): string
	{
		return 'GET';
	}


	/**
	 * Determines whether request requires authorization
	 * @return bool
	 */
	function requiresAuth(): bool
	{
		return true;
	}


	/**
	 * Execute request and return a response
	 * @return QuestionRegistrationsResponse
	 * @throws ApiException
	 */
	function execute(): IRelationsResponse
	{
		return new QuestionRegistrationsResponse($this->request());
	}


	public function setQuestionId(int $id): void
	{
		$this->questionId = $id;
	}


	public function setId(int $id): void
	{
		$this->setQuestionId($id);
	}
}
