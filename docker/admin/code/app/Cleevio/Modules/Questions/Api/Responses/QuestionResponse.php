<?php

declare(strict_types=1);

namespace Cleevio\Questions\API\Responses;

use Cleevio\ApiClient\IResponse;
use InvalidArgumentException;

class QuestionResponse implements IResponse
{

	public const QUESTION_VISIBILITY_ENABLED = 'enabled';
	public const QUESTION_VISIBILITY_DISABLED = 'disabled';
	public const QUESTION_VISIBILITY_PREVIEW = 'preview';
	public const QUESTION_VISIBILITY = [
		self::QUESTION_VISIBILITY_ENABLED => 'success',
		self::QUESTION_VISIBILITY_DISABLED => 'danger',
		self::QUESTION_VISIBILITY_PREVIEW => 'warning',
	];

	/**
	 * @var int
	 */
	private $id;

	/**
	 * @var string
	 */
	private $type;

	/**
	 * @var string
	 */
	private $question;

	/**
	 * @var string|null
	 */
	private $validation;

	/**
	 * @var TextQuestionResponse|null
	 */
	private $text;

	/**
	 * @var array|null
	 */
	private $bool;

	/**
	 * @var NumberQuestionResponse|null
	 */
	private $number;

	/**
	 * @var ChoiceQuestionResponse|null
	 */
	private $choice;

	/**
	 * @var bool
	 */
	private $required;

	/**
	 * @var QuestionResponse|null
	 */
	private $parent;

	/**
	 * @var string|null
	 */
	private $parentRule;

	/**
	 * @var QuestionsGroupResponse
	 */
	private $group;

	/**
	 * @var string|null
	 */
	private $validationHelp;

	/**
	 * @var string|null
	 */
	private $help;

	/**
	 * @var string|null
	 */
	private $placeholder;

	/**
	 * @var bool
	 */
	private $hasChildren;

	/**
	 * @var bool
	 */
	private $isPrefilled;

	/**
	 * @var bool
	 */
	private $isDeleted;

	/**
	 * @var DatetimeQuestionResponse|null
	 */
	private $datetime;

	/**
	 * @var int
	 */
	private $order;

	/**
	 * @var string
	 */
	private $visibility;


	/**
	 * QuestionResponse constructor.
	 * @param array $question
	 */
	public function __construct(array $question)
	{
		if (array_key_exists('id', $question) &&
			array_key_exists('type', $question) &&
			array_key_exists('question', $question) &&
			array_key_exists('validation', $question) &&
			array_key_exists('required', $question) &&
			array_key_exists('order', $question)
		) {
			$this->id = $question['id'];
			$this->type = $question['type'];
			$this->parent = $question['parent'] !== null
				? new QuestionResponse($question['parent'])
				: null;
			$this->parentRule = $question['parentRule'];
			$this->question = $question['question'];
			$this->group = new QuestionsGroupResponse($question['group']);
			$this->required = $question['required'];
			$this->validation = $question['validation'];
			$this->validationHelp = $question['validationHelp'];
			$this->help = $question['help'];
			$this->placeholder = $question['placeholder'];
			$this->hasChildren = (bool) $question['hasChildren'];
			$this->visibility = $question['visibility'];
			$this->isPrefilled = $question['isPrefilled'];
			$this->isDeleted = $question['isDeleted'];
			$this->order = $question['order'];
			$this->text = $question['text'] === null
				? null
				: new TextQuestionResponse($question['text']);
			$this->bool = $question['bool'];
			$this->number = $question['number'] === null
				? null
				: new NumberQuestionResponse($question['number']);
			$this->choice = $question['choice'] === null
				? null
				: new ChoiceQuestionResponse($question['choice']);
			$this->datetime = $question['datetime'] === null
				? null
				: new DatetimeQuestionResponse($question['datetime']);

		} else {
			throw new InvalidArgumentException;
		}
	}


	/**
	 * @return int
	 */
	public function getId(): int
	{
		return $this->id;
	}


	/**
	 * @return string
	 */
	public function getType(): string
	{
		return $this->type;
	}


	/**
	 * @return string
	 */
	public function getQuestion(): string
	{
		return $this->question;
	}


	/**
	 * @return string|null
	 */
	public function getValidation(): ?string
	{
		return $this->validation;
	}


	/**
	 * @return QuestionsGroupResponse
	 */
	public function getGroup(): QuestionsGroupResponse
	{
		return $this->group;
	}


	/**
	 * @return TextQuestionResponse|null
	 */
	public function getText(): ?TextQuestionResponse
	{
		return $this->text;
	}


	/**
	 * @return array|null
	 */
	public function getBool(): ?array
	{
		return $this->bool;
	}


	/**
	 * @return NumberQuestionResponse|null
	 */
	public function getNumber(): ?NumberQuestionResponse
	{
		return $this->number;
	}


	/**
	 * @return ChoiceQuestionResponse|null
	 */
	public function getChoice(): ?ChoiceQuestionResponse
	{
		return $this->choice;
	}


	/**
	 * @return bool
	 */
	public function getRequired(): bool
	{
		return $this->required;
	}

	/**
	 * @return QuestionResponse|null
	 */
	public function getParent(): ?QuestionResponse
	{
		return $this->parent;
	}

	/**
	 * @return string|null
	 */
	public function getParentRule(): ?string
	{
		return $this->parentRule;
	}


	/**
	 * @return string|null
	 */
	public function getValidationHelp(): ?string
	{
		return $this->validationHelp;
	}


	/**
	 * @return string|null
	 */
	public function getHelp(): ?string
	{
		return $this->help;
	}


	/**
	 * @return string|null
	 */
	public function getPlaceholder(): ?string
	{
		return $this->placeholder;
	}

	/**
	 * @return bool
	 */
	public function hasChildren(): bool
	{
		return $this->hasChildren;
	}


	/**
	 * @return mixed
	 */
	public function getVisibility()
	{
		return $this->visibility;
	}


	/**
	 * @return bool
	 */
	public function isPrefilled(): bool
	{
		return $this->isPrefilled;
	}

	/**
	 * @return bool
	 */
	public function isDeleted(): bool
	{
		return $this->isDeleted;
	}

	/**
	 * @return DatetimeQuestionResponse|null
	 */
	public function getDatetime(): ?DatetimeQuestionResponse
	{
		return $this->datetime;
	}


	public function getOrder(): int
	{
		return $this->order;
	}

}
