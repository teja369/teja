<?php

declare(strict_types=1);

namespace Cleevio\Questions\API\Responses;

use Cleevio\ApiClient\IResponse;
use InvalidArgumentException;

class QuestionsGroupResponse implements IResponse
{

	/**
	 * @var int
	 */
	private $id;

	/**
	 * @var string
	 */
	private $type;

	/**
	 * @var string
	 */
	private $name;


	/**
	 * @param array $group
	 */
	public function __construct(array $group)
	{
		if (array_key_exists('id', $group) && array_key_exists('type', $group) && array_key_exists('name', $group)) {
			$this->id = $group['id'];
			$this->name = $group['name'];
			$this->type = $group['type'];
		} else {
			throw new InvalidArgumentException;
		}
	}


	/**
	 * @return int
	 */
	public function getId(): int
	{
		return $this->id;
	}


	/**
	 * @return string
	 */
	public function getType(): string
	{
		return $this->type;
	}


	/**
	 * @return string
	 */
	public function getName(): string
	{
		return $this->name;
	}

}
