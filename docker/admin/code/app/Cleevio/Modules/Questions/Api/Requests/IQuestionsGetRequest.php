<?php

declare(strict_types=1);

namespace Cleevio\Questions\API\Requests;

interface IQuestionsGetRequest
{

	/**
	 * @param int $groupId
	 */
	public function setGroupId(int $groupId): void;


	/**
	 * @param array $params
	 */
	public function setParams(array $params): void;

}
