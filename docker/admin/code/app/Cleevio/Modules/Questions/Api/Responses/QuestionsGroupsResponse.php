<?php

declare(strict_types=1);

namespace Cleevio\Questions\API\Responses;

use Cleevio\ApiClient\IResponse;

class QuestionsGroupsResponse implements IResponse
{

	/**
	 * @var QuestionsGroupResponse[]
	 */
	private $questionGroups = [];


	/**
	 * QuestionsGroupsResponse constructor.
	 * @param array $questionGroups
	 */
	public function __construct(array $questionGroups)
	{
		$this->questionGroups = array_map(static function ($group) {
			return new QuestionsGroupResponse($group);
		}, $questionGroups);
	}

	/**
	 * @return QuestionsGroupResponse[]
	 */
	public function getQuestionGroups(): array
	{
		return $this->questionGroups;
	}

}
