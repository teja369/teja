<?php

declare(strict_types=1);

namespace Cleevio\Questions\API\Responses;

use Cleevio\Translations\Components\SetTranslationFormModal\TranslatableResponse;
use Cleevio\Translations\Components\SetTranslationFormModal\TranslationMapping;
use Cleevio\Translations\Components\SetTranslationFormModal\TranslationMappingField;
use InvalidArgumentException;

class QuestionGroupTranslationResponse extends TranslatableResponse
{

	/**
	 * @var string|null
	 */
	private $name;


	/**
	 * @param array $translation
	 */
	public function __construct(array $translation)
	{
		if (array_key_exists('name', $translation)) {
			$this->name = $translation['name'];
		} else {
			throw new InvalidArgumentException;
		}
	}

	public function getName(): ?string
	{
		return $this->name;
	}


	public function getMapping(): TranslationMapping
	{
		$mapping = new TranslationMapping;
		$mapping->addField(new TranslationMappingField('text', 'back.questions.groups.form.label.name', $this->getName()));

		return $mapping;
	}
}
