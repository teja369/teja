<?php

declare(strict_types=1);

namespace Cleevio\Quesetions\API\Requests;

interface IQuestionsGroupDeleteRequest
{

	public function setGroupId(int $id): void;
}
