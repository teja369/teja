<?php

declare(strict_types=1);

namespace Cleevio\Questions\API\Requests;

use Cleevio\Api\BaseRequest;
use Cleevio\ApiClient\Exception\ApiException;
use Cleevio\Questions\API\Responses\QuestionGroupTranslationResponse;
use Cleevio\Translations\Components\SetTranslationFormModal\IGetTranslationRequest;
use Cleevio\Translations\Components\SetTranslationFormModal\TranslatableResponse;
use InvalidArgumentException;

class QuestionGroupTranslationGetRequest extends BaseRequest implements IQuestionGroupTranslationGetRequest, IGetTranslationRequest
{

	/**
	 * @var int
	 */
	private $questionId;

	/**
	 * @var string
	 */
	private $languageCode;


	/**
	 * Endpoint path
	 * @return string
	 */
	protected function path(): string
	{
		if ($this->questionId === null || $this->languageCode === null) {
			throw new InvalidArgumentException;
		}

		return sprintf('/questions/groups/%s/translation/%s', $this->questionId, $this->languageCode);
	}


	/**
	 * Method
	 * @return string
	 */
	protected function method(): string
	{
		return 'GET';
	}


	/**
	 * Determines whether request requires authorization
	 * @return bool
	 */
	function requiresAuth(): bool
	{
		return true;
	}


	/**
	 * Execute request and return a response
	 * @return QuestionGroupTranslationResponse
	 * @throws ApiException
	 */
	function execute(): TranslatableResponse
	{
		return new QuestionGroupTranslationResponse($this->request());
	}


	public function setLanguageCode(string $languageCode): void
	{
		$this->languageCode = $languageCode;
	}


	public function setId(int $id): void
	{
		$this->questionId = $id;
	}
}
