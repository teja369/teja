<?php

declare(strict_types=1);

namespace Cleevio\Questions\API\Responses;

use Cleevio\ApiClient\IResponse;
use InvalidArgumentException;

class TextQuestionResponse implements IResponse
{

	/**
	 * @var int|null
	 */
	private $maxLength;

	/**
	 * @var int|null
	 */
	private $minLength;


	/**
	 * TextQuestionResponse constructor.
	 * @param array $textQuestion
	 */
	public function __construct(array $textQuestion)
	{
		if (array_key_exists('minLength', $textQuestion) && array_key_exists('maxLength', $textQuestion)) {
			$this->minLength = $textQuestion['minLength'];
			$this->maxLength = $textQuestion['maxLength'];
		} else {
			throw new InvalidArgumentException;
		}
	}


	/**
	 * @return int|null
	 */
	public function getMaxLength(): ?int
	{
		return $this->maxLength;
	}


	/**
	 * @return int|null
	 */
	public function getMinLength(): ?int
	{
		return $this->minLength;
	}

}
