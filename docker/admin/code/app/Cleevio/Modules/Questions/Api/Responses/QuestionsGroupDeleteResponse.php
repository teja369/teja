<?php

declare(strict_types=1);

namespace Cleevio\Questions\API\Responses;

use Cleevio\ApiClient\IResponse;

class QuestionsGroupDeleteResponse implements IResponse
{

}
