<?php

declare(strict_types=1);

namespace Cleevio\Questions\API\Requests;

use Cleevio\Api\BaseRequest;
use Cleevio\ApiClient\DTO\EmptyResponse;
use Cleevio\ApiClient\Exception\ApiException;
use InvalidArgumentException;

class QuestionOptionDeleteRequest extends BaseRequest implements IQuestionOptionDeleteRequest
{

	/**
	 * @var int
	 */
	private $optionId;


	/**
	 * Endpoint path
	 * @return string
	 */
	protected function path(): string
	{
		if ($this->optionId === null) {
			throw new InvalidArgumentException;
		}

		return sprintf('/questions/option/%s', $this->optionId);
	}


	/**
	 * Method
	 * @return string
	 */
	protected function method(): string
	{
		return 'DELETE';
	}


	/**
	 * Determines whether request requires authorization
	 * @return bool
	 */
	function requiresAuth(): bool
	{
		return true;
	}


	/**
	 * Execute request and return a response
	 * @return EmptyResponse
	 * @throws ApiException
	 */
	function execute(): EmptyResponse
	{
		$this->request();

		return new EmptyResponse;
	}


	public function setOptionId(int $id): void
	{
		$this->optionId = $id;
	}
}
