<?php

declare(strict_types=1);

namespace Cleevio\Questions\API\Requests;

use Cleevio\Api\BaseRequest;
use Cleevio\ApiClient\Exception\ApiException;
use Cleevio\Questions\API\Responses\QuestionTranslationResponse;
use Cleevio\Translations\Components\SetTranslationFormModal\IGetTranslationRequest;
use Cleevio\Translations\Components\SetTranslationFormModal\TranslatableResponse;
use InvalidArgumentException;

class QuestionTranslationGetRequest extends BaseRequest implements IQuestionTranslationGetRequest, IGetTranslationRequest
{

	/**
	 * @var int
	 */
	private $questionId;

	/**
	 * @var string
	 */
	private $languageCode;


	/**
	 * Endpoint path
	 * @return string
	 */
	protected function path(): string
	{
		if ($this->questionId === null || $this->languageCode === null) {
			throw new InvalidArgumentException;
		}

		return sprintf('/questions/%s/translation/%s', $this->questionId, $this->languageCode);
	}


	/**
	 * Method
	 * @return string
	 */
	protected function method(): string
	{
		return 'GET';
	}


	/**
	 * Determines whether request requires authorization
	 * @return bool
	 */
	function requiresAuth(): bool
	{
		return true;
	}


	/**
	 * Execute request and return a response
	 * @return QuestionTranslationResponse
	 * @throws ApiException
	 */
	function execute(): TranslatableResponse
	{
		return new QuestionTranslationResponse($this->request());
	}


	public function setLanguageCode(string $languageCode): void
	{
		$this->languageCode = $languageCode;
	}


	public function setId(int $id): void
	{
		$this->questionId = $id;
	}
}
