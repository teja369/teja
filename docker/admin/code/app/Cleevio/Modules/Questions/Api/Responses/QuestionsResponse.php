<?php

declare(strict_types=1);

namespace Cleevio\Questions\API\Responses;

use Cleevio\ApiClient\DTO\PaginationResponse;

class QuestionsResponse extends PaginationResponse
{

	/**
	 * @var QuestionResponse[]
	 */
	private $items = [];


	/**
	 * @param array $response
	 */
	public function __construct(array $response)
	{
		parent::__construct($response['data']);

		$this->items = array_map(static function ($question) {
			return new QuestionResponse($question);
		}, $response['data']['items']);
	}


	/**
	 * @return QuestionResponse[]
	 */
	public function getItems(): array
	{
		return $this->items;
	}
}
