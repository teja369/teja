<?php

declare(strict_types=1);

namespace Cleevio\Questions\API\Responses;

use Cleevio\ApiClient\IResponse;
use InvalidArgumentException;

class DatetimeQuestionResponse implements IResponse
{

	/**
	 * @var string
	 */
	private $variant;


	/**
	 * DatetimeQuestionResponse constructor.
	 * @param array $datetimeQuestion
	 */
	public function __construct(array $datetimeQuestion)
	{
		if (array_key_exists('variant', $datetimeQuestion)) {
			$this->variant = $datetimeQuestion['variant'];
		} else {
			throw new InvalidArgumentException;
		}
	}


	/**
	 * @return string
	 */
	public function getVariant(): string
	{
		return $this->variant;
	}

}
