<?php

declare(strict_types=1);

namespace Cleevio\Questions\API\Responses;

use Cleevio\Countries\API\Responses\CountryResponse;
use Cleevio\UI\SetFormModal\IRelationsResponse;

class OptionCountriesResponse implements IRelationsResponse
{

	/**
	 * @var CountryResponse[]
	 */
	private $items = [];


	/**
	 * OptionCountriesResponse constructor.
	 * @param array $request
	 */
	public function __construct(array $request)
	{
		$this->items = array_map(static function ($host) {
			return new CountryResponse($host);
		}, $request);
	}


	/**
	 * @return CountryResponse[]
	 */
	public function getItems(): array
	{
		return $this->items;
	}

}
