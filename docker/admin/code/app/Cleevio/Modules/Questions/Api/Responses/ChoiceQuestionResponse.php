<?php

declare(strict_types=1);

namespace Cleevio\Questions\API\Responses;

use Cleevio\ApiClient\IResponse;
use InvalidArgumentException;

class ChoiceQuestionResponse implements IResponse
{

	/**
	 * @var bool
	 */
	private $multiChoice;

	/**
	 * @var OptionResponse[]
	 */
	private $options;


	/**
	 * TextQuestionResponse constructor.
	 * @param array $choiceQuestion
	 */
	public function __construct(array $choiceQuestion)
	{
		if (array_key_exists('multiChoice', $choiceQuestion) && array_key_exists('options', $choiceQuestion)) {
			$this->multiChoice = $choiceQuestion['multiChoice'];
			$this->options = array_map(static function ($option) {
				return new OptionResponse($option);
			}, $choiceQuestion['options']);

		} else {
			throw new InvalidArgumentException;
		}
	}


	/**
	 * @return bool
	 */
	public function isMultiChoice(): bool
	{
		return $this->multiChoice;
	}


	/**
	 * @return OptionResponse[]
	 */
	public function getOptions(): array
	{
		return $this->options;
	}

}
