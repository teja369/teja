<?php

declare(strict_types=1);

namespace Cleevio\Questions\API\Requests;

use Cleevio\Api\BaseRequest;
use Cleevio\ApiClient\DTO\EmptyResponse;
use Cleevio\ApiClient\Exception\ApiException;
use InvalidArgumentException;

class QuestionDeleteRequest extends BaseRequest implements IQuestionDeleteRequest
{
	/**
	 * @var int
	 */
	private $questionId;

	/**
	 * Endpoint path
	 * @return string
	 */
	protected function path(): string
	{
		if ($this->questionId === null) {
			throw new InvalidArgumentException;
		}

		return sprintf('/questions/%s', $this->questionId);
	}


	/**
	 * Method
	 * @return string
	 */
	protected function method(): string
	{
		return 'DELETE';
	}


	/**
	 * Determines whether request requires authorization
	 * @return bool
	 */
	function requiresAuth(): bool
	{
		return true;
	}


	/**
	 * Execute request and return a response
	 * @return EmptyResponse
	 * @throws ApiException
	 */
	function execute(): EmptyResponse
	{
		$this->request();

		return new EmptyResponse;
	}

	/**
	 * @param int $id
	 */
	public function setQuestionId(int $id): void
	{
		$this->questionId = $id;
	}
}
