<?php

declare(strict_types=1);

namespace Cleevio\Questions\API\Responses;

use Cleevio\ApiClient\IResponse;
use InvalidArgumentException;

class QuestionOptionResponse implements IResponse
{

	/**
	 * @var int
	 */
	private $id;

	/**
	 * @var string
	 */
	private $text;

	/**
	 * @var int
	 */
	private $order;


	/**
	 * TextQuestionResponse constructor.
	 * @param array $option
	 */
	public function __construct(array $option)
	{
		if (array_key_exists('id', $option) && array_key_exists('text', $option)) {
			$this->id = $option['id'];
			$this->text = $option['text'];
			$this->order = $option['order'];
		} else {
			throw new InvalidArgumentException;
		}
	}


	public function getId(): int
	{
		return $this->id;
	}


	public function getText(): string
	{
		return $this->text;
	}


	/**
	 * @return int
	 */
	public function getOrder(): int
	{
		return $this->order;
	}

}
