<?php

declare(strict_types=1);

namespace Cleevio\Questions\API\Responses;

use Cleevio\Registrations\API\Responses\RegistrationResponse;
use Cleevio\UI\SetFormModal\IRelationsResponse;

class QuestionRegistrationsResponse implements IRelationsResponse
{

	/**
	 * @var RegistrationResponse[]
	 */
	private $items = [];


	/**
	 * RegistrationResponse constructor.
	 * @param array $request
	 */
	public function __construct(array $request)
	{
		$this->items = array_map(static function ($host) {
			return new RegistrationResponse($host);
		}, $request);
	}


	/**
	 * @return RegistrationResponse[]
	 */
	public function getItems(): array
	{
		return $this->items;
	}

}
