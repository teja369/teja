<?php

declare(strict_types=1);

namespace Cleevio\Questions\API\Requests;

use Cleevio\Api\BaseRequest;
use Cleevio\ApiClient\Exception\ApiException;
use Cleevio\Quesetions\API\Requests\IQuestionsGroupDeleteRequest;
use Cleevio\Questions\API\Responses\QuestionsGroupDeleteResponse;

class QuestionsGroupDeleteRequest extends BaseRequest implements IQuestionsGroupDeleteRequest
{

	/**
	 * @var int
	 */
	private $groupId;


	/**
	 * Endpoint path
	 * @return string
	 */
	protected function path(): string
	{
		return sprintf('/questions/groups/%s', $this->groupId);
	}


	/**
	 * Method
	 * @return string
	 */
	protected function method(): string
	{
		return 'DELETE';
	}


	/**
	 * Determines whether request requires authorization
	 * @return bool
	 */
	function requiresAuth(): bool
	{
		return true;
	}


	/**
	 * @param int $id
	 */
	function setGroupId(int $id): void
	{
		$this->groupId = $id;
	}


	/**
	 * Execute request and return a response
	 * @return QuestionsGroupDeleteResponse
	 * @throws ApiException
	 */
	function execute(): QuestionsGroupDeleteResponse
	{
		$this->request();

		return new QuestionsGroupDeleteResponse;
	}
}
