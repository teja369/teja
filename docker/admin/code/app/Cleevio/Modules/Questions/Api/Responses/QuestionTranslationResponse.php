<?php

declare(strict_types=1);

namespace Cleevio\Questions\API\Responses;

use Cleevio\Translations\Components\SetTranslationFormModal\TranslatableResponse;
use Cleevio\Translations\Components\SetTranslationFormModal\TranslationMapping;
use Cleevio\Translations\Components\SetTranslationFormModal\TranslationMappingField;
use InvalidArgumentException;

class QuestionTranslationResponse extends TranslatableResponse
{

	/**
	 * @var string|null
	 */
	private $text;

	/**
	 * @var string|null
	 */
	private $help;

	/**
	 * @var string|null
	 */
	private $placeholder;

	/**
	 * @var string|null
	 */
	private $validationHelp;


	/**
	 * @param array $translation
	 */
	public function __construct(array $translation)
	{
		if (array_key_exists('text', $translation) && array_key_exists('help', $translation) && array_key_exists('placeholder', $translation) && array_key_exists('validationHelp', $translation)) {
			$this->text = $translation['text'];
			$this->help = $translation['help'];
			$this->placeholder = $translation['placeholder'];
			$this->validationHelp = $translation['validationHelp'];
		} else {
			throw new InvalidArgumentException;
		}
	}


	public function getText(): ?string
	{
		return $this->text;
	}


	public function getHelp(): ?string
	{
		return $this->help;
	}


	public function getPlaceholder(): ?string
	{
		return $this->placeholder;
	}


	public function getValidationHelp(): ?string
	{
		return $this->validationHelp;
	}


	public function getMapping(): TranslationMapping
	{
		$mapping = new TranslationMapping;
		$mapping->addField(new TranslationMappingField('text', 'back.questions.form.label.question', $this->getText()));
		$mapping->addField(new TranslationMappingField('help', 'back.questions.form.help', $this->getHelp()));
		$mapping->addField(new TranslationMappingField('placeholder', 'back.questions.form.placeholder', $this->getPlaceholder()));
		$mapping->addField(new TranslationMappingField('validationHelp', 'back.questions.form.validation-help', $this->getValidationHelp()));

		return $mapping;
	}
}
