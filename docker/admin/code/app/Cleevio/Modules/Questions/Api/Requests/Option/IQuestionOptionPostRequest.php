<?php

declare(strict_types=1);

namespace Cleevio\Questions\API\Requests;

interface IQuestionOptionPostRequest
{

	public function setId(int $id): void;
}
