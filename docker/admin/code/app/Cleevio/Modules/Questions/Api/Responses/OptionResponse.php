<?php

declare(strict_types=1);

namespace Cleevio\Questions\API\Responses;

use Cleevio\ApiClient\IResponse;
use InvalidArgumentException;

class OptionResponse implements IResponse
{

	/**
	 * @var int
	 */
	private $id;

	/**
	 * @var string
	 */
	private $text;

	/**
	 * @var int
	 */
	private $order;


	/**
	 * QuestionResponse constructor.
	 * @param array $option
	 */
	public function __construct(array $option)
	{
		if (isset($option['id'], $option['text'], $option['order'])) {
			$this->id = $option['id'];
			$this->text = $option['text'];
			$this->order = $option['order'];
		} else {
			throw new InvalidArgumentException;
		}
	}


	/**
	 * @return int
	 */
	public function getId(): int
	{
		return $this->id;
	}


	/**
	 * @return string
	 */
	public function getText(): string
	{
		return $this->text;
	}


	public function getOrder(): int
	{
		return $this->order;
	}
}
