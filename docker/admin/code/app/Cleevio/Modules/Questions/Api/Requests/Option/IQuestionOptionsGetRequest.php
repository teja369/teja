<?php

declare(strict_types=1);

namespace Cleevio\Questions\API\Requests;

interface IQuestionOptionsGetRequest
{

	/**
	 * @param int $id
	 */
	public function setQuestionId(int $id): void;


	/**
	 * @param int|null $id
	 */
	public function setTripId(?int $id): void;


	/**
	 * @param string|null $search
	 */
	public function setSearch(?string $search): void;
}
