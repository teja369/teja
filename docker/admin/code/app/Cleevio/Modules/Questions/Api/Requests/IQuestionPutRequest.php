<?php

declare(strict_types=1);

namespace Cleevio\Questions\API\Requests;

interface IQuestionPutRequest
{

	public function setQuestionId(int $id): void;
}
