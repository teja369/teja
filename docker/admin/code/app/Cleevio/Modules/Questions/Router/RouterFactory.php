<?php

declare(strict_types=1);

namespace Cleevio\Questions\Router;

use Nette;
use Nette\Application\Routers\Route;
use Nette\Application\Routers\RouteList;

class RouterFactory
{
	/**
	 * @return Nette\Application\IRouter
	 */
	public function createRouter()
	{
		$router = new RouteList;

		$router[] = $usersModule = new RouteList('Questions');

		$usersModule[] = new Route('questions/<presenter>/<action>[/<id>]', [
			'presenter' => 'Questions',
			'action' => 'default',
			'module' => 'Back',
		]);

		return $router;
	}

}
