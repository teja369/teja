<?php

declare(strict_types=1);

namespace Cleevio\Questions\BackModule\Presenters;

use Cleevio\Countries\Components\SetCountryFormModal\ISetCountryFormModalFactory;
use Cleevio\Countries\Components\SetCountryFormModal\SetCountryFormModal;
use Cleevio\Questions\API\Requests\QuestionOptionCountriesGetRequest;
use Cleevio\Questions\API\Requests\QuestionOptionCountriesPutRequest;
use Cleevio\Questions\API\Requests\QuestionOptionGetRequest;
use Nette\NotImplementedException;

class OptionsPresenter extends BasePresenter
{

	/**
	 * @var QuestionOptionCountriesGetRequest
	 */
	private $questionOptionCountriesGetRequest;

	/**
	 * @var QuestionOptionCountriesPutRequest
	 */
	private $questionOptionCountriesPutRequest;

	/**
	 * @var QuestionOptionGetRequest
	 */
	private $questionOptionGetRequest;


	public function __construct(
		QuestionOptionCountriesGetRequest $questionOptionCountriesGetRequest,
		QuestionOptionCountriesPutRequest $questionOptionCountriesPutRequest,
		QuestionOptionGetRequest $questionOptionGetRequest
	)
	{
		parent::__construct();
		$this->questionOptionCountriesGetRequest = $questionOptionCountriesGetRequest;
		$this->questionOptionCountriesPutRequest = $questionOptionCountriesPutRequest;
		$this->questionOptionGetRequest = $questionOptionGetRequest;
	}


	/**
	 * @param int $id
	 * @param int $questionId
	 * @throws \Cleevio\ApiClient\Exception\ApiException
	 */
	public function actionEdit(int $id, int $questionId): void
	{
		$this->template->questionId = $questionId;

		$this->questionOptionGetRequest->setId($id);
		$this->template->option = $this->questionOptionGetRequest->execute();

		$this->questionOptionCountriesGetRequest->setId($id);
		$this->template->countries = $this->questionOptionCountriesGetRequest->execute()->getItems();

		$this->questionOptionCountriesPutRequest->setId($id);
		$this['fieldCountryModalForm']->setGetRequest($this->questionOptionCountriesGetRequest);
		$this['fieldCountryModalForm']->setPutRequest($this->questionOptionCountriesPutRequest);
	}


	public function handleDeleteCountry(int $id): void
	{
		throw new NotImplementedException(sprintf('Cannot remove ID %s', $id));
	}


	protected function createComponentFieldCountryModalForm(ISetCountryFormModalFactory $factory): SetCountryFormModal
	{
		$control = $factory->create();

		$control->onSuccess[] = function () {
			$this->questionOptionCountriesGetRequest->setId((int) $this->getParameter('id'));
			$this->template->countries = $this->questionOptionCountriesGetRequest->execute()->getItems();
			$this->redrawControl('fieldCountries');
		};

		return $control;
	}

}
