<?php

declare(strict_types=1);

namespace Cleevio\Questions\BackModule\Presenters;

use Cleevio\ApiClient\Exception\ApiException;
use Cleevio\ApiClient\Exception\NotFoundException;
use Cleevio\Questions\API\Requests\QuestionDeleteRequest;
use Cleevio\Questions\API\Requests\QuestionGetRequest;
use Cleevio\Questions\API\Requests\QuestionsGetRequest;
use Cleevio\Questions\API\Requests\QuestionTranslationGetRequest;
use Cleevio\Questions\API\Requests\QuestionTranslationPutRequest;
use Cleevio\Questions\API\Requests\QuestionVisibilityPutRequest;
use Cleevio\Questions\API\Responses\QuestionResponse;
use Cleevio\Questions\Components\FilterForm\FilterForm;
use Cleevio\Questions\Components\FilterForm\IFilterFormFactory;
use Cleevio\Questions\Components\QuestionForm\IQuestionFormFactory;
use Cleevio\Questions\Components\QuestionForm\QuestionForm;
use Cleevio\Translations\Components\SetTranslationFormModal\ISetTranslationFormModal;
use Cleevio\Translations\Components\SetTranslationFormModal\SetTranslationFormModal;
use Cleevio\UI\ApiPaginationControl\ApiPaginationControl;
use Cleevio\UI\ApiPaginationControl\IApiPaginationControlFactory;
use Nette\Application\AbortException;
use Nette\Application\BadRequestException;

class QuestionsPresenter extends BasePresenter
{

	/**
	 * @var QuestionGetRequest
	 */
	private $questionGetRequest;

	/**
	 * @var QuestionsGetRequest
	 */
	private $questionsGetRequest;

	/**
	 * @var QuestionTranslationGetRequest
	 */
	private $questionTranslationGetRequest;

	/**
	 * @var QuestionTranslationPutRequest
	 */
	private $questionTranslationPutRequest;

	/**
	 * @var QuestionDeleteRequest
	 */
	private $questionDeleteRequest;

	/**
	 * @var QuestionVisibilityPutRequest
	 */
	private $questionVisibilityPutRequest;


	/**
	 * @param QuestionGetRequest $questionGetRequest
	 * @param QuestionsGetRequest $questionsGetRequest
	 * @param QuestionTranslationGetRequest $questionTranslationGetRequest
	 * @param QuestionTranslationPutRequest $questionTranslationPutRequest
	 * @param QuestionDeleteRequest $questionDeleteRequest
	 * @param QuestionVisibilityPutRequest $questionVisibilityPutRequest
	 */
	public function __construct(
		QuestionGetRequest $questionGetRequest,
		QuestionsGetRequest $questionsGetRequest,
		QuestionTranslationGetRequest $questionTranslationGetRequest,
		QuestionTranslationPutRequest $questionTranslationPutRequest,
		QuestionDeleteRequest $questionDeleteRequest,
		QuestionVisibilityPutRequest $questionVisibilityPutRequest
	)
	{
		parent::__construct();
		$this->questionGetRequest = $questionGetRequest;
		$this->questionsGetRequest = $questionsGetRequest;
		$this->questionTranslationGetRequest = $questionTranslationGetRequest;
		$this->questionTranslationPutRequest = $questionTranslationPutRequest;
		$this->questionDeleteRequest = $questionDeleteRequest;
		$this->questionVisibilityPutRequest = $questionVisibilityPutRequest;
	}


	public function startup()
	{
		parent::startup();
	}

	public function actionDefault(
		?string $country,
		?string $state,
		?string $search = null
	): void
	{

		$this->questionsGetRequest->setParams([
			'country' => $country,
			'state' => $state,
			'search' => $search,
		]);

		$this->template->items = $this['apiPagination']->getItems()->getItems();
		$this->template->visibility = QuestionResponse::QUESTION_VISIBILITY;

		$this['filterForm']['form']->setDefaults([
			'country' => $country,
			'state' => $state,
			'search' => $search,
		]);

		$this['filterForm']->onSuccess[] = function ($params) {
			$this->redirect(':Questions:Back:Questions:default', $params);
		};
	}

	public function actionCreate(?int $questionId = null): void
	{
		if ($questionId !== null) {
			$this->questionGetRequest->setQuestionId($questionId);

			try {
				$question = $this->questionGetRequest->execute();
				$this['questionForm']->setQuestionResponse($question);
			} catch (NotFoundException $exception) {
				$this->flashMessage($this->translator->translate('back.questions.not-found'));
				$this->redirectPermanent('this', ['questionId' => null]);
			} catch (ApiException $exception) {
				$this->flashMessage($this->translator->translate('back.questions.unknown-error'));
				$this->redirectPermanent('this', ['questionId' => null]);
			}
		}
	}


	public function actionEdit(?int $questionId = null): void
	{
		if ($questionId !== null) {
			$this->questionGetRequest->setQuestionId($questionId);

			try {
				$question = $this->questionGetRequest->execute();
				$this['questionForm']->setQuestionResponse($question);
			} catch (NotFoundException $exception) {
				$this->flashMessage($this->translator->translate('back.questions.not-found'));
				$this->redirectPermanent('this', ['questionId' => null]);
			} catch (ApiException $exception) {
				$this->flashMessage($this->translator->translate('back.questions.unknown-error'));
				$this->redirectPermanent('this', ['questionId' => null]);
			}
		}
	}


	/**
	 * @param int $questionId
	 * @param string $visibility
	 * @throws BadRequestException
	 * @throws AbortException
	 */
	public function handleVisibility(int $questionId, string $visibility): void
	{
		try {
			$this->questionVisibilityPutRequest->setQuestionId($questionId);
			$this->questionVisibilityPutRequest->setData([
				'visibility' => $visibility,
			]);
			$this->questionVisibilityPutRequest->execute();

			$this->flashMessage($this->translator->translate('back.questions.visibility.success'), 'success');

			$this->template->items = $this['apiPagination']->getItems()->getItems();

			if ($this->isAjax()) {
				$this->redrawControl('flashes');
				$this->redrawControl('questions');
			} else {
				$this->redirect('this');
			}
		} catch (ApiException $e) {
			$this->error($this->translator->translate('back.' . $e->getMessage()), $e->getCode());
		}
	}


	/**
	 * @param int $questionId
	 * @throws AbortException
	 * @throws BadRequestException
	 */
	public function handleDelete(int $questionId): void
	{
		try {
			$this->questionDeleteRequest->setQuestionId($questionId);
			$this->questionDeleteRequest->execute();

			$this->template->items = $this['apiPagination']->getItems()->getItems();

			$this->flashMessage($this->translator->translate('back.questions.delete.success'), 'success');

			if ($this->isAjax()) {
				$this->redrawControl('flashes');
				$this->redrawControl('questions');
			} else {
				$this->redirect('this');
			}
		} catch (ApiException $ex) {
			$this->error($this->translator->translate('back.' . $ex->getMessage()), $ex->getCode());
		}
	}


	protected function createComponentTranslationOptionModalForm(ISetTranslationFormModal $factory
	): SetTranslationFormModal
	{
		$control = $factory->create();
		$control->setGetRequest($this->questionTranslationGetRequest);
		$control->setPutRequest($this->questionTranslationPutRequest);

		return $control;
	}


	protected function createComponentQuestionForm(IQuestionFormFactory $questionFormFactory): QuestionForm
	{
		$control = $questionFormFactory->create();

		$control->onSuccess[] = function (QuestionResponse $questionResponse) {
			$this->redirectPermanent('this', ['questionId' => $questionResponse->getId()]);
		};

		return $control;
	}


	protected function createComponentApiPagination(IApiPaginationControlFactory $factory): ApiPaginationControl
	{
		$control = $factory->create();
		$control->setRequest($this->questionsGetRequest);

		return $control;
	}


	protected function createComponentFilterForm(IFilterFormFactory $filterFormFactory): FilterForm
	{
		return $filterFormFactory->create();
	}
}
