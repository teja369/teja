<?php

declare(strict_types=1);

namespace Cleevio\Questions\BackModule\Presenters;

use Cleevio\ApiClient\Exception\ApiException;
use Cleevio\ApiClient\Exception\BadRequestException;
use Cleevio\Questions\API\Requests\QuestionGetRequest;
use Cleevio\Questions\API\Requests\QuestionGroupTranslationGetRequest;
use Cleevio\Questions\API\Requests\QuestionGroupTranslationPutRequest;
use Cleevio\Questions\API\Requests\QuestionPutRequest;
use Cleevio\Questions\API\Requests\QuestionsGetRequest;
use Cleevio\Questions\API\Requests\QuestionsGroupDeleteRequest;
use Cleevio\Questions\API\Requests\QuestionsGroupGetRequest;
use Cleevio\Questions\API\Requests\QuestionsGroupsGetRequest;
use Cleevio\Questions\Components\GroupForm\GroupForm;
use Cleevio\Questions\Components\GroupForm\IGroupFormFactory;
use Cleevio\Translations\Components\SetTranslationFormModal\ISetTranslationFormModal;
use Cleevio\Translations\Components\SetTranslationFormModal\SetTranslationFormModal;
use Cleevio\UI\ApiPaginationControl\ApiPaginationControl;
use Cleevio\UI\ApiPaginationControl\IApiPaginationControlFactory;
use Nette\Application\AbortException;

class GroupsPresenter extends BasePresenter
{

	/**
	 * @var QuestionsGroupsGetRequest
	 */
	private $questionsGroupsGetRequest;

	/**
	 * @var QuestionsGroupDeleteRequest
	 */
	private $questionsGroupDeleteRequest;

	/**
	 * @var QuestionsGroupGetRequest
	 */
	private $questionsGroupGetRequest;

	/**
	 * @var QuestionGroupTranslationGetRequest
	 */
	private $questionGroupTranslationGetRequest;

	/**
	 * @var QuestionGroupTranslationPutRequest
	 */
	private $questionGroupTranslationPutRequest;

	/**
	 * @var QuestionsGetRequest
	 */
	private $questionsGetRequest;

	/**
	 * @var QuestionPutRequest
	 */
	private $questionPutRequest;

	/**
	 * @var QuestionGetRequest
	 */
	private $questionGetRequest;


	/**
	 * @param QuestionsGroupsGetRequest $questionsGroupsGetRequest
	 * @param QuestionsGroupDeleteRequest $questionsGroupDeleteRequest
	 * @param QuestionsGroupGetRequest $questionsGroupGetRequest
	 * @param QuestionGroupTranslationGetRequest $questionGroupTranslationGetRequest
	 * @param QuestionGroupTranslationPutRequest $questionGroupTranslationPutRequest
	 * @param QuestionsGetRequest $questionsGetRequest
	 * @param QuestionPutRequest $questionPutRequest
	 * @param QuestionGetRequest $questionGetRequest
	 */
	public function __construct(
		QuestionsGroupsGetRequest $questionsGroupsGetRequest,
		QuestionsGroupDeleteRequest $questionsGroupDeleteRequest,
		QuestionsGroupGetRequest $questionsGroupGetRequest,
		QuestionGroupTranslationGetRequest $questionGroupTranslationGetRequest,
		QuestionGroupTranslationPutRequest $questionGroupTranslationPutRequest,
		QuestionsGetRequest $questionsGetRequest,
		QuestionPutRequest $questionPutRequest,
		QuestionGetRequest $questionGetRequest
	)
	{
		parent::__construct();
		$this->questionsGroupsGetRequest = $questionsGroupsGetRequest;
		$this->questionsGroupDeleteRequest = $questionsGroupDeleteRequest;
		$this->questionsGroupGetRequest = $questionsGroupGetRequest;
		$this->questionGroupTranslationGetRequest = $questionGroupTranslationGetRequest;
		$this->questionGroupTranslationPutRequest = $questionGroupTranslationPutRequest;
		$this->questionsGetRequest = $questionsGetRequest;
		$this->questionPutRequest = $questionPutRequest;
		$this->questionGetRequest = $questionGetRequest;
	}


	public function startup()
	{
		parent::startup();
	}


	/**
	 * @throws \Nette\Application\BadRequestException
	 */
	public function renderDefault(): void
	{
		try {
			$groups = $this->questionsGroupsGetRequest->execute();
			$this->template->groups = $groups->getQuestionGroups();
		} catch (ApiException $ex) {
			$this->error($this->translator->translate('back.' . $ex->getMessage()), $ex->getCode());
		}
	}


	/**
	 * @param int $id
	 * @throws \Nette\Application\BadRequestException
	 */
	public function renderEdit(int $id): void
	{
		try {
			$this->questionsGroupGetRequest->setGroupId($id);
			$group = $this->questionsGroupGetRequest->execute();

			$this->template->questions = $this['apiPagination']->getItems()->getItems();

			$this['groupForm']['form']->setDefaults([
				'name' => $group->getName(),
				'type' => $group->getType(),
				'id' => $group->getId(),
			]);
		} catch (ApiException $ex) {
			$this->error($this->translator->translate('back.' . $ex->getMessage()), $ex->getCode());
		}
	}


	/**
	 * @param int $id
	 * @throws AbortException
	 * @throws \Nette\Application\BadRequestException
	 */
	public function handleDelete(int $id): void
	{
		try {
			$this->questionsGroupDeleteRequest->setGroupId($id);
			$this->questionsGroupDeleteRequest->execute();

			$this->template->groups = $this->questionsGroupsGetRequest->execute()->getQuestionGroups();

			$this->flashMessage($this->translator->translate('back.questions.groups.delete.success'), 'success');

			if ($this->isAjax()) {
				$this->redrawControl('flashes');
				$this->redrawControl('groups');
			} else {
				$this->redirect(':Countries:Back:Country:');
			}
		} catch (ApiException $ex) {
			$this->error($this->translator->translate('back.' . $ex->getMessage()), $ex->getCode());
		}
	}


	public function handleSaveOptionPosition(int $questionId): void
	{
		try {
			$order = max(0, (int) $this->request->getPost('position'));

			$this->questionGetRequest->setQuestionId($questionId);
			$question = $this->questionGetRequest->execute();

			$this->questionPutRequest->setQuestionId($questionId);
			$this->questionPutRequest->setData([
				'text' => $question->getQuestion(),
				'type' => $question->getType(),
				'group' => $question->getGroup()->getId(),
				'required' => $question->getRequired(),
				'order' => $order,
			]);
			$this->questionPutRequest->execute();

			$this->template->questions = $this['apiPagination']->getItems()->getItems();
			$this->redrawControl('questions');
			$this->redrawControl("flashes");

		} catch (BadRequestException $e) {
			$this->flashMessage($this->translator->translate('back.' . $e->getMessage(), $e->getParams()), 'danger');
			$this->redrawControl("flashes");
		} catch (\Exception $e) {
			$this->flashMessage($this->translator->translate('back.questions.options.error'), 'danger');
			$this->redrawControl("flashes");
		}
	}


	protected function createComponentGroupForm(IGroupFormFactory $factory): GroupForm
	{
		$control = $factory->create();

		$control->onSuccess[] = function ($message) {
			$this->flashMessage($this->translator->translate($message), 'success');
			$this->redirect(':Questions:Back:Groups:default');
		};

		return $control;
	}

	protected function createComponentTranslationOptionModalForm(ISetTranslationFormModal $factory): SetTranslationFormModal
	{
		$control = $factory->create();
		$control->setGetRequest($this->questionGroupTranslationGetRequest);
		$control->setPutRequest($this->questionGroupTranslationPutRequest);

		return $control;
	}


	protected function createComponentApiPagination(IApiPaginationControlFactory $factory): ApiPaginationControl
	{
		$this->questionsGetRequest->setGroupId((int) $this->getParameter('id'));

		$control = $factory->create();
		$control->setRequest($this->questionsGetRequest);

		return $control;
	}
}
