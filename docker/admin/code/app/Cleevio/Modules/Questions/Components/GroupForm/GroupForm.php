<?php

declare(strict_types=1);

namespace Cleevio\Questions\Components\GroupForm;

use Cleevio\ApiClient\Exception\ApiException;
use Cleevio\ApiClient\Exception\BadRequestException;
use Cleevio\Questions\API\Requests\QuestionsGroupPostRequest;
use Cleevio\Questions\API\Requests\QuestionsGroupPutRequest;
use Cleevio\UI\BaseControl;
use Cleevio\UI\BaseForm;
use Nette\Localization\ITranslator;

class GroupForm extends BaseControl
{

	/**
	 * @var ITranslator
	 */
	private $translator;

	/**
	 * @var array
	 */
	public $onSuccess = [];

	/**
	 * @var array
	 */
	public $onError = [];

	/**
	 * @var QuestionsGroupPostRequest
	 */
	private $questionsGroupPostRequest;

	/**
	 * @var QuestionsGroupPutRequest
	 */
	private $questionsGroupPutRequest;


	public function __construct(
		ITranslator $translator,
		QuestionsGroupPostRequest $questionsGroupPostRequest,
		QuestionsGroupPutRequest $questionsGroupPutRequest
	)
	{
		$this->translator = $translator;
		$this->questionsGroupPostRequest = $questionsGroupPostRequest;
		$this->questionsGroupPutRequest = $questionsGroupPutRequest;
	}


	public function render(): void
	{
		$this->template->render(__DIR__ . '/templates/default.latte');
	}


	public function createComponentForm(): BaseForm
	{
		$form = new BaseForm;

		$form->setTranslator($this->translator);

		$form->addText('name', 'back.questions.groups.form.label.name')
			->setRequired('back.questions.groups.form.required.name');

		$form->addSelect('type', 'back.questions.groups.form.label.type', [
			'purpose' => 'back.questions.groups.form.type-option.purpose',
			'detail' => 'back.questions.groups.form.type-option.detail',
		])->setRequired('back.questions.groups.form.required.type');

		$form->addHidden('id');

		$form->addSubmit('send', 'back.questions.groups.form.label.submit');

		$form->onSuccess[] = function (BaseForm $form) {
			$values = $form->getValues();

			try {
				if ($values->id === '') {
					$this->questionsGroupPostRequest->setData([
						'name' => $values->name,
						'type' => $values->type,
					]);
					$this->questionsGroupPostRequest->execute();

					$this->onSuccess('back.questions.groups.create.success');
				} else {
					$this->questionsGroupPutRequest->setGroupId((int) $values->id);
					$this->questionsGroupPutRequest->setData([
						'name' => $values->name,
						'type' => $values->type,
					]);
					$this->questionsGroupPutRequest->execute();

					$this->onSuccess('back.questions.groups.update.success');
				}
			} catch (BadRequestException $e) {
				$this->flashMessage($this->translator->translate('back.' . $e->getMessage(), $e->getParams()), 'danger');
			} catch (ApiException $e) {
				$this->flashMessage('back.questions.form.error.unknown', 'danger');
			}
		};

		return $form;
	}

}

interface IGroupFormFactory
{

	/**
	 * @return GroupForm
	 */
	function create();
}
