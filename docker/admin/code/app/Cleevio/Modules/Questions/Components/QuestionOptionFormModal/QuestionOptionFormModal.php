<?php

declare(strict_types=1);

namespace Cleevio\Questions\Components\QuestionOptionFormModal;

use Cleevio\ApiClient\Exception\BadRequestException;
use Cleevio\Questions\API\Requests\QuestionOptionPostRequest;
use Cleevio\UI\BaseControlModal;
use Cleevio\UI\BaseForm;
use Nette\Localization\ITranslator;

class QuestionOptionFormModal extends BaseControlModal
{

	/**
	 * @var int
	 */
	private $questionId;

	/**
	 * @var array
	 */
	public $onSuccess = [];

	/**
	 * @var QuestionOptionPostRequest
	 */
	private $questionOptionPostRequest;

	/**
	 * @var ITranslator
	 */
	private $translator;


	public function __construct(
		QuestionOptionPostRequest $questionOptionPostRequest,
		ITranslator $translator
	)
	{
		parent::__construct();
		$this->setTemplateDirectory(__DIR__ . '/templates');
		$this->questionOptionPostRequest = $questionOptionPostRequest;
		$this->translator = $translator;
	}


	public function handleShowModal(): void
	{
		$this->setView('default');
	}


	protected function createComponentForm(): BaseForm
	{
		$form = new BaseForm;
		$form->setTranslator($this->translator);

		$form->addTextArea('option', 'back.questions.options.form.field-option')
			->setRequired('back.questions.options.validation.required.option');

		$form->addSubmit('send', 'back.questions.options.form.button-send');

		$form->onSuccess[] = function (BaseForm $form) {
			$values = $form->getValues();

			try {
				$options = preg_split("/\r\n|\n|\r/", $values->option);

				if ($options === false) {
					throw new \InvalidArgumentException;
				}

				$options = array_filter($options, static function (string $option) {
					return preg_match('/^.*[a-z0-9_]+.*$/', $option);
				});

				$options = array_map(static function (string $option) {
					return ['text' => $option];
				}, $options);

				$this->questionOptionPostRequest->setId($this->questionId);
				$this->questionOptionPostRequest->setData(array_values($options));

				$response = $this->questionOptionPostRequest->execute();
				$this->onSuccess($response);

				if (property_exists($form['option'], 'value')) {
					$form['option']->value = '';
				}

				$this->flashMessage($this->translator->translate('back.questions.options.success'), 'success');
				$this->redrawControl();

			} catch (BadRequestException $e) {
				$this->flashMessage($this->translator->translate('back.' . $e->getMessage(), $e->getParams()), 'danger');
			} catch (\Exception $e) {
				$this->flashMessage($this->translator->translate('back.questions.options.error'), 'danger');
				$this->redrawControl();
			}
		};

		return $form;
	}


	public function setQuestionId(int $questionId): void
	{
		$this->questionId = $questionId;
	}
}

interface IQuestionOptionFormModalFactory
{

	/**
	 * @return QuestionOptionFormModal
	 */
	public function create(): QuestionOptionFormModal;
}
