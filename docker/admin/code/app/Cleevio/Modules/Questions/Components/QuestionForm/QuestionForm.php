<?php

declare(strict_types=1);

namespace Cleevio\Questions\Components\QuestionForm;

use Cleevio\ApiClient\Exception\ApiException;
use Cleevio\ApiClient\Exception\BadRequestException;
use Cleevio\Countries\Components\SetCountryFormModal\ISetCountryFormModalFactory;
use Cleevio\Countries\Components\SetCountryFormModal\SetCountryFormModal;
use Cleevio\Hosts\Components\SetCountryFormModal\ISetHostFormModalFactory;
use Cleevio\Hosts\Components\SetCountryFormModal\SetHostFormModal;
use Cleevio\Questions\API\Requests\QuestionCountriesGetRequest;
use Cleevio\Questions\API\Requests\QuestionCountriesPutRequest;
use Cleevio\Questions\API\Requests\QuestionGetRequest;
use Cleevio\Questions\API\Requests\QuestionHostsGetRequest;
use Cleevio\Questions\API\Requests\QuestionHostsPutRequest;
use Cleevio\Questions\API\Requests\QuestionOptionDeleteRequest;
use Cleevio\Questions\API\Requests\QuestionOptionGetRequest;
use Cleevio\Questions\API\Requests\QuestionOptionPutRequest;
use Cleevio\Questions\API\Requests\QuestionOptionsGetRequest;
use Cleevio\Questions\API\Requests\QuestionOptionTranslationGetRequest;
use Cleevio\Questions\API\Requests\QuestionOptionTranslationPutRequest;
use Cleevio\Questions\API\Requests\QuestionPostRequest;
use Cleevio\Questions\API\Requests\QuestionPutRequest;
use Cleevio\Questions\API\Requests\QuestionRegistrationsGetRequest;
use Cleevio\Questions\API\Requests\QuestionRegistrationsPutRequest;
use Cleevio\Questions\API\Requests\QuestionsGroupsGetRequest;
use Cleevio\Questions\API\Responses\ChoiceQuestionResponse;
use Cleevio\Questions\API\Responses\NumberQuestionResponse;
use Cleevio\Questions\API\Responses\QuestionResponse;
use Cleevio\Questions\API\Responses\TextQuestionResponse;
use Cleevio\Questions\Components\QuestionOptionFormModal\IQuestionOptionFormModalFactory;
use Cleevio\Questions\Components\QuestionOptionFormModal\QuestionOptionFormModal;
use Cleevio\Questions\Components\QuestionParentFormModal\IQuestionParentFormModelFactory;
use Cleevio\Questions\Components\QuestionParentFormModal\QuestionParentFormModal;
use Cleevio\Registrations\Components\SetRegistrationFormModal\ISetRegistrationFormModalFactory;
use Cleevio\Registrations\Components\SetRegistrationFormModal\SetRegistrationFormModal;
use Cleevio\Translations\API\Requests\LanguagesGetRequest;
use Cleevio\Translations\API\Responses\LanguageResponse;
use Cleevio\Translations\Components\SetTranslationFormModal\ISetTranslationFormModal;
use Cleevio\Translations\Components\SetTranslationFormModal\SetTranslationFormModal;
use Cleevio\UI\ApiPaginationControl\ApiPaginationControl;
use Cleevio\UI\ApiPaginationControl\IApiPaginationControlFactory;
use Cleevio\UI\BaseControl;
use Cleevio\UI\BaseForm;
use Kdyby\Autowired\AutowireComponentFactories;
use Nette\Application\IPresenter;
use Nette\Http\IRequest;
use Nette\Localization\ITranslator;

class QuestionForm extends BaseControl
{

	use AutowireComponentFactories;

	/**
	 * @var ITranslator
	 */
	private $translator;

	/**
	 * @var array
	 */
	public $onSuccess = [];

	/**
	 * @var array
	 */
	public $onError = [];

	/**
	 * @var array|LanguageResponse[]
	 */
	private $languages;

	/**
	 * @var QuestionsGroupsGetRequest
	 */
	private $questionsGroupsGetRequest;

	/**
	 * @var QuestionPostRequest
	 */
	private $questionPostRequest;

	/**
	 * @var QuestionResponse
	 */
	private $questionResponse;

	/**
	 * @var QuestionPutRequest
	 */
	private $questionPutRequest;

	/**
	 * @var QuestionGetRequest
	 */
	private $questionGetRequest;

	/**
	 * @var QuestionOptionDeleteRequest
	 */
	private $questionOptionDeleteRequest;

	/**
	 * @var QuestionCountriesGetRequest
	 */
	private $questionCountriesGetRequest;

	/**
	 * @var QuestionCountriesPutRequest
	 */
	private $questionCountriesPutRequest;

	/**
	 * @var IRequest
	 */
	private $request;

	/**
	 * @var QuestionOptionGetRequest
	 */
	private $questionOptionGetRequest;

	/**
	 * @var QuestionOptionPutRequest
	 */
	private $questionOptionPutRequest;

	/**
	 * @var QuestionOptionTranslationGetRequest
	 */
	private $questionOptionTranslationGetRequest;

	/**
	 * @var QuestionOptionTranslationPutRequest
	 */
	private $questionOptionTranslationPutRequest;

	/**
	 * @var QuestionHostsGetRequest
	 */
	private $questionHostsGetRequest;

	/**
	 * @var QuestionHostsPutRequest
	 */
	private $questionHostsPutRequest;

	/**
	 * @var QuestionRegistrationsGetRequest
	 */
	private $questionRegistrationsGetRequest;

	/**
	 * @var QuestionRegistrationsPutRequest
	 */
	private $questionRegistrationsPutRequest;

	/**
	 * @var QuestionOptionsGetRequest
	 */
	private $optionListRequest;


	/**
	 * QuestionForm constructor.
	 * @param ITranslator $translator
	 * @param IRequest $request
	 * @param LanguagesGetRequest $languagesGetRequest
	 * @param QuestionsGroupsGetRequest $questionsGroupsGetRequest
	 * @param QuestionOptionDeleteRequest $questionOptionDeleteRequest
	 * @param QuestionPostRequest $questionPostRequest
	 * @param QuestionPutRequest $questionPutRequest
	 * @param QuestionGetRequest $questionGetRequest
	 * @param QuestionCountriesGetRequest $questionCountriesGetRequest
	 * @param QuestionCountriesPutRequest $questionCountriesPutRequest
	 * @param QuestionOptionGetRequest $questionOptionGetRequest
	 * @param QuestionOptionPutRequest $questionOptionPutRequest
	 * @param QuestionOptionTranslationGetRequest $questionOptionTranslationGetRequest
	 * @param QuestionOptionTranslationPutRequest $questionOptionTranslationPutRequest
	 * @param QuestionHostsGetRequest $questionHostsGetRequest
	 * @param QuestionHostsPutRequest $questionHostsPutRequest
	 * @param QuestionRegistrationsGetRequest $questionRegistrationsGetRequest
	 * @param QuestionRegistrationsPutRequest $questionRegistrationsPutRequest
	 * @param QuestionOptionsGetRequest $optionListRequest
	 * @throws ApiException
	 */
	public function __construct(
		ITranslator $translator,
		IRequest $request,
		LanguagesGetRequest $languagesGetRequest,
		QuestionsGroupsGetRequest $questionsGroupsGetRequest,
		QuestionOptionDeleteRequest $questionOptionDeleteRequest,
		QuestionPostRequest $questionPostRequest,
		QuestionPutRequest $questionPutRequest,
		QuestionGetRequest $questionGetRequest,
		QuestionCountriesGetRequest $questionCountriesGetRequest,
		QuestionCountriesPutRequest $questionCountriesPutRequest,
		QuestionOptionGetRequest $questionOptionGetRequest,
		QuestionOptionPutRequest $questionOptionPutRequest,
		QuestionOptionTranslationGetRequest $questionOptionTranslationGetRequest,
		QuestionOptionTranslationPutRequest $questionOptionTranslationPutRequest,
		QuestionHostsGetRequest $questionHostsGetRequest,
		QuestionHostsPutRequest $questionHostsPutRequest,
		QuestionRegistrationsGetRequest $questionRegistrationsGetRequest,
		QuestionRegistrationsPutRequest $questionRegistrationsPutRequest,
		QuestionOptionsGetRequest $optionListRequest
	)
	{
		$this->request = $request;
		$this->translator = $translator;
		$this->languages = $languagesGetRequest->execute()->getLanguages();
		$this->questionsGroupsGetRequest = $questionsGroupsGetRequest;
		$this->questionPostRequest = $questionPostRequest;
		$this->questionPutRequest = $questionPutRequest;
		$this->questionGetRequest = $questionGetRequest;
		$this->questionOptionDeleteRequest = $questionOptionDeleteRequest;
		$this->questionCountriesGetRequest = $questionCountriesGetRequest;
		$this->questionCountriesPutRequest = $questionCountriesPutRequest;
		$this->questionOptionGetRequest = $questionOptionGetRequest;
		$this->optionListRequest = $optionListRequest;


		$this->monitor(IPresenter::class, function () {
			$this->questionCountriesPutRequest->setId((int) $this->getPresenter()->getParameter('questionId'));
			$this->questionCountriesGetRequest->setId((int) $this->getPresenter()->getParameter('questionId'));
			$this->optionListRequest->setQuestionId((int) $this->getPresenter()->getParameter('questionId'));

			$this['countryQuestionModalForm']->setGetRequest($this->questionCountriesGetRequest);
			$this['countryQuestionModalForm']->setPutRequest($this->questionCountriesPutRequest);

			$this['optionQuestionModalForm']->setQuestionId((int) $this->getPresenter()->getParameter('questionId'));

			$this['parentQuestionModalForm']->setQuestionId((int) $this->getPresenter()->getParameter('questionId'));
		});

		$this->questionOptionPutRequest = $questionOptionPutRequest;
		$this->questionOptionTranslationGetRequest = $questionOptionTranslationGetRequest;
		$this->questionOptionTranslationPutRequest = $questionOptionTranslationPutRequest;
		$this->questionHostsGetRequest = $questionHostsGetRequest;
		$this->questionHostsPutRequest = $questionHostsPutRequest;
		$this->questionRegistrationsGetRequest = $questionRegistrationsGetRequest;
		$this->questionRegistrationsPutRequest = $questionRegistrationsPutRequest;
	}


	public function render(): void
	{
		$this->template->_form = $this['completeForm'];

		if (isset($this->template->questionResponse) === false) {
			$this->template->questionResponse = $this->questionResponse;
		}

		$this->template->languages = $this->languages;

		if ($this->questionResponse !== null && $this->questionResponse->getParent() !== null && isset($this->template->parentQuestion) === false) {
			$this->template->parentQuestion = $this->questionResponse->getParent();
		}

		if ($this->questionResponse !== null) {
			$this->template->question = $this->questionResponse;

			$this->questionCountriesGetRequest->setId($this->questionResponse->getId());
			$this->template->countries = $this->questionCountriesGetRequest->execute()->getItems();

			$this->questionHostsGetRequest->setQuestionId($this->questionResponse->getId());
			$this->template->hosts = $this->questionHostsGetRequest->execute()->getItems();

			$this->questionRegistrationsGetRequest->setQuestionId($this->questionResponse->getId());
			$this->template->registrations = $this->questionRegistrationsGetRequest->execute()->getItems();

			if ($this->questionResponse->getType() === 'choice') {
				$this->template->options = $this['apiPagination']->getItems()->getItems();
			}
		}

		if ($this->questionResponse === null) {
			$this->template->render(__DIR__ . '/templates/mandatory.latte');
		} else {
			$this->template->render(__DIR__ . '/templates/complete.latte');
		}
	}


	private function buildBasicForm(): BaseForm
	{
		$form = new BaseForm;
		$form->setTranslator($this->translator);

		$questionGroups = $this->questionsGroupsGetRequest->execute()->getQuestionGroups();

		$questionGroupsForm = [];

		foreach ($questionGroups as $questionGroup) {
			$questionGroupsForm[$questionGroup->getId()] = $questionGroup->getName();
		}

		$form->addHidden('id');

		$form->addSelect('group', 'back.questions.form.label.group', $questionGroupsForm)
			->setRequired('back.questions.form.validation.required.group');

		$form->addSelect('type', 'back.questions.form.label.type', [
			'text' => 'back.questions.form.label.type.text',
			'number' => 'back.questions.form.label.type.number',
			'bool' => 'back.questions.form.label.type.bool',
			'choice' => 'back.questions.form.label.type.choice',
			'datetime' => 'back.questions.form.label.type.datetime',
			'country' => 'back.questions.form.label.type.country',
		]);

		$form->addText('question', 'back.questions.form.label.question')
			->setRequired('back.questions.form.validation.required.question')
			->addRule($form::PATTERN, 'back.questions.form.field.question.validation.pattern', '[a-z0-9_]*');

		$form->addSubmit('send', 'back.questions.form.label.send');

		return $form;
	}


	protected function createComponentForm(): BaseForm
	{
		$form = $this->buildBasicForm();

		$form->onSuccess[] = function (BaseForm $form) {

			$values = $form->getValues();

			$this->questionPostRequest->setData([
				'text' => $values->question,
				'type' => $values->type,
				'group' => (int) $values->group,
				'required' => false,
			]);

			try {
				$response = $this->questionPostRequest->execute();

				$this->flashMessage('back.questions.form.create.success', 'success');
				$this->onSuccess($response);
			} catch (BadRequestException $e) {
				$this->flashMessage($this->translator->translate('back.' . $e->getMessage(), $e->getParams()), 'danger');
			} catch (ApiException $e) {
				$this->flashMessage('back.questions.form.error.unknown', 'danger');
			}

			if ($this->isAjax()) {
				$this->redrawControl('flashes');
			}
		};

		return $form;
	}


	protected function createComponentCompleteForm(): BaseForm
	{
		$form = $this->buildBasicForm();

		$rules = $form->addContainer('validationRules');
		$rules->addCheckbox('required', 'back.questions.form.field.required');
		$rules->addCheckbox('multichoice', 'back.questions.form.field.multichoice');
		$rules->addText('minimalLength', 'back.questions.form.field.minimal-length');
		$rules->addText('maximalLength', 'back.questions.form.field.maximal-length');
		$rules->addText('minimalValue', 'back.questions.form.field.minimal-value');
		$rules->addText('maximalValue', 'back.questions.form.field.maximal-value');
		$rules->addText('regularExpression', 'back.questions.form.field.regular-expression');
		$rules->addText('parentRuleExpression', 'back.questions.form.field.parent-rule-expression');

		$attributes = $form->addContainer('questionAttributes');
		$attributes->addText('help', 'back.questions.form.help');
		$attributes->addText('placeholder', 'back.questions.form.placeholder');
		$attributes->addText('validationHelp', 'back.questions.form.validation-help');
		$attributes->addCheckbox('isPrefilled', 'back.question.form.label.prefilled');
		$attributes->addSelect('variant', 'back.question.form.label.variant', [
			'date' => 'back.question.form.option.variant.date',
			'time' => 'back.question.form.option.variant.time',
			'datetime' => 'back.question.form.option.variant.datetime',
		]);



		$form->onSuccess[] = function (BaseForm $form) {
			$values = $form->getValues();

			try {
				$this->questionGetRequest->setQuestionId((int) $values->id);
				$question = $this->questionGetRequest->execute();

				$this->questionPutRequest->setQuestionId((int) $values->id);
				$this->questionPutRequest->setData([
					'text' => $values->question,
					'type' => $values->type,
					'group' => (int) $values->group,
					'required' => $values->validationRules->required,
					'multiChoice' => $values->validationRules->multichoice,
					'validation' => $values->validationRules->regularExpression,
					'minLength' => (int) $values->validationRules->minimalLength,
					'maxLength' => (int) $values->validationRules->maximalLength,
					'minValue' => (int) $values->validationRules->minimalValue,
					'maxValue' => (int) $values->validationRules->maximalValue,
					'parentRule' => $values->validationRules->parentRuleExpression,
					'parent' => $question->getParent() !== null ? $question->getParent()->getId() : null,
					'help' => $values->questionAttributes->help,
					'placeholder' => $values->questionAttributes->placeholder,
					'validationHelp' => $values->questionAttributes->validationHelp,
					'isPrefilled' => $values->questionAttributes->isPrefilled,
					'variant' => $values->type === 'datetime'
						? $values->questionAttributes->variant
						: null,
				]);

				$this->template->questionResponse = $this->questionPutRequest->execute();

				$this->flashMessage('back.questions.form.update.success', 'success');
			} catch (BadRequestException $e) {
				$this->flashMessage($this->translator->translate('back.' . $e->getMessage(), $e->getParams()), 'danger');
			} catch (ApiException $e) {
				$this->flashMessage('back.questions.form.error.unknown', 'danger');
			}

			if ($this->isAjax()) {
				$this->redrawControl('flashes');
			}
		};

		return $form;
	}


	protected function createComponentParentQuestionModalForm(IQuestionParentFormModelFactory $factory): QuestionParentFormModal
	{
		$control = $factory->create();

		$control->onSuccess[] = function (int $questionParentId) {
			$this->questionGetRequest->setQuestionId($questionParentId);
			$this->template->parentQuestion = $this->questionGetRequest->execute();
			$this->template->_form = $this['completeForm'];
			$this->redrawControl('parentQuestion');
		};

		return $control;
	}


	protected function createComponentOptionQuestionModalForm(IQuestionOptionFormModalFactory $factory): QuestionOptionFormModal
	{
		$control = $factory->create();

		$control->onSuccess[] = function () {
			$this->questionGetRequest->setQuestionId($this->questionResponse->getId());
			$this->template->questionResponse = $this->questionGetRequest->execute();

			$this->redrawControl('options');
		};

		return $control;
	}


	protected function createComponentCountryQuestionModalForm(ISetCountryFormModalFactory $factory): SetCountryFormModal
	{
		$control = $factory->create();
		$control->setGetRequest($this->questionCountriesGetRequest);
		$control->setPutRequest($this->questionCountriesPutRequest);

		return $control;
	}


	protected function createComponentTranslationOptionModalForm(ISetTranslationFormModal $factory): SetTranslationFormModal
	{
		$control = $factory->create();
		$control->setGetRequest($this->questionOptionTranslationGetRequest);
		$control->setPutRequest($this->questionOptionTranslationPutRequest);

		return $control;
	}


	protected function createComponentHostQuestionModalForm(ISetHostFormModalFactory $factory): SetHostFormModal
	{
		$control = $factory->create();
		$control->setPutRequest($this->questionHostsPutRequest);
		$control->setGetRequest($this->questionHostsGetRequest);

		return $control;
	}

	protected function createComponentRegistrationQuestionModalForm(ISetRegistrationFormModalFactory $factory): SetRegistrationFormModal
	{
		$control = $factory->create();
		$control->setPutRequest($this->questionRegistrationsPutRequest);
		$control->setGetRequest($this->questionRegistrationsGetRequest);

		return $control;
	}

	protected function createComponentApiPagination(IApiPaginationControlFactory $factory): ApiPaginationControl
	{
		$control = $factory->create();
		$control->setRequest($this->optionListRequest);

		return $control;
	}


	public function setQuestionResponse(QuestionResponse $questionResponse): void
	{
		$this->questionResponse = $questionResponse;

		$this['form']->setDefaults([
			'id' => $questionResponse->getId(),
			'type' => $questionResponse->getType(),
			'group' => $questionResponse->getGroup()->getId(),
			'question' => $questionResponse->getQuestion(),
		]);

		$this['completeForm']->setDefaults([
			'id' => $questionResponse->getId(),
			'type' => $questionResponse->getType(),
			'group' => $questionResponse->getGroup()->getId(),
			'question' => $questionResponse->getQuestion(),
		]);

		$this['completeForm']['validationRules']->setDefaults([
			'required' => $questionResponse->getRequired(),
			'multichoice' => $questionResponse->getChoice() instanceof ChoiceQuestionResponse ? $questionResponse->getChoice()
				->isMultiChoice() : false,
			'minimalLength' => $questionResponse->getText() instanceof TextQuestionResponse ? $questionResponse->getText()
				->getMinLength() : null,
			'maximalLength' => $questionResponse->getText() instanceof TextQuestionResponse ? $questionResponse->getText()
				->getMaxLength() : null,
			'minimalValue' => $questionResponse->getNumber() instanceof NumberQuestionResponse ? $questionResponse->getNumber()
				->getMinValue() : null,
			'maximalValue' => $questionResponse->getNumber() instanceof NumberQuestionResponse ? $questionResponse->getNumber()
				->getMaxValue() : null,
			'regularExpression' => $questionResponse->getValidation(),
			'parentRule' => $questionResponse->getParentRule(),
		]);

		$this['completeForm']['questionAttributes']->setDefaults([
			'help' => $questionResponse->getHelp(),
			'placeholder' => $questionResponse->getPlaceholder(),
			'validationHelp' => $questionResponse->getValidationHelp(),
			'isPrefilled' => $questionResponse->isPrefilled(),
			'variant' => $questionResponse->getDatetime() !== null
				? $questionResponse->getDatetime()->getVariant()
				: null,
		]);

	}


	public function handleDeleteParent(): void
	{
		$this->questionPutRequest->setQuestionId($this->questionResponse->getId());
		$this->questionPutRequest->setData([
			'text' => $this->questionResponse->getQuestion(),
			'type' => $this->questionResponse->getType(),
			'group' => $this->questionResponse->getGroup()->getId(),
			'required' => $this->questionResponse->getRequired(),
			'parent' => null,
			'parentRule' => null,
		]);
		$this->questionPutRequest->execute();

		$this->questionGetRequest->setQuestionId($this->questionResponse->getId());
		$this->questionResponse = $this->questionGetRequest->execute();

		unset($this->template->parentQuestion);
		$this->redrawControl('parentQuestion');
	}


	public function handleDeleteOption(int $id): void
	{
		$this->questionOptionDeleteRequest->setOptionId($id);
		$this->questionOptionDeleteRequest->execute();

		$this->questionGetRequest->setQuestionId($this->questionResponse->getId());
		$this->template->questionResponse = $this->questionGetRequest->execute();

		$this->redrawControl('options');
	}


	public function handleSaveOptionPosition(int $optionId): void
	{
		try {
			$order = max(0, (int) $this->request->getPost('position'));

			$this->questionOptionGetRequest->setId($optionId);
			$option = $this->questionOptionGetRequest->execute();

			$this->questionOptionPutRequest->setId($optionId);
			$this->questionOptionPutRequest->setData([
				'text' => $option->getText(),
				'order' => $order,
			]);
			$this->questionOptionPutRequest->execute();

			$this->questionGetRequest->setQuestionId($this->questionResponse->getId());
			$this->template->questionResponse = $this->questionGetRequest->execute();

			$this->redrawControl("flashes");
			$this->redrawControl('options');

		} catch (BadRequestException $e) {
			$this->flashMessage($this->translator->translate('back.' . $e->getMessage(), $e->getParams()), 'danger');
			$this->redrawControl("flashes");
		} catch (\Exception $e) {
			$this->flashMessage($this->translator->translate('back.questions.options.error'), 'danger');
			$this->redrawControl("flashes");
		}
	}
}

interface IQuestionFormFactory
{

	/**
	 * @return QuestionForm
	 */
	function create();
}
