<?php

declare(strict_types=1);

namespace Cleevio\Questions\Components\FilterForm;

use Cleevio\Countries\API\Requests\CountriesGetRequest;
use Cleevio\Questions\API\Responses\QuestionResponse;
use Cleevio\UI\ApiFilterForm\FilterForm as BaseFilterForm;
use Cleevio\UI\ApiFilterForm\IFilterInput;
use Cleevio\UI\ApiFilterForm\Inputs\RemoteInput;
use Cleevio\UI\ApiFilterForm\Inputs\SelectInput;
use Cleevio\UI\ApiFilterForm\Inputs\TextInput;
use Nette\Localization\ITranslator;

class FilterForm extends BaseFilterForm
{
	/**
	 * @var CountriesGetRequest
	 */
	private $countriesGetRequest;


	/**
	 * @param ITranslator         $translator
	 * @param CountriesGetRequest $countriesGetRequest
	 */
	public function __construct(
		ITranslator $translator,
		CountriesGetRequest $countriesGetRequest
	)
	{
		parent::__construct($translator);

		$this->countriesGetRequest = $countriesGetRequest;
	}


	/**
	 * @return IFilterInput[]
	 */
	protected function getInputs(): array
	{
		return [
			new RemoteInput('back.questions.filter.country', 'country', $this->countriesGetRequest, 4),
			new SelectInput("back.questions.filter.state", "state", [
				QuestionResponse::QUESTION_VISIBILITY_ENABLED => "back.questions.list.visibility.enabled",
				QuestionResponse::QUESTION_VISIBILITY_DISABLED => "back.questions.list.visibility.disabled",
				QuestionResponse::QUESTION_VISIBILITY_PREVIEW => "back.questions.list.visibility.preview",
			], 4),
			new TextInput('back.questions.filter.search', 'search', 4),
		];
	}
}

interface IFilterFormFactory
{

	/**
	 * @return FilterForm
	 */
	function create();
}
