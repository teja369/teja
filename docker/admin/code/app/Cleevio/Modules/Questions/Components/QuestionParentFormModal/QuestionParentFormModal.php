<?php

declare(strict_types=1);

namespace Cleevio\Questions\Components\QuestionParentFormModal;

use Cleevio\ApiClient\Exception\ApiException;
use Cleevio\Questions\API\Requests\QuestionGetRequest;
use Cleevio\Questions\API\Requests\QuestionPutRequest;
use Cleevio\Questions\API\Requests\QuestionsGetRequest;
use Cleevio\Questions\API\Responses\QuestionResponse;
use Cleevio\UI\BaseControlModal;
use Nette\Localization\ITranslator;

class QuestionParentFormModal extends BaseControlModal
{

	/**
	 * @var int
	 */
	private $questionId;

	/**
	 * @var QuestionsGetRequest
	 */
	private $questionsGetRequest;

	/**
	 * @var QuestionGetRequest
	 */
	private $questionGetRequest;

	/**
	 * @var QuestionPutRequest
	 */
	private $questionPutRequest;

	/**
	 * @var array
	 */
	public $onSuccess = [];
	/**
	 * @var ITranslator
	 */
	private $translator;


	public function __construct(
		ITranslator $translator,
		QuestionsGetRequest $questionsGetRequest,
		QuestionGetRequest $questionGetRequest,
		QuestionPutRequest $questionPutRequest
	)
	{
		parent::__construct();

		$this->setTemplateDirectory(__DIR__ . '/templates');

		$this->questionsGetRequest = $questionsGetRequest;
		$this->questionGetRequest = $questionGetRequest;
		$this->questionPutRequest = $questionPutRequest;
		$this->translator = $translator;
	}


	public function handleShowModal(): void
	{
		$this->questionGetRequest->setQuestionId($this->questionId);
		$currentQuestion = $this->questionGetRequest->execute();
		$parentId = $currentQuestion->getParent() !== null
			? $currentQuestion->getParent()->getId()
			: null;

		$this->questionsGetRequest->setGroupId($currentQuestion->getGroup()->getId());

		$this->template->selected = false;
		$this->template->questions = array_filter($this->questionsGetRequest->execute()->getItems(), function (QuestionResponse $question) use ($parentId) {
			return $question->getId() !== $this->questionId && $question->getId() !== $parentId;
		});

		$this->setView('default');
	}


	public function handleSelectQuestion(int $parentQuestionId): void
	{
		try {
			$this->questionGetRequest->setQuestionId($this->questionId);
			$question = $this->questionGetRequest->execute();

			$this->questionPutRequest->setQuestionId($this->questionId);
			$this->questionPutRequest->setData([
				'text' => $question->getQuestion(),
				'type' => $question->getType(),
				'group' => $question->getGroup()->getId(),
				'required' => $question->getRequired(),
				'parent' => $parentQuestionId,
			]);
			$this->questionPutRequest->execute();
			$this->template->selected = true;

			$this->onSuccess($parentQuestionId);
		} catch (ApiException $e) {

			$this->questionGetRequest->setQuestionId($this->questionId);
			$question = $this->questionGetRequest->execute();

			$this->questionsGetRequest->setGroupId($question->getGroup()->getId());

			$this->template->selected = false;
			$this->template->questions = array_filter($this->questionsGetRequest->execute()->getItems(), function (QuestionResponse $question) {
				return $question->getId() !== $this->questionId;
			});

			$this->flashMessage($this->translator->translate('back.' . $e->getMessage(), $e->getParams()), 'danger');
			$this->redrawControl();
		}
	}


	public function setQuestionId(int $questionId): void
	{
		$this->questionId = $questionId;
	}
}

interface IQuestionParentFormModelFactory
{

	/**
	 * @return QuestionParentFormModal
	 */
	public function create(): QuestionParentFormModal;
}
