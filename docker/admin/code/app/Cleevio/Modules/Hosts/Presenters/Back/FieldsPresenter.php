<?php

declare(strict_types=1);

namespace Cleevio\Hosts\BackModule\Presenters;

use Cleevio\ApiClient\Exception\ApiException;
use Cleevio\ApiClient\Exception\ServerError;
use Cleevio\Countries\Components\SetCountryFormModal\ISetCountryFormModalFactory;
use Cleevio\Countries\Components\SetCountryFormModal\SetCountryFormModal;
use Cleevio\Fields\API\Requests\FieldCountriesGetRequest;
use Cleevio\Fields\API\Requests\FieldCountriesPutRequest;
use Cleevio\Fields\API\Requests\FieldCountryDeleteRequest;
use Cleevio\Fields\API\Requests\FieldDeleteRequest;
use Cleevio\Fields\API\Requests\FieldGetRequest;
use Cleevio\Fields\API\Requests\FieldsGetRequest;
use Cleevio\Fields\API\Requests\FieldTranslationGetRequest;
use Cleevio\Fields\API\Requests\FieldTranslationPutRequest;
use Cleevio\Hosts\Components\FieldForm\FieldForm;
use Cleevio\Hosts\Components\FieldForm\IFieldFormFactory;
use Cleevio\Translations\Components\SetTranslationFormModal\ISetTranslationFormModal;
use Cleevio\Translations\Components\SetTranslationFormModal\SetTranslationFormModal;
use Cleevio\UI\SearchForm\ISearchFormFactory;
use Cleevio\UI\SearchForm\SearchForm;
use Nette\Application\AbortException;
use Nette\Application\BadRequestException;

class FieldsPresenter extends BasePresenter
{

	/**
	 * @var FieldsGetRequest
	 */
	private $fieldsGetRequest;

	/**
	 * @var FieldDeleteRequest
	 */
	private $fieldDeleteRequest;
	/**
	 * @var FieldGetRequest
	 */
	private $fieldGetRequest;

	/**
	 * @var FieldCountriesGetRequest
	 */
	private $fieldCountriesGetRequest;

	/**
	 * @var FieldCountryDeleteRequest
	 */
	private $fieldCountryDeleteRequest;

	/**
	 * @var FieldCountriesPutRequest
	 */
	private $fieldCountriesPutRequest;

	/**
	 * @var FieldTranslationGetRequest
	 */
	private $fieldTranslationGetRequest;

	/**
	 * @var FieldTranslationPutRequest
	 */
	private $fieldTranslationPutRequest;

	/**
	 * @param FieldsGetRequest $fieldsGetRequest
	 * @param FieldGetRequest $fieldGetRequest
	 * @param FieldDeleteRequest $fieldDeleteRequest
	 * @param FieldCountriesGetRequest $fieldCountriesGetRequest
	 * @param FieldCountriesPutRequest $fieldCountriesPutRequest
	 * @param FieldCountryDeleteRequest $fieldCountryDeleteRequest
	 * @param FieldTranslationGetRequest $fieldTranslationGetRequest
	 * @param FieldTranslationPutRequest $fieldTranslationPutRequest
	 */
	public function __construct(
		FieldsGetRequest $fieldsGetRequest,
		FieldGetRequest $fieldGetRequest,
		FieldDeleteRequest $fieldDeleteRequest,
		FieldCountriesGetRequest $fieldCountriesGetRequest,
		FieldCountriesPutRequest $fieldCountriesPutRequest,
		FieldCountryDeleteRequest $fieldCountryDeleteRequest,
		FieldTranslationGetRequest $fieldTranslationGetRequest,
		FieldTranslationPutRequest $fieldTranslationPutRequest
	)
	{
		parent::__construct();

		$this->fieldsGetRequest = $fieldsGetRequest;
		$this->fieldDeleteRequest = $fieldDeleteRequest;
		$this->fieldGetRequest = $fieldGetRequest;
		$this->fieldCountriesGetRequest = $fieldCountriesGetRequest;
		$this->fieldCountryDeleteRequest = $fieldCountryDeleteRequest;
		$this->fieldCountriesPutRequest = $fieldCountriesPutRequest;
		$this->fieldTranslationGetRequest = $fieldTranslationGetRequest;
		$this->fieldTranslationPutRequest = $fieldTranslationPutRequest;
	}

	public function startup()
	{
		parent::startup();
	}

	/**
	 * @param string|null $search
	 * @throws ServerError
	 * @throws BadRequestException
	 */
	public function actionDefault(?string $search = null): void
	{
		try {
			$this->fieldsGetRequest->setSearch($search);
			$this->template->fields = $this->fieldsGetRequest->execute()->getFields();

			$this['searchForm']['form']->setDefaults([
				'search' => $search,
			]);

			$this['searchForm']->onSuccess[] = function ($search) {
				$this->redirect(':Hosts:Back:Fields:default', $search);
			};

		} catch (ApiException $ex) {
			$this->error($this->translator->translate('back.' . $ex->getMessage()), $ex->getCode());
		}
	}

	public function actionCreate(): void
	{
		$this['fieldForm']->onSuccess[] = function () {
			$this->flashMessage($this->translator->translate('back.hosts.fields.create.success'), 'success');
			$this->redirect(':Hosts:Back:Fields:default');
		};

		$this['fieldForm']->onError[] = function () {
			$this->flashMessage($this->translator->translate('back.hosts.fields.create.error'), 'danger');
			$this->redirect(':Hosts:Back:Fields:default');
		};
	}

	/**
	 * @param int $id
	 * @throws BadRequestException
	 */
	public function actionEdit(int $id): void
	{
		try {
			$this->fieldGetRequest->setFieldId($id);
			$field = $this->fieldGetRequest->execute();
			$this->template->field = $field;

			$this->fieldCountriesGetRequest->setId($id);
			$this->template->countries = $this->fieldCountriesGetRequest->execute()->getItems();

			$this->fieldCountriesPutRequest->setId($id);
			$this['fieldCountryModalForm']->setGetRequest($this->fieldCountriesGetRequest);
			$this['fieldCountryModalForm']->setPutRequest($this->fieldCountriesPutRequest);

			$this['fieldForm']['form']->setDefaults([
				'name' => $field->getName(),
				'type' => $field->getType(),
				'placeholder' => $field->getPlaceholder(),
				'required' => $field->isRequired(),
				'validation' => $field->getValidation(),
				'id' => $field->getId(),
			]);

			$this['fieldForm']->onSuccess[] = function () {
				$this->flashMessage($this->translator->translate('back.hosts.fields.edit.success'), 'success');
				$this->redirect(':Hosts:Back:Fields:default');
			};

			$this['fieldForm']->onError[] = function () {
				$this->flashMessage($this->translator->translate('back.hosts.fields.edit.error'), 'danger');
				$this->redirect(':Hosts:Back:Fields:default');
			};
		} catch (ApiException $ex) {
			$this->error($this->translator->translate('back.' . $ex->getMessage()), $ex->getCode());
		}
	}

	/**
	 * @param int $id
	 * @throws AbortException
	 * @throws BadRequestException
	 */
	public function handleDelete(int $id): void
	{
		try {
			$this->fieldDeleteRequest->setFieldId($id);
			$this->fieldDeleteRequest->execute();

			$this->flashMessage($this->translator->translate('back.hosts.fields.delete.success'), 'success');

			$this->template->fields = $this->fieldsGetRequest->execute()->getFields();

			if ($this->isAjax()) {
				$this->redrawControl('flashes');
				$this->redrawControl('fields');
			} else {
				$this->redirect('this');
			}
		} catch (ApiException $ex) {
			$this->error($this->translator->translate('back.' . $ex->getMessage()), $ex->getCode());
		}
	}

	/**
	 * @param int $id
	 * @param string $code
	 * @throws AbortException
	 * @throws ApiException
	 */
	public function handleDeleteCountry(int $id, string $code): void
	{
		$this->fieldCountryDeleteRequest->setFieldId($id);
		$this->fieldCountryDeleteRequest->setCountryCode($code);
		$this->fieldCountryDeleteRequest->execute();

		$this->flashMessage($this->translator->translate('back.hosts.fields.countries.delete.success'), 'success');
		$this->redirect('this');
	}

	/**
	 * @param ISearchFormFactory $searchFormFactory
	 * @return SearchForm
	 */
	protected function createComponentSearchForm(ISearchFormFactory $searchFormFactory): SearchForm
	{
		return $searchFormFactory->create();
	}

	/**
	 * @param IFieldFormFactory $fieldFormFactory
	 * @return FieldForm
	 */
	protected function createComponentFieldForm(IFieldFormFactory $fieldFormFactory): FieldForm
	{
		return $fieldFormFactory->create();
	}

	protected function createComponentFieldCountryModalForm(ISetCountryFormModalFactory $factory): SetCountryFormModal
	{
		$control = $factory->create();

		$control->onSuccess[] = function () {

			$this->template->countries = $this->fieldCountriesGetRequest->execute()->getItems();

			$this->redrawControl('fieldCountries');
		};

		return $control;
	}


	protected function createComponentTranslationOptionModalForm(ISetTranslationFormModal $factory): SetTranslationFormModal
	{
		$control = $factory->create();
		$control->setGetRequest($this->fieldTranslationGetRequest);
		$control->setPutRequest($this->fieldTranslationPutRequest);

		return $control;
	}

}
