<?php

declare(strict_types=1);

namespace Cleevio\Hosts\BackModule\Presenters;

use Cleevio\ApiClient\Exception\ApiException;
use Cleevio\Fields\API\Requests\AddressFieldDeleteRequest;
use Cleevio\Fields\API\Requests\AddressFieldPutRequest;
use Cleevio\Fields\API\Requests\AddressFieldsGetRequest;
use Cleevio\Hosts\Components\SetCountryFormModal\ISetAddressFieldFormModal;
use Cleevio\Hosts\Components\SetCountryFormModal\SetAddressFieldFormModal;
use Nette\Application\AbortException;
use Nette\Application\BadRequestException;

class  AddressPresenter extends BasePresenter
{

	/**
	 * @var AddressFieldsGetRequest
	 */
	private $addressFieldsGetRequest;

	/**
	 * @var AddressFieldDeleteRequest
	 */
	private $addressFieldDeleteRequest;

	/**
	 * @var AddressFieldPutRequest
	 */
	private $addressFieldPutRequest;


	/**
	 * AddressPresenter constructor.
	 * @param AddressFieldsGetRequest $addressFieldsGetRequest
	 * @param AddressFieldDeleteRequest $addressFieldDeleteRequest
	 * @param AddressFieldPutRequest $addressFieldPutRequest
	 */
	public function __construct(
		AddressFieldsGetRequest $addressFieldsGetRequest,
		AddressFieldDeleteRequest $addressFieldDeleteRequest,
		AddressFieldPutRequest $addressFieldPutRequest
	)
	{
		parent::__construct();
		$this->addressFieldsGetRequest = $addressFieldsGetRequest;
		$this->addressFieldDeleteRequest = $addressFieldDeleteRequest;
		$this->addressFieldPutRequest = $addressFieldPutRequest;
	}


	public function startup()
	{
		parent::startup();
	}


	/**
	 * @throws BadRequestException
	 */
	public function actionDefault(): void
	{
		try {
			$addressFields = $this->addressFieldsGetRequest->execute();
			$this->template->addressFields = $addressFields->getFields();
		} catch (ApiException $ex) {
			$this->error($this->translator->translate('back.' . $ex->getMessage()), $ex->getCode());
		}
	}


	/**
	 * @param int $id
	 * @throws AbortException
	 * @throws BadRequestException
	 */
	public function handleDelete(int $id): void
	{
		try {
			$this->addressFieldDeleteRequest->setId($id);
			$this->addressFieldDeleteRequest->execute();

			$this->template->addressFields = $this->addressFieldsGetRequest->execute()->getFields();

			if ($this->isAjax()) {
				$this->redrawControl('address');
				$this->redrawControl('fields');
			} else {
				$this->redirect('this');
			}
		} catch (ApiException $ex) {
			$this->error($this->translator->translate('back.' . $ex->getMessage()), $ex->getCode());
		}
	}


	/**
	 * @param int $fieldId
	 * @param int|null $position
	 * @throws BadRequestException
	 */
	public function handleSaveFieldPosition(int $fieldId, ?int $position = null): void
	{
		try {
			$this->addressFieldPutRequest->setId($fieldId);
			$this->addressFieldPutRequest->setData(['order' => $position]);
			$this->addressFieldPutRequest->execute();

			$addressFields = $this->addressFieldsGetRequest->execute();
			$this->template->addressFields = $addressFields->getFields();

			$this->redrawControl('address');
			$this->redrawControl("flashes");
		} catch (ApiException $ex) {
			$this->error($this->translator->translate('back.' . $ex->getMessage()), $ex->getCode());
		}
	}


	protected function createComponentSetAddressFieldFormModal(ISetAddressFieldFormModal $factory
	): SetAddressFieldFormModal
	{
		return $factory->create();
	}
}
