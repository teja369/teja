<?php

declare(strict_types=1);

namespace Cleevio\Hosts\BackModule\Presenters;

use Cleevio\ApiClient\Exception\ApiException;
use Cleevio\ApiClient\Exception\BadRequestException;
use Cleevio\Fields\API\Requests\HostDeleteRequest;
use Cleevio\Fields\API\Requests\HostGetRequest;
use Cleevio\Fields\API\Requests\HostParamsDeleteRequest;
use Cleevio\Fields\API\Requests\HostParamsGetRequest;
use Cleevio\Fields\API\Requests\HostsCsvRequest;
use Cleevio\Fields\API\Requests\HostsGetRequest;
use Cleevio\Hosts\Components\HostForm\HostForm;
use Cleevio\Hosts\Components\HostForm\IHostFormFactory;
use Cleevio\Hosts\Components\ParamFormModal\IParamFormModalFactory;
use Cleevio\Hosts\Components\ParamFormModal\ParamFormModal;
use Cleevio\UI\ApiPaginationControl\ApiPaginationControl;
use Cleevio\UI\ApiPaginationControl\IApiPaginationControlFactory;
use Cleevio\UI\SearchForm\ISearchFormFactory;
use Cleevio\UI\SearchForm\SearchForm;
use Nette\Application\AbortException;

class  HostsPresenter extends BasePresenter
{

	/**
	 * @var HostsGetRequest
	 */
	private $hostsGetRequest;

	/**
	 * @var HostDeleteRequest
	 */
	private $hostDeleteRequest;

	/**
	 * @var HostGetRequest
	 */
	private $hostGetRequest;

	/**
	 * @var HostParamsGetRequest
	 */
	private $hostParamsGetRequest;

	/**
	 * @var HostParamsDeleteRequest
	 */
	private $hostParamsDeleteRequest;

	/**
	 * @var HostsCsvRequest
	 */
	private $hostsCsvRequest;

	/**
	 * @param HostsGetRequest $hostsGetRequest
	 * @param HostDeleteRequest $hostDeleteRequest
	 * @param HostGetRequest $hostGetRequest
	 * @param HostParamsGetRequest $hostParamsGetRequest
	 * @param HostParamsDeleteRequest $hostParamsDeleteRequest
	 * @param HostsCsvRequest $hostsCsvRequest
	 */
	public function __construct(
		HostsGetRequest $hostsGetRequest,
		HostDeleteRequest $hostDeleteRequest,
		HostGetRequest $hostGetRequest,
		HostParamsGetRequest $hostParamsGetRequest,
		HostParamsDeleteRequest $hostParamsDeleteRequest,
		HostsCsvRequest $hostsCsvRequest
	)
	{
		parent::__construct();

		$this->hostsGetRequest = $hostsGetRequest;
		$this->hostDeleteRequest = $hostDeleteRequest;
		$this->hostGetRequest = $hostGetRequest;
		$this->hostParamsGetRequest = $hostParamsGetRequest;
		$this->hostParamsDeleteRequest = $hostParamsDeleteRequest;
		$this->hostsCsvRequest = $hostsCsvRequest;
	}

	public function startup()
	{
		parent::startup();
	}

	/**
	 * @param string|null $search
	 */
	public function actionDefault(?string $search = null): void
	{
		$this->hostsGetRequest->setType('client');
		$this->hostsGetRequest->setSearch($search);

		$this->template->hosts = $this['apiPagination']->getItems()->getItems();

		$this['searchForm']['form']->setDefaults([
			'search' => $search,
		]);

		$this['searchForm']->onSuccess[] = function ($search) {
			$this->redirect(':Hosts:Back:Hosts:default', $search);
		};
	}

	public function actionCreate(): void
	{
		$this['hostForm']->onSuccess[] = function () {
			$this->flashMessage($this->translator->translate('back.hosts.create.success'), 'success');
			$this->redirect(':Hosts:Back:Hosts:default');
		};

		$this['hostForm']->onError[] = function () {
			$this->flashMessage($this->translator->translate('back.hosts.create.error'), 'danger');
			$this->redirect(':Hosts:Back:Hosts:default');
		};
	}

	public function actionCsv(): void
	{
		try {
			$this->sendResponse($this->hostsCsvRequest->execute());
		} catch (BadRequestException $e) {
			$this->flashMessage($this->translator->translate('back.' . $e->getMessage(), $e->getParams()), 'danger');
		} catch (ApiException $e) {
			$this->flashMessage('back.error.unknown', 'danger');
		}

		$this->redirect(':Hosts:Back:Hosts:default');
	}

	/**
	 * @param int $id
	 * @throws \Nette\Application\BadRequestException
	 */
	public function actionEdit(int $id): void
	{
		try {
			$this->hostGetRequest->setHostId($id);
			$host = $this->hostGetRequest->execute();
			$this->template->host = $host;

			$this->hostParamsGetRequest->setHostId($id);
			$this->template->hostParams = $this->hostParamsGetRequest->execute()->getParams();

			$this['hostForm']['form']->setDefaults([
				'name' => $host->getName(),
				'country' => $host->getCountry()->getCode(),
				'type' => $host->getType(),
				'id' => $host->getId(),
			]);

			$this['hostForm']->onSuccess[] = function () {

				$this->flashMessage($this->translator->translate('back.hosts.edit.success'), 'success');

				$this->redirect(':Hosts:Back:Hosts:default');
			};

			$this['hostForm']->onError[] = function () {

				$this->flashMessage($this->translator->translate('back.hosts.edit.error'), 'danger');

				$this->redirect(':Hosts:Back:Hosts:default');
			};

			$this['paramModalForm']->setHostId($id);
		} catch (ApiException $ex) {
			$this->error($this->translator->translate('back.' . $ex->getMessage()), $ex->getCode());
		}
	}

	/**
	 * @param int $id
	 * @throws AbortException
	 * @throws \Nette\Application\BadRequestException
	 */
	public function handleDelete(int $id): void
	{
		try {
			$this->hostDeleteRequest->setHostId($id);
			$this->hostDeleteRequest->execute();

			$this->template->hosts = $this['apiPagination']->getItems()->getItems();

			$this->flashMessage($this->translator->translate('back.hosts.delete.success'), 'success');

			if ($this->isAjax()) {
				$this->redrawControl('flashes');
				$this->redrawControl('hosts');
			} else {
				$this->redirect('this');
			}
		} catch (ApiException $ex) {
			$this->error($this->translator->translate('back.' . $ex->getMessage()), $ex->getCode());
		}
	}

	/**
	 * @param int $id
	 * @param int $field
	 * @throws AbortException
	 * @throws \Nette\Application\BadRequestException
	 */
	public function handleDeleteParam(int $id, int $field): void
	{
		try {
			$this->hostParamsDeleteRequest->setHostId($id);
			$this->hostParamsDeleteRequest->setFieldId($field);
			$host = $this->hostParamsDeleteRequest->execute();

			if ($host->getId() !== $id) {
				if ($this->isAjax()) {
					$this->payload->forceRedirect = true;
					$this->redirect(':Hosts:Back:Hosts:edit', $host->getId() . "");
				} else {
					$this->redirect(':Hosts:Back:Hosts:edit', $host->getId() . "");
				}
			}

			$this->flashMessage($this->translator->translate('back.hosts.params.delete.success'), 'success');
			$this->redirect('this');
		} catch (ApiException $ex) {
			$this->error($this->translator->translate('back.' . $ex->getMessage()), $ex->getCode());
		}
	}

	/**
	 * @param ISearchFormFactory $searchFormFactory
	 * @return SearchForm
	 */
	protected function createComponentSearchForm(ISearchFormFactory $searchFormFactory): SearchForm
	{
		return $searchFormFactory->create();
	}

	/**
	 * @param IHostFormFactory $hostFormFactory
	 * @return HostForm
	 */
	protected function createComponentHostForm(IHostFormFactory $hostFormFactory): HostForm
	{
		return $hostFormFactory->create();
	}

	/**
	 * @param IParamFormModalFactory $factory
	 * @return ParamFormModal
	 */
	protected function createComponentParamModalForm(IParamFormModalFactory $factory): ParamFormModal
	{
		$control = $factory->create();

		$control->onSuccess[] = function (?int $hostId) {
			if ($hostId !== null) {
				if ($this->isAjax()) {
					$this->payload->forceRedirect = true;
					$this->redirect(':Hosts:Back:Hosts:edit', $hostId . "");
				} else {
					$this->redirect(':Hosts:Back:Hosts:edit', $hostId . "");
				}
			} else {
				$this->template->hostParams = $this->hostParamsGetRequest->execute()->getParams();

				$this->redrawControl('hostParams');
			}
		};

		return $control;
	}

	/**
	 * @param IApiPaginationControlFactory $factory
	 * @return ApiPaginationControl
	 */
	protected function createComponentApiPagination(IApiPaginationControlFactory $factory): ApiPaginationControl
	{
		$control = $factory->create();
		$control->setRequest($this->hostsGetRequest);

		return $control;
	}
}
