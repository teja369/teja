<?php

declare(strict_types=1);

namespace Cleevio\Hosts\API\Responses;

use Cleevio\ApiClient\IResponse;
use Cleevio\Countries\API\Responses\CountriesResponse;
use InvalidArgumentException;

class FieldResponse implements IResponse
{

	/**
	 * @var int
	 */
	private $id;

	/**
	 * @var string
	 */
	private $name;

	/**
	 * @var string
	 */
	private $type;

	/**
	 * @var string|null
	 */
	private $placeholder;

	/**
	 * @var bool
	 */
	private $required;

	/**
	 * @var string|null
	 */
	private $validation;

	/**
	 * @var CountriesResponse
	 */
	private $countries;

	/**
	 * FieldResponse constructor.
	 * @param array $field
	 */
	public function __construct(array $field)
	{
		if (array_key_exists('id', $field) &&
			array_key_exists('name', $field) &&
			array_key_exists('type', $field) &&
			array_key_exists('required', $field) &&
			array_key_exists('validation', $field) &&
			array_key_exists('countries', $field)) {
			$this->id = $field['id'];
			$this->name = $field['name'];
			$this->type = $field['type'];
			$this->required = (bool) $field['required'];
			$this->validation = $field['validation'];
			$this->placeholder = $field['placeholder'];
			$this->countries = new CountriesResponse($field['countries']);
		} else {
			throw new InvalidArgumentException;
		}
	}


	/**
	 * @return int
	 */
	public function getId(): int
	{
		return $this->id;
	}


	/**
	 * @return string
	 */
	public function getName(): string
	{
		return $this->name;
	}


	/**
	 * @return string
	 */
	public function getType(): string
	{
		return $this->type;
	}


	/**
	 * @return string|null
	 */
	public function getPlaceholder(): ?string
	{
		return $this->placeholder;
	}


	/**
	 * @return bool
	 */
	public function isRequired(): bool
	{
		return $this->required;
	}

	/**
	 * @return string|null
	 */
	public function getValidation(): ?string
	{
		return $this->validation;
	}

	/**
	 * @return CountriesResponse
	 */
	public function getCountries(): CountriesResponse
	{
		return $this->countries;
	}
}
