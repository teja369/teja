<?php

declare(strict_types=1);

namespace Cleevio\Hosts\API\Responses;

use Cleevio\ApiClient\DTO\PaginationResponse;

class HostsResponse extends PaginationResponse
{

	/**
	 * @var HostResponse[]
	 */
	private $items = [];


	/**
	 * HostsResponse constructor.
	 * @param array $request
	 */
	public function __construct(array $request)
	{
		parent::__construct($request);

		$this->items = array_map(static function ($host) {
			return new HostResponse($host);
		}, $request['items']);
	}


	/**
	 * @return HostResponse[]
	 */
	public function getItems(): array
	{
		return $this->items;
	}

}
