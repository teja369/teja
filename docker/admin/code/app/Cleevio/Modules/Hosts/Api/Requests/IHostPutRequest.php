<?php

declare(strict_types=1);

namespace Cleevio\Hosts\API\Requests;

interface IHostPutRequest
{
	public function setHostId(int $id): void;
}
