<?php

declare(strict_types=1);

namespace Cleevio\Fields\API\Requests;

use Cleevio\Api\BaseRequest;
use Cleevio\ApiClient\Exception\ApiException;
use Cleevio\Hosts\API\Requests\IHostParamGetRequest;
use Cleevio\Hosts\API\Responses\ParamResponse;
use InvalidArgumentException;

class HostParamGetRequest extends BaseRequest implements IHostParamGetRequest
{
	/**
	 * @var int
	 */
	private $hostId;

	/**
	 * @var int
	 */
	private $fieldId;

	/**
	 * Endpoint path
	 * @return string
	 */
	protected function path(): string
	{
		if ($this->hostId === null || $this->fieldId === null) {
			throw new InvalidArgumentException;
		}

		return sprintf('/hosts/%s/param/%s', $this->hostId, $this->fieldId);
	}

	/**
	 * Method
	 * @return string
	 */
	protected function method(): string
	{
		return 'GET';
	}

	/**
	 * Determines whether request requires authorization
	 * @return bool
	 */
	function requiresAuth(): bool
	{
		return true;
	}

	protected function dispatchEvents()
	{
		return false;
	}

	/**
	 * Execute request and return a response
	 * @return ParamResponse
	 * @throws ApiException
	 */
	function execute(): ParamResponse
	{
		return new ParamResponse($this->request());
	}

	public function setHostId(int $id): void
	{
		$this->hostId = $id;
	}

	public function setFieldId(int $id): void
	{
		$this->fieldId = $id;
	}
}
