<?php

declare(strict_types=1);

namespace Cleevio\Fields\API\Requests;

use Cleevio\ApiClient\Exception\ApiException;
use Cleevio\Hosts\API\Requests\IHostsGetRequest;
use Cleevio\Hosts\API\Responses\HostsResponse;
use Cleevio\UI\SetFormModal\DataGetRequest;

class HostsGetRequest extends DataGetRequest implements IHostsGetRequest
{

	/**
	 * @var string|null $type
	 */
	private $type = null;

	/**
	 * @var string|null
	 */
	private $search = null;

	/**
	 * Endpoint path
	 * @return string
	 */
	protected function path(): string
	{
		$path = '/hosts';

		$params = [
			'limit' => $this->getLimit(),
			'page' => $this->getPage(),
			'type' => $this->type,
			'search' => $this->search,
		];

		$query = http_build_query($params);

		return strlen($query) > 0
			? $path . '?' . $query
			: $path;
	}

	/**
	 * Method
	 * @return string
	 */
	protected function method(): string
	{
		return 'GET';
	}

	/**
	 * Determines whether request requires authorization
	 * @return bool
	 */
	function requiresAuth(): bool
	{
		return true;
	}

	/**
	 * Execute request and return a response
	 * @return HostsResponse
	 * @throws ApiException
	 */
	function execute(): HostsResponse
	{
		return new HostsResponse($this->request());
	}

	public function setType(string $type): void
	{
		$this->type = $type;
	}

	public function setSearch(?string $search): void
	{
		$this->search = $search;
	}
}
