<?php

declare(strict_types=1);

namespace Cleevio\Fields\API\Requests;

use Cleevio\Api\BaseRequest;
use Cleevio\ApiClient\Exception\ApiException;
use Cleevio\Hosts\API\Requests\IHostsCsvRequest;
use Cleevio\Hosts\API\Responses\HostsCsvResponse;

class HostsCsvRequest extends BaseRequest implements IHostsCsvRequest
{

	/**
	 * Endpoint path
	 * @return string
	 */
	protected function path(): string
	{
		return '/hosts/csv';
	}

	/**
	 * Method
	 * @return string
	 */
	protected function method(): string
	{
		return 'GET';
	}

	/**
	 * Determines whether request requires authorization
	 * @return bool
	 */
	function requiresAuth(): bool
	{
		return true;
	}

	/**
	 * Execute request and return a response
	 * @return HostsCsvResponse
	 * @throws ApiException
	 */
	function execute(): HostsCsvResponse
	{
		return new HostsCsvResponse($this->request()[0]);
	}
}
