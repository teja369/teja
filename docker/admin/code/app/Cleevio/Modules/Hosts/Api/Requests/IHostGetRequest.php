<?php

declare(strict_types=1);

namespace Cleevio\Hosts\API\Requests;

interface IHostGetRequest
{
	public function setHostId(int $id): void;
}
