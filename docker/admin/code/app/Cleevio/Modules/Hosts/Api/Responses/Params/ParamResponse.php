<?php

declare(strict_types=1);

namespace Cleevio\Hosts\API\Responses;

use Cleevio\ApiClient\IResponse;
use InvalidArgumentException;

class ParamResponse implements IResponse
{

	/**
	 * @var int
	 */
	private $id;

	/**
	 * @var string
	 */
	private $value;

	/**
	 * @var FieldResponse
	 */
	private $field;

	/**
	 * FieldResponse constructor.
	 * @param array $field
	 */
	public function __construct(array $field)
	{
		if (array_key_exists('id', $field) &&
			array_key_exists('value', $field)) {
			$this->id = $field['id'];
			$this->field = new FieldResponse($field['field']);
			$this->value = $field['value'];
		} else {
			throw new InvalidArgumentException;
		}
	}


	/**
	 * @return int
	 */
	public function getId(): int
	{
		return $this->id;
	}

	/**
	 * @return FieldResponse
	 */
	public function getField(): FieldResponse
	{
		return $this->field;
	}

	/**
	 * @return string
	 */
	public function getValue(): string
	{
		return $this->value;
	}

}
