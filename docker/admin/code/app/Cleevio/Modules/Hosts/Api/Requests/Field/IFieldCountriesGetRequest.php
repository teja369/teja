<?php

declare(strict_types=1);

namespace Cleevio\Hosts\API\Requests;

interface IFieldCountriesGetRequest
{
	public function setId(int $id): void;
}
