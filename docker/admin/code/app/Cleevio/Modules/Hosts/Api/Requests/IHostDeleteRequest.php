<?php

declare(strict_types=1);

namespace Cleevio\Hosts\API\Requests;

interface IHostDeleteRequest
{
	public function setHostId(int $id): void;
}
