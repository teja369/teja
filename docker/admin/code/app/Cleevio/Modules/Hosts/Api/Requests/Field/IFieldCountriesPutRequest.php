<?php

declare(strict_types=1);

namespace Cleevio\Hosts\API\Requests;

interface IFieldCountriesPutRequest
{
	public function setId(int $id): void;
}
