<?php

declare(strict_types=1);

namespace Cleevio\Hosts\API\Responses;

use Cleevio\ApiClient\IResponse;
use InvalidArgumentException;

class AddressFieldResponse implements IResponse
{

	/**
	 * @var int
	 */
	private $id;

	/**
	 * @var int
	 */
	private $order;

	/**
	 * @var FieldResponse
	 */
	private $field;


	/**
	 * FieldResponse constructor.
	 * @param array $field
	 */
	public function __construct(array $field)
	{
		if (array_key_exists('id', $field) &&
			array_key_exists('field', $field) &&
			array_key_exists('order', $field)) {
			$this->id = $field['id'];
			$this->field = new FieldResponse($field['field']);
			$this->order = $field['order'];
		} else {
			throw new InvalidArgumentException;
		}
	}


	/**
	 * @return int
	 */
	public function getId(): int
	{
		return $this->id;
	}


	/**
	 * @return int
	 */
	public function getOrder(): int
	{
		return $this->order;
	}


	/**
	 * @return FieldResponse
	 */
	public function getField(): FieldResponse
	{
		return $this->field;
	}

}
