<?php

declare(strict_types=1);

namespace Cleevio\Fields\API\Requests;

use Cleevio\Api\BaseRequest;
use Cleevio\ApiClient\Exception\ApiException;
use Cleevio\Countries\API\Responses\CountriesResponse;
use Cleevio\Hosts\API\Requests\IFieldCountriesPutRequest;
use Cleevio\UI\SetFormModal\IRelationSetRequest;
use InvalidArgumentException;

class FieldCountriesPutRequest extends BaseRequest implements IFieldCountriesPutRequest, IRelationSetRequest
{
	/**
	 * @var int
	 */
	private $fieldId;

	/**
	 * Endpoint path
	 * @return string
	 */
	protected function path(): string
	{
		if ($this->fieldId === null) {
			throw new InvalidArgumentException;
		}

		return sprintf('/fields/%s/country', $this->fieldId);
	}


	/**
	 * Method
	 * @return string
	 */
	protected function method(): string
	{
		return 'PUT';
	}

	/**
	 * Determines whether request requires authorization
	 * @return bool
	 */
	function requiresAuth(): bool
	{
		return true;
	}

	/**
	 * Execute request and return a response
	 * @return CountriesResponse
	 * @throws ApiException
	 */
	function execute(): CountriesResponse
	{
		return new CountriesResponse($this->request());
	}

	public function setId(int $id): void
	{
		$this->fieldId = $id;
	}
}
