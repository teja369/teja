<?php

declare(strict_types=1);

namespace Cleevio\Fields\API\Requests;

use Cleevio\Api\BaseRequest;
use Cleevio\ApiClient\Exception\ApiException;
use Cleevio\Fields\API\Responses\FieldTranslationResponse;
use Cleevio\Translations\Components\SetTranslationFormModal\IGetTranslationRequest;
use Cleevio\Translations\Components\SetTranslationFormModal\TranslatableResponse;
use InvalidArgumentException;

class FieldTranslationGetRequest extends BaseRequest implements IFieldTranslationGetRequest, IGetTranslationRequest
{

	/**
	 * @var int
	 */
	private $fieldId;

	/**
	 * @var string
	 */
	private $languageCode;


	/**
	 * Endpoint path
	 * @return string
	 */
	protected function path(): string
	{
		if ($this->fieldId === null || $this->languageCode === null) {
			throw new InvalidArgumentException;
		}

		return sprintf('/fields/%s/translation/%s', $this->fieldId, $this->languageCode);
	}


	/**
	 * Method
	 * @return string
	 */
	protected function method(): string
	{
		return 'GET';
	}


	/**
	 * Determines whether request requires authorization
	 * @return bool
	 */
	function requiresAuth(): bool
	{
		return true;
	}


	/**
	 * Execute request and return a response
	 * @return FieldTranslationResponse
	 * @throws ApiException
	 */
	function execute(): TranslatableResponse
	{
		return new FieldTranslationResponse($this->request());
	}


	public function setLanguageCode(string $languageCode): void
	{
		$this->languageCode = $languageCode;
	}


	public function setId(int $id): void
	{
		$this->fieldId = $id;
	}
}
