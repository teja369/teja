<?php

declare(strict_types=1);

namespace Cleevio\Hosts\API\Responses;

use Cleevio\ApiClient\IResponse;

class AddressFieldsResponse implements IResponse
{
	/**
	 * @var AddressFieldResponse[]
	 */
	private $fields = [];


	/**
	 * AddressFieldsResponse constructor.
	 * @param array $fields
	 */
	public function __construct(array $fields)
	{
		$this->fields = array_map(static function ($field) {
			return new AddressFieldResponse($field);
		}, $fields);
	}

	/**
	 * @return AddressFieldResponse[]
	 */
	public function getFields(): array
	{
		return $this->fields;
	}

}
