<?php

declare(strict_types=1);

namespace Cleevio\Fields\API\Requests;

use Cleevio\Api\BaseRequest;
use Cleevio\ApiClient\Exception\ApiException;
use Cleevio\Hosts\API\Requests\IFieldGetRequest;
use Cleevio\Hosts\API\Responses\FieldResponse;
use InvalidArgumentException;

class FieldGetRequest extends BaseRequest implements IFieldGetRequest
{
	/**
	 * @var int
	 */
	private $fieldId;

	/**
	 * Endpoint path
	 * @return string
	 */
	protected function path(): string
	{
		if ($this->fieldId === null) {
			throw new InvalidArgumentException;
		}

		return sprintf('/fields/%s', $this->fieldId);
	}


	/**
	 * Method
	 * @return string
	 */
	protected function method(): string
	{
		return 'GET';
	}

	/**
	 * Determines whether request requires authorization
	 * @return bool
	 */
	function requiresAuth(): bool
	{
		return true;
	}

	/**
	 * Execute request and return a response
	 * @return FieldResponse
	 * @throws ApiException
	 */
	function execute(): FieldResponse
	{
		return new FieldResponse($this->request());
	}

	public function setFieldId(int $id): void
	{
		$this->fieldId = $id;
	}
}
