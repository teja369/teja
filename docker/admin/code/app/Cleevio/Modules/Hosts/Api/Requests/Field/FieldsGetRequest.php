<?php

declare(strict_types=1);

namespace Cleevio\Fields\API\Requests;

use Cleevio\ApiClient\Exception\ApiException;
use Cleevio\Hosts\API\Requests\IFieldsGetRequest;
use Cleevio\Hosts\API\Responses\FieldsResponse;
use Cleevio\UI\SetFormModal\DataGetRequest;

class FieldsGetRequest extends DataGetRequest implements IFieldsGetRequest
{

	/**
	 * @var string|null
	 */
	private $search = null;

	/**
	 * Endpoint path
	 * @return string
	 */
	protected function path(): string
	{
		$path = '/fields';

		$params = [
			'search' => $this->search,
		];

		$query = http_build_query($params);

		return strlen($query) > 0
			? $path . '?' . $query
			: $path;
	}

	/**
	 * Method
	 * @return string
	 */
	protected function method(): string
	{
		return 'GET';
	}

	/**
	 * Determines whether request requires authorization
	 * @return bool
	 */
	function requiresAuth(): bool
	{
		return true;
	}

	/**
	 * Execute request and return a response
	 * @return FieldsResponse
	 * @throws ApiException
	 */
	function execute(): FieldsResponse
	{
		return new FieldsResponse($this->request());
	}

	public function setSearch(?string $search): void
	{
		$this->search = $search;
	}
}
