<?php

declare(strict_types=1);

namespace Cleevio\Hosts\API\Requests;

interface IFieldPutRequest
{
	public function setFieldId(int $id): void;
}
