<?php

declare(strict_types=1);

namespace Cleevio\Fields\API\Requests;

use Cleevio\Api\BaseRequest;
use Cleevio\ApiClient\Exception\ApiException;
use Cleevio\Hosts\API\Responses\AddressFieldResponse;
use InvalidArgumentException;

class AddressFieldPutRequest extends BaseRequest implements IAddressFieldPutRequest
{

	/**
	 * @var int
	 */
	private $id;


	/**
	 * Endpoint path
	 * @return string
	 */
	protected function path(): string
	{
		if ($this->id === null) {
			throw new InvalidArgumentException;
		}

		return sprintf('/fields/address/%s', $this->id);
	}

	/**
	 * Method
	 * @return string
	 */
	protected function method(): string
	{
		return 'PUT';
	}

	/**
	 * Determines whether request requires authorization
	 * @return bool
	 */
	function requiresAuth(): bool
	{
		return true;
	}

	public function setId(int $id): void
	{
		$this->id = $id;
	}


	/**
	 * @return AddressFieldResponse
	 * @throws ApiException
	 */
	function execute(): AddressFieldResponse
	{
		return new AddressFieldResponse($this->request());
	}

}
