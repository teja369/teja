<?php

declare(strict_types=1);

namespace Cleevio\Hosts\API\Requests;

interface IFieldCountryDeleteRequest
{
	public function setFieldId(int $id): void;

	public function setCountryCode(string $code): void;
}
