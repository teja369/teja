<?php

declare(strict_types=1);

namespace Cleevio\Fields\API\Requests;

use Cleevio\Api\BaseRequest;
use Cleevio\ApiClient\Exception\ApiException;
use Cleevio\Hosts\API\Requests\IFieldsPostRequest;
use Cleevio\Hosts\API\Responses\FieldResponse;

class FieldsPostRequest extends BaseRequest implements IFieldsPostRequest
{

	/**
	 * Endpoint path
	 * @return string
	 */
	protected function path(): string
	{
		return '/fields';
	}

	/**
	 * Method
	 * @return string
	 */
	protected function method(): string
	{
		return 'POST';
	}

	/**
	 * Determines whether request requires authorization
	 * @return bool
	 */
	function requiresAuth(): bool
	{
		return true;
	}

	/**
	 * Execute request and return a response
	 * @return FieldResponse
	 * @throws ApiException
	 */
	function execute(): FieldResponse
	{
		return new FieldResponse($this->request());
	}
}
