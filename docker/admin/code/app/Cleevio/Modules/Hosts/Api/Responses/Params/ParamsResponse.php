<?php

declare(strict_types=1);

namespace Cleevio\Hosts\API\Responses;

use Cleevio\ApiClient\IResponse;

class ParamsResponse implements IResponse
{
	/**
	 * @var ParamResponse[]
	 */
	private $params = [];

	/**
	 * ParamResponse constructor.
	 * @param array $params
	 */
	public function __construct(array $params)
	{
		$this->params = array_map(static function ($param) {
			return new ParamResponse($param);
		}, $params);
	}

	/**
	 * @return ParamResponse[]
	 */
	public function getParams(): array
	{
		return $this->params;
	}

}
