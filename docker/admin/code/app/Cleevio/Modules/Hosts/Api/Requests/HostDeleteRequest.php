<?php

declare(strict_types=1);

namespace Cleevio\Fields\API\Requests;

use Cleevio\Api\BaseRequest;
use Cleevio\ApiClient\DTO\EmptyResponse;
use Cleevio\ApiClient\Exception\ApiException;
use Cleevio\Hosts\API\Requests\IHostDeleteRequest;
use InvalidArgumentException;

class HostDeleteRequest extends BaseRequest implements IHostDeleteRequest
{
	/**
	 * @var int
	 */
	private $hostId;

	/**
	 * Endpoint path
	 * @return string
	 */
	protected function path(): string
	{
		if ($this->hostId === null) {
			throw new InvalidArgumentException;
		}

		return sprintf('/hosts/%s', $this->hostId);
	}

	/**
	 * Method
	 * @return string
	 */
	protected function method(): string
	{
		return 'DELETE';
	}

	/**
	 * Determines whether request requires authorization
	 * @return bool
	 */
	function requiresAuth(): bool
	{
		return true;
	}

	/**
	 * Execute request and return a response
	 * @return EmptyResponse
	 * @throws ApiException
	 */
	function execute(): EmptyResponse
	{
		$this->request();

		return new EmptyResponse;
	}

	public function setHostId(int $id): void
	{
		$this->hostId = $id;
	}
}
