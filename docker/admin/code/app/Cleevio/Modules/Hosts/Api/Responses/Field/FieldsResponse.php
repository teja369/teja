<?php

declare(strict_types=1);

namespace Cleevio\Hosts\API\Responses;

use Cleevio\ApiClient\IResponse;

class FieldsResponse implements IResponse
{
	/**
	 * @var FieldResponse[]
	 */
	private $fields = [];


	/**
	 * HostFieldsResponse constructor.
	 * @param array $fields
	 */
	public function __construct(array $fields)
	{
		$this->fields = array_map(static function ($field) {
			return new FieldResponse($field);
		}, $fields);
	}

	/**
	 * @return FieldResponse[]
	 */
	public function getFields(): array
	{
		return $this->fields;
	}

}
