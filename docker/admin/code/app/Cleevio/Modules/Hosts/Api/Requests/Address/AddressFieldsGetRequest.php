<?php

declare(strict_types=1);

namespace Cleevio\Fields\API\Requests;

use Cleevio\Api\BaseRequest;
use Cleevio\ApiClient\Exception\ApiException;
use Cleevio\Hosts\API\Responses\AddressFieldsResponse;

class AddressFieldsGetRequest extends BaseRequest implements IAddressFieldsGetRequest
{
	/**
	 * Endpoint path
	 * @return string
	 */
	protected function path(): string
	{
		return '/fields/address';
	}

	/**
	 * Method
	 * @return string
	 */
	protected function method(): string
	{
		return 'GET';
	}

	/**
	 * Determines whether request requires authorization
	 * @return bool
	 */
	function requiresAuth(): bool
	{
		return true;
	}


	/**
	 * @return AddressFieldsResponse
	 * @throws ApiException
	 */
	function execute(): AddressFieldsResponse
	{
		return new AddressFieldsResponse($this->request());
	}

}
