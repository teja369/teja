<?php

declare(strict_types=1);

namespace Cleevio\Hosts\API\Responses;

use Cleevio\Countries\API\Responses\CountryResponse;
use Cleevio\UI\SetFormModal\IRelationResponse;
use InvalidArgumentException;

class HostResponse implements IRelationResponse
{

	/**
	 * @var int
	 */
	private $id;

	/**
	 * @var string
	 */
	private $name;

	/**
	 * @var string
	 */
	private $type;
	
	/**
	 * @var CountryResponse
	 */
	private $country;

	/**
	 * FieldResponse constructor.
	 * @param array $field
	 */
	public function __construct(array $field)
	{
		if (array_key_exists('id', $field) &&
			array_key_exists('name', $field) &&
			array_key_exists('type', $field) &&
			array_key_exists('country', $field)) {
			$this->id = $field['id'];
			$this->name = $field['name'];
			$this->type = $field['type'];
			$this->country = new CountryResponse($field['country']);
		} else {
			throw new InvalidArgumentException;
		}
	}


	/**
	 * @return int
	 */
	public function getId(): int
	{
		return $this->id;
	}


	/**
	 * @return string
	 */
	public function getName(): string
	{
		return $this->name;
	}


	/**
	 * @return string
	 */
	public function getType(): string
	{
		return $this->type;
	}

	/**
	 * @return CountryResponse
	 */
	public function getCountry(): CountryResponse
	{
		return $this->country;
	}

}
