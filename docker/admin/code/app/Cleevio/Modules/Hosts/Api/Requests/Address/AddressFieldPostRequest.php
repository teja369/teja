<?php

declare(strict_types=1);

namespace Cleevio\Fields\API\Requests;

use Cleevio\Api\BaseRequest;
use Cleevio\ApiClient\Exception\ApiException;
use Cleevio\Hosts\API\Responses\AddressFieldResponse;

class AddressFieldPostRequest extends BaseRequest implements IAddressFieldPostRequest
{
	/**
	 * Endpoint path
	 * @return string
	 */
	protected function path(): string
	{
		return '/fields/address';
	}

	/**
	 * Method
	 * @return string
	 */
	protected function method(): string
	{
		return 'POST';
	}

	/**
	 * Determines whether request requires authorization
	 * @return bool
	 */
	function requiresAuth(): bool
	{
		return true;
	}


	/**
	 * @return AddressFieldResponse
	 * @throws ApiException
	 */
	function execute(): AddressFieldResponse
	{
		return new AddressFieldResponse($this->request());
	}

}
