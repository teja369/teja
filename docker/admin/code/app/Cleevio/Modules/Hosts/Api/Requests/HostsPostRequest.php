<?php

declare(strict_types=1);

namespace Cleevio\Fields\API\Requests;

use Cleevio\Api\BaseRequest;
use Cleevio\ApiClient\Exception\ApiException;
use Cleevio\Hosts\API\Requests\IHostsPostRequest;
use Cleevio\Hosts\API\Responses\HostResponse;

class HostsPostRequest extends BaseRequest implements IHostsPostRequest
{

	/**
	 * @var string|null $type
	 */
	private $type = null;

	/**
	 * Endpoint path
	 * @return string
	 */
	protected function path(): string
	{
		$path = '/hosts';

		if ($this->type !== null) {
			$path = sprintf($path . "?type=" . $this->type);
		}

		return $path;
	}

	/**
	 * Method
	 * @return string
	 */
	protected function method(): string
	{
		return 'POST';
	}

	/**
	 * Determines whether request requires authorization
	 * @return bool
	 */
	function requiresAuth(): bool
	{
		return true;
	}

	/**
	 * Execute request and return a response
	 * @return HostResponse
	 * @throws ApiException
	 */
	function execute(): HostResponse
	{
		return new HostResponse($this->request());
	}

	public function setType(string $type): void
	{
		$this->type = $type;
	}
}
