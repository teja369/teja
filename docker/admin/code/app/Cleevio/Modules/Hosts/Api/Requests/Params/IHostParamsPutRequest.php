<?php

declare(strict_types=1);

namespace Cleevio\Hosts\API\Requests;

interface IHostParamsPutRequest
{
	public function setHostId(int $id): void;

	public function setFieldId(int $id): void;
}
