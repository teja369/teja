<?php

declare(strict_types=1);

namespace Cleevio\Fields\API\Requests;

use Cleevio\Api\BaseRequest;
use Cleevio\ApiClient\Exception\ApiException;
use Cleevio\Hosts\API\Requests\IHostParamsPutRequest;
use Cleevio\Hosts\API\Responses\HostResponse;
use InvalidArgumentException;

class HostParamsPutRequest extends BaseRequest implements IHostParamsPutRequest
{
	/**
	 * @var int
	 */
	private $hostId;

	/**
	 * @var int
	 */
	private $fieldId;

	/**
	 * Endpoint path
	 * @return string
	 */
	protected function path(): string
	{
		if ($this->hostId === null || $this->fieldId === null) {
			throw new InvalidArgumentException;
		}

		return sprintf('/hosts/%s/param/%s', $this->hostId, $this->fieldId);
	}

	/**
	 * Method
	 * @return string
	 */
	protected function method(): string
	{
		return 'PUT';
	}

	/**
	 * Determines whether request requires authorization
	 * @return bool
	 */
	function requiresAuth(): bool
	{
		return true;
	}

	/**
	 * Execute request and return a response
	 * @return HostResponse
	 * @throws ApiException
	 */
	function execute(): HostResponse
	{
		return new HostResponse($this->request());
	}

	public function setHostId(int $id): void
	{
		$this->hostId = $id;
	}

	public function setFieldId(int $id): void
	{
		$this->fieldId = $id;
	}
}
