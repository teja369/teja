<?php

declare(strict_types=1);

namespace Cleevio\Fields\API\Requests;

use Cleevio\Api\BaseRequest;
use Cleevio\ApiClient\DTO\EmptyResponse;
use Cleevio\ApiClient\Exception\ApiException;
use InvalidArgumentException;

class AddressFieldDeleteRequest extends BaseRequest implements IAddressFieldDeleteRequest
{

	/**
	 * @var int
	 */
	private $id;


	/**
	 * Endpoint path
	 * @return string
	 */
	protected function path(): string
	{
		if ($this->id === null) {
			throw new InvalidArgumentException;
		}

		return sprintf('/fields/address/%s', $this->id);
	}

	/**
	 * Method
	 * @return string
	 */
	protected function method(): string
	{
		return 'DELETE';
	}

	/**
	 * Determines whether request requires authorization
	 * @return bool
	 */
	function requiresAuth(): bool
	{
		return true;
	}

	public function setId(int $id): void
	{
		$this->id = $id;
	}


	/**
	 * @return EmptyResponse
	 * @throws ApiException
	 */
	function execute(): EmptyResponse
	{
		$this->request();

		return new EmptyResponse;
	}

}
