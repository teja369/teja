<?php

declare(strict_types=1);

namespace Cleevio\Fields\API\Responses;

use Cleevio\Translations\Components\SetTranslationFormModal\TranslatableResponse;
use Cleevio\Translations\Components\SetTranslationFormModal\TranslationMapping;
use Cleevio\Translations\Components\SetTranslationFormModal\TranslationMappingField;
use InvalidArgumentException;

class FieldTranslationResponse extends TranslatableResponse
{

	/**
	 * @var string|null
	 */
	private $name;

	/**
	 * @var string|null
	 */
	private $placeholder;


	/**
	 * @param array $translation
	 */
	public function __construct(array $translation)
	{
		if (array_key_exists('name', $translation) && array_key_exists('placeholder', $translation)) {
			$this->name = $translation['name'];
			$this->placeholder = $translation['placeholder'];
		} else {
			throw new InvalidArgumentException;
		}
	}

	public function getName(): ?string
	{
		return $this->name;
	}

	public function getPlaceholder(): ?string
	{
		return $this->placeholder;
	}


	public function getMapping(): TranslationMapping
	{
		$mapping = new TranslationMapping;
		$mapping->addField(new TranslationMappingField('name', 'back.hosts.fields.form.field.label.name', $this->getName()));
		$mapping->addField(new TranslationMappingField('placeholder', 'back.hosts.fields.form.field.label.placeholder', $this->getPlaceholder()));

		return $mapping;
	}
}
