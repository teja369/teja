<?php

declare(strict_types=1);

namespace Cleevio\Hosts\API\Requests;

use Cleevio\UI\SearchForm\ISearchRequest;

interface IHostsGetRequest extends ISearchRequest
{

}
