<?php

declare(strict_types=1);

namespace Cleevio\Hosts\API\Responses;

use Cleevio\API\CsvResponse;
use Cleevio\ApiClient\IResponse;

class HostsCsvResponse extends CsvResponse implements IResponse
{

	/**
	 * HostsResponse constructor.
	 * @param string $data
	 */
	public function __construct(string $data)
	{
		parent::__construct($data, 'genesis.csv');
	}

}
