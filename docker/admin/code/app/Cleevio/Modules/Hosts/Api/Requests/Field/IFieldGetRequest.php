<?php

declare(strict_types=1);

namespace Cleevio\Hosts\API\Requests;

interface IFieldGetRequest
{
	public function setFieldId(int $id): void;
}
