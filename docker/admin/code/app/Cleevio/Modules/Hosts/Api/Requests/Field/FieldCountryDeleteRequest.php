<?php

declare(strict_types=1);

namespace Cleevio\Fields\API\Requests;

use Cleevio\Api\BaseRequest;
use Cleevio\ApiClient\Exception\ApiException;
use Cleevio\Countries\API\Responses\CountriesResponse;
use Cleevio\Hosts\API\Requests\IFieldCountryDeleteRequest;
use InvalidArgumentException;

class FieldCountryDeleteRequest extends BaseRequest implements IFieldCountryDeleteRequest
{
	/**
	 * @var int
	 */
	private $fieldId;

	/**
	 * @var string
	 */
	private $countryCode;

	/**
	 * Endpoint path
	 * @return string
	 */
	protected function path(): string
	{
		if ($this->fieldId === null || $this->countryCode === null) {
			throw new InvalidArgumentException;
		}

		return sprintf('/fields/%s/country/%s', $this->fieldId, $this->countryCode);
	}


	/**
	 * Method
	 * @return string
	 */
	protected function method(): string
	{
		return 'DELETE';
	}

	/**
	 * Determines whether request requires authorization
	 * @return bool
	 */
	function requiresAuth(): bool
	{
		return true;
	}

	/**
	 * Execute request and return a response
	 * @return CountriesResponse
	 * @throws ApiException
	 */
	function execute(): CountriesResponse
	{
		return new CountriesResponse($this->request());
	}

	public function setFieldId(int $id): void
	{
		$this->fieldId = $id;
	}

	public function setCountryCode(string $code): void
	{
		$this->countryCode = $code;
	}
}
