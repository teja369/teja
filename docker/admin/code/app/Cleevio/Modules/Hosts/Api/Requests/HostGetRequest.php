<?php

declare(strict_types=1);

namespace Cleevio\Fields\API\Requests;

use Cleevio\Api\BaseRequest;
use Cleevio\ApiClient\Exception\ApiException;
use Cleevio\Hosts\API\Requests\IHostDeleteRequest;
use Cleevio\Hosts\API\Responses\HostResponse;
use InvalidArgumentException;

class HostGetRequest extends BaseRequest implements IHostDeleteRequest
{
	/**
	 * @var int
	 */
	private $hostId;

	/**
	 * Endpoint path
	 * @return string
	 */
	protected function path(): string
	{
		if ($this->hostId === null) {
			throw new InvalidArgumentException;
		}

		return sprintf('/hosts/%s', $this->hostId);
	}

	/**
	 * Method
	 * @return string
	 */
	protected function method(): string
	{
		return 'GET';
	}

	/**
	 * Determines whether request requires authorization
	 * @return bool
	 */
	function requiresAuth(): bool
	{
		return true;
	}

	/**
	 * Execute request and return a response
	 * @return HostResponse
	 * @throws ApiException
	 */
	function execute(): HostResponse
	{
		return new HostResponse($this->request());
	}

	public function setHostId(int $id): void
	{
		$this->hostId = $id;
	}
}
