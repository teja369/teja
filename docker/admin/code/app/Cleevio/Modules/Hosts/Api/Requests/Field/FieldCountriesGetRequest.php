<?php

declare(strict_types=1);

namespace Cleevio\Fields\API\Requests;

use Cleevio\Api\BaseRequest;
use Cleevio\ApiClient\Exception\ApiException;
use Cleevio\Hosts\API\Requests\IFieldCountriesGetRequest;
use Cleevio\Hosts\API\Responses\FieldCountriesResponse;
use Cleevio\UI\SetFormModal\IRelationGetRequest;
use Cleevio\UI\SetFormModal\IRelationsResponse;
use InvalidArgumentException;

class FieldCountriesGetRequest extends BaseRequest implements IFieldCountriesGetRequest, IRelationGetRequest
{
	/**
	 * @var int
	 */
	private $fieldId;

	/**
	 * Endpoint path
	 * @return string
	 */
	protected function path(): string
	{
		if ($this->fieldId === null) {
			throw new InvalidArgumentException;
		}

		return sprintf('/fields/%s/country', $this->fieldId);
	}


	/**
	 * Method
	 * @return string
	 */
	protected function method(): string
	{
		return 'GET';
	}

	/**
	 * Determines whether request requires authorization
	 * @return bool
	 */
	function requiresAuth(): bool
	{
		return true;
	}

	/**
	 * Execute request and return a response
	 * @return FieldCountriesResponse
	 * @throws ApiException
	 */
	function execute(): IRelationsResponse
	{
		return new FieldCountriesResponse($this->request());
	}

	public function setId(int $id): void
	{
		$this->fieldId = $id;
	}
}
