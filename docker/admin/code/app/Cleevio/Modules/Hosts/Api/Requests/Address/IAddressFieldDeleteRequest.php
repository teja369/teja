<?php

declare(strict_types=1);

namespace Cleevio\Fields\API\Requests;

interface IAddressFieldDeleteRequest
{

	/**
	 * @param int $id
	 */
	public function setId(int $id): void;
}
