<?php

declare(strict_types=1);

namespace Cleevio\Fields\API\Requests;

use Cleevio\Api\BaseRequest;
use Cleevio\ApiClient\DTO\EmptyResponse;
use Cleevio\ApiClient\Exception\ApiException;
use Cleevio\Hosts\API\Requests\IFieldDeleteRequest;
use InvalidArgumentException;

class FieldDeleteRequest extends BaseRequest implements IFieldDeleteRequest
{
	/**
	 * @var int
	 */
	private $fieldId;

	/**
	 * Endpoint path
	 * @return string
	 */
	protected function path(): string
	{
		if ($this->fieldId === null) {
			throw new InvalidArgumentException;
		}

		return sprintf('/fields/%s', $this->fieldId);
	}

	/**
	 * Method
	 * @return string
	 */
	protected function method(): string
	{
		return 'DELETE';
	}

	/**
	 * Determines whether request requires authorization
	 * @return bool
	 */
	function requiresAuth(): bool
	{
		return true;
	}

	/**
	 * Execute request and return a response
	 * @return EmptyResponse
	 * @throws ApiException
	 */
	function execute(): EmptyResponse
	{
		$this->request();

		return new EmptyResponse;
	}

	public function setFieldId(int $id): void
	{
		$this->fieldId = $id;
	}
}
