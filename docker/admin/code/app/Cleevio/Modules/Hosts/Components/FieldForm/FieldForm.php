<?php

declare(strict_types=1);

namespace Cleevio\Hosts\Components\FieldForm;

use Cleevio\ApiClient\Exception\ApiException;
use Cleevio\ApiClient\Exception\BadRequestException;
use Cleevio\Fields\API\Requests\FieldPutRequest;
use Cleevio\Fields\API\Requests\FieldsPostRequest;
use Cleevio\UI\BaseControl;
use Cleevio\UI\BaseForm;
use Nette\Localization\ITranslator;

class FieldForm extends BaseControl
{

	/**
	 * @var ITranslator
	 */
	private $translator;

	/**
	 * @var array
	 */
	public $onSuccess = [];

	/**
	 * @var array
	 */
	public $onError = [];

	/**
	 * @var FieldsPostRequest
	 */
	private $fieldsPostRequest;

	/**
	 * @var FieldPutRequest
	 */
	private $fieldPutRequest;

	/**
	 * @param ITranslator $translator
	 * @param FieldsPostRequest $fieldsPostRequest
	 * @param FieldPutRequest $fieldPutRequest
	 */
	public function __construct(
		ITranslator $translator,
		FieldsPostRequest $fieldsPostRequest,
		FieldPutRequest $fieldPutRequest
	)
	{
		$this->translator = $translator;
		$this->fieldsPostRequest = $fieldsPostRequest;
		$this->fieldPutRequest = $fieldPutRequest;
	}


	public function render(): void
	{
		$this->template->render(__DIR__ . '/templates/default.latte');
	}


	protected function createComponentForm(): BaseForm
	{
		$form = new BaseForm;
		$form->setTranslator($this->translator);

		$form->addHidden('id');

		$form->addText('name', 'back.hosts.fields.form.field.label.name')
			->setRequired('back.hosts.fields.form.field.name.validation.required')
			->addRule($form::PATTERN, 'back.hosts.fields.form.field.name.validation.pattern', '[a-z0-9_]*');

		$form->addText('placeholder', 'back.hosts.fields.form.field.label.placeholder');

		$form->addSelect('type', 'back.hosts.fields.form.field.label.type', [
			'text' => 'back.hosts.fields.form.field.label.type.text',
			'number' => 'back.hosts.fields.form.field.label.type.number',
		]);

		$form->addCheckbox('required', 'back.hosts.fields.form.field.label.required');

		$form->addText('validation', 'back.hosts.fields.form.field.label.validation');

		$form->addSubmit('send', 'back.hosts.fields.form.label.submit');

		$form->onSuccess[] = function (BaseForm $form) {

			$values = $form->getValues();

			try {
				if ($values->id === null || $values->id === "") {
					$this->fieldsPostRequest->setData((array) $values);
					$this->fieldsPostRequest->execute();
				} else {
					$this->fieldPutRequest->setFieldId((int) $values->id);
					$this->fieldPutRequest->setData((array) $values);
					$this->fieldPutRequest->execute();
				}

				$this->onSuccess();
			} catch (BadRequestException $e) {
				$this->flashMessage($this->translator->translate('back.' . $e->getMessage(), $e->getParams()), 'danger');
			} catch (ApiException $e) {
				$this->flashMessage('back.hosts.fields.form.error.unknown', 'danger');
			}

			if ($this->isAjax()) {
				$this->redrawControl('flashes');
			}
		};

		return $form;
	}

}

interface IFieldFormFactory
{

	/**
	 * @return FieldForm
	 */
	function create();
}
