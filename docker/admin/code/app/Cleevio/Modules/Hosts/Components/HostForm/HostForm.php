<?php

declare(strict_types=1);

namespace Cleevio\Hosts\Components\HostForm;

use Cleevio\ApiClient\Exception\ApiException;
use Cleevio\ApiClient\Exception\BadRequestException;
use Cleevio\Countries\API\Requests\CountriesGetRequest;
use Cleevio\Fields\API\Requests\HostPutRequest;
use Cleevio\Fields\API\Requests\HostsPostRequest;
use Cleevio\UI\BaseControl;
use Cleevio\UI\BaseForm;
use Nette\Localization\ITranslator;

class HostForm extends BaseControl
{

	/**
	 * @var ITranslator
	 */
	private $translator;

	/**
	 * @var array
	 */
	public $onSuccess = [];

	/**
	 * @var array
	 */
	public $onError = [];

	/**
	 * @var CountriesGetRequest
	 */
	private $countriesGetRequest;

	/**
	 * @var HostsPostRequest
	 */
	private $hostsPostRequest;

	/**
	 * @var HostPutRequest
	 */
	private $hostPutRequest;

	/**
	 * @param ITranslator $translator
	 * @param CountriesGetRequest $countriesGetRequest
	 * @param HostsPostRequest $hostsPostRequest
	 * @param HostPutRequest $hostPutRequest
	 */
	public function __construct(
		ITranslator $translator,
		CountriesGetRequest $countriesGetRequest,
		HostsPostRequest $hostsPostRequest,
		HostPutRequest $hostPutRequest
	)
	{
		$this->translator = $translator;
		$this->countriesGetRequest = $countriesGetRequest;
		$this->hostsPostRequest = $hostsPostRequest;
		$this->hostPutRequest = $hostPutRequest;
	}


	public function render(): void
	{
		$this->template->render(__DIR__ . '/templates/default.latte');
	}


	protected function createComponentForm(): BaseForm
	{
		$form = new BaseForm;
		$form->setTranslator($this->translator);

		$form->addHidden('id');

		$form->addText('name', 'back.hosts.form.field.label.name')
			->setRequired('back.hosts.form.field.name.validation.required');

		$form->addSelect('type', 'back.hosts.form.field.label.type', [
			'client' => 'back.hosts.form.field.label.type.client',
		]);

		$countries = [];

		$this->countriesGetRequest->setLimit(1000);
		$items = $this->countriesGetRequest->execute()->getItems();

		foreach ($items as $country) {
			$countries[$country->getCode()] = $country->getName();
		}

		$form->addSelect('country', 'back.hosts.form.field.label.country', $countries)
			->setRequired('back.hosts.form.field.country.validation.required');

		$form->addSubmit('send', 'back.hosts.form.field.label.submit');

		$form->onSuccess[] = function (BaseForm $form) {

			$values = $form->getValues();

			try {
				if ($values->id === null || $values->id === "") {
					$values->params = [];
					$this->hostsPostRequest->setData((array) $values);
					$this->hostsPostRequest->execute();
				} else {
					$this->hostPutRequest->setHostId((int) $values->id);
					$this->hostPutRequest->setData((array) $values);
					$this->hostPutRequest->execute();
				}

				$this->onSuccess();
			} catch (BadRequestException $e) {
				$this->flashMessage($this->translator->translate('back.' . $e->getMessage(), $e->getParams()), 'danger');
			} catch (ApiException $e) {
				$this->flashMessage('back.hosts.fields.form.error.unknown', 'danger');
			}

			if ($this->isAjax()) {
				$this->redrawControl('flashes');
			}
		};

		return $form;
	}

}

interface IHostFormFactory
{

	/**
	 * @return HostForm
	 */
	function create();
}
