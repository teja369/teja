<?php

declare(strict_types=1);

namespace Cleevio\Hosts\Components\SetCountryFormModal;

use Cleevio\Fields\API\Requests\HostsGetRequest;
use Cleevio\UI\SetRelationFormModal\SetRelationFormModal;
use Nette\Localization\ITranslator;

class SetHostFormModal extends SetRelationFormModal
{

	/**
	 * Host set modal
	 *
	 * @param ITranslator $translator
	 * @param HostsGetRequest $hostsGetRequest
	 */
	public function __construct(ITranslator $translator, HostsGetRequest $hostsGetRequest)
	{
		parent::__construct($translator, $hostsGetRequest);
	}

	protected function translationHeadline(): string
	{
		return "back.hosts.modal.headline";
	}

}

interface ISetHostFormModalFactory
{

	/**
	 * @return SetHostFormModal
	 */
	public function create(): SetHostFormModal;
}
