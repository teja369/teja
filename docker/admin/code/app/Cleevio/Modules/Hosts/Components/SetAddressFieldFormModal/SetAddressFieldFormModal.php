<?php

declare(strict_types=1);

namespace Cleevio\Hosts\Components\SetCountryFormModal;

use Cleevio\ApiClient\Exception\ApiException;
use Cleevio\Fields\API\Requests\AddressFieldDeleteRequest;
use Cleevio\Fields\API\Requests\AddressFieldPostRequest;
use Cleevio\Fields\API\Requests\AddressFieldsGetRequest;
use Cleevio\Fields\API\Requests\FieldsGetRequest;
use Cleevio\UI\BaseControlModal;
use Nette\Application\AbortException;

class SetAddressFieldFormModal extends BaseControlModal
{

	/**
	 * @var FieldsGetRequest
	 */
	private $fieldsGetRequest;

	/**
	 * @var AddressFieldsGetRequest
	 */
	private $addressFieldsGetRequest;

	/**
	 * @var AddressFieldPostRequest
	 */
	private $addressFieldPostRequest;

	/**
	 * @var AddressFieldDeleteRequest
	 */
	private $addressFieldDeleteRequest;


	/**
	 * SetAddressFieldFormModal constructor.
	 * @param FieldsGetRequest          $fieldsGetRequest
	 * @param AddressFieldsGetRequest   $addressFieldsGetRequest
	 * @param AddressFieldPostRequest   $addressFieldPostRequest
	 * @param AddressFieldDeleteRequest $addressFieldDeleteRequest
	 */
	public function __construct(
		FieldsGetRequest $fieldsGetRequest,
		AddressFieldsGetRequest $addressFieldsGetRequest,
		AddressFieldPostRequest $addressFieldPostRequest,
		AddressFieldDeleteRequest $addressFieldDeleteRequest
	)
	{
		parent::__construct();
		$this->fieldsGetRequest = $fieldsGetRequest;
		$this->addressFieldsGetRequest = $addressFieldsGetRequest;
		$this->addressFieldPostRequest = $addressFieldPostRequest;
		$this->addressFieldDeleteRequest = $addressFieldDeleteRequest;
	}


	/**
	 * @throws ApiException
	 */
	public function render(): void
	{
		$fieldIds = [];
		$addedFields = [];
		$response = $this->addressFieldsGetRequest->execute();
		$fields = $this->fieldsGetRequest->execute();

		foreach ($response->getFields() as $field) {
			$fieldIds[] = $field->getField()->getId();
			$addedFields[$field->getField()->getId()] = $field->getId();
		}

		$this->template->fields = $fields->getFields();
		$this->template->fieldIds = $fieldIds;
		$this->template->addedFields = $addedFields;
		$this->template->render(__DIR__ . '/templates/default.latte');
	}


	/**
	 * @param int $id
	 * @param int $order
	 * @throws ApiException
	 * @throws AbortException
	 */
	public function handleAdd(int $id, int $order): void
	{
		$this->addressFieldPostRequest->setData([
			'fieldId' => $id,
			'order' => $order,
		]);
		$this->addressFieldPostRequest->execute();

		$this->redirectPermanent('showModal!');
	}


	/**
	 * @param int $id
	 * @throws ApiException
	 * @throws AbortException
	 */
	public function handleDelete(int $id): void
	{
		$this->addressFieldDeleteRequest->setId($id);
		$this->addressFieldDeleteRequest->execute();

		$this->redirectPermanent('showModal!');
	}


	public function handleShowModal(): void
	{
		$this->setView('default');
	}

}

interface ISetAddressFieldFormModal
{

	/**
	 * @return SetAddressFieldFormModal
	 */
	public function create(): SetAddressFieldFormModal;
}
