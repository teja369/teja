<?php

declare(strict_types=1);

namespace Cleevio\Hosts\Components\ParamFormModal;

use Cleevio\ApiClient\Exception\ApiException;
use Cleevio\ApiClient\Exception\BadRequestException;
use Cleevio\Fields\API\Requests\FieldGetRequest;
use Cleevio\Fields\API\Requests\FieldsGetRequest;
use Cleevio\Fields\API\Requests\HostParamGetRequest;
use Cleevio\Fields\API\Requests\HostParamsPutRequest;
use Cleevio\UI\BaseControlModal;
use Cleevio\UI\BaseForm;
use Kdyby\Autowired\AutowireComponentFactories;
use Nette\Localization\ITranslator;

class ParamFormModal extends BaseControlModal
{

	use AutowireComponentFactories;

	/**
	 * @var array
	 */
	public $onSuccess = [];

	/**
	 * @var ITranslator
	 */
	private $translator;

	/**
	 * @var FieldsGetRequest
	 */
	private $fieldsGetRequest;

	/**
	 * @var HostParamsPutRequest
	 */
	private $hostParamsPutRequest;

	/**
	 * @var int
	 */
	private $hostId;

	/**
	 * @var int|null
	 */
	private $fieldId;
	/**
	 * @var HostParamGetRequest
	 */
	private $hostParamGetRequest;
	/**
	 * @var FieldGetRequest
	 */
	private $fieldGetRequest;

	/**
	 * @var string
	 */
	private $type;

	/**
	 * @param ITranslator $translator
	 * @param FieldGetRequest $fieldGetRequest
	 * @param FieldsGetRequest $fieldsGetRequest
	 * @param HostParamsPutRequest $hostParamsPutRequest
	 * @param HostParamGetRequest $hostParamGetRequest
	 */
	public function __construct(
		ITranslator $translator,
		FieldGetRequest $fieldGetRequest,
		FieldsGetRequest $fieldsGetRequest,
		HostParamsPutRequest $hostParamsPutRequest,
		HostParamGetRequest $hostParamGetRequest
	)
	{
		parent::__construct();

		$this->setTemplateDirectory(__DIR__ . '/templates');

		$this->translator = $translator;
		$this->fieldsGetRequest = $fieldsGetRequest;
		$this->hostParamsPutRequest = $hostParamsPutRequest;
		$this->hostParamGetRequest = $hostParamGetRequest;
		$this->fieldGetRequest = $fieldGetRequest;
	}

	public function handleChangeInput(?int $field = null): void
	{
		$this->fieldId = $field;

		$this->template->_form = $this['form'];

		$this->redrawControl("fieldInput");
	}

	public function handleShowModal(?int $field = null): void
	{
		$this->fieldId = $field;

		$this->template->_form = $this['form'];

		$this->setView('default');
	}

	protected function createComponentForm(): BaseForm
	{
		$form = new BaseForm;
		$form->setTranslator($this->translator);

		$fields = [];

		foreach ($this->fieldsGetRequest->execute()->getFields() as $field) {
			$fields[$field->getId()] = $field->getName();
		}

		$field = null;

		if ($this->fieldId !== null) {
			try {
				$this->hostParamGetRequest->setHostId($this->hostId);
				$this->hostParamGetRequest->setFieldId($this->fieldId);

				$response = $this->hostParamGetRequest->execute();
				$field = $response->getValue();
				$this->type = $response->getField()->getType();
			} catch (ApiException $e) {
				$this->fieldGetRequest->setFieldId($this->fieldId);
				$this->type = $this->fieldGetRequest->execute()->getType();
			}
		}

		$form->addHidden('type', $this->type);

		$form->addSelect('param', 'back.hosts.params.form.field.label.param', $fields)
			->setRequired('back.hosts.params.form.field.param.validation.required')
			->setDefaultValue($this->fieldId);

		$form->addText('value', 'back.hosts.params.form.field.label.value')
			->setRequired('back.hosts.params.form.field.value.validation.required')
			->setDefaultValue($field);

		$form->addSubmit('send', 'back.hosts.params.form.label.submit');

		$form->onSuccess[] = function (BaseForm $form) {
			$this->template->_form = $this['form'];

			$values = $form->getValues();

			try {
				$this->hostParamsPutRequest->setHostId($this->hostId);
				$this->hostParamsPutRequest->setFieldId($values->param);
				$this->hostParamsPutRequest->setData(
					[
						'value' => $values->value,
					]
				);

				$host = $this->hostParamsPutRequest->execute();

				$this->flashMessage($this->translator->translate('back.hosts.params.update.success'), 'success');

				// If host ID changes, forward it to callback
				$this->onSuccess($this->hostId !== $host->getId() ? $host->getId() : null);

			} catch (BadRequestException $e) {
				$this->flashMessage($this->translator->translate('back.' . $e->getMessage(), $e->getParams()), 'danger');
			} catch (ApiException $e) {
				$this->flashMessage('back.hosts.params.form.error.unknown', 'danger');
			}

			if ($this->isAjax()) {
				$this->redrawControl('flashes');
			}
		};

		return $form;
	}

	public function setHostId(int $id): void
	{
		$this->hostId = $id;
	}

}

interface IParamFormModalFactory
{

	/**
	 * @return ParamFormModal
	 */
	public function create(): ParamFormModal;
}
