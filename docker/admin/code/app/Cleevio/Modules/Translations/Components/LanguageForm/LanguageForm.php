<?php

declare(strict_types=1);

namespace Cleevio\Translations\Components\LanguageForm;

use Cleevio\ApiClient\Exception\ApiException;
use Cleevio\ApiClient\Exception\BadRequestException;
use Cleevio\Countries\API\Requests\CountriesGetRequest;
use Cleevio\Translations\API\Requests\LanguagePostRequest;
use Cleevio\Translations\API\Requests\LanguagePutRequest;
use Cleevio\UI\BaseControl;
use Cleevio\UI\BaseForm;
use Nette\Application\UI\Form;
use Nette\Localization\ITranslator;

class LanguageForm extends BaseControl
{

	/**
	 * @var ITranslator
	 */
	private $translator;

	/**
	 * @var bool
	 */
	private $editMode = false;

	/**
	 * @var LanguagePostRequest
	 */
	private $languagePostRequest;

	/**
	 * @var array
	 */
	public $onSuccess = [];

	/**
	 * @var array
	 */
	public $onError = [];

	/**
	 * @var CountriesGetRequest
	 */
	private $countriesGetRequest;

	/**
	 * @var LanguagePutRequest
	 */
	private $languagePutRequest;

	/**
	 * @param ITranslator $translator
	 * @param CountriesGetRequest $countriesGetRequest
	 * @param LanguagePostRequest $languagePostRequest
	 * @param LanguagePutRequest $languagePutRequest
	 */
	public function __construct(
		ITranslator $translator,
		CountriesGetRequest $countriesGetRequest,
		LanguagePostRequest $languagePostRequest,
		LanguagePutRequest $languagePutRequest
	)
	{
		$this->translator = $translator;
		$this->languagePostRequest = $languagePostRequest;
		$this->countriesGetRequest = $countriesGetRequest;
		$this->languagePutRequest = $languagePutRequest;
	}


	public function render(): void
	{
		$this->template->editMode = $this->editMode;
		$this->template->render(__DIR__ . '/templates/default.latte');
	}


	protected function createComponentForm(): BaseForm
	{
		$form = new BaseForm;
		$form->setTranslator($this->translator);

		$form->addHidden('codeOld');

		$form->addText('name', 'back.languages.form.field.name')
			->setRequired('back.languages.form.field.name.validation.required');

		$form->addText('code', 'back.languages.form.field.code')
			->setRequired('back.languages.form.field.code.validation.required')
			->addRule(Form::MIN_LENGTH, 'back.languages.form.field.code.validation.length', 2)
			->addRule(Form::MAX_LENGTH, 'back.languages.form.field.code.validation.length', 2);

		$countries = [];

		$this->countriesGetRequest->setLimit(1000);
		$items = $this->countriesGetRequest->execute()->getItems();

		foreach ($items as $country) {
			$countries[$country->getCode()] = $country->getName();
		}

		$form->addSelect('country', 'back.hosts.form.field.label.country', $countries)
			->setRequired('back.hosts.form.field.country.validation.required');


		$form->addSubmit('send', 'back.languages.form.label.submit');

		$form->onSuccess[] = function (BaseForm $form) {
			$values = $form->getValues();

			try {
				if ($values->codeOld === "") {
					$this->languagePostRequest->setData([
						'code' => $values->code,
						'name' => $values->name,
						'country' => $values->country,
					]);

					$this->languagePostRequest->execute();
				} else {

					$this->languagePutRequest->setCode($values->codeOld);
					$this->languagePutRequest->setData([
						'name' => $values->name,
						'country' => $values->country,
					]);

					$this->languagePutRequest->execute();
				}

				$this->onSuccess();
			} catch (BadRequestException $e) {
				$this->flashMessage($this->translator->translate('back.' . $e->getMessage(), $e->getParams()), 'danger');
			} catch (ApiException $e) {
				dumpe($e);
				$this->flashMessage('back.translations.languages.form.error.unknown', 'danger');
			}
		};

		return $form;
	}


	public function setEdit(): void
	{
		$this->editMode = true;

		$this['form']['code']->setDisabled(true);
	}

}

interface ILanguageFormFactory
{

	/**
	 * @return LanguageForm
	 */
	function create();
}
