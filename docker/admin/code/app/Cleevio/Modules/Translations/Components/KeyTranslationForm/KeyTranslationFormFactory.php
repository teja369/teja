<?php

declare(strict_types=1);

namespace Cleevio\Translations\Components\LanguageForm;

use Cleevio\Translations\API\Requests\TranslationPutRequest;
use Cleevio\UI\BaseControl;
use Cleevio\UI\BaseForm;
use Nette\Http\Request;
use Nette\Localization\ITranslator;

class KeyTranslationForm extends BaseControl
{

	/**
	 * @var ITranslator
	 */
	private $translator;

	/**
	 * @var array
	 */
	public $onSuccess = [];

	/**
	 * @var array
	 */
	public $onError = [];

	/**
	 * @var TranslationPutRequest
	 */
	private $translationPutRequest;

	/**
	 * @var Request
	 */
	private $request;


	public function __construct(
		ITranslator $translator,
		TranslationPutRequest $translationPutRequest,
		Request $request
	)
	{
		$this->translator = $translator;
		$this->translationPutRequest = $translationPutRequest;
		$this->request = $request;
	}


	public function render(array $parameters): void
	{
		$this['form']->setDefaults([
			'languageCode' => $parameters['activeLanguageCode'],
			'translation' => $parameters['translation'],
			'key' => $parameters['key']->getKey(),
		]);

		$this->template->key = $parameters['key']->getKey();
		$this->template->translation = $parameters['translation'];
		$this->template->defaultTranslation = $parameters['defaultTranslation'];
		$this->template->activeLanguageCode = $parameters['activeLanguageCode'];

		$this->template->render(__DIR__ . '/templates/default.latte');
	}


	public function handleSave(): void
	{
		$this->translationPutRequest->setLanguageCode($this->request->getPost('language'));
		$this->translationPutRequest->setData([['key' => $this->request->getPost('key'), 'value' => $this->request->getPost('value')]]);

		$this->translationPutRequest->execute();

		// Avoid of rendering response
		exit;
	}


	protected function createComponentForm(): BaseForm
	{
		$form = new BaseForm;
		$form->setTranslator($this->translator);

		$form->addHidden('key');
		$form->addHidden('languageCode');

		$form->addText('translation', 'back.languages.form.field-name')
			 ->setRequired('back.languages.form.field-name.validation.required');

		$form->addSubmit('send', 'back.languages.translations.form.label.submit');

		$form->onSuccess[] = function (BaseForm $form) {
			$values = $form->getValues();

			$this->translationPutRequest->setLanguageCode($values->languageCode);
			$this->translationPutRequest->setData([['key' => $values->key, 'value' => $values->translation]]);

			$this->translationPutRequest->execute();
		};

		return $form;
	}
}

interface IKeyTranslationFormFactory
{

	/**
	 * @return KeyTranslationForm
	 */
	function create();
}
