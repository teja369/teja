<?php

declare(strict_types=1);

namespace Cleevio\Translations\Components\SetTranslationFormModal;

use Cleevio\Translations\API\Requests\LanguagesGetRequest;
use Cleevio\Translations\API\Responses\LanguageResponse;
use Cleevio\UI\BaseControlModal;
use Cleevio\UI\BaseForm;
use Kdyby\Autowired\AutowireComponentFactories;
use Nette\Application\IPresenter;
use Nette\Http\Request;
use Nette\Localization\ITranslator;

class SetTranslationFormModal extends BaseControlModal
{

	use AutowireComponentFactories;

	/**
	 * @var array|LanguageResponse[]
	 */
	private $languages;

	/**
	 * @var IGetTranslationRequest
	 */
	private $translationGetRequest;

	/**
	 * @var IPutTranslationRequest
	 */
	private $translationPutRequest;

	/**
	 * @var Request
	 */
	private $request;

	/**
	 * @var ITranslator
	 */
	private $translator;


	public function __construct(
		LanguagesGetRequest $languagesGetRequest,
		ITranslator $translator,
		Request $request
	)
	{
		parent::__construct();

		$this->setTemplateDirectory(__DIR__ . '/templates');
		$this->languages = $languagesGetRequest->execute()->getLanguages();

		$this->monitor(IPresenter::class, function () {
			$this->template->fields = [];
			$this->template->languages = $this->languages;
		});
		$this->request = $request;
		$this->translator = $translator;
	}


	public function handleShowModal(int $id): void
	{
		$this->prepareForm($id);
		$this->setView('default');
	}


	private function prepareForm(int $id): void
	{
		$form = $this['form'];
		$form->setTranslator($this->translator);
		$form->setDefaults(['id' => $id]);

		foreach ($this->languages as $language) {
			$this->translationGetRequest->setId($id);
			$this->translationGetRequest->setLanguageCode($language->getCode());

			$translation = $this->translationGetRequest->execute();

			$container = $form->addContainer($language->getCode());

			foreach ($translation->getMapping()->getFields() as $field) {
				$container->addText($field->getName(), $field->getTranslationKey())->setDefaultValue($field->getValue());
			}

			$this->template->fields = $translation->getMapping()->getFields();
			$container->addSubmit('send', 'back.translations.modal.label.submit');
		}
	}


	protected function createComponentForm(): BaseForm
	{
		$form = new BaseForm;
		$form->addHidden('id');

		$form->onSuccess[] = function (BaseForm $form) {
			$values = $form->getValues();

			foreach ($this->request->getPost() as $language => $fields) {
				if (isset($fields['send'])) {
					unset($fields['send']);

					$this->translationPutRequest->setLanguageCode($language);
					$this->translationPutRequest->setId((int) $values->id);

					$data = [];

					foreach ($fields as $name => $value) {
						$data[$name] = $value;
					}

					$this->translationPutRequest->setData($data);
					$this->translationPutRequest->execute();
				}
			}

			$this->prepareForm((int) $values->id);

		};

		return $form;
	}


	public function setGetRequest(IGetTranslationRequest $translationGetRequest): void
	{
		$this->translationGetRequest = $translationGetRequest;
	}


	public function setPutRequest(IPutTranslationRequest $translationPutRequest): void
	{
		$this->translationPutRequest = $translationPutRequest;
	}
}

interface ISetTranslationFormModal
{

	/**
	 * @return SetTranslationFormModal
	 */
	public function create(): SetTranslationFormModal;
}
