<?php

declare(strict_types=1);

namespace Cleevio\Translations\Components\SetTranslationFormModal;

class TranslationMappingField
{

	/**
	 * @var string
	 */
	private $name;

	/**
	 * @var string
	 */
	private $translationKey;

	/**
	 * @var string|null
	 */
	private $value;


	public function __construct(string $name, string $translationKey, ?string $value = null)
	{
		$this->name = $name;
		$this->translationKey = $translationKey;
		$this->value = $value;
	}


	public function getName(): string
	{
		return $this->name;
	}


	public function getTranslationKey(): string
	{
		return $this->translationKey;
	}


	public function getValue(): ?string
	{
		return $this->value;
	}
}
