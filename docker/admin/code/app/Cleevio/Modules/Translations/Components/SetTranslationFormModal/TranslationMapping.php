<?php

declare(strict_types=1);

namespace Cleevio\Translations\Components\SetTranslationFormModal;

class TranslationMapping
{

	/**
	 * @var array
	 */
	private $fields = [];


	public function addField(TranslationMappingField $field): TranslationMapping
	{
		$this->fields[$field->getName()] = $field;

		return $this;
	}


	/**
	 * @return array|TranslationMappingField[]
	 */
	public function getFields(): array
	{
		return $this->fields;
	}
}
