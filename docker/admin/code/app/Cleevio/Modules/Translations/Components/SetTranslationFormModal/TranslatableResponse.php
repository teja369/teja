<?php

declare(strict_types=1);

namespace Cleevio\Translations\Components\SetTranslationFormModal;

use Cleevio\ApiClient\IResponse;

abstract class TranslatableResponse implements IResponse
{

	abstract public function getMapping(): TranslationMapping;
}
