<?php

declare(strict_types=1);

namespace Cleevio\Translations\Components\SetTranslationFormModal;

interface IPutTranslationRequest
{

	public function setId(int $id): void;


	public function setLanguageCode(string $languageCode): void;


	public function setData(array $data): void;


	public function execute(): TranslatableResponse;

}
