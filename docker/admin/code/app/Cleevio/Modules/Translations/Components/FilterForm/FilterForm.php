<?php

declare(strict_types=1);

namespace Cleevio\Translations\Components\FilterForm;

use Cleevio\UI\ApiFilterForm\FilterForm as BaseFilterForm;
use Cleevio\UI\ApiFilterForm\IFilterInput;
use Cleevio\UI\ApiFilterForm\Inputs\HiddenInput;
use Cleevio\UI\ApiFilterForm\Inputs\SelectInput;
use Cleevio\UI\ApiFilterForm\Inputs\TextInput;
use Nette\Localization\ITranslator;

class FilterForm extends BaseFilterForm
{

	/**
	 * FilterForm constructor.
	 * @param ITranslator $translator
	 */
	public function __construct(ITranslator $translator)
	{
		parent::__construct($translator);
	}


	/**
	 * @return IFilterInput[]
	 */
	protected function getInputs(): array
	{
		return [
			new HiddenInput('code'),
			new TextInput('back.translations.filter.search', 'search', 9),
			new SelectInput('back.translations.filter.state', 'state', [
				0 => 'back.translations.filter.state.not-translated',
				1 => 'back.translations.filter.state.translated',
			], 3),
		];
	}
}

interface IFilterFormFactory
{

	/**
	 * @return FilterForm
	 */
	function create();
}
