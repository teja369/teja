<?php

declare(strict_types=1);

namespace Cleevio\Translations\API\Requests;

use Cleevio\Api\BaseRequest;
use Cleevio\ApiClient\Exception\ApiException;
use Cleevio\Translations\API\Responses\LanguageDeleteResponse;
use InvalidArgumentException;

class LanguageDeleteRequest extends BaseRequest implements ILanguageDeleteRequest
{

	/**
	 * @var string
	 */
	private $languageCode;


	/**
	 * Endpoint path
	 * @return string
	 */
	protected function path(): string
	{
		if ($this->languageCode === null) {
			throw new InvalidArgumentException;
		}

		return sprintf('/languages/%s', $this->languageCode);
	}


	/**
	 * Method
	 * @return string
	 */
	protected function method(): string
	{
		return 'DELETE';
	}


	/**
	 * Determines whether request requires authorization
	 * @return bool
	 */
	function requiresAuth(): bool
	{
		return true;
	}


	/**
	 * Execute request and return a response
	 * @throws ApiException
	 */
	function execute(): LanguageDeleteResponse
	{
		$this->request();

		return new LanguageDeleteResponse;
	}


	/**
	 * @param string $code
	 */
	public function setLanguageCode(string $code): void
	{
		$this->languageCode = $code;
	}
}
