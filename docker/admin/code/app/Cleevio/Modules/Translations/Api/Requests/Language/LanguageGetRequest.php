<?php

declare(strict_types=1);

namespace Cleevio\Translations\API\Requests;

use Cleevio\Api\BaseRequest;
use Cleevio\ApiClient\Exception\ApiException;
use Cleevio\ApiClient\Exception\NotFoundException;
use Cleevio\Translations\API\Responses\LanguageResponse;
use InvalidArgumentException;

class LanguageGetRequest extends BaseRequest implements ILanguageGetRequest
{

	/**
	 * @var string
	 */
	private $languageCode;


	/**
	 * Endpoint path
	 * @return string
	 */
	protected function path(): string
	{
		if ($this->languageCode === null) {
			throw new InvalidArgumentException;
		}

		return sprintf('/languages/%s', $this->languageCode);
	}


	/**
	 * Method
	 * @return string
	 */
	protected function method(): string
	{
		return 'GET';
	}


	/**
	 * Determines whether request requires authorization
	 * @return bool
	 */
	function requiresAuth(): bool
	{
		return true;
	}


	/**
	 * Execute request and return a response
	 * @return LanguageResponse
	 * @throws ApiException
	 * @throws NotFoundException
	 */
	function execute(): LanguageResponse
	{
		return new LanguageResponse($this->request());
	}


	/**
	 * @param string $code
	 */
	public function setLanguageCode(string $code): void
	{
		$this->languageCode = $code;
	}
}
