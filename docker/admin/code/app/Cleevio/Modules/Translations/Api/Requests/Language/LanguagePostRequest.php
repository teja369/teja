<?php

declare(strict_types=1);

namespace Cleevio\Translations\API\Requests;

use Cleevio\Api\BaseRequest;
use Cleevio\ApiClient\Exception\ApiException;
use Cleevio\Translations\API\Responses\LanguageResponse;

class LanguagePostRequest extends BaseRequest implements ILanguagePostRequest
{

	/**
	 * Endpoint path
	 * @return string
	 */
	protected function path(): string
	{
		return '/languages';
	}


	/**
	 * Method
	 * @return string
	 */
	protected function method(): string
	{
		return 'POST';
	}


	/**
	 * Determines whether request requires authorization
	 * @return bool
	 */
	function requiresAuth(): bool
	{
		return true;
	}


	/**
	 * Execute request and return a response
	 * @return LanguageResponse
	 * @throws ApiException
	 */
	function execute(): LanguageResponse
	{
		return new LanguageResponse($this->request());
	}
}
