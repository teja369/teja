<?php

declare(strict_types=1);

namespace Cleevio\Translations\API\Requests;

interface ITranslationPostRequest
{

	/**
	 * @param string $code
	 */
	public function setLanguageCode(string $code): void;
}
