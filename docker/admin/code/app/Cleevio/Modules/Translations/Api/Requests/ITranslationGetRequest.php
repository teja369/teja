<?php

declare(strict_types=1);

namespace Cleevio\Translations\API\Requests;

interface ITranslationGetRequest
{

	/**
	 * @param string $code
	 * @return ITranslationGetRequest
	 */
	public function setCode(string $code): ITranslationGetRequest;


	/**
	 * @param array $params
	 */
	public function setParams(array $params): void;
}
