<?php

declare(strict_types=1);

namespace Cleevio\Translations\API\Responses;

use Cleevio\ApiClient\IResponse;

class TranslationsResponse implements IResponse
{

	/**
	 * @var TranslationResponse[]
	 */
	private $translations = [];



	/**
	 * LanguagesResponse constructor.
	 * @param array $translations
	 */
	public function __construct(array $translations)
	{
		$this->translations = array_map(static function ($translation) {
			return new TranslationResponse($translation);
		}, $translations);
	}


	/**
	 * @return TranslationResponse[]
	 */
	public function getTranslations(): array
	{
		return $this->translations;
	}

}
