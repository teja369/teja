<?php

declare(strict_types=1);

namespace Cleevio\Translations\API\Requests;

use Cleevio\Api\BaseRequest;
use Cleevio\ApiClient\Exception\ApiException;
use Cleevio\Translations\API\Responses\TranslationKeysResponse;

class TranslationKeysGetRequest extends BaseRequest implements ITranslationKeysGetRequest
{

	/**
	 * @var array
	 */
	private $params = [];

	/**
	 * Endpoint path
	 * @return string
	 */
	protected function path(): string
	{
		$query = http_build_query($this->params);

		$path = '/translations/keys';

		return strlen($query) > 0
			? $path . '?' . $query
			: $path;
	}


	/**
	 * Method
	 * @return string
	 */
	protected function method(): string
	{
		return 'GET';
	}


	/**
	 * Determines whether request requires authorization
	 * @return bool
	 */
	function requiresAuth(): bool
	{
		return true;
	}


	/**
	 * @param array $params
	 */
	function setParams(array $params): void
	{
		$this->params = $params;
	}


	/**
	 * Execute request and return a response
	 * @return TranslationKeysResponse
	 * @throws ApiException
	 */
	function execute(): TranslationKeysResponse
	{
		return new TranslationKeysResponse($this->request());
	}
}
