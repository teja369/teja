<?php

declare(strict_types=1);

namespace Cleevio\Translations\API\Requests;

use Cleevio\Api\BaseRequest;
use Cleevio\ApiClient\Exception\ApiException;
use Cleevio\Translations\API\Responses\LanguageResponse;

class LanguagePutRequest extends BaseRequest implements ILanguagePutRequest
{
	/**
	 * @var string
	 */
	private $code;

	/**
	 * Endpoint path
	 * @return string
	 */
	protected function path(): string
	{
		if ($this->code === null) {
			throw new \InvalidArgumentException;
		}

		return sprintf('/languages/%s', $this->code);
	}


	/**
	 * Method
	 * @return string
	 */
	protected function method(): string
	{
		return 'PUT';
	}


	/**
	 * Determines whether request requires authorization
	 * @return bool
	 */
	function requiresAuth(): bool
	{
		return true;
	}

	/**
	 * Execute request and return a response
	 * @return LanguageResponse
	 * @throws ApiException
	 */
	function execute(): LanguageResponse
	{
		return new LanguageResponse($this->request());
	}

	/**
	 * @param string $code
	 */
	function setCode(string $code): void
	{
		$this->code = $code;
	}
}
