<?php

declare(strict_types=1);

namespace Cleevio\Translations\API\Requests;

interface ILanguagePutRequest
{

	function setCode(string $code): void;

}
