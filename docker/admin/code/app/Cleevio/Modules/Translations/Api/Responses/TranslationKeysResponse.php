<?php

declare(strict_types=1);

namespace Cleevio\Translations\API\Responses;

use Cleevio\ApiClient\IResponse;

class TranslationKeysResponse implements IResponse
{

	/**
	 * @var TranslationKeyResponse[]
	 */
	private $translationKeys = [];


	/**
	 * @param array $keys
	 */
	public function __construct(array $keys)
	{
		$this->translationKeys = array_map(static function ($key) {
			return new TranslationKeyResponse($key);
		}, $keys);
	}


	public function getTranslationKeys(): array
	{
		return $this->translationKeys;
	}

}
