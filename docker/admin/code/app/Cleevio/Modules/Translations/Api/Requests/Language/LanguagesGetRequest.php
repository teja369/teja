<?php

declare(strict_types=1);

namespace Cleevio\Translations\API\Requests;

use Cleevio\Api\BaseRequest;
use Cleevio\ApiClient\Exception\ApiException;
use Cleevio\Translations\API\Responses\LanguagesResponse;

class LanguagesGetRequest extends BaseRequest implements ILanguagesGetRequest
{

	/**
	 * Endpoint path
	 * @return string
	 */
	protected function path(): string
	{
		return '/languages';
	}

	/**
	 * Method
	 * @return string
	 */
	protected function method(): string
	{
		return 'GET';
	}

	/**
	 * Determines whether request requires authorization
	 * @return bool
	 */
	function requiresAuth(): bool
	{
		return false;
	}

	/**
	 * Execute request and return a response
	 * @return LanguagesResponse
	 * @throws ApiException
	 */
	function execute(): LanguagesResponse
	{
		return new LanguagesResponse($this->request());
	}
}
