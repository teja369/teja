<?php

declare(strict_types=1);

namespace Cleevio\Translations\API\Responses;

use Cleevio\ApiClient\DTO\Locale;
use Cleevio\Countries\API\Responses\CountryResponse;
use InvalidArgumentException;

class LanguageResponse extends Locale
{

	/**
	 * @var int
	 */
	private $id;

	/**
	 * @var string
	 */
	private $code;

	/**
	 * @var string
	 */
	private $name;

	/**
	 * @var bool
	 */
	private $isDefault;

	/**
	 * @var CountryResponse
	 */
	private $country;

	/**
	 * LanguagesResponse constructor.
	 * @param array $language
	 */
	public function __construct(array $language)
	{
		if (
			array_key_exists('code', $language) &&
			array_key_exists('name', $language) &&
			array_key_exists('id', $language) &&
			array_key_exists('isDefault', $language) &&
			array_key_exists('country', $language)
		) {
			$this->id = $language['id'];
			$this->code = $language['code'];
			$this->name = $language['name'];
			$this->country = new CountryResponse($language['country']);
			$this->isDefault = $language['isDefault'];
		} else {
			throw new InvalidArgumentException;
		}
	}


	public function getCode(): string
	{
		return $this->code;
	}


	public function getId(): int
	{
		return $this->id;
	}


	public function getName(): string
	{
		return $this->name;
	}

	public function isDefault(): bool
	{
		return $this->isDefault;
	}

	public function getCountry(): CountryResponse
	{
		return $this->country;
	}

	public function getDateFormat(): string
	{
		return 'd-m-Y';
	}

	public function getDateTimeFormat(): string
	{
		return 'd-m-Y H:i:s';
	}
}
