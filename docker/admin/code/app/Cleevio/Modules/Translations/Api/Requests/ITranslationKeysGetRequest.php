<?php

declare(strict_types=1);

namespace Cleevio\Translations\API\Requests;

interface ITranslationKeysGetRequest
{

	/**
	 * @param array $params
	 */
	public function setParams(array $params): void;
}
