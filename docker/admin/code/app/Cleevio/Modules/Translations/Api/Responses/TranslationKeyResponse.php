<?php

declare(strict_types=1);

namespace Cleevio\Translations\API\Responses;

use Cleevio\ApiClient\IResponse;
use InvalidArgumentException;

class TranslationKeyResponse implements IResponse
{

	/**
	 * @var int
	 */
	private $id;

	/**
	 * @var string
	 */
	private $key;


	/**
	 * @param array $key
	 */
	public function __construct(array $key)
	{
		if (array_key_exists('id', $key) && array_key_exists('key', $key)) {
			$this->id = $key['id'];
			$this->key = $key['key'];
		} else {
			throw new InvalidArgumentException;
		}
	}


	public function getId(): int
	{
		return $this->id;
	}


	public function getKey(): string
	{
		return $this->key;
	}
}
