<?php

declare(strict_types=1);

namespace Cleevio\Translations\API\Requests;

use Cleevio\Api\BaseRequest;
use Cleevio\ApiClient\Exception\ApiException;
use Cleevio\Translations\API\Responses\TranslationsResponse;

class TranslationGetRequest extends BaseRequest implements ITranslationGetRequest
{

	/**
	 * @var string
	 */
	private $code = 'en';

	/**
	 * @var array
	 */
	private $params = [];


	/**
	 * @param string $code
	 * @return TranslationGetRequest
	 */
	public function setCode(string $code): ITranslationGetRequest
	{
		$this->code = $code;

		return $this;
	}


	/**
	 * @param array $params
	 */
	public function setParams(array $params): void
	{
		$this->params = $params;
	}


	/**
	 * Endpoint path
	 * @return string
	 */
	protected function path(): string
	{
		$path = '/translations/' . $this->code;

		$query = http_build_query($this->params);

		return strlen($query) > 0
			? $path . '?' . $query
			: $path;
	}


	/**
	 * Method
	 * @return string
	 */
	protected function method(): string
	{
		return 'GET';
	}


	/**
	 * Determines whether request requires authorization
	 * @return bool
	 */
	function requiresAuth(): bool
	{
		return false;
	}


	/**
	 * Execute request and return a response
	 * @return TranslationsResponse
	 * @throws ApiException
	 */
	function execute(): TranslationsResponse
	{
		return new TranslationsResponse($this->request());
	}
}
