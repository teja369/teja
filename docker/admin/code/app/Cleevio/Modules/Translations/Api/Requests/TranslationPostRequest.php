<?php

declare(strict_types=1);

namespace Cleevio\Translations\API\Requests;

use Cleevio\Api\BaseRequest;
use Cleevio\ApiClient\Exception\ApiException;
use Cleevio\Translations\API\Responses\TranslationPostResponse;
use InvalidArgumentException;

class TranslationPostRequest extends BaseRequest implements ITranslationPostRequest
{

	/**
	 * @var string
	 */
	private $languageCode;


	/**
	 * Endpoint path
	 * @return string
	 */
	protected function path(): string
	{
		if ($this->languageCode === null) {
			throw new InvalidArgumentException;
		}

		return sprintf('/translations/%s', $this->languageCode);
	}


	/**
	 * Method
	 * @return string
	 */
	protected function method(): string
	{
		return 'POST';
	}


	/**
	 * Determines whether request requires authorization
	 * @return bool
	 */
	function requiresAuth(): bool
	{
		return true;
	}


	/**
	 * Execute request and return a response
	 * @return TranslationPostResponse
	 * @throws ApiException
	 */
	function execute(): TranslationPostResponse
	{
		$this->request();

		return new TranslationPostResponse;
	}


	/**
	 * @param string $code
	 */
	public function setLanguageCode(string $code): void
	{
		$this->languageCode = $code;
	}
}
