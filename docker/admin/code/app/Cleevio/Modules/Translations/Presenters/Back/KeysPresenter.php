<?php

declare(strict_types=1);

namespace Cleevio\Translations\BackModule\Presenters;

use Cleevio\ApiClient\Exception\NotFoundException;
use Cleevio\Translations\API\Requests\LanguagesGetRequest;
use Cleevio\Translations\API\Requests\TranslationGetRequest;
use Cleevio\Translations\API\Requests\TranslationKeysGetRequest;
use Cleevio\Translations\API\Responses\TranslationKeyResponse;
use Cleevio\Translations\Components\FilterForm\IFilterFormFactory;
use Cleevio\Translations\Components\LanguageForm\IKeyTranslationFormFactory;
use Cleevio\Translations\Components\LanguageForm\KeyTranslationForm;
use Cleevio\UI\ApiFilterForm\FilterForm;
use Nette\Utils\Strings;

class KeysPresenter extends BasePresenter
{

	/**
	 * @var LanguagesGetRequest
	 */
	private $languagesGetRequest;

	/**
	 * @var TranslationGetRequest
	 */
	private $translationGetRequest;

	/**
	 * @var TranslationKeysGetRequest
	 */
	private $translationKeysGetRequest;


	public function __construct(
		LanguagesGetRequest $languagesGetRequest,
		TranslationGetRequest $translationGetRequest,
		TranslationKeysGetRequest $translationKeysGetRequest
	)
	{
		parent::__construct();
		$this->languagesGetRequest = $languagesGetRequest;
		$this->translationGetRequest = $translationGetRequest;
		$this->translationKeysGetRequest = $translationKeysGetRequest;
	}


	public function startup()
	{
		parent::startup();
	}


	public function renderDefault(?string $code, ?string $search, ?int $state): void
	{
		$languages = $this->languagesGetRequest->execute()->getLanguages();

		if ($code === null && count($languages) > 0) {
			$this->redirectPermanent('this', ['code' => $languages[0]->getCode()]);
		}

		if ($code !== null) {

			$params = [
				'search' => $search,
				'code' => $code,
				'state' => $state,
			];

			$this['filterForm']->setDefaults($params);

			$this->translationGetRequest->setCode(Strings::lower($code));
			$this->translationGetRequest->setParams($params);
			$translations = $this->translationGetRequest->execute()->getTranslations();

			$this->translationKeysGetRequest->setParams($params);

			$keysResponse = $this->translationKeysGetRequest->execute()->getTranslationKeys();

			$translatedKeys = [];
			$keys = [];

			foreach ($translations as $translationResponse) {
				$translatedKeys[$translationResponse->getKey()] = $translationResponse->getValue();
			}

			/** @var TranslationKeyResponse $key */
			foreach ($keysResponse as $key) {
				if (($state === 0 || $state === null) && !array_key_exists($key->getKey(), $translatedKeys)) {
					$keys[] = $key;
				} elseif (($state === 1 || $state === null) && array_key_exists($key->getKey(), $translatedKeys)) {
					$keys[] = $key;
				}
			}

			try {
				$this->translationGetRequest->setCode('en');
				$englishTranslations = $this->translationGetRequest->execute()->getTranslations();

				$englishTranslatedKeys = [];

				foreach ($englishTranslations as $englishTranslation) {
					$englishTranslatedKeys[$englishTranslation->getKey()] = $englishTranslation->getValue();
				}

				$this->template->englishTranslatedKeys = $englishTranslatedKeys;
			} catch (NotFoundException $e) {
			}

			$this->template->missingTranslations = (count($keysResponse) - count($translations));
			$this->template->translatedKeys = $translatedKeys;
			$this->template->keys = $keys;
		}

		$this->template->activeCode = $code;
		$this->template->languages = $languages;

	}


	protected function createComponentKeyTranslationForm(IKeyTranslationFormFactory $keyTranslationFormFactory
	): KeyTranslationForm
	{
		return $keyTranslationFormFactory->create();
	}


	protected function createComponentFilterForm(IFilterFormFactory $factory): FilterForm
	{
		$control = $factory->create();

		$control->onSuccess[] = function (array $params) {
			$this->redirectPermanent(':Translations:Back:Keys:', $params);
		};

		return $control;
	}
}
