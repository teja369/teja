<?php

declare(strict_types=1);

namespace Cleevio\Translations\BackModule\Presenters;

use Cleevio\ApiClient\Exception\ApiException;
use Cleevio\ApiClient\Exception\NotFoundException;
use Cleevio\Translations\API\Requests\LanguageDeleteRequest;
use Cleevio\Translations\API\Requests\LanguageGetRequest;
use Cleevio\Translations\API\Requests\LanguagesGetRequest;
use Cleevio\Translations\Components\LanguageForm\ILanguageFormFactory;
use Cleevio\Translations\Components\LanguageForm\LanguageForm;

class HomepagePresenter extends BasePresenter
{

	/**
	 * @var LanguagesGetRequest
	 */
	private $languagesGetRequest;

	/**
	 * @var LanguageDeleteRequest
	 */
	private $languageDeleteRequest;

	/**
	 * @var LanguageGetRequest
	 */
	private $languageGetRequest;


	public function __construct(
		LanguagesGetRequest $languagesGetRequest,
		LanguageDeleteRequest $languageDeleteRequest,
		LanguageGetRequest $languageGetRequest
	)
	{
		parent::__construct();
		$this->languagesGetRequest = $languagesGetRequest;
		$this->languageDeleteRequest = $languageDeleteRequest;
		$this->languageGetRequest = $languageGetRequest;
	}


	public function startup()
	{
		parent::startup();
	}


	public function renderDefault(): void
	{
		$this->template->languages = $this->languagesGetRequest->execute()->getLanguages();
	}


	public function actionEdit(string $code): void
	{
		$this->languageGetRequest->setLanguageCode($code);

		try {
			$language = $this->languageGetRequest->execute();
			$this->template->language = $language;

			$this['languageForm']->setEdit();
			$this['languageForm']['form']->setDefaults([
				'codeOld' => $language->getCode(),
				'code' => $language->getCode(),
				'name' => $language->getName(),
				'country' => $language->getCountry()->getCode(),
			]);

		} catch (NotFoundException $e) {
			$this->flashMessage($this->translator->translate('back.languages.edit.not-found'), 'danger');
			$this->redirect(':Translations:Back:Homepage:default');
		} catch (ApiException $e) {
			$this->flashMessage($this->translator->translate('back.languages.delete.error'), 'danger');
			$this->redirect(':Translations:Back:Homepage:default');
		}
	}


	public function handleDelete(string $code): void
	{
		try {
			$this->languageDeleteRequest->setLanguageCode($code);
			$this->languageDeleteRequest->execute();
		} catch (ApiException $e) {
			$this->flashMessage($this->translator->translate('back.languages.delete.error'), 'danger');
		}

		$this->flashMessage($this->translator->translate('back.languages.delete.success'), 'success');
		$this->redirect('this');
	}


	protected function createComponentLanguageForm(ILanguageFormFactory $languageFormFactory): LanguageForm
	{
		$control = $languageFormFactory->create();

		$control->onSuccess[] = function () {
			$this->flashMessage($this->translator->translate('back.languages.create.success'), 'success');
			$this->redirect(':Translations:Back:Homepage:default');
		};

		$control->onError[] = function () {
			$this->flashMessage($this->translator->translate('back.languages.create.error'), 'danger');
			$this->redirect(':Translations:Back:Homepage:default');
		};

		return $control;
	}
}
