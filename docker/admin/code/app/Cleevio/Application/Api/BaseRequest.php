<?php

declare(strict_types=1);

namespace Cleevio\Api;

use Cleevio\ApiClient\BaseRequest as ClientBaseRequest;

abstract class BaseRequest extends ClientBaseRequest
{
	protected function request(): array
	{
		return parent::request()['data'];
	}

}
