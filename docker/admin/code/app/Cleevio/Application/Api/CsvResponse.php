<?php

declare(strict_types=1);

namespace Cleevio\API;

use Nette;
use Nette\Application\IResponse;

class CsvResponse implements IResponse
{
	/**
	 * @var string
	 */
	private $data;

	/**
	 * @var string
	 */
	private $filename;

	/**
	 * @param string $data
	 * @param string $filename
	 */
	public function __construct(string $data, string $filename = 'csv.csv')
	{
		$this->data = $data;
		$this->filename = $filename;
	}

	/**
	 * Sends response to output.
	 * @param Nette\Http\IRequest $httpRequest
	 * @param Nette\Http\IResponse $httpResponse
	 */
	function send(Nette\Http\IRequest $httpRequest, Nette\Http\IResponse $httpResponse): void
	{
		unset($httpRequest);

		$httpResponse->setContentType('text/csv', 'utf-8');
		$attachment = 'attachment; filename="' . $this->filename . '"';
		$httpResponse->setHeader('Content-Disposition', $attachment);
		$httpResponse->setHeader('Content-Length', strlen($this->data) . '');

		print $this->data;
	}
}
