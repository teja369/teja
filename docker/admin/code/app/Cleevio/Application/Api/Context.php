<?php

declare(strict_types=1);

namespace Cleevio\API;

use Cleevio\ApiClient\DTO\Authorization;
use Cleevio\ApiClient\DTO\Locale;
use Cleevio\ApiClient\DTO\Subject;
use Cleevio\ApiClient\IContext;
use Cleevio\Sso\SsoContext;
use Cleevio\Users\Api\Responses\UserResponse;
use DateTime;
use Exception;
use Nette\Http\Session;
use Nette\Http\SessionSection;
use Nette\Security\AuthenticationException;
use Nette\Security\IAuthenticator;
use Nette\Security\Identity;
use Nette\Security\IIdentity;

class Context implements IContext, IAuthenticator
{
	private const SESSION_AUTHORIZATION_PARAM = 'auth';
	private const SESSION_LOCALE_PARAM = 'locale';
	private const SESSION_USER_PARAM = 'user';

	private const SESSION_TIMEOUT_PARAM = 'timeout';
	private const SESSION_TIMEOUT_SECTION = 'auth';

	/**
	 * @var SessionSection
	 */
	private $session;

	/**
	 * @var SsoContext
	 */
	private $ssoContext;

	/**
	 * @param Session $session
	 * @param SsoContext $ssoContext
	 */
	public function __construct(
		Session $session,
		SsoContext $ssoContext
	)
	{
		$this->session = $session->getSection(self::SESSION_TIMEOUT_SECTION);
		$this->ssoContext = $ssoContext;
	}

	/**
	 * Get stored authorization credentials
	 *
	 * @return Authorization
	 */
	public function getAuthorization(): ?Authorization
	{
		if (!$this->session->offsetExists(self::SESSION_AUTHORIZATION_PARAM)) {
			return null;
		}

		$auth = @unserialize($this->session[self::SESSION_AUTHORIZATION_PARAM]);

		return $auth === false
			? null
			: $auth;
	}

	/**
	 * @return bool
	 * @throws Exception
	 */
	public function getTimeout()
	{
		if ($this->ssoContext->getUri() === null) {
			return true;
		}

		if ($this->ssoContext->getTimeout() <= 0) {
			return true;
		}

		if (!$this->session->offsetExists(self::SESSION_TIMEOUT_PARAM)) {
			$this->session[self::SESSION_TIMEOUT_PARAM] = (new DateTime)->getTimestamp() + $this->ssoContext->getTimeout();
		} else {
			$left = $this->session[self::SESSION_TIMEOUT_PARAM] - (new DateTime)->getTimestamp();

			if ($left < 0) {
				unset($this->session[self::SESSION_TIMEOUT_PARAM]);

				return false;
			} else {
				$this->session[self::SESSION_TIMEOUT_PARAM] = (new DateTime)->getTimestamp() + $this->ssoContext->getTimeout();
			}
		}

		return true;
	}

	/**
	 * Set new authorization credentials
	 *
	 * @param Authorization $authorization
	 * @return void
	 */
	public function setAuthorization(Authorization $authorization): void
	{
		$this->session[self::SESSION_AUTHORIZATION_PARAM] = serialize($authorization);
	}

	/**
	 * Get current language
	 * @return Locale|null
	 */
	public function getLocale(): ?Locale
	{
		if (!$this->session->offsetExists(self::SESSION_LOCALE_PARAM)) {
			return null;
		}

		$locale = @unserialize($this->session[self::SESSION_LOCALE_PARAM]);

		return $locale === false
			? null
			: $locale;
	}

	/**
	 * Set language for translation
	 * @param Locale $locale
	 */
	public function setLocale(Locale $locale): void
	{
		$this->session[self::SESSION_LOCALE_PARAM] = serialize($locale);
	}

	/**
	 * @return Subject|null
	 */
	public function getSubject(): ?Subject
	{
		if (!$this->session->offsetExists(self::SESSION_USER_PARAM)) {
			return null;
		}

		$user = @unserialize($this->session[self::SESSION_USER_PARAM]);

		return $user === false
			? null
			: $user;
	}

	/**
	 * Set subject
	 * @param Subject $subject
	 */
	public function setSubject(Subject $subject): void
	{
		$this->session[self::SESSION_USER_PARAM] = serialize($subject);
	}

	/**
	 * Send locale parameter to API
	 * @return bool
	 */
	public function sendLocale(): bool
	{
		return false;
	}

	/**
	 * Clear context data
	 */
	public function clear(): void
	{
		unset($this->session[self::SESSION_AUTHORIZATION_PARAM]);
		unset($this->session[self::SESSION_USER_PARAM]);
		unset($this->session[self::SESSION_TIMEOUT_PARAM]);
	}

	/**
	 * Performs an authentication against e.g. database.
	 * and returns IIdentity on success or throws AuthenticationException
	 * @param array $credentials
	 * @return IIdentity
	 * @throws AuthenticationException
	 */
	function authenticate(array $credentials): IIdentity
	{
		[$username] = $credentials;

		$subject = $this->getSubject();

		if ($this->getAuthorization() instanceof Authorization && $subject instanceof UserResponse) {
			return new Identity($subject->getId(), $subject->getRole(), [
					'name' => $subject->getName(),
					'email' => $subject->getEmail(),
					'username' => $username,
				]
			);
		}

		throw new AuthenticationException;
	}
}
