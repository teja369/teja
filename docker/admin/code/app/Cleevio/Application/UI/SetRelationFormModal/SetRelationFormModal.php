<?php

declare(strict_types=1);

namespace Cleevio\UI\SetRelationFormModal;

use Cleevio\UI\ApiPaginationControl\ApiPaginationControl;
use Cleevio\UI\ApiPaginationControl\IApiPaginationControlFactory;
use Cleevio\UI\BaseControlModal;
use Cleevio\UI\SetFormModal\DataGetRequest;
use Cleevio\UI\SetFormModal\IRelationGetRequest;
use Cleevio\UI\SetFormModal\IRelationResponse;
use Cleevio\UI\SetFormModal\IRelationSetRequest;
use Kdyby\Autowired\AutowireComponentFactories;
use Nette\Application\AbortException;
use Nette\Localization\ITranslator;

/**
 * Class SetRelationFormModal
 * @package Cleevio\UI\SetFormModal
 */
abstract class SetRelationFormModal extends BaseControlModal
{

	use AutowireComponentFactories;

	/**
	 * @var array
	 */
	public $onSuccess = [];

	/**
	 * Request to retrieve all possible data to set
	 *
	 * @var DataGetRequest
	 */
	private $dataGetRequest;

	/**
	 * @var ITranslator
	 */
	private $translator;

	/**
	 * Request to retrieve already connected data
	 *
	 * @var IRelationGetRequest
	 */
	private $getRequest;

	/**
	 * Request to set new connection
	 *
	 * @var IRelationSetRequest
	 */
	private $putRequest;

	/**
	 * @param ITranslator $translator
	 * @param DataGetRequest $dataGetRequest
	 */
	public function __construct(ITranslator $translator, DataGetRequest $dataGetRequest)
	{
		parent::__construct();

		$this->setTemplateDirectory(__DIR__ . '/templates');

		$this->translator = $translator;
		$this->dataGetRequest = $dataGetRequest;
	}

	/**
	 * Get translation key for headline
	 *
	 * @return string
	 */
	protected function translationHeadline(): string
	{
		return "back.relation.modal.headline";
	}

	/**
	 * Get translation key for add button
	 *
	 * @return string
	 */
	protected function translationAdd(): string
	{
		return "back.relation.modal.link.add";
	}

	/**
	 * Get translation key for add button
	 *
	 * @return string
	 */
	protected function translationDelete(): string
	{
		return "back.relation.modal.link.delete";
	}

	/**
	 * LOad and show data to set
	 *
	 * @param int $itemId
	 */
	public function handleShowModal(int $itemId): void
	{
		$this->getRequest->setId($itemId);
		$response = $this->getRequest->execute();

		$ids = [];

		foreach ($response->getItems() as $item) {
			$ids[$item->getId()] = $item->getId();
		}

		$this->template->itemIdsUsed = $ids;
		$this->template->itemId = $itemId;
		$this->template->items = $this['apiPagination']->getItems()->getItems();

		$this->template->translationHeadline = $this->translationHeadline();
		$this->template->translationAdd = $this->translationAdd();
		$this->template->translationDelete = $this->translationDelete();

		$this->setView('default');
	}

	/**
	 * Add item action
	 *
	 * @param int $entityId
	 * @param int $itemId
	 * @throws AbortException
	 */
	public function handleAdd(int $entityId, int $itemId): void
	{
		$this->getRequest->setId($itemId);
		$response = $this->getRequest->execute();

		$ids = array_map(static function (IRelationResponse $item) {
			return $item->getId();
		}, $response->getItems());

		$entityIds = [];

		foreach ($ids as $id) {
			$entityIds[] = ['id' => $id];
		}

		$entityIds[] = ['id' => $entityId];

		$this->putRequest->setId($itemId);
		$this->putRequest->setData($entityIds);
		$this->putRequest->execute();

		$this->flashMessage($this->translator->translate('back.relation.modal.update.success'), 'success');

		$this->redirectPermanent('showModal!', ['itemId' => $itemId]);
	}

	/**
	 * Remove item action
	 *
	 * @param int $entityId
	 * @param int $itemId
	 * @throws AbortException
	 */
	public function handleDelete(int $entityId, int $itemId): void
	{
		$this->getRequest->setId($itemId);
		$response = $this->getRequest->execute();

		$ids = array_map(static function (IRelationResponse $item) {
			return $item->getId();
		}, $response->getItems());

		$entityIds = [];

		foreach ($ids as $id) {
			if ($id !== $entityId) {
				$entityIds[] = ['id' => $id];
			}
		}

		$this->putRequest->setId($itemId);
		$this->putRequest->setData($entityIds);
		$this->putRequest->execute();

		$this->flashMessage($this->translator->translate('back.relation.modal.update.success'), 'success');

		$this->redirectPermanent('showModal!', ['itemId' => $itemId]);
	}

	/**
	 * Create pagination component
	 *
	 * @param IApiPaginationControlFactory $factory
	 * @return ApiPaginationControl
	 */
	protected function createComponentApiPagination(IApiPaginationControlFactory $factory): ApiPaginationControl
	{
		$control = $factory->create();

		$control->setLimit(1000);
		$control->setRequest($this->dataGetRequest);

		return $control;
	}

	public function setGetRequest(IRelationGetRequest $getRequest): void
	{
		$this->getRequest = $getRequest;
	}


	public function setPutRequest(IRelationSetRequest $putRequest): void
	{
		$this->putRequest = $putRequest;
	}

}

interface ISetRelationFormModalFactory
{

	/**
	 * @return SetRelationFormModal
	 */
	public function create(): SetRelationFormModal;
}
