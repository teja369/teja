<?php

declare(strict_types=1);

namespace Cleevio\UI\SetFormModal;

use Cleevio\ApiClient\IResponse;

/**
 * Single item response for connected entities
 *
 * @package Cleevio\UI\SetFormModal
 */
interface IRelationResponse extends IResponse
{
	public function getId(): int;
}
