<?php

declare(strict_types=1);

namespace Cleevio\UI\SetFormModal;

use Cleevio\ApiClient\Requests\PaginationRequest;

abstract class DataGetRequest extends PaginationRequest implements IDataGetRequest
{
	protected function request(): array
	{
		return parent::request()['data'];
	}
}
