<?php

declare(strict_types=1);

namespace Cleevio\UI\SetFormModal;

use Cleevio\ApiClient\IRequest;

interface IRelationSetRequest extends IRequest
{

	public function setId(int $id): void;

}
