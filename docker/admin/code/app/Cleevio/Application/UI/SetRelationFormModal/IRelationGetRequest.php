<?php

declare(strict_types=1);

namespace Cleevio\UI\SetFormModal;

use Cleevio\ApiClient\IRequest;

interface IRelationGetRequest extends IRequest
{

	function execute(): IRelationsResponse;

	public function setId(int $id): void;

}
