<?php

declare(strict_types=1);

namespace Cleevio\UI\SearchForm;

use Cleevio\UI\BaseControl;
use Cleevio\UI\BaseForm;
use Nette\Localization\ITranslator;

class SearchForm extends BaseControl
{

	/**
	 * @var ITranslator
	 */
	private $translator;

	/**
	 * @var array
	 */
	public $onSuccess = [];

	/**
	 * @var array
	 */
	public $onError = [];

	/**
	 * @param ITranslator $translator
	 */
	public function __construct(
		ITranslator $translator
	)
	{
		$this->translator = $translator;
	}


	public function render(): void
	{
		$this->template->render(__DIR__ . '/templates/default.latte');
	}


	protected function createComponentForm(): BaseForm
	{
		$form = new BaseForm;
		$form->setTranslator($this->translator);

		$form->addText('search', 'back.hosts.search.form.field.label.search');

		$form->addSubmit('send', 'back.hosts.search.form.field.button.search');

		$form->onSuccess[] = function (BaseForm $form) {
			$this->onSuccess($form->getValues()->search);
		};

		return $form;
	}

}

interface ISearchFormFactory
{

	/**
	 * @return SearchForm
	 */
	function create();
}
