<?php

declare(strict_types=1);

namespace Cleevio\UI\SearchForm;

interface ISearchRequest
{
	public function setSearch(?string $search): void;
}
