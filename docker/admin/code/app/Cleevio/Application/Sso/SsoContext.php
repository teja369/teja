<?php

declare(strict_types=1);

namespace Cleevio\Sso;

class SsoContext
{

	/**
	 * SSO URI endpoint
	 *
	 * @var string|null
	 */
	private $uri;

	/**
	 * SSO session timeout configuration
	 *
	 * @var int
	 */
	private $timeout;

	/**
	 * @param string $uri
	 * @param int $timeout
	 */
	public function __construct(?string $uri, int $timeout)
	{
		$this->uri = $uri;
		$this->timeout = $timeout;
	}

	public function getUri(): ?string
	{
		return $this->uri;
	}

	public function getTimeout(): int
	{
		return $this->timeout;
	}

}
