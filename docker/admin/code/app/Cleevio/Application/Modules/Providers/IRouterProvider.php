<?php

declare(strict_types=1);

namespace Cleevio\Modules\Providers;

interface IRouterProvider
{

	public function getRouterSettings(): array;
}
