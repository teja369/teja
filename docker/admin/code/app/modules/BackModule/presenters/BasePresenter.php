<?php

declare(strict_types=1);

namespace Cleevio\BackModule\Presenters;

use Cleevio\API\Context;
use Cleevio\Sso\SsoContext;
use Cleevio\Translations\Components\LanguageSelect\ILanguageSelectFactory;
use Cleevio\Translations\Components\LanguageSelect\LanguageSelect;
use Cleevio\Users\BackModule\Presenters\LoginPresenter;
use Cleevio\Users\BackModule\Presenters\LogoutPresenter;
use Kdyby\Autowired\AutowireComponentFactories;
use Kdyby\Autowired\AutowireProperties;
use Nette;

abstract class BasePresenter extends Nette\Application\UI\Presenter
{

	private const TRAVELLER_ENV = 'TRAVELLER_URI';

	use AutowireProperties;
	use AutowireComponentFactories;

	/**
	 * @var Nette\Localization\ITranslator
	 * @inject
	 */
	public $translator;

	/**
	 * @var Context
	 * @inject
	 */
	public $context;

	/**
	 * @var SsoContext
	 * @inject
	 */
	public $ssoContext;

	public function startup()
	{
		parent::startup();
		$this->autoCanonicalize = false;

		// Handle language
		$this['languageSelect']->handleLang();

		if (($this->context === null || $this->context->getAuthorization() === null) && !$this->isAuthAction()) {
			if ($this->ssoContext->getUri() !== null) {
				$this->redirectUrl($this->ssoContext->getUri());
			} else {
				$this->redirect(':Users:Back:Login:default');
			}
		}

		if (!$this->getUser()->isAllowed('admin') && !$this->isAuthAction()) {
			$this->flashMessage("back.forbidden", "danger");
			$this->redirect(':Users:Back:Login:default');
		}

		if ($this->ssoContext->getUri() !== null && !$this->context->getTimeout() && !$this->isAuthAction()) {
			$this->redirect(':Users:Back:Logout:sso');
		}

		if ($this->getPresenter() instanceof LoginPresenter) {
			$this->setLayout(__DIR__ . '/templates/@layout.latte');
		} else {
			$this->setLayout(__DIR__ . '/templates/@layout-content.latte');
		}

		$this->template->travellerUri = env(self::TRAVELLER_ENV);
		$this->template->localeContext = $this->context->getLocale();
	}

	/**
	 * Determines if current uri is login/logout endpoint
	 * @return bool
	 */
	private function isAuthAction(): bool
	{
		return !($this->getPresenter() instanceof LoginPresenter === false && $this->getPresenter() instanceof LogoutPresenter === false);
	}

	/**
	 * @param ILanguageSelectFactory $factory
	 * @return LanguageSelect
	 */
	public function createComponentLanguageSelect(ILanguageSelectFactory $factory): LanguageSelect
	{
		$control = $factory->create();

		$control->onSuccess[] = function () {
			if ($this->isAjax()) {
				$this->payload->forceRedirect = true;
				$this->redirect('this');
			} else {
				$this->redirect('this');
			}
		};

		return $control;
	}

	/**
	 * Redirect to the traveller app
	 *
	 * @throws Nette\Application\AbortException
	 * @throws Nette\Application\BadRequestException
	 */
	public function handleTraveller(): void
	{
		if ($this->context === null || $this->context->getAuthorization() === null) {
			$this->error("Unauthorized", 401);
		}

		$travellerUri = env(self::TRAVELLER_ENV);

		if ($travellerUri !== null && $travellerUri !== '') {

			$params = [
				'refreshToken' => $this->context->getAuthorization()->getRefreshToken(),
			];

			$query = http_build_query($params);

			$this->redirectUrl(strlen($query) > 0 ? $travellerUri . '?' . $query : $travellerUri);
		}

		$this->error(sprintf("%s parameter is empty", self::TRAVELLER_ENV), 400);
	}

	/**
	 * Logout handler
	 * @throws Nette\Application\AbortException
	 */
	public function handleLogout(): void
	{
		$this->context->clear();

		if ($this->ssoContext->getUri() !== null) {
			$this->redirectPermanent(':Users:Back:Logout:sso');
		} else {
			$this->redirect('this');
		}
	}
}
