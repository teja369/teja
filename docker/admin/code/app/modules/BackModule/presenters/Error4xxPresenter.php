<?php

declare(strict_types=1);

namespace Cleevio\BackModule\Presenters;

use Nette\Application\UI\ITemplate;
use Nette\Application\UI\Presenter;
use Nette\SmartObject;

class Error4xxPresenter extends Presenter
{

	use SmartObject;

	public function __construct()
	{
		parent::__construct();
	}

	protected function startup()
	{
		parent::startup();

		$this->setLayout(__DIR__ . '/templates/@layout-blank.latte');
	}

	public function beforeRender(): void
	{
		if ($this->getRequest() !== null) {
			switch ($this->getRequest()->getParameter('exception')->getCode()) {

				case 401:
					$this->redirectPermanent(":Users:Back:Login:default");

					break;
				case 403:
					$this->render403();

					break;
				case 404:
					$this->render404();

					break;
				default:
					$this->renderOther(); // cannot name it default because it somehow overrides 404 a displays 500.phtml
			}
		}
	}


	public function render403(): void
	{
		if ($this->template instanceof ITemplate) {
			$this->template->setFile(__DIR__ . '/templates/Error/403.latte');
		}
	}

	public function render404(): void
	{
		if ($this->template instanceof ITemplate) {
			$this->template->setFile(__DIR__ . '/templates/Error/404.latte');
		}
	}


	public function renderOther(): void
	{
		if ($this->template instanceof ITemplate) {
			$this->template->setFile(__DIR__ . '/templates/Error/500.phtml');
		}
	}

}
