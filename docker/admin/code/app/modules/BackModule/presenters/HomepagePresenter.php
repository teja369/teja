<?php

declare(strict_types=1);

namespace Cleevio\BackModule\Presenters;

class HomepagePresenter extends BasePresenter
{

	public function startup()
	{
		parent::startup();

		if ($this->getUser()->isAllowed('admin.questions')) {
			$this->redirect(':Questions:Back:Questions:default');
		} elseif ($this->getUser()->isAllowed('admin.hosts')) {
			$this->redirect(':Hosts:Back:Hosts:default');
		} elseif ($this->getUser()->isAllowed('admin.trips')) {
			$this->redirect(':Trips:Back:Trip:default');
		} elseif ($this->getUser()->isAllowed('admin.countries')) {
			$this->redirect(':Countries:Back:Country:default');
		} elseif ($this->getUser()->isAllowed('admin.localizations')) {
			$this->redirect(':Translations:Back:Homepage:default');
		} else {
			$this->flashMessage("back.forbidden", "danger");
			$this->redirect(':Users:Back:Login:default');
		}
	}

}
