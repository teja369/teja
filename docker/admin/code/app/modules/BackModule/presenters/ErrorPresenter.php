<?php

declare(strict_types=1);

namespace Cleevio\BackModule\Presenters;

use Nette\Application\BadRequestException;
use Nette\Application\Helpers;
use Nette\Application\IResponse;
use Nette\Application\Request;
use Nette\Application\Responses;
use Nette\Application\UI\Presenter;
use Nette\Http;
use Nette\SmartObject;
use Tracy\ILogger;

class ErrorPresenter extends Presenter
{

	use SmartObject;

	/**
	 * @var ILogger
	 */
	private $logger;


	public function __construct(ILogger $logger)
	{
		parent::__construct();
		$this->logger = $logger;
	}


	public function run(Request $request): IResponse
	{
		$exception = $request->getParameter('exception');

		$this->logger->log($exception, ILogger::EXCEPTION);

		if ($exception instanceof BadRequestException) {
			[$module, , $sep] = Helpers::splitName($request->getPresenterName());

			return new Responses\ForwardResponse($request->setPresenterName($module . $sep . 'Error4xx'));
		}

		return new Responses\CallbackResponse(static function (Http\IRequest $httpRequest, Http\IResponse $httpResponse) {
			unset($httpRequest);

			if (file_exists(__DIR__ . '/templates/Error/' . $httpResponse->getCode() . '.phtml')) {
				require __DIR__ . '/templates/Error/' . $httpResponse->getCode() . '.phtml';
			} else {
				require __DIR__ . '/templates/Error/500.phtml';
			}
		});
	}
}
