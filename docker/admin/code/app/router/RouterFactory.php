<?php

declare(strict_types=1);

namespace App;

use Cleevio\Application\Http\Routers\Route;
use Nette;
use Nette\Application\Routers\RouteList;

class RouterFactory
{

	use Nette\StaticClass;

	/**
	 * @return Nette\Application\IRouter
	 */
	public static function createRouter()
	{
		$router = new RouteList;

		$router[] = $frontModule = new RouteList('Back');

		$frontModule[] = new Route('/', 'Homepage:default');

		return $router;
	}

}
