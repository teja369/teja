"use strict";

import naja from 'naja';

const typeSelectId = '#frm-questionForm-completeForm-type';
const validators = {
	"text": {
		"validationRequired": true,
		"attributesIsPrefilled": true,
		"validationMultichoice": false,
		"validationMinimalLength": true,
		"validationMaximalLength": true,
		"validationMinimalValue": false,
		"validationMaximalValue": false,
		"validationRegularExpression": true,
		"attributesValidationHelp": true,
		"attributesHelp": true,
		"attributesPlaceholder": true,
		"attributesVariant" : false
	},
	"number": {
		"validationRequired": true,
		"attributesIsPrefilled": true,
		"validationMultichoice": false,
		"validationMinimalLength": false,
		"validationMaximalLength": false,
		"validationMinimalValue": true,
		"validationMaximalValue": true,
		"validationRegularExpression": true,
		"attributesValidationHelp": true,
		"attributesHelp": true,
		"attributesPlaceholder": true,
		"attributesVariant" : false
	},
	"bool": {
		"validationRequired": true,
		"attributesIsPrefilled": true,
		"validationMultichoice": false,
		"validationMinimalLength": false,
		"validationMaximalLength": false,
		"validationMinimalValue": false,
		"validationMaximalValue": false,
		"validationRegularExpression": true,
		"attributesValidationHelp": true,
		"attributesHelp": true,
		"attributesPlaceholder": false,
		"attributesVariant": false
	},
	"choice": {
		"validationRequired": true,
		"attributesIsPrefilled": true,
		"validationMultichoice": true,
		"validationMinimalLength": false,
		"validationMaximalLength": false,
		"validationMinimalValue": false,
		"validationMaximalValue": false,
		"validationRegularExpression": true,
		"attributesValidationHelp": true,
		"attributesHelp": true,
		"attributesPlaceholder": true,
		"attributesVariant": false
	},
	"datetime": {
		"validationRequired": true,
		"attributesIsPrefilled": true,
		"validationMultichoice": false,
		"validationMinimalLength": false,
		"validationMaximalLength": false,
		"validationMinimalValue": false,
		"validationMaximalValue": false,
		"validationRegularExpression": true,
		"attributesValidationHelp": true,
		"attributesHelp": true,
		"attributesPlaceholder": true,
		"attributesVariant": true
	},
	"country": {
		"validationRequired": true,
		"attributesIsPrefilled": true,
		"validationMultichoice": false,
		"validationMinimalLength": false,
		"validationMaximalLength": false,
		"validationMinimalValue": false,
		"validationMaximalValue": false,
		"validationRegularExpression": true,
		"attributesValidationHelp": true,
		"attributesHelp": true,
		"attributesPlaceholder": true,
		"attributesVariant": false
	}
};

const positioning = {
	init: function () {
		!function () {
			if ($('#sortable').length) {
				$('a.position-edit-button').unbind();
				$('a.position-save-button').unbind();

				$('a.position-edit-button').on('click', function () {
					const itemId = $(this).data('item-id');

					$('div[data-order-new-position="' + itemId + '"]').show();
					$('div[data-order-current-position="' + itemId + '"]').hide();

					$('div[data-order-buttons="' + itemId + '"] a.position-edit-button').hide();
					$('div[data-order-buttons="' + itemId + '"] a.position-save-button').show();
				});

				$('a.position-save-button').on('click', function () {
					const itemId = $(this).data('item-id');

					naja.makeRequest('POST', $(this).data('link'), {position: $('input[data-item-id="'+ itemId +'"]').val()}, {history: false});

					$('div[data-order-new-position="' + itemId + '"]').hide();
					$('div[data-order-current-position="' + itemId + '"]').show();

					$('div[data-order-buttons="' + itemId + '"] a.position-edit-button').show();
					$('div[data-order-buttons="' + itemId + '"] a.position-save-button').hide();
				});
			}
		}();
	}
};

const languageSwitcher = {
	init: function () {
		!function () {
			$('a.language-switcher').on('click', function () {
				let tab = $(this).data('tab');

				$('a.language-switcher').removeClass('active');
				$('a[data-tab="' + tab + '"]').addClass('active');

				$('div.language-holder').hide();
				$('div[data-tab-holder="' + tab + '"]').show();
			});
		}();
	}
};

$(document).ready(function () {
	if ($('#complete').length) {
		refreshByType($(typeSelectId).children("option:selected").val());

		$(typeSelectId).on('change', function () {
			refreshByType($(typeSelectId).children("option:selected").val());
		});
	}

	naja.addEventListener('complete', function () {
		positioning.init();
		languageSwitcher.init();
	});

	positioning.init();
	languageSwitcher.init();
});

function refreshByType(type) {
	$.each(validators[type], function (key, value) {
		if (value === true) {
			$('#' + key).show();
		} else {
			$('#' + key).hide();
		}
	});
}
