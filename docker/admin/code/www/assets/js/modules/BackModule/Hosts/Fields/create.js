"use strict";

import naja from 'naja';

$(document).ready(function () {

    function initFieldParamChange() {

        $("#frm-paramModalForm-form-param").unbind();
        $("#frm-paramModalForm-form-param").change(function () {

            $("#frm-paramModalForm-form").find("input").each(function () {
                $(this).attr('disabled', true);
            });

            naja.makeRequest('GET', someBasicUrl + "&paramModalForm-field=" + $(this).val(), [], {
                history: false
            });

            naja.addEventListener('complete', function () {

                $("#frm-paramModalForm-form").find("input").each(function () {
                    $(this).attr('disabled', false);
                });

                initFieldParamChange();

            });
        })
    }

    initFieldParamChange();


});
