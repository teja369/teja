"use strict";

import naja from 'naja';

$(document).ready(function () {
	$('a.language-switcher').on('click', function () {
		let tab = $(this).data('tab');

		$('a.language-switcher').removeClass('active');
		$('a[data-tab="' + tab + '"]').addClass('active');

		$('div.language-holder').hide();
		$('div[data-tab-holder="' + tab + '"]').show();
	});
});
