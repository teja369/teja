$(document).ready(function () {
	$('.calendar-birthdate').calendar({
		type: 'date',
		ampm: false,
		formatter: {
			date: function (date, settings) {

				if (!date) return '';

				const dateString = ('0' + date.getDate()).slice(-2) + '.'
					+ ('0' + (date.getMonth() + 1)).slice(-2) + '.'
					+ date.getFullYear();

				return dateString;
			}
		}
	});
});