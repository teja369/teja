"use strict";

import naja from 'naja';

let typingTimer;
const doneTypingInterval = 500;

$(document).ready(function () {
	$('.language-input').on({
		keyup: function () {
			clearTimeout(typingTimer);

			let element = $(this);
			if (element.val()) {
				typingTimer = setTimeout(function () {
					naja.makeRequest('POST', element.data('save-link'), {
						'value': element.val(),
						'language': element.data('language'),
						'key': element.data('key')
					}, {'history': false});

					$('div[data-confirmer-key="' + element.data('key') + '"]').show().delay(1000).fadeOut(100);
					$('div[data-holder-key="' + element.data('key') + '"]').removeClass('border-danger');
					$('div[data-error-key="' + element.data('key') + '"]').hide();
				}, doneTypingInterval);
			} else {
				$('div[data-holder-key="' + element.data('key') + '"]').addClass('border-danger');
				$('div[data-error-key="' + element.data('key') + '"]').show();
			}
		}
	});
});
