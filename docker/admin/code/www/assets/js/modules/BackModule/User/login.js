import $ from 'jquery'
import naja from 'naja'

$.fn.transition = require('semantic-ui-transition');

$(document).on('click', '#reset-password-form-link', function () {
	$('#login-form-wrapper').transition({
		animation: 'fade left',
		onComplete: function () {
			$('#reset-password-form-wrapper').transition('fade right');
		}
	});
});

$(document).on('click', '#login-form-link', function () {
	$('#reset-password-form-wrapper').transition({
		animation: 'fade right',
		onComplete: function () {
			$('#login-form-wrapper').transition('fade left');
		}
	});
});

$(document).on('change', '#language-selector', function () {
	naja.makeRequest('GET', urlLanguageChange, {iso: $(this).val()}, {history: false});
});