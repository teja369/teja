import $ from 'jquery'
import naja from "naja";

function open(element) {
	element.modal({});
}

function ModalExtension(naja) {
	naja.addEventListener('init', () => {
		$('.modal[id^="snippet-"]').each(function () {
			const content = $(this).find('.modal-content');
			if (!content.length) {
				return;
			}

			open($(this));
		});
	});

	naja.snippetHandler.addEventListener('afterUpdate', (event) => {
		const element = $(event.snippet);

		if (!element.find('.modal')) {
			return;
		}

		const content = element.find('.modal-content');
		if (!content.length) {
			return;
		}

		open(element);
	});

	return this;
}

naja.registerExtension(ModalExtension);
