"use strict";

import naja from 'naja';

const positioning = {
	init: function () {
		!function () {
			if ($('#sortable').length) {
				$('a.position-edit-button').unbind();
				$('a.position-save-button').unbind();

				$('a.position-edit-button').on('click', function () {
					const itemId = $(this).data('item-id');

					$('div[data-order-new-position="' + itemId + '"]').show();
					$('div[data-order-current-position="' + itemId + '"]').hide();

					$('div[data-order-buttons="' + itemId + '"] a.position-edit-button').hide();
					$('div[data-order-buttons="' + itemId + '"] a.position-save-button').show();
				});

				$('a.position-save-button').on('click', function () {
					const itemId = $(this).data('item-id');

					naja.makeRequest('POST', $(this).data('link'), {position: $('input[data-item-id="'+ itemId +'"]').val()}, {history: false});

					$('div[data-order-new-position="' + itemId + '"]').hide();
					$('div[data-order-current-position="' + itemId + '"]').show();

					$('div[data-order-buttons="' + itemId + '"] a.position-edit-button').show();
					$('div[data-order-buttons="' + itemId + '"] a.position-save-button').hide();
				});
			}
		}();
	}
};


$(document).ready(function () {
	naja.addEventListener('complete', function () {
		positioning.init();
	});

	positioning.init();
});
