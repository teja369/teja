import $ from 'jquery'

$.fn.transition = require('semantic-ui-transition');
$.fn.dropdown = require('semantic-ui-dropdown');
$.fn.search = require('semantic-ui-search');
$.fn.sticky = require('semantic-ui-sticky');
$.fn.dimmer = require('semantic-ui-dimmer');
$.fn.sidebar = require('semantic-ui-sidebar');
$.fn.popup = require('semantic-ui-popup');
$.fn.modal = require('semantic-ui-modal');

$('.dropdown').dropdown();

$('.menu .ui.sticky').sticky({
	silent: true,
	container: $('html'),
	context: $('.pusher > .content-wrapper')
});

// main sidebar
$('#toc').sidebar({
	dimPage: true,
	transition: 'overlay',
	mobileTransition: 'uncover'
});

$('.ui.modal').modal('show');

// launch buttons
$('#toc').sidebar('attach events', '.launch.button, .view-ui, .launch.item');