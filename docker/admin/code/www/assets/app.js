// Dependencies
import 'jquery';
import 'vanilla-lazyload';
import 'bootstrap';

// Font
import './css/font-awesome.css';
import './js/font-awesome';

// Dashkit Stuff
import './dashkit/libs/select2/dist/css/select2.min.css';
import './dashkit/fonts/feather/feather.css';
import './dashkit/css/theme.css'
import './dashkit/js/theme';

// Dashkit Extensions
import './css/backend/custom.admin.less';
import './css/backend/switch.admin.less';
import './css/backend/flags.admin.less';
import './css/backend/selectedCountries.admin.less';

// Application dependencies
import './js/naja';
import './js/lazyLoad';

// Calendar
import 'semantic-ui-calendar/dist/calendar';
import 'semantic-ui-calendar/dist/calendar.css';

// Extensions
import './js/modules/BackModule/extensions/ajax.dimmer';
import './js/modules/BackModule/extensions/ajax.modal';

// Modules dependencies
import './js/modules/BackModule/User/login.js';
import './js/modules/BackModule/User/profile.js';
import $ from "jquery";
