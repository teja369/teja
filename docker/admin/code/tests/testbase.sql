-- Languages
INSERT INTO `languages` (`id`, `name`, `iso`, `flag`) VALUES
(1,	'Czech',	'cs',	'cz'),
(2,	'English',	'en',	'gb');