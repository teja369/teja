<?php

declare(strict_types=1);

namespace Migrations;

use Cleevio\Migrations\Migration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20191121124223 extends Migration
{
    public function getDescription() : string
    {
        return 'translations for assistant section';
    }

    public function up(Schema $schema) : void
    {
        $this->createTranslation('trips.dashboard.link.assistant', 'Assistants');
        $this->createTranslation('trips.dashboard.header.assistant', 'Assistants');
        $this->createTranslation('users.assistants.header', 'Add your Assistant');
        $this->createTranslation('users.assistants.intro', 'Assistant can help you with almost anything regarding trips. You can also be asked to become assistant for someone else.');
        $this->createTranslation('users.request-assistant.email.label', 'Email');
        $this->createTranslation('users.request-assistant.submit.label', 'Send');
        $this->createTranslation('users.assistants.assistants-list', 'Your Assistants');
        $this->createTranslation('users.assistants.requests-list', 'Request for assistance');
        $this->createTranslation('users.assistants.show-all', 'SHOW ALL');
        $this->createTranslation('users.assistants.table.delete', 'Delete');
        $this->createTranslation('users.assistants.travellers-list', 'I assist to');
        $this->createTranslation('users.assistants.table.header.no-users', 'Assistants');
        $this->createTranslation('users.assistants.back-to-top.button', 'Back to top');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs

    }
}
