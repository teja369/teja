<?php

declare(strict_types=1);

namespace Migrations;

use Cleevio\Migrations\Migration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20191106151443 extends Migration
{
    public function getDescription() : string
    {
        return 'dropping isEnabled adding visibility enum';
    }

    public function up(Schema $schema) : void
    {

        $this->addSql("ALTER TABLE `questions` ADD COLUMN `visibility` VARCHAR(255) NOT NULL DEFAULT 'disabled' AFTER `variant`, DROP is_enabled");

		$this->createTranslation('questions.list.visibility.disabled','Disabled');
		$this->createTranslation('questions.list.visibility.enabled','Enabled');
		$this->createTranslation('questions.list.visibility.preview','Preview');
		$this->createTranslation('questions.visibility.success','Visibility changed');
		$this->createTranslation('questions.table.state','State');
    }

    public function down(Schema $schema) : void
    {
        $this->addSql('ALTER TABLE questions ADD is_enabled TINYINT(1) NOT NULL, DROP visibility');

    }
}
