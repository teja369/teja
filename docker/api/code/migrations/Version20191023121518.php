<?php

declare(strict_types=1);

namespace Migrations;

use Cleevio\Migrations\Migration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20191023121518 extends Migration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->createTranslation('questions.table.deleted', 'Deleted');
        $this->createTranslation('questions.list.link.delete.confirmation.children', 'Are you sure you want to delete question? It has depending questions which will no longer be shown to users');
        $this->createTranslation('trips.table.header.actions', 'Actions');
        $this->createTranslation('trips.table.link.detail', 'Detail');
        $this->createTranslation('questions.form.edit.headline', 'Edit question');
    }

    public function down(Schema $schema) : void
    {
    }
}
