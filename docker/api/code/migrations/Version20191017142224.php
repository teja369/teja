<?php

declare(strict_types=1);

namespace Migrations;

use Cleevio\Migrations\Migration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20191017142224 extends Migration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
		$this->createTranslation('filter', 'Filter');
		$this->createTranslation('filter.button', 'Filter');
		$this->createTranslation('translations.filter.search', 'Search');
		$this->createTranslation('translations.filter.state', 'State');
		$this->createTranslation('translations.filter.state.not-translated', 'Not translated');
		$this->createTranslation('translations.filter.state.translated', 'Translated');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs

    }
}
