<?php

declare(strict_types=1);

namespace Migrations;

use Cleevio\Migrations\Migration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20191016092746 extends Migration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        $this->createTranslation('form.field.button.filter', 'Submit');
        $this->createTranslation('trips.filter', 'Filter');
        $this->createTranslation('trips.filter.country', 'Country');
        $this->createTranslation('trips.filter.state', 'State');
        $this->createTranslation('trips.filter.registration', 'Registration');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs

    }
}
