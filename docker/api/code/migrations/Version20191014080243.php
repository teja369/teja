<?php

declare(strict_types=1);

namespace Migrations;

use Cleevio\Migrations\Migration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20191014080243 extends Migration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
    	$this->createTranslation('hosts.search.form.field.button.search', 'Search');
		$this->createTranslation('hosts.fields.form.label.submit', 'Submit');
		$this->createTranslation('hosts.form.field.label.submit', 'Submit');
		$this->createTranslation('hosts.params.form.label.submit', 'Submit');
		$this->createTranslation('languages.form.label.submit', 'Submit');
		$this->createTranslation('translations.modal.label.submit', 'Submit');
		$this->createTranslation('users.login.default.sign-in.form.label.submit', 'Submit');
		$this->createTranslation('languages.translations.form.label.submit', 'Submit');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs

    }
}
