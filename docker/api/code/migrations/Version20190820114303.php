<?php

declare(strict_types=1);

namespace Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190820114303 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE `questions` 
                            ADD COLUMN `multi_choice` TINYINT(1) DEFAULT NULL AFTER `max_value`;
                            ');

        $this->addSql('CREATE TABLE `questions_option` (
                              `id` INT NOT NULL AUTO_INCREMENT,
                              `text` VARCHAR(255) NOT NULL,
                              `question_id` INT NOT NULL,
                              PRIMARY KEY (`id`)) DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;
                            ');
        
        $this->addSql('ALTER TABLE `questions_option` 
                            ADD INDEX `fk_questions_id_idx` (`question_id` ASC);
                            ALTER TABLE `questions_option` 
                            ADD CONSTRAINT `fk_questions_id`
                              FOREIGN KEY (`question_id`)
                              REFERENCES `questions` (`id`)
                              ON DELETE CASCADE
                              ON UPDATE CASCADE,
                             DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;
                            ');

        $this->addSql('CREATE TABLE `questions_option_translation` (
                          `locale` varchar(10) NOT NULL DEFAULT \'en\',
                          `field` varchar(255) NOT NULL,
                          `content` varchar(255) NOT NULL,
                          `object_id` int(10) NOT NULL,
                          PRIMARY KEY (`locale`)) DEFAULT CHARACTER SET UTF8 COLLATE utf8_general_ci ENGINE = InnoDB;
        ');

        $this->addSql('ALTER TABLE `questions_option_translation` ADD INDEX `index` (`object_id` ASC);');
        $this->addSql('ALTER TABLE `questions_option_translation` 
                            ADD CONSTRAINT `fk_question_option`
                              FOREIGN KEY (`object_id`)
                              REFERENCES `questions_option` (`id`)
                              ON DELETE CASCADE
                              ON UPDATE CASCADE;');
    }

    public function down(Schema $schema): void
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE `questions` 
                            DROP COLUMN `multi_choice`;
                            ');
        $this->addSql('DROP TABLE `questions_option_translation`');
        $this->addSql('DROP TABLE `questions_option`');
    }
}
