<?php

declare(strict_types=1);

namespace Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190820082643 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE `questions` ADD COLUMN `validation` VARCHAR(255) NULL DEFAULT NULL AFTER `help`;');
        $this->addSql('ALTER TABLE `questions` ADD COLUMN `min_length` INT NULL DEFAULT NULL AFTER `is_prefilled`;');
        $this->addSql('ALTER TABLE `questions` ADD COLUMN `max_length` INT NULL DEFAULT NULL AFTER `min_length`;');
    }

    public function down(Schema $schema) : void
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE `questions` DROP COLUMN `validation`;');
        $this->addSql('ALTER TABLE `questions` DROP COLUMN `min_length`;');
        $this->addSql('ALTER TABLE `questions` DROP COLUMN `max_length`;');
    }
}
