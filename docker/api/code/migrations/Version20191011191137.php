<?php

declare(strict_types=1);

namespace Migrations;

use Cleevio\Migrations\Migration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20191011191137 extends Migration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
    	$this->addSql('ALTER TABLE `registrations` ADD UNIQUE INDEX `uq_name` (`name` ASC);');

		$this->createRegistration('A1');
		$this->createRegistration('PWD');

    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs

    }
}
