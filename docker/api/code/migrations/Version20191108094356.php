<?php

declare(strict_types=1);

namespace Migrations;

use Cleevio\Migrations\Migration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20191108094356 extends Migration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        $this->createTranslation('trips.trip.onHold.email.subject', 'Your trip has been put on hold');
        $this->createTranslation('trips.trip.onHold.email.message', 'Reason: %message%');

    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs

    }
}
