<?php

declare(strict_types=1);

namespace Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190814141841 extends AbstractMigration
{

	public function getDescription(): string
	{
		return '';
	}


	public function up(Schema $schema): void
	{
		$this->addSql('CREATE TABLE fields (id INT AUTO_INCREMENT NOT NULL, type VARCHAR(255) NOT NULL, 
			name VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT  CHARSET=utf8 COLLATE=utf8_general_ci ENGINE = InnoDB;');
		$this->addSql('CREATE TABLE fields_translation (id INT AUTO_INCREMENT NOT NULL, object_id INT DEFAULT NULL, 
			locale VARCHAR(8) NOT NULL, field VARCHAR(32) NOT NULL, content LONGTEXT DEFAULT NULL, 
			INDEX IDX_DB62F55B232D562B (object_id), PRIMARY KEY(id)) DEFAULT  CHARSET=utf8 COLLATE=utf8_general_ci ENGINE = InnoDB;');
		$this->addSql('ALTER TABLE fields_translation ADD CONSTRAINT FK_DB62F55B232D562B FOREIGN KEY (object_id) 
			REFERENCES fields (id) ON DELETE CASCADE;');

		$this->addSql('CREATE TABLE host_fields (id INT AUTO_INCREMENT NOT NULL, field_id INT NOT NULL, 
			country_id INT DEFAULT NULL, INDEX IDX_5AF22263443707B0 (field_id), INDEX IDX_5AF22263F92F3E70 (country_id), 
			PRIMARY KEY(id)) DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci ENGINE = InnoDB;');
		$this->addSql('ALTER TABLE host_fields ADD CONSTRAINT FK_5AF22263443707B0 FOREIGN KEY (field_id) 
			REFERENCES fields (id);');
		$this->addSql('ALTER TABLE host_fields ADD CONSTRAINT FK_5AF22263F92F3E70 FOREIGN KEY (country_id) 
			REFERENCES countries (id);');
	}


	public function down(Schema $schema): void
	{
		$this->addSql('DROP TABLE `host_fields`;');
		$this->addSql('DROP TABLE `fields_translation`;');
		$this->addSql('DROP TABLE `fields`;');
	}
}
