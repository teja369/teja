<?php

declare(strict_types=1);

namespace Migrations;

use Cleevio\Migrations\Migration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20191031113645 extends Migration
{
    public function getDescription(): string
    {
        return 'translations for issue form';
    }

    public function up(Schema $schema): void
    {
        $this->createTranslation('trips.dashboard.issue.message-sent.success', 'Your message was successfuly sent');
        $this->createTranslation('issues.modal.headline', 'Report an Issue');
        $this->createTranslation('issues.modal.description', 'Report your problem in the form below and we will try to solve it as soon as possible.');
        $this->createTranslation('issues.modal.button.close', 'Close');
        $this->createTranslation('issues.modal.input.title.label', 'Title');
        $this->createTranslation('issues.modal.input.title.placeholder', 'Title');
        $this->createTranslation('issues.modal.input.title.required', 'Title is required');
        $this->createTranslation('issues.modal.input.message.label', 'Message');
        $this->createTranslation('issues.modal.input.message.placeholder', 'Type here...');
        $this->createTranslation('issues.modal.input.message.required', 'Message is required');
        $this->createTranslation('issues.modal.input.send.label', 'Send');
        $this->createTranslation('issues.modal.form.error.unknown', 'Unknown error');
        $this->createTranslation('issues.issue.error.email', 'Issue email could not be send');

    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs

    }
}
