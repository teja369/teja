<?php

declare(strict_types=1);

namespace Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190819072354 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE `purposes` 
                            DROP FOREIGN KEY `fk_country`;
                            ALTER TABLE `purposes` 
                            DROP COLUMN `country_id`,
                            DROP INDEX `fk_country_idx` ;
                            ');

        $this->addSql('CREATE TABLE `purposes_country` (
                              `id` INT NOT NULL AUTO_INCREMENT,
                              `purpose_id` INT NOT NULL,
                              `country_id` INT NOT NULL,
                              PRIMARY KEY (`id`)) 
                              DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;
                            ');

        $this->addSql('ALTER TABLE `purposes_country` 
                            ADD INDEX `fk_countries_idx` (`country_id` ASC);
                            ALTER TABLE `purposes_country` 
                            ADD CONSTRAINT `fk_countries`
                              FOREIGN KEY (`country_id`)
                              REFERENCES `countries` (`id`)
                              ON DELETE RESTRICT
                              ON UPDATE RESTRICT;
                            ');

        $this->addSql('ALTER TABLE `purposes_country` 
                            ADD INDEX `fk_purposes_idx` (`purpose_id` ASC);
                            ALTER TABLE `purposes_country` 
                            ADD CONSTRAINT `fk_purposes`
                              FOREIGN KEY (`purpose_id`)
                              REFERENCES `purposes` (`id`)
                              ON DELETE CASCADE
                              ON UPDATE CASCADE;
                            ');
    }

    public function down(Schema $schema) : void
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE `purposes_country`');

        $this->addSql('ALTER TABLE `purposes` 
                            ADD COLUMN `country_id` INT(45) NULL DEFAULT NULL AFTER `name`,
                            ADD INDEX `fk_country_id_idx` (`country_id` ASC);
                            ALTER TABLE `purposes` 
                            ADD CONSTRAINT `fk_country_id`
                              FOREIGN KEY (`country_id`)
                              REFERENCES `countries` (`id`)
                              ON DELETE RESTRICT
                              ON UPDATE RESTRICT;
                            ');
    }
}
