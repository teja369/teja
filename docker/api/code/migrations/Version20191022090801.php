<?php

declare(strict_types=1);

namespace Migrations;

use Cleevio\Migrations\Migration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20191022090801 extends Migration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
		$this->createTranslation('menu.address', 'Address');
		$this->createTranslation('hosts.address-fields', 'Address fields');
		$this->createTranslation('hosts.address-fields.available', 'Available address fields');
		$this->createTranslation('hosts.address-fields.add', 'Add address fields');
		$this->createTranslation('hosts.address-fields.name', 'Name');
		$this->createTranslation('hosts.address-fields.type', 'Type');
		$this->createTranslation('hosts.address-fields.order', 'Order');
		$this->createTranslation('hosts.address-fields.actions', 'Actions');
		$this->createTranslation('hosts.address-fields.delete', 'Delete');
		$this->createTranslation('hosts.address-fields.delete.confirmation', 'Delete address field?');
		$this->createTranslation('hosts.address-fields.modal.heading', 'Available fields');
		$this->createTranslation('hosts.address-fields.modal.name', 'Name');
		$this->createTranslation('hosts.address-fields.modal.type', 'Type');
		$this->createTranslation('hosts.address-fields.modal.delete', 'Delete');
		$this->createTranslation('hosts.address-fields.modal.add', 'Add');
		$this->createTranslation('hosts.address-fields.modal.close', 'Close');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs

    }
}
