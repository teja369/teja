<?php

declare(strict_types=1);

namespace Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190815130756 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE `trips` ADD COLUMN `host_id` INT(11) NULL DEFAULT NULL AFTER `user_id`');
        $this->addSql('ALTER TABLE `trips` 
                            ADD INDEX `fk_host_id_idx` (`host_id` ASC);
                            ALTER TABLE `trips` 
                            ADD CONSTRAINT `fk_host_id`
                              FOREIGN KEY (`host_id`)
                              REFERENCES `hosts` (`id`)
                              ON DELETE RESTRICT
                              ON UPDATE RESTRICT;');
                                }

    public function down(Schema $schema) : void
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE `trips` DROP COLUMN `host_id`');
    }
}
