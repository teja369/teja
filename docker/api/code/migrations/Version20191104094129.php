<?php

declare(strict_types=1);

namespace Migrations;

use Cleevio\Migrations\Migration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20191104094129 extends Migration
{
    public function getDescription() : string
    {
        return 'forgotten headling translation in trip create registration screen';
    }

    public function up(Schema $schema) : void
    {
		$this->createTranslation('trips.create-trip.create-registrations.heading', 'Details overview');
		$this->createTranslation('trips.create-trip.create-registrations.required', 'Required');
		$this->createTranslation('trips.create-trip.create-registrations.not-required', 'Not required');

    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs

    }
}
