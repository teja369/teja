<?php

declare(strict_types=1);

namespace Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190826143926 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE `countries_translation` DROP PRIMARY KEY;');

        $this->addSql('ALTER TABLE `countries_translation` 
                            ADD COLUMN `id` INT(11) NOT NULL AUTO_INCREMENT FIRST,
                            ADD PRIMARY KEY (`id`);
                            ');

        $this->addSql('ALTER TABLE `purposes_translation` DROP PRIMARY KEY;');

        $this->addSql('ALTER TABLE `purposes_translation` 
                            ADD COLUMN `id` INT(11) NOT NULL AUTO_INCREMENT FIRST,
                            ADD PRIMARY KEY (`id`);
                            ');

        $this->addSql('ALTER TABLE `questions_option_translation` DROP PRIMARY KEY;');

        $this->addSql('ALTER TABLE `questions_option_translation` 
                            ADD COLUMN `id` INT(11) NOT NULL AUTO_INCREMENT FIRST,
                            ADD PRIMARY KEY (`id`);
                            ');
    }

    public function down(Schema $schema): void
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE `countries_translation` DROP COLUMN `id`;');
        $this->addSql('ALTER TABLE `purposes_translation` DROP COLUMN `id`;');
        $this->addSql('ALTER TABLE `questions_option_translation` DROP COLUMN `id`;');
    }
}
