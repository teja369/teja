<?php

declare(strict_types=1);

namespace Migrations;

use Cleevio\Migrations\Migration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20191111133708 extends Migration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        $this->createTranslation('error-403.title', '403');
        $this->createTranslation('error-403.header', 'Forbidden');
        $this->createTranslation('error-403.message', 'Access to the requested resource is forbidden.');
        $this->createTranslation('error-403.link.home', 'Go Home.');

    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs

    }
}
