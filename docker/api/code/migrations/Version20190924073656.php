<?php

declare(strict_types=1);

namespace Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190924073656 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE questions_option_country (id INT AUTO_INCREMENT NOT NULL, country_id INT NOT NULL, 
			option_id INT NOT NULL, INDEX IDX_20F6E8A0F92F3E70 (country_id), INDEX IDX_20F6E8A01E27F6BF (option_id), 
			PRIMARY KEY(id)) DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci ENGINE = InnoDB;');

        $this->addSql('ALTER TABLE `questions_option_country` 
                            ADD CONSTRAINT `fk_questions_option_country`
                              FOREIGN KEY (`country_id`)
                              REFERENCES `countries` (`id`)
                              ON DELETE RESTRICT
                              ON UPDATE RESTRICT;');

        $this->addSql('ALTER TABLE `questions_option_country` 
                            ADD CONSTRAINT `fk_questions_option_option`
                              FOREIGN KEY (`option_id`)
                              REFERENCES `questions_option` (`id`)
                              ON DELETE CASCADE
                              ON UPDATE CASCADE;');
    }

    public function down(Schema $schema) : void
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE `questions_option_country`');
    }
}
