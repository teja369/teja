<?php

declare(strict_types=1);

namespace Migrations;

use Cleevio\Migrations\Migration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20191024102814 extends Migration
{
    public function getDescription() : string
    {
        return 'translations for admin section descriptions';
    }

    public function up(Schema $schema) : void
    {
      	$this->addSql('ALTER TABLE translations MODIFY `value` TEXT;');
        $this->createTranslation('questions.description', 'In this section you can create new questions, as well as manage the already existing questions displayed within the traveller interface of NIUMAD. Every question must have a unique identifier – Question key.');
        $this->createTranslation('questions.groups.description', 'This section allows structuring of questions into a logically interconnected clusters, e.g. host address related questions, home representative related questions etc. Each of the groups is displayed on a separate page in the traveller NIUMAD interface. The two major group types are “purpose”- for exemption engine to decide whether the PWD registration is required, and “detail” – questions dependent or PWD being required.');
        $this->createTranslation('hosts.fields.description', 'In this section fields names for host companies can be created and managed. The fields specify details of host companies and serve the same purpose as column names in Genesis file.');
        $this->createTranslation('hosts.description', 'Section displays the list of all the hosts in all the countries. Here you can manage existing hosts or create a new one if necessary. Once you click on “Edit”, you can see and update all the parameters of the selected host. If you need additional parameter for the Host, you can create it in the Hosts field section.');
        $this->createTranslation('trips.description', 'This section contains a list of all the trips and their details created by travellers. It allows you to keep track of all the actions that occurred in traveller NIUMAD interface.');
        $this->createTranslation('countries.description', 'This section contains a list of all the countries present in NIUMAD. When the country is disabled, the traveller will not be able to create a trip to the such country.');
        $this->createTranslation('languages.description', 'This section contains a list of languages available within NIUMAD. A new language can be created or an existing language can be deleted here. In case of creation of a new language, an option to translate the individual questions, countries and traveller NIUMAD interface placeholders will be added into the whole ADMIN NIUMAD interface.');
        $this->createTranslation('translations.description', 'This section allows you to translate the NIUMAD traveller interface to available languages. You can search the translations based on key words or filter them based on their translation state.');
        $this->createTranslation('hosts.address.description', 'This section allows you to choose fields which will be associated with addresses.');


    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs

    }
}
