<?php

declare(strict_types=1);

namespace Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190826072225 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE `questions` 
                            ADD COLUMN `parent_id` INT(11) NULL DEFAULT NULL AFTER `end_date`,
                            ADD INDEX `fk_parent_id_idx` (`parent_id` ASC);
                            ALTER TABLE `questions` 
                            ADD CONSTRAINT `fk_parent_id`
                              FOREIGN KEY (`parent_id`)
                              REFERENCES `questions` (`id`)
                              ON DELETE CASCADE
                              ON UPDATE CASCADE;
                            ');
        $this->addSql('ALTER TABLE `questions` ADD COLUMN `parent_rule` VARCHAR(255) NULL DEFAULT NULL AFTER `parent_id`;');
    }

    public function down(Schema $schema): void
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE `fields` DROP COLUMN `parent_id`;');
        $this->addSql('ALTER TABLE `fields` DROP COLUMN `parent_rule`;');
    }
}
