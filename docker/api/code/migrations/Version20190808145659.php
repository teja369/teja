<?php

declare(strict_types=1);

namespace Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190808145659 extends AbstractMigration
{

	public function getDescription(): string
	{
		return '';
	}


	public function up(Schema $schema): void
	{
		$this->addSql('ALTER TABLE trips ADD country_id INT NOT NULL, DROP country;');
		$this->addSql('ALTER TABLE trips ADD CONSTRAINT FK_AA7370DAF92F3E70 
		FOREIGN KEY (country_id) REFERENCES countries (id);');
	}


	public function down(Schema $schema): void
	{
		$this->addSql('ALTER TABLE `trips` DROP FOREIGN KEY `country_id`;');
		$this->addSql('ALTER TABLE `trips` DROP INDEX UNIQ_AA7370DAF92F3E70;');
		$this->addSql('ALTER TABLE `trips` ADD country VARCHAR(255) NOT NULL;');

	}
}
