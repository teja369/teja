<?php

declare(strict_types=1);

namespace Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190812123923 extends AbstractMigration
{

	public function getDescription(): string
	{
		return '';
	}


	public function up(Schema $schema): void
	{
		$this->addSql('ALTER TABLE trips ADD user_id INT DEFAULT NULL;');
		$this->addSql('ALTER TABLE trips ADD CONSTRAINT FK_AA7370DAA76ED395 FOREIGN KEY (user_id) REFERENCES users (id);');
		$this->addSql('CREATE INDEX IDX_AA7370DAA76ED395 ON trips (user_id);');

	}


	public function down(Schema $schema): void
	{
		$this->addSql('ALTER TABLE `trips` DROP FOREIGN KEY `user_id`;');
		$this->addSql('ALTER TABLE `trips` DROP INDEX `IDX_AA7370DAA76ED395`;');
	}
}
