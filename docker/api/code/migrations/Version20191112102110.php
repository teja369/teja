<?php

declare(strict_types=1);

namespace Migrations;

use Cleevio\Acl\Models\IPrivilege;
use Cleevio\Migrations\Migration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20191112102110 extends Migration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        $this->createRole("robot");

        $this->createResource("robot");

        $this->createPrivilege("robot", "robot", IPrivilege::PRIVILEGE_ALL, true);

    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs

    }
}
