<?php

declare(strict_types=1);

namespace Migrations;

use Cleevio\Migrations\Migration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20191011185548 extends Migration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
		$this->createCountry('DE', 'Germany', 'germany');
		$this->createCountry('CZ', 'Czechia', 'czechia');
		$this->createCountry('AU', 'Austria', 'austria');
		$this->createCountry('SE', 'Sweden', 'sweden');
		$this->createCountry('SK', 'Slovakia', 'slovakia');
		$this->createCountry('IT', 'Italy', 'italy');
		$this->createCountry('ES', 'Spain', 'spain');
		$this->createCountry('PT', 'Portugal', 'portugal');
		$this->createCountry('FR', 'France', 'france');
		$this->createCountry('GR', 'Greece', 'greece');
		$this->createCountry('NL', 'Netherlands', 'netherlands');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs

    }
}
