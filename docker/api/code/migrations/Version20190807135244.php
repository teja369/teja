<?php

declare(strict_types=1);

namespace Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190807135244 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
		$this->addSql('ALTER TABLE users CHANGE email email VARCHAR(255) DEFAULT NULL;');
		$this->addSql('ALTER TABLE users ADD username VARCHAR(255) NOT NULL;');
		$this->addSql('CREATE UNIQUE INDEX UNIQ_1483A5E9E7927C74 ON users (email);');
		$this->addSql('CREATE UNIQUE INDEX UNIQ_1483A5E9F85E0677 ON users (username);');

    }

    public function down(Schema $schema) : void
    {
		$this->addSql('ALTER TABLE users CHANGE email email VARCHAR(255) NOT NULL;');
		$this->addSql('ALTER TABLE "users" DROP "username";');
		$this->addSql('DROP INDEX UNIQ_1483A5E9E7927C74');
		$this->addSql('DROP INDEX UNIQ_1483A5E9F85E0677');


    }
}
