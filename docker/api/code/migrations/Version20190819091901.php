<?php

declare(strict_types=1);

namespace Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190819091901 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE `trips_registration` (
                              `id` INT NOT NULL AUTO_INCREMENT,
                              `trip_id` INT NOT NULL,
                              `registration_id` INT NOT NULL,
                              PRIMARY KEY (`id`),
                              INDEX `ix_trip_1` (`trip_id` ASC),
                              INDEX `ix_registration_1` (`registration_id` ASC),
                              CONSTRAINT `fk_trips_1`
                                FOREIGN KEY (`trip_id`)
                                REFERENCES `trips` (`id`)
                                ON DELETE CASCADE
                                ON UPDATE CASCADE,
                              CONSTRAINT `fk_registration_1`
                                FOREIGN KEY (`registration_id`)
                                REFERENCES `registrations` (`id`)
                                ON DELETE RESTRICT
                                ON UPDATE RESTRICT)
                              DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;
                            ');

        $this->addSql('ALTER TABLE `trips_registration` 
                            ADD UNIQUE INDEX `uq_trip_registration` (`registration_id` ASC, `trip_id` ASC);
                            ');

        $this->addSql('ALTER TABLE `trips` 
                            ADD COLUMN `registration_check` DATETIME NULL DEFAULT NULL AFTER `host_id`;
                            ');
    }

    public function down(Schema $schema) : void
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE `trips_registration`');
    }
}
