<?php

declare(strict_types=1);

namespace Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190809120003 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        $this->addSql('ALTER TABLE `trips` 
                            CHANGE COLUMN `start` `start` DATE NOT NULL ,
                            CHANGE COLUMN `end` `end` DATE NOT NULL ;
                            ');
    }

    public function down(Schema $schema) : void
    {
        $this->addSql('ALTER TABLE `trips` 
                            CHANGE COLUMN `start` `start` DATETIME NOT NULL ,
                            CHANGE COLUMN `end` `end` DATETIME NOT NULL ;
                            ');
    }
}
