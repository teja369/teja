<?php

declare(strict_types=1);

namespace Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190813073823 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE `purposes` (
                `id` INT NOT NULL AUTO_INCREMENT,
                `name` VARCHAR(255) NOT NULL,
                PRIMARY KEY(id)) DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci ENGINE = InnoDB;
        ');

        $this->addSql('CREATE TABLE `purposes_translation` (
                          `locale` varchar(10) NOT NULL DEFAULT \'en\',
                          `field` varchar(255) NOT NULL,
                          `content` varchar(255) NOT NULL,
                          `object_id` int(10) NOT NULL,
                          PRIMARY KEY (`locale`)) DEFAULT CHARACTER SET UTF8 COLLATE utf8_general_ci ENGINE = InnoDB;
        ');

        $this->addSql('ALTER TABLE `purposes_translation` ADD INDEX `index` (`object_id` ASC);');
        $this->addSql('ALTER TABLE `purposes_translation` 
                            ADD CONSTRAINT `fk_purposes`
                              FOREIGN KEY (`object_id`)
                              REFERENCES `purposes` (`id`)
                              ON DELETE CASCADE
                              ON UPDATE CASCADE;');

    }

    public function down(Schema $schema) : void
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE `purposes_translation`');
        $this->addSql('DROP TABLE `purposes`');
    }
}
