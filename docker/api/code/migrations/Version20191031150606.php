<?php

declare(strict_types=1);

namespace Migrations;

use Cleevio\Migrations\Migration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20191031150606 extends Migration
{
    public function getDescription() : string
    {
        return '404 translations';
    }

    public function up(Schema $schema) : void
    {
        $this->createTranslation('error-404.title', '404');
        $this->createTranslation('error-404.header', 'Page not found');
        $this->createTranslation('error-404.message', 'The Page you are looking for doesn’t exist or an other error occurred.');
        $this->createTranslation('error-404.link.home', 'Go Home.');

    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs

    }
}
