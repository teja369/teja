<?php

declare(strict_types=1);

namespace Migrations;

use Cleevio\Migrations\Migration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20191105153258 extends Migration
{
    public function getDescription() : string
    {
        return 'added country column referencing country in country answer';
    }

    public function up(Schema $schema) : void
    {
		$this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

		$this->addSql('ALTER TABLE answers ADD country_id INT DEFAULT NULL;');
		$this->addSql('ALTER TABLE answers ADD CONSTRAINT FK_50D0C606F92F3E70 FOREIGN KEY (country_id) REFERENCES countries (id);');
		$this->addSql('CREATE INDEX IDX_50D0C606F92F3E70 ON answers (country_id);');

		$this->createTranslation('questions.form.label.type.country', 'Country');

    }

    public function down(Schema $schema) : void
    {
		$this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

		$this->addSql('ALTER TABLE `answers` DROP FOREIGN KEY `FK_50D0C606F92F3E70`');
		$this->addSql('ALTER TABLE answers DROP country_id;');

    }
}
