<?php

declare(strict_types=1);

namespace Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190808091046 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE `countries` 
                            ADD COLUMN `code` VARCHAR(2) NOT NULL AFTER `id`,
                            ADD COLUMN `name` VARCHAR(255) NOT NULL AFTER `code`,
                            ADD UNIQUE INDEX `name_UNIQUE` (`name` ASC);');

        $this->addSql('CREATE TABLE `countries_translation` (
                          `locale` varchar(10) NOT NULL DEFAULT \'en\',
                          `field` varchar(255) NOT NULL,
                          `content` varchar(255) NOT NULL,
                          `object_id` int(10) NOT NULL,
                          PRIMARY KEY (`locale`)) DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci ENGINE = InnoDB;
        ');

        $this->addSql('ALTER TABLE `countries_translation` ADD INDEX `index` (`object_id` ASC);');
        $this->addSql('ALTER TABLE `countries_translation` 
                            ADD CONSTRAINT `fk_countries`
                              FOREIGN KEY (`object_id`)
                              REFERENCES `countries` (`id`)
                              ON DELETE CASCADE
                              ON UPDATE CASCADE;');
    }

    public function down(Schema $schema) : void
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE `countries_translation`');
    }
}
