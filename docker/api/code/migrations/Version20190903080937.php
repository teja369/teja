<?php

declare(strict_types=1);

namespace Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190903080937 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE `questions_group` (
                              `id` INT NOT NULL AUTO_INCREMENT,
                              `name` VARCHAR(255) NOT NULL,
                              `type` VARCHAR(255) NOT NULL,
                              PRIMARY KEY (`id`)) 
                              DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci');

        $this->addSql('CREATE TABLE `questions_group_translation` (
                          `locale` varchar(10) NOT NULL DEFAULT \'en\',
                          `field` varchar(255) NOT NULL,
                          `content` varchar(255) NOT NULL,
                          `object_id` int(10) NOT NULL,
                          PRIMARY KEY (`locale`)) DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci ENGINE = InnoDB;
        ');

        $this->addSql('ALTER TABLE `questions_group_translation` ADD INDEX `index` (`object_id` ASC);');
        $this->addSql('ALTER TABLE `questions_group_translation` 
                            ADD CONSTRAINT `fk_questions_group`
                              FOREIGN KEY (`object_id`)
                              REFERENCES `questions_group` (`id`)
                              ON DELETE CASCADE
                              ON UPDATE CASCADE;');

        $this->addSql('ALTER TABLE `questions` CHANGE COLUMN `step` `group_id` INT(11) DEFAULT NULL ;');
        $this->addSql('ALTER TABLE `questions` 
                            ADD INDEX `fk_questions_group_id_idx` (`group_id` ASC);
                            ALTER TABLE `questions` 
                            ADD CONSTRAINT `fk_questions_group_id`
                              FOREIGN KEY (`group_id`)
                              REFERENCES `questions_group` (`id`)
                              ON DELETE CASCADE
                              ON UPDATE CASCADE;
                            ');
        $this->addSql('DROP TABLE `purposes_question`;');
    }

    public function down(Schema $schema) : void
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE `questions_group_translation`');
        $this->addSql('DROP TABLE `questions_group`');

        $this->addSql('CREATE TABLE `purposes_question` (
                              `id` INT NOT NULL AUTO_INCREMENT,
                              `question_id` INT NOT NULL,
                              PRIMARY KEY (`id`)) 
                              DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;
                            ');

        $this->addSql('ALTER TABLE `purposes_question` 
                            ADD INDEX `fk_questions_id_idx` (`question_id` ASC);
                            ALTER TABLE `purposes_question` 
                            ADD CONSTRAINT `fk_questions_id`
                              FOREIGN KEY (`question_id`)
                              REFERENCES `questions` (`id`)
                              ON DELETE CASCADE
                              ON UPDATE CASCADE;
                            ');
    }
}
