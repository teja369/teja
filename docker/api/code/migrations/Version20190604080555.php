<?php

declare(strict_types=1);

namespace Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190604080555 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE `users` 
                            DROP COLUMN `lastname`,
                            CHANGE COLUMN `firstname` `name` VARCHAR(255) CHARACTER SET \'utf8\' NULL DEFAULT NULL ;');
    }

    public function down(Schema $schema) : void
    {

        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE `users` 
                            CHANGE COLUMN `name` `firstName` VARCHAR(255) CHARACTER SET \'utf8\' NULL DEFAULT NULL ,
                            ADD COLUMN `lastName` VARCHAR(255) NULL DEFAULT NULL AFTER `firstName`;');
    }
}
