<?php

declare(strict_types=1);

namespace Migrations;

use Cleevio\Migrations\Migration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20191104153949 extends Migration
{

	public function getDescription(): string
	{
		return 'trip filter translations';
	}


	public function up(Schema $schema): void
	{
		$this->createTranslation('trips.filter.dates.start', 'Start');
		$this->createTranslation('trips.filter.dates.end', 'End');

	}


	public function down(Schema $schema): void
	{
		// this down() migration is auto-generated, please modify it to your needs

	}
}
