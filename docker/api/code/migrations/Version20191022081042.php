<?php

declare(strict_types=1);

namespace Migrations;

use Cleevio\Migrations\Migration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20191022081042 extends Migration
{

	public function getDescription(): string
	{
		return '';
	}


	public function up(Schema $schema): void
	{
		$this->createTranslation('trips.table.hosts.not-chosen', 'Not chosen');
		$this->createTranslation('trips.table.hosts.none', 'None');
	}


	public function down(Schema $schema): void
	{
		// this down() migration is auto-generated, please modify it to your needs

	}
}
