<?php

declare(strict_types=1);

namespace Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20191014114537 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
		$this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

		$this->addSql('CREATE TABLE `trips_favorite` (
							  `id` INT NOT NULL AUTO_INCREMENT,
							  `user_id` INT(10) NOT NULL,
							  `trip_id` INT(10) NOT NULL,
							  PRIMARY KEY (`id`));
							  
							ALTER TABLE `trips_favorite` 
							ADD UNIQUE INDEX `uq_user_trip` (`user_id` ASC, `trip_id` ASC);
							;
							  
							ALTER TABLE `trips_favorite` 
							ADD CONSTRAINT `fk_favorite_ user_id`
							  FOREIGN KEY (`user_id`)
							  REFERENCES `cleevio`.`users` (`id`)
							  ON DELETE CASCADE
							  ON UPDATE CASCADE;
							  
							ALTER TABLE `trips_favorite` 
							ADD CONSTRAINT `fk_favorite_ trip_id`
							  FOREIGN KEY (`trip_id`)
							  REFERENCES `cleevio`.`trips` (`id`)
							  ON DELETE CASCADE
							  ON UPDATE CASCADE;');
    }

    public function down(Schema $schema) : void
    {
		$this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

		$this->addSql('DROP TABLE `trips_favorite`;');
    }
}
