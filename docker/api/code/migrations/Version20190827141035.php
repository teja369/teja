<?php

declare(strict_types=1);

namespace Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190827141035 extends AbstractMigration
{

	public function getDescription(): string
	{
		return '';
	}


	public function up(Schema $schema): void
	{
		// this up() migration is auto-generated, please modify it to your needs
		$this->addSql('CREATE TABLE host_fields_country (id INT AUTO_INCREMENT NOT NULL, 
			host_field_id INT NOT NULL, country_id INT NOT NULL, INDEX IDX_884ED0D815DEFBA6 (host_field_id), 
			INDEX IDX_884ED0D8F92F3E70 (country_id), PRIMARY KEY(id)) DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci ENGINE = InnoDB;');
		$this->addSql('ALTER TABLE host_fields_country ADD CONSTRAINT FK_884ED0D815DEFBA6 
			FOREIGN KEY (host_field_id) REFERENCES host_fields (id);');
		$this->addSql('ALTER TABLE host_fields_country ADD CONSTRAINT FK_884ED0D8F92F3E70 
			FOREIGN KEY (country_id) REFERENCES countries (id);');
		$this->addSql('ALTER TABLE host_fields DROP FOREIGN KEY FK_5AF22263F92F3E70, ADD UNIQUE INDEX UNIQ_5AF22263443707B0 (field_id);');
		$this->addSql('DROP INDEX IDX_5AF22263F92F3E70 ON host_fields;');
		$this->addSql('ALTER TABLE host_fields DROP country_id;');
	}


	public function down(Schema $schema): void
	{
		$this->addSql('DROP TABLE host_field_countries');
	}
}
