<?php

declare(strict_types=1);

namespace Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190805135556 extends AbstractMigration
{
    public function getDescription() : string
    {
        return 'New table for Language';
    }

    public function up(Schema $schema) : void
    {
        $this->addSql('CREATE TABLE languages 
			(id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, 
			code VARCHAR(255) NOT NULL, PRIMARY KEY(id)) 
			DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci ENGINE = InnoDB;');

    }

    public function down(Schema $schema) : void
    {
		$this->addSql('DROP TABLE `languages`');
    }
}
