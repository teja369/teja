<?php

declare(strict_types=1);

namespace Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20191120150549 extends AbstractMigration
{

	public function getDescription(): string
	{
		return '';
	}


	public function up(Schema $schema): void
	{
		$this->addSql('CREATE TABLE `travel_assistant` (`id` INT NOT NULL AUTO_INCREMENT, `traveller_id` INT(10) NOT NULL,
 			`assistant_id` INT(10) NOT NULL, PRIMARY KEY (`id`));');

		$this->addSql('ALTER TABLE `travel_assistant` 
			ADD CONSTRAINT `fk_traveller_id` 
			FOREIGN KEY (`traveller_id`)
			REFERENCES `users` (`id`)
			ON DELETE CASCADE
			ON UPDATE CASCADE;');

		$this->addSql('ALTER TABLE `travel_assistant` 
			ADD CONSTRAINT `fk_assistant_id`
			FOREIGN KEY (`assistant_id`)
			REFERENCES `users` (`id`)
			ON DELETE CASCADE 
			ON UPDATE CASCADE;');

	}


	public function down(Schema $schema): void
	{
		$this->addSql('DROP TABLE `travel_assistant`;');

	}
}
