<?php

declare(strict_types=1);

namespace Migrations;

use Cleevio\Migrations\Migration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20191015124506 extends Migration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
		$this->createTranslation('menu.trips', 'Trips');
		$this->createTranslation('trips.list.headline', 'Trips');
		$this->createTranslation('trips.table.headline', 'All trips');
		$this->createTranslation('trips.table.header.start', 'Start');
		$this->createTranslation('trips.table.header.end', 'End');
		$this->createTranslation('trips.table.header.country', 'Country');
		$this->createTranslation('trips.table.header.user', 'User');
		$this->createTranslation('trips.table.header.host', 'Host');
		$this->createTranslation('trips.table.header.registration', 'Registration(s)');
		$this->createTranslation('trips.table.header.state', 'State');
		$this->createTranslation('trips.table.header.created-at', 'Created');
		$this->createTranslation('trips.table.header.updated-at', 'Updated');
		$this->createTranslation('trips.table.header.submitted-at', 'Submitted');
		$this->createTranslation('trips.table.host.no-host', 'none');
		$this->createTranslation('trips.table.registration.no-registration', 'none');
		$this->createTranslation('trips.table.submitted.not-submitted', 'Not submitted');
		$this->createTranslation('trips.states.draft', 'draft');
		$this->createTranslation('trips.states.deleted', 'deleted');
		$this->createTranslation('trips.states.submitted', 'submitted');
		$this->createTranslation('trips.states.processed', 'processed');
		$this->createTranslation('trips.states.on-hold', 'on hold');
		$this->createTranslation('trips.states.cancelled', 'cancelled');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs

    }
}
