<?php

declare(strict_types=1);

namespace Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190820144206 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE `answers` (
                          `id` INT NOT NULL AUTO_INCREMENT,
                          `type` VARCHAR(255) NOT NULL,
                          `trip_id` INT NOT NULL,
                          `question_id` INT NOT NULL,
                          `text` TEXT DEFAULT NULL,
                          `number` INT DEFAULT NULL,
                          `bool` TINYINT(1) DEFAULT NULL,
                          PRIMARY KEY (`id`)) 
                          DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;');

        $this->addSql('ALTER TABLE `answers` 
                            ADD INDEX `fk_trip_id_idx` (`trip_id` ASC);
                            ALTER TABLE `answers` 
                            ADD CONSTRAINT `fk_trip_id`
                              FOREIGN KEY (`trip_id`)
                              REFERENCES `trips` (`id`)
                              ON DELETE CASCADE
                              ON UPDATE CASCADE;
                            ');

        $this->addSql('ALTER TABLE `answers` 
                            ADD INDEX `fk_question_id_idx` (`question_id` ASC);
                            ALTER TABLE `answers` 
                            ADD CONSTRAINT `fk_question_id`
                              FOREIGN KEY (`question_id`)
                              REFERENCES `questions` (`id`)
                              ON DELETE CASCADE
                              ON UPDATE CASCADE;
                            ');
    }

    public function down(Schema $schema) : void
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE `answers`');
    }
}
