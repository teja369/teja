<?php

declare(strict_types=1);

namespace Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190822141920 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE `answers_option` (
                              `id` INT NOT NULL AUTO_INCREMENT,
                              `answer_id` INT NOT NULL,
                              `question_option_id` INT NOT NULL,
                              PRIMARY KEY (`id`)) 
                              DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;
                            ');

        $this->addSql('ALTER TABLE `answers_option` 
                            ADD INDEX `fk_questions_option_id_idx` (`question_option_id` ASC);
                            ALTER TABLE `answers_option` 
                            ADD CONSTRAINT `fk_questions_option_id`
                              FOREIGN KEY (`question_option_id`)
                              REFERENCES `questions_option` (`id`)
                              ON DELETE CASCADE
                              ON UPDATE CASCADE;
                            ');

        $this->addSql('ALTER TABLE `answers_option` 
                            ADD INDEX `fk_answer_id_idx` (`answer_id` ASC);
                            ALTER TABLE `answers_option` 
                            ADD CONSTRAINT `fk_answer_id`
                              FOREIGN KEY (`answer_id`)
                              REFERENCES `answers` (`id`)
                              ON DELETE CASCADE
                              ON UPDATE CASCADE;
                            ');
    }

    public function down(Schema $schema) : void
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE `answers_option`');
    }
}
