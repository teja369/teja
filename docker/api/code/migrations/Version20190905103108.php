<?php

declare(strict_types=1);

namespace Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190905103108 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE `questions` 
                            DROP FOREIGN KEY `fk_questions_group_id`;
                            ALTER TABLE `questions` 
                            CHANGE COLUMN `group_id` `group_id` INT(11) NOT NULL ;
                            ALTER TABLE `questions` 
                            ADD CONSTRAINT `fk_questions_group_id`
                              FOREIGN KEY (`group_id`)
                              REFERENCES `questions_group` (`id`)
                              ON DELETE CASCADE
                              ON UPDATE CASCADE;
                            ');
    }

    public function down(Schema $schema) : void
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('');
    }
}
