<?php

declare(strict_types=1);

namespace Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190815141728 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE `hosts_param` ADD COLUMN `field_id` INT(11) NOT NULL AFTER `host_id`');
        $this->addSql('ALTER TABLE `hosts_param` ADD INDEX `fk_field_idx` (`field_id` ASC)');
        $this->addSql('ALTER TABLE `hosts_param` 
                            ADD CONSTRAINT `fk_field`
                              FOREIGN KEY (`field_id`)
                              REFERENCES `fields` (`id`)
                              ON DELETE RESTRICT
                              ON UPDATE RESTRICT;
                            ');
    }

    public function down(Schema $schema) : void
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE `hosts_param` DROP COLUMN `field_id`');
    }
}
