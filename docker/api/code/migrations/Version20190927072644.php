<?php

declare(strict_types=1);

namespace Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190927072644 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE `questions` ADD COLUMN `created_at` DATETIME NULL DEFAULT NULL AFTER `variant`;');
        $this->addSql('ALTER TABLE `questions` ADD COLUMN `updated_at` DATETIME NULL DEFAULT NULL AFTER `created_at`;');
        $this->addSql('ALTER TABLE `questions` ADD COLUMN `deleted_at` DATETIME NULL DEFAULT NULL AFTER `updated_at`;');
    }

    public function down(Schema $schema) : void
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE`questions` DROP COLUMN `created_at`;');
        $this->addSql('ALTER TABLE`questions` DROP COLUMN `updated_at`;');
        $this->addSql('ALTER TABLE`questions` DROP COLUMN `deleted_at`;');
    }
}
