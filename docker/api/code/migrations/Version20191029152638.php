<?php

declare(strict_types=1);

namespace Migrations;

use Cleevio\Migrations\Migration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20191029152638 extends Migration
{

	public function getDescription(): string
	{
		return 'translations for put on hold and on hold message';
	}


	public function up(Schema $schema): void
	{
		$this->addSql('ALTER TABLE trips ADD state_message LONGTEXT DEFAULT NULL;');
		$this->createTranslation('trips.table.link.onhold', 'Put on-hold');
		$this->createTranslation('trips.table.link.release', 'Release');
		$this->createTranslation('trips.message-modal.headline', 'Put on hold message');
		$this->createTranslation('trips.message-modal.label.message', 'Message');
		$this->createTranslation('trips.message-modal.label.submit', 'Send');
		$this->createTranslation('trips.message-modal.required.message', 'Message is required');
		$this->createTranslation('trips.message-modal.on-hold.success.message', 'Trip put on hold');
		$this->createTranslation('trips.message-modal.release.success.message', 'Trip released');
		$this->createTranslation('trips.message-modal.button.close', 'Close');

	}


	public function down(Schema $schema): void
	{
		$this->addSql('ALTER TABLE `trips` DROP `on_hold_message`;');
	}
}
