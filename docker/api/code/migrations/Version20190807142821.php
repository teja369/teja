<?php

declare(strict_types=1);

namespace Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190807142821 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        $this->addSql('CREATE TABLE `trips` (
        `id` INT NOT NULL AUTO_INCREMENT,
		PRIMARY KEY(id)) DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci ENGINE = InnoDB;');
    }

    public function down(Schema $schema) : void
    {
        $this->addSql('DROP TABLE `trips`');
    }
}
