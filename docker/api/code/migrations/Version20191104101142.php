<?php

declare(strict_types=1);

namespace Migrations;

use Cleevio\Migrations\Migration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20191104101142 extends Migration
{
    public function getDescription() : string
    {
        return 'translations for question filter';
    }

    public function up(Schema $schema) : void
    {
        $this->createTranslation('questions.filter.country','Country');
        $this->createTranslation('questions.filter.state','State');
        $this->createTranslation('questions.states.published','Published');
        $this->createTranslation('questions.states.unpublished','Unpublished');
        $this->createTranslation('questions.filter.search','Search');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs

    }
}
