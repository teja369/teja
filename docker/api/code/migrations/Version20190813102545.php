<?php

declare(strict_types=1);

namespace Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190813102545 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE `purposes` ADD COLUMN `country_id` INT(45) NULL DEFAULT NULL AFTER `id`;');
        $this->addSql('ALTER TABLE `purposes` 
                            ADD INDEX `fk_country_idx` (`country_id` ASC);
                            ALTER TABLE `purposes` 
                            ADD CONSTRAINT `fk_country`
                              FOREIGN KEY (`country_id`)
                              REFERENCES `countries` (`id`)
                              ON DELETE CASCADE
                              ON UPDATE CASCADE;');
    }

    public function down(Schema $schema): void
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE `purposes` 
                            DROP FOREIGN KEY `fk_country`;
                            ALTER TABLE `purposes` 
                            DROP COLUMN `country_id`,
                            DROP INDEX `fk_country_idx` ;
                            ');
    }
}
