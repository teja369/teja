<?php

declare(strict_types=1);

namespace Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190814144254 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE `hosts` (
                `id` INT NOT NULL AUTO_INCREMENT,
                `type` VARCHAR(45) NOT NULL,
                `name` VARCHAR(255) NOT NULL,
                `country_id` INT(45) NOT NULL,
                PRIMARY KEY(id)) DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci ENGINE = InnoDB;
        ');
        $this->addSql('ALTER TABLE `hosts` 
                            ADD INDEX `fk_country_idx` (`country_id` ASC);
                            ALTER TABLE `hosts` 
                            ADD CONSTRAINT `fk_country`
                              FOREIGN KEY (`country_id`)
                              REFERENCES `countries` (`id`)
                              ON DELETE CASCADE
                              ON UPDATE CASCADE;');
    }

    public function down(Schema $schema) : void
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE `hosts`');
    }
}
