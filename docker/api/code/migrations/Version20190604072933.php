<?php

declare(strict_types=1);

namespace Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190604072933 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE `users` (
                              `id` int(11) NOT NULL AUTO_INCREMENT,
                              `email` varchar(255) COLLATE utf8_general_ci NOT NULL,
                              `password` varchar(255) COLLATE utf8_general_ci DEFAULT NULL,
                              `firstname` varchar(255) COLLATE utf8_general_ci DEFAULT NULL,
                              `lastname` varchar(255) COLLATE utf8_general_ci DEFAULT NULL,
                              `role` varchar(255) COLLATE utf8_general_ci NOT NULL,
                              `created` datetime NOT NULL,
                              `password_reset_hash` varchar(255) COLLATE utf8_general_ci DEFAULT NULL,
                              `last_password_change` datetime DEFAULT NULL,
                              `last_password_reset_request` datetime DEFAULT NULL,
                              PRIMARY KEY (`id`)
                      ) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;');

        $this->addSql('ALTER TABLE `users` 
                            ADD UNIQUE INDEX `email_UNIQUE` (`email` ASC);
                      ');

        $this->addSql('CREATE TABLE `languages` (
                          `id` int(11) NOT NULL AUTO_INCREMENT,
                          `name` varchar(255) COLLATE utf8_general_ci NOT NULL,
                          `iso` varchar(255) COLLATE utf8_general_ci NOT NULL,
                          `flag` varchar(255) COLLATE utf8_general_ci DEFAULT NULL,
                          PRIMARY KEY (`id`)
                        ) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;
                      ');
    }

    public function down(Schema $schema) : void
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE `users`');
        $this->addSql('DROP TABLE `languages`');
    }
}
