<?php

declare(strict_types=1);

namespace Migrations;

use Cleevio\Migrations\Migration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20191029105359 extends Migration
{
    public function getDescription() : string
    {
        return 'translations for admin trip detail';
    }

    public function up(Schema $schema) : void
    {
        $this->createTranslation('trips.detail.headline', 'Trip detail');
        $this->createTranslation('trips.detail.delete', 'Delete trip');
        $this->createTranslation('trips.detail.basic.headline', 'Trip');
        $this->createTranslation('trips.detail.basic.state', 'State');
        $this->createTranslation('trips.detail.basic.start', 'Start');
        $this->createTranslation('trips.detail.basic.end', 'End');
        $this->createTranslation('trips.detail.basic.country', 'Country');
        $this->createTranslation('trips.detail.basic.submitted-at', 'Submitted at');
        $this->createTranslation('trips.detail.basic.not-submitted', 'Not submitted');
        $this->createTranslation('trips.detail.answers.headline', 'Answers');
        $this->createTranslation('trips.detail.answers.no-answers', 'No answers');
        $this->createTranslation('trips.detail.hosts.headline', 'Hosts');
        $this->createTranslation('trips.detail.hosts.no-host', 'Not chosen');
        $this->createTranslation('trips.detail.hosts.none', 'None');
        $this->createTranslation('trips.detail.registrations.headline', 'Registrations');
        $this->createTranslation('trips.detail.registrations.no-registrations', 'No registrations');

    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs

    }
}
