<?php

declare(strict_types=1);

namespace Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190821141739 extends AbstractMigration
{

	public function getDescription(): string
	{
		return '';
	}


	public function up(Schema $schema): void
	{
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

		$this->addSql('ALTER TABLE `fields` ADD `placeholder` varchar(255) COLLATE `utf8_general_ci` NULL;');
    }


	public function down(Schema $schema): void
	{
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

		$this->addSql('ALTER TABLE `fields` DROP `placeholder`;');
	}
}
