<?php

declare(strict_types=1);

namespace Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190806080310 extends AbstractMigration
{

	public function getDescription(): string
	{
		return '';
	}


	public function up(Schema $schema): void
	{
		$this->addSql('CREATE TABLE translations 
		(id INT AUTO_INCREMENT NOT NULL, language_id INT NOT NULL, `key` VARCHAR(255) NOT NULL, 
		value VARCHAR(255) NOT NULL, INDEX IDX_C6B7DA8782F1BAF4 (language_id), 
		PRIMARY KEY(id)) DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci ENGINE = InnoDB;');

		$this->addSql('ALTER TABLE translations 
		ADD CONSTRAINT FK_C6B7DA8782F1BAF4 FOREIGN KEY (language_id) REFERENCES languages (id);');

	}


	public function down(Schema $schema): void
	{
		$this->addSql('DROP TABLE `translations`');

	}
}
