<?php

declare(strict_types=1);

namespace Migrations;

use Cleevio\Migrations\Migration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20191015110352 extends Migration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE `questions_option` ADD UNIQUE INDEX `uq_option_text` (`text` ASC, `question_id` ASC);');
        $this->createTranslation('questions.question.put.error.text-invalid', 'Option text has invalid format');
        $this->createTranslation('questions.question.get.error.option-invalid-text', 'Option has invalid format');
    }

    public function down(Schema $schema) : void
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE `questions_option` DROP INDEX `uq_option_text` ;');
    }
}
