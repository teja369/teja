<?php

declare(strict_types=1);

namespace Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190823080304 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE `purposes_question` (
                              `id` INT NOT NULL AUTO_INCREMENT,
                              `question_id` INT NOT NULL,
                              PRIMARY KEY (`id`)) DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;
                            ');

        $this->addSql('ALTER TABLE `purposes_question` 
                            ADD INDEX `fk_questions_id_idx` (`question_id` ASC);
                            ALTER TABLE `purposes_question` 
                            ADD CONSTRAINT `fk_questions_id`
                              FOREIGN KEY (`question_id`)
                              REFERENCES `questions` (`id`)
                              ON DELETE CASCADE
                              ON UPDATE CASCADE;
                            ');
    }

    public function down(Schema $schema) : void
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE `purposes_question`');
    }
}
