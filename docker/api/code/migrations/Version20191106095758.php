<?php

declare(strict_types=1);

namespace Migrations;

use Cleevio\Acl\Models\IPrivilege;
use Cleevio\Migrations\Migration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20191106095758 extends Migration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        $this->createRole("traveller");
        $this->createRole("admin-external", "traveller");
        $this->createRole("admin", "admin-external");

        $this->createResource("frontend"); // Global frontend resource
        $this->createResource("admin", "frontend"); // Global backend resource

        $this->createResource("admin.hosts");
        $this->createResource("admin.questions");
        $this->createResource("admin.trips");
        $this->createResource("admin.countries");
        $this->createResource("admin.localizations");

        $this->createPrivilege("admin-external", "admin", IPrivilege::PRIVILEGE_ALL, true);
        $this->createPrivilege("admin-external", "admin.hosts", IPrivilege::PRIVILEGE_ALL, true);

        $this->createPrivilege("admin", "admin", IPrivilege::PRIVILEGE_ALL, true);
        $this->createPrivilege("admin", "admin.questions", IPrivilege::PRIVILEGE_ALL, true);
        $this->createPrivilege("admin", "admin.trips", IPrivilege::PRIVILEGE_ALL, true);
        $this->createPrivilege("admin", "admin.countries", IPrivilege::PRIVILEGE_ALL, true);
        $this->createPrivilege("admin", "admin.localizations", IPrivilege::PRIVILEGE_ALL, true);
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs

    }
}
