<?php

declare(strict_types=1);

namespace Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20191030100508 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE `identity` 
                            ADD COLUMN `access_token` VARCHAR(255) NULL DEFAULT NULL AFTER `last_password_reset_request`,
                            ADD COLUMN `expires` INT(10) NULL DEFAULT NULL AFTER `refresh_token`;
                      ');
    }

    public function down(Schema $schema) : void
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE `identity` 
                            DROP COLUMN `expires`,
                            DROP COLUMN `access_token`;
                            ');
    }
}
