<?php

declare(strict_types=1);

namespace Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20191106085506 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE `roles` (
                              `id` INT NOT NULL AUTO_INCREMENT,
                              `name` VARCHAR(255) NOT NULL,
                              `parent_id` INT NULL DEFAULT NULL,
                              PRIMARY KEY (`id`),
                              UNIQUE INDEX `id_UNIQUE` (`id` ASC),
                              UNIQUE INDEX `name_UNIQUE` (`name` ASC));
                            ');

        $this->addSql('ALTER TABLE `roles` 
                            ADD INDEX `fk_roles_parent_idx` (`parent_id` ASC);
                            ALTER TABLE `roles` 
                            ADD CONSTRAINT `fk_roles_parent`
                              FOREIGN KEY (`parent_id`)
                              REFERENCES `roles` (`id`)
                              ON DELETE SET NULL
                              ON UPDATE CASCADE;
                            ');

        $this->addSql('CREATE TABLE `resources` (
                              `id` INT NOT NULL AUTO_INCREMENT,
                              `name` VARCHAR(255) NOT NULL,
                              `parent_id` INT NULL DEFAULT NULL,
                              PRIMARY KEY (`id`),
                              UNIQUE INDEX `id_UNIQUE` (`id` ASC),
                              UNIQUE INDEX `name_UNIQUE` (`name` ASC));
                            ');

        $this->addSql('ALTER TABLE `resources` 
                            ADD INDEX `fk_resources_parent_idx` (`parent_id` ASC);
                            ALTER TABLE `resources` 
                            ADD CONSTRAINT `fk_resources_parent`
                              FOREIGN KEY (`parent_id`)
                              REFERENCES `resources` (`id`)
                              ON DELETE SET NULL
                              ON UPDATE CASCADE;
                            ');

        $this->addSql('ALTER TABLE `users` DROP COLUMN `role`;');

        $this->addSql('CREATE TABLE `users_role` (
                              `id` INT NOT NULL AUTO_INCREMENT,
                              `role_id` INT NOT NULL,
                              `user_id` INT NOT NULL,
                              PRIMARY KEY (`id`),
                              UNIQUE INDEX `id_UNIQUE` (`id` ASC),
                              INDEX `fk_users_role_idx` (`role_id` ASC),
                              INDEX `fk_users_role_user_idx` (`user_id` ASC),
                              CONSTRAINT `fk_users_role_role`
                                FOREIGN KEY (`role_id`)
                                REFERENCES `roles` (`id`)
                                ON DELETE CASCADE
                                ON UPDATE CASCADE,
                              CONSTRAINT `fk_users_role_user`
                                FOREIGN KEY (`user_id`)
                                REFERENCES `users` (`id`)
                                ON DELETE CASCADE
                                ON UPDATE CASCADE);
                            ');

        $this->addSql('CREATE TABLE `privileges` (
                              `id` INT NOT NULL AUTO_INCREMENT,
                              `privilege` VARCHAR(255) NULL DEFAULT NULL,
                              `allow` TINYINT(1) NOT NULL DEFAULT 0,
                              `resource_id` INT NOT NULL,
                              PRIMARY KEY (`id`),
                              UNIQUE INDEX `id_UNIQUE` (`id` ASC),
                              INDEX `fk_privileges_resource_idx` (`resource_id` ASC),
                              CONSTRAINT `fk_privileges_resource`
                                FOREIGN KEY (`resource_id`)
                                REFERENCES `resources` (`id`)
                                ON DELETE CASCADE
                                ON UPDATE CASCADE);
                            ');

        $this->addSql('ALTER TABLE `privileges` 
                            DROP INDEX `id_UNIQUE` ,
                            ADD UNIQUE INDEX `id_UNIQUE` (`privilege` ASC, `allow` ASC, `resource_id` ASC);
                            ');

        $this->addSql('CREATE TABLE `roles_privilege` (
                              `id` INT NOT NULL AUTO_INCREMENT,
                              `role_id` INT NOT NULL,
                              `privilege_id` INT NOT NULL,
                              PRIMARY KEY (`id`),
                              UNIQUE INDEX `id_UNIQUE` (`id` ASC),
                              INDEX `fk_roles_privilege_role_idx` (`role_id` ASC),
                              INDEX `fk_roles_privilege_privilege_idx` (`privilege_id` ASC),
                              CONSTRAINT `fk_roles_privilege_role`
                                FOREIGN KEY (`role_id`)
                                REFERENCES `roles` (`id`)
                                ON DELETE CASCADE
                                ON UPDATE CASCADE,
                              CONSTRAINT `fk_roles_privilege_privilege`
                                FOREIGN KEY (`privilege_id`)
                                REFERENCES `privileges` (`id`)
                                ON DELETE CASCADE
                                ON UPDATE CASCADE);
                            ');

        $this->addSql('ALTER TABLE `roles_privilege` 
                            DROP INDEX `id_UNIQUE` ,
                            ADD UNIQUE INDEX `id_UNIQUE` (`role_id` ASC, `privilege_id` ASC);
');
    }

    public function down(Schema $schema): void
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE `roles`;');
        $this->addSql('DROP TABLE `resources`;');
        $this->addSql('DROP TABLE `users_role`;');
        $this->addSql('DROP TABLE `privileges`;');
        $this->addSql('DROP TABLE `roles_privilege`;');
    }
}
