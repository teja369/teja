<?php

declare(strict_types=1);

namespace Migrations;

use Cleevio\Migrations\Migration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20191113085045 extends Migration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // Austria fix
        $this->addSql('UPDATE `countries` SET `code`=\'AT\' WHERE `code`=\'AU\'');

        // Remove icon
        $this->addSql('ALTER TABLE `countries` DROP COLUMN `icon`;');

        $this->createTranslation('languages.country', 'Country');

        // Add countries
        $this->createCountry("AF","Afghanistan");
        $this->createCountry("AX","Åland Islands");
        $this->createCountry("AL","Albania");
        $this->createCountry("DZ","Algeria");
        $this->createCountry("AS","American Samoa");
        $this->createCountry("AD","Andorra");
        $this->createCountry("AO","Angola");
        $this->createCountry("AI","Anguilla");
        $this->createCountry("AQ","Antarctica");
        $this->createCountry("AG","Antigua and Barbuda");
        $this->createCountry("AR","Argentina");
        $this->createCountry("AM","Armenia");
        $this->createCountry("AW","Aruba");
        $this->createCountry("AU","Australia");
        $this->createCountry("AT","Austria");
        $this->createCountry("AZ","Azerbaijan");
        $this->createCountry("BS","Bahamas");
        $this->createCountry("BH","Bahrain");
        $this->createCountry("BD","Bangladesh");
        $this->createCountry("BB","Barbados");
        $this->createCountry("BY","Belarus");
        $this->createCountry("BE","Belgium");
        $this->createCountry("BZ","Belize");
        $this->createCountry("BJ","Benin");
        $this->createCountry("BM","Bermuda");
        $this->createCountry("BT","Bhutan");
        $this->createCountry("BO","Bolivia (Plurinational State of)");
        $this->createCountry("BQ","Bonaire, Sint Eustatius and Saba");
        $this->createCountry("BA","Bosnia and Herzegovina");
        $this->createCountry("BW","Botswana");
        $this->createCountry("BV","Bouvet Island");
        $this->createCountry("BR","Brazil");
        $this->createCountry("IO","British Indian Ocean Territory");
        $this->createCountry("BN","Brunei Darussalam");
        $this->createCountry("BG","Bulgaria");
        $this->createCountry("BF","Burkina Faso");
        $this->createCountry("BI","Burundi");
        $this->createCountry("CV","Cabo Verde");
        $this->createCountry("KH","Cambodia");
        $this->createCountry("CM","Cameroon");
        $this->createCountry("CA","Canada");
        $this->createCountry("KY","Cayman Islands");
        $this->createCountry("CF","Central African Republic");
        $this->createCountry("TD","Chad");
        $this->createCountry("CL","Chile");
        $this->createCountry("CN","China");
        $this->createCountry("CX","Christmas Island");
        $this->createCountry("CC","Cocos (Keeling) Islands");
        $this->createCountry("CO","Colombia");
        $this->createCountry("KM","Comoros");
        $this->createCountry("CG","Congo");
        $this->createCountry("CD","Congo, Democratic Republic of the");
        $this->createCountry("CK","Cook Islands");
        $this->createCountry("CR","Costa Rica");
        $this->createCountry("CI","Côte d'Ivoire");
        $this->createCountry("HR","Croatia");
        $this->createCountry("CU","Cuba");
        $this->createCountry("CW","Curaçao");
        $this->createCountry("CY","Cyprus");
        $this->createCountry("CZ","Czechia");
        $this->createCountry("DK","Denmark");
        $this->createCountry("DJ","Djibouti");
        $this->createCountry("DM","Dominica");
        $this->createCountry("DO","Dominican Republic");
        $this->createCountry("EC","Ecuador");
        $this->createCountry("EG","Egypt");
        $this->createCountry("SV","El Salvador");
        $this->createCountry("GQ","Equatorial Guinea");
        $this->createCountry("ER","Eritrea");
        $this->createCountry("EE","Estonia");
        $this->createCountry("SZ","Eswatini");
        $this->createCountry("ET","Ethiopia");
        $this->createCountry("FK","Falkland Islands (Malvinas)");
        $this->createCountry("FO","Faroe Islands");
        $this->createCountry("FJ","Fiji");
        $this->createCountry("FI","Finland");
        $this->createCountry("FR","France");
        $this->createCountry("GF","French Guiana");
        $this->createCountry("PF","French Polynesia");
        $this->createCountry("TF","French Southern Territories");
        $this->createCountry("GA","Gabon");
        $this->createCountry("GM","Gambia");
        $this->createCountry("GE","Georgia");
        $this->createCountry("DE","Germany");
        $this->createCountry("GH","Ghana");
        $this->createCountry("GI","Gibraltar");
        $this->createCountry("GR","Greece");
        $this->createCountry("GL","Greenland");
        $this->createCountry("GD","Grenada");
        $this->createCountry("GP","Guadeloupe");
        $this->createCountry("GU","Guam");
        $this->createCountry("GT","Guatemala");
        $this->createCountry("GG","Guernsey");
        $this->createCountry("GN","Guinea");
        $this->createCountry("GW","Guinea-Bissau");
        $this->createCountry("GY","Guyana");
        $this->createCountry("HT","Haiti");
        $this->createCountry("HM","Heard Island and McDonald Islands");
        $this->createCountry("VA","Holy See");
        $this->createCountry("HN","Honduras");
        $this->createCountry("HK","Hong Kong");
        $this->createCountry("HU","Hungary");
        $this->createCountry("IS","Iceland");
        $this->createCountry("IN","India");
        $this->createCountry("ID","Indonesia");
        $this->createCountry("IR","Iran (Islamic Republic of)");
        $this->createCountry("IQ","Iraq");
        $this->createCountry("IE","Ireland");
        $this->createCountry("IM","Isle of Man");
        $this->createCountry("IL","Israel");
        $this->createCountry("IT","Italy");
        $this->createCountry("JM","Jamaica");
        $this->createCountry("JP","Japan");
        $this->createCountry("JE","Jersey");
        $this->createCountry("JO","Jordan");
        $this->createCountry("KZ","Kazakhstan");
        $this->createCountry("KE","Kenya");
        $this->createCountry("KI","Kiribati");
        $this->createCountry("KP","Korea (Democratic People's Republic of)");
        $this->createCountry("KR","Korea, Republic of");
        $this->createCountry("KW","Kuwait");
        $this->createCountry("KG","Kyrgyzstan");
        $this->createCountry("LA","Lao People's Democratic Republic");
        $this->createCountry("LV","Latvia");
        $this->createCountry("LB","Lebanon");
        $this->createCountry("LS","Lesotho");
        $this->createCountry("LR","Liberia");
        $this->createCountry("LY","Libya");
        $this->createCountry("LI","Liechtenstein");
        $this->createCountry("LT","Lithuania");
        $this->createCountry("LU","Luxembourg");
        $this->createCountry("MO","Macao");
        $this->createCountry("MG","Madagascar");
        $this->createCountry("MW","Malawi");
        $this->createCountry("MY","Malaysia");
        $this->createCountry("MV","Maldives");
        $this->createCountry("ML","Mali");
        $this->createCountry("MT","Malta");
        $this->createCountry("MH","Marshall Islands");
        $this->createCountry("MQ","Martinique");
        $this->createCountry("MR","Mauritania");
        $this->createCountry("MU","Mauritius");
        $this->createCountry("YT","Mayotte");
        $this->createCountry("MX","Mexico");
        $this->createCountry("FM","Micronesia (Federated States of)");
        $this->createCountry("MD","Moldova, Republic of");
        $this->createCountry("MC","Monaco");
        $this->createCountry("MN","Mongolia");
        $this->createCountry("ME","Montenegro");
        $this->createCountry("MS","Montserrat");
        $this->createCountry("MA","Morocco");
        $this->createCountry("MZ","Mozambique");
        $this->createCountry("MM","Myanmar");
        $this->createCountry("NA","Namibia");
        $this->createCountry("NR","Nauru");
        $this->createCountry("NP","Nepal");
        $this->createCountry("NL","Netherlands");
        $this->createCountry("NC","New Caledonia");
        $this->createCountry("NZ","New Zealand");
        $this->createCountry("NI","Nicaragua");
        $this->createCountry("NE","Niger");
        $this->createCountry("NG","Nigeria");
        $this->createCountry("NU","Niue");
        $this->createCountry("NF","Norfolk Island");
        $this->createCountry("MK","North Macedonia");
        $this->createCountry("MP","Northern Mariana Islands");
        $this->createCountry("NO","Norway");
        $this->createCountry("OM","Oman");
        $this->createCountry("PK","Pakistan");
        $this->createCountry("PW","Palau");
        $this->createCountry("PS","Palestine, State of");
        $this->createCountry("PA","Panama");
        $this->createCountry("PG","Papua New Guinea");
        $this->createCountry("PY","Paraguay");
        $this->createCountry("PE","Peru");
        $this->createCountry("PH","Philippines");
        $this->createCountry("PN","Pitcairn");
        $this->createCountry("PL","Poland");
        $this->createCountry("PT","Portugal");
        $this->createCountry("PR","Puerto Rico");
        $this->createCountry("QA","Qatar");
        $this->createCountry("RE","Réunion");
        $this->createCountry("RO","Romania");
        $this->createCountry("RU","Russian Federation");
        $this->createCountry("RW","Rwanda");
        $this->createCountry("BL","Saint Barthélemy");
        $this->createCountry("SH","Saint Helena, Ascension and Tristan da Cunha");
        $this->createCountry("KN","Saint Kitts and Nevis");
        $this->createCountry("LC","Saint Lucia");
        $this->createCountry("MF","Saint Martin (French part)");
        $this->createCountry("PM","Saint Pierre and Miquelon");
        $this->createCountry("VC","Saint Vincent and the Grenadines");
        $this->createCountry("WS","Samoa");
        $this->createCountry("SM","San Marino");
        $this->createCountry("ST","Sao Tome and Principe");
        $this->createCountry("SA","Saudi Arabia");
        $this->createCountry("SN","Senegal");
        $this->createCountry("RS","Serbia");
        $this->createCountry("SC","Seychelles");
        $this->createCountry("SL","Sierra Leone");
        $this->createCountry("SG","Singapore");
        $this->createCountry("SX","Sint Maarten (Dutch part)");
        $this->createCountry("SK","Slovakia");
        $this->createCountry("SI","Slovenia");
        $this->createCountry("SB","Solomon Islands");
        $this->createCountry("SO","Somalia");
        $this->createCountry("ZA","South Africa");
        $this->createCountry("GS","South Georgia and the South Sandwich Islands");
        $this->createCountry("SS","South Sudan");
        $this->createCountry("ES","Spain");
        $this->createCountry("LK","Sri Lanka");
        $this->createCountry("SD","Sudan");
        $this->createCountry("SR","Suriname");
        $this->createCountry("SJ","Svalbard and Jan Mayen");
        $this->createCountry("SE","Sweden");
        $this->createCountry("CH","Switzerland");
        $this->createCountry("SY","Syrian Arab Republic");
        $this->createCountry("TW","Taiwan, Province of China");
        $this->createCountry("TJ","Tajikistan");
        $this->createCountry("TZ","Tanzania, United Republic of");
        $this->createCountry("TH","Thailand");
        $this->createCountry("TL","Timor-Leste");
        $this->createCountry("TG","Togo");
        $this->createCountry("TK","Tokelau");
        $this->createCountry("TO","Tonga");
        $this->createCountry("TT","Trinidad and Tobago");
        $this->createCountry("TN","Tunisia");
        $this->createCountry("TR","Turkey");
        $this->createCountry("TM","Turkmenistan");
        $this->createCountry("TC","Turks and Caicos Islands");
        $this->createCountry("TV","Tuvalu");
        $this->createCountry("UG","Uganda");
        $this->createCountry("UA","Ukraine");
        $this->createCountry("AE","United Arab Emirates");
        $this->createCountry("GB","United Kingdom of Great Britain and Northern Ireland");
        $this->createCountry("US","United States of America");
        $this->createCountry("UM","United States Minor Outlying Islands");
        $this->createCountry("UY","Uruguay");
        $this->createCountry("UZ","Uzbekistan");
        $this->createCountry("VU","Vanuatu");
        $this->createCountry("VE","Venezuela (Bolivarian Republic of)");
        $this->createCountry("VN","Viet Nam");
        $this->createCountry("VG","Virgin Islands (British)");
        $this->createCountry("VI","Virgin Islands (U.S.)");
        $this->createCountry("WF","Wallis and Futuna");
        $this->createCountry("EH","Western Sahara");
        $this->createCountry("YE","Yemen");
        $this->createCountry("ZM","Zambia");
        $this->createCountry("ZW","Zimbabwe");
    }

    public function down(Schema $schema): void
    {
    }
}
