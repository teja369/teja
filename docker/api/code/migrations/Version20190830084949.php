<?php

declare(strict_types=1);

namespace Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190830084949 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('TRUNCATE `translations`;');
        $this->addSql('ALTER TABLE `translations` CHANGE COLUMN `key` `translation_key_id` INT(11) NOT NULL ;');
        $this->addSql('CREATE TABLE `translations_key` (
                              `id` INT NOT NULL AUTO_INCREMENT,
                              `key` VARCHAR(255) NOT NULL,
                              PRIMARY KEY (`id`),
                              UNIQUE INDEX `key_UNIQUE` (`key` ASC)) 
                              DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;');
        $this->addSql('ALTER TABLE `translations` ADD INDEX `fk_translations__key_id_idx` (`translation_key_id` ASC);');
        $this->addSql('ALTER TABLE `translations` 
                            ADD CONSTRAINT `fk_translations__key_id`
                              FOREIGN KEY (`translation_key_id`)
                              REFERENCES `translations_key` (`id`)
                              ON DELETE CASCADE
                              ON UPDATE CASCADE;');

        $this->addSql('ALTER TABLE `languages` ADD UNIQUE INDEX `code_UNIQUE` (`code` ASC);');
        $this->addSql('ALTER TABLE `translations` ADD UNIQUE INDEX `unique_translation` (`language_id` ASC, `translation_key_id` ASC);');
    }

    public function down(Schema $schema) : void
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE `translations_key`');
        $this->addSql('ALTER TABLE `translations` CHANGE COLUMN `key` `translation_key_id` VARCHAR(255) NOT NULL ;');
    }
}
