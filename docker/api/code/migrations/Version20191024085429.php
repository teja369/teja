<?php

declare(strict_types=1);

namespace Migrations;

use Cleevio\Migrations\Migration;
use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20191024085429 extends Migration
{
    public function getDescription() : string
    {
        return 'added translation for parent question placeholder';
    }

    public function up(Schema $schema) : void
    {
        $this->createTranslation('questions.form.no-parent-questions', 'No parent questions');

    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs

    }
}
