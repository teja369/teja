<?php

declare(strict_types=1);

namespace Migrations;

use Cleevio\Migrations\Migration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20191112155112 extends Migration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE `languages` DROP COLUMN `icon`;');
        $this->addSql('ALTER TABLE `languages` ADD COLUMN `country_id` INT NOT NULL DEFAULT 0 AFTER `is_default`;');

        $this->addSql('UPDATE `languages` SET country_id = (SELECT id FROM `countries` ORDER BY is_home DESC LIMIT 1)');

        $this->addSql('ALTER TABLE `languages` 
                            ADD INDEX `fk_languages_country_idx` (`country_id` ASC);
                            ALTER TABLE `languages` 
                            ADD CONSTRAINT `fk_languages_country`
                              FOREIGN KEY (`country_id`)
                              REFERENCES `countries` (`id`)
                              ON DELETE RESTRICT
                              ON UPDATE RESTRICT;
                            ');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs

    }
}
