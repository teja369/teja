<?php

declare(strict_types=1);

namespace Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20191119114836 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
    	$this->addSql('ALTER TABLE `users` ADD COLUMN `first_name` VARCHAR(255) NULL DEFAULT NULL;');
    	$this->addSql('ALTER TABLE `users` ADD COLUMN `last_name` VARCHAR(255) NULL DEFAULT NULL;');

    }

    public function down(Schema $schema) : void
    {
        $this->addSql('ALTER TABLE `users` DROP COLUMN `first_name`;');
        $this->addSql('ALTER TABLE `users` DROP COLUMN `last_name`;');

    }
}
