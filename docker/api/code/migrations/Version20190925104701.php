<?php

declare(strict_types=1);

namespace Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190925104701 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE questions_group_translation');

        $this->addSql('CREATE TABLE questions_group_translation (id INT AUTO_INCREMENT NOT NULL, object_id INT DEFAULT NULL, 
			locale VARCHAR(8) NOT NULL, field VARCHAR(32) NOT NULL, content LONGTEXT DEFAULT NULL, 
			INDEX IDX_50FAFEDD232D562C (object_id), PRIMARY KEY(id)) DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci ENGINE = InnoDB;');

        $this->addSql('ALTER TABLE questions_group_translation ADD CONSTRAINT FK_50FAFEDD232D562C 
			FOREIGN KEY (object_id) REFERENCES questions_group (id) ON DELETE CASCADE;');
    }

    public function down(Schema $schema): void
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE questions_group_translation');

        $this->addSql('CREATE TABLE questions_group_translation (object_id INT DEFAULT NULL, 
			locale VARCHAR(8) NOT NULL, field VARCHAR(32) NOT NULL, content LONGTEXT DEFAULT NULL, 
			INDEX IDX_50FAFEDD232D562C (object_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 
			COLLATE utf8_general_ci ENGINE = InnoDB;');

        $this->addSql('ALTER TABLE questions_group_translation ADD CONSTRAINT FK_50FAFEDD232D562C 
			FOREIGN KEY (object_id) REFERENCES questions_group (id) ON DELETE CASCADE;');
    }
}
