<?php

declare(strict_types=1);

namespace Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190808140659 extends AbstractMigration
{

	public function getDescription(): string
	{
		return '';
	}


	public function up(Schema $schema): void
	{
		$this->addSql('ALTER TABLE trips ADD country VARCHAR(255) NOT NULL, ADD start DATETIME NOT NULL, 
		ADD end DATETIME NOT NULL, ADD state VARCHAR(255) NOT NULL;');

	}


	public function down(Schema $schema): void
	{
		$this->addSql('ALTER TABLE trips DROP COLUMN country, start, end, state');

	}
}
