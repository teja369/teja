<?php

declare(strict_types=1);

namespace Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20191002111903 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE `hosts_param` ADD UNIQUE INDEX `uq_host_field` (`host_id` ASC, `field_id` ASC);');
        $this->addSql('ALTER TABLE `fields_country` ADD UNIQUE INDEX `uq_field_country` (`field_id` ASC, `country_id` ASC);');
        $this->addSql('ALTER TABLE `fields_translation` ADD UNIQUE INDEX `uq_fields_translation` (`object_id` ASC, `field` ASC, `locale` ASC);');
        $this->addSql('ALTER TABLE `countries_translation` ADD UNIQUE INDEX `uq_countries_translation` (`locale` ASC, `field` ASC, `object_id` ASC);');
        $this->addSql('ALTER TABLE `questions_country` ADD UNIQUE INDEX `uq_questions_country` (`country_id` ASC, `question_id` ASC);');
        $this->addSql('ALTER TABLE `questions_group_translation` ADD UNIQUE INDEX `uq_guestions_group_translation` (`object_id` ASC, `locale` ASC, `field` ASC);');
        $this->addSql('ALTER TABLE `questions_host` ADD UNIQUE INDEX `uq_question_host` (`host_id` ASC, `question_id` ASC);');
        $this->addSql('ALTER TABLE `questions_option_country` ADD UNIQUE INDEX `uq_question_option_country` (`country_id` ASC, `option_id` ASC);');
        $this->addSql('ALTER TABLE `questions_option_translation` ADD UNIQUE INDEX `uq_questions_country_translation` (`object_id` ASC, `field` ASC, `locale` ASC);');
        $this->addSql('ALTER TABLE `questions_translation` ADD UNIQUE INDEX `uq_questions_translation` (`object_id` ASC, `locale` ASC, `field` ASC);');
        $this->addSql('ALTER TABLE `registrations_question` ADD UNIQUE INDEX `uq_registration_question` (`question_id` ASC, `registration_id` ASC);');

    }

    public function down(Schema $schema) : void
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE `hosts_param` DROP INDEX `uq_host_field` ;');
        $this->addSql('ALTER TABLE `fields_country` DROP INDEX `uq_field_country` ;');
        $this->addSql('ALTER TABLE `uq_fields_translation` DROP INDEX `uq_translation` ;');
        $this->addSql('ALTER TABLE `countries_translation` DROP INDEX `uq_countries_translation` ;');
        $this->addSql('ALTER TABLE `questions_country` DROP INDEX `uq_questions_country` ;');
        $this->addSql('ALTER TABLE `questions_group_translation` DROP INDEX `uq_guestions_group_translation` ;');
        $this->addSql('ALTER TABLE `questions_host` DROP INDEX `uq_question_host` ;');
        $this->addSql('ALTER TABLE `questions_option_country` DROP INDEX `uq_question_option_country` ;');
        $this->addSql('ALTER TABLE `questions_option_translation` DROP INDEX `uq_questions_country_translation` ;');
        $this->addSql('ALTER TABLE `questions_translation` DROP INDEX `uq_questions_country` ;');
        $this->addSql('ALTER TABLE `registrations_question` DROP INDEX `uq_registration_question` ;');
    }
}
