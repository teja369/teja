<?php

declare(strict_types=1);

namespace Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20191001082613 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        $this->addSql('CREATE TABLE fields_country (id INT AUTO_INCREMENT NOT NULL, 
			field_id INT NOT NULL, country_id INT NOT NULL, INDEX IDX_884ED0D815DEFBA7 (field_id), 
			INDEX IDX_884ED0D8F92F3E71 (country_id), PRIMARY KEY(id)) DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci ENGINE = InnoDB;');
        $this->addSql('ALTER TABLE fields_country ADD CONSTRAINT FK_884ED0D815DEFBA7 
			FOREIGN KEY (field_id) REFERENCES fields (id);');
        $this->addSql('ALTER TABLE fields_country ADD CONSTRAINT FK_884ED0D8F92F3E71 
			FOREIGN KEY (country_id) REFERENCES countries (id);');

        $this->addSql('DROP TABLE host_fields_country');
        $this->addSql('DROP TABLE host_fields');
    }

    public function down(Schema $schema) : void
    {
        $this->addSql('DROP TABLE fields_country');
    }
}
