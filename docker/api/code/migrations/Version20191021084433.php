<?php

declare(strict_types=1);

namespace Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20191021084433 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
		$this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

		$this->addSql('ALTER TABLE `users` ADD COLUMN `language_id` INT(11) NULL DEFAULT NULL AFTER `username`;');
		$this->addSql('ALTER TABLE `users` 
							ADD INDEX `fk_language_id_idx` (`language_id` ASC);
							;
							ALTER TABLE `users` 
							ADD CONSTRAINT `fk_language_id`
							  FOREIGN KEY (`language_id`)
							  REFERENCES `languages` (`id`)
							  ON DELETE SET NULL
							  ON UPDATE SET NULL;
							');
    }

    public function down(Schema $schema) : void
    {
		$this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

		$this->addSql('');
    }
}
