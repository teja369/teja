<?php

declare(strict_types=1);

namespace Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190823103146 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE `fields` ADD COLUMN `required` TINYINT(1) NOT NULL DEFAULT 0 AFTER `placeholder`;');
        $this->addSql('ALTER TABLE `fields` ADD COLUMN `validation` VARCHAR(255) NULL DEFAULT NULL AFTER `required`;');
    }

    public function down(Schema $schema) : void
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE `fields` DROP COLUMN `required`;');
        $this->addSql('ALTER TABLE `fields` DROP COLUMN `validation`;');

    }
}
