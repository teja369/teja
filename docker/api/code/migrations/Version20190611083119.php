<?php

declare(strict_types=1);

namespace Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190611083119 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE `users` 
                            DROP COLUMN `password`');

        $this->addSql('CREATE TABLE `identity` (
                              `id` int(11) NOT NULL AUTO_INCREMENT,
                              `user_id` int(11) NOT NULL,
                              `type` varchar(255) COLLATE utf8_general_ci DEFAULT NULL,
                              `password` varchar(255) COLLATE utf8_general_ci DEFAULT NULL,
                              `password_reset_hash` VARCHAR(255) NULL DEFAULT NULL,
                              `last_password_change` DATETIME NULL DEFAULT NULL,
                              `last_password_reset_request` DATETIME NULL DEFAULT NULL,    
                              `refresh_token` VARCHAR(255) NULL DEFAULT NULL,                 
                              `facebook_id` VARCHAR(255) NULL DEFAULT NULL,                   
                              `account_kit_id` VARCHAR(255) NULL DEFAULT NULL,                          
                              PRIMARY KEY (`id`)
                            ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;
                            ');

        $this->addSql('
                            ALTER TABLE `identity` 
                            ADD UNIQUE INDEX `user_id_type_refresh_token` (`user_id` ASC, `type` ASC, `refresh_token` ASC);) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;
                            ');

        $this->addSql('
                            ALTER TABLE `identity` 
                            ADD CONSTRAINT `fk_user_id`
                              FOREIGN KEY (`user_id`)
                              REFERENCES `users` (`id`)
                              ON DELETE CASCADE
                              ON UPDATE CASCADE;
                            ');

        $this->addSql('
                            ALTER TABLE `users` 
                            DROP COLUMN `last_password_reset_request`,
                            DROP COLUMN `last_password_change`,
                            DROP COLUMN `password_reset_hash`;
                            ');
    }

    public function down(Schema $schema): void
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE `users` 
                            ADD COLUMN `password` VARCHAR(255) NULL AFTER `email`,
                            ADD COLUMN `password_reset_hash` VARCHAR(255) NULL DEFAULT NULL AFTER `password`,
                            ADD COLUMN `last_password_change` DATETIME NULL DEFAULT NULL AFTER `password_reset_hash`,
                            ADD COLUMN `last_password_reset_request` DATETIME NULL DEFAULT NULL AFTER `last_password_change`;
                     ');
        $this->addSql('DROP TABLE `identity`');

    }
}
