<?php

declare(strict_types=1);

namespace Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190828104944 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {

        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE `purposes_country`');
        $this->addSql('DROP TABLE `purposes_translation`');
        $this->addSql('DROP TABLE `trips_purpose`');
        $this->addSql('DROP TABLE `purposes`');
    }

    public function down(Schema $schema) : void
    {

        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE `purposes` (
                `id` INT NOT NULL AUTO_INCREMENT,
                `name` VARCHAR(255) NOT NULL,
                PRIMARY KEY(id)) DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci ENGINE = InnoDB;
        ');

        $this->addSql('CREATE TABLE `purposes_translation` (
                          `locale` varchar(10) NOT NULL DEFAULT \'en\',
                          `field` varchar(255) NOT NULL,
                          `content` varchar(255) NOT NULL,
                          `object_id` int(10) NOT NULL,
                          PRIMARY KEY (`locale`)) DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci ENGINE = InnoDB;
        ');

        $this->addSql('ALTER TABLE `purposes_translation` ADD INDEX `index` (`object_id` ASC);');
        $this->addSql('ALTER TABLE `purposes_translation` 
                            ADD CONSTRAINT `fk_purposes`
                              FOREIGN KEY (`object_id`)
                              REFERENCES `purposes` (`id`)
                              ON DELETE CASCADE
                              ON UPDATE CASCADE;');


        $this->addSql('CREATE TABLE `purposes_country` (
                              `id` INT NOT NULL AUTO_INCREMENT,
                              `purpose_id` INT NOT NULL,
                              `country_id` INT NOT NULL,
                              PRIMARY KEY (`id`)) 
                              DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;
                            ');

        $this->addSql('ALTER TABLE `purposes_country` 
                            ADD INDEX `fk_countries_idx` (`country_id` ASC);
                            ALTER TABLE `purposes_country` 
                            ADD CONSTRAINT `fk_countries`
                              FOREIGN KEY (`country_id`)
                              REFERENCES `countries` (`id`)
                              ON DELETE RESTRICT
                              ON UPDATE RESTRICT;
                            ');

        $this->addSql('ALTER TABLE `purposes_country` 
                            ADD INDEX `fk_purposes_idx` (`purpose_id` ASC);
                            ALTER TABLE `purposes_country` 
                            ADD CONSTRAINT `fk_purposes`
                              FOREIGN KEY (`purpose_id`)
                              REFERENCES `purposes` (`id`)
                              ON DELETE CASCADE
                              ON UPDATE CASCADE,
                               DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci
                            ');

        $this->addSql('CREATE TABLE `trips_purpose` (
                              `id` INT NOT NULL AUTO_INCREMENT,
                              `trip_id` INT NOT NULL,
                              `purpose_id` INT NOT NULL,
                              PRIMARY KEY (`id`),
                              INDEX `ix_trip` (`trip_id` ASC),
                              INDEX `ix_purpose` (`purpose_id` ASC),
                              CONSTRAINT `fk_trips`
                                FOREIGN KEY (`trip_id`)
                                REFERENCES `trips` (`id`)
                                ON DELETE CASCADE
                                ON UPDATE CASCADE,
                              CONSTRAINT `fk_purpose`
                                FOREIGN KEY (`purpose_id`)
                                REFERENCES `purposes` (`id`)
                                ON DELETE RESTRICT
                                ON UPDATE RESTRICT);
                            ');
        $this->addSql('ALTER TABLE `trips_purpose` 
                            ADD UNIQUE INDEX `uq_trip_purpose` (`purpose_id` ASC, `trip_id` ASC);
                            ');
    }
}
