<?php

declare(strict_types=1);

namespace Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190819090525 extends AbstractMigration
{

	public function getDescription(): string
	{
		return '';
	}


	public function up(Schema $schema): void
	{
		$this->addSql('CREATE TABLE questions_translation (id INT AUTO_INCREMENT NOT NULL, object_id INT DEFAULT NULL, 
			locale VARCHAR(8) NOT NULL, field VARCHAR(32) NOT NULL, content LONGTEXT DEFAULT NULL, 
			INDEX IDX_50FAFEDD232D562B (object_id), PRIMARY KEY(id)) DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci ENGINE = InnoDB;');

		$this->addSql('CREATE TABLE questions (id INT AUTO_INCREMENT NOT NULL, type VARCHAR(255) NOT NULL, 
			text VARCHAR(255) NOT NULL, start_date DATETIME DEFAULT NULL, end_date DATETIME DEFAULT NULL, 
			help VARCHAR(255) DEFAULT NULL, step INT NOT NULL, `order` INT NOT NULL, is_prefilled TINYINT(1) NOT NULL, 
			PRIMARY KEY(id)) DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci ENGINE = InnoDB;');

		$this->addSql('CREATE TABLE questions_country (id INT AUTO_INCREMENT NOT NULL, country_id INT NOT NULL, 
			question_id INT NOT NULL, INDEX IDX_20F6E9A0F92F3E70 (country_id), INDEX IDX_20F6E9A01E27F6BF (question_id), 
			PRIMARY KEY(id)) DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci ENGINE = InnoDB;');

		$this->addSql('ALTER TABLE questions_translation ADD CONSTRAINT FK_50FAFEDD232D562B 
			FOREIGN KEY (object_id) REFERENCES questions (id) ON DELETE CASCADE;');

		$this->addSql('ALTER TABLE questions_country ADD CONSTRAINT FK_20F6E9A0F92F3E70 
			FOREIGN KEY (country_id) REFERENCES countries (id);');

		$this->addSql('ALTER TABLE questions_country ADD CONSTRAINT FK_20F6E9A01E27F6BF 
			FOREIGN KEY (question_id) REFERENCES questions (id);');

		$this->addSql('CREATE TABLE questions_host (id INT AUTO_INCREMENT NOT NULL, host_id INT NOT NULL, 
			question_id INT NOT NULL, INDEX IDX_D287B7AF1FB8D185 (host_id), INDEX IDX_D287B7AF1E27F6BF (question_id), 
			PRIMARY KEY(id)) DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci ENGINE = InnoDB;');

		$this->addSql('ALTER TABLE questions_host ADD CONSTRAINT FK_D287B7AF1FB8D185 
			FOREIGN KEY (host_id) REFERENCES hosts (id);');

		$this->addSql('ALTER TABLE questions_host ADD CONSTRAINT FK_D287B7AF1E27F6BF 
			FOREIGN KEY (question_id) REFERENCES questions (id);');

		$this->addSql('CREATE TABLE registrations_question (id INT AUTO_INCREMENT NOT NULL, 
			question_id INT NOT NULL, registration_id INT NOT NULL, INDEX IDX_2E0A11151E27F6BF (question_id), 
			INDEX IDX_2E0A1115833D8F43 (registration_id), PRIMARY KEY(id)) DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci ENGINE = InnoDB;');

		$this->addSql('ALTER TABLE registrations_question ADD CONSTRAINT FK_2E0A11151E27F6BF 
			FOREIGN KEY (question_id) REFERENCES questions (id);');

		$this->addSql('ALTER TABLE registrations_question ADD CONSTRAINT FK_2E0A1115833D8F43 
			FOREIGN KEY (registration_id) REFERENCES registrations (id);');
	}


	public function down(Schema $schema): void
	{
		$this->addSql('DROP TABLE `questions_translation`');
		$this->addSql('DROP TABLE `questions_country`');
		$this->addSql('DROP TABLE `questions`');
		$this->addSql('DROP TABLE `questions_host`');
		$this->addSql('DROP TABLE `registrations_question`');
	}
}
