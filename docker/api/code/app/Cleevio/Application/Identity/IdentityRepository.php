<?php

declare(strict_types=1);

namespace Cleevio\Identity;

use Cleevio\Identity\Identities\AccountKitIdentity;
use Cleevio\Identity\Identities\ApiIdentity;
use Cleevio\Identity\Identities\FacebookIdentity;
use Cleevio\Identity\Identities\PasswordIdentity;
use Cleevio\Identity\Identities\SsoIdentity;
use Cleevio\Repository\Repositories\SqlRepository;

class IdentityRepository extends SqlRepository implements IIdentityRepository
{

	public function type(): string
	{
		return Identity::class;
	}

	/**
	 * @param string $refreshToken
	 * @return ApiIdentity|null
	 */
	public function findByRefreshToken(string $refreshToken): ?ApiIdentity
	{
		$result = $this->em->getRepository(ApiIdentity::class)->findOneBy(['refreshToken' => $refreshToken]);

		if ($result instanceof ApiIdentity) {
			return $result;
		}

		return null;
	}

	/**
	 * @param string $facebookId
	 * @return FacebookIdentity|null
	 */
	public function findByFacebookId(string $facebookId): ?FacebookIdentity
	{
		$result = $this->em->getRepository(FacebookIdentity::class)->findOneBy(['facebookId' => $facebookId]);

		if ($result instanceof FacebookIdentity) {
			return $result;
		}

		return null;
	}

	/**
	 * @param string $accountKitId
	 * @return AccountKitIdentity|null
	 */
	public function findByAccountKitId(string $accountKitId): ?AccountKitIdentity
	{
		$result = $this->em->getRepository(AccountKitIdentity::class)->findOneBy(['accountKitId' => $accountKitId]);

		if ($result instanceof AccountKitIdentity) {
			return $result;
		}

		return null;
	}

	/**
	 * @param string $hash
	 * @return PasswordIdentity|null
	 */
	public function findByPasswordHash(string $hash): ?PasswordIdentity
	{
		$result = $this->em->getRepository(PasswordIdentity::class)->findOneBy(['passwordResetHash' => $hash]);

		if ($result instanceof PasswordIdentity) {
			return $result;
		}

		return null;
	}

	/**
	 * @param string $token
	 * @return SsoIdentity|null
	 */
	public function findBySsoToken(string $token): ?SsoIdentity
	{
		$result = $this->em->getRepository(SsoIdentity::class)->findOneBy(
			[
				'accessToken' => $token,
			]);

		if ($result instanceof SsoIdentity) {
			return $result;
		}

		return null;
	}
}
