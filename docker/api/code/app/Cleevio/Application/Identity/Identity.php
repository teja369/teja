<?php

declare(strict_types=1);

namespace Cleevio\Identity;

use Cleevio\RestApi\Auth\IAuthSubject;
use Cleevio\Users\Entities\User;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 * @ORM\Table(name="identity")
 * @ORM\InheritanceType("SINGLE_TABLE")
 * @ORM\DiscriminatorColumn(name="type", type="string")
 * @ORM\DiscriminatorMap({
 *     "base" = "Identity",
 *     "password"   = "Cleevio\Identity\Identities\PasswordIdentity",
 *     "facebook"   = "Cleevio\Identity\Identities\FacebookIdentity",
 *     "accountKit" = "Cleevio\Identity\Identities\AccountKitIdentity",
 *     "api"        = "Cleevio\Identity\Identities\ApiIdentity",
 *     "sso"        = "Cleevio\Identity\Identities\SsoIdentity"
 * })
 */
abstract class Identity
{

	public const IDENTITY_API = "api";
	public const IDENTITY_PASSWORD = "password";
	public const IDENTITY_FACEBOOK = "facebook";
	public const IDENTITY_ACCOUNT_KIT = "accountKit";
	public const IDENTITY_SSO = "sso";

	/**
	 * @ORM\Id
	 * @ORM\Column(type="integer")
	 * @ORM\GeneratedValue
	 * @var int|null
	 */
	private $id;

	/**
	 * @var IAuthSubject
	 * @ORM\ManyToOne(targetEntity="Cleevio\Users\Entities\User", inversedBy="identities", cascade={"persist"})
	 */
	protected $user;

	/**
	 * Identity constructor.
	 * @param IAuthSubject $user
	 */
	public function __construct(IAuthSubject $user)
	{
		$this->user = $user;

		if ($user instanceof User) {
			$user->addIdentity($this);
		}
	}

	/**
	 * @return int|null
	 */
	final public function getId()
	{
		return $this->id;
	}


	public function __clone()
	{
		$this->id = null;
	}

	/**
	 * @return IAuthSubject
	 */
	public function getUser(): IAuthSubject
	{
		return $this->user;
	}

	/**
	 * @return string
	 */
	abstract public function getType(): string;

	/**
	 * Verify identity against credentials
	 * @param array $credentials
	 * @return bool
	 */
	abstract public function verify(array $credentials): bool;

}
