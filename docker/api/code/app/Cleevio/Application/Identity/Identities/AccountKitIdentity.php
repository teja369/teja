<?php

declare(strict_types=1);

namespace Cleevio\Identity\Identities;

use Cleevio\Identity\Identity;
use Cleevio\RestApi\Auth\IAuthSubject;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 */
class AccountKitIdentity extends Identity
{

	/**
	 * @ORM\Column(type="string", nullable=TRUE)
	 * @var string|null
	 */
	protected $accountKitId;

	/**
	 * Identity constructor.
	 * @param IAuthSubject $user
	 * @param string $accountKitId
	 */
	public function __construct(IAuthSubject $user, string $accountKitId)
	{
		parent::__construct($user);

		$this->accountKitId = $accountKitId;

	}


	/**
	 * @return string
	 */
	public function getType(): string
	{
		return self::IDENTITY_ACCOUNT_KIT;
	}

	/**
	 * Verify identity against credentials
	 * @param array $credentials
	 * @return bool
	 */
	public function verify(array $credentials): bool
	{
		unset($credentials);

		return false;
	}
}
