<?php

declare(strict_types=1);

namespace Cleevio\Identity\Identities;

use Cleevio\Identity\Identity;
use Cleevio\RestApi\Auth\IAuthSubject;
use Doctrine\ORM\Mapping as ORM;
use Ramsey\Uuid\Uuid;

/**
 * APi token identity
 * @ORM\Entity()
 */
class ApiIdentity extends Identity
{

	/**
	 * @ORM\Column(type="string", nullable=FALSE)
	 * @var string
	 */
	protected $refreshToken;

	/**
	 * ApiIdentity constructor.
	 * @param IAuthSubject $user
	 */
	public function __construct(IAuthSubject $user)
	{
		parent::__construct($user);

		try {
			$this->refreshToken = Uuid::uuid4()->toString();
		} catch (\Exception $e) {
			throw new \RuntimeException($e->getMessage());
		}
	}

	/**
	 * @return string
	 */
	public function getRefreshToken(): string
	{
		return $this->refreshToken;
	}

	/**
	 * @return string
	 */
	public function getType(): string
	{
		return self::IDENTITY_API;
	}

	/**
	 * Verify identity against credentials
	 * @param array $credentials
	 * @return bool
	 */
	public function verify(array $credentials): bool
	{
		unset($credentials);

		return false;
	}
}
