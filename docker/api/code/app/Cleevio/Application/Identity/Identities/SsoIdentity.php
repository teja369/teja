<?php

declare(strict_types=1);

namespace Cleevio\Identity\Identities;

use Cleevio\Identity\Identity;
use Cleevio\RestApi\Auth\IAuthSubject;
use DateTime;
use Doctrine\ORM\Mapping as ORM;
use Exception;
use Nette\NotImplementedException;

/**
 * @ORM\Entity()
 */
class SsoIdentity extends Identity
{

	private const EXPIRATION = 60;

	/**
	 * @ORM\Column(type="string", nullable=FALSE)
	 * @var string
	 */
	protected $accessToken;

	/**
	 * @ORM\Column(type="integer", nullable=FALSE)
	 * @var int
	 */
	protected $expires;

	/**
	 * Identity constructor.
	 * @param IAuthSubject $user
	 * @param string $token
	 * @throws Exception
	 */
	public function __construct(IAuthSubject $user, string $token)
	{
		parent::__construct($user);

		$this->accessToken = $token;
		$this->expires = (new DateTime)->getTimestamp() + self::EXPIRATION;
	}

	/**
	 * @return string
	 */
	public function getType(): string
	{
		return self::IDENTITY_SSO;
	}

	/**
	 * @return string
	 */
	public function getToken(): string
	{
		return $this->accessToken;
	}

	/**
	 * @return int
	 */
	public function getExpires(): int
	{
		return $this->expires;
	}

	/**
	 * Verify identity against credentials
	 * @param array $credentials
	 * @return bool
	 */
	public function verify(array $credentials): bool
	{
		unset($credentials);

		throw new NotImplementedException;
	}
}
