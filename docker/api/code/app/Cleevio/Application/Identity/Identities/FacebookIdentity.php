<?php

declare(strict_types=1);

namespace Cleevio\Identity\Identities;

use Cleevio\Identity\Identity;
use Cleevio\RestApi\Auth\IAuthSubject;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 */
class FacebookIdentity extends Identity
{

	/**
	 * @ORM\Column(type="string", nullable=TRUE)
	 * @var string|null
	 */
	protected $facebookId;

	/**
	 * Identity constructor.
	 * @param IAuthSubject $user
	 * @param string $facebookId
	 */
	public function __construct(IAuthSubject $user, string $facebookId)
	{
		parent::__construct($user);

		$this->facebookId = $facebookId;

	}

	/**
	 * @return string|null
	 */
	public function getFacebookId(): ?string
	{
		return $this->facebookId;
	}

	/**
	 * @return string
	 */
	public function getType(): string
	{
		return self::IDENTITY_FACEBOOK;
	}

	/**
	 * Verify identity against credentials
	 * @param array $credentials
	 * @return bool
	 */
	public function verify(array $credentials): bool
	{
		unset($credentials);

		return false;
	}
}
