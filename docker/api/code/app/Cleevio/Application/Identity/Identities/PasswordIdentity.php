<?php

declare(strict_types=1);

namespace Cleevio\Identity\Identities;

use Cleevio\Identity\Identity;
use Cleevio\RestApi\Auth\IAuthSubject;
use DateTime;
use Doctrine\ORM\Mapping as ORM;
use Nette\Security\Passwords;

/**
 * @ORM\Entity()
 */
class PasswordIdentity extends Identity
{

	/**
	 * @ORM\Column(type="string", nullable=TRUE)
	 * @var string
	 */
	protected $password;

	/**
	 * @ORM\Column(type="string", nullable=TRUE)
	 * @var string|null
	 */
	protected $passwordResetHash;

	/**
	 * @ORM\Column(type="datetime", nullable=TRUE)
	 * @var DateTime|null
	 */
	protected $lastPasswordChange;

	/**
	 * @ORM\Column(type="datetime", nullable=TRUE)
	 * @var DateTime|null
	 */
	protected $lastPasswordResetRequest;

	/**
	 * Identity constructor.
	 * @param IAuthSubject $user
	 * @param string $password
	 */
	public function __construct(IAuthSubject $user, string $password)
	{
		parent::__construct($user);

		$passwords = new Passwords;
		$this->setPassword($passwords->hash($password));

	}

	/**
	 * @return string
	 */
	public function getPassword()
	{
		return $this->password;
	}


	/**
	 * @param string $password
	 */
	public function setPassword($password): void
	{
		$this->password = $password;
	}

	/**
	 * @return string
	 */
	public function getType(): string
	{
		return self::IDENTITY_PASSWORD;
	}

	/**
	 * @return string|null
	 */
	public function getPasswordResetHash()
	{
		return $this->passwordResetHash;
	}


	/**
	 * @param string|null $passwordResetHash
	 */
	public function setPasswordResetHash($passwordResetHash): void
	{
		$this->passwordResetHash = $passwordResetHash;
	}


	/**
	 * @return DateTime|null
	 */
	public function getLastPasswordChange()
	{
		return $this->lastPasswordChange;
	}


	/**
	 * @param DateTime|null $lastPasswordChange
	 */
	public function setLastPasswordChange($lastPasswordChange): void
	{
		$this->lastPasswordChange = $lastPasswordChange;
	}


	/**
	 * @return DateTime|null
	 */
	public function getLastPasswordResetRequest()
	{
		return $this->lastPasswordResetRequest;
	}


	/**
	 * @param DateTime|null $lastPasswordResetRequest
	 */
	public function setLastPasswordResetRequest($lastPasswordResetRequest): void
	{
		$this->lastPasswordResetRequest = $lastPasswordResetRequest;
	}

	/**
	 * Verify identity against credentials
	 * @param array $credentials
	 * @return bool
	 */
	public function verify(array $credentials): bool
	{
		return (new Passwords)->verify($credentials[1], $this->getPassword());
	}
}
