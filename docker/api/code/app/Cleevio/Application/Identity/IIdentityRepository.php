<?php

declare(strict_types=1);

namespace Cleevio\Identity;

use Cleevio\Identity\Identities\AccountKitIdentity;
use Cleevio\Identity\Identities\ApiIdentity;
use Cleevio\Identity\Identities\FacebookIdentity;
use Cleevio\Identity\Identities\PasswordIdentity;
use Cleevio\Identity\Identities\SsoIdentity;
use Cleevio\Repository\IRepository;

interface IIdentityRepository extends IRepository
{

	public function findByRefreshToken(string $refreshToken): ?ApiIdentity;

	public function findByPasswordHash(string $hash): ?PasswordIdentity;

	public function findByFacebookId(string $facebookId): ?FacebookIdentity;

	public function findByAccountKitId(string $accountKitId): ?AccountKitIdentity;

	public function findBySsoToken(string $token): ?SsoIdentity;
}
