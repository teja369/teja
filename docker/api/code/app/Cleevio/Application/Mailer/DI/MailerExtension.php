<?php

declare(strict_types=1);

namespace Cleevio\Mailer\DI;

use Cleevio\Mailer\Context;
use InvalidArgumentException;
use Nette\DI\CompilerExtension;

class MailerContextExtension extends CompilerExtension
{

	/**
	 * Register services
	 *
	 * @return void
	 */
	public function loadConfiguration()
	{
		$container = $this->getContainerBuilder();
		$config = $this->getConfig();

		if (defined('NETTE_TESTER')) {
			$container->addDefinition($this->prefix('mail.context'))
				->setFactory(Context::class)
				->setArguments(["", ""]);
		} else {
			if ($config['address'] === null) {
				throw new InvalidArgumentException("System email address has not been configured");
			}

			if ($config['support'] === null) {
				throw new InvalidArgumentException("Support email address has not been configured");
			}

			$container->addDefinition($this->prefix('mail.context'))
				->setFactory(Context::class)
				->setArguments([$config['address'], $config['support']]);
		}
	}
}
