<?php

declare(strict_types=1);

namespace Cleevio\Mailer;

class Context
{
	/**
	 * @var string
	 */
	private $addressSystem;
	/**
	 * @var string
	 */
	private $addressSupport;

	/**
	 * @param string $addressSystem
	 * @param string $addressSupport
	 */
	public function __construct(string $addressSystem, string $addressSupport)
	{
		$this->addressSystem = $addressSystem;
		$this->addressSupport = $addressSupport;
	}

	public function getAddressSystem(): string
	{
		return $this->addressSystem;
	}

	public function getAddressSupport(): string
	{
		return $this->addressSupport;
	}
}
