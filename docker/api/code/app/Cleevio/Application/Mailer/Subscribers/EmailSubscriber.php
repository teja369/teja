<?php

declare(strict_types=1);

namespace Cleevio\Mailer\Subscribers;

use Cleevio\Mailer\EmailEvent;
use Cleevio\Mailer\MailBuilderFactory;
use Latte\Engine;
use Nette\Bridges\ApplicationLatte\Template;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

final class EmailSubscriber implements EventSubscriberInterface
{
	/**
	 * @var MailBuilderFactory
	 */
	private $mailBuilderFactory;

	/**
	 * @param MailBuilderFactory $mailBuilderFactory
	 */
	public function __construct(MailBuilderFactory $mailBuilderFactory)
	{
		$this->mailBuilderFactory = $mailBuilderFactory;
	}

	public static function getSubscribedEvents()
	{
		return [
			EmailEvent::class => 'send',
		];
	}

	public function send(EmailEvent $email): void
	{
		if (defined('NETTE_TESTER')) {
			return;
		}

		$builder = $this->mailBuilderFactory->create();

		$template = new Template(new Engine);
		$template->setFile(APP_DIR . "/templates/@mail.latte");

		$template->add("subject", $email->getSubject());
		$template->add("message", $email->getMessage());

		$builder->setFrom($email->getFrom());
		$builder->addTo($email->getTo());
		$builder->setTemplate($template);
		$builder->send();
	}
}
