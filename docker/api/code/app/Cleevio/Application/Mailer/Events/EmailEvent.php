<?php

declare(strict_types=1);

namespace Cleevio\Mailer;

use Symfony\Contracts\EventDispatcher\Event;

class EmailEvent extends Event
{
	/**
	 * @var string
	 */
	private $subject;

	/**
	 * @var string
	 */
	private $message;

	/**
	 * @var string
	 */
	private $to;

	/**
	 * @var string
	 */
	private $from;

	/**
	 * @param string $subject
	 * @param string $message
	 * @param string $to
	 * @param string $from
	 */
	public function __construct(string $subject, string $message, string $to, string $from)
	{
		$this->subject = $subject;
		$this->message = $message;
		$this->to = $to;
		$this->from = $from;
	}

	/**
	 * @return string
	 */
	public function getSubject(): string
	{
		return $this->subject;
	}

	/**
	 * @return string
	 */
	public function getMessage(): string
	{
		return $this->message;
	}

	/**
	 * @return string
	 */
	public function getTo(): string
	{
		return $this->to;
	}

	/**
	 * @return string
	 */
	public function getFrom(): string
	{
		return $this->from;
	}

}
