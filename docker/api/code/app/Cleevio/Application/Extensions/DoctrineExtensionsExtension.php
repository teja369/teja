<?php

declare(strict_types=1);

namespace Cleevio\Extensions;

use Doctrine\Common\Annotations\Reader;
use Gedmo\SoftDeleteable\SoftDeleteableListener;
use Gedmo\Timestampable\TimestampableListener;
use Nette\DI\CompilerExtension;
use Nette\Schema\Expect;
use Nette\Schema\Schema;

class DoctrineExtensionsExtension extends CompilerExtension
{
	public const TAG_NETTRINE_SUBSCRIBER = 'nettrine.subscriber';

	public function getConfigSchema(): Schema
	{
		return Expect::structure([
			'softDeleteable' => Expect::bool()->default(false),
			'timestampable' => Expect::bool()->default(false),
			'sortable' => Expect::bool()->default(false),
		]);
	}

	public function loadConfiguration(): void
	{
		$builder = $this->getContainerBuilder();

		if ($this->config->softDeleteable !== false) {
			$builder->addDefinition($this->prefix('softDeleteable'))
				->setFactory(SoftDeleteableListener::class)
				->addSetup('setAnnotationReader', ['@' . Reader::class])
				->addTag(self::TAG_NETTRINE_SUBSCRIBER);
		}

		if ($this->config->timestampable !== false) {
			$builder->addDefinition($this->prefix('timestampable'))
				->setFactory(TimestampableListener::class)
				->addSetup('setAnnotationReader', ['@' . Reader::class])
				->addTag(self::TAG_NETTRINE_SUBSCRIBER);
		}

		if ($this->config->sortable !== false) {
			$builder->addDefinition($this->prefix('sortable'))
				->setFactory(SoftSortableListener::class)
				->addSetup('setAnnotationReader', ['@' . Reader::class])
				->addTag(self::TAG_NETTRINE_SUBSCRIBER);
		}
	}
}
