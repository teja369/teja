<?php

declare(strict_types=1);

namespace Cleevio\Extensions;

use Doctrine\Common\Persistence\Mapping\ClassMetadata;
use Gedmo\Sortable\Mapping\Event\SortableAdapter;
use Gedmo\Sortable\SortableListener;

class SoftSortableListener extends SortableListener
{

	/**
	 * Computes node positions and updates the sort field in memory and in the db
	 *
	 * @param SortableAdapter $ea
	 * @param array $config
	 * @param ClassMetadata $meta
	 * @param object $object
	 */
	protected function processUpdate(SortableAdapter $ea, array $config, $meta, $object)
	{
		parent::processUpdate($ea, $config, $meta, $object);

		// Set new position
		if (method_exists($object, 'isDeleted') && $object->isDeleted()) {
			$this->setFieldValue($ea, $object, $config['position'], -1, -1);
		}
	}
}
