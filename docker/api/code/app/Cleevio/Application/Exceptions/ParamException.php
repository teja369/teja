<?php

declare(strict_types=1);

namespace Cleevio\Exceptions;

class ParamException extends \Exception
{

	/**
	 * @var array
	 */
	private $params;


	public function __construct(string $message = '', array $params = [])
	{
		parent::__construct($message);

		$this->params = $params;
	}


	/**
	 * @return array
	 */
	public function getParams(): array
	{
		return $this->params;
	}
}
