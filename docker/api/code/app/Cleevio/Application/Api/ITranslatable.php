<?php

declare(strict_types=1);

namespace Cleevio\Api;

interface ITranslatable
{
	/**
	 * Get accepted language
	 * @return string|null
	 */
	public function getLang(): ?string;
}
