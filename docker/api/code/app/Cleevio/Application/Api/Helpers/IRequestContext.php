<?php

declare(strict_types=1);

namespace Cleevio\Api\Helpers;

interface IRequestContext
{

	public function getLang(): ?string;

}
