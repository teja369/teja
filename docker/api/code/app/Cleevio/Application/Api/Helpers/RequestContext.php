<?php

declare(strict_types=1);

namespace Cleevio\Api\Helpers;

use Nette\Http\IRequest;

class RequestContext implements IRequestContext
{

	/**
	 * @var IRequest
	 */
	private $request;

	public function __construct(IRequest $request)
	{
		$this->request = $request;
	}

	public function getLang(): ?string
	{
		return $this->request->getHeader("Accept-Language");
	}
}
