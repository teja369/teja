<?php

declare(strict_types=1);

namespace Cleevio\Api\Translatable;

use Cleevio\Api\ITranslatable;
use Cleevio\RestApi\Request\PagedRequest;
use Cleevio\RestApi\Response\PagedResponse as BasePagedResponse;

abstract class PagedResponse extends BasePagedResponse implements ITranslatable
{
	/**
	 * @var string|null
	 */
	private $lang;

	/**
	 * @param PagedRequest $request
	 * @param string|null $lang
	 */
	public function __construct(PagedRequest $request, ?string $lang)
	{
		parent::__construct($request);

		$this->lang = $lang;
	}

	/**
	 * @return string|null
	 */
	public function getLang(): ?string
	{
		return $this->lang;
	}
}
