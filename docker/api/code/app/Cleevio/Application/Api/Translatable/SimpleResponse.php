<?php

declare(strict_types=1);

namespace Cleevio\Api\Translatable;

use Cleevio\Api\ITranslatable;
use Cleevio\RestApi\Response\SimpleResponse as BaseSimpleResponse;

abstract class SimpleResponse extends BaseSimpleResponse implements ITranslatable
{
	/**
	 * @var string|null
	 */
	private $lang;

	/**
	 * @param string|null $lang
	 */
	public function __construct(?string $lang)
	{
		$this->lang = $lang;
	}

	/**
	 * @return string|null
	 */
	public function getLang(): ?string
	{
		return $this->lang;
	}
}
