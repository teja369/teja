<?php

declare(strict_types=1);

namespace Cleevio\API\GA;

use Cleevio\ApiClient\IApiConfig;

class ConfigGAAuth implements IApiConfig
{

	/**
	 * Api base URI
	 *
	 * @example http://api.example.com/api/v1
	 * @return string
	 */
	public function getUri(): string
	{
		return env('GA_AUTH_API');
	}
}
