<?php

declare(strict_types=1);

namespace Cleevio\API\GA;

use Cleevio\ApiClient\DTO\Authorization;
use Cleevio\ApiClient\DTO\Locale;
use Cleevio\ApiClient\DTO\Subject;
use Cleevio\ApiClient\IContext;
use Nette\Http\Session;
use Nette\Http\SessionSection;
use Nette\NotImplementedException;

class ContextGAAuth implements IContext
{
	private const SESSION_SECTION_NAME = 'ga';

	private const SESSION_AUTHORIZATION_PARAM = 'auth';

	/**
	 * @var SessionSection
	 */
	private $session;


	public function __construct(Session $session)
	{
		$this->session = $session->getSection(self::SESSION_SECTION_NAME);
	}


	/**
	 * Get stored authorization credentials
	 * @return Authorization
	 */
	public function getAuthorization(): ?Authorization
	{
		if (!$this->session->offsetExists(self::SESSION_AUTHORIZATION_PARAM)) {
			return null;
		}

		$auth = @unserialize($this->session[self::SESSION_AUTHORIZATION_PARAM]);

		return $auth === false
			? null
			: $auth;
	}


	/**
	 * Set new authorization credentials
	 * @param Authorization $authorization
	 * @return void
	 */
	public function setAuthorization(Authorization $authorization): void
	{
		$this->session[self::SESSION_AUTHORIZATION_PARAM] = serialize($authorization);
	}


	/**
	 * @return Subject|null
	 */
	public function getSubject(): ?Subject
	{
		return null;
	}


	/**
	 * Set subject
	 * @param Subject $subject
	 */
	public function setSubject(Subject $subject): void
	{
		unset($subject);

		throw new NotImplementedException;
	}


	/**
	 * Get current language
	 * @return Locale|null
	 */
	public function getLocale(): ?Locale
	{
		return null;
	}


	/**
	 * Set language for translation
	 * @param Locale $locale
	 */
	public function setLocale(Locale $locale): void
	{
		unset($locale);

		throw new NotImplementedException;
	}


	/**
	 * Send locale parameter to API
	 * @return bool
	 */
	public function sendLocale(): bool
	{
		return false;
	}


	/**
	 * Clear context data
	 */
	public function clear(): void
	{
		unset($this->session[self::SESSION_AUTHORIZATION_PARAM]);
		unset($this->session[self::SESSION_SECTION_NAME]);
	}
}
