<?php

declare(strict_types=1);

namespace Cleevio\Migrations;

use Doctrine\Migrations\AbstractMigration;

abstract class Migration extends AbstractMigration
{

	/**
	 * Create key with a default translation
	 *
	 * @param string $key
	 * @param string $translation
	 * @return void
	 */
	public function createTranslation(string $key, string $translation): void
	{
		$this->write(sprintf('Translating %s', $key));
		$this->addSql('INSERT IGNORE INTO `translations_key` (`key`) VALUES (?)', [$key]);
		$this->addSql('REPLACE INTO `translations` (`language_id`, `translation_key_id`, `value`) 
							VALUES ((SELECT id FROM `languages` ORDER BY id ASC LIMIT 1), (SELECT id FROM `translations_key` WHERE `key` = ?) , ?)',
			[$key, $translation]);
	}

	/**
	 * Create country
	 *
	 * @param string $code
	 * @param string $name
	 * @return void
	 */
	public function createCountry(string $code, string $name): void
	{
		$this->write(sprintf('Creating country %s', $name));
		$this->addSql('INSERT INTO `countries` (`code`,`name`,`is_enabled`) VALUES (?,?,0) ON DUPLICATE KEY UPDATE code = ?, name = ?', [$code, $name, $code, $name]);
	}

	/**
	 * Create registration
	 *
	 * @param string $name
	 * @return void
	 */
	public function createRegistration(string $name): void
	{
		$this->write(sprintf('Creating registration %s', $name));
		$this->addSql('INSERT IGNORE INTO `registrations` (`name`) VALUES (?) ', [$name]);
	}

	/**
	 * Create role
	 *
	 * @param string $name
	 * @param string|null $parent
	 * @return void
	 */
	public function createRole(string $name, ?string $parent = null): void
	{
		$this->write(sprintf('Creating role %s', $name));

		if ($parent !== null) {
			$this->addSql('INSERT IGNORE INTO `roles` (`name`, `parent_id`) 
                                VALUES (?, (SELECT id FROM(SELECT id FROM `roles` WHERE `name` = ? LIMIT 1)t))', [$name, $parent]);
		} else {
			$this->addSql('INSERT IGNORE INTO `roles` (`name`) VALUES (?) ', [$name]);
		}
	}

	/**
	 * Create resource
	 *
	 * @param string $name
	 * @param string|null $parent
	 * @return void
	 */
	public function createResource(string $name, ?string $parent = null): void
	{
		$this->write(sprintf('Creating resource %s', $name));

		if ($parent !== null) {
			$this->addSql('INSERT IGNORE INTO `resources` (`name`, `parent_id`) 
                                VALUES (?, (SELECT id FROM(SELECT id FROM `resources` WHERE `name` = ? LIMIT 1)t))', [$name, $parent]);
		}else{
			$this->addSql('INSERT IGNORE INTO `resources` (`name`) VALUES (?) ', [$name]);
		}
	}

	/**
	 * Create resource
	 *
	 * @param string $role
	 * @param string $resource
	 * @param string $privilege |null
	 * @param bool $allow
	 * @return void
	 */
	public function createPrivilege(string $role, string $resource, ?string $privilege, bool $allow): void
	{
		$this->write(sprintf('Creating privilege %s %s', $role, $privilege));
		$this->addSql('INSERT IGNORE INTO `roles` (`name`) VALUES (?) ', [$role]);
		$this->addSql('INSERT IGNORE INTO `resources` (`name`) VALUES (?) ', [$resource]);

		$this->addSql('REPLACE INTO `privileges` (`privilege`, `allow`, `resource_id`) 
							VALUES (?, ?, (SELECT id FROM `resources`  WHERE `name` = ? LIMIT 1))',
			[$privilege, $allow, $resource]);

		$this->addSql('REPLACE INTO `roles_privilege` (`role_id`, `privilege_id`) 
							VALUES ((SELECT id FROM `roles`  WHERE `name` = ? LIMIT 1), 
							        (SELECT id FROM `privileges` WHERE `allow` = ? AND `resource_id` =  (SELECT id FROM `resources`  WHERE `name` = ? LIMIT 1) LIMIT 1))',
			[$role, $allow, $resource]);
	}

}
