<?php

declare(strict_types=1);

namespace Cleevio\Users\DI;

use Cleevio\Modules\Providers\IPresenterMappingProvider;
use Cleevio\Modules\Providers\IRouterProvider;
use Cleevio\Modules\Providers\ITranslationProvider;
use Cleevio\Users\Router\RouterFactory;
use Nette\DI\CompilerExtension;
use Nettrine\ORM\DI\Traits\TEntityMapping;

class UsersExtension extends CompilerExtension implements IPresenterMappingProvider, ITranslationProvider, IRouterProvider
{

	use TEntityMapping;

	public function loadConfiguration()
	{
		$this->compiler->loadConfig(__DIR__ . '/services.neon');
		$this->setEntityMappings(
			[
				'Cleevio\Users\Entities\User' => __DIR__ . '/..',
			]
		);
	}


	public function getPresenterMapping(): array
	{
		return ['Users' => 'Cleevio\\Users\\*Module\\Presenters\\*Presenter'];
	}


	public function getTranslationLocation(): string
	{
		return __DIR__ . '/../Translations';
	}


	public function getRouterSettings(): array
	{
		return [100 => RouterFactory::class];
	}
}
