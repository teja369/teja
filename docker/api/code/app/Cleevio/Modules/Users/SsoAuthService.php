<?php

declare(strict_types=1);

namespace Cleevio\Users;

use Cleevio\Identity\IIdentityRepository;
use Cleevio\RestApi\Auth\ApiAuthService;
use Cleevio\RestApi\Auth\AuthResponse;
use Cleevio\RestApi\Auth\ITokenProvider;
use Cleevio\RestApi\Exceptions\InvalidHeaderException;
use Cleevio\RestApi\Presenters\RestPresenter;
use DateTime;
use Exception;
use Nette\NotImplementedException;

class SsoAuthService extends ApiAuthService
{
	protected const HEADER = "Authorization";

	private const AUTHORIZATION_PREFIX = 'Bearer';

	/**
	 * @var IIdentityRepository
	 */
	private $identityRepository;

	/**
	 * @param ITokenProvider $tokenProvider
	 * @param IUserService $userService
	 * @param IIdentityRepository $identityRepository
	 */
	public function __construct(ITokenProvider $tokenProvider, IUserService $userService, IIdentityRepository $identityRepository)
	{
		parent::__construct($tokenProvider, $userService);

		$this->identityRepository = $identityRepository;
	}

	/**
	 * Login user via credentials
	 * @param RestPresenter $resource
	 * @return AuthResponse
	 * @throws Exception
	 */
	public function login(RestPresenter $resource): AuthResponse
	{
		$header = $resource->getHttpRequest()->getHeader(self::HEADER);

		if (is_null($header) || substr($header, 0, strlen(self::AUTHORIZATION_PREFIX) + 1) !== self::AUTHORIZATION_PREFIX . " ") {
			throw new InvalidHeaderException(self::HEADER);
		}

		$accessToken = substr($header, strlen(self::AUTHORIZATION_PREFIX) + 1);

		$identity = $this->identityRepository->findBySsoToken($accessToken);

		if ($identity === null) {
			throw new InvalidAccessTokenException;
		}

		try {
			if ($identity->getExpires() <= (new DateTime)->getTimestamp()) {
				throw new ExpiredAccessTokenException;
			}

			$response = $this->getResponse($identity->getUser());

			$this->identityRepository->delete($identity);

			return $response;
		} catch (Exception $ex) {
			$this->identityRepository->delete($identity);

			throw $ex;
		}
	}

	/**
	 * Refresh user token
	 * @param RestPresenter $resource
	 * @return AuthResponse
	 */
	public function refresh(RestPresenter $resource): AuthResponse
	{
		unset($resource);

		throw new NotImplementedException;
	}

}
