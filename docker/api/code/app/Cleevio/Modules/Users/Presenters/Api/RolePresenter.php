<?php

declare(strict_types=1);

namespace Cleevio\Users\ApiModule\Presenters;

use Cleevio\Repository\Constraints;
use Cleevio\RestApi\Annotations\ApiRoute;
use Cleevio\RestApi\Annotations\Response;
use Cleevio\RestApi\Presenters\RestPresenter;
use Cleevio\Users\Api\Response\ResourcesResponseBuilder;
use Cleevio\Users\Api\Response\RolesResponseBuilder;
use Cleevio\Users\Repositories\IResourceRepository;
use Cleevio\Users\Repositories\IRoleRepository;
use Nette\Http\IResponse;

/**
 * @ApiRoute("/api/v1/user/role",presenter="Users:Api:Role")
 */
class RolePresenter extends RestPresenter
{
	/**
	 * @var IRoleRepository
	 */
	private $roleRepository;
	/**
	 * @var IResourceRepository
	 */
	private $resourceRepository;

	/**
	 * @param IRoleRepository $roleRepository
	 * @param IResourceRepository $resourceRepository
	 */
	public function __construct(IRoleRepository $roleRepository, IResourceRepository $resourceRepository)
	{
		parent::__construct();

		$this->roleRepository = $roleRepository;
		$this->resourceRepository = $resourceRepository;
	}


	/**
	 * @ApiRoute("/api/v1/user/role", method="GET", description="List all roles")
	 * @Response(200, file="response/users/roles.json")
	 */
	public function actionGetRoles(): void
	{
		$roles = $this->roleRepository->findAll(Constraints::empty());

		$this->sendSuccessResponse(new RolesResponseBuilder($roles), IResponse::S200_OK);
	}

	/**
	 * @ApiRoute("/api/v1/user/resource", method="GET", description="List all resources")
	 * @Response(200, file="response/users/resources.json")
	 */
	public function actionGetResources(): void
	{
		$roles = $this->resourceRepository->findAll(Constraints::empty());

		$this->sendSuccessResponse(new ResourcesResponseBuilder($roles), IResponse::S200_OK);
	}

}
