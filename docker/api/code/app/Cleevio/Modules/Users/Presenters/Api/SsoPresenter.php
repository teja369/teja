<?php

declare(strict_types=1);

namespace Cleevio\Users\ApiModule\Presenters;

use Cleevio\RestApi\Annotations\ApiRoute;
use Cleevio\RestApi\Annotations\Header;
use Cleevio\RestApi\Annotations\Response;
use Cleevio\RestApi\Presenters\RestPresenter;
use Cleevio\Users\SsoAuthService;
use Exception;
use Nette\Http\IResponse;

/**
 * @ApiRoute("/api/v1/sso",presenter="Users:Api:Sso")
 */
class SsoPresenter extends RestPresenter
{

	/**
	 * @var SsoAuthService
	 */
	private $authService;


	public function __construct(SsoAuthService $authService)
	{
		parent::__construct();

		$this->authService = $authService;
	}

	/**
	 * @ApiRoute("/api/v1/sso/login", method="POST", description="Login user using SSO token")
	 * @Response(200, file="response/users/login.json")
	 * @Header(name="Authorization", description="Bearer with SSO token")
	 * @Response(400)
	 * @Response(401)
	 * @throws Exception
	 */
	public function actionLogin(): void
	{
		$response = $this->authService->login($this);

		$this->sendSuccessResponse($response, IResponse::S200_OK);
	}

}
