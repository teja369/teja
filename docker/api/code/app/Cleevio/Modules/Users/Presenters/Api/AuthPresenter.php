<?php

declare(strict_types=1);

namespace Cleevio\Users\ApiModule\Presenters;

use Cleevio\RestApi\Annotations\ApiRoute;
use Cleevio\RestApi\Annotations\Header;
use Cleevio\RestApi\Annotations\Response;
use Cleevio\RestApi\Auth\Service\Basic\BasicAuthService;
use Cleevio\RestApi\Presenters\RestPresenter;
use Nette\Http\IResponse;

/**
 * @ApiRoute("/api/v1/auth",presenter="Users:Api:Auth")
 */
class AuthPresenter extends RestPresenter
{

	/**
	 * @var BasicAuthService
	 */
	private $basicAuthService;


	public function __construct(BasicAuthService $basicAuthService)
	{
		parent::__construct();
		$this->basicAuthService = $basicAuthService;
	}


	/**
	 * @ApiRoute("/api/v1/auth/login", method="POST", description="Login user using auth header")
	 * @Response(200, file="response/users/login.json")
	 * @Response(400, code="0", description="Bad request.")
	 * @Response(401, code="802", description="User not found.")
	 * @Header("Authorization", description="Basic base64 username:password", required=true)
	 */
	public function actionLogin(): void
	{
		$response = $this->basicAuthService->login($this);

		$this->sendSuccessResponse($response, IResponse::S200_OK);
	}

	/**
	 * @ApiRoute("/api/v1/auth/refresh", method="POST", description="Refresh user using auth header")
	 * @Response(200, file="response/users/refresh.json")
	 * @Response(400, code="0", description="Bad request.")
	 * @Response(401, code="802", description="User not found.")
	 * @Header("Authorization", description="Bearer refreshToken", required=true)
	 */
	public function actionRefresh(): void
	{
		$response = $this->basicAuthService->refresh($this);

		$this->sendSuccessResponse($response, IResponse::S200_OK);
	}

}
