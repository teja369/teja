<?php

declare(strict_types=1);

namespace Cleevio\Users\ApiModule\Presenters;

use Cleevio\RestApi\Annotations\ApiRoute;
use Cleevio\RestApi\Annotations\Header;
use Cleevio\RestApi\Annotations\Response;
use Cleevio\RestApi\Presenters\RestPresenter;
use Cleevio\Users\SamlAuthService;
use Exception;
use Nette\Http\IResponse;

/**
 * @ApiRoute("/api/v1/sso",presenter="Users:Api:Saml")
 */
class SamlPresenter extends RestPresenter
{

	/**
	 * @var SamlAuthService
	 */
	private $authService;


	public function __construct(SamlAuthService $authService)
	{
		parent::__construct();

		$this->authService = $authService;
	}

	/**
	 * @ApiRoute("/api/v1/saml/login", method="POST", description="Login user using SAML response")
	 * @Response(200, file="response/users/saml.json")
	 * @Header(name="Authorization", description="Bearer with SAML response base64+url encoded")
	 * @Response(400)
	 * @Response(401)
	 * @throws Exception
	 */
	public function actionLogin(): void
	{
		$response = $this->authService->login($this);

		$this->sendSuccessResponse($response, IResponse::S200_OK);
	}

}
