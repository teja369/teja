<?php

declare(strict_types=1);

namespace Cleevio\Users\ApiModule\Presenters;

use Cleevio\Api\Helpers\IRequestContext;
use Cleevio\Repository\Constraints;
use Cleevio\RestApi\Annotations\ApiRoute;
use Cleevio\RestApi\Annotations\Authenticated;
use Cleevio\RestApi\Annotations\Response;
use Cleevio\RestApi\Exceptions\BadRequestException;
use Cleevio\RestApi\Presenters\RestPresenter;
use Cleevio\RestApi\Request\PagedRequest;
use Cleevio\Users\Api\Response\UsersResponseBuilder;
use Cleevio\Users\Entities\TravelAssistant;
use Cleevio\Users\Entities\User;
use Cleevio\Users\Repositories\ITravelAssistantRepository;
use Cleevio\Users\Repositories\TravelAssistantConstraints;
use Nette\Http\IResponse;

/**
 * @ApiRoute("/api/v1/assistant",presenter="Users:Api:Assistant")
 */
class AssistantPresenter extends RestPresenter
{
	/**
	 * @var IRequestContext
	 */
	private $requestContext;

	/**
	 * @var ITravelAssistantRepository
	 */
	private $travelAssistantRepository;


	public function __construct(
		IRequestContext $requestContext,
		ITravelAssistantRepository $travelAssistantRepository
	)
	{
		parent::__construct();

		$this->requestContext = $requestContext;
		$this->travelAssistantRepository = $travelAssistantRepository;
	}

	/**
	 * @ApiRoute("/api/v1/user/assistant", method="GET", description="Get assistants for traveller")
	 * @Response(200, file="response/users/user.json")
	 * @Response(401, description="User not found.")
	 * @Authenticated()
	 */
	public function actionGetAssistantList(): void
	{
		$authenticator = $this->getAuthenticator();

		$user = $authenticator->getUser();

		if (!$user instanceof User) {
			throw new BadRequestException;
		}

		$request = PagedRequest::of($this->getHttpRequest());

		$travelAssistantConstraints = new TravelAssistantConstraints;
		$travelAssistantConstraints->setTraveller($user);

		$constraints = $request->toConstraints($travelAssistantConstraints)->build();

		$result = $this->travelAssistantRepository->findAll($constraints);
		$current = $this->travelAssistantRepository->count($constraints);
		$total = $this->travelAssistantRepository->count(Constraints::empty());

		$assistants = array_map(static function (TravelAssistant $travelAssistant) {
			return $travelAssistant->getAssistant();
		}, $result);

		$response = (new UsersResponseBuilder($request, $assistants, $this->requestContext->getLang()))
			->setCurrent($current)
			->setTotal($total);

		$this->sendSuccessResponse($response, IResponse::S200_OK);

	}


	/**
	 * @ApiRoute("/api/v1/assistant/user", method="GET", description="Get travellers for assistant")
	 * @Response(200, file="response/users/user.json")
	 * @Response(401, description="User not found.")
	 * @Authenticated()
	 */
	public function actionGetUsersList(): void
	{
		$authenticator = $this->getAuthenticator();

		$user = $authenticator->getUser();

		if (!$user instanceof User) {
			throw new BadRequestException;
		}

		$travelAssistantConstraints = new TravelAssistantConstraints;

		$travelAssistantConstraints->setAssistant($user);

		$request = PagedRequest::of($this->getHttpRequest());
		$constraints = $request->toConstraints($travelAssistantConstraints)->build();

		$result = $this->travelAssistantRepository->findAll($constraints);
		$filtered = $this->travelAssistantRepository->count($constraints);
		$total = $this->travelAssistantRepository->count(Constraints::empty());

		$travellers = array_map(static function (TravelAssistant $travelAssistant) {
			return $travelAssistant->getTraveller();
		}, $result);

		$response = (new UsersResponseBuilder($request, $travellers, $this->requestContext->getLang()))
			->setCurrent($filtered)
			->setTotal($total);

		$this->sendSuccessResponse($response, IResponse::S200_OK);
	}

}
