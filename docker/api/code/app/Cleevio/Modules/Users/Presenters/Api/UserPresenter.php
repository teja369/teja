<?php

declare(strict_types=1);

namespace Cleevio\Users\ApiModule\Presenters;

use Cleevio\Api\Helpers\IRequestContext;
use Cleevio\RestApi\Annotations\ApiRoute;
use Cleevio\RestApi\Annotations\Authenticated;
use Cleevio\RestApi\Annotations\Response;
use Cleevio\RestApi\Exceptions\BadRequestException;
use Cleevio\RestApi\Presenters\RestPresenter;
use Cleevio\Users\Api\Response\UserResponseBuilder;
use Cleevio\Users\Entities\User;
use Nette\Http\IResponse;

/**
 * @ApiRoute("/api/v1/user",presenter="Users:Api:User")
 */
class UserPresenter extends RestPresenter
{
	/**
	 * @var IRequestContext
	 */
	private $requestContext;

	public function __construct(IRequestContext $requestContext)
	{
		parent::__construct();

		$this->requestContext = $requestContext;
	}

	/**
	 * @ApiRoute("/api/v1/user", method="GET", description="Get user information")
	 * @Response(200, file="response/users/user.json")
	 * @Response(401, description="User not found.")
	 * @Authenticated()
	 */
	public function actionGetUser(): void
	{
		$authenticator = $this->getAuthenticator();

		$user = $authenticator->getUser();

		if (!$user instanceof User) {
			throw new BadRequestException;
		}

		$this->sendSuccessResponse(new UserResponseBuilder($user, $this->requestContext->getLang()), IResponse::S200_OK);
	}

}
