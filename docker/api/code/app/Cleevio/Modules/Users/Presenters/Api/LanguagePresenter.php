<?php

declare(strict_types=1);

namespace Cleevio\Users\ApiModule\Presenters;

use Cleevio\Api\Helpers\IRequestContext;
use Cleevio\Languages\ILanguageService;
use Cleevio\RestApi\Annotations\ApiRoute;
use Cleevio\RestApi\Annotations\Authenticated;
use Cleevio\RestApi\Annotations\Response;
use Cleevio\RestApi\Exceptions\BadRequestException;
use Cleevio\RestApi\Exceptions\NotFoundException;
use Cleevio\RestApi\Presenters\RestPresenter;
use Cleevio\Users\Api\Response\UserResponseBuilder;
use Cleevio\Users\Entities\User;
use Cleevio\Users\Repositories\IUserRepository;
use Nette\Http\IResponse;

/**
 * @ApiRoute("/api/v1/user",presenter="Users:Api:Language")
 */
class LanguagePresenter extends RestPresenter
{
	/**
	 * @var ILanguageService
	 */
	private $languageService;
	/**
	 * @var IUserRepository
	 */
	private $userRepository;

	/**
	 * @var IRequestContext
	 */
	private $requestContext;

	/**
	 * LanguagePresenter constructor.
	 * @param ILanguageService $languageService
	 * @param IUserRepository $userRepository
	 * @param IRequestContext $requestContext
	 */
	public function __construct(
		ILanguageService $languageService,
		IUserRepository $userRepository,
		IRequestContext $requestContext
	)
	{
		parent::__construct();

		$this->languageService = $languageService;
		$this->userRepository = $userRepository;
		$this->requestContext = $requestContext;
	}


	/**
	 * @ApiRoute("/api/v1/user/language/<language>", method="PUT", description="Set user language information")
	 * @Response(200, file="response/users/user.json")
	 * @Response(401, description="User not found.")
	 * @Response(404, code="10001", description="Language was not found")
	 * @Authenticated()
	 * @param string $language
	 */
	public function actionPutLanguage(string $language): void
	{
		$authenticator = $this->getAuthenticator();

		$user = $authenticator->getUser();

		if (!$user instanceof User) {
			throw new BadRequestException;
		}

		$language = $this->languageService->getLanguage($language);

		if ($language === null) {
			throw new NotFoundException('languages.language.get.not-found', 10001);
		}

		$user->setLanguage($language);
		$this->userRepository->persist($user);

		$this->sendSuccessResponse(new UserResponseBuilder($user, $this->requestContext->getLang()), IResponse::S200_OK);
	}

}
