<?php

declare(strict_types=1);

namespace Cleevio\Users\Api\Response;

use Cleevio\Api\Translatable\PagedResponse;
use Cleevio\RestApi\Request\PagedRequest;
use Cleevio\Users\Entities\User;

final class UsersResponseBuilder extends PagedResponse
{
	/**
	 * @var array
	 */
	private $data;

	/**
	 * UsersResponseBuilder constructor.
	 * @param PagedRequest $request
	 * @param array $data
	 * @param string|null $lang
	 */
	public function __construct(PagedRequest $request, array $data, ?string $lang)
	{
		parent::__construct($request, $lang);

		$this->data = $data;
	}

	/**
	 * @return array
	 */
	protected function data(): array
	{
		return array_map(function (User $user) {
			return (new UserResponseBuilder($user, $this->getLang()))->build();
		}, $this->data);
	}

}
