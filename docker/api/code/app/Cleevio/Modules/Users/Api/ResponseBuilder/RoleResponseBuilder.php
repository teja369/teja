<?php

declare(strict_types=1);

namespace Cleevio\Users\Api\Response;

use Cleevio\Acl\Models\IPrivilege;
use Cleevio\Acl\Models\IRole;
use Cleevio\RestApi\Response\SimpleResponse;

final class RoleResponseBuilder extends SimpleResponse
{
	/**
	 * @var IRole
	 */
	private $data;

	/**
	 * UserResponseBuilder constructor.
	 * @param IRole $role
	 */
	public function __construct(IRole $role)
	{
		$this->data = $role;
	}


	/**
	 * @return array
	 */
	protected function data(): array
	{
		return [
			'name' => $this->data->getName(),
			'parent' => $this->data->getParent() === null ? null : (new RoleResponseBuilder($this->data->getParent()))->build(),
			'privileges' => array_map(static function (IPrivilege $privilege) {
				return (new PrivilegeResponseBuilder($privilege))->build();
			}, $this->data->getPrivileges()),
		];
	}
}
