<?php

declare(strict_types=1);

namespace Cleevio\Users\Api\Response;

use Cleevio\Api\Translatable\SimpleResponse;
use Cleevio\Languages\Api\Response\LanguageResponseBuilder;
use Cleevio\Users\Entities\User;

final class UserResponseBuilder extends SimpleResponse
{
	/**
	 * @var User
	 */
	private $data;

	/**
	 * UserResponseBuilder constructor.
	 * @param User $user
	 * @param string|null $lang
	 */
	public function __construct(User $user, ?string $lang)
	{
		parent::__construct($lang);

		$this->data = $user;
	}


	/**
	 * @return array
	 */
	protected function data(): array
	{
		return [
			'id' => $this->data->getId(),
			'name' => $this->data->getName(),
			'username' => $this->data->getUsername(),
			'email' => $this->data->getEmail(),
			'role' => (new SimpleRolesResponseBuilder($this->data->getRoles()))->build(),
			'language' => $this->data->getLanguage() !== null ? (new LanguageResponseBuilder($this->data->getLanguage(), $this->getLang()))->build() : null,
		];
	}
}
