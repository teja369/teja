<?php

declare(strict_types=1);

namespace Cleevio\Users\Api\Response;

use Cleevio\RestApi\Auth\AuthResponse;

final class SsoResponseBuilder extends AuthResponse
{

	/**
	 * @var string
	 */
	private $accessToken;

	/**
	 * @var int
	 */
	private $expires;

	/**
	 * @var array
	 */
	private $attributes;

	/**
	 * UserResponseBuilder constructor.
	 * @param string $accessToken
	 * @param int $expires
	 * @param array $attributes
	 */
	public function __construct(string $accessToken, int $expires, array $attributes)
	{
		parent::__construct($accessToken, $expires, "");

		$this->accessToken = $accessToken;
		$this->expires = $expires;
		$this->attributes = $attributes;
	}

	/**
	 * @return array
	 */
	protected function data(): array
	{
		return array_merge([
			"accessToken" => $this->accessToken,
			"expires" => $this->expires,
		], $this->attributes);
	}
}
