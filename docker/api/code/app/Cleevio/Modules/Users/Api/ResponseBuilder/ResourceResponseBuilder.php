<?php

declare(strict_types=1);

namespace Cleevio\Users\Api\Response;

use Cleevio\Acl\Models\IResource;
use Cleevio\RestApi\Response\SimpleResponse;

final class ResourceResponseBuilder extends SimpleResponse
{
	/**
	 * @var IResource
	 */
	private $data;

	/**
	 * ResourceResponseBuilder constructor.
	 * @param IResource $role
	 */
	public function __construct(IResource $role)
	{
		$this->data = $role;
	}


	/**
	 * @return array
	 */
	protected function data(): array
	{
		return [
			'name' => $this->data->getName(),
			'parent' => $this->data->getParent() === null ? null : (new ResourceResponseBuilder($this->data->getParent()))->build(),
		];
	}
}
