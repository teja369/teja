<?php

declare(strict_types=1);

namespace Cleevio\Users\Api\Response;

use Cleevio\Acl\Models\IResource;
use Cleevio\Acl\Models\IRole;
use Cleevio\RestApi\Response\SimpleResponse;

final class ResourcesResponseBuilder extends SimpleResponse
{
	/**
	 * @var array
	 */
	private $data;

	/**
	 * ResourcesResponseBuilder constructor.
	 * @param IRole[] $data
	 */
	public function __construct(array $data)
	{
		$this->data = $data;
	}

	/**
	 * @return array
	 */
	protected function data(): array
	{
		return array_map(static function (IResource $role) {
			return (new ResourceResponseBuilder($role))->build();
		}, $this->data);
	}

}
