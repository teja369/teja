<?php

declare(strict_types=1);

namespace Cleevio\Users\Api\Response;

use Cleevio\Acl\Models\IPrivilege;
use Cleevio\RestApi\Response\SimpleResponse;

final class PrivilegeResponseBuilder extends SimpleResponse
{
	/**
	 * @var IPrivilege
	 */
	private $data;

	/**
	 * UserResponseBuilder constructor.
	 * @param IPrivilege $privilege
	 */
	public function __construct(IPrivilege $privilege)
	{
		$this->data = $privilege;
	}


	/**
	 * @return array
	 */
	protected function data(): array
	{
		return [
			'privilege' => $this->data->getPrivilege(),
			'resource' => (new ResourceResponseBuilder($this->data->getResource()))->build(),
			'allow' => $this->data->allow(),
		];
	}
}
