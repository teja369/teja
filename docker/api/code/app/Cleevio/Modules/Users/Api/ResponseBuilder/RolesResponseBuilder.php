<?php

declare(strict_types=1);

namespace Cleevio\Users\Api\Response;

use Cleevio\Acl\Models\IRole;
use Cleevio\RestApi\Response\SimpleResponse;

final class RolesResponseBuilder extends SimpleResponse
{
	/**
	 * @var array
	 */
	private $data;

	/**
	 * UsersResponseBuilder constructor.
	 * @param IRole[] $data
	 */
	public function __construct(array $data)
	{
		$this->data = $data;
	}

	/**
	 * @return array
	 */
	protected function data(): array
	{
		return array_map(static function (IRole $role) {
			return (new RoleResponseBuilder($role))->build();
		}, $this->data);
	}

}
