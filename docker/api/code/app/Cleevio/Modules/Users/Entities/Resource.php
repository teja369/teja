<?php

declare(strict_types=1);

namespace Cleevio\Users\Entities;

use Cleevio\Acl\Models\IModel;
use Cleevio\Acl\Models\IResource as IResourceModel;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 * @ORM\Table(name="resources")
 */
class Resource implements IResourceModel
{
	/**
	 * @ORM\Id
	 * @ORM\Column(type="integer")
	 * @ORM\GeneratedValue
	 * @var int|null
	 */
	private $id;

	/**
	 * @ORM\Column(type="string", nullable=false)
	 * @var string
	 */
	protected $name;

	/**
	 * @ORM\ManyToOne(targetEntity="Cleevio\Users\Entities\Resource")
	 * @ORM\JoinColumn(name="parent_id", referencedColumnName="id", nullable=true)
	 * @var IResourceModel|null
	 */
	protected $parent;

	/**
	 * @param string $name
	 */
	public function __construct(string $name)
	{
		$this->name = $name;
	}

	/**
	 * @return int|null
	 */
	final public function getId()
	{
		return $this->id;
	}

	/**
	 * Permission name
	 *
	 * @return string
	 */
	function getName(): string
	{
		return $this->name;
	}

	/**
	 * Another resource that this resource inherits from
	 * @return IResourceModel|null
	 */
	function getParent(): ?IModel
	{
		return $this->parent;
	}
}
