<?php

declare(strict_types=1);

namespace Cleevio\Users\Entities;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 * @ORM\Table(name="travel_assistant")
 */
class TravelAssistant
{

	/**
	 * @ORM\Id
	 * @ORM\Column(type="integer")
	 * @ORM\GeneratedValue
	 * @var int|null
	 */
	private $id;

	/**
	 * @ORM\ManyToOne(targetEntity="Cleevio\Users\Entities\User")
	 * @ORM\JoinColumn(name="traveller_id", referencedColumnName="id", nullable=false)
	 * @var User
	 */
	private $traveller;

	/**
	 * @ORM\ManyToOne(targetEntity="Cleevio\Users\Entities\User")
	 * @ORM\JoinColumn(name="assistant_id", referencedColumnName="id", nullable=false)
	 * @var User
	 */
	private $assistant;


	public function __construct(
		User $traveller,
		User $assistant
	)
	{
		$this->traveller = $traveller;
		$this->assistant = $assistant;
	}


	/**
	 * @return int|null
	 */
	public function getId(): ?int
	{
		return $this->id;
	}


	/**
	 * @return User
	 */
	public function getTraveller(): User
	{
		return $this->traveller;
	}


	/**
	 * @param User $traveller
	 */
	public function setTraveller(User $traveller): void
	{
		$this->traveller = $traveller;
	}


	/**
	 * @return User
	 */
	public function getAssistant(): User
	{
		return $this->assistant;
	}


	/**
	 * @param User $assistant
	 */
	public function setAssistant(User $assistant): void
	{
		$this->assistant = $assistant;
	}

}
