<?php

declare(strict_types=1);

namespace Cleevio\Users\Entities;

use Cleevio\Acl\Models\IModel;
use Cleevio\Acl\Models\IPrivilege;
use Cleevio\Acl\Models\IRole;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 * @ORM\Table(name="roles")
 */
class Role implements IRole
{
	/**
	 * @ORM\Id
	 * @ORM\Column(type="integer")
	 * @ORM\GeneratedValue
	 * @var int|null
	 */
	private $id;

	/**
	 * @ORM\Column(type="string", nullable=false, unique=true)
	 * @var string
	 */
	protected $name;

	/**
	 * @ORM\ManyToOne(targetEntity="Cleevio\Users\Entities\Role")
	 * @ORM\JoinColumn(name="parent_id", referencedColumnName="id", nullable=true)
	 * @var Role|null
	 */
	protected $parent;

	/**
	 * @ORM\ManyToMany(targetEntity="Cleevio\Users\Entities\Privilege")
	 * @ORM\JoinTable(name="roles_privilege",
	 *      joinColumns={@ORM\JoinColumn(name="role_id", referencedColumnName="id")},
	 *      inverseJoinColumns={@ORM\JoinColumn(name="privilege_id", referencedColumnName="id")}
	 *      )
	 * @var ArrayCollection
	 */
	protected $privileges;

	/**
	 * @ORM\ManyToMany(targetEntity="Cleevio\Users\Entities\User")
	 * @ORM\JoinTable(name="users_role",
	 *      joinColumns={@ORM\JoinColumn(name="user_id", referencedColumnName="id")},
	 *      inverseJoinColumns={@ORM\JoinColumn(name="role_id", referencedColumnName="id")}
	 *      )
	 * @var ArrayCollection
	 */
	protected $users;

	/**
	 * @param string $name
	 */
	public function __construct(string $name)
	{
		$this->name = $name;
		$this->privileges = new ArrayCollection;
	}

	/**
	 * @return int|null
	 */
	final public function getId()
	{
		return $this->id;
	}

	/**
	 * Role name
	 *
	 * @return string
	 */
	function getName(): string
	{
		return $this->name;
	}

	/**
	 * @return IPrivilege[]
	 */
	function getPrivileges(): array
	{
		return $this->privileges->getValues();
	}

	/**
	 * Another role that this role inherits from
	 * @return IRole|null
	 */
	function getParent(): ?IModel
	{
		return $this->parent;
	}
}
