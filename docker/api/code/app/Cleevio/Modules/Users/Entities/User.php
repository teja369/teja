<?php

declare(strict_types=1);

namespace Cleevio\Users\Entities;

use Cleevio\Acl\Models\IAclSubject;
use Cleevio\Acl\Models\IPrivilege;
use Cleevio\Acl\Models\IRole;
use Cleevio\Identity\Identity;
use Cleevio\Languages\Entities\Language;
use Cleevio\RestApi\Auth\IAuthSubject;
use Cleevio\Trips\Entities\Trip;
use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 * @ORM\Table(name="users")
 */
class User implements IAuthSubject, IAclSubject
{
	/**
	 * @ORM\Id
	 * @ORM\Column(type="integer")
	 * @ORM\GeneratedValue
	 * @var int|null
	 */
	private $id;

	/**
	 * @ORM\Column(type="string", nullable=true, unique=true)
	 * @var string|null
	 */
	protected $email;

	/**
	 * @ORM\Column(type="string", nullable=false, unique=true)
	 * @var string
	 */
	protected $username;

	/**
	 * @ORM\Column(type="string", nullable=true)
	 * @var string|null
	 */
	protected $name;

	/**
	 * @ORM\Column(type="string", nullable=true)
	 * @var string|null
	 */
	protected $lastName;

	/**
	 * @ORM\Column(type="string", nullable=true)
	 * @var string|null
	 */
	protected $firstName;

	/**
	 * @ORM\ManyToMany(targetEntity="Cleevio\Users\Entities\Role", cascade={"all"}, orphanRemoval=true)
	 * @ORM\JoinTable(name="users_role",
	 *      joinColumns={@ORM\JoinColumn(name="user_id", referencedColumnName="id")},
	 *      inverseJoinColumns={@ORM\JoinColumn(name="role_id", referencedColumnName="id")}
	 *      )
	 * @var ArrayCollection
	 */
	protected $roles;

	/**
	 * @ORM\ManyToOne(targetEntity="Cleevio\Languages\Entities\Language")
	 * @ORM\JoinColumn(name="language_id", referencedColumnName="id", nullable=true)
	 * @var Language|null
	 */
	protected $language;

	/**
	 * @var ArrayCollection|null
	 * @ORM\OneToMany(targetEntity="Cleevio\Identity\Identity", mappedBy="user", cascade={"persist"}, orphanRemoval=true)
	 */
	protected $identities;

	/**
	 * @var ArrayCollection|null
	 * @ORM\OneToMany(targetEntity="Cleevio\Users\Entities\TravelAssistant", mappedBy="traveller", cascade={"all"}, orphanRemoval=true)
	 */
	protected $assistants;

	/**
	 * @var ArrayCollection|null
	 * @ORM\OneToMany(targetEntity="Cleevio\Users\Entities\TravelAssistant", mappedBy="assistants", cascade={"all"}, orphanRemoval=true)
	 */
	protected $travellers;

	/**
	 * @var ArrayCollection
	 * @ORM\OneToMany(targetEntity="Cleevio\Trips\Entities\Trip", mappedBy="user", cascade={"persist"})
	 */
	protected $trips;

	/**
	 * @ORM\Column(type="datetime", nullable=false)
	 * @var DateTime
	 */
	protected $created;

	/**
	 * @param string $username
	 * @throws \Exception
	 */
	public function __construct(string $username)
	{
		$this->username = $username;
		$this->identities = new ArrayCollection;
		$this->trips = new ArrayCollection;
		$this->roles = new ArrayCollection;
		$this->assistants = new ArrayCollection;
		$this->travellers = new ArrayCollection;
		$this->setCreated(new DateTime);
	}

	/**
	 * @return int|null
	 */
	final public function getId()
	{
		return $this->id;
	}


	public function __clone()
	{
		$this->id = null;
	}


	/**
	 * @return string|null
	 */
	public function getEmail(): ?string
	{
		return $this->email;
	}


	/**
	 * @param string|null $email
	 */
	public function setEmail(?string $email): void
	{
		$this->email = $email;
	}


	/**
	 * @return string|null
	 */
	public function getName()
	{
		return $this->firstName . ' ' . $this->lastName;
	}


	/**
	 * @param string|null $name
	 */
	public function setName($name): void
	{
		$this->name = $name;
	}

	/**
	 * @return string|null
	 */
	public function getLastName(): ?string
	{
		return $this->lastName;
	}

	/**
	 * @param string|null $lastName
	 */
	public function setLastName(?string $lastName): void
	{
		$this->lastName = $lastName;
	}

	/**
	 * @return string|null
	 */
	public function getFirstName(): ?string
	{
		return $this->firstName;
	}

	/**
	 * @param string|null $firstName
	 */
	public function setFirstName(?string $firstName): void
	{
		$this->firstName = $firstName;
	}


	/**
	 * @return IRole[]
	 */
	function getRoles(): array
	{
		return $this->roles->getValues();
	}

	/**
	 * @return IPrivilege[]
	 */
	function getPrivileges(): array
	{
		return [];
	}

	/**
	 * @return Language|null
	 */
	public function getLanguage(): ?Language
	{
		return $this->language;
	}

	/**
	 * @param Language|null $language
	 */
	public function setLanguage(?Language $language): void
	{
		$this->language = $language;
	}

	/**
	 * @return DateTime
	 */
	public function getCreated()
	{
		return $this->created;
	}


	/**
	 * @param DateTime $created
	 */
	public function setCreated($created): void
	{
		$this->created = $created;
	}

	/**
	 * @param string $type
	 * @return Identity|null
	 */
	final public function getIdentity(string $type): ?Identity
	{
		if (!is_null($this->identities)) {
			$first = $this->identities
				->filter(static function (Identity $identity) use ($type) {
					return $identity->getType() === $type;
				})
				->first();

			return $first === false
				? null
				: $first;
		}

		return null;
	}


	/**
	 * @param Identity $identity
	 */
	final public function addIdentity(Identity $identity): void
	{
		if (is_null($this->identities)) {
			$this->identities = new ArrayCollection;
		}

		$this->identities->add($identity);
	}


	/**
	 * Verify identity against credentials
	 * @param string $identity
	 * @param array $credentials
	 * @return bool
	 */
	public function verify(string $identity, array $credentials): bool
	{
		$identity = $this->getIdentity($identity);

		if (!is_null($identity)) {
			return $identity->verify($credentials);
		}

		return false;
	}


	/**
	 * @return string
	 */
	public function getUsername(): string
	{
		return $this->username;
	}


	/**
	 * @param string $username
	 */
	public function setUsername(string $username): void
	{
		$this->username = $username;
	}


	/**
	 * @return Trip[]
	 */
	public function getTrips(): array
	{
		return $this->trips->getValues();
	}


	/**
	 * @param Role[] $roles
	 */
	public function setRole(array $roles): void
	{
		$this->roles = new ArrayCollection($roles);
	}

}
