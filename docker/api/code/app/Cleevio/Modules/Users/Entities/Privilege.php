<?php

declare(strict_types=1);

namespace Cleevio\Users\Entities;

use Cleevio\Acl\Models\IPrivilege;
use Cleevio\Acl\Models\IResource;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 * @ORM\Table(name="privileges")
 */
class Privilege implements IPrivilege
{
	/**
	 * @ORM\Id
	 * @ORM\Column(type="integer")
	 * @ORM\GeneratedValue
	 * @var int|null
	 */
	private $id;

	/**
	 * @ORM\ManyToOne(targetEntity="Cleevio\Users\Entities\Resource")
	 * @ORM\JoinColumn(name="resource_id", referencedColumnName="id", nullable=false)
	 * @var IResource
	 */
	protected $resource;

	/**
	 * @ORM\Column(type="string", nullable=true)
	 * @var string|null
	 */
	protected $privilege = IPrivilege::PRIVILEGE_ALL;

	/**
	 * @ORM\Column(type="boolean", nullable=false)
	 * @var bool
	 */
	protected $allow = false;

	public function __construct(Resource $resource)
	{
		$this->resource = $resource;
	}

	/**
	 * @return int|null
	 */
	final public function getId()
	{
		return $this->id;
	}

	/**
	 * @return IResource
	 */
	function getResource(): IResource
	{
		return $this->resource;
	}

	/**
	 * @return string|null
	 */
	function getPrivilege(): ?string
	{
		return $this->privilege;
	}

	/**
	 * Allow or deny access
	 * @return bool
	 */
	function allow(): bool
	{
		return $this->allow;
	}
}
