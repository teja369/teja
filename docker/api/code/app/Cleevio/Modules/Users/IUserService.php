<?php

declare(strict_types=1);

namespace Cleevio\Users;

use Cleevio\Identity\Identities\PasswordIdentity;
use Cleevio\Identity\Identities\SsoIdentity;
use Cleevio\RestApi\Auth\IApiUserService;
use Cleevio\RestApi\Auth\ITokenProvider;
use Cleevio\RestApi\Auth\Service\AccountKit\IAccountKitService;
use Cleevio\RestApi\Auth\Service\Facebook\IFacebookService;
use Cleevio\Users\Entities\User;

interface IUserService extends IApiUserService, IFacebookService, IAccountKitService
{

	/**
	 * @param int $id
	 * @return User
	 */
	public function getUser(int $id): ?User;

	/**
	 * Find user by username
	 *
	 * @param string $username
	 * @return User|null
	 */
	public function findByUsername(string $username): ?User;

	/**
	 * Find user by email
	 *
	 * @param string $email
	 * @return User|null
	 */
	public function findByEmail(string $email): ?User;

	/**
	 * @param string $username
	 * @param string|null $email
	 * @param string $password
	 * @return User
	 */
	public function createUser(string $username, ?string $email, ?string $password = null): User;

	/**
	 * @param string $username
	 * @param string $email
	 * @param string $linktype
	 * @param ITokenProvider $tokenProvider
	 * @return SsoIdentity
	 */
	public function createSsoUser(string $username, string $email, string $linktype, ITokenProvider $tokenProvider): SsoIdentity;

	/**
	 * @param PasswordIdentity $identity
	 * @param string $newPassword
	 * @param string|null $oldPassword
	 * @throws InvalidPassword
	 * @throws PasswordResetTimeLimit
	 * @throws UserNotFound
	 */
	public function changePassword(PasswordIdentity $identity, string $newPassword, ?string $oldPassword = null): void;

	/**
	 * @param string $email
	 * @throws PasswordResetTimeLimit
	 * @throws UserNotFound
	 */
	public function requestPasswordReset(string $email): void;
}
