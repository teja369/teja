<?php

declare(strict_types=1);

namespace Cleevio\Users\Repositories;

use Cleevio\Repository\Repositories\SqlRepository;
use Cleevio\Users\Entities\TravelAssistant;

class TravelAssistantRepository extends SqlRepository implements ITravelAssistantRepository
{

	public function type(): string
	{
		return TravelAssistant::class;
	}

}
