<?php

declare(strict_types=1);

namespace Cleevio\Users\Repositories;

use Cleevio\Repository\Repositories\SqlRepository;
use Cleevio\RestApi\Auth\IAuthSubject;
use Cleevio\Users\Entities\User;

class UserRepository extends SqlRepository implements IUserRepository
{

	public function type(): string
	{
		return User::class;
	}


	/**
	 * @param string $email
	 * @return User|null
	 */
	public function findByEmail(string $email): ?User
	{
		$result = $this->em->getRepository(User::class)->findOneBy(['email' => $email]);

		if ($result instanceof User) {
			return $result;
		}

		return null;
	}


	/**
	 * @param string $username
	 * @return User|null
	 */
	public function findByUsername(string $username): ?User
	{
		$result = $this->em->getRepository($this->type())->findOneBy(['username' => $username]);

		if ($result instanceof User) {
			return $result;
		}

		return null;
	}

	/**
	 * @param string $auth
	 * @return User|null
	 */
	public function findByAuth(string $auth): ?IAuthSubject
	{
		return $this->findByUsername($auth);
	}
}
