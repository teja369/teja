<?php

declare(strict_types=1);

namespace Cleevio\Users\Repositories;

use Cleevio\Repository\ConstraintsBuilder;
use Cleevio\Repository\Expressions\Comparison;

class UserConstraints extends ConstraintsBuilder
{

	function setEmail(string $email): UserConstraints
	{
		$this->addCriteria(new Comparison("email", "=", $email));

		return $this;
	}

	function setRole(string $role): UserConstraints
	{
		$this->addCriteria(new Comparison("role", "=", $role));

		return $this;
	}

}
