<?php

declare(strict_types=1);

namespace Cleevio\Users\Repositories;

use Cleevio\Repository\ConstraintsBuilder;
use Cleevio\Repository\Expressions\Comparison;
use Cleevio\Users\Entities\User;

class TravelAssistantConstraints extends ConstraintsBuilder
{
	function setAssistant(User $user): TravelAssistantConstraints
	{
		$this->addCriteria(new Comparison("assistant", "=", $user->getId()));

		return $this;
	}

	function setTraveller(User $user): TravelAssistantConstraints
	{
		$this->addCriteria(new Comparison("traveller", "=", $user->getId()));

		return $this;
	}

}
