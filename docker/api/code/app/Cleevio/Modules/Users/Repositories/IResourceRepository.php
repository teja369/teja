<?php

declare(strict_types=1);

namespace Cleevio\Users\Repositories;

use Cleevio\Acl\Providers\IResourcesProvider;
use Cleevio\Repository\IRepository;

interface IResourceRepository extends IRepository, IResourcesProvider
{

}
