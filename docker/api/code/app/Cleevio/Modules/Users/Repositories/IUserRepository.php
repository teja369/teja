<?php

declare(strict_types=1);

namespace Cleevio\Users\Repositories;

use Cleevio\Repository\IRepository;
use Cleevio\RestApi\Auth\IAuthRepository;
use Cleevio\Users\Entities\User;

interface IUserRepository extends IRepository, IAuthRepository
{

	/**
	 * @param string $email
	 * @return User|null
	 */
	public function findByEmail(string $email): ?User;


	/**
	 * @param string $username
	 * @return User|null
	 */
	public function findByUsername(string $username): ?User;

}
