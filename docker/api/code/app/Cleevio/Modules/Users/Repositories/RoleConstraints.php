<?php

declare(strict_types=1);

namespace Cleevio\Users\Repositories;

use Cleevio\Repository\ConstraintsBuilder;
use Cleevio\Repository\Expressions\Comparison;

class RoleConstraints extends ConstraintsBuilder
{
	function setRole(string $role): RoleConstraints
	{
		$this->addCriteria(new Comparison("name", "=", $role));

		return $this;
	}

}
