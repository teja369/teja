<?php

declare(strict_types=1);

namespace Cleevio\Users\Repositories;

use Cleevio\Acl\Models\IResource;
use Cleevio\Repository\Constraints;
use Cleevio\Repository\Repositories\SqlRepository;
use Cleevio\Users\Entities\Resource;
use Doctrine\ORM\Query\QueryException;

class ResourceRepository extends SqlRepository implements IResourceRepository
{

	public function type(): string
	{
		return Resource::class;
	}

	/**
	 * @return IResource[]
	 * @throws QueryException
	 */
	function getResources(): array
	{
		return $this->findAll(Constraints::empty());
	}
}
