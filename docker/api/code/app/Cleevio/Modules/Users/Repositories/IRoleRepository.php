<?php

declare(strict_types=1);

namespace Cleevio\Users\Repositories;

use Cleevio\Acl\Providers\IRolesProvider;
use Cleevio\Repository\IRepository;

interface IRoleRepository extends IRepository, IRolesProvider
{

	/**
	 * @param array $names
	 * @return array
	 */
	public function findByNames(array $names): array;
}
