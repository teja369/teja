<?php

declare(strict_types=1);

namespace Cleevio\Users\Repositories;

use Cleevio\Repository\IRepository;

interface ITravelAssistantRepository extends IRepository
{

}
