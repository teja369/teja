<?php

declare(strict_types=1);

namespace Cleevio\Users\Repositories;

use Cleevio\Acl\Models\IRole;
use Cleevio\Repository\Constraints;
use Cleevio\Repository\Repositories\SqlRepository;
use Cleevio\Users\Entities\Role;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\Query\QueryException;

class RoleRepository extends SqlRepository implements IRoleRepository
{

	public function type(): string
	{
		return Role::class;
	}

	/**
	 * @return IRole[]
	 * @throws QueryException
	 */
	function getRoles(): array
	{
		return $this->findAll(Constraints::empty());
	}

	function findByNames(array $names): array
	{
		$result = $this->getRepository()
			->createQueryBuilder("t")
			->addCriteria(Criteria::create()->where(Criteria::expr()->in("name", $names)))
			->getQuery()
			->execute();

		return !is_null($result) ? $result : [];
	}
}
