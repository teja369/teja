<?php

declare(strict_types=1);

namespace Cleevio\Users\Commands;

use Cleevio\Users\UsernameAlreadyExists;
use Cleevio\Users\Repositories\IUserRepository;
use Cleevio\Users\IUserService;
use Cleevio\Users\Entities\User;
use Nette\Utils\Random;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\Table;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class CreateUserCommand
 * @package Cleevio\Users\Commands
 * phpcs:ignoreFile
 */
class CreateUserCommand extends Command
{

    /**
     * @var IUserService
     */
    private $userService;

    /**
     * @var IUserRepository
     */
    private $userRepository;

    /**
     * @var string
     */
    public static $defaultName = 'cleevio:users:create';


    public function __construct(IUserService $userService, IUserRepository $userRepository)
    {
        parent::__construct();
        $this->userService = $userService;
        $this->userService = $userService;
        $this->userRepository = $userRepository;
    }


    protected function configure(): void
    {
        $this->setName(self::$defaultName)
            ->setDescription('Creates administration user.');

        $this->addArgument('username', InputArgument::REQUIRED, 'Username of created user.')
            ->addArgument('password', InputArgument::OPTIONAL, 'Password (optional, will be auto-generated).');
    }


    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $username = $input->getArgument('username');
        $password = $input->getArgument('password');

        if (is_null($password)) {
            $password = Random::generate(10);
        }

        try {
            $user = $this->userService->createUser($username, null, $password);

            $this->userRepository->persist($user);

        } catch (UsernameAlreadyExists $e) {
            $output->writeln('<error>[ERROR]</error> User with this username already exists.');

            return 1;
        }

        $output->writeln('<info>[OK]</info> User has been successfully created.');

        $table = new Table($output);
        $table->setHeaders(['Username', 'Password']);
        $table->setRows([[$username, $password]]);
        $table->render();

        return 0;
    }
}
