<?php

declare(strict_types=1);

namespace Cleevio\Users;

use Cleevio\Acl\Models\IAclSubject;
use Cleevio\Acl\Models\IRole;
use Cleevio\RestApi\Auth\ApiAuthService;
use Cleevio\RestApi\Auth\AuthResponse;
use Cleevio\RestApi\Auth\ITokenProvider;
use Cleevio\RestApi\Exceptions\BadRequestException;
use Cleevio\RestApi\Exceptions\InvalidHeaderException;
use Cleevio\RestApi\Presenters\RestPresenter;
use Cleevio\Users\Api\Response\SsoResponseBuilder;
use DateTime;
use Exception;
use InvalidArgumentException;
use LightSaml\Credential\KeyHelper;
use LightSaml\Credential\X509Certificate;
use LightSaml\Error\LightSamlSecurityException;
use LightSaml\Model\Context\DeserializationContext;
use LightSaml\Model\Protocol\Response;
use LightSaml\Model\XmlDSig\SignatureXmlReader;
use Nette\NotImplementedException;
use Nette\Security\Identity as SecurityIdentity;
use Nette\Security\User;
use RobRichards\XMLSecLibs\XMLSecurityKey;

class SamlAuthService extends ApiAuthService
{
	protected const HEADER = "Authorization";

	private const AUTHORIZATION_PREFIX = 'Bearer';

	private const REPLACEMENTS = [
		'PortalLanguage' => 'lang',
		'PortalOrgID' => 'externalId',
		'PortalID' => 'username',
		'HomeUrl' => 'homeUrl',
		'email' => 'email',
		'linktype' => 'linktype',
		'DataCenter' => 'dataCenter',
		'BackUrl' => 'backUrl',
		'SAMLDest_URL' => 'destUrl',
		'OrgID' => 'orgId'];

	/**
	 * @var IUserService
	 */
	protected $userService;

	/**
	 * @var User
	 */
	private $user;

	/**
	 * @param ITokenProvider $tokenProvider
	 * @param IUserService $userService
	 * @param User $user
	 */
	public function __construct(ITokenProvider $tokenProvider,
								IUserService $userService,
								User $user)
	{
		parent::__construct($tokenProvider, $userService);

		$this->userService = $userService;
		$this->user = $user;
	}

	/**
	 * Login user via credentials
	 * @param RestPresenter $resource
	 * @return AuthResponse
	 * @throws Exception
	 */
	public function login(RestPresenter $resource): AuthResponse
	{
		$header = $resource->getHttpRequest()->getHeader(self::HEADER);

		if (is_null($header) || substr($header, 0, strlen(self::AUTHORIZATION_PREFIX) + 1) !== self::AUTHORIZATION_PREFIX . " ") {
			throw new InvalidHeaderException(self::HEADER);
		}

		$response = substr($header, strlen(self::AUTHORIZATION_PREFIX) + 1);

		// Decode response
		$response = str_replace("^", "", $response);
		$response = rawurldecode($response);
		$response = base64_decode($response, true);

		if ($response === false) {
			throw new InvalidBodyException;
		}

		$deserializationContext = new DeserializationContext;
		$deserializationContext->getDocument()->loadXML($response);

		$response = new Response;
		$response->deserialize($deserializationContext->getDocument()->firstChild, $deserializationContext);

		$key = $this->getCertKey();

		try {
			/** @var SignatureXmlReader $signatureReader */
			$signatureReader = $response->getSignature();

			$ok = $signatureReader->validate($key);

			if ($ok) {

				$attributes = [];

				$assertion = $response->getFirstAssertion();

				if ($assertion !== null && $assertion->getFirstAttributeStatement() !== null) {
					foreach ($assertion->getFirstAttributeStatement()->getAllAttributes() as $attribute) {
						if (array_key_exists($attribute->getName(), self::REPLACEMENTS)) {
							$attributes[self::REPLACEMENTS[$attribute->getName()]] = $attribute->getFirstAttributeValue();
						}
					}
				}

				// Check all necessary keys
				if (!array_key_exists('username', $attributes) ||
					$attributes['username'] === null ||
					!array_key_exists('email', $attributes) ||
					$attributes['email'] === null ||
					!array_key_exists('linktype', $attributes) ||
					$attributes['linktype'] === null) {
					throw new InvalidBodyException;
				}

				$identity = $this->userService->createSsoUser($attributes['username'], $attributes['email'], $attributes['linktype'], $this->tokenProvider);
				$user = $identity->getUser();

				$roles = [];

				if ($user instanceof IAclSubject) {
					$roles = array_map(static function (IRole $role) {
						return $role->getName();
					}, $user->getRoles());
				}

				$this->user->login(new SecurityIdentity($user->getId(), $roles, [
					'user' => $user,
				]));

				$attributes['endpoint'] = $this->user->isAllowed('admin')
					? 'admin'
					: 'frontend';

				return new SsoResponseBuilder($identity->getToken(), $identity->getExpires() - (new DateTime)->getTimestamp(), $attributes);
			}

			throw new InvalidSignatureException;
		} catch (LightSamlSecurityException $ex) {
			throw new InvalidSignatureException;
		}
	}

	/**
	 * Read cert key from file
	 *
	 * @return XMLSecurityKey
	 * @throws BadRequestException
	 */
	private function getCertKey(): XMLSecurityKey
	{
		try {
			if (defined('APP_DIR')) {
				return KeyHelper::createPublicKey(
					X509Certificate::fromFile(APP_DIR . '/cert/saml.pem')
				);
			}

			throw new InvalidArgumentException;
		} catch (InvalidArgumentException $ex) {
			throw new BadRequestException('Error reading certificate file');
		}
	}

	/**
	 * Refresh user token
	 * @param RestPresenter $resource
	 * @return AuthResponse
	 */
	public
	function refresh(RestPresenter $resource): AuthResponse
	{
		unset($resource);

		throw new NotImplementedException;
	}

}
