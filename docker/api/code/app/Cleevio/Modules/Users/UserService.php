<?php

declare(strict_types=1);

namespace Cleevio\Users;

use Cleevio\GA\IGAService;
use Cleevio\GA\UserInformationException;
use Cleevio\Identity\Identities\AccountKitIdentity;
use Cleevio\Identity\Identities\ApiIdentity;
use Cleevio\Identity\Identities\FacebookIdentity;
use Cleevio\Identity\Identities\PasswordIdentity;
use Cleevio\Identity\Identities\SsoIdentity;
use Cleevio\Identity\Identity;
use Cleevio\Identity\IIdentityRepository;
use Cleevio\RestApi\Auth\IAuthSubject;
use Cleevio\RestApi\Auth\ITokenProvider;
use Cleevio\RestApi\Exceptions\AuthenticationException;
use Cleevio\RestApi\Utils\Facebook\Exception\FacebookException;
use Cleevio\Users\Entities\User;
use Cleevio\Users\Repositories\IRoleRepository;
use Cleevio\Users\Repositories\IUserRepository;
use DateTime;
use Facebook\GraphNodes\GraphUser;
use Nette\Security\Passwords;
use Nette\SmartObject;
use Nette\Utils\Random;

class UserService implements IUserService
{

	use SmartObject;

	private const SSO_ADMIN_ROLE_ID = 4912;

	private const ADMIN_ROLE_NAME = 'admin';
	private const ADMIN_EXTERNAL_ROLE_NAME = 'admin-external';
	private const TRAVELLER_ROLE_NAME = 'traveller';

	/**
	 * @var IUserRepository
	 */
	private $userRepository;

	/**
	 * @var IIdentityRepository
	 */
	private $identityRepository;

	/**
	 * @var IRoleRepository
	 */
	private $roleRepository;

	/**
	 * @var IGAService
	 */
	private $GAService;


	public function __construct(
		IUserRepository $userRepository,
		IIdentityRepository $identityRepository,
		IRoleRepository $roleRepository,
		IGAService $GAService
	)
	{
		$this->userRepository = $userRepository;
		$this->identityRepository = $identityRepository;
		$this->roleRepository = $roleRepository;
		$this->GAService = $GAService;
	}

	/**
	 * @param int $id
	 * @return User
	 */
	public function getUser(int $id): ?User
	{
		return $this->userRepository->find($id);
	}

	/**
	 * Find user by username
	 *
	 * @param string $username
	 * @return User|null
	 */
	public function findByUsername(string $username): ?User
	{
		return $this->userRepository->findByUsername($username);
	}

	/**
	 * Find user by email
	 *
	 * @param string $email
	 * @return User|null
	 */
	public function findByEmail(string $email): ?User
	{
		return $this->userRepository->findByEmail($email);
	}

	/**
	 * @param string $username
	 * @param string|null $email
	 * @param string|null $password
	 * @return User
	 * @throws UsernameAlreadyExists
	 */
	public function createUser(string $username, ?string $email, ?string $password = null): User
	{
		$user = $this->userRepository->findByUsername($username);

		if (!$user instanceof User) {
			$user = new User($username);
			$user->setEmail($email);
			$this->userRepository->persist($user);
		}

		if (!is_null($password)) {
			if (!is_null($user->getIdentity(Identity::IDENTITY_PASSWORD))) {
				throw new UsernameAlreadyExists;
			}

			$identity = new PasswordIdentity($user, $password);
			$this->identityRepository->persist($identity);
		}

		return $user;
	}


	/**
	 * @param string $username
	 * @param string $email
	 * @param string $linktype
	 * @param ITokenProvider $tokenProvider
	 * @return SsoIdentity
	 * @throws UsernameAlreadyExists
	 */
	public function createSsoUser(string $username, string $email, string $linktype, ITokenProvider $tokenProvider): SsoIdentity
	{
		try {
			$this->userRepository->beginTransaction();
			$user = $this->findByEmail($email);

			if ($user === null) {
				$user = $this->createUser($username, $email);
			}

			$userInformation = $this->GAService->getUserInformation($username);

			$identity = new SsoIdentity($user, $tokenProvider->getToken($user));

			$user->addIdentity($identity);
			$user->setFirstName($userInformation->getFirstName());
			$user->setLastName($userInformation->getLastName());

			$roles = [];

			if ($linktype === 'internal') {
				$roles[self::ADMIN_ROLE_NAME] = true;
			} else {
				foreach ($userInformation->getRoles()->getRoles() as $role) {
					$roles[$role->getId() === self::SSO_ADMIN_ROLE_ID
						? self::ADMIN_EXTERNAL_ROLE_NAME
						: self::TRAVELLER_ROLE_NAME] = true;
				}
			}

			$roles = $this->roleRepository->findByNames(array_keys($roles));

			$user->setRole($roles);

			$this->userRepository->persist($user);
			$this->userRepository->commitTransaction();

			return $identity;
		} catch (UserInformationException $e) {
			$this->userRepository->rollbackTransaction();

			throw new AuthenticationException($e->getMessage());
		}
	}

	/**
	 * Find subject by refresh token
	 * @param string $refreshToken
	 * @return IAuthSubject|null
	 */
	function findByRefreshToken(string $refreshToken): ?IAuthSubject
	{
		$identity = $this->identityRepository->findByRefreshToken($refreshToken);

		if (is_null($identity)) {
			return null;
		}

		return $identity->getUser();
	}


	/**
	 * Generate and return a new refresh token for a user
	 * @param IAuthSubject $user
	 * @return string
	 */
	function getRefreshToken(IAuthSubject $user): string
	{
		$refresh = new ApiIdentity($user);
		$this->identityRepository->persist($refresh);

		return $refresh->getRefreshToken();
	}


	/**
	 * Create a new user from Facebook graph response
	 * @param GraphUser $graphUser
	 * @return IAuthSubject
	 * @throws \Exception
	 */
	function createUserFromFacebook(GraphUser $graphUser): IAuthSubject
	{
		if (is_null($graphUser->getId()) || is_null($graphUser->getEmail())) {
			throw new FacebookException;
		}

		$user = $this->userRepository->findByEmail($graphUser->getEmail());

		if (is_null($user)) {
			$user = new User($graphUser->getEmail());
			$this->userRepository->persist($user);
		}

		$identity = new FacebookIdentity($user, $graphUser->getId());
		$this->identityRepository->persist($identity);

		return $user;
	}


	/**
	 * Find subject by Facebook ID
	 * @param string $facebookId
	 * @return IAuthSubject|null
	 */
	function findByFacebookId(string $facebookId): ?IAuthSubject
	{
		$identity = $this->identityRepository->findByFacebookId($facebookId);

		if (is_null($identity)) {
			return null;
		}

		return $identity->getUser();
	}


	/**
	 * Create new account kit subject
	 * Create a new user from Facebook account kit response
	 * @param string $accountKitId
	 * @return IAuthSubject
	 * @throws \Exception
	 */
	function createUserFromAccountKit(string $accountKitId): IAuthSubject
	{

		$user = new User($accountKitId . "@account-kit");
		$this->userRepository->persist($user);

		$identity = new AccountKitIdentity($user, $accountKitId);
		$this->identityRepository->persist($identity);

		return $user;
	}


	/**
	 * Find auth subject by account kit ID
	 * @param string $accountKitId
	 * @return IAuthSubject|null
	 */
	function findByAccountKitId(string $accountKitId): ?IAuthSubject
	{
		$identity = $this->identityRepository->findByAccountKitId($accountKitId);

		if (is_null($identity)) {
			return null;
		}

		return $identity->getUser();
	}


	/**
	 * @param PasswordIdentity $identity
	 * @param string $newPassword
	 * @param string|null $oldPassword
	 * @throws InvalidPassword
	 * @throws PasswordResetTimeLimit
	 * @throws UserNotFound
	 */
	public function changePassword(PasswordIdentity $identity, string $newPassword, ?string $oldPassword = null): void
	{
		$user = $identity->getUser();

		if (!$user instanceof User) {
			throw new UserNotFound;
		}

		if (!is_null($identity->getLastPasswordResetRequest()) &&
			$identity->getLastPasswordResetRequest() <= (new DateTime)->modify('-1 hour')) {
			throw new PasswordResetTimeLimit;
		}

		if (!is_null($oldPassword) && password_verify($oldPassword, $identity->getPassword()) === false) {
			throw new InvalidPassword;
		}

		$passwords = new Passwords;

		$identity->setPassword($passwords->hash($newPassword));
		$identity->setLastPasswordChange(new DateTime);
		$this->identityRepository->persist($identity);
	}


	/**
	 * @param string $email
	 * @throws PasswordResetTimeLimit
	 * @throws UserNotFound
	 */
	public function requestPasswordReset(string $email): void
	{
		$user = $this->userRepository->findByEmail($email);

		if (is_null($user)) {
			throw new UserNotFound;
		}

		$identity = $user->getIdentity(Identity::IDENTITY_PASSWORD);

		if (!$identity instanceof PasswordIdentity) {
			throw new UserNotFound;
		}

		if ($identity->getLastPasswordResetRequest() <= (new DateTime)->modify('-30 minutes')) {
			$identity->setLastPasswordResetRequest(new DateTime);
			$identity->setPasswordResetHash(Random::generate(12, 'a-z'));

			$this->identityRepository->persist($identity);

		} else {
			throw new PasswordResetTimeLimit;
		}
	}
}
