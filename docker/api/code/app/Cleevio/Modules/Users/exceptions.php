<?php

declare(strict_types=1);

namespace Cleevio\Users;

use Cleevio\Exceptions\ParamException;
use Cleevio\RestApi\Exceptions\ApiException;
use Cleevio\RestApi\Exceptions\AuthenticationException;
use Nette\Http\IResponse;

class UsernameAlreadyExists extends ParamException
{

}

class UserNotFound extends ParamException
{

}

class VerificationCodeNotValid extends ParamException
{

}

class InvalidPassword extends ParamException
{

}

class PasswordResetTimeLimit extends ParamException
{

}

class InvalidAccessTokenException extends AuthenticationException
{
	public function __construct()
	{
		parent::__construct("Invalid access token", AuthenticationException::BAD_AUTHANTICATION);
	}
}

class ExpiredAccessTokenException extends AuthenticationException
{
	public function __construct()
	{
		parent::__construct("Expired access token", AuthenticationException::BAD_AUTHANTICATION);
	}
}

class InvalidBodyException extends ApiException
{

	public function __construct()
	{
		parent::__construct('Response in body is invalid', 0, IResponse::S400_BAD_REQUEST);
	}
}

class InvalidSignatureException extends ApiException
{

	public function __construct()
	{
		parent::__construct('Response signature could not be validated', 0, IResponse::S401_UNAUTHORIZED);
	}
}
