<?php

declare(strict_types=1);

namespace Cleevio\Hosts;

use Cleevio\Exceptions\ParamException;

class HostNotFound extends ParamException
{

}

class InvalidFieldException extends ParamException
{

}

class MissingHostException extends ParamException
{

}
