<?php

declare(strict_types=1);

namespace Cleevio\Hosts\Repositories;

use Cleevio\Fields\Entities\Field;
use Cleevio\Hosts\Entities\Host;
use Cleevio\Repository\ConstraintsBuilder;
use Cleevio\Repository\Expressions\Comparison;

class HostParamConstraints extends ConstraintsBuilder
{

	/**
	 * @param Host $host
	 * @return HostParamConstraints
	 */
	function setHost(Host $host): HostParamConstraints
	{
		$this->addCriteria(new Comparison('host.id', '=', $host->getId() . ""));

		return $this;
	}

	/**
	 * @param Field $field
	 * @return HostParamConstraints
	 */
	function setField(Field $field): HostParamConstraints
	{
		$this->addCriteria(new Comparison('field.id', '=', $field->getId() . ""));

		return $this;
	}

}
