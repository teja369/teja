<?php

declare(strict_types=1);

namespace Cleevio\Hosts\Repositories;

use Cleevio\Hosts\Entities\Host;
use Cleevio\Repository\IRepository;

interface IHostRepository extends IRepository
{

	/**
	 * @param int[] $ids
	 * @return Host[]
	 */
	public function findByIds(array $ids): array;

}
