<?php

declare(strict_types=1);

namespace Cleevio\Hosts\Repositories;

use Cleevio\Repository\IRepository;

interface IHostParamRepository extends IRepository
{

}
