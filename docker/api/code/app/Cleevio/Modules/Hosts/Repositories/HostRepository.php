<?php

declare(strict_types=1);

namespace Cleevio\Hosts\Repositories;

use Cleevio\Hosts\Entities\Host;
use Cleevio\Repository\Repositories\SqlRepository;
use Doctrine\Common\Collections\Criteria;

class HostRepository extends SqlRepository implements IHostRepository
{

	/**
	 * Model class type
	 * @return string
	 */
	public function type(): string
	{
		return Host::class;
	}

	/**
	 * @param int[] $ids
	 * @return Host[]
	 */
	public function findByIds(array $ids): array
	{
		$result = $this->getRepository()
			->createQueryBuilder("t")
			->addCriteria(Criteria::create()->where(Criteria::expr()->in("id", $ids)))
			->getQuery()
			->execute();

		return !is_null($result) ? $result : [];
	}
}
