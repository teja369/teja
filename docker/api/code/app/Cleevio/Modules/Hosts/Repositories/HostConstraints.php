<?php

declare(strict_types=1);

namespace Cleevio\Hosts\Repositories;

use Cleevio\Countries\Entities\Country;
use Cleevio\Repository\ConstraintsBuilder;
use Cleevio\Repository\Expressions\Comparison;
use Cleevio\Repository\Expressions\IsNull;

class HostConstraints extends ConstraintsBuilder
{

	/**
	 * @param Country $country
	 * @return HostConstraints
	 */
	function setCountry(Country $country): HostConstraints
	{
		$this->addCriteria(new Comparison('country.id', '=', $country->getId() . ""));

		return $this;
	}

	/**
	 * @param string $type
	 * @return HostConstraints
	 */
	function setType(string $type): HostConstraints
	{
		$this->addCriteria(new Comparison('type', '=', $type));

		return $this;
	}

	/**
	 * @param string $search
	 * @return HostConstraints
	 */
	function setSearch(string $search): HostConstraints
	{
		$this->addCriteria(new Comparison('name', 'CONTAINS', $search));

		return $this;
	}

	/**
	 * @param bool|null $isDeleted
	 * @return HostConstraints
	 */
	function setIsDeleted(?bool $isDeleted): HostConstraints
	{
		if ($isDeleted !== null) {
			if ($isDeleted) {
				$this->addCriteria(new Comparison('deletedAt', '<>', ''));
			} else {
				$this->addCriteria(new IsNull('deletedAt'));
			}
		}

		return $this;
	}

}
