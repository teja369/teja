<?php

declare(strict_types=1);

namespace Cleevio\Hosts\Repositories;

use Cleevio\Hosts\Entities\HostParam;
use Cleevio\Repository\Repositories\SqlRepository;

class HostParamRepository extends SqlRepository implements IHostParamRepository
{

	/**
	 * Model class type
	 * @return string
	 */
	public function type(): string
	{
		return HostParam::class;
	}

}
