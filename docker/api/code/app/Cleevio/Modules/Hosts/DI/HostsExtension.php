<?php

declare(strict_types=1);

namespace Cleevio\Hosts\DI;

use Cleevio\Modules\Providers\IPresenterMappingProvider;
use Nette\DI\CompilerExtension;
use Nettrine\ORM\DI\Traits\TEntityMapping;

class HostsExtension extends CompilerExtension implements IPresenterMappingProvider
{

	use TEntityMapping;

	public function loadConfiguration()
	{
		$this->compiler->loadConfig(__DIR__ . '/services.neon');
		$this->setEntityMappings(
			[
				'Cleevio\Hosts\Entities\Host' => __DIR__ . '/..',
			]
		);
	}


	public function getPresenterMapping(): array
	{
		return ['Hosts' => 'Cleevio\\Hosts\\*Module\\Presenters\\*Presenter'];
	}

}
