<?php

declare(strict_types=1);

namespace Cleevio\Hosts;

use Cleevio\Countries\Entities\Country;
use Cleevio\Fields\Entities\Field;
use Cleevio\Fields\FieldTypeInvalid;
use Cleevio\Hosts\Entities\Host;
use Cleevio\Hosts\Entities\HostParam;
use Cleevio\Hosts\Repositories\HostConstraints;
use Nette\Http\IRequest;

interface IHostsService
{

	/**
	 * @param string  $type
	 * @param Country $country
	 * @param string  $name
	 * @return Host
	 */
	public function createHost(
		string $type,
		Country $country,
		string $name
	): Host;


	/**
	 * Get host by ID
	 * @param int $id
	 * @return Host|null
	 */
	public function getHost(int $id): ?Host;


	/**
	 * Returns hosts for a provided country.
	 * @param Country|null $country
	 * @param string|null  $type
	 * @param string|null  $search
	 * @return Host[]
	 */
	public function getHosts(?Country $country, ?string $type, ?string $search): array;


	/**
	 * @param array $ids
	 * @return Host[]
	 * @throws HostNotFound
	 */
	public function findByIds(array $ids): array;


	/**
	 * Validate field value
	 * @param Host  $host
	 * @param Field $field
	 * @param mixed $value
	 * @return HostParam
	 * @throws InvalidFieldException
	 * @throws FieldTypeInvalid
	 */
	public function validate(Host $host, Field $field, $value): HostParam;


	/**
	 * @param Host $id
	 * @return void
	 */
	public function deleteHost(Host $id): void;


	/**
	 * @param HostConstraints $constraints
	 * @param IRequest        $request
	 * @param Country         $country
	 * @return HostConstraints
	 */
	public function queryToConstraints(
		HostConstraints $constraints,
		IRequest $request,
		?Country $country
	): HostConstraints;
}
