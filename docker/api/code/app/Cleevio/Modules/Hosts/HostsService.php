<?php

declare(strict_types=1);

namespace Cleevio\Hosts;

use Cleevio\Countries\Entities\Country;
use Cleevio\Fields\Entities\Field;
use Cleevio\Fields\FieldTypeInvalid;
use Cleevio\Fields\Validators\NumberFieldValidator;
use Cleevio\Fields\Validators\TextFieldValidator;
use Cleevio\Hosts\Entities\Host;
use Cleevio\Hosts\Entities\HostParam;
use Cleevio\Hosts\Repositories\HostConstraints;
use Cleevio\Hosts\Repositories\IHostRepository;
use DateTime;
use Exception;
use Nette\Http\IRequest;
use Nette\SmartObject;

class HostsService implements IHostsService
{

	use SmartObject;

	/**
	 * @var IHostRepository
	 */
	private $hostRepository;


	/**
	 * @param IHostRepository $hostRepository
	 */
	public function __construct(IHostRepository $hostRepository)
	{
		$this->hostRepository = $hostRepository;
	}


	/**
	 * Get host by ID
	 * @param int $id
	 * @return Host|null
	 */
	public function getHost(int $id): ?Host
	{
		return $this->hostRepository->find($id);
	}


	/**
	 * @param string $type
	 * @param Country $country
	 * @param string $name
	 * @return Host
	 */
	public function createHost(string $type, Country $country, string $name): Host
	{
		$host = new Host($name, $country, $type);
		$this->hostRepository->persist($host);

		return $host;
	}


	/**
	 * Returns hosts for a provided country.
	 * @param Country|null $country
	 * @param string|null $type
	 * @param string|null $search
	 * @return Host[]
	 */
	public function getHosts(?Country $country, ?string $type, ?string $search): array
	{
		$constraints = (new HostConstraints)
			->setIsDeleted(false);

		if ($country !== null) {
			$constraints->setCountry($country);
		}

		if ($type !== null) {
			$constraints->setType($type);
		}

		if ($search !== null) {
			$constraints->setSearch($search);
		}

		return $this->hostRepository->findAll($constraints->build());
	}


	/**
	 * @param array $ids
	 * @return Host[]
	 * @throws HostNotFound
	 */
	public function findByIds(array $ids): array
	{
		$ids = array_unique($ids);
		$hosts = $this->hostRepository->findByIds($ids);

		if (count($hosts) !== count($ids)) {
			throw new HostNotFound;
		}

		return $hosts;
	}


	/**
	 * Validate field value
	 * @param Host $host
	 * @param Field $field
	 * @param mixed $value
	 * @return HostParam
	 * @throws FieldTypeInvalid
	 * @throws InvalidFieldException
	 */
	public function validate(Host $host, Field $field, $value): HostParam
	{
		switch ($field->getType()) {

			case Field::FIELD_TYPE_TEXT:
				return (new TextFieldValidator)->validate($host, $field, $value);
			case Field::FIELD_TYPE_NUMBER:
				return (new NumberFieldValidator)->validate($host, $field, $value);
			default:
				throw new FieldTypeInvalid('fields.field.get.invalid-type');
		}
	}


	/**
	 * @param Host $host
	 * @return void
	 * @throws Exception
	 */
	public function deleteHost(Host $host): void
	{
		$host->setDeletedAt(new DateTime);
		$this->hostRepository->persist($host);
	}


	/**
	 * @param HostConstraints $constraints
	 * @param IRequest $request
	 * @param Country $country
	 * @return HostConstraints
	 */
	public function queryToConstraints(
		HostConstraints $constraints,
		IRequest $request,
		?Country $country
	): HostConstraints
	{
		if ($request->getQuery('type') !== null) {
			$constraints->setType($request->getQuery('type'));
		}

		if ($request->getQuery('search') !== null) {
			$constraints->setSearch($request->getQuery('search'));
		}

		if ($country !== null) {
			$constraints->setCountry($country);
		}

		return $constraints;
	}
}
