<?php

declare(strict_types=1);

namespace Cleevio\Hosts\Api\Response;

use Cleevio\Api\Translatable\SimpleResponse;
use Cleevio\Fields\Api\Response\FieldResponseBuilder;
use Cleevio\Hosts\Entities\HostParam;

final class HostParamResponseBuilder extends SimpleResponse
{

	/**
	 * @var HostParam
	 */
	private $data;

	/**
	 * CountryResponseBuilder constructor.
	 * @param HostParam $host
	 * @param string|null $lang
	 */
	public function __construct(HostParam $host, ?string $lang)
	{
		parent::__construct($lang);

		$this->data = $host;
	}

	/**
	 * @return array
	 */
	protected function data(): array
	{
		return [
			'id' => $this->data->getId(),
			'field' => (new FieldResponseBuilder($this->data->getField(), $this->getLang()))->build(),
			'value' => $this->data->getValue(),
		];
	}
}
