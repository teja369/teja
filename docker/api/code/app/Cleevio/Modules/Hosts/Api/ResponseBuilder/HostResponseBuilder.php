<?php

declare(strict_types=1);

namespace Cleevio\Hosts\Api\Response;

use Cleevio\Api\Translatable\SimpleResponse;
use Cleevio\Countries\Api\Response\CountryResponseBuilder;
use Cleevio\Hosts\Entities\Host;

final class HostResponseBuilder extends SimpleResponse
{

	/**
	 * @var Host
	 */
	private $data;

	/**
	 * CountryResponseBuilder constructor.
	 * @param Host $host
	 * @param string|null $lang
	 */
	public function __construct(Host $host, ?string $lang)
	{
		parent::__construct($lang);

		$this->data = $host;
	}

	/**
	 * @return array
	 */
	protected function data(): array
	{
		return [
			'id' => $this->data->getId(),
			'type' => $this->data->getType(),
			'name' => $this->data->getName(),
			'address' => $this->data->getAddress(),
			'country' => (new CountryResponseBuilder($this->data->getCountry(), $this->getLang()))->build(),
			'params' => (new HostsParamsResponseBuilder($this->data->getParams(), $this->getLang()))->build(),
		];
	}
}
