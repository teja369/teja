<?php

declare(strict_types=1);

namespace Cleevio\Hosts\Api\Response;

use Cleevio\Api\Translatable\PagedResponse;
use Cleevio\Hosts\Entities\Host;
use Cleevio\RestApi\Request\PagedRequest;

final class HostsPagedResponseBuilder extends PagedResponse
{
	/**
	 * @var array
	 */
	private $data;


	/**
	 * HostsPagedResponseBuilder constructor.
	 * @param PagedRequest $request
	 * @param array        $data
	 * @param string|null  $lang
	 */
	public function __construct(PagedRequest $request, array $data, ?string $lang)
	{
		parent::__construct($request, $lang);

		$this->data = $data;
	}

	/**
	 * @return array
	 */
	protected function data(): array
	{
		return array_map(function (Host $host) {
			return (new HostResponseBuilder($host, $this->getLang()))->build();
		}, $this->data);
	}

}
