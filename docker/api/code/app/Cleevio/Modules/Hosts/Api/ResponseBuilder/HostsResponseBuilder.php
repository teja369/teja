<?php

declare(strict_types=1);

namespace Cleevio\Hosts\Api\Response;

use Cleevio\Api\Translatable\SimpleResponse;
use Cleevio\Hosts\Entities\Host;

final class HostsResponseBuilder extends SimpleResponse
{
	/**
	 * @var array
	 */
	private $data;

	/**
	 * HostsResponseBuilder constructor.
	 * @param array $data
	 * @param string|null $lang
	 */
	public function __construct(array $data, ?string $lang)
	{
		parent::__construct($lang);

		$this->data = $data;
	}

	/**
	 * @return array
	 */
	protected function data(): array
	{
		return array_map(function (Host $purpose) {
			return (new HostResponseBuilder($purpose, $this->getLang()))->build();
		}, $this->data);
	}

}
