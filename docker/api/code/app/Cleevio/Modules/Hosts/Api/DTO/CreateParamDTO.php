<?php

declare(strict_types=1);

namespace Cleevio\Hosts\Api\DTO;

use Cleevio\RestApi\Command\ICommandDTO;

class CreateParamDTO implements ICommandDTO
{

	/**
	 * @var string
	 */
	private $value;


	public function __construct(
		string $value
	)
	{
		$this->value = $value;
	}

	public static function fromRequest($data): ICommandDTO
	{
		return new static(
			$data->value
		);
	}

	public function getValue(): string
	{
		return $this->value;
	}
}
