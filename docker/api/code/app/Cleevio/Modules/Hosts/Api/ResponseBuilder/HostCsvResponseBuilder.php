<?php

declare(strict_types=1);

namespace Cleevio\Hosts\Api\Response;

use Cleevio\Api\Translatable\SimpleResponse;
use Cleevio\Fields\Entities\Field;
use Cleevio\Hosts\Entities\Host;
use Cleevio\RestApi\Response\Types\ContentTypeFactory;

final class HostCsvResponseBuilder extends SimpleResponse
{

	/**
	 * @var Host
	 */
	private $data;

	/**
	 * @var Field[]
	 */
	private $fields;

	/**
	 * CountryResponseBuilder constructor.
	 * @param Host $host
	 * @param array $fields
	 * @param string|null $lang
	 */
	public function __construct(Host $host, array $fields, ?string $lang)
	{
		parent::__construct($lang);

		$this->data = $host;
		$this->fields = $fields;
	}

	/**
	 * @return string
	 */
	protected function getContentType(): string
	{
		return ContentTypeFactory::CONTENT_TYPE_CSV;
	}


	/**
	 * @return array
	 */
	public function data(): array
	{
		$output = [
			'id' => $this->data->getId(),
			'type' => $this->data->getType(),
			'name' => $this->data->getName(),
			'address' => $this->data->getAddress(),
			'country' => $this->data->getCountry()->getName(null),
		];

		foreach ($this->fields as $field) {
			$hostParam = $this->data->getParam($field);
			$output[] = $hostParam === null
				? null
				: $hostParam->getValue();
		}

		return $output;
	}
}
