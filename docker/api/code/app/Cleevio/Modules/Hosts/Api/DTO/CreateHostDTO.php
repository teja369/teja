<?php

declare(strict_types=1);

namespace Cleevio\Hosts\Api\DTO;

use Cleevio\RestApi\Command\ICommandDTO;
use InvalidArgumentException;

class CreateHostDTO implements ICommandDTO
{

	/**
	 * @var string|null
	 */
	private $type;

	/**
	 * @var string
	 */
	private $name;

	/**
	 * @var string
	 */
	protected $country;

	/**
	 * @var array|null
	 */
	protected $params;

	public function __construct(
		?string $type,
		string $name,
		string $country,
		?array $params
	)
	{
		$this->type = $type;
		$this->name = $name;
		$this->country = $country;
		$this->params = $params;
	}

	public static function fromRequest($data): ICommandDTO
	{
		return new static(
			property_exists($data, 'type') ? $data->type : null,
			$data->name,
			$data->country,
			property_exists($data, 'params') ? $data->params : null
		);
	}

	/**
	 * @return string|null
	 */
	public function getType(): ?string
	{
		return $this->type;
	}

	public function getName(): string
	{
		return $this->name;
	}

	public function getCountry(): string
	{
		return $this->country;
	}

	public function getParams(): ?array
	{
		return $this->params;
	}

	/**
	 * @param int $id
	 * @return string
	 */
	public function getParamValue(int $id): string
	{
		if ($this->getParams() === null) {
			throw new InvalidArgumentException;
		}

		foreach ($this->getParams() as $param) {
			if ($param->id === $id) {
				return $param->value;
			}
		}

		throw new InvalidArgumentException;
	}

}
