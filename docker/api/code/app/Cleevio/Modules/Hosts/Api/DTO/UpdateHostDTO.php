<?php

declare(strict_types=1);

namespace Cleevio\Hosts\Api\DTO;

use Cleevio\RestApi\Command\ICommandDTO;
use InvalidArgumentException;

class UpdateHostDTO implements ICommandDTO
{

	/**
	 * @var string
	 */
	private $name;

	/**
	 * @var string
	 */
	protected $country;

	/**
	 * @var array|null
	 */
	protected $params;

	public function __construct(
		string $name,
		string $country,
		?array $params
	)
	{
		$this->name = $name;
		$this->country = $country;
		$this->params = $params;
	}

	public static function fromRequest($data): ICommandDTO
	{
		return new static(
			$data->name,
			$data->country,
			property_exists($data, 'params') ? $data->params : null
		);
	}

	public function getName(): string
	{
		return $this->name;
	}

	public function getCountry(): string
	{
		return $this->country;
	}

	public function getParams(): ?array
	{
		return $this->params;
	}

	/**
	 * @param int $id
	 * @return string
	 */
	public function getParamValue(int $id): string
	{
		if ($this->getParams() === null) {
			throw new InvalidArgumentException;
		}

		foreach ($this->getParams() as $param) {
			if ($param->id === $id) {
				return $param->value;
			}
		}

		throw new InvalidArgumentException;
	}

}
