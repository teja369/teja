<?php

declare(strict_types=1);

namespace Cleevio\Hosts\Api\Response;

use Cleevio\Api\Translatable\SimpleResponse;
use Cleevio\Fields\Entities\Field;
use Cleevio\Hosts\Entities\Host;
use Cleevio\RestApi\Response\Types\ContentTypeFactory;

final class HostsCsvResponseBuilder extends SimpleResponse
{
	/**
	 * @var array
	 */
	private $data;

	/**
	 * @var Field[]
	 */
	private $fields;

	/**
	 * @var bool
	 */
	private $header;

	/**
	 * HostsResponseBuilder constructor.
	 * @param array $data
	 * @param array $fields
	 * @param bool $header
	 * @param string|null $lang
	 */
	public function __construct(array $data, array $fields, bool $header, ?string $lang)
	{
		parent::__construct($lang);

		$this->data = $data;
		$this->fields = $fields;
		$this->header = $header;
	}

	protected function getContentType(): string
	{
		return ContentTypeFactory::CONTENT_TYPE_CSV;
	}

	/**
	 * @return array
	 */
	public function data(): array
	{
		$header = ['id', 'type', 'name', 'address', 'country'];

		foreach ($this->fields as $field) {
			$header[] = $field->getName(null);
		}

		$fields = $this->fields;

		$data = array_map(function (Host $host) use ($fields) {
			return (new HostCsvResponseBuilder($host, $fields, $this->getLang()))->build();
		}, $this->data);

		return ($this->header ? [$header] : []) + $data;
	}

}
