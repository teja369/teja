<?php

declare(strict_types=1);

namespace Cleevio\Fields\Validators;

use Cleevio\Fields\Entities\Field;
use Cleevio\Hosts\Entities\Host;
use Cleevio\Hosts\Entities\HostParam;
use Cleevio\Hosts\InvalidFieldException;

class BaseValidator implements IFieldValidator
{

	/**
	 * Validate question answer
	 *
	 * @param Host $host
	 * @param Field $field
	 * @param mixed|null $value
	 * @return HostParam
	 * @throws InvalidFieldException
	 */
	function validate(Host $host, Field $field, $value): HostParam
	{
		if ($field->isRequired() && ($value === null || strlen($value) === 0)) {
			throw new InvalidFieldException("trips.create-trip.host.form.validation.field.required", ['name' => $field->getName(null)]);
		}

		if ($field->getValidation() !== null && $value !== null) {
			$match = preg_match('/' . $field->getValidation() . '/i', $value);

			if ($match === 0 || $match === false) {
				throw new InvalidFieldException("trips.create-trip.host.form.validation.field.pattern", ['name' => $field->getName(null)]);
			}
		}

		return new HostParam($host, $field, (string) $value); //Todo: Non text fields
	}
}
