<?php

declare(strict_types=1);

namespace Cleevio\Fields\Validators;

use Cleevio\Fields\Entities\Field;
use Cleevio\Hosts\Entities\Host;
use Cleevio\Hosts\Entities\HostParam;
use Cleevio\Hosts\InvalidFieldException;

class TextFieldValidator extends BaseValidator
{

	/**
	 * Validate question answer
	 *
	 * @param Host $host
	 * @param Field $field
	 * @param mixed|null $value
	 * @return HostParam
	 * @throws InvalidFieldException
	 */
	function validate(Host $host, Field $field, $value): HostParam
	{
		return parent::validate($host, $field, $value);
	}
}
