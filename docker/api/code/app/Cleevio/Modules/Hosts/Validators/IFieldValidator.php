<?php

declare(strict_types=1);

namespace Cleevio\Fields\Validators;

use Cleevio\Fields\Entities\Field;
use Cleevio\Hosts\Entities\Host;
use Cleevio\Hosts\Entities\HostParam;

interface IFieldValidator
{

	/**
	 * Validate question answer
	 *
	 * @param Host $host
	 * @param Field $field
	 * @param mixed|null $value
	 * @return HostParam
	 */
	function validate(Host $host, Field $field, $value): HostParam;

}
