<?php

declare(strict_types=1);

namespace Cleevio\Fields\Validators;

use Cleevio\Fields\Entities\Field;
use Cleevio\Hosts\Entities\Host;
use Cleevio\Hosts\Entities\HostParam;
use Cleevio\Hosts\InvalidFieldException;

class NumberFieldValidator extends BaseValidator
{

	/**
	 * Validate question answer
	 *
	 * @param Host $host
	 * @param Field $field
	 * @param mixed|null $value
	 * @return HostParam
	 * @throws InvalidFieldException
	 */
	function validate(Host $host, Field $field, $value): HostParam
	{
		if (($value !== null && strlen($value) !== 0) && !is_numeric($value)) {
			throw new InvalidFieldException("trips.create-trip.host.form.validation.field.number", ['name' => $field->getName(null)]);
		}

		return parent::validate($host, $field, $value);
	}
}
