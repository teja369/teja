<?php

declare(strict_types=1);

namespace Cleevio\Hosts\ApiModule\Presenters;

use Cleevio\Api\Helpers\IRequestContext;
use Cleevio\Fields\Entities\Field;
use Cleevio\Fields\FieldTypeInvalid;
use Cleevio\Fields\Repositories\IFieldRepository;
use Cleevio\Hosts\Api\DTO\CreateParamDTO;
use Cleevio\Hosts\Api\Response\HostParamResponseBuilder;
use Cleevio\Hosts\Api\Response\HostResponseBuilder;
use Cleevio\Hosts\Api\Response\HostsParamsResponseBuilder;
use Cleevio\Hosts\Entities\Host;
use Cleevio\Hosts\IHostsService;
use Cleevio\Hosts\InvalidFieldException;
use Cleevio\Hosts\Repositories\HostParamConstraints;
use Cleevio\Hosts\Repositories\IHostParamRepository;
use Cleevio\Hosts\Repositories\IHostRepository;
use Cleevio\RestApi\Annotations\ApiRoute;
use Cleevio\RestApi\Annotations\Authenticated;
use Cleevio\RestApi\Annotations\DTO;
use Cleevio\RestApi\Annotations\JsonSchema;
use Cleevio\RestApi\Annotations\Response;
use Cleevio\RestApi\Exceptions\BadRequestException;
use Cleevio\RestApi\Exceptions\NotFoundException;
use Cleevio\RestApi\Presenters\RestPresenter;
use Cleevio\Trips\Repositories\ITripRepository;
use Cleevio\Trips\Repositories\TripConstraints;
use InvalidArgumentException;
use Nette\Http\IResponse;

/**
 * @ApiRoute("/api/v1/hosts",presenter="Hosts:Api:Params")
 */
class ParamsPresenter extends RestPresenter
{

	/**
	 * @var IHostRepository
	 */
	private $hostRepository;

	/**
	 * @var IRequestContext
	 */
	private $requestContext;
	/**
	 * @var IHostParamRepository
	 */
	private $hostParamRepository;
	/**
	 * @var IFieldRepository
	 */
	private $fieldRepository;
	/**
	 * @var IHostsService
	 */
	private $hostsService;
	/**
	 * @var ITripRepository
	 */
	private $tripRepository;

	/**
	 * @param IFieldRepository $fieldRepository
	 * @param IHostRepository $hostRepository
	 * @param IHostsService $hostsService
	 * @param IHostParamRepository $hostParamRepository
	 * @param ITripRepository $tripRepository
	 * @param IRequestContext $requestContext
	 */
	public function __construct(
		IFieldRepository $fieldRepository,
		IHostRepository $hostRepository,
		IHostsService $hostsService,
		IHostParamRepository $hostParamRepository,
		ITripRepository $tripRepository,
		IRequestContext $requestContext
	)
	{
		parent::__construct();

		$this->hostRepository = $hostRepository;
		$this->requestContext = $requestContext;
		$this->hostParamRepository = $hostParamRepository;
		$this->fieldRepository = $fieldRepository;
		$this->hostsService = $hostsService;
		$this->tripRepository = $tripRepository;
	}

	/**
	 * @ApiRoute("/api/v1/hosts/<host>/param", method="GET", description="Get host parameters")
	 * @Response(200, file="response/hosts/hostParams.json")
	 * @Response(404, code="10002", description="Host does not exist")
	 * @param int $host
	 * @Authenticated("admin.hosts")
	 */
	public function actionParamsList(int $host): void
	{
		$host = $this->hostRepository->find($host);

		if (!$host instanceof Host || $host->isDeleted()) {
			throw new NotFoundException('hosts.host.get.error.host-not-found', 10002);
		}

		$constraints = new HostParamConstraints;
		$constraints->setHost($host);
		$params = $this->hostParamRepository->findAll($constraints->build());

		$this->sendSuccessResponse(new HostsParamsResponseBuilder($params, $this->requestContext->getLang()), IResponse::S200_OK);
	}

	/**
	 * @ApiRoute("/api/v1/hosts/<host>/param/<param>", method="GET", description="Get single parameter value for host")
	 * @Response(200, file="response/hosts/hostParam.json")
	 * @Response(404, code="10002", description="Host does not exist")
	 * @Response(404, code="10003", description="Field does not exist")
	 * @param int $host
	 * @param int $param
	 * @Authenticated("admin.hosts")
	 */
	public function actionParamsGet(int $host, int $param): void
	{
		$host = $this->hostRepository->find($host);

		if (!$host instanceof Host || $host->isDeleted()) {
			throw new NotFoundException('hosts.host.get.error.host-not-found', 10002);
		}

		$field = $this->fieldRepository->find($param);

		if (!$field instanceof Field || $field->isDeleted()) {
			throw new NotFoundException('fields.field.get.error.field-not-found', 10003);
		}

		$constraints = (new HostParamConstraints)
			->setHost($host)
			->setField($field);

		$hostParam = $this->hostParamRepository->findBy($constraints->build());

		if ($hostParam === null) {
			throw new NotFoundException('hosts.params.get.error.param-not-found', 10004);
		}

		$this->sendSuccessResponse(new HostParamResponseBuilder($hostParam, $this->requestContext->getLang()), IResponse::S200_OK);
	}

	/**
	 * @ApiRoute("/api/v1/hosts/<host>/param/<param>", method="PUT", description="Set single parameter value for host")
	 * @Response(200, file="response/hosts/hostParams.json")
	 * @JsonSchema("request/hosts/createParam.json")
	 * @Response(404, code="10002", description="Host does not exist")
	 * @Response(404, code="10003", description="Field does not exist")
	 * @Response(404, code="10004", description="Field type is not supported")
	 * @DTO("Cleevio\Hosts\Api\DTO\CreateParamDTO")
	 * @param int $host
	 * @param int $param
	 * @Authenticated("admin.hosts")
	 */
	public function actionParamsPut(int $host, int $param): void
	{
		$command = $this->getCommandDTO();

		if (!$command instanceof CreateParamDTO) {
			throw new InvalidArgumentException;
		}

		$host = $this->hostRepository->find($host);

		if (!$host instanceof Host || $host->isDeleted()) {
			throw new NotFoundException('hosts.host.get.error.host-not-found', 10002);
		}

		$field = $this->fieldRepository->find($param);

		if (!$field instanceof Field || $field->isDeleted()) {
			throw new NotFoundException('fields.field.get.error.field-not-found', 10003);
		}

		$tripConstraints = new TripConstraints;
		$tripConstraints->setHost($host);

		try {

			$this->hostRepository->beginTransaction();

			if ($this->tripRepository->count($tripConstraints->build()) > 0) {
				$updated = clone $host;
				$this->hostsService->deleteHost($host);
			} else {
				$updated = $host;
			}
			
			$this->hostRepository->persist($updated);

			$constraints = (new HostParamConstraints)
				->setHost($updated)
				->setField($field);

			$hostParam = $this->hostParamRepository->findBy($constraints->build());
			$value = $this->hostsService->validate($updated, $field, $command->getValue());

			if ($hostParam === null) {
				$hostParam = $value;
			} else {
				$hostParam->setValue($value->getValue());
			}

			$this->hostParamRepository->persist($hostParam);

			$this->hostRepository->commitTransaction();

			$this->sendSuccessResponse(new HostResponseBuilder($updated, $this->requestContext->getLang()), IResponse::S200_OK);
		} catch (InvalidFieldException $e) {
			throw new BadRequestException($e->getMessage(), 10003, $e->getParams());
		} catch (FieldTypeInvalid $e) {
			throw new BadRequestException($e->getMessage(), 10004, $e->getParams());
		}
	}

	/**
	 * @ApiRoute("/api/v1/hosts/<host>/param/<param>", method="DELETE", description="Set single parameter value for host")
	 * @Response(200, file="response/hosts/host.json")
	 * @Response(404, code="10002", description="Host does not exist")
	 * @Response(404, code="10003", description="Field does not exist")
	 * @Response(404, code="10004", description="Host and parameter combination was not previusly set")
	 * @param int $host
	 * @param int $param
	 * @Authenticated("admin.hosts")
	 */
	public function actionParamsDelete(int $host, int $param): void
	{
		$host = $this->hostRepository->find($host);

		if (!$host instanceof Host || $host->isDeleted()) {
			throw new NotFoundException('hosts.host.get.error.host-not-found', 10002);
		}

		$field = $this->fieldRepository->find($param);

		if (!$field instanceof Field || $field->isDeleted()) {
			throw new NotFoundException('fields.field.get.error.field-not-found', 10003);
		}

		$tripConstraints = new TripConstraints;
		$tripConstraints->setHost($host);

		if ($this->tripRepository->count($tripConstraints->build()) > 0) {
			$updated = clone $host;
			$this->hostsService->deleteHost($host);
		} else {
			$updated = $host;
		}

		$this->hostRepository->beginTransaction();

		$this->hostRepository->persist($updated);

		$constraints = (new HostParamConstraints)
			->setHost($updated)
			->setField($field);

		$hostParam = $this->hostParamRepository->findBy($constraints->build());

		if ($hostParam === null) {
			throw new NotFoundException('hosts.params.get.error.param-not-found', 10004);
		}

		$this->hostParamRepository->delete($hostParam);

		$this->hostRepository->commitTransaction();

		$this->sendSuccessResponse(new HostResponseBuilder($updated, $this->requestContext->getLang()), IResponse::S200_OK);
	}

}
