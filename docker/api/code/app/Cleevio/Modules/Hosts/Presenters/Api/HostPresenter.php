<?php

declare(strict_types=1);

namespace Cleevio\Hosts\ApiModule\Presenters;

use Cleevio\Api\Helpers\IRequestContext;
use Cleevio\Countries\ICountryService;
use Cleevio\Fields\FieldNotFound;
use Cleevio\Fields\FieldTypeInvalid;
use Cleevio\Fields\IFieldService;
use Cleevio\Hosts\Api\DTO\CreateHostDTO;
use Cleevio\Hosts\Api\DTO\UpdateHostDTO;
use Cleevio\Hosts\Api\Response\HostResponseBuilder;
use Cleevio\Hosts\Api\Response\HostsCsvResponseBuilder;
use Cleevio\Hosts\Api\Response\HostsPagedResponseBuilder;
use Cleevio\Hosts\Entities\Host;
use Cleevio\Hosts\IHostsService;
use Cleevio\Hosts\InvalidFieldException;
use Cleevio\Hosts\Repositories\HostConstraints;
use Cleevio\Hosts\Repositories\IHostRepository;
use Cleevio\Repository\Constraints;
use Cleevio\RestApi\Annotations\ApiRoute;
use Cleevio\RestApi\Annotations\Authenticated;
use Cleevio\RestApi\Annotations\DTO;
use Cleevio\RestApi\Annotations\JsonSchema;
use Cleevio\RestApi\Annotations\Queries;
use Cleevio\RestApi\Annotations\Response;
use Cleevio\RestApi\Exceptions\AuthenticationException;
use Cleevio\RestApi\Exceptions\BadRequestException;
use Cleevio\RestApi\Exceptions\NotFoundException;
use Cleevio\RestApi\Presenters\RestPresenter;
use Cleevio\RestApi\Request\PagedRequest;
use Cleevio\Trips\Entities\Trip;
use Cleevio\Trips\Repositories\ITripRepository;
use Cleevio\Trips\Repositories\TripConstraints;
use Cleevio\Users\Entities\User;
use InvalidArgumentException;
use Nette\Http\IResponse;

/**
 * @ApiRoute("/api/v1/hosts",presenter="Hosts:Api:Host")
 */
class HostPresenter extends RestPresenter
{

	/**
	 * @var IHostsService
	 */
	private $hostsService;

	/**
	 * @var ITripRepository
	 */
	private $tripRepository;

	/**
	 * @var ICountryService
	 */
	private $countryService;

	/**
	 * @var IFieldService
	 */
	private $fieldService;

	/**
	 * @var IHostRepository
	 */
	private $hostRepository;

	/**
	 * @var IRequestContext
	 */
	private $requestContext;


	/**
	 * @param IHostsService $hostsService
	 * @param IHostRepository $hostRepository
	 * @param ITripRepository $tripRepository
	 * @param ICountryService $countryService
	 * @param IFieldService $fieldService
	 * @param IRequestContext $requestContext
	 */
	public function __construct(
		IHostsService $hostsService,
		IHostRepository $hostRepository,
		ITripRepository $tripRepository,
		ICountryService $countryService,
		IFieldService $fieldService,
		IRequestContext $requestContext
	)
	{
		parent::__construct();

		$this->hostsService = $hostsService;
		$this->tripRepository = $tripRepository;
		$this->countryService = $countryService;
		$this->fieldService = $fieldService;
		$this->hostRepository = $hostRepository;
		$this->requestContext = $requestContext;
	}


	/**
	 * @ApiRoute("/api/v1/hosts/<id>", method="GET", description="Get host")
	 * @Response(200, file="response/hosts/hosts.json")
	 * @Response(404, code="10002", description="Host does not exist")
	 * @param int $id
	 * @Authenticated()
	 */
	public function actionHostGet(int $id): void
	{
		$host = $this->hostRepository->find($id);

		if (!$host instanceof Host || $host->isDeleted()) {
			throw new NotFoundException('hosts.host.get.error.host-not-found', 10002);
		}

		$this->sendSuccessResponse(new HostResponseBuilder($host, $this->requestContext->getLang()), IResponse::S200_OK);
	}

	/**
	 * @ApiRoute("/api/v1/hosts", method="GET", description="Fetches all available host entities")
	 * @Response(200, file="response/hosts/hosts.json")
	 * @Response(404, code="10002", description="Trip was not found or belongs to another user")
	 * @Queries("queries/hosts/hosts.json")
	 * @Authenticated()
	 */
	public function actionHostsList(): void
	{
		$authenticator = $this->getAuthenticator();

		$user = $authenticator->getUser();

		if (!$user instanceof User) {
			throw new AuthenticationException("trips.trip.create.error.user-not-found", 10003);
		}

		$tripId = $this->getHttpRequest()->getQuery("trip");

		if ($tripId !== null && $tripId !== "") {

			$trip = $this->tripRepository->find($tripId);

			if (!$trip instanceof Trip || $trip->getState() === Trip::TRIP_STATE_DELETED || $trip->getUser()
					->getId() !== $user->getId()) {
				throw new NotFoundException('trips.trip.get.error.trip-not-found', 10002);
			}

			$country = $trip->getCountry();
		}

		$hostConstraints = new HostConstraints;
		$hostConstraints->setIsDeleted(false);
		$hostConstraints = $this->hostsService->queryToConstraints($hostConstraints, $this->getHttpRequest(), $country ?? null);

		$total = $this->hostRepository->count(Constraints::empty());
		$filtered = $this->hostRepository->count($hostConstraints->build());

		$request = PagedRequest::of($this->getHttpRequest());

		$constraints = $request->toConstraints($hostConstraints)->build();

		$hosts = $this->hostRepository->findAll($constraints);

		$response = (new HostsPagedResponseBuilder($request, $hosts, $this->requestContext->getLang()))
			->setCurrent($filtered)
			->setTotal($total);

		$this->sendSuccessResponse($response, IResponse::S200_OK);
	}

	/**
	 * @ApiRoute("/api/v1/hosts/csv", method="GET", description="Fetches all available host entities")
	 * @Response(200)
	 * @Authenticated("admin.hosts")
	 */
	public function actionHostsCsv(): void
	{
		$i = 0;
		$limit = 1000;
		$constraints = new HostConstraints;
		$constraints->setIsDeleted(false);

		$fields = $this->fieldService->getFields(null, null);

		do {
			$constraints->setLength($limit);
			$constraints->setOffset($i * $limit);
			$hosts = $this->hostRepository->findAll($constraints->build());

			$response = new HostsCsvResponseBuilder($hosts, $fields, $i === 0, $this->requestContext->getLang());
			$response->send($this->getHttpRequest(), $this->getHttpResponse());

			$this->hostRepository->flush();
			++$i;
		} while (count($hosts) > 0);

		exit;
	}

	/**
	 * @ApiRoute("/api/v1/hosts", method="POST", description="Create a new non-client host")
	 * @JsonSchema("request/hosts/create.json")
	 * @Response(200, file="response/hosts/hosts.json")
	 * @Response(404, code="10001", description="Country does not exist")
	 * @Response(404, code="10002", description="Field not found")
	 * @Response(400, code="10003", description="Field validation failed")
	 * @Response(404, code="10004", description="Field type is not supported")
	 * @DTO("Cleevio\Hosts\Api\DTO\CreateHostDTO")
	 * @Authenticated()
	 */
	public function actionHostsPost(): void
	{
		$command = $this->getCommandDTO();

		if (!$command instanceof CreateHostDTO) {
			throw new InvalidArgumentException;
		}

		$country = $this->countryService->findByCode($command->getCountry());

		if ($country === null) {
			throw new BadRequestException("countries.country.get.error.country-not-found", 10001);
		}

		try {
			// Todo: Allow only non client hosts for non administrators after roles are introduced
			$host = $this->hostsService->createHost($command->getType() ?? Host::HOST_TYPE_NON_CLIENT,
				$country, $command->getName());

			if ($command->getParams() !== null) {
				$ids = array_map(static function ($param) {
					return $param->id;
				}, $command->getParams());

				$fields = $this->fieldService->getFieldsByIds($ids);

				$params = [];

				foreach ($fields as $field) {
					$params[] = $this->hostsService->validate($host, $field, $command->getParamValue($field->getId()));
				}

				$host->setParams($params);
				$this->hostRepository->persist($host);
			}
		} catch (FieldNotFound $e) {
			throw new NotFoundException("fields.field.get.error.field-not-found", 10002);
		} catch (InvalidFieldException $e) {
			throw new BadRequestException($e->getMessage(), 10003, $e->getParams());
		} catch (FieldTypeInvalid $e) {
			throw new BadRequestException($e->getMessage(), 10004, $e->getParams());
		}

		$this->sendSuccessResponse(new HostResponseBuilder($host, $this->requestContext->getLang()), IResponse::S200_OK);
	}


	/**
	 * @ApiRoute("/api/v1/host/<id>", method="PUT", description="Updates host")
	 * @JsonSchema("request/hosts/update.json")
	 * @Response(200, file="response/hosts/hosts.json")
	 * @Response(404, code="10001", description="Country not found not found")
	 * @Response(404, code="10002", description="Field not found")
	 * @Response(404, code="10003", description="Host does not exist")
	 * @Response(404, code="10004", description="Field type is not supported")
	 * @DTO("Cleevio\Hosts\Api\DTO\UpdateHostDTO")
	 * @param int $id
	 * @Authenticated()
	 */
	public function actionHostPut(int $id): void
	{
		$command = $this->getCommandDTO();

		if (!$command instanceof UpdateHostDTO) {
			throw new InvalidArgumentException;
		}

		$country = $this->countryService->findByCode($command->getCountry());

		if ($country === null) {
			throw new NotFoundException("countries.country.get.error.country-not-found", 10001);
		}

		$host = $this->hostRepository->find($id);

		if (!$host instanceof Host || $host->isDeleted()) {
			throw new NotFoundException('hosts.host.get.error.host-not-found', 10003);
		}

		$tripConstraints = new TripConstraints;
		$tripConstraints->setHost($host);

		try {
			$this->hostRepository->beginTransaction();

			if ($this->tripRepository->count($tripConstraints->build()) > 0) {
				$updated = clone $host;
				$this->hostsService->deleteHost($host);
			} else {
				$updated = $host;
			}

			if ($command->getParams() !== null) {
				$ids = array_map(static function ($param) {
					return $param->id;
				}, $command->getParams());

				$fields = $this->fieldService->getFieldsByIds($ids);

				$params = [];

				foreach ($fields as $field) {
					$params[] = $this->hostsService->validate($updated, $field, $command->getParamValue($field->getId()));
				}

				$updated->setParams($params);
			}

			$updated->setName($command->getName());
			$updated->setCountry($country);

			$this->hostRepository->persist($updated);

			$this->hostRepository->commitTransaction();

			$this->sendSuccessResponse(new HostResponseBuilder($updated, $this->requestContext->getLang()), IResponse::S200_OK);
		} catch (FieldNotFound $e) {
			throw new NotFoundException('fields.field.get.error.field-not-found', 10002);
		} catch (InvalidFieldException $e) {
			throw new BadRequestException($e->getMessage(), 10003, $e->getParams());
		} catch (FieldTypeInvalid $e) {
			throw new BadRequestException($e->getMessage(), 10004, $e->getParams());
		}
	}


	/**
	 * @ApiRoute("/api/v1/hosts/<id>", method="DELETE", description="Deletes host")
	 * @Response(200)
	 * @Response(404, code="10002", description="Host does not exist")
	 * @param int $id
	 * @Authenticated("admin.hosts")
	 */
	public function actionHostDelete(int $id): void
	{
		$host = $this->hostRepository->find($id);

		if (!$host instanceof Host || $host->isDeleted()) {
			throw new NotFoundException('hosts.host.get.error.host-not-found', 10002);
		}

		$this->hostsService->deleteHost($host);

		$this->sendSuccessResponse(new HostResponseBuilder($host, $this->requestContext->getLang()), IResponse::S200_OK);
	}
}
