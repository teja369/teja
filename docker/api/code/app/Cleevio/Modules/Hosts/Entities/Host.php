<?php

declare(strict_types=1);

namespace Cleevio\Hosts\Entities;

use Cleevio\Countries\Entities\Country;
use Cleevio\Fields\Entities\Field;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\SoftDeleteable\Traits\SoftDeleteableEntity;
use InvalidArgumentException;

/**
 * @ORM\Entity()
 * @ORM\Table(name="hosts")
 */
class Host
{

	use SoftDeleteableEntity;

	public const HOST_TYPE_CLIENT = 'client';

	public const HOST_TYPE_NON_CLIENT = 'non-client';

	/**
	 * @ORM\Id
	 * @ORM\Column(type="integer")
	 * @ORM\GeneratedValue
	 * @var int|null
	 */
	private $id;

	/**
	 * @ORM\Column(name="name", type="string", nullable=false)
	 * @var string
	 */
	private $name;

	/**
	 * @ORM\ManyToOne(targetEntity="Cleevio\Countries\Entities\Country")
	 * @ORM\JoinColumn(name="country_id", referencedColumnName="id", nullable=false)
	 * @var Country
	 */
	protected $country;

	/**
	 * @ORM\Column(type="string", nullable=false)
	 * @var string
	 */
	protected $type;

	/**
	 * @var ArrayCollection
	 * @ORM\OneToMany(targetEntity="Cleevio\Trips\Entities\Trip", mappedBy="host")
	 */
	protected $trips;

	/**
	 * @var ArrayCollection
	 * @ORM\OneToMany(targetEntity="Cleevio\Hosts\Entities\HostParam", mappedBy="host", cascade={"all"}, orphanRemoval=true)
	 */
	protected $params;

	/**
	 * @ORM\OneToMany(targetEntity="Cleevio\Questions\Entities\QuestionHost", mappedBy="host", orphanRemoval=true)
	 * @var ArrayCollection
	 */
	protected $questions;


	/**
	 * @param string $name
	 * @param Country $country
	 * @param string $type
	 */
	public function __construct(string $name, Country $country, string $type)
	{
		if (!in_array($type, [self::HOST_TYPE_CLIENT, self::HOST_TYPE_NON_CLIENT], true)) {
			throw new InvalidArgumentException;
		}

		$this->name = $name;
		$this->country = $country;
		$this->type = $type;
		$this->params = new ArrayCollection;
		$this->questions = new ArrayCollection;
	}


	final public function getId(): int
	{
		if($this->id === null){
			throw new InvalidArgumentException;
		}

		return $this->id;
	}


	public function getName(): string
	{
		return $this->name;
	}


	public function setName(string $name): void
	{
		$this->name = $name;
	}


	public function getCountry(): Country
	{
		return $this->country;
	}


	public function setCountry(Country $country): void
	{
		$this->country = $country;
	}


	public function getType(): string
	{
		return $this->type;
	}


	public function setType(string $type): void
	{
		$this->type = $type;
	}


	/**
	 * @return HostParam[]
	 */
	public function getParams(): array
	{
		return $this->params->getValues();
	}


	/**
	 * @param Field $field
	 * @return HostParam|null
	 */
	public function getParam(Field $field): ?HostParam
	{
		foreach ($this->getParams() as $param) {
			if ($param->getField()->getId() === $field->getId()) {
				return $param;
			}
		}

		return null;
	}

	/**
	 * Build host address from address fields
	 * @return string|null
	 */
	public function getAddress(): ?string
	{
		$address = array_filter($this->getParams(), static function (HostParam $param) {
			return $param->getField()->getAddress() !== null;
		});

		usort($address, static function (HostParam $a1, HostParam $a2) {
			if ($a1->getField()->getAddress() === null || $a2->getField()->getAddress() === null) {
				return 0;
			}

			if ($a1->getField()->getAddress()->getOrder() === $a2->getField()->getAddress()->getOrder()) {
				return 0;
			}

			return $a1->getField()->getAddress()->getOrder() > $a2->getField()->getAddress()->getOrder()
				? 1
				: -1;
		});

		$address = array_map(static function (HostParam $param) {
			return $param->getValue();
		}, $address);

		return count($address) === 0
			? null
			: implode(' ', $address);
	}

	/**
	 * @param HostParam[] $params
	 */
	public function setParams(array $params): void
	{
		$this->params = new ArrayCollection($params);
	}


	/**
	 * CLone trip
	 * @throws \Exception
	 */
	public function __clone()
	{
		$this->id = null;

		$params = $this->getParams();
		$this->params = new ArrayCollection;

		foreach ($params as $param) {
			$param = clone $param;
			$param->setHost($this);
			$this->params->add($param);
		}

		$this->questions = new ArrayCollection;
	}
}
