<?php

declare(strict_types=1);

namespace Cleevio\Hosts\Entities;

use Cleevio\Fields\Entities\Field;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 * @ORM\Table(name="hosts_param")
 */
class HostParam
{

	/**
	 * @ORM\Id
	 * @ORM\Column(type="integer")
	 * @ORM\GeneratedValue
	 * @var int|null
	 */
	private $id;

	/**
	 * @ORM\ManyToOne(targetEntity="Cleevio\Hosts\Entities\Host", inversedBy="params")
	 * @ORM\JoinColumn(name="host_id", referencedColumnName="id", nullable=false)
	 * @var Host
	 */
	protected $host;

	/**
	 * @ORM\ManyToOne(targetEntity="Cleevio\Fields\Entities\Field", inversedBy="hostParams")
	 * @ORM\JoinColumn(name="field_id", referencedColumnName="id", nullable=false)
	 * @var Field
	 */
	protected $field;

	/**
	 * @ORM\Column(name="value", type="string", nullable=false)
	 * @var string
	 */
	private $value;

	/**
	 * @param Host $host
	 * @param Field $field
	 * @param string $value
	 */
	public function __construct(Host $host, Field $field, string $value)
	{
		$this->host = $host;
		$this->field = $field;
		$this->value = $value;
	}

	final public function getId(): int
	{
		if($this->id === null){
			throw new \InvalidArgumentException;
		}

		return $this->id;
	}

	public function getHost(): Host
	{
		return $this->host;
	}

	public function setHost(Host $host): void
	{
		$this->host = $host;
	}

	public function getField(): Field
	{
		return $this->field;
	}

	public function getValue(): string
	{
		return $this->value;
	}

	public function setValue(string $value): void
	{
		$this->value = $value;
	}

	public function __clone()
	{
		$this->id = null;
	}
}
