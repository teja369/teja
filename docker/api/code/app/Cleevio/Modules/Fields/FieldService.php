<?php

declare(strict_types=1);

namespace Cleevio\Fields;

use Cleevio\Countries\Entities\Country;
use Cleevio\Fields\Entities\Address;
use Cleevio\Fields\Entities\Field;
use Cleevio\Fields\Entities\FieldCountry;
use Cleevio\Fields\Entities\Fields\NumberField;
use Cleevio\Fields\Entities\Fields\TextField;
use Cleevio\Fields\Entities\FieldTranslation;
use Cleevio\Fields\Repositories\FieldConstraints;
use Cleevio\Fields\Repositories\IAddressRepository;
use Cleevio\Fields\Repositories\IFieldCountryRepository;
use Cleevio\Fields\Repositories\IFieldRepository;
use Cleevio\Languages\Entities\Language;
use Cleevio\Questions\IFieldTranslation;
use Cleevio\Repository\ConstraintsBuilder;
use DateTime;
use Exception;
use Nette\SmartObject;

class FieldService implements IFieldService
{

	use SmartObject;

	/**
	 * @var IFieldRepository
	 */
	private $fieldRepository;
	/**
	 * @var IFieldCountryRepository
	 */
	private $fieldCountryRepository;
	/**
	 * @var IAddressRepository
	 */
	private $addressRepository;

	/**
	 * @param IFieldRepository $fieldRepository
	 * @param IFieldCountryRepository $fieldCountryRepository
	 * @param IAddressRepository $addressRepository
	 */
	public function __construct(
		IFieldRepository $fieldRepository,
		IFieldCountryRepository $fieldCountryRepository,
		IAddressRepository $addressRepository
	)
	{
		$this->fieldRepository = $fieldRepository;
		$this->fieldCountryRepository = $fieldCountryRepository;
		$this->addressRepository = $addressRepository;
	}

	/**
	 * @param int $id
	 * @return Field|null
	 */
	public function getField(int $id): ?Field
	{
		return $this->fieldRepository->find($id);
	}

	/**
	 * @param Country|null $country
	 * @param string|null $search
	 * @return Field[]
	 */
	public function getFields(?Country $country, ?string $search): array
	{
		$constraints = new FieldConstraints;
		$constraints->setIsDeleted(false);

		if ($search !== null) {
			$constraints->setSearch($search);
		}

		$fields = $this->fieldRepository->findAll($constraints->build());

		$output = [];

		foreach ($fields as $field) {
			if (!$field instanceof Field || $field->isDeleted()) {
				continue;
			}

			if (!$this->hasCountry($field, $country)) {
				continue;
			}

			$output[] = $field;

		}

		return $output;
	}

	/**
	 * Check if field belongs to provided country
	 *
	 * @param Field $field
	 * @param Country|null $country
	 * @return bool
	 */
	private function hasCountry(Field $field, ?Country $country)
	{
		if ($country === null || count($field->getCountries()) === 0) {
			return true;
		}

		foreach ($field->getCountries() as $fieldCountry) {
			if ($fieldCountry->getCountry()->getId() === $country->getId()) {
				return true;
			}
		}

		return false;
	}

	/**
	 * Retrieve purposes by IDs
	 * @param int[] $ids
	 * @return Field[]
	 * @throws FieldNotFound
	 */
	public function getFieldsByIds(array $ids): array
	{
		$ids = array_unique($ids);
		$fields = $this->fieldRepository->findByIds($ids);

		if (count($fields) !== count($ids)) {
			throw new FieldNotFound;
		}

		return $fields;
	}

	/**
	 * @param string $type
	 * @param string $name
	 * @param string|null $placeholder
	 * @param bool $required
	 * @param string|null $validation
	 * @return Field
	 * @throws FieldInvalidName
	 * @throws FieldTypeInvalid
	 * @throws FieldUnique
	 */
	public function createField(string $type, string $name, ?string $placeholder, bool $required, ?string $validation): Field
	{
		switch ($type) {

			case Field::FIELD_TYPE_TEXT:
				$field = new TextField($name);

				break;
			case Field::FIELD_TYPE_NUMBER:
				$field = new NumberField($name);

				break;

				break;
			default:
				throw new FieldTypeInvalid($type);
		}

		return $this->updateField($field, $name, $placeholder, $required, $validation);
	}

	/**
	 * @param Field $field
	 * @param string $name
	 * @param string|null $placeholder
	 * @param bool $required
	 * @param string|null $validation
	 * @return Field
	 * @throws FieldInvalidName
	 * @throws FieldUnique
	 */
	public function updateField(Field $field, string $name, ?string $placeholder, bool $required, ?string $validation): Field
	{
		$nameValidation = preg_match('/^[a-z0-9_]*$/', $name);

		if ($nameValidation === 0 || $nameValidation === false) {
			throw new FieldInvalidName;
		}

		$unique = $this->fieldRepository->findBy((new FieldConstraints)->setName($name)->build());

		if ($unique instanceof Field && !$unique->isDeleted() && (!$field->hasId() || $unique->getId() !== $field->getId())) {
			throw new FieldUnique;
		}

		$field->setName($name);
		$field->setPlaceholder($placeholder !== '' ? $placeholder : null);
		$field->setRequired($required);
		$field->setValidation($validation !== '' ? $validation : null);
		$this->fieldRepository->persist($field);

		return $field;
	}

	/**
	 * @param Field $field
	 * @return void
	 * @throws Exception
	 */
	public function deleteField(Field $field): void
	{
		$field->setDeletedAt(new DateTime);
		$this->fieldRepository->persist($field);
	}

	/**
	 * @param Field $field
	 * @param Country[] $countries
	 * @return Field
	 */
	public function setCountries(Field $field, array $countries): Field
	{
		$field->setCountries(
			array_map(static function (Country $country) use ($field) {
				return new FieldCountry($field, $country);
			}, $countries)
		);

		$this->fieldRepository->persist($field);

		return $field;
	}

	/**
	 * @param Field $field
	 * @param Country $country
	 * @return Field
	 */
	public function deleteCountry(Field $field, Country $country): Field
	{
		foreach ($field->getCountries() as $c) {
			if ($c->getCountry()->getId() === $country->getId()) {
				$this->fieldCountryRepository->delete($c);
			}
		}

		return $field;
	}

	/**
	 * Translate field text parameters
	 * @param Field $field
	 * @param Language $language
	 * @param IFieldTranslation $translation
	 * @return Field
	 */
	public function translateField(Field $field, Language $language, IFieldTranslation $translation): Field
	{
		if ($translation->getName() !== null && $translation->getName() !== '') {
			$field->addTranslation(new FieldTranslation($language->getCode(), "name", $translation->getName()));
		} else {
			$field->clearTranslation("name", $language);
		}

		if ($translation->getPlaceholder() !== null && $translation->getPlaceholder() !== '') {
			$field->addTranslation(new FieldTranslation($language->getCode(), "placeholder", $translation->getPlaceholder()));
		} else {
			$field->clearTranslation("placeholder", $language);
		}

		$this->fieldRepository->persist($field);

		return $field;
	}

	/**
	 * Get all fields for address
	 * @return Address[]
	 */
	public function getAddress(): array
	{
		$constraints = new ConstraintsBuilder;
		$constraints->setOrderBy("order");

		return $this->addressRepository->findAll($constraints->build());
	}

	/**
	 * Get single address field
	 * @param int $id
	 * @return Address|null
	 */
	public function getAddressField(int $id): ?Address
	{
		return $this->addressRepository->find($id);
	}

	/**
	 * @param Address $address
	 */
	public function deleteAddress(Address $address): void
	{
		$this->addressRepository->delete($address);
	}
}
