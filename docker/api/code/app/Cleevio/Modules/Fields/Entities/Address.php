<?php

declare(strict_types=1);

namespace Cleevio\Fields\Entities;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity()
 * @ORM\Table(name="address")
 */
class Address
{

	/**
	 * @ORM\Id
	 * @ORM\Column(type="integer")
	 * @ORM\GeneratedValue
	 * @var int
	 */
	private $id;

	/**
	 * @ORM\OneToOne(targetEntity="Cleevio\Fields\Entities\Field", inversedBy="address")
	 * @ORM\JoinColumn(name="field_id", referencedColumnName="id", nullable=false)
	 * @var Field
	 */
	private $field;

	/**
	 * @Gedmo\SortablePosition()
	 * @ORM\Column(name="`order`", type="integer", nullable=false)
	 * @var int
	 */
	private $order;

	/**
	 * @param Field $field
	 * @param int $order
	 */
	public function __construct(
		Field $field, int $order
	)

	{
		$this->field = $field;
		$this->order = $order;
	}


	/**
	 * @return int
	 */
	public function getId(): int
	{
		return $this->id;
	}


	/**
	 * @param int $id
	 */
	public function setId(int $id): void
	{
		$this->id = $id;
	}

	/**
	 * @return Field
	 */
	public function getField(): Field
	{
		return $this->field;
	}

	/**
	 * @param Field $field
	 */
	public function setField(Field $field): void
	{
		$this->field = $field;
	}

	/**
	 * @return int
	 */
	public function getOrder(): int
	{
		return $this->order;
	}

	/**
	 * @param int $order
	 */
	public function setOrder(int $order): void
	{
		$this->order = $order;
	}

}
