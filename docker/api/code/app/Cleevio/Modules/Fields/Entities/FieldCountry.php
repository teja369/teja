<?php

declare(strict_types=1);

namespace Cleevio\Fields\Entities;

use Cleevio\Countries\Entities\Country;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 * @ORM\Table(name="fields_country")
 */
class FieldCountry
{

	/**
	 * @ORM\Id
	 * @ORM\Column(type="integer")
	 * @ORM\GeneratedValue
	 * @var int
	 */
	private $id;

	/**
	 * @ORM\ManyToOne(targetEntity="Cleevio\Fields\Entities\Field", inversedBy="countries")
	 * @ORM\JoinColumn(name="field_id", referencedColumnName="id", nullable=false)
	 * @var Field
	 */
	private $field;

	/**
	 * @ORM\ManyToOne(targetEntity="Cleevio\Countries\Entities\Country")
	 * @ORM\JoinColumn(name="country_id", referencedColumnName="id", nullable=false)
	 * @var Country
	 */
	private $country;


	public function __construct(
		Field $field,
		Country $country
	)

	{
		$this->field = $field;
		$this->country = $country;
	}


	/**
	 * @return int
	 */
	public function getId(): int
	{
		return $this->id;
	}


	/**
	 * @param int $id
	 */
	public function setId(int $id): void
	{
		$this->id = $id;
	}

	/**
	 * @return Field
	 */
	public function getField(): Field
	{
		return $this->field;
	}

	/**
	 * @param Field $field
	 */
	public function setField(Field $field): void
	{
		$this->field = $field;
	}

	/**
	 * @return Country
	 */
	public function getCountry(): Country
	{
		return $this->country;
	}


	/**
	 * @param Country $country
	 */
	public function setCountry(Country $country): void
	{
		$this->country = $country;
	}

}
