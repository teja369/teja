<?php

declare(strict_types=1);

namespace Cleevio\Fields\Entities\Fields;

use Cleevio\Fields\Entities\Field;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 */
class TextField extends Field
{

	/**
	 * @param string $text
	 */
	public function __construct(string $text)
	{
		parent::__construct($text);
	}

	/**
	 * @return string
	 */
	public function getType(): string
	{
		return self::FIELD_TYPE_TEXT;
	}
}
