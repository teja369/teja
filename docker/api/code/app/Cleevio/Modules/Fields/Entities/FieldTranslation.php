<?php

declare(strict_types=1);

namespace Cleevio\Fields\Entities;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Translatable\Entity\MappedSuperclass\AbstractPersonalTranslation;

/**
 * @ORM\Entity()
 * @ORM\Table(name="fields_translation")
 */
class FieldTranslation extends AbstractPersonalTranslation
{
	/**
	 * @param string $locale
	 * @param string $field
	 * @param string $value
	 */
	public function __construct($locale, $field, $value)
	{
		$this->setLocale($locale);
		$this->setField($field);
		$this->setContent($value);
	}
	/**
	 * @var Field
	 * @ORM\ManyToOne(targetEntity="Field", inversedBy="translations")
	 * @ORM\JoinColumn(name="object_id", referencedColumnName="id", onDelete="CASCADE")
	 */
	protected $object;
}
