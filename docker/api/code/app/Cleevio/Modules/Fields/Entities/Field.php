<?php

declare(strict_types=1);

namespace Cleevio\Fields\Entities;

use Cleevio\Languages\Entities\Language;
use Cleevio\Questions\Entities\QuestionTranslation;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\SoftDeleteable\Traits\SoftDeleteableEntity;
use Gedmo\Translatable\Translatable;

/**
 * @ORM\Entity()
 * @ORM\Table(name="fields")
 * @Gedmo\TranslationEntity(class="FieldTranslation")
 * @ORM\InheritanceType("SINGLE_TABLE")
 * @ORM\DiscriminatorColumn(name="type", type="string")
 * @ORM\DiscriminatorMap({
 *     "base"       = "Field",
 *     "text"       = "Cleevio\Fields\Entities\Fields\TextField",
 *     "number"       = "Cleevio\Fields\Entities\Fields\NumberField"
 * })
 */
abstract class Field implements Translatable
{

	use SoftDeleteableEntity;

	public const FIELD_TYPE_TEXT = 'text';
	public const FIELD_TYPE_NUMBER = 'number';

	/**
	 * @ORM\Id
	 * @ORM\Column(type="integer")
	 * @ORM\GeneratedValue
	 * @var int
	 */
	private $id;

	/**
	 * @ORM\Column(name="name", type="string", nullable=false)
	 * @var string
	 */
	private $name;

	/**
	 * @ORM\Column(name="placeholder", type="string", nullable=true)
	 * @var string|null
	 */
	private $placeholder;

	/**
	 * @ORM\Column(name="required", type="boolean", nullable=false)
	 * @var bool
	 */
	private $required;

	/**
	 * @ORM\OneToOne(targetEntity="Cleevio\Fields\Entities\Address", mappedBy="field")
	 * @var Address|null
	 */
	private $address;

	/**
	 * Validation REGEXP rule
	 * @ORM\Column(name="validation", type="string", nullable=true)
	 * @var string|null
	 */
	private $validation;

	/**
	 * @ORM\OneToMany(targetEntity="FieldTranslation", mappedBy="object", cascade={"all"}, orphanRemoval=true)
	 * @var ArrayCollection
	 */
	private $translations;

	/**
	 * @var ArrayCollection
	 * @ORM\OneToMany(targetEntity="Cleevio\Hosts\Entities\HostParam", mappedBy="field", cascade={"all"}, orphanRemoval=true)
	 */
	private $hostParams;

	/**
	 * @var ArrayCollection
	 * @ORM\OneToMany(targetEntity="Cleevio\Fields\Entities\FieldCountry", mappedBy="field", cascade={"all"}, orphanRemoval=true)
	 */
	private $countries;

	/**
	 * @param string $name Field name/key
	 */
	public function __construct(string $name)
	{
		$this->name = $name;
		$this->required = false;
		$this->countries = new ArrayCollection;
		$this->translations = new ArrayCollection;
	}

	/**
	 * @return int
	 */
	public function getId(): int
	{
		return $this->id;
	}

	/**
	 * @return bool
	 */
	public function hasId(): bool
	{
		return $this->id !== null;
	}

	/**
	 * @param int $id
	 */
	public function setId(int $id): void
	{
		$this->id = $id;
	}

	/**
	 * @param string|null $lang
	 * @return string
	 */
	public function getName(?string $lang): string
	{
		return $this->getTranslation('name', $lang) ?? $this->name;
	}


	/**
	 * @param string $name
	 */
	public function setName(string $name): void
	{
		$this->name = $name;
	}


	/**
	 * @param string|null $lang
	 * @return string|null
	 */
	public function getPlaceholder(?string $lang): ?string
	{
		return $this->getTranslation('placeholder', $lang) ?? $this->placeholder;
	}


	/**
	 * @param string|null $placeholder
	 */
	public function setPlaceholder(?string $placeholder): void
	{
		$this->placeholder = $placeholder;
	}

	/**
	 * @return bool
	 */
	public function isRequired(): bool
	{
		return $this->required;
	}

	/**
	 * @param bool $required
	 */
	public function setRequired(bool $required): void
	{
		$this->required = $required;
	}


	/**
	 * @return string|null
	 */
	public function getValidation(): ?string
	{
		return $this->validation;
	}

	/**
	 * @param string|null $validation
	 */
	public function setValidation(?string $validation): void
	{
		$this->validation = $validation;
	}

	/**
	 * @return Address|null
	 */
	public function getAddress(): ?Address
	{
		return $this->address;
	}

	/**
	 * @return FieldCountry[]
	 */
	public function getCountries(): array
	{
		return $this->countries->getValues();
	}

	/**
	 * @param FieldCountry[] $countries
	 */
	public function setCountries(array $countries): void
	{
		$this->countries = new ArrayCollection($countries);
	}

	/**
	 * @return string
	 */
	abstract public function getType(): string;

	/**
	 * @param string $name
	 * @param string|null $lang
	 * @return string|null
	 */
	private function getTranslation(string $name, ?string $lang): ?string
	{
		foreach ($this->getTranslations() as $translation) {
			if ($translation->getLocale() === $lang && $translation->getField() === $name) {
				return $translation->getContent();
			}
		}

		return null;
	}

	/**
	 * @return QuestionTranslation[]
	 */
	public function getTranslations(): array
	{
		return $this->translations->getValues();
	}


	/**
	 * @param FieldTranslation $t
	 */
	public function addTranslation(FieldTranslation $t): void
	{
		foreach ($this->translations as $translation) {
			if ($translation->getLocale() === $t->getLocale() && $translation->getField() === $t->getField()) {
				$translation->setContent($t->getContent());
				$t->setObject($this);

				return;
			}
		}

		$this->translations[] = $t;
		$t->setObject($this);
	}

	/**
	 * @param string $field
	 * @param Language $language
	 */
	public function clearTranslation(string $field, Language $language): void
	{
		foreach ($this->translations as $translation) {
			if ($translation->getLocale() === $language->getCode() && $translation->getField() === $field) {
				$this->translations->removeElement($translation);

				return;
			}
		}
	}
}
