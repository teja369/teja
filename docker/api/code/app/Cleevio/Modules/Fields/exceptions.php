<?php

declare(strict_types=1);

namespace Cleevio\Fields;

use Cleevio\Exceptions\ParamException;

class FieldNotFound extends ParamException
{

}

class FieldUnique extends ParamException
{

}

class FieldInvalidName extends ParamException
{

}

class FieldTypeInvalid extends ParamException
{

}
