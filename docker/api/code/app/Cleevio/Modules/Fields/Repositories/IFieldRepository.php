<?php

declare(strict_types=1);

namespace Cleevio\Fields\Repositories;

use Cleevio\Fields\Entities\Field;
use Cleevio\Repository\IRepository;

interface IFieldRepository extends IRepository
{

	/**
	 * @param int[] $ids
	 * @return Field[]
	 */
	public function findByIds(array $ids): array;

}
