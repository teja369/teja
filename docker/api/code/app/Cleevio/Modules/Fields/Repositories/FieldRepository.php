<?php

declare(strict_types=1);

namespace Cleevio\Fields\Repositories;

use Cleevio\Fields\Entities\Field;
use Cleevio\Repository\Repositories\SqlRepository;
use Doctrine\Common\Collections\Criteria;

class FieldRepository extends SqlRepository implements IFieldRepository
{

	public function type(): string
	{
		return Field::class;
	}

	/**
	 * @param int[] $ids
	 * @return Field[]
	 */
	public function findByIds(array $ids): array
	{
		$result = $this->getRepository()
			->createQueryBuilder("t")
			->addCriteria(Criteria::create()->where(Criteria::expr()->in("id", $ids)))
			->getQuery()
			->execute();

		return !is_null($result) ? $result : [];
	}
}
