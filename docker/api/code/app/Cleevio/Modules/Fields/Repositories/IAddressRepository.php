<?php

declare(strict_types=1);

namespace Cleevio\Fields\Repositories;

use Cleevio\Repository\IRepository;

interface IAddressRepository extends IRepository
{

}
