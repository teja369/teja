<?php

declare(strict_types=1);

namespace Cleevio\Fields\Repositories;

use Cleevio\Fields\Entities\Address;
use Cleevio\Repository\Repositories\SqlRepository;

class AddressRepository extends SqlRepository implements IAddressRepository
{

	public function type(): string
	{
		return Address::class;
	}

}
