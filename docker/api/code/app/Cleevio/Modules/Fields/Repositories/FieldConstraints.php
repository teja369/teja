<?php

declare(strict_types=1);

namespace Cleevio\Fields\Repositories;

use Cleevio\Countries\Entities\Country;
use Cleevio\Repository\ConstraintsBuilder;
use Cleevio\Repository\Expressions\Comparison;
use Cleevio\Repository\Expressions\IsNull;

class FieldConstraints extends ConstraintsBuilder
{

	/**
	 * @param string $search
	 * @return FieldConstraints
	 */
	function setSearch(string $search): FieldConstraints
	{
		$this->addCriteria(new Comparison('name', 'CONTAINS', $search));

		return $this;
	}

	/**
	 * @param string $name
	 * @return FieldConstraints
	 */
	function setName(string $name): FieldConstraints
	{
		$this->addCriteria(new Comparison('name', '=', $name));

		return $this;
	}

	/**
	 * @param string[] $name
	 * @return FieldConstraints
	 */
	function setNames(array $name): FieldConstraints
	{
		$this->addCriteria(new Comparison('name', 'IN', $name));

		return $this;
	}

	/**
	 * @param bool|null $isDeleted
	 * @return FieldConstraints
	 */
	function setIsDeleted(?bool $isDeleted): FieldConstraints
	{
		if ($isDeleted !== null) {
			if ($isDeleted) {
				$this->addCriteria(new Comparison('deletedAt', '<>', ''));
			} else {
				$this->addCriteria(new IsNull('deletedAt'));
			}
		}

		return $this;
	}

	/**
	 * @param Country $country
	 * @return FieldConstraints
	 */
	function setCountry(Country $country): FieldConstraints
	{
		$this->addCriteria(new Comparison('countries.country.code', '=', $country->getCode()));

		return $this;
	}

}
