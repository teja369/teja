<?php

declare(strict_types=1);

namespace Cleevio\Fields\Repositories;

use Cleevio\Fields\Entities\FieldCountry;
use Cleevio\Repository\Repositories\SqlRepository;

class FieldCountryRepository extends SqlRepository implements IFieldCountryRepository
{

	public function type(): string
	{
		return FieldCountry::class;
	}

}
