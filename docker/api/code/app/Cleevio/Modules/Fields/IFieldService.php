<?php

declare(strict_types=1);

namespace Cleevio\Fields;

use Cleevio\Countries\Entities\Country;
use Cleevio\Fields\Entities\Address;
use Cleevio\Fields\Entities\Field;
use Cleevio\Languages\Entities\Language;
use Cleevio\Questions\IFieldTranslation;

interface IFieldService
{

	/**
	 * @param int $id
	 * @return Field
	 */
	public function getField(int $id): ?Field;

	/**
	 * @param Country|null $country
	 * @param string|null $search
	 * @return Field[]
	 */
	public function getFields(?Country $country, ?string $search): array;

	/**
	 * Retrieve purposes by IDs
	 * @param int[] $ids
	 * @return Field[]
	 * @throws FieldNotFound
	 */
	public function getFieldsByIds(array $ids): array;

	/**
	 * @param string $type
	 * @param string $name
	 * @param string|null $placeholder
	 * @param bool $required
	 * @param string|null $validation
	 * @return Field
	 * @throws FieldInvalidName
	 * @throws FieldTypeInvalid
	 * @throws FieldUnique
	 */
	public function createField(string $type, string $name, ?string $placeholder, bool $required, ?string $validation): Field;

	/**
	 * @param Field $field
	 * @param string $name
	 * @param string|null $placeholder
	 * @param bool $required
	 * @param string|null $validation
	 * @return Field
	 * @throws FieldInvalidName
	 * @throws FieldUnique
	 */
	public function updateField(Field $field, string $name, ?string $placeholder, bool $required, ?string $validation): Field;

	/**
	 * @param Field $id
	 * @return void
	 */
	public function deleteField(Field $id): void;

	/**
	 * @param Field $field
	 * @param Country[] $countries
	 * @return Field
	 */
	public function setCountries(Field $field, array $countries): Field;

	/**
	 * @param Field $field
	 * @param Country $country
	 * @return Field
	 */
	public function deleteCountry(Field $field, Country $country): Field;

	/**
	 * Translate field text parameters
	 * @param Field $field
	 * @param Language $language
	 * @param IFieldTranslation $translation
	 * @return Field
	 */
	public function translateField(Field $field, Language $language, IFieldTranslation $translation): Field;

	/**
	 * Get all fields for address
	 * @return Address[]
	 */
	public function getAddress(): array;

	/**
	 * Get single address field
	 * @param int $id
	 * @return Address|null
	 */
	public function getAddressField(int $id): ?Address;

	/**
	 * @param Address $address
	 */
	public function deleteAddress(Address $address): void;
}
