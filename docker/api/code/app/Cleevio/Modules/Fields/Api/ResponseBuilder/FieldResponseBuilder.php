<?php

declare(strict_types=1);

namespace Cleevio\Fields\Api\Response;

use Cleevio\Api\Translatable\SimpleResponse;
use Cleevio\Countries\Api\Response\CountriesResponseBuilder;
use Cleevio\Fields\Entities\Field;
use Cleevio\Fields\Entities\FieldCountry;

final class FieldResponseBuilder extends SimpleResponse
{

	/**
	 * @var Field
	 */
	private $data;


	/**
	 * FieldResponseBuilder constructor.
	 * @param Field $field
	 * @param string|null $lang
	 */
	public function __construct(Field $field, ?string $lang)
	{
		parent::__construct($lang);

		$this->data = $field;
	}


	/**
	 * @return array
	 */
	protected function data(): array
	{
		return [
			'id' => $this->data->getId(),
			'name' => $this->data->getName($this->getLang()),
			'type' => $this->data->getType(),
			'required' => $this->data->isRequired(),
			'validation' => $this->data->getValidation(),
			'placeholder' => $this->data->getPlaceholder($this->getLang()),
			'countries' => (new CountriesResponseBuilder(array_map(static function (FieldCountry $country) {
				return $country->getCountry();
			}, $this->data->getCountries()), $this->getLang()))->build(),
		];
	}
}
