<?php

declare(strict_types=1);

namespace Cleevio\Fields\Api\DTO;

use Cleevio\RestApi\Command\ICommandDTO;

class UpdateFieldAddressPutDTO implements ICommandDTO
{

	/**
	 * @var int
	 */
	protected $order;

	public function __construct(
		int $order
	)
	{
		$this->order = $order;
	}

	public static function fromRequest($data): ICommandDTO
	{
		return new static(
			$data->order
		);
	}

	/**
	 * @return int
	 */
	public function getOrder(): int
	{
		return $this->order;
	}

}
