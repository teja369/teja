<?php

declare(strict_types=1);

namespace Cleevio\Fields\Api\DTO;

use Cleevio\RestApi\Command\ICommandDTO;

class CreateFieldDTO implements ICommandDTO
{

	/**
	 * @var string
	 */
	private $name;

	/**
	 * @var string
	 */
	private $type;

	/**
	 * @var string|null
	 */
	private $placeholder;

	/**
	 * @var bool
	 */
	private $required;

	/**
	 * @var string|null
	 */
	private $validation;


	public function __construct(
		string $name,
		string $type,
		?string $placeholder,
		bool $required,
		?string $validation
	)
	{
		$this->name = $name;
		$this->type = $type;
		$this->placeholder = $placeholder;
		$this->required = $required;
		$this->validation = $validation;
	}

	public static function fromRequest($data): ICommandDTO
	{
		return new static(
			$data->name,
			$data->type,
			property_exists($data, 'placeholder') ? $data->placeholder : null,
			$data->required,
			property_exists($data, 'validation') ? $data->validation : null
		);
	}

	public function getName(): string
	{
		return $this->name;
	}

	/**
	 * @return string
	 */
	public function getType(): string
	{
		return $this->type;
	}

	/**
	 * @return string|null
	 */
	public function getPlaceholder(): ?string
	{
		return $this->placeholder;
	}

	/**
	 * @return bool
	 */
	public function isRequired(): bool
	{
		return $this->required;
	}

	/**
	 * @return string|null
	 */
	public function getValidation(): ?string
	{
		return $this->validation;
	}
}
