<?php

declare(strict_types=1);

namespace Cleevio\Fields\Api\Response;

use Cleevio\Api\Translatable\SimpleResponse;
use Cleevio\Fields\Entities\Address;

final class AddressResponseBuilder extends SimpleResponse
{

	/**
	 * @var Address
	 */
	private $data;


	/**
	 * AddressResponseBuilder constructor.
	 * @param Address $address
	 * @param string|null $lang
	 */
	public function __construct(Address $address, ?string $lang)
	{
		parent::__construct($lang);

		$this->data = $address;
	}


	/**
	 * @return array
	 */
	protected function data(): array
	{
		return [
			'id' => $this->data->getId(),
			'field' => (new FieldResponseBuilder($this->data->getField(), $this->getLang()))->build(),
			'order' => $this->data->getOrder(),
		];
	}
}
