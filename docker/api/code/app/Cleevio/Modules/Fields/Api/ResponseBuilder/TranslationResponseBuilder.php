<?php

declare(strict_types=1);

namespace Cleevio\Fields\Api\Response;

use Cleevio\Api\Translatable\SimpleResponse;
use Cleevio\Fields\Entities\Field;
use Cleevio\Questions\IFieldTranslation;

final class TranslationResponseBuilder extends SimpleResponse implements IFieldTranslation
{

	/**
	 * @var Field
	 */
	private $data;

	/**
	 * QuestionResponseBuilder constructor.
	 * @param Field $field
	 * @param string|null $lang
	 */
	public function __construct(Field $field, ?string $lang)
	{
		parent::__construct($lang);

		$this->data = $field;
	}

	/**
	 * @return array
	 */
	protected function data(): array
	{
		return [
			'name' => $this->getName(),
			'placeholder' => $this->getPlaceholder(),
		];
	}

	function getName(): ?string
	{
		return $this->data->getName($this->getLang());
	}

	function getPlaceholder(): ?string
	{
		return $this->data->getPlaceholder($this->getLang());
	}

}
