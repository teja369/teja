<?php

declare(strict_types=1);

namespace Cleevio\Fields\Api\DTO;

use Cleevio\RestApi\Command\ICommandDTO;

class CreateFieldAddressPostDTO implements ICommandDTO
{

	/**
	 * @var int
	 */
	protected $fieldId;

	/**
	 * @var int
	 */
	protected $order;


	public function __construct(
		int $fieldId,
		int $order
	)
	{
		$this->fieldId = $fieldId;
		$this->order = $order;
	}

	public static function fromRequest($data): ICommandDTO
	{
		return new static(
			$data->fieldId,
			$data->order
		);
	}

	/**
	 * @return int
	 */
	public function getFieldId(): int
	{
		return $this->fieldId;
	}

	/**
	 * @return int
	 */
	public function getOrder(): int
	{
		return $this->order;
	}

}
