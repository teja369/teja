<?php

declare(strict_types=1);

namespace Cleevio\Fields\Api\Response;

use Cleevio\Api\Translatable\SimpleResponse;
use Cleevio\Fields\Entities\Address;

final class AddressesResponseBuilder extends SimpleResponse
{
	/**
	 * @var array
	 */
	private $data;

	/**
	 * FieldsResponseBuilder constructor.
	 * @param Address[] $data
	 * @param string|null $lang
	 */
	public function __construct(array $data, ?string $lang)
	{
		parent::__construct($lang);

		$this->data = $data;
	}

	/**
	 * @return array
	 */
	protected function data(): array
	{
		return array_map(function (Address $address) {
			return (new AddressResponseBuilder($address, $this->getLang()))->build();
		}, $this->data);
	}

}
