<?php

declare(strict_types=1);

namespace Cleevio\Fields\Api\DTO;

use Cleevio\Questions\IFieldTranslation;
use Cleevio\RestApi\Command\ICommandDTO;

class TranslationPutDTO implements ICommandDTO, IFieldTranslation
{
	/**
	 * @var string|null
	 */
	protected $name;

	/**
	 * @var string|null
	 */
	protected $placeholder;


	public function __construct(
		?string $name,
		?string $placeholder
	)
	{
		$this->name = $name;
		$this->placeholder = $placeholder;
	}

	public static function fromRequest($data): ICommandDTO
	{
		return new static(
			property_exists($data, 'name') ? $data->name : null,
			property_exists($data, 'placeholder') ? $data->placeholder : null
		);
	}

	public function getName(): ?string
	{
		return $this->name;
	}

	public function getPlaceholder(): ?string
	{
		return $this->placeholder;
	}
}
