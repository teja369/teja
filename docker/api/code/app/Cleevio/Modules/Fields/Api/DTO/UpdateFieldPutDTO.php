<?php

declare(strict_types=1);

namespace Cleevio\Fields\Api\DTO;

use Cleevio\RestApi\Command\ICommandDTO;

class UpdateFieldPutDTO implements ICommandDTO
{

	/**
	 * @var string
	 */
	private $name;

	/**
	 * @var string|null
	 */
	private $placeholder;

	/**
	 * @var bool
	 */
	private $required;

	/**
	 * @var string|null
	 */
	private $validation;


	public function __construct(
		string $name,
		?string $placeholder,
		bool $required,
		?string $validation
	)
	{
		$this->name = $name;
		$this->placeholder = $placeholder;
		$this->required = $required;
		$this->validation = $validation;
	}

	public static function fromRequest($data): ICommandDTO
	{
		return new static(
			$data->name,
			property_exists($data, 'placeholder') ? $data->placeholder : null,
			$data->required,
			property_exists($data, 'validation') ? $data->validation : null
		);
	}

	public function getName(): string
	{
		return $this->name;
	}

	/**
	 * @return string|null
	 */
	public function getPlaceholder(): ?string
	{
		return $this->placeholder;
	}

	/**
	 * @return bool
	 */
	public function isRequired(): bool
	{
		return $this->required;
	}

	/**
	 * @return string|null
	 */
	public function getValidation(): ?string
	{
		return $this->validation;
	}
}
