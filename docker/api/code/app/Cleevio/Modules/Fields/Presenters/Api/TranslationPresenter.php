<?php

declare(strict_types=1);

namespace Cleevio\Fields\ApiModule\Presenters;

use Cleevio\Fields\Api\DTO\TranslationPutDTO;
use Cleevio\Fields\Api\Response\TranslationResponseBuilder;
use Cleevio\Fields\Entities\Field;
use Cleevio\Fields\IFieldService;
use Cleevio\Languages\ILanguageService;
use Cleevio\RestApi\Annotations\ApiRoute;
use Cleevio\RestApi\Annotations\Authenticated;
use Cleevio\RestApi\Annotations\DTO;
use Cleevio\RestApi\Annotations\JsonSchema;
use Cleevio\RestApi\Annotations\Response;
use Cleevio\RestApi\Exceptions\AuthenticationException;
use Cleevio\RestApi\Exceptions\NotFoundException;
use Cleevio\RestApi\Presenters\RestPresenter;
use Cleevio\Users\Entities\User;
use InvalidArgumentException;
use Nette\Http\IResponse;

/**
 * @ApiRoute("/api/v1/fields",presenter="Fields:Api:Translation", tags={"Field"})
 */
class TranslationPresenter extends RestPresenter
{

	/**
	 * @var ILanguageService
	 */
	private $languageService;
	
	/**
	 * @var IFieldService
	 */
	private $fieldService;

	/**
	 * @param IFieldService $fieldService
	 * @param ILanguageService $languageService
	 */
	public function __construct(IFieldService $fieldService,
								ILanguageService $languageService)
	{
		parent::__construct();

		$this->languageService = $languageService;
		$this->fieldService = $fieldService;
	}

	/**
	 * @ApiRoute("/api/v1/fields/<field>/translation/<lang>", method="GET", description="Get all translations set for a selected field")
	 * @Response(200, file="response/fields/translation.json")
	 * @Response(404, code="10002", description="Langauge with provided code was not found")
	 * @Response(404, code="10003", description="Field with provided ID was not found")
	 * @Authenticated("admin.hosts")
	 * @param int $field
	 * @param string $lang
	 */
	public function actionList(int $field, string $lang): void
	{
		$authenticator = $this->getAuthenticator();

		$user = $authenticator->getUser();

		if (!$user instanceof User) {
			throw new AuthenticationException("users.auth.error.user-not-found", 10001);
		}

		$field = $this->fieldService->getField($field);

		if (!$field instanceof Field || $field->isDeleted()) {
			throw new NotFoundException('fields.field.get.error.field-not-found', 10003);
		}

		$language = $this->languageService->getLanguage($lang);

		if ($language === null) {
			throw new NotFoundException('languages.language.get.not-found', 10002);
		}

		$this->sendSuccessResponse(new TranslationResponseBuilder($field, $lang), IResponse::S200_OK);
	}

	/**
	 * @ApiRoute("/api/v1/fields/<field>/translation/<lang>", method="PUT", description="Update translations for a provided field")
	 * @Response(200, file="response/fields/translation.json")
	 * @DTO("Cleevio\Fields\Api\DTO\TranslationPutDTO")
	 * @JsonSchema("request/fields/putTranslation.json")
	 * @Response(404, code="10002", description="Langauge with provided code was not found")
	 * @Response(404, code="10003", description="Field with provided ID was not found")
	 * @Authenticated("admin.hosts")
	 * @param int $field
	 * @param string $lang
	 */
	public function actionPut(int $field, string $lang): void
	{
		$authenticator = $this->getAuthenticator();

		$user = $authenticator->getUser();

		if (!$user instanceof User) {
			throw new AuthenticationException("users.auth.error.user-not-found", 10001);
		}

		$command = $this->getCommandDTO();

		if (!$command instanceof TranslationPutDTO) {
			throw new InvalidArgumentException;
		}

		$field = $this->fieldService->getField($field);

		if (!$field instanceof Field || $field->isDeleted()) {
			throw new NotFoundException('fields.field.get.error.field-not-found', 10003);
		}

		$language = $this->languageService->getLanguage($lang);

		if ($language === null) {
			throw new NotFoundException('languages.language.get.not-found', 10002);
		}

		$this->fieldService->translateField($field, $language, $command);

		$this->sendSuccessResponse(new TranslationResponseBuilder($field, $lang), IResponse::S200_OK);
	}

}
