<?php

declare(strict_types=1);

namespace Cleevio\Fields\ApiModule\Presenters;

use Cleevio\Api\Helpers\IRequestContext;
use Cleevio\Fields\Api\DTO\CreateFieldAddressPostDTO;
use Cleevio\Fields\Api\DTO\UpdateFieldAddressPutDTO;
use Cleevio\Fields\Api\Response\AddressesResponseBuilder;
use Cleevio\Fields\Api\Response\AddressResponseBuilder;
use Cleevio\Fields\Entities\Address;
use Cleevio\Fields\IFieldService;
use Cleevio\Fields\Repositories\IAddressRepository;
use Cleevio\RestApi\Annotations\ApiRoute;
use Cleevio\RestApi\Annotations\Authenticated;
use Cleevio\RestApi\Annotations\DTO;
use Cleevio\RestApi\Annotations\JsonSchema;
use Cleevio\RestApi\Annotations\Response;
use Cleevio\RestApi\Exceptions\AuthenticationException;
use Cleevio\RestApi\Exceptions\NotFoundException;
use Cleevio\RestApi\Presenters\RestPresenter;
use Cleevio\RestApi\Response\EmptyResponse;
use Cleevio\Users\Entities\User;
use InvalidArgumentException;
use Nette\Http\IResponse;

/**
 * @ApiRoute("/api/v1/fields/address",presenter="Fields:Api:Address", tags={"Field"})
 */
class AddressPresenter extends RestPresenter
{

	/**
	 * @var IRequestContext
	 */
	private $requestContext;

	/**
	 * @var IFieldService
	 */
	private $fieldService;
	/**
	 * @var IAddressRepository
	 */
	private $addressRepository;

	/**
	 * @param IFieldService $fieldService
	 * @param IAddressRepository $addressRepository
	 * @param IRequestContext $requestContext
	 */
	public function __construct(IFieldService $fieldService,
								IAddressRepository $addressRepository,
								IRequestContext $requestContext)
	{
		parent::__construct();

		$this->fieldService = $fieldService;
		$this->requestContext = $requestContext;
		$this->addressRepository = $addressRepository;
	}

	/**
	 * @ApiRoute("/api/v1/fields/address", method="GET", description="Get all fields in address")
	 * @Response(200, file="response/fields/addresses.json")
	 * @Authenticated("admin.hosts")
	 */
	public function actionAddressList(): void
	{
		$authenticator = $this->getAuthenticator();

		$user = $authenticator->getUser();

		if (!$user instanceof User) {
			throw new AuthenticationException("users.auth.error.user-not-found", 10001);
		}

		$fields = $this->fieldService->getAddress();

		$this->sendSuccessResponse(new AddressesResponseBuilder($fields, $this->requestContext->getLang()), IResponse::S200_OK);
	}

	/**
	 * @ApiRoute("/api/v1/fields/address", method="POST", description="Creates address field")
	 * @JsonSchema("request/fields/createAddress.json")
	 * @Response(200, file="response/fields/address.json")
	 * @Response(404, code="10002", description="Field not found")
	 * @DTO("Cleevio\Fields\Api\DTO\CreateFieldAddressPostDTO")
	 * @Authenticated("admin.hosts")
	 */
	public function actionAddressPost(): void
	{
		$authenticator = $this->getAuthenticator();

		$user = $authenticator->getUser();

		if (!$user instanceof User) {
			throw new AuthenticationException("users.auth.error.user-not-found", 10001);
		}

		$command = $this->getCommandDTO();

		if (!$command instanceof CreateFieldAddressPostDTO) {
			throw new InvalidArgumentException;
		}

		$field = $this->fieldService->getField($command->getFieldId());

		if ($field === null) {
			throw new NotFoundException('fields.field.get.field-not-found', 10002);
		}

		$address = new Address($field, $command->getOrder());
		$this->addressRepository->persist($address);

		$this->sendSuccessResponse(new AddressResponseBuilder($address, $this->requestContext->getLang()), IResponse::S200_OK);
	}

	/**
	 * @ApiRoute("/api/v1/fields/address/<address>", method="PUT", description="Updates address field")
	 * @JsonSchema("request/fields/putAddress.json")
	 * @Response(200, file="response/fields/field.json")
	 * @Response(404, code="10002", description="Address not found")
	 * @DTO("Cleevio\Fields\Api\DTO\UpdateFieldAddressPutDTO")
	 * @param int $address
	 * @Authenticated("admin.hosts")
	 */
	public function actionAddressPut(int $address): void
	{
		$authenticator = $this->getAuthenticator();

		$user = $authenticator->getUser();

		if (!$user instanceof User) {
			throw new AuthenticationException("users.auth.error.user-not-found", 10001);
		}

		$address = $this->fieldService->getAddressField($address);

		if ($address === null) {
			throw new NotFoundException('fields.address.get.address-not-found', 10001);
		}

		$command = $this->getCommandDTO();

		if (!$command instanceof UpdateFieldAddressPutDTO) {
			throw new InvalidArgumentException;
		}

		$address->setOrder($command->getOrder());
		$this->addressRepository->persist($address);

		$this->sendSuccessResponse(new AddressResponseBuilder($address, $this->requestContext->getLang()), IResponse::S200_OK);
	}

	/**
	 * @Authenticated("admin.hosts")
	 * @ApiRoute("/api/v1/fields/address/<address>", method="DELETE", description="Delete address")
	 * @Response(200)
	 * @Response(401, code="10001", description="Address with provided id was not found")
	 * @param int $address
	 */
	public function actionAddressDelete(int $address): void
	{
		$authenticator = $this->getAuthenticator();

		$user = $authenticator->getUser();

		if (!$user instanceof User) {
			throw new AuthenticationException("users.auth.error.user-not-found", 10001);
		}

		$address = $this->fieldService->getAddressField($address);

		if ($address === null) {
			throw new NotFoundException('fields.address.get.address-not-found', 10001);
		}

		$this->fieldService->deleteAddress($address);

		$this->sendSuccessResponse(new EmptyResponse, IResponse::S200_OK);
	}
}
