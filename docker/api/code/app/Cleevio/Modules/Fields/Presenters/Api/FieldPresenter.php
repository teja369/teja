<?php

declare(strict_types=1);

namespace Cleevio\Fields\ApiModule\Presenters;

use Cleevio\Api\Helpers\IRequestContext;
use Cleevio\Fields\Api\DTO\CreateFieldDTO;
use Cleevio\Fields\Api\DTO\UpdateFieldPutDTO;
use Cleevio\Fields\Api\Response\FieldResponseBuilder;
use Cleevio\Fields\Api\Response\FieldsResponseBuilder;
use Cleevio\Fields\Entities\Field;
use Cleevio\Fields\FieldInvalidName;
use Cleevio\Fields\FieldTypeInvalid;
use Cleevio\Fields\FieldUnique;
use Cleevio\Fields\IFieldService;
use Cleevio\RestApi\Annotations\ApiRoute;
use Cleevio\RestApi\Annotations\Authenticated;
use Cleevio\RestApi\Annotations\DTO;
use Cleevio\RestApi\Annotations\JsonSchema;
use Cleevio\RestApi\Annotations\Response;
use Cleevio\RestApi\Exceptions\AuthenticationException;
use Cleevio\RestApi\Exceptions\BadRequestException;
use Cleevio\RestApi\Exceptions\NotFoundException;
use Cleevio\RestApi\Presenters\RestPresenter;
use Cleevio\RestApi\Response\EmptyResponse;
use Cleevio\Trips\Entities\Trip;
use Cleevio\Trips\Repositories\ITripRepository;
use Cleevio\Users\Entities\User;
use InvalidArgumentException;
use Nette\Http\IResponse;

/**
 * @ApiRoute("/api/v1/fields",presenter="Fields:Api:Field")
 */
class FieldPresenter extends RestPresenter
{

	/**
	 * @var IFieldService
	 */
	private $fieldService;

	/**
	 * @var IRequestContext
	 */
	private $requestContext;
	/**
	 * @var ITripRepository
	 */
	private $tripRepository;


	/**
	 * @param IFieldService $fieldService
	 * @param ITripRepository $tripRepository
	 * @param IRequestContext $requestContext
	 */
	public function __construct(
		IFieldService $fieldService,
		ITripRepository $tripRepository,
		IRequestContext $requestContext
	)
	{
		parent::__construct();

		$this->fieldService = $fieldService;
		$this->requestContext = $requestContext;
		$this->tripRepository = $tripRepository;
	}

	/**
	 * @ApiRoute("/api/v1/fields/<field>", method="GET", description="Fetches all available fields")
	 * @Response(200, file="response/fields/fields.json")
	 * @Response(404, code="10003", description="Field with provided ID was not found")
	 * @Authenticated("admin.hosts")
	 * @param int $field
	 */
	public function actionFieldsGet(int $field): void
	{
		$authenticator = $this->getAuthenticator();

		$user = $authenticator->getUser();

		if (!$user instanceof User) {
			throw new AuthenticationException("trips.trip.create.error.user-not-found", 10003);
		}

		$field = $this->fieldService->getField($field);

		if (!$field instanceof Field || $field->isDeleted()) {
			throw new NotFoundException('fields.field.get.error.field-not-found', 10003);
		}

		$this->sendSuccessResponse(new FieldResponseBuilder($field, $this->requestContext->getLang()), IResponse::S200_OK);
	}

	/**
	 * @ApiRoute("/api/v1/fields", method="GET", description="Fetches all available fields")
	 * @Response(200, file="response/fields/fields.json")
	 * @Authenticated()
	 */
	public function actionFieldsList(): void
	{
		$authenticator = $this->getAuthenticator();

		$user = $authenticator->getUser();

		if (!$user instanceof User) {
			throw new AuthenticationException("trips.trip.create.error.user-not-found", 10003);
		}

		$tripId = $this->getHttpRequest()->getQuery("trip");
		$search = $this->getHttpRequest()->getQuery("search");
		$country = null;

		if ($tripId !== null && $tripId !== "") {

			$trip = $this->tripRepository->find($tripId);

			if (!$trip instanceof Trip || $trip->getState() === Trip::TRIP_STATE_DELETED || $trip->getUser()->getId() !== $user->getId()) {
				throw new NotFoundException('trips.trip.get.error.trip-not-found', 10002);
			}

			$country = $trip->getCountry();
		}

		$fields = $this->fieldService->getFields($country, $search);

		$this->sendSuccessResponse(new FieldsResponseBuilder($fields, $this->requestContext->getLang()), IResponse::S200_OK);
	}

	/**
	 * @ApiRoute("/api/v1/fields", method="POST", description="Create a new field")
	 * @JsonSchema("request/fields/create.json")
	 * @Response(200, file="response/fields/field.json")
	 * @DTO("Cleevio\Fields\Api\DTO\CreateFieldDTO")
	 * @Response(400, code="10003", description="Field name must be unique")
	 * @Response(400, code="10004", description="Field name is invalid")
	 * @Authenticated("admin.hosts")
	 */
	public function actionFieldsPost(): void
	{
		$command = $this->getCommandDTO();

		if (!$command instanceof CreateFieldDTO) {
			throw new InvalidArgumentException;
		}

		try {
			$field = $this->fieldService->createField($command->getType(), $command->getName(), $command->getPlaceholder(), $command->isRequired(), $command->getValidation());

			$this->sendSuccessResponse(new FieldResponseBuilder($field, $this->requestContext->getLang()), IResponse::S200_OK);
		} catch (FieldUnique $e) {
			throw new BadRequestException('fields.field.post.error.field-not-unique', 10003);
		} catch (FieldInvalidName $e) {
			throw new BadRequestException('fields.field.post.error.field-name-invalid', 10004);
		} catch (FieldTypeInvalid $e) {
			throw new BadRequestException('fields.field.post.error.field-name-invalid', 10004);
		}
	}

	/**
	 * @ApiRoute("/api/v1/fields/<field>", method="PUT", description="Update field")
	 * @JsonSchema("request/fields/put.json")
	 * @Response(200, file="response/fields/field.json")
	 * @Response(404, code="10003", description="Field with provided ID was not found")
	 * @Response(400, code="10004", description="Field name must be unique")
	 * @Response(400, code="10005", description="Field name is invalid")
	 * @DTO("Cleevio\Fields\Api\DTO\UpdateFieldPutDTO")
	 * @Authenticated("admin.hosts")
	 * @param int $field
	 */
	public function actionFieldsPut(int $field): void
	{
		$command = $this->getCommandDTO();

		if (!$command instanceof UpdateFieldPutDTO) {
			throw new InvalidArgumentException;
		}

		$field = $this->fieldService->getField($field);

		if (!$field instanceof Field || $field->isDeleted()) {
			throw new NotFoundException('fields.field.get.error.field-not-found', 10003);
		}

		try {
			$field = $this->fieldService->updateField($field, $command->getName(), $command->getPlaceholder(), $command->isRequired(), $command->getValidation());

			$this->sendSuccessResponse(new FieldResponseBuilder($field, $this->requestContext->getLang()), IResponse::S200_OK);
		} catch (FieldUnique $e) {
			throw new BadRequestException('fields.field.post.error.field-not-unique', 10004);
		} catch (FieldInvalidName $e) {
			throw new BadRequestException('fields.field.post.error.field-name-invalid', 10005);
		}
	}

	/**
	 * @ApiRoute("/api/v1/fields/<field>", method="DELETE", description="Delete field")
	 * @Response(200)
	 * @Response(404, code="10003", description="Field with provided ID was not found")
	 * @Authenticated("admin.hosts")
	 * @param int $field
	 */
	public function actionFieldDelete(int $field): void
	{
		$field = $this->fieldService->getField($field);

		if (!$field instanceof Field || $field->isDeleted()) {
			throw new NotFoundException('fields.field.get.error.field-not-found', 10003);
		}

		$this->fieldService->deleteField($field);

		$this->sendSuccessResponse(new EmptyResponse, IResponse::S200_OK);
	}

}
