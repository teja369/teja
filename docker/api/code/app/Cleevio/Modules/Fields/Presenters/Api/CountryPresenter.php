<?php

declare(strict_types=1);

namespace Cleevio\Fields\ApiModule\Presenters;

use Cleevio\Api\Helpers\IRequestContext;
use Cleevio\Countries\Api\DTO\CountriesPutDTO;
use Cleevio\Countries\Api\Response\CountriesResponseBuilder;
use Cleevio\Countries\CountryNotFound;
use Cleevio\Countries\ICountryService;
use Cleevio\Fields\Entities\FieldCountry;
use Cleevio\Fields\IFieldService;
use Cleevio\RestApi\Annotations\ApiRoute;
use Cleevio\RestApi\Annotations\Authenticated;
use Cleevio\RestApi\Annotations\DTO;
use Cleevio\RestApi\Annotations\JsonSchema;
use Cleevio\RestApi\Annotations\Response;
use Cleevio\RestApi\Exceptions\AuthenticationException;
use Cleevio\RestApi\Exceptions\NotFoundException;
use Cleevio\RestApi\Presenters\RestPresenter;
use Cleevio\Users\Entities\User;
use InvalidArgumentException;
use Nette\Http\IResponse;

/**
 * @ApiRoute("/api/v1/fields/<field>/country",presenter="Fields:Api:Country", tags={"Field"}, priority=4)
 */
class CountryPresenter extends RestPresenter
{

	/**
	 * @var IRequestContext
	 */
	private $requestContext;

	/**
	 * @var ICountryService
	 */
	private $countryService;

	/**
	 * @var IFieldService
	 */
	private $fieldService;

	/**
	 * @param IFieldService $fieldService
	 * @param IRequestContext $requestContext
	 * @param ICountryService $countryService
	 */
	public function __construct(IFieldService $fieldService,
								IRequestContext $requestContext,
								ICountryService $countryService)
	{
		parent::__construct();

		$this->fieldService = $fieldService;
		$this->requestContext = $requestContext;
		$this->countryService = $countryService;
	}

	/**
	 * @ApiRoute("/api/v1/fields/<field>/country", method="GET", description="Get all countries set for selected field")
	 * @Response(200, file="response/countries/countries.json")
	 * @Response(404, code="10003", description="Field with provided ID was not found")
	 * @Authenticated("admin.hosts")
	 * @param int $field
	 */
	public function actionCountryList(int $field): void
	{
		$authenticator = $this->getAuthenticator();

		$user = $authenticator->getUser();

		if (!$user instanceof User) {
			throw new AuthenticationException("users.auth.error.user-not-found", 10001);
		}

		$field = $this->fieldService->getField($field);

		if ($field === null || $field->isDeleted()) {
			throw new NotFoundException('fields.field.get.error.field-not-found', 10003);
		}

		$countries = array_map(static function (FieldCountry $country) {
			return $country->getCountry();
		}, $field->getCountries());

		$this->sendSuccessResponse(new CountriesResponseBuilder($countries, $this->requestContext->getLang()), IResponse::S200_OK);
	}

	/**
	 * @ApiRoute("/api/v1/fields/<field>/country", method="PUT", description="Set supported countries for provided field")
	 * @Response(200, file="response/countries/countries.json")
	 * @DTO("Cleevio\Countries\Api\DTO\CountriesPutDTO")
	 * @JsonSchema("request/fields/putCountries.json")
	 * @Response(404, code="10003", description="Field with provided ID was not found")
	 * @Response(404, code="10004", description="Country with provided code was not found")
	 * @Authenticated("admin.hosts")
	 * @param int $field
	 */
	public function actionCountryPut(int $field): void
	{
		$authenticator = $this->getAuthenticator();

		$user = $authenticator->getUser();

		if (!$user instanceof User) {
			throw new AuthenticationException("users.auth.error.user-not-found", 10001);
		}

		$command = $this->getCommandDTO();

		if (!$command instanceof CountriesPutDTO) {
			throw new InvalidArgumentException;
		}

		$field = $this->fieldService->getField($field);

		if ($field === null || $field->isDeleted()) {
			throw new NotFoundException('fields.field.get.error.field-not-found', 10003);
		}

		try {
			$countries = $this->countryService->findByIds($command->getIds());

			$this->fieldService->setCountries($field, $countries);

			$this->sendSuccessResponse(new CountriesResponseBuilder($countries, $this->requestContext->getLang()), IResponse::S200_OK);
		} catch (CountryNotFound $e) {
			throw new NotFoundException('countries.country.get.error.country-not-found', 10004);
		}
	}

	/**
	 * @ApiRoute("/api/v1/fields/<field>/country/<country>", method="DELETE", description="Delete supported countries for provided field")
	 * @Response(200, file="response/countries/countries.json")
	 * @Response(404, code="10003", description="Field with provided ID was not found")
	 * @Response(404, code="10004", description="Country with provided code was not found")
	 * @Authenticated("admin.hosts")
	 * @param int $field
	 * @param string $country
	 */
	public function actionCountryDelete(int $field, string $country): void
	{
		$authenticator = $this->getAuthenticator();

		$user = $authenticator->getUser();

		if (!$user instanceof User) {
			throw new AuthenticationException("users.auth.error.user-not-found", 10001);
		}

		$field = $this->fieldService->getField($field);

		if ($field === null || $field->isDeleted()) {
			throw new NotFoundException('fields.field.get.error.field-not-found', 10003);
		}

		$country = $this->countryService->findByCode($country);

		if ($country === null) {
			throw new NotFoundException('countries.country.get.error.country-not-found', 10004);
		}

		$this->fieldService->deleteCountry($field, $country);

		$countries = array_map(static function (FieldCountry $country) {
			return $country->getCountry();
		}, $field->getCountries());

		$this->sendSuccessResponse(new CountriesResponseBuilder($countries, $this->requestContext->getLang()), IResponse::S200_OK);
	}

}
