<?php

declare(strict_types=1);

namespace Cleevio\Questions;

/**
 * Interface IFieldTranslation
 * Getters for all translatable fields in field
 * @package Cleevio\Questions
 */
interface IFieldTranslation
{

	function getName(): ?string;

	function getPlaceholder(): ?string;

}
