<?php

declare(strict_types=1);

namespace Cleevio\Questions;

use Cleevio\Exceptions\ParamException;

class RegistrationNotFound extends ParamException
{

}
