<?php

declare(strict_types=1);

namespace Cleevio\Registrations\ApiModule\Presenters;

use Cleevio\Api\Helpers\IRequestContext;
use Cleevio\Registrations\Api\Response\RegistrationsPagedResponseBuilder;
use Cleevio\Registrations\Repositories\IRegistrationRepository;
use Cleevio\Repository\Constraints;
use Cleevio\Repository\ConstraintsBuilder;
use Cleevio\RestApi\Annotations\ApiRoute;
use Cleevio\RestApi\Annotations\Authenticated;
use Cleevio\RestApi\Annotations\Response;
use Cleevio\RestApi\Presenters\RestPresenter;
use Cleevio\RestApi\Request\PagedRequest;
use Nette\Http\IResponse;

/**
 * @ApiRoute("/api/v1/registrations",presenter="Registrations:Api:Registration")
 */
class RegistrationPresenter extends RestPresenter
{
	/**
	 * @var IRegistrationRepository
	 */
	private $registrationRepository;
	/**
	 * @var IRequestContext
	 */
	private $requestContext;

	/**
	 * @param IRegistrationRepository $registrationRepository
	 * @param IRequestContext $requestContext
	 */
	public function __construct(IRegistrationRepository $registrationRepository,
								IRequestContext $requestContext)
	{
		parent::__construct();

		$this->registrationRepository = $registrationRepository;
		$this->requestContext = $requestContext;
	}

	/**
	 * @ApiRoute("/api/v1/registrations", method="GET", description="Fetches all available registrations")
	 * @Response(200, file="response/registrations/registrationsPaged.json")
	 * @Authenticated()
	 */
	public function actionRegistrationsGet(): void
	{
		$hostConstraints = new ConstraintsBuilder;

		$total = $this->registrationRepository->count(Constraints::empty());
		$filtered = $this->registrationRepository->count($hostConstraints->build());

		$request = PagedRequest::of($this->getHttpRequest());

		$constraints = $request->toConstraints($hostConstraints)->build();

		$registrations = $this->registrationRepository->findAll($constraints);

		$response = (new RegistrationsPagedResponseBuilder($request, $registrations, $this->requestContext->getLang()))
			->setCurrent($filtered)
			->setTotal($total);

		$this->sendSuccessResponse($response, IResponse::S200_OK);
	}

}
