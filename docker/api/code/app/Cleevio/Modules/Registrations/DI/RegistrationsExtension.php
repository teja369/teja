<?php

declare(strict_types=1);

namespace Cleevio\Registrations\DI;

use Cleevio\Modules\Providers\IPresenterMappingProvider;
use Nette\DI\CompilerExtension;
use Nettrine\ORM\DI\Traits\TEntityMapping;

class RegistrationsExtension extends CompilerExtension implements IPresenterMappingProvider
{

	use TEntityMapping;

	public function loadConfiguration()
	{
		$this->compiler->loadConfig(__DIR__ . '/services.neon');
		$this->setEntityMappings(
			[
				'Cleevio\Registrations\Entities\Registration' => __DIR__ . '/..',
			]
		);
	}


	public function getPresenterMapping(): array
	{
		return ['Registrations' => 'Cleevio\\Registrations\\*Module\\Presenters\\*Presenter'];
	}

}
