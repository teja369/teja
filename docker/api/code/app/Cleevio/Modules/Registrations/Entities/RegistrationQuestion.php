<?php

declare(strict_types=1);

namespace Cleevio\Registrations\Entities;

use Cleevio\Questions\Entities\Question;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 * @ORM\Table(name="registrations_question")
 */
class RegistrationQuestion
{

	/**
	 * @ORM\Id
	 * @ORM\Column(type="integer")
	 * @ORM\GeneratedValue
	 * @var int
	 */
	private $id;

	/**
	 * @ORM\ManyToOne(targetEntity="Cleevio\Questions\Entities\Question", inversedBy="registrations")
	 * @ORM\JoinColumn(name="question_id", referencedColumnName="id", nullable=false)
	 * @var Question
	 */
	private $question;

	/**
	 * @ORM\ManyToOne(targetEntity="Cleevio\Registrations\Entities\Registration", inversedBy="questions")
	 * @ORM\JoinColumn(name="registration_id", referencedColumnName="id", nullable=false)
	 * @var Registration
	 */
	private $registration;


	/**
	 * RegistrationsQuestion constructor.
	 * @param Question     $question
	 * @param Registration $registration
	 */
	public function __construct(
		Question $question,
		Registration $registration
	)
	{
		$this->question	= $question;
		$this->registration = $registration;
	}


	/**
	 * @return int
	 */
	final public function getId(): int
	{
		return $this->id;
	}


	/**
	 * @return Question
	 */
	public function getQuestion(): Question
	{
		return $this->question;
	}


	/**
	 * @param Question $question
	 */
	public function setQuestion(Question $question): void
	{
		$this->question = $question;
	}


	/**
	 * @return Registration
	 */
	public function getRegistration(): Registration
	{
		return $this->registration;
	}


	/**
	 * @param Registration $registration
	 */
	public function setRegistration(Registration $registration): void
	{
		$this->registration = $registration;
	}

}
