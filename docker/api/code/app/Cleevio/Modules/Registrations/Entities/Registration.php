<?php

declare(strict_types=1);

namespace Cleevio\Registrations\Entities;

use Cleevio\Questions\Entities\Question;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 * @ORM\Table(name="registrations")
 */
class Registration
{

	/**
	 * @ORM\Id
	 * @ORM\Column(type="integer")
	 * @ORM\GeneratedValue
	 * @var int
	 */
	private $id;

	/**
	 * @ORM\Column(name="name", type="string", nullable=false)
	 * @var string
	 */
	private $name;

	/**
	 * @ORM\OneToMany(targetEntity="Cleevio\Registrations\Entities\RegistrationQuestion", mappedBy="registration", cascade={"all"}, orphanRemoval=true)
	 * @ORM\JoinColumn(name="question_id", unique=false, nullable=false)
	 * @var ArrayCollection
	 */
	private $questions;


	/**
	 * @param string $name
	 */
	public function __construct(string $name)
	{
		$this->name = $name;
		$this->questions = new ArrayCollection;
	}


	final public function getId(): int
	{
		return $this->id;
	}


	public function getName(): string
	{
		return $this->name;
	}


	/**
	 * @return Question[]
	 */
	public function getQuestions(): array
	{
		return $this->questions->getValues();
	}

	/**
	 * @param Question[] $questions
	 */
	public function setQuestions(array $questions): void
	{
		$this->questions = new ArrayCollection($questions);
	}

}
