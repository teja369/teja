<?php

declare(strict_types=1);

namespace Cleevio\Registrations;

use Cleevio\Questions\RegistrationNotFound;
use Cleevio\Registrations\Entities\Registration;
use Cleevio\Registrations\Repositories\IRegistrationRepository;

class RegistrationService implements IRegistrationService
{
	/**
	 * @var IRegistrationRepository
	 */
	private $registrationRepository;

	public function __construct(IRegistrationRepository $registrationRepository)
	{
		$this->registrationRepository = $registrationRepository;
	}

	/**
	 * @param int $id
	 * @return Registration|null
	 */
	public function getRegistration(int $id): ?Registration
	{
		return $this->registrationRepository->find($id);
	}

	/**
	 * @param array $ids
	 * @return Registration[]
	 * @throws RegistrationNotFound
	 */
	public function findByIds(array $ids): array
	{
		$ids = array_unique($ids);
		$registrations = $this->registrationRepository->findByIds($ids);

		if (count($registrations) !== count($ids)) {
			throw new RegistrationNotFound;
		}

		return $registrations;
	}
}
