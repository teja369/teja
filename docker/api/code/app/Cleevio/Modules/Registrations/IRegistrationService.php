<?php

declare(strict_types=1);

namespace Cleevio\Registrations;

use Cleevio\Questions\RegistrationNotFound;
use Cleevio\Registrations\Entities\Registration;

interface IRegistrationService
{
	/**
	 * @param int $id
	 * @return Registration|null
	 */
	public function getRegistration(int $id): ?Registration;

	/**
	 * @param array $ids
	 * @return Registration[]
	 * @throws RegistrationNotFound
	 */
	public function findByIds(array $ids): array;

}
