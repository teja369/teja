<?php

declare(strict_types=1);

namespace Cleevio\Registrations\Api\Response;

use Cleevio\Api\Translatable\PagedResponse;
use Cleevio\Registrations\Entities\Registration;
use Cleevio\RestApi\Request\PagedRequest;

final class RegistrationsPagedResponseBuilder extends PagedResponse
{
	/**
	 * @var array
	 */
	private $data;


	/**
	 * RegistrationsPagedResponseBuilder constructor.
	 * @param PagedRequest $request
	 * @param array $data
	 * @param string|null $lang
	 */
	public function __construct(PagedRequest $request, array $data, ?string $lang)
	{
		parent::__construct($request, $lang);

		$this->data = $data;
	}

	/**
	 * @return array
	 */
	protected function data(): array
	{
		return array_map(static function (Registration $registration) {
			return (new RegistrationResponseBuilder($registration))->build();
		}, $this->data);
	}

}
