<?php

declare(strict_types=1);

namespace Cleevio\Registrations\Api\Response;

use Cleevio\Registrations\Entities\Registration;
use Cleevio\RestApi\Response\SimpleResponse;

final class RegistrationsResponseBuilder extends SimpleResponse
{
	/**
	 * @var array
	 */
	private $data;

	/**
	 * CountriesResponseBuilder constructor.
	 * @param array $data
	 */
	public function __construct(array $data)
	{
		$this->data = $data;
	}

	/**
	 * @return array
	 */
	protected function data(): array
	{
		return array_map(static function (Registration $purpose) {
			return (new RegistrationResponseBuilder($purpose))->build();
		}, $this->data);
	}

}
