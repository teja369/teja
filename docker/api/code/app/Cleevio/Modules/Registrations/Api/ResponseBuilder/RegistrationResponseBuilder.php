<?php

declare(strict_types=1);

namespace Cleevio\Registrations\Api\Response;

use Cleevio\Registrations\Entities\Registration;
use Cleevio\RestApi\Response\SimpleResponse;

final class RegistrationResponseBuilder extends SimpleResponse
{

	/**
	 * @var Registration
	 */
	private $data;

	/**
	 * RegistrationResponseBuilder constructor.
	 * @param Registration $registration
	 */
	public function __construct(Registration $registration)
	{
		$this->data = $registration;
	}

	/**
	 * @return array
	 */
	protected function data(): array
	{
		return [
			'id' => $this->data->getId(),
			'name' => $this->data->getName(),
		];
	}
}
