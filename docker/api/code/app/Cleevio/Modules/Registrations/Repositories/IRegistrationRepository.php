<?php

declare(strict_types=1);

namespace Cleevio\Registrations\Repositories;

use Cleevio\Registrations\Entities\Registration;
use Cleevio\Repository\IRepository;

interface IRegistrationRepository extends IRepository
{

	/**
	 * @param int[] $ids
	 * @return Registration[]
	 */
	public function findByIds(array $ids): array;

}
