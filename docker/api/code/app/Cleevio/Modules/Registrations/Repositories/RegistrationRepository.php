<?php

declare(strict_types=1);

namespace Cleevio\Registrations\Repositories;

use Cleevio\Registrations\Entities\Registration;
use Cleevio\Repository\Repositories\SqlRepository;
use Doctrine\Common\Collections\Criteria;

class RegistrationRepository extends SqlRepository implements IRegistrationRepository
{

	public function type(): string
	{
		return Registration::class;
	}

	/**
	 * @param int[] $ids
	 * @return Registration[]
	 */
	public function findByIds(array $ids): array
	{
		$result = $this->getRepository()
			->createQueryBuilder("t")
			->addCriteria(Criteria::create()->where(Criteria::expr()->in("id", $ids)))
			->getQuery()
			->execute();

		return !is_null($result) ? $result : [];
	}
}
