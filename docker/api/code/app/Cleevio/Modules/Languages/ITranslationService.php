<?php

declare(strict_types=1);

namespace Cleevio\Languages;

use Cleevio\Languages\Entities\Language;
use Cleevio\Languages\Repositories\TranslationConstraints;
use Cleevio\Languages\Repositories\TranslationKeyConstraints;
use Cleevio\Translations\TranslationKeyNotFound;
use DateTime;
use Nette\Http\IRequest;

interface ITranslationService
{
	/**
	 * @param string $key
	 * @param Language|null $language
	 * @param array|null $params
	 * @return string
	 */
	public function translate(string $key, ?Language $language, ?array $params = []): string;

	/**
	 * @param TranslationConstraints $constraints
	 * @return array
	 */
	public function getTranslations(TranslationConstraints $constraints): array;


	/**
	 * @param TranslationKeyConstraints $constraints
	 * @return array
	 */
	public function getTranslationKeys(TranslationKeyConstraints $constraints): array;


	/**
	 * @param Language $language
	 * @param array $translations
	 * @param bool $update
	 * @throws TranslationKeyNotFound
	 */
	public function updateTranslations(Language $language, array $translations, ?bool $update = false): void;

	/**
	 * Get translations last modification date
	 * @return DateTime|null
	 */
	public function getModified(): ?DateTime;

	/**
	 * @param TranslationConstraints $constraints
	 * @param string $code
	 * @param IRequest $request
	 * @return TranslationConstraints
	 */
	public function queryToConstraints(
		TranslationConstraints $constraints,
		string $code,
		IRequest $request
	): TranslationConstraints;
}
