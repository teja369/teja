<?php

declare(strict_types=1);

namespace Cleevio\Languages;

use Cleevio\Countries\Entities\Country;
use Cleevio\Languages\Entities\Language;

interface ILanguageService
{

	/**
	 * @param string $code
	 * @return Language|null
	 */
	public function getLanguage(string $code): ?Language;

	/**
	 * @return Language[]
	 */
	public function getLanguages(): array;

	/**
	 * Set default language
	 * @return Language|null
	 */
	public function getDefaultLanguage(): ?Language;

	/**
	 * Set default language
	 * @param Language $language
	 * @return Language
	 */
	public function setDefaultLanguage(Language $language): Language;

	/**
	 * @param string $code
	 * @param string $name
	 * @param Country $country
	 * @return Language
	 */
	public function createLanguage(string $code, string $name, Country $country): Language;

	/**
	 * @param Language $language
	 * @param string $name
	 * @param Country $country
	 * @return Language
	 */
	public function updateLanguage(Language $language, string $name, Country $country): Language;

	/**
	 * @param Language $language
	 * @return void
	 */
	public function deleteLanguage(Language $language): void;
}
