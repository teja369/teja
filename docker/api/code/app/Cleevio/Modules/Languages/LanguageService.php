<?php

declare(strict_types=1);

namespace Cleevio\Languages;

use Cleevio\Countries\Entities\Country;
use Cleevio\Languages\Entities\Language;
use Cleevio\Languages\Repositories\ILanguageRepository;
use Cleevio\Languages\Repositories\LanguageConstraints;
use Cleevio\Repository\Constraints;
use Nette\SmartObject;

class LanguageService implements ILanguageService
{

	use SmartObject;

	/**
	 * @var ILanguageRepository
	 */
	private $languageRepository;


	public function __construct(ILanguageRepository $languageRepository)
	{
		$this->languageRepository = $languageRepository;
	}

	/**
	 * @return Language[]
	 */
	function getLanguages(): array
	{
		return $this->languageRepository->findAll(Constraints::empty());
	}

	/**
	 * @param string $code
	 * @return Language|null
	 */
	public function getLanguage(string $code): ?Language
	{
		return $this->languageRepository->findBy((new LanguageConstraints)->setCode($code)->build());
	}

	/**
	 * Set default language
	 * @return Language|null
	 */
	public function getDefaultLanguage(): ?Language
	{
		$default = null;

		foreach ($this->getLanguages() as $language) {
			if ($default === null || $language->isDefault()) {
				$default = $language;
			}
		}

		return $default;
	}

	/**
	 * Set default language
	 * @param Language $language
	 * @return Language
	 */
	public function setDefaultLanguage(Language $language): Language
	{
		$current = $this->languageRepository->findBy((new LanguageConstraints)->setDefault()->build());

		if ($current instanceof Language) {
			$current->setIsDefault(false);
			$this->languageRepository->persist($language);
		}

		$language->setIsDefault(true);
		$this->languageRepository->persist($language);

		return $language;
	}

	/**
	 * @param string $code
	 * @param string $name
	 * @param Country $country
	 * @return Language
	 */
	public function createLanguage(string $code, string $name, Country $country): Language
	{
		$language = new Language($name, $code, $country);

		$this->languageRepository->persist($language);

		return $language;
	}

	/**
	 * @param Language $language
	 * @param string $name
	 * @param Country $country
	 * @return Language
	 */
	public function updateLanguage(Language $language, string $name, Country $country): Language
	{
		$language->setName($name);
		$language->setCountry($country);

		$this->languageRepository->persist($language);

		return $language;
	}

	/**
	 * @param Language $language
	 * @return void
	 */
	public function deleteLanguage(Language $language): void
	{
		$this->languageRepository->delete($language);
	}

}
