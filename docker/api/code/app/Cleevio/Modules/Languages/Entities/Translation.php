<?php

declare(strict_types=1);

namespace Cleevio\Languages\Entities;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;

/**
 * @ORM\Entity()
 * @ORM\Table(name="translations")
 */
class Translation
{

	use TimestampableEntity;

	/**
	 * @ORM\Id
	 * @ORM\Column(type="integer")
	 * @ORM\GeneratedValue
	 * @var int
	 */
	private $id;

	/**
	 * @ORM\ManyToOne(targetEntity="TranslationKey", cascade={"persist"})
	 * @ORM\JoinColumn(name="translation_key_id", referencedColumnName="id", nullable=false)
	 * @var TranslationKey
	 */
	protected $key;

	/**
	 * @ORM\ManyToOne(targetEntity="Language", cascade={"persist"})
	 * @ORM\JoinColumn(name="language_id", referencedColumnName="id", nullable=false)
	 * @var Language
	 */
	protected $language;

	/**
	 * @ORM\Column(type="string", nullable=false)
	 * @var string
	 */
	protected $value;

	/**
	 * @param TranslationKey $key
	 * @param Language $language
	 * @param string $value
	 */
	public function __construct(
		TranslationKey $key,
		Language $language,
		string $value
	)
	{
		$this->key = $key;
		$this->language = $language;
		$this->value = $value;
	}


	/**
	 * @return int
	 */
	public function getId(): int
	{
		return $this->id;
	}


	/**
	 * @return TranslationKey
	 */
	public function getKey(): TranslationKey
	{
		return $this->key;
	}


	/**
	 * @param TranslationKey $key
	 */
	public function setKey(TranslationKey $key): void
	{
		$this->key = $key;
	}


	/**
	 * @return Language
	 */
	public function getLanguage(): Language
	{
		return $this->language;
	}


	/**
	 * @param Language $language
	 */
	public function setLanguage(Language $language): void
	{
		$this->language = $language;
	}


	/**
	 * @return string
	 */
	public function getValue(): string
	{
		return $this->value;
	}


	/**
	 * @param string $value
	 */
	public function setValue(string $value): void
	{
		$this->value = $value;
	}

}
