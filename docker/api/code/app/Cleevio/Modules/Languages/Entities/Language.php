<?php

declare(strict_types=1);

namespace Cleevio\Languages\Entities;

use Cleevio\Countries\Entities\Country;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 * @ORM\Table(name="languages")
 */
class Language
{

	/**
	 * @ORM\Id
	 * @ORM\Column(type="integer")
	 * @ORM\GeneratedValue
	 * @var int
	 */
	private $id;

	/**
	 * @ORM\Column(type="string", nullable=false)
	 * @var string
	 */
	protected $name;

	/**
	 * @ORM\Column(type="string", nullable=false)
	 * @var string
	 */
	protected $code;

	/**
	 * @ORM\Column(name="is_default", type="boolean", nullable=false)
	 * @var bool
	 */
	private $isDefault;

	/**
	 * @ORM\ManyToOne(targetEntity="Cleevio\Countries\Entities\Country", cascade={"persist"})
	 * @ORM\JoinColumn(name="country_id", referencedColumnName="id", nullable=false)
	 * @var Country
	 */
	protected $country;

	/**
	 * @ORM\OneToMany(targetEntity="Translation", mappedBy="language", cascade={"all"}, orphanRemoval=true)
	 * @var Collection
	 */
	protected $translations;


	public function __construct(
		string $name,
		string $code,
		Country $country
	)
	{
		$this->code = $code;
		$this->name = $name;
		$this->isDefault = false;
		$this->country = $country;
		$this->translations = new ArrayCollection;
	}


	/**
	 * @return int
	 */
	final public function getId(): int
	{
		return $this->id;
	}


	/**
	 * @return string
	 */
	public function getName(): string
	{
		return $this->name;
	}


	/**
	 * @param string $name
	 */
	public function setName(string $name): void
	{
		$this->name = $name;
	}

	/**
	 * @return string
	 */
	public function getCode(): string
	{
		return $this->code;
	}


	/**
	 * @param string $code
	 */
	public function setCode(string $code): void
	{
		$this->code = $code;
	}

	/**
	 * @return bool
	 */
	public function isDefault(): bool
	{
		return $this->isDefault;
	}

	/**
	 * @param bool $isDefault
	 */
	public function setIsDefault(bool $isDefault): void
	{
		$this->isDefault = $isDefault;
	}

	/**
	 * @return Country
	 */
	public function getCountry(): Country
	{
		return $this->country;
	}

	/**
	 * @param Country $country
	 */
	public function setCountry(Country $country): void
	{
		$this->country = $country;
	}
}
