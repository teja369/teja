<?php

declare(strict_types=1);

namespace Cleevio\Languages\Entities;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 * @ORM\Table(name="translations_key")
 */
class TranslationKey
{

	/**
	 * @ORM\Id
	 * @ORM\Column(type="integer")
	 * @ORM\GeneratedValue
	 * @var int
	 */
	private $id;

	/**
	 * @ORM\Column(name="`key`", type="string", nullable=false, unique=true)
	 * @var string
	 */
	private $key;

	/**
	 * @ORM\OneToMany(targetEntity="Translation", mappedBy="key")
	 * @var Collection
	 */
	protected $translations;

	/**
	 * @param string $key
	 */
	public function __construct(string $key)
	{
		$this->key = $key;
	}

	public function getId(): int
	{
		return $this->id;
	}

	public function getKey(): string
	{
		return $this->key;
	}

	/**
	 * @return Translation[]
	 */
	public function getTranslations(): array
	{
		return $this->translations->getValues();
	}

	/**
	 * @param Translation[] $translations
	 */
	public function setTranslations(array $translations): void
	{
		$this->translations = new ArrayCollection($translations);
	}

	/**
	 * @param Language $language
	 * @return Translation|null
	 */
	public function getTranslation(Language $language): ?Translation
	{
		foreach ($this->getTranslations() as $translation) {
			if ($translation->getLanguage()->getId() === $language->getId()) {
				return $translation;
			}
		}

		return null;
	}
}
