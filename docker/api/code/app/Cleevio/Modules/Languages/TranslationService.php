<?php

declare(strict_types=1);

namespace Cleevio\Languages;

use Cleevio\Languages\Entities\Language;
use Cleevio\Languages\Entities\Translation;
use Cleevio\Languages\Repositories\ITranslationKeyRepository;
use Cleevio\Languages\Repositories\ITranslationRepository;
use Cleevio\Languages\Repositories\TranslationConstraints;
use Cleevio\Languages\Repositories\TranslationKeyConstraints;
use Cleevio\Translations\TranslationKeyNotFound;
use DateTime;
use Nette\Http\IRequest;
use Nette\SmartObject;

class TranslationService implements ITranslationService
{

	use SmartObject;

	/**
	 * @var ITranslationRepository
	 */
	private $translationRepository;

	/**
	 * @var ITranslationKeyRepository
	 */
	private $translationKeyRepository;


	public function __construct(
		ITranslationRepository $translationRepository,
		ITranslationKeyRepository $translationKeyRepository
	)
	{
		$this->translationRepository = $translationRepository;
		$this->translationKeyRepository = $translationKeyRepository;
	}


	/**
	 * @param string $key
	 * @param Language|null $language
	 * @param array|null $params
	 * @return string
	 * Todo: Introduce cache
	 */
	public function translate(string $key, ?Language $language, ?array $params = []): string
	{
		$constraints = new TranslationConstraints;
		$constraints->setKey($key);

		if ($language !== null) {
			$constraints->setCode($language->getCode());
		}

		$translation = $this->translationRepository->findBy($constraints->build());

		if (!$translation instanceof Translation) {
			return $key;
		}

		$translation = $translation->getValue();

		if ($params !== null) {
			foreach ($params as $k => $value) {
				$translation = str_replace('%' . $k . '%', $value, $translation);
			}
		}

		return $translation;
	}

	/**
	 * @param TranslationKeyConstraints $constraints
	 * @return array
	 */
	public function getTranslationKeys(TranslationKeyConstraints $constraints): array
	{
		return $this->translationKeyRepository->findAll($constraints->build());
	}


	/**
	 * @param TranslationConstraints $constraints
	 * @return array
	 */
	public function getTranslations(TranslationConstraints $constraints): array
	{
		return $this->translationRepository->findAll($constraints->build());
	}


	/**
	 * @param Language $language
	 * @param array $translations
	 * @param bool $update
	 * @throws TranslationKeyNotFound
	 */
	public function updateTranslations(Language $language, array $translations, ?bool $update = false): void
	{
		$keys = array_keys($translations);
		$translationKeys = $this->translationKeyRepository->findByKeys($keys);

		if (count($keys) !== count($translationKeys)) {
			throw new TranslationKeyNotFound;
		}

		foreach ($translationKeys as $translationKey) {
			$translation = $translationKey->getTranslation($language);

			if ($translation === null) {
				$translation = new Translation($translationKey, $language, $translations[$translationKey->getKey()]);
			} elseif ($update === true) {
				$translation->setValue($translations[$translationKey->getKey()]);
			}

			$this->translationKeyRepository->persist($translation);
		}
	}


	/**
	 * @param TranslationConstraints $constraints
	 * @param string $code
	 * @param IRequest $request
	 * @return TranslationConstraints
	 */
	public function queryToConstraints(TranslationConstraints $constraints, string $code, IRequest $request): TranslationConstraints
	{
		$constraints->setCode($code);

		if ($request->getQuery('translationKey') !== null) {
			$constraints->setKey($request->getQuery('translationKey'));
		}

		if ($request->getQuery('translation') !== null) {
			$constraints->setValue($request->getQuery('translation'));
		}

		return $constraints;
	}

	/**
	 * Get translations last modification date
	 * @return DateTime|null
	 */
	public function getModified(): ?DateTime
	{
		$constraints = new TranslationConstraints;
		$constraints->setOrderBy('updatedAt');
		$constraints->setOrderDir('desc');

		$latest = $this->translationRepository->findBy($constraints->build());

		return !$latest instanceof Translation
			? null
			: $latest->getUpdatedAt();
	}
}
