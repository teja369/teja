<?php

declare(strict_types=1);

namespace Cleevio\Languages\Repositories;

use Cleevio\Repository\ConstraintsBuilder;
use Cleevio\Repository\Expressions\Comparison;

class TranslationConstraints extends ConstraintsBuilder
{

	function setCode(string $code): TranslationConstraints
	{
		$this->addCriteria(new Comparison('language.code', '=', $code));

		return $this;
	}


	function setKey(string $key): TranslationConstraints
	{
		$this->addCriteria(new Comparison('key.key', '=', $key));

		return $this;
	}


	function setState(?bool $translated): TranslationConstraints
	{
		if ($translated !== null) {
			$this->addCriteria(new Comparison('value', $translated ? '<>' : '=', ''));
		}

		return $this;
	}


	function setValue(string $value): TranslationConstraints
	{
		$this->addCriteria(new Comparison('value', '=', $value));

		return $this;
	}
}
