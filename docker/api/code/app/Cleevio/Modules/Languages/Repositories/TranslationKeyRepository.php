<?php

declare(strict_types=1);

namespace Cleevio\Languages\Repositories;

use Cleevio\Languages\Entities\TranslationKey;
use Cleevio\Repository\Repositories\SqlRepository;
use Doctrine\Common\Collections\Criteria;

class TranslationKeyRepository extends SqlRepository implements ITranslationKeyRepository
{

	public function type(): string
	{
		return TranslationKey::class;
	}

	/**
	 * @param array $keys
	 * @return TranslationKey[]
	 */
	public function findByKeys(array $keys): array
	{
		$result = $this->getRepository()
			->createQueryBuilder("t")
			->addCriteria(Criteria::create()->where(Criteria::expr()->in("key", $keys)))
			->getQuery()
			->execute();

		return !is_null($result) ? $result : [];
	}
}
