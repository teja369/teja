<?php

declare(strict_types=1);

namespace Cleevio\Languages\Repositories;

use Cleevio\Languages\Entities\Language;
use Cleevio\Repository\Repositories\SqlRepository;

class LanguageRepository extends SqlRepository implements ILanguageRepository
{

	public function type(): string
	{
		return Language::class;
	}

}
