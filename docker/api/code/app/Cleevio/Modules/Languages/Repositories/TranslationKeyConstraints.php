<?php

declare(strict_types=1);

namespace Cleevio\Languages\Repositories;

use Cleevio\Repository\ConstraintsBuilder;
use Cleevio\Repository\Expressions\Comparison;
use Cleevio\Repository\Expressions\CompositeExpression;

class TranslationKeyConstraints extends ConstraintsBuilder
{
	public function setSearch(string $search): TranslationKeyConstraints
	{
		$this->addCriteria(new CompositeExpression(CompositeExpression::TYPE_OR,[
			new Comparison('key', 'CONTAINS', $search),
			new Comparison('translations.value', 'CONTAINS', $search),
		]));

		return $this;
	}
}
