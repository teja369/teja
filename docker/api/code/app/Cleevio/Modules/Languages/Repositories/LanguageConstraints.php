<?php

declare(strict_types=1);

namespace Cleevio\Languages\Repositories;

use Cleevio\Repository\ConstraintsBuilder;
use Cleevio\Repository\Expressions\Comparison;

class LanguageConstraints extends ConstraintsBuilder
{

	function setCode(string $code): LanguageConstraints
	{
		$this->addCriteria(new Comparison('code', '=', $code));

		return $this;
	}

	function setDefault(): LanguageConstraints
	{
		$this->addCriteria(new Comparison('isDefault', '=', true));

		return $this;
	}

}
