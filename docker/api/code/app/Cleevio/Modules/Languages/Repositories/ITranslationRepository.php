<?php

declare(strict_types=1);

namespace Cleevio\Languages\Repositories;

use Cleevio\Repository\IRepository;

interface ITranslationRepository extends IRepository
{
}
