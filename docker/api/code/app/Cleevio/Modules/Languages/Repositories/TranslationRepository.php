<?php

declare(strict_types=1);

namespace Cleevio\Languages\Repositories;

use Cleevio\Languages\Entities\Translation;
use Cleevio\Repository\Repositories\SqlRepository;

class TranslationRepository extends SqlRepository implements ITranslationRepository
{

	public function type(): string
	{
		return Translation::class;
	}
}
