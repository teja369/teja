<?php

declare(strict_types=1);

namespace Cleevio\Languages\Repositories;

use Cleevio\Languages\Entities\TranslationKey;
use Cleevio\Repository\IRepository;

interface ITranslationKeyRepository extends IRepository
{
	/**
	 * @param array $keys
	 * @return TranslationKey[]
	 */
	public function findByKeys(array $keys): array;

}
