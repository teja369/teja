<?php

declare(strict_types=1);

namespace Cleevio\Translations;

use Exception;

class TranslationKeyNotFound extends Exception
{

}
