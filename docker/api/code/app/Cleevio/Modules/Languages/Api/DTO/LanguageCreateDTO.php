<?php

declare(strict_types=1);

namespace Cleevio\Languages\Api\DTO;

use Cleevio\RestApi\Command\ICommandDTO;

class LanguageCreateDTO implements ICommandDTO
{
	/**
	 * @var string
	 */
	protected $code;

	/**
	 * @var string
	 */
	protected $name;

	/**
	 * @var string
	 */
	protected $country;


	public function __construct(
		string $code,
		string $name,
		string $country
	)
	{
		$this->code = $code;
		$this->name = $name;
		$this->country = $country;
	}


	public static function fromRequest($data): ICommandDTO
	{
		return new static(
			$data->code,
			$data->name,
			$data->country
		);
	}

	public function getCode(): string
	{
		return $this->code;
	}

	public function getName(): string
	{
		return $this->name;
	}

	public function getCountry(): string
	{
		return $this->country;
	}
}
