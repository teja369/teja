<?php

declare(strict_types=1);

namespace Cleevio\Languages\Api\Response;

use Cleevio\Api\Translatable\SimpleResponse;
use Cleevio\Languages\Entities\Language;

final class LanguagesResponseBuilder extends SimpleResponse
{
	/**
	 * @var array
	 */
	private $data;

	/**
	 * LanguagesResponseBuilder constructor.
	 * @param array $data
	 * @param string|null $lang
	 */
	public function __construct(array $data, ?string $lang)
	{
		parent::__construct($lang);

		$this->data = $data;
	}

	/**
	 * @return array
	 */
	protected function data(): array
	{
		return array_map(function (Language $language) {
			return (new LanguageResponseBuilder($language, $this->getLang()))->build();
		}, $this->data);
	}

}
