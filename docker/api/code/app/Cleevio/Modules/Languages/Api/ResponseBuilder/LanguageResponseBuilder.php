<?php

declare(strict_types=1);

namespace Cleevio\Languages\Api\Response;

use Cleevio\Api\Translatable\SimpleResponse;
use Cleevio\Countries\Api\Response\CountryResponseBuilder;
use Cleevio\Languages\Entities\Language;

final class LanguageResponseBuilder extends SimpleResponse
{
	/**
	 * @var Language
	 */
	private $data;

	/**
	 * LanguageResponseBuilder constructor.
	 * @param Language $language
	 * @param string|null $lang
	 */
	public function __construct(Language $language, ?string $lang)
	{
		parent::__construct($lang);

		$this->data = $language;
	}


	/**
	 * @return array
	 */
	protected function data(): array
	{
		return [
			'id' => $this->data->getId(),
			'name' => $this->data->getName(),
			'code' => $this->data->getCode(),
			'country' => (new CountryResponseBuilder($this->data->getCountry(), $this->getLang()))->build(),
			'isDefault' => $this->data->isDefault(),
		];
	}
}
