<?php

declare(strict_types=1);

namespace Cleevio\Languages\Api\DTO;

use Cleevio\RestApi\Command\ICommandDTO;

class TranslationsPutDTO implements ICommandDTO
{
	/**
	 * @var array
	 */
	protected $translations;


	public function __construct(
		array $translations
	)
	{
		$this->translations = $translations;
	}


	public static function fromRequest($data): ICommandDTO
	{
		$translations = [];

		foreach ($data as $translation) {
			$translations[$translation->key] = $translation->value;
		}

		return new static($translations);
	}


	public function getTranslations(): array
	{
		return $this->translations;
	}

}
