<?php

declare(strict_types=1);

namespace Cleevio\Languages\Api\DTO;

use Cleevio\RestApi\Command\ICommandDTO;

class LanguageUpdateDTO implements ICommandDTO
{

	/**
	 * @var string
	 */
	protected $name;

	/**
	 * @var string
	 */
	protected $country;


	public function __construct(
		string $name,
		string $country
	)
	{
		$this->name = $name;
		$this->country = $country;
	}


	public static function fromRequest($data): ICommandDTO
	{
		return new static(
			$data->name,
			$data->country
		);
	}

	public function getName(): string
	{
		return $this->name;
	}

	public function getCountry(): string
	{
		return $this->country;
	}
}
