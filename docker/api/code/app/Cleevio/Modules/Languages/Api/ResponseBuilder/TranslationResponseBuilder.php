<?php

declare(strict_types=1);

namespace Cleevio\Languages\Api\Response;

use Cleevio\Languages\Entities\Translation;
use Cleevio\RestApi\Response\SimpleResponse;

final class TranslationResponseBuilder extends SimpleResponse
{

	/**
	 * @var Translation
	 */
	private $data;


	/**
	 * LanguageResponseBuilder constructor.
	 * @param Translation $translation
	 */
	public function __construct(Translation $translation)
	{
		$this->data = $translation;
	}


	/**
	 * @return array
	 */
	protected function data(): array
	{
		return [
			'id' => $this->data->getId(),
			'key' => $this->data->getKey()->getKey(),
			'language' => $this->data->getLanguage()->getCode(),
			'value' => $this->data->getValue(),
		];
	}
}
