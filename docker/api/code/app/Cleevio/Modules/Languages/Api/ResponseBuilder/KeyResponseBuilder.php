<?php

declare(strict_types=1);

namespace Cleevio\Languages\Api\Response;

use Cleevio\Languages\Entities\TranslationKey;
use Cleevio\RestApi\Response\SimpleResponse;

final class KeyResponseBuilder extends SimpleResponse
{

	/**
	 * @var TranslationKey
	 */
	private $data;


	/**
	 * KeyResponseBuilder constructor.
	 * @param TranslationKey $translation
	 */
	public function __construct(TranslationKey $translation)
	{
		$this->data = $translation;
	}


	/**
	 * @return array
	 */
	protected function data(): array
	{
		return [
			'id' => $this->data->getId(),
			'key' => $this->data->getKey(),
		];
	}
}
