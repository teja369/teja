<?php

declare(strict_types=1);

namespace Cleevio\Languages\Api\Response;

use Cleevio\Languages\Entities\Translation;
use Cleevio\RestApi\Response\SimpleResponse;

final class TranslationsResponseBuilder extends SimpleResponse
{
	/**
	 * @var array
	 */
	private $data;

	/**
	 * TranslationsResponseBuilder constructor.
	 * @param array $data
	 */
	public function __construct(array $data)
	{
		$this->data = $data;
	}

	/**
	 * @return array
	 */
	protected function data(): array
	{
		return array_map(static function (Translation $translation) {
			return (new TranslationResponseBuilder($translation))->build();
		}, $this->data);
	}

}
