<?php

declare(strict_types=1);

namespace Cleevio\Languages\Api\Response;

use Cleevio\Languages\Entities\TranslationKey;
use Cleevio\RestApi\Response\SimpleResponse;

final class KeysResponseBuilder extends SimpleResponse
{
	/**
	 * @var TranslationKey[]
	 */
	private $data;

	/**
	 * KeysResponseBuilder constructor.
	 * @param array $data
	 */
	public function __construct(array $data)
	{
		$this->data = $data;
	}

	/**
	 * @return array
	 */
	protected function data(): array
	{
		return array_map(static function (TranslationKey $key) {
			return (new KeyResponseBuilder($key))->build();
		}, $this->data);
	}

}
