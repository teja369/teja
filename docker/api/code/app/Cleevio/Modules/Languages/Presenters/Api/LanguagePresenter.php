<?php

declare(strict_types=1);

namespace Cleevio\Languages\ApiModule\Presenters;

use Cleevio\Api\Helpers\IRequestContext;
use Cleevio\Countries\ICountryService;
use Cleevio\Languages\Api\DTO\LanguageCreateDTO;
use Cleevio\Languages\Api\DTO\LanguageUpdateDTO;
use Cleevio\Languages\Api\Response\LanguageResponseBuilder;
use Cleevio\Languages\Api\Response\LanguagesResponseBuilder;
use Cleevio\Languages\ILanguageService;
use Cleevio\RestApi\Annotations\ApiRoute;
use Cleevio\RestApi\Annotations\Authenticated;
use Cleevio\RestApi\Annotations\DTO;
use Cleevio\RestApi\Annotations\JsonSchema;
use Cleevio\RestApi\Annotations\Response;
use Cleevio\RestApi\Exceptions\AuthenticationException;
use Cleevio\RestApi\Exceptions\BadRequestException;
use Cleevio\RestApi\Exceptions\ConflictException;
use Cleevio\RestApi\Exceptions\NotFoundException;
use Cleevio\RestApi\Presenters\RestPresenter;
use Cleevio\RestApi\Response\EmptyResponse;
use Cleevio\Users\Entities\User;
use InvalidArgumentException;
use Nette\Http\IResponse;

/**
 * @ApiRoute("/api/v1/languages",presenter="Languages:Api:Language")
 */
class LanguagePresenter extends RestPresenter
{

	/**
	 * @var ILanguageService
	 */
	private $languageService;

	/**
	 * @var IRequestContext
	 */
	private $requestContext;

	/**
	 * @var ICountryService
	 */
	private $countryService;

	/**
	 * @param ILanguageService $languageService
	 * @param ICountryService $countryService
	 * @param IRequestContext $requestContext
	 */
	public function __construct(ILanguageService $languageService, ICountryService $countryService, IRequestContext $requestContext)
	{
		parent::__construct();

		$this->languageService = $languageService;
		$this->requestContext = $requestContext;
		$this->countryService = $countryService;
	}

	/**
	 * @ApiRoute("/api/v1/languages/<code>", method="GET", description="Fetch language")
	 * @Response(200, file="response/languages/language.json")
	 * @Response(401, code="10001", description="Language with provided code was not found")
	 * @param string $code
	 */
	public function actionLanguagesGet(string $code): void
	{
		$language = $this->languageService->getLanguage($code);

		if ($language === null) {
			throw new NotFoundException('languages.language.get.not-found', 10001);
		}

		$this->sendSuccessResponse((new LanguageResponseBuilder($language, $this->requestContext->getLang())), IResponse::S200_OK);
	}

	/**
	 * @ApiRoute("/api/v1/languages/default", method="GET", description="Get default language")
	 * @Response(200, file="response/languages/language.json")
	 * @Response(404, code="10001", description="Language was not found")
	 */
	public function actionLanguagesGetDefault(): void
	{
		$language = $this->languageService->getDefaultLanguage();

		if ($language === null) {
			throw new NotFoundException('languages.language.get.not-found', 10001);
		}

		$this->sendSuccessResponse((new LanguageResponseBuilder($language, $this->requestContext->getLang())), IResponse::S200_OK);
	}

	/**
	 * @ApiRoute("/api/v1/languages/<code>/default", method="PUT", description="Set language as default. Only one language can be default")
	 * @Response(200, file="response/languages/language.json")
	 * @Response(401, code="10001", description="Language with provided code was not found")
	 * @param string $code
	 * @Authenticated("admin.localizations")
	 */
	public function actionLanguagesPutDefault(string $code): void
	{
		$language = $this->languageService->getLanguage($code);

		if ($language === null) {
			throw new NotFoundException('languages.language.get.not-found', 10001);
		}

		$language = $this->languageService->setDefaultLanguage($language);

		$this->sendSuccessResponse((new LanguageResponseBuilder($language, $this->requestContext->getLang())), IResponse::S200_OK);
	}

	/**
	 * @ApiRoute("/api/v1/languages", method="GET", description="Fetches all available languages")
	 * @Response(200, file="response/languages/languages.json")
	 */
	public function actionLanguagesList(): void
	{
		$languages = $this->languageService->getLanguages();

		$this->sendSuccessResponse((new LanguagesResponseBuilder($languages, $this->requestContext->getLang())), IResponse::S200_OK);
	}

	/**
	 * @Authenticated("admin.localizations")
	 * @ApiRoute("/api/v1/languages", method="POST", description="Create a new language")
	 * @DTO("Cleevio\Languages\Api\DTO\LanguageCreateDTO")
	 * @JsonSchema("request/languages/createLanguage.json")
	 * @Response(409, code="10002", description="Language with provided code already exists")
	 * @Response(400, code="10003", description="Country with provided code was not found")
	 * @Response(200, file="response/languages/language.json")
	 */
	public function actionLanguagesCreate(): void
	{
		$authenticator = $this->getAuthenticator();

		$user = $authenticator->getUser();

		if (!$user instanceof User) {
			throw new AuthenticationException("users.auth.error.user-not-found", 10001);
		}

		$command = $this->getCommandDTO();

		if (!$command instanceof LanguageCreateDTO) {
			throw new InvalidArgumentException;
		}

		if ($this->languageService->getLanguage($command->getCode()) !== null) {
			throw new ConflictException("languages.language.create.conflict", 10002);
		}

		$country = $this->countryService->findByCode($command->getCountry());

		if ($country === null) {
			throw new NotFoundException('countries.country.get.error.country-not-found', 10003);
		}

		$language = $this->languageService->createLanguage($command->getCode(), $command->getName(), $country);

		$this->sendSuccessResponse(new LanguageResponseBuilder($language, $this->requestContext->getLang()), IResponse::S200_OK);
	}

	/**
	 * @Authenticated("admin.localizations")
	 * @ApiRoute("/api/v1/languages/<code>", method="PUT", description="Update language")
	 * @DTO("Cleevio\Languages\Api\DTO\LanguageUpdateDTO")
	 * @JsonSchema("request/languages/updateLanguage.json")
	 * @Response(404, code="10002", description="Language with provided code does not exist")
	 * @Response(400, code="10003", description="Country with provided code was not found")
	 * @Response(200, file="response/languages/language.json")
	 * @param string $code
	 */
	public function actionLanguagesUpdate(string $code): void
	{
		$authenticator = $this->getAuthenticator();

		$user = $authenticator->getUser();

		if (!$user instanceof User) {
			throw new AuthenticationException("users.auth.error.user-not-found", 10001);
		}

		$command = $this->getCommandDTO();

		if (!$command instanceof LanguageUpdateDTO) {
			throw new InvalidArgumentException;
		}

		$language = $this->languageService->getLanguage($code);

		if ($language === null) {
			throw new NotFoundException('languages.language.get.not-found', 10001);
		}

		$country = $this->countryService->findByCode($command->getCountry());

		if ($country === null) {
			throw new NotFoundException('countries.country.get.error.country-not-found', 10003);
		}

		$language = $this->languageService->updateLanguage($language, $command->getName(), $country);

		$this->sendSuccessResponse(new LanguageResponseBuilder($language, $this->requestContext->getLang()), IResponse::S200_OK);
	}

	/**
	 * @Authenticated("admin.localizations")
	 * @ApiRoute("/api/v1/languages/<code>", method="DELETE", description="Delete language")
	 * @Response(200, file="response/languages/language.json")
	 * @Response(401, code="10001", description="Language with provided code was not found")
	 * @Response(400, code="10002", description="Cannot delete default language")
	 * @param string $code
	 */
	public function actionLanguagesDelete(string $code): void
	{
		$authenticator = $this->getAuthenticator();

		$user = $authenticator->getUser();

		if (!$user instanceof User) {
			throw new AuthenticationException("users.auth.error.user-not-found", 10001);
		}

		$language = $this->languageService->getLanguage($code);

		if ($language === null) {
			throw new NotFoundException('languages.language.get.not-found', 10001);
		}

		if ($language->isDefault()) {
			throw new BadRequestException('languages.language.delete.default-error', 10002);
		}

		$this->languageService->deleteLanguage($language);

		$this->sendSuccessResponse(new EmptyResponse, IResponse::S200_OK);
	}
}
