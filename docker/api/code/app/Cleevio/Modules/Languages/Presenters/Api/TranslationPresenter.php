<?php

declare(strict_types=1);

namespace Cleevio\Languages\ApiModule\Presenters;

use Cleevio\Languages\Api\DTO\TranslationsCreateDTO;
use Cleevio\Languages\Api\DTO\TranslationsPutDTO;
use Cleevio\Languages\Api\Response\KeysResponseBuilder;
use Cleevio\Languages\Api\Response\TranslationsResponseBuilder;
use Cleevio\Languages\ILanguageService;
use Cleevio\Languages\ITranslationService;
use Cleevio\Languages\Repositories\TranslationConstraints;
use Cleevio\Languages\Repositories\TranslationKeyConstraints;
use Cleevio\RestApi\Annotations\ApiRoute;
use Cleevio\RestApi\Annotations\Authenticated;
use Cleevio\RestApi\Annotations\DTO;
use Cleevio\RestApi\Annotations\JsonSchema;
use Cleevio\RestApi\Annotations\Response;
use Cleevio\RestApi\Exceptions\AuthenticationException;
use Cleevio\RestApi\Exceptions\BadRequestException;
use Cleevio\RestApi\Exceptions\NotFoundException;
use Cleevio\RestApi\Presenters\RestPresenter;
use Cleevio\RestApi\Response\EmptyResponse;
use Cleevio\Translations\TranslationKeyNotFound;
use Cleevio\Users\Entities\User;
use InvalidArgumentException;
use Nette\Http\IResponse;

/**
 * @ApiRoute("/api/v1/translations",presenter="Languages:Api:Translation")
 */
class TranslationPresenter extends RestPresenter
{

	/**
	 * @var ITranslationService
	 */
	private $translationService;

	/**
	 * @var ILanguageService
	 */
	private $languageService;


	public function __construct(ITranslationService $translationService, ILanguageService $languageService)
	{
		parent::__construct();

		$this->translationService = $translationService;
		$this->languageService = $languageService;
	}

	/**
	 * @Authenticated()
	 * @ApiRoute("/api/v1/translations/keys", method="GET", description="Fetches translations keys")
	 * @Response(200, file="response/languages/keys.json")
	 */
	public function actionTranslationKeysList(): void
	{
		$constraints = new TranslationKeyConstraints;

		if ($this->getHttpRequest()->getQuery('search') !== null) {
			$constraints->setSearch($this->getHttpRequest()->getQuery('search'));
		}

		$keys = $this->translationService->getTranslationKeys($constraints);

		$this->sendSuccessResponse(new KeysResponseBuilder($keys), IResponse::S200_OK);
	}

	/**
	 * @ApiRoute("/api/v1/translations/<lang>", method="GET", description="Fetches translations for given language")
	 * @Response(200, file="response/languages/translations.json")
	 * @param string $lang
	 * @throws \Exception
	 */
	public function actionTranslationsList(string $lang): void
	{
		$constraints = new TranslationConstraints;

		$constraints = $this->translationService->queryToConstraints($constraints, $lang, $this->getHttpRequest());

		$modified = $this->translationService->getModified();

		$this->handleModifiedSince($modified);

		$translations = $this->translationService->getTranslations($constraints);

		$this->sendSuccessResponse((new TranslationsResponseBuilder($translations))
			->withLastModified($modified), IResponse::S200_OK);
	}

	/**
	 * @Authenticated("admin.localizations")
	 * @ApiRoute("/api/v1/translations/<lang>", method="POST", description="Add new translations for a language")
	 * @DTO("Cleevio\Languages\Api\DTO\TranslationsCreateDTO")
	 * @JsonSchema("request/languages/createTranslations.json")
	 * @Response(200)
	 * @Response(400, code="10002", description="Language with provided code was not found")
	 * @Response(400, code="10003", description="Translation key was not found on the server")
	 * @param string $lang
	 */
	public function actionTranslationsCreate(string $lang): void
	{
		$authenticator = $this->getAuthenticator();

		$user = $authenticator->getUser();

		if (!$user instanceof User) {
			throw new AuthenticationException("users.auth.error.user-not-found", 10001);
		}

		$command = $this->getCommandDTO();

		if (!$command instanceof TranslationsCreateDTO) {
			throw new InvalidArgumentException;
		}

		$language = $this->languageService->getLanguage($lang);

		if ($language === null) {
			throw new NotFoundException('languages.language.get.not-found', 10002);
		}

		try {
			$this->translationService->updateTranslations($language, $command->getTranslations(), true);
		} catch (TranslationKeyNotFound $e) {
			throw new BadRequestException('languages.translation.get.not-found', 10003);
		}

		$this->sendSuccessResponse(new EmptyResponse, IResponse::S200_OK);
	}

	/**
	 * @Authenticated("admin.localizations")
	 * @ApiRoute("/api/v1/translations/<lang>", method="PUT", description="Edit translations for a language")
	 * @DTO("Cleevio\Languages\Api\DTO\TranslationsPutDTO")
	 * @JsonSchema("request/languages/putTranslations.json")
	 * @Response(200)
	 * @Response(400, code="10002", description="Language with provided code was not found")
	 * @Response(400, code="10003", description="Translation key was not found on the server")
	 * @param string $lang
	 */
	public function actionTranslationsPut(string $lang): void
	{
		$authenticator = $this->getAuthenticator();

		$user = $authenticator->getUser();

		if (!$user instanceof User) {
			throw new AuthenticationException("users.auth.error.user-not-found", 10001);
		}

		$command = $this->getCommandDTO();

		if (!$command instanceof TranslationsPutDTO) {
			throw new InvalidArgumentException;
		}

		$language = $this->languageService->getLanguage($lang);

		if ($language === null) {
			throw new NotFoundException('languages.language.get.not-found', 10002);
		}

		try {
			$this->translationService->updateTranslations($language, $command->getTranslations(), true);
		} catch (TranslationKeyNotFound $e) {
			throw new BadRequestException('languages.translation.get.not-found', 10003);
		}

		$this->sendSuccessResponse(new EmptyResponse, IResponse::S200_OK);
	}
}
