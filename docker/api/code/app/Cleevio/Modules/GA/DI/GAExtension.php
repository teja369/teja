<?php

declare(strict_types=1);

namespace Cleevio\GA\DI;

use InvalidArgumentException;
use Nette\DI\CompilerExtension;

class GAExtension extends CompilerExtension
{
	/**
	 * @var array
	 */
	private static $GA_VARIABLES = [
		'GA_AUTH_API',
		'GA_AUTH_USER',
		'GA_AUTH_PASSWORD',
		'GA_AUD_PORTAL',
	];

	public function loadConfiguration()
	{
		foreach (self::$GA_VARIABLES as $var) {
			if (env($var) === null) {
				throw new InvalidArgumentException('Environment variable GA_AUD_PORTAL is missing');
			}
		}

		$this->compiler->loadConfig(__DIR__ . '/services.neon');
	}

}
