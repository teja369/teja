<?php

declare(strict_types=1);

namespace Cleevio\GA;

use Cleevio\Api\GA\Requests\LongLivedTokenRequest;
use Cleevio\Api\GA\Requests\ShortLivedTokenRequest;
use Cleevio\Api\GA\Requests\UserInformationRequest;
use Cleevio\Api\GA\Responses\ShortLivedTokenResponse;
use Cleevio\Api\GA\Responses\UserInformationResponse;
use Cleevio\ApiClient\Exception\ApiException;

class GAService implements IGAService
{

	/**
	 * @var LongLivedTokenRequest
	 */
	private $longLivedTokenRequest;

	/**
	 * @var ShortLivedTokenRequest
	 */
	private $shortLivedTokenRequest;

	/**
	 * @var UserInformationRequest
	 */
	private $userInformationRequest;


	public function __construct(
		LongLivedTokenRequest $longLivedTokenRequest,
		ShortLivedTokenRequest $shortLivedTokenRequest,
		UserInformationRequest $userInformationRequest
	)
	{
		$this->longLivedTokenRequest = $longLivedTokenRequest;
		$this->shortLivedTokenRequest = $shortLivedTokenRequest;
		$this->userInformationRequest = $userInformationRequest;
	}


	public function getShortLivedToken(): ShortLivedTokenResponse
	{
		try {
			$this->longLivedTokenRequest->setData([
				'USERID' => env('GA_AUTH_USER'),
				'password' => env('GA_AUTH_PASSWORD'),
			]);

			$response = $this->longLivedTokenRequest->execute();

			$this->shortLivedTokenRequest->setHeader('Authorization', 'Bearer ' . $response->getToken());

			return $this->shortLivedTokenRequest->execute();
		} catch (ApiException $e) {
			throw new ShortLivedTokenException;
		}
	}


	public function getUserInformation(string $username): UserInformationResponse
	{
		try {
			$shortLivedToken = $this->getShortLivedToken();

			$this->userInformationRequest->setHeader('Authorization', 'Bearer ' . $shortLivedToken->getToken());
			$this->userInformationRequest->setUserId($username);

			return $this->userInformationRequest->execute();
		} catch (ApiException|ShortLivedTokenException $e) {
			throw new UserInformationException('Could not obtain user information');
		}
	}

}
