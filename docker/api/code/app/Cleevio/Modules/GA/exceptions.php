<?php

declare(strict_types=1);

namespace Cleevio\GA;

use Cleevio\Exceptions\ParamException;

class LongLivedTokenException extends ParamException
{

}
class ShortLivedTokenException extends ParamException
{

}
class UserInformationException extends ParamException
{

}
