<?php

declare(strict_types=1);

namespace Cleevio\Api\GA\Requests;

use Cleevio\Api\GA\Responses\UserInformationResponse;
use Cleevio\ApiClient\BaseRequest;
use Cleevio\ApiClient\Exception\ApiException;
use InvalidArgumentException;

class UserInformationRequest extends BaseRequest implements IUserInformationRequest
{

	/**
	 * @var string|null
	 */
	private $userId;


	/**
	 * Endpoint path
	 * @return string
	 */
	protected function path(): string
	{
		if (env('GA_LOOKUP_API') === null) {
			throw new InvalidArgumentException;
		}

		$this->setEndpoint(env('GA_LOOKUP_API'));

		$path = '/portaladmin/api/UserService/UserInformation';

		$query = http_build_query([
			'userID' => $this->userId,
		]);

		return strlen($query) > 0
			? $path . '?' . $query
			: $path;
	}


	/**
	 * Method
	 * @return string
	 */
	protected function method(): string
	{
		return "GET";
	}


	/**
	 * Determines whether request requires authorization
	 * @return bool
	 */
	function requiresAuth(): bool
	{
		return false;
	}


	/**
	 * @return bool
	 */
	protected function dispatchEvents()
	{
		return false;
	}


	/**
	 * @param string|null $id
	 */
	public function setUserId(?string $id): void
	{
		$this->userId = $id;
	}


	/**
	 * @return UserInformationResponse
	 * @throws ApiException
	 */
	function execute(): UserInformationResponse
	{
		return new UserInformationResponse($this->request());
	}
}
