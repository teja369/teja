<?php

declare(strict_types=1);

namespace Cleevio\Api\GA\Responses;

use Cleevio\ApiClient\IResponse;
use InvalidArgumentException;

class RolesResponse implements IResponse
{

	/**
	 * @var RoleResponse[]
	 */
	private $roles;


	/**
	 * RolesResponse constructor.
	 * @param array $roles
	 */
	public function __construct(array $roles)
	{
		if(array_key_exists('ROLE', $roles)) {
			$this->roles = array_map(static function ($role) {
				return new RoleResponse($role);
			}, $roles['ROLE']);
		} else {
			throw new InvalidArgumentException;
		}
	}


	/**
	 * @return RoleResponse[]
	 */
	public function getRoles(): array
	{
		return $this->roles;
	}

}
