<?php

declare(strict_types=1);

namespace Cleevio\Api\GA\Responses;

use Cleevio\ApiClient\IResponse;
use InvalidArgumentException;

class RoleResponse implements IResponse
{

	/**
	 * @var int
	 */
	private $id;

	/**
	 * @var string
	 */
	private $name;


	/**
	 * RoleResponse constructor.
	 * @param array $role
	 */
	public function __construct(array $role)
	{
		if (array_key_exists('ID', $role) && array_key_exists('NAME', $role)) {
			$this->id = (int) $role['ID'];
			$this->name = $role['NAME'];
		} else {
			throw new InvalidArgumentException;
		}
	}


	/**
	 * @return int
	 */
	public function getId(): int
	{
		return $this->id;
	}


	/**
	 * @return string
	 */
	public function getName(): string
	{
		return $this->name;
	}

}
