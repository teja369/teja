<?php

declare(strict_types=1);

namespace Cleevio\Api\GA\Responses;

use Cleevio\ApiClient\IResponse;
use InvalidArgumentException;

class LongLivedTokenResponse implements IResponse
{

	/**
	 * @var bool
	 */
	private $isValidUser;

	/**
	 * @var string
	 */
	private $message;

	/**
	 * @var string
	 */
	private $token;


	/**
	 * LongLivedTokenResponse constructor.
	 * @param array $response
	 */
	public function __construct(array $response)
	{
		if (array_key_exists('Success', $response) && array_key_exists('JWT', $response)) {
			$this->isValidUser = $response['ValidUser'];
			$this->message = $response['Message'];
			$this->token = $response['JWT'];
		} else {
			throw new InvalidArgumentException;
		}
	}


	/**
	 * @return bool
	 */
	public function isValidUser(): bool
	{
		return $this->isValidUser;
	}


	/**
	 * @return string
	 */
	public function getMessage(): string
	{
		return $this->message;
	}


	/**
	 * @return string
	 */
	public function getToken(): string
	{
		return $this->token;
	}

}
