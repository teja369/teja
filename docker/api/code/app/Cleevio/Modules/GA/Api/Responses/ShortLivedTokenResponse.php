<?php

declare(strict_types=1);

namespace Cleevio\Api\GA\Responses;

use Cleevio\ApiClient\IResponse;
use InvalidArgumentException;

class ShortLivedTokenResponse implements IResponse
{

	/**
	 * @var string
	 */
	private $token;


	/**
	 * ShortLivedTokenResponse constructor.
	 * @param array $response
	 */
	public function __construct(array $response)
	{
		if (array_key_exists('JWT', $response)) {
			$this->token = $response['JWT'];
		} else {
			throw new InvalidArgumentException;
		}
	}


	/**
	 * @return string
	 */
	public function getToken(): string
	{
		return $this->token;
	}

}
