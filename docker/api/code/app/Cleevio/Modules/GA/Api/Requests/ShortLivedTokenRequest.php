<?php

declare(strict_types=1);

namespace Cleevio\Api\GA\Requests;

use Cleevio\Api\GA\Responses\ShortLivedTokenResponse;
use Cleevio\ApiClient\BaseRequest;
use Cleevio\ApiClient\Exception\ApiException;
use InvalidArgumentException;

class ShortLivedTokenRequest extends BaseRequest implements IShortLivedTokenRequest
{

	/**
	 * Endpoint path
	 * @return string
	 */
	protected function path(): string
	{
		if (env('GA_AUD_PORTAL') === null) {
			throw new InvalidArgumentException;
		}

		return '/IASTCPortal/sessionapi/Login/ValidateShortLivedUser?appName='. env('GA_AUD_PORTAL');
	}


	/**
	 * Method
	 * @return string
	 */
	protected function method(): string
	{
		return "POST";
	}


	/**
	 * Determines whether request requires authorization
	 * @return bool
	 */
	function requiresAuth(): bool
	{
		return false;
	}


	/**
	 * @return bool
	 */
	protected function dispatchEvents()
	{
		return false;
	}


	/**
	 * @return ShortLivedTokenResponse
	 * @throws ApiException
	 */
	function execute(): ShortLivedTokenResponse
	{
		return new ShortLivedTokenResponse($this->request());
	}
}
