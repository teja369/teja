<?php

declare(strict_types=1);

namespace Cleevio\Api\GA\Responses;

use Cleevio\ApiClient\IResponse;
use InvalidArgumentException;

class UserInformationResponse implements IResponse
{

	/**
	 * @var string
	 */
	private $firstName;

	/**
	 * @var string
	 */
	private $lastName;

	/**
	 * @var RolesResponse
	 */
	private $roles;


	/**
	 * UserInformationResponse constructor.
	 * @param array $user
	 */
	public function __construct(array $user)
	{
		if (array_key_exists('USER', $user) &&
			array_key_exists('FIRSTNAME', $user['USER']) &&
			array_key_exists('LASTNAME', $user['USER']) &&
			array_key_exists('ROLES', $user['USER'])) {
			$this->firstName = $user['USER']['FIRSTNAME'];
			$this->lastName = $user['USER']['LASTNAME'];
			$this->roles = new RolesResponse($user['USER']['ROLES']);
		} else {
			throw new InvalidArgumentException;
		}
	}


	/**
	 * @return string
	 */
	public function getFirstName(): string
	{
		return $this->firstName;
	}


	/**
	 * @return string
	 */
	public function getLastName(): string
	{
		return $this->lastName;
	}


	/**
	 * @return RolesResponse
	 */
	public function getRoles(): RolesResponse
	{
		return $this->roles;
	}

}
