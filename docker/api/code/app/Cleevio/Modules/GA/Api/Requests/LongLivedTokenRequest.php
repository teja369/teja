<?php

declare(strict_types=1);

namespace Cleevio\Api\GA\Requests;

use Cleevio\Api\GA\Responses\LongLivedTokenResponse;
use Cleevio\ApiClient\BaseRequest;
use Cleevio\ApiClient\Exception\ApiException;

class LongLivedTokenRequest extends BaseRequest implements ILongLivedTokenRequest
{

	/**
	 * Endpoint path
	 * @return string
	 */
	protected function path(): string
	{
		return '/IASTCPortal/sessionapi/Login/ValidateMobileUser';
	}


	/**
	 * Method
	 * @return string
	 */
	protected function method(): string
	{
		return "POST";
	}


	/**
	 * Determines whether request requires authorization
	 * @return bool
	 */
	function requiresAuth(): bool
	{
		return false;
	}


	/**
	 * @return bool
	 */
	protected function dispatchEvents()
	{
		return false;
	}


	/**
	 * @return LongLivedTokenResponse
	 * @throws ApiException
	 */
	function execute(): LongLivedTokenResponse
	{
		return new LongLivedTokenResponse($this->request());
	}
}
