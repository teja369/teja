<?php

declare(strict_types=1);

namespace Cleevio\Api\GA\Requests;

interface IUserInformationRequest
{

	/**
	 * @param string|null $id
	 */
	public function setUserId(?string $id): void;
}
