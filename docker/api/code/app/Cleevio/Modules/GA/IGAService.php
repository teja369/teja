<?php

declare(strict_types=1);

namespace Cleevio\GA;

use Cleevio\Api\GA\Responses\ShortLivedTokenResponse;
use Cleevio\Api\GA\Responses\UserInformationResponse;

interface IGAService
{

	/**
	 * @return ShortLivedTokenResponse
	 */
	public function getShortLivedToken(): ShortLivedTokenResponse;


	/**
	 * @param string $username
	 * @return UserInformationResponse
	 */
	public function getUserInformation(string $username): UserInformationResponse;
}
