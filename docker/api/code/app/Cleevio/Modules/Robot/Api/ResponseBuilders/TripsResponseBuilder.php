<?php

declare(strict_types=1);

namespace Cleevio\Robot\Api\Response;

use Cleevio\Api\Translatable\SimpleResponse;
use Cleevio\Fields\Entities\Field;
use Cleevio\Questions\Entities\Question;
use Cleevio\Registrations\Entities\Registration;
use Cleevio\RestApi\Response\Types\ContentTypeFactory;
use Cleevio\Trips\Entities\Trip;

final class TripsResponseBuilder extends SimpleResponse
{

	/**
	 * @var Trip[]
	 */
	private $data;
	/**
	 * @var array
	 */
	private $fields;

	/**
	 * @var bool
	 */
	private $showHeader;

	/**
	 * @var array|Registration[]
	 */
	private $registrations;
	/**
	 * @var Question[]
	 */
	private $questions;

	/**
	 * TripResponseBuilder constructor.
	 * @param Trip[] $trips
	 * @param Field[] $fields
	 * @param Registration[] $registrations
	 * @param Question[] $questions
	 * @param string|null $lang
	 * @param bool $showHeader
	 */
	public function __construct(array $trips, array $fields, array $registrations, array $questions, ?string $lang, bool $showHeader)
	{
		parent::__construct($lang);

		$this->data = $trips;
		$this->fields = $fields;
		$this->showHeader = $showHeader;
		$this->registrations = $registrations;
		$this->questions = $questions;
	}

	protected function getContentType(): string
	{
		return ContentTypeFactory::CONTENT_TYPE_CSV;
	}

	/**
	 * @return array
	 */
	protected function header(): array
	{

		$output = [
			'id',
			'userId',
			'start',
			'end',
			'country',
			'submittedAt',
			'hostName',
			'hostType',
		];

		// Handle fields
		foreach ($this->fields as $field) {
			$output[] = 'fields_' . $field->getName(null);
		}

		// Handle registrations
		foreach ($this->registrations as $registration) {
			$output[] = strtolower($registration->getName()) . '_registration_required';
		}

		// Handle questions
		foreach ($this->questions as $question) {
			$output[] = 'questions_' . $question->getText(null);
		}

		return $output;
	}

	/**
	 * @return array
	 */
	protected function data(): array
	{
		$data = array_map(function (Trip $trip) {
			return (new TripResponseBuilder($trip, $this->fields, $this->registrations, $this->questions, null))->build();
		}, $this->data);

		return $this->showHeader
			? array_merge([$this->header()], $data)
			: $data;
	}
}
