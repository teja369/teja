<?php

declare(strict_types=1);

namespace Cleevio\Robot\Api\Response;

use Cleevio\Answers\Entities\Answers\AnswerOption;
use Cleevio\Answers\Entities\Answers\BoolAnswer;
use Cleevio\Answers\Entities\Answers\ChoiceAnswer;
use Cleevio\Answers\Entities\Answers\CountryAnswer;
use Cleevio\Answers\Entities\Answers\DateTimeAnswer;
use Cleevio\Answers\Entities\Answers\NumberAnswer;
use Cleevio\Answers\Entities\Answers\TextAnswer;
use Cleevio\Api\Translatable\SimpleResponse;
use Cleevio\Questions\Entities\Question;
use Cleevio\Questions\Entities\Questions\CountryQuestion;
use Cleevio\Questions\Entities\Questions\DateTimeQuestion;
use Cleevio\Registrations\Entities\Registration;
use Cleevio\Trips\Entities\Trip;

final class TripResponseBuilder extends SimpleResponse
{

	/**
	 * @var Trip
	 */
	private $data;

	/**
	 * @var Registration[]
	 */
	private $registrations;

	/**
	 * @var array
	 */
	private $fields;

	/**
	 * @var Question[]
	 */
	private $questions;

	/**
	 * TripResponseBuilder constructor.
	 * @param Trip $trip
	 * @param array $fields
	 * @param Registration[] $registrations
	 * @param Question[] $questions
	 * @param string|null $lang
	 */
	public function __construct(Trip $trip, array $fields, array $registrations, array $questions, ?string $lang)
	{
		parent::__construct($lang);

		$this->data = $trip;
		$this->registrations = $registrations;
		$this->fields = $fields;
		$this->questions = $questions;
	}


	/**
	 * @return array
	 */
	protected function data(): array
	{

		$output = [
			'id' => $this->data->getId(),
			'userId' => $this->data->getUser()->getId(),
			'start' => $this->data->getStart()->format("Y-m-d"),
			'end' => $this->data->getEnd()->format("Y-m-d"),
			'country' => $this->data->getCountry()->getCode(),
			'submittedAt' => $this->data->getSubmittedAt() !== null ? $this->data->getSubmittedAt()->format("Y-m-d H:i") : null,
			'hostName' => $this->data->getHost() !== null ? $this->data->getHost()->getName() : null,
			'hostType' => $this->data->getHost() !== null ? $this->data->getHost()->getType() : null,
		];

		// Handle fields
		foreach ($this->fields as $field) {
			$param = $this->data->getHost() !== null
				? $this->data->getHost()->getParam($field)
				: null;
			$output['fields_' . $field->getName(null)] = $param !== null
				? $param->getValue()
				: null;
		}

		// Handle registrations
		foreach ($this->registrations as $registration) {
			$output[strtolower($registration->getName()) . '_registration_required'] = (int) $this->data->requiresRegistration($registration);
		}

		// Handle questions
		foreach ($this->questions as $question) {
			$answer = $this->data->getAnswer($question);

			if ($answer === null) {
				$output['questions_' . $question->getText(null)] = $answer;
			} elseif ($answer instanceof TextAnswer) {
				$output['questions_' . $question->getText(null)] = $answer->getText();
			} elseif ($answer instanceof NumberAnswer) {
				$output['questions_' . $question->getText(null)] = $answer->getNumber();
			} elseif ($answer instanceof BoolAnswer) {
				$output['questions_' . $question->getText(null)] = $answer->getBool();
			} elseif ($answer instanceof DateTimeAnswer && $question instanceof DateTimeQuestion) {
				$output['questions_' . $question->getText(null)] = $answer->getDatetime()->format($question->getFormat());
			} elseif ($answer instanceof ChoiceAnswer) {
				$output['questions_' . $question->getText(null)] = implode('|', array_map(static function (AnswerOption $option) {
					return $option->getOption()->getText(null);
				}, $answer->getOptions()));
			} elseif ($answer instanceof CountryAnswer && $question instanceof CountryQuestion) {
				$output['questions_' . $question->getText(null)] = $answer->getCountry()->getCode();
			} else {
				throw new \InvalidArgumentException("Unsupported question type");
			}
		}

		return $output;
	}
}
