<?php

declare(strict_types=1);

namespace Cleevio\Robot\Api\Response;

use Cleevio\Api\Translatable\SimpleResponse;
use Cleevio\Fields\Entities\Field;
use Cleevio\Hosts\Entities\Host;
use Cleevio\RestApi\Response\Types\ContentTypeFactory;

final class HostsResponseBuilder extends SimpleResponse
{

	/**
	 * @var Host[]
	 */
	private $data;

	/**
	 * @var bool
	 */
	private $showHeader;
	/**
	 * @var array
	 */
	private $fields;

	/**
	 * TripResponseBuilder constructor.
	 * @param Host[] $hosts
	 * @param Field[] $fields
	 * @param string|null $lang
	 * @param bool $showHeader
	 */
	public function __construct(array $hosts, array $fields, ?string $lang, bool $showHeader)
	{
		parent::__construct($lang);

		$this->data = $hosts;
		$this->showHeader = $showHeader;
		$this->fields = $fields;
	}

	protected function getContentType(): string
	{
		return ContentTypeFactory::CONTENT_TYPE_CSV;
	}

	/**
	 * @return array
	 */
	protected function header(): array
	{

		$output = ['id', 'country', 'name', 'type'];

		// Handle fields
		foreach ($this->fields as $field) {
			$output[] = 'fields_' . $field->getName(null);
		}

		return $output;
	}

	/**
	 * @return array
	 */
	protected function data(): array
	{
		$data = array_map(function (Host $host) {
			return (new HostResponseBuilder($host, $this->fields, null))->build();
		}, $this->data);

		return $this->showHeader
			? array_merge([$this->header()], $data)
			: $data;
	}
}
