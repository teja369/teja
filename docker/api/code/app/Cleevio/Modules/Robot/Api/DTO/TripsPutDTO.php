<?php

declare(strict_types=1);

namespace Cleevio\Robot\Api\DTO;

use Cleevio\RestApi\Command\ICommandDTO;

class TripsPutDTO implements ICommandDTO
{
	/**
	 * @var array
	 */
	protected $trips;


	public function __construct(
		array $trips
	)
	{
		$this->trips = $trips;
	}


	public static function fromRequest($data): ICommandDTO
	{
		return new static(
			array_map(static function (object $item) {
				return (array) $item;
			}, $data)
		);
	}

	public function getTrips(): array
	{
		return $this->trips;
	}

}
