<?php

declare(strict_types=1);

namespace Cleevio\Robot\Api\DTO;

use Cleevio\RestApi\Command\ICommandDTO;

class TripGetDTO implements ICommandDTO
{
	/**
	 * @var array
	 */
	protected $fields;


	public function __construct(
		array $fields
	)
	{
		$this->fields = $fields;
	}


	public static function fromRequest($data): ICommandDTO
	{
		return new static(
			array_map(static function (object $item) {
				return ((array) $item)['field'];
			}, $data)
		);
	}

	public function getFields(): array
	{
		return $this->fields;
	}

}
