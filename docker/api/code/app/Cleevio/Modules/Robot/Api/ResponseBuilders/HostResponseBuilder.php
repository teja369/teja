<?php

declare(strict_types=1);

namespace Cleevio\Robot\Api\Response;

use Cleevio\Api\Translatable\SimpleResponse;
use Cleevio\Fields\Entities\Field;
use Cleevio\Hosts\Entities\Host;

final class HostResponseBuilder extends SimpleResponse
{

	/**
	 * @var Host
	 */
	private $data;

	/**
	 * @var  Field[]
	 */
	private $fields;

	/**
	 * TripResponseBuilder constructor.
	 * @param Host $host
	 * @param Field[] $fields
	 * @param string|null $lang
	 */
	public function __construct(Host $host, array $fields, ?string $lang)
	{
		parent::__construct($lang);

		$this->data = $host;
		$this->fields = $fields;
	}

	/**
	 * @return array
	 */
	protected function data(): array
	{

		$output = [
			'id' => $this->data->getId(),
			'country' => $this->data->getCountry()->getCode(),
			'name' => $this->data->getName(),
			'type' => $this->data->getType(),
		];

		// Handle fields
		foreach ($this->fields as $field) {
			$param = $this->data->getParam($field);
			$output['fields_' . $field->getName(null)] = $param !== null
				? $param->getValue()
				: null;
		}

		return $output;
	}
}
