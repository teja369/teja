<?php

declare(strict_types=1);

namespace Cleevio\Robot\ApiModule\Presenters;

use Cleevio\Api\Helpers\IRequestContext;
use Cleevio\Fields\Repositories\FieldConstraints;
use Cleevio\Fields\Repositories\IFieldRepository;
use Cleevio\Hosts\IHostsService;
use Cleevio\Hosts\Repositories\HostConstraints;
use Cleevio\Hosts\Repositories\IHostRepository;
use Cleevio\RestApi\Annotations\ApiRoute;
use Cleevio\RestApi\Annotations\Authenticated;
use Cleevio\RestApi\Annotations\DTO;
use Cleevio\RestApi\Annotations\JsonSchema;
use Cleevio\RestApi\Annotations\Response;
use Cleevio\RestApi\Exceptions\NotFoundException;
use Cleevio\RestApi\Presenters\RestPresenter;
use Cleevio\Robot\Api\DTO\HostGetDTO;
use Cleevio\Robot\Api\Response\HostResponseBuilder;
use Cleevio\Robot\Api\Response\HostsResponseBuilder;
use InvalidArgumentException;
use Nette\Http\IResponse;

/**
 * @ApiRoute("/api/v1/robot/hosts",presenter="Robot:Api:Hosts", tags={"Robot"})
 * @Authenticated("robot")
 */
class HostsPresenter extends RestPresenter
{

	/**
	 * @var IRequestContext
	 */
	private $requestContext;

	/**
	 * @var IHostsService
	 */
	private $hostsService;

	/**
	 * @var IHostRepository
	 */
	private $hostRepository;
	/**
	 * @var IFieldRepository
	 */
	private $fieldRepository;

	/**
	 * @param IRequestContext $requestContext
	 * @param IHostsService $hostsService
	 * @param IHostRepository $hostRepository
	 * @param IFieldRepository $fieldRepository
	 */
	public function __construct(
		IRequestContext $requestContext,
		IHostsService $hostsService,
		IHostRepository $hostRepository,
		IFieldRepository $fieldRepository
	)
	{
		parent::__construct();

		$this->requestContext = $requestContext;
		$this->hostsService = $hostsService;
		$this->hostRepository = $hostRepository;
		$this->fieldRepository = $fieldRepository;
	}

	/**
	 * @ApiRoute("/api/v1/robot/hosts/<host>", method="POST", description="Get host detail")
	 * @Response(400, code="10001", description="Provided host was not found")
	 * @DTO("Cleevio\Robot\Api\DTO\HostGetDTO")
	 * @JsonSchema("request/robot/getHost.json")
	 * @Response(200, file="response/robot/host.json")
	 * @param int $host
	 */
	public function actionHostGet(int $host): void
	{
		$command = $this->getCommandDTO();

		if (!$command instanceof HostGetDTO) {
			throw new InvalidArgumentException;
		}

		$host = $this->hostsService->getHost($host);

		if ($host === null) {
			throw new NotFoundException('hosts.host.get.error.host-not-found', 10001);
		}

		$fieldConstraints = new FieldConstraints;
		$fieldConstraints->setIsDeleted(false);

		if (count($command->getFields()) > 0) {
			$fieldConstraints->setNames($command->getFields());
		}

		$fields = $this->fieldRepository->findAll($fieldConstraints->build());

		$this->sendSuccessResponse(new HostResponseBuilder($host, $fields, $this->requestContext->getLang()), IResponse::S200_OK);
	}

	/**
	 * @ApiRoute("/api/v1/robot/hosts", method="GET", description="Get hosts")
	 * @Response(200)
	 */
	public function actionHostList(): void
	{
		$i = 0;
		$limit = 1000;
		$hostConstraints = (new HostConstraints);
		$hostConstraints->setIsDeleted(false);
		$hostConstraints = $this->hostsService->queryToConstraints($hostConstraints, $this->getHttpRequest(), null);

		$fields = $this->fieldRepository->findAll((new FieldConstraints)->setIsDeleted(false)->build());

		do {
			$hostConstraints->setLength($limit);
			$hostConstraints->setOffset($i * $limit);
			$hosts = $this->hostRepository->findAll($hostConstraints->build());

			$response = new HostsResponseBuilder($hosts, $fields, null, $i === 0);
			$response->send($this->getHttpRequest(), $this->getHttpResponse());
			$this->hostRepository->flush();

			++$i;
		} while (count($hosts) > 0);

		exit;
	}

}
