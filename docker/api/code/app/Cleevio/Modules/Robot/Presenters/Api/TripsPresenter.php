<?php

declare(strict_types=1);

namespace Cleevio\Robot\ApiModule\Presenters;

use Cleevio\Api\Helpers\IRequestContext;
use Cleevio\Countries\CountryNotFound;
use Cleevio\Fields\Repositories\FieldConstraints;
use Cleevio\Fields\Repositories\IFieldRepository;
use Cleevio\Hosts\HostNotFound;
use Cleevio\Questions\Entities\Question;
use Cleevio\Questions\RegistrationNotFound;
use Cleevio\Questions\Repositories\IQuestionRepository;
use Cleevio\Questions\Repositories\QuestionConstraints;
use Cleevio\Registrations\Repositories\IRegistrationRepository;
use Cleevio\Repository\Constraints;
use Cleevio\RestApi\Annotations\ApiRoute;
use Cleevio\RestApi\Annotations\Authenticated;
use Cleevio\RestApi\Annotations\DTO;
use Cleevio\RestApi\Annotations\JsonSchema;
use Cleevio\RestApi\Annotations\Queries;
use Cleevio\RestApi\Annotations\Response;
use Cleevio\RestApi\Exceptions\BadRequestException;
use Cleevio\RestApi\Exceptions\NotFoundException;
use Cleevio\RestApi\Presenters\RestPresenter;
use Cleevio\RestApi\Response\EmptyResponse;
use Cleevio\Robot\Api\DTO\TripGetDTO;
use Cleevio\Robot\Api\DTO\TripsPutDTO;
use Cleevio\Robot\Api\Response\TripResponseBuilder;
use Cleevio\Robot\Api\Response\TripsResponseBuilder;
use Cleevio\Trips\InvalidOrderByParameter;
use Cleevio\Trips\ITripService;
use Cleevio\Trips\Repositories\ITripRepository;
use Cleevio\Trips\Repositories\TripConstraints;
use Cleevio\Trips\TripQueryInvalidDate;
use Cleevio\Trips\TripQueryInvalidDateTime;
use Cleevio\Trips\TripStateInvalid;
use Cleevio\Trips\ValidationException;
use Cleevio\Users\UserNotFound;
use InvalidArgumentException;
use Nette\Http\IResponse;

/**
 * @ApiRoute("/api/v1/robot/trips>",presenter="Robot:Api:Trips", tags={"Robot"})
 * @Authenticated("robot")
 */
class TripsPresenter extends RestPresenter
{

	/**
	 * @var IRequestContext
	 */
	private $requestContext;

	/**
	 * @var ITripService
	 */
	private $tripService;

	/**
	 * @var IRegistrationRepository
	 */
	private $registrationRepository;

	/**
	 * @var ITripRepository
	 */
	private $tripRepository;
	/**
	 * @var IFieldRepository
	 */
	private $fieldRepository;
	/**
	 * @var IQuestionRepository
	 */
	private $questionRepository;

	/**
	 * @param IRequestContext $requestContext
	 * @param ITripService $tripService
	 * @param ITripRepository $tripRepository
	 * @param IRegistrationRepository $registrationRepository
	 * @param IFieldRepository $fieldRepository
	 * @param IQuestionRepository $questionRepository
	 */
	public function __construct(
		IRequestContext $requestContext,
		ITripService $tripService,
		ITripRepository $tripRepository,
		IRegistrationRepository $registrationRepository,
		IFieldRepository $fieldRepository,
		IQuestionRepository $questionRepository
	)
	{
		parent::__construct();

		$this->requestContext = $requestContext;
		$this->tripService = $tripService;
		$this->registrationRepository = $registrationRepository;
		$this->tripRepository = $tripRepository;
		$this->fieldRepository = $fieldRepository;
		$this->questionRepository = $questionRepository;
	}

	/**
	 * @ApiRoute("/api/v1/robot/trips/<trip>", method="POST", description="Get trip detail")
	 * @DTO("Cleevio\Robot\Api\DTO\TripGetDTO")
	 * @JsonSchema("request/robot/getTrip.json")
	 * @Response(200, file="response/robot/trip.json")
	 * @param int $trip
	 */
	public function actionTripGet(int $trip): void
	{
		$command = $this->getCommandDTO();

		if (!$command instanceof TripGetDTO) {
			throw new InvalidArgumentException;
		}

		$trip = $this->tripService->findById($trip);

		if ($trip === null) {
			throw new NotFoundException('questions.question.get.error.trip-not-found', 10001);
		}

		$fieldConstraints = (new FieldConstraints)
			->setIsDeleted(false);

		if (count($command->getFields()) > 0) {
			$fieldConstraints->setNames($command->getFields());
		}

		$fields = $this->fieldRepository->findAll($fieldConstraints->build());

		$registrations = $this->registrationRepository->findAll(Constraints::empty());

		$questionConstraints = (new QuestionConstraints)
			->setIsDeleted(false)
			->setVisibility([Question::QUESTION_VISIBILITY_ENABLED]);

		if (count($command->getFields()) > 0) {
			$questionConstraints->setTexts($command->getFields());
		}

		$questions = $this->questionRepository->findAll($questionConstraints->build());

		$this->sendSuccessResponse(new TripResponseBuilder($trip, $fields, $registrations, $questions, $this->requestContext->getLang()), IResponse::S200_OK);
	}

	/**
	 * @ApiRoute("/api/v1/robot/trips", method="GET", description="Get trips")
	 * @Queries("queries/robot/trips.json")
	 * @Response(200)
	 * @Response(400, code="10002", description="Provided country was not found")
	 * @Response(400, code="10003", description="Provided host was not found")
	 * @Response(400, code="10004", description="Invalid order by argument")
	 * @Response(400, code="10005", description="Invalid datetime format in query parameter")
	 * @Response(400, code="10006", description="Invalid date format in query parameter")
	 * @Response(400, code="10007", description="Provided user was not found")
	 * @Response(400, code="10008", description="Provided registration was not found")
	 */
	public function actionTripList(): void
	{
		try {
			$i = 0;
			$limit = 1000;
			$tripConstraints = (new TripConstraints);
			$tripConstraints = $this->tripService->queryToConstraints($tripConstraints, $this->getHttpRequest());

			$registrations = $this->registrationRepository->findAll(Constraints::empty());
			$fields = $this->fieldRepository->findAll((new FieldConstraints)->setIsDeleted(false)->build());
			$questions = $this->questionRepository->findAll((new QuestionConstraints)->setIsDeleted(false)->setVisibility([Question::QUESTION_VISIBILITY_ENABLED])->build());

			do {
				$tripConstraints->setLength($limit);
				$tripConstraints->setOffset($i * $limit);

				$trips = $this->tripRepository->findAll($tripConstraints->build());

				$response = new TripsResponseBuilder($trips, $fields, $registrations, $questions, null, $i === 0);
				$response->send($this->getHttpRequest(), $this->getHttpResponse());

				$this->tripRepository->flush();
				++$i;
			} while (count($trips) > 0);

			exit;

		} catch (CountryNotFound $e) {
			throw new BadRequestException('countries.country.get.error.country-not-found', 10002);
		} catch (HostNotFound $e) {
			throw new BadRequestException('hosts.host.get.error.host-not-found', 10003);
		} catch (InvalidOrderByParameter $e) {
			throw new BadRequestException('trips.trip.get.error.orderBy-not-found', 10004);
		} catch (TripQueryInvalidDateTime $e) {
			throw new BadRequestException('trips.trip.get.error.query-datetime-invalid', 10005);
		} catch (TripQueryInvalidDate $e) {
			throw new BadRequestException('trips.trip.get.error.query-date-invalid', 10006);
		} catch (UserNotFound $e) {
			throw new BadRequestException('users.user.get.error.user-not-found', 10007);
		} catch (RegistrationNotFound $e) {
			throw new BadRequestException('registrations.registration.get.error.registration-not-found', 10008);
		}
	}

	/**
	 * @ApiRoute("/api/v1/robot/trips", method="PUT", description="Update trip status")
	 * @DTO("Cleevio\Robot\Api\DTO\TripsPutDTO")
	 * @JsonSchema("request/robot/putTrips.json")
	 * @Response(400, code="10001", description="Trip was not found")
	 * @Response(400, code="10003", description="Invalid trip state")
	 */
	public function actionTripPut(): void
	{
		$command = $this->getCommandDTO();

		if (!$command instanceof TripsPutDTO) {
			throw new InvalidArgumentException;
		}

		foreach ($command->getTrips() as $commandTrip) {

			$trip = $this->tripService->findById($commandTrip['id']);

			if ($trip === null) {
				throw new NotFoundException('trips.trip.get.error.trip-not-found', 10001);
			}

			try {
				$this->tripService->setTripState($trip, $commandTrip['state'], false);
			} catch (ValidationException $e) {
				throw new BadRequestException($e->getMessage(), 10002, $e->getParams());
			} catch (TripStateInvalid $e) {
				throw new BadRequestException('trips.trip.put.error.state-invalid', 10003);
			}
		}

		$this->sendSuccessResponse(new EmptyResponse, IResponse::S200_OK);
	}

}
