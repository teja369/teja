<?php

declare(strict_types=1);

namespace Cleevio\Issues\ApiModule\Presenters;

use Cleevio\Issues\Api\DTO\SendIssueDTO;
use Cleevio\Mailer\Context;
use Cleevio\Mailer\EmailEvent;
use Cleevio\RestApi\Annotations\ApiRoute;
use Cleevio\RestApi\Annotations\Authenticated;
use Cleevio\RestApi\Annotations\DTO;
use Cleevio\RestApi\Annotations\JsonSchema;
use Cleevio\RestApi\Annotations\Response;
use Cleevio\RestApi\Exceptions\AuthenticationException;
use Cleevio\RestApi\Exceptions\BadRequestException;
use Cleevio\RestApi\Presenters\RestPresenter;
use Cleevio\Users\Entities\User;
use InvalidArgumentException;
use Nette\Http\IResponse;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

/**
 * @ApiRoute("/api/v1/issues",presenter="Issues:Api:Issue")
 */
class IssuePresenter extends RestPresenter
{

	/**
	 * @var EventDispatcherInterface
	 */
	private $eventDispatcher;

	/**
	 * @var Context
	 */
	private $mailContext;

	/**
	 * IssuePresenter constructor.
	 * @param EventDispatcherInterface $eventDispatcher
	 * @param Context $mailContext
	 */
	public function __construct(EventDispatcherInterface $eventDispatcher, Context $mailContext)
	{
		parent::__construct();
		$this->eventDispatcher = $eventDispatcher;
		$this->mailContext = $mailContext;
	}

	/**
	 * @ApiRoute("/api/v1/issues", method="POST", description="Sends email from issue form")
	 * @DTO("Cleevio\Issues\Api\DTO\SendIssueDTO")
	 * @JsonSchema("request/issues/issuePost.json")
	 * @Response(400, code="10002", description="Sending user does not have an email address")
	 * @Response(400, code="10003", description="Email could not be send")
	 * @Authenticated()
	 */
	public function actionIssuePost(): void
	{
		$authenticator = $this->getAuthenticator();

		$user = $authenticator->getUser();

		if (!$user instanceof User) {
			throw new AuthenticationException("users.auth.error.user-not-found", 10001);
		}

		$command = $this->getCommandDTO();

		if (!$command instanceof SendIssueDTO) {
			throw new InvalidArgumentException;
		}

		if ($user->getEmail() === null) {
			throw new BadRequestException("users.auth.error.user-no-email", 10002);
		}

		try {
			$this->eventDispatcher->dispatch(new EmailEvent(
				$command->getTitle(),
				$command->getMessage(),
				$this->mailContext->getAddressSupport(),
				$this->mailContext->getAddressSystem()
			));
		} catch (\Exception $ex) {
			throw new BadRequestException("issues.issue.error.email", 10003);
		}

		$this->sendSuccessResponse(null, IResponse::S200_OK);

	}

}
