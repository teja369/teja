<?php

declare(strict_types=1);

namespace Cleevio\Issues\DI;

use Cleevio\Modules\Providers\IPresenterMappingProvider;
use Nette\DI\CompilerExtension;
use Nettrine\ORM\DI\Traits\TEntityMapping;

class IssuesExtension extends CompilerExtension implements IPresenterMappingProvider
{

	use TEntityMapping;

	public function loadConfiguration()
	{
		$this->compiler->loadConfig(__DIR__ . '/services.neon');
	}

	public function getPresenterMapping(): array
	{
		return ['Issues' => 'Cleevio\\Issues\\*Module\\Presenters\\*Presenter'];
	}

}
