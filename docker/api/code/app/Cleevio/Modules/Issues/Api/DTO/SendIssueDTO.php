<?php

declare(strict_types=1);

namespace Cleevio\Issues\Api\DTO;

use Cleevio\RestApi\Command\ICommandDTO;

class SendIssueDTO implements ICommandDTO
{

	/**
	 * @var string
	 */
	private $title;

	/**
	 * @var string
	 */
	private $message;


	public function __construct(
		string $title,
		string $message
	)
	{
		$this->title = $title;
		$this->message = $message;
	}


	public static function fromRequest($data): ICommandDTO
	{
		return new static(
			$data->title,
			$data->message
		);
	}


	/**
	 * @return string
	 */
	public function getTitle(): string
	{
		return $this->title;
	}


	/**
	 * @return string
	 */
	public function getMessage(): string
	{
		return $this->message;
	}

}
