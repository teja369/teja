<?php

declare(strict_types=1);

namespace Cleevio\Answers\Api\Response;

use Cleevio\Answers\Api\Response\Answers\ChoiceAnswerResponseBuilder;
use Cleevio\Answers\Entities\Answer;
use Cleevio\Answers\Entities\Answers\BoolAnswer;
use Cleevio\Answers\Entities\Answers\ChoiceAnswer;
use Cleevio\Answers\Entities\Answers\CountryAnswer;
use Cleevio\Answers\Entities\Answers\DateTimeAnswer;
use Cleevio\Answers\Entities\Answers\NumberAnswer;
use Cleevio\Answers\Entities\Answers\TextAnswer;
use Cleevio\Api\Translatable\SimpleResponse;
use Cleevio\Countries\Api\Response\CountryResponseBuilder;
use Cleevio\Questions\Api\Response\QuestionResponseBuilder;
use Cleevio\Questions\Entities\Questions\DateTimeQuestion;

final class AnswerResponseBuilder extends SimpleResponse
{

	/**
	 * @var Answer
	 */
	private $data;

	/**
	 * QuestionResponseBuilder constructor.
	 * @param Answer $answer
	 * @param string|null $lang
	 */
	public function __construct(Answer $answer, ?string $lang)
	{
		parent::__construct($lang);

		$this->data = $answer;
	}

	/**
	 * @return array
	 */
	protected function data(): array
	{
		$question = $this->data->getQuestion();

		return [
			'id' => $this->data->getId(),
			'question' => (new QuestionResponseBuilder($question, $this->data->getTrip(), $this->getLang()))->build(),
			'text' => $this->data instanceof TextAnswer ? $this->data->getText() : null,
			'bool' => $this->data instanceof BoolAnswer ? $this->data->getBool() : null,
			'country' => $this->data instanceof CountryAnswer ? (new CountryResponseBuilder($this->data->getCountry(), $this->getLang()))->build() : null,
			'number' => $this->data instanceof NumberAnswer ? $this->data->getNumber() : null,
			'choice' => $this->data instanceof ChoiceAnswer ? (new ChoiceAnswerResponseBuilder($this->data))->build() : null,
			'datetime' => $this->data instanceof DateTimeAnswer && $question instanceof DateTimeQuestion
				? $this->data->getDatetime()->format($question->getFormat())
				: null,
		];
	}
}
