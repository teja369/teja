<?php

declare(strict_types=1);

namespace Cleevio\Answers\Api\Response;

use Cleevio\Answers\Entities\Answer;
use Cleevio\Api\Translatable\SimpleResponse;

final class AnswersResponseBuilder extends SimpleResponse
{
	/**
	 * @var array
	 */
	private $data;

	/**
	 * AnswersResponseBuilder constructor.
	 * @param Answer[] $data
	 * @param string|null $lang
	 */
	public function __construct(array $data, ?string $lang)
	{
		parent::__construct($lang);

		$this->data = $data;
	}

	/**
	 * @return array
	 */
	protected function data(): array
	{
		return array_map(function (Answer $answer) {
			return (new AnswerResponseBuilder($answer, $this->getLang()))->build();
		}, $this->data);
	}

}
