<?php

declare(strict_types=1);

namespace Cleevio\Answers\Api\Response\Answers;

use Cleevio\Answers\Entities\Answers\AnswerOption;
use Cleevio\Answers\Entities\Answers\ChoiceAnswer;
use Cleevio\RestApi\Response\SimpleResponse;

final class ChoiceAnswerResponseBuilder extends SimpleResponse
{

	/**
	 * @var ChoiceAnswer
	 */
	private $data;

	/**
	 * ChoiceAnswerResponseBuilder constructor.
	 * @param ChoiceAnswer $answer
	 */
	public function __construct(ChoiceAnswer $answer)
	{
		$this->data = $answer;
	}

	/**
	 * @return array
	 */
	protected function data(): array
	{
		return array_map(static function (AnswerOption $option) {
				return $option->getOption()->getId();
			}, $this->data->getOptions())
		;
	}
}
