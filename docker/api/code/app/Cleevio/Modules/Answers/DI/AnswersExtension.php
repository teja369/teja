<?php

declare(strict_types=1);

namespace Cleevio\Answers\DI;

use Cleevio\Modules\Providers\IPresenterMappingProvider;
use Nette\DI\CompilerExtension;
use Nettrine\ORM\DI\Traits\TEntityMapping;

class AnswersExtension extends CompilerExtension implements IPresenterMappingProvider
{

	use TEntityMapping;

	public function loadConfiguration()
	{
		$this->compiler->loadConfig(__DIR__ . '/services.neon');
		$this->setEntityMappings(
			[
				'Cleevio\Answers\Entities\Answer' => __DIR__ . '/..',
			]
		);
	}


	public function getPresenterMapping(): array
	{
		return ['Answers' => 'Cleevio\\Answers\\*Module\\Presenters\\*Presenter'];
	}

}
