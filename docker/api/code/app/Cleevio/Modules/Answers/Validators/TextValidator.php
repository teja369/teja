<?php

declare(strict_types=1);

namespace Cleevio\Answers\Validators;

use Cleevio\Answers\Entities\Answer;
use Cleevio\Answers\Entities\Answers\TextAnswer;
use Cleevio\Answers\InvalidAnswerException;
use Cleevio\Questions\Entities\Question;
use Cleevio\Questions\Entities\Questions\TextQuestion;
use Cleevio\Trips\Entities\Trip;

class TextValidator extends BaseValidator
{

	/**
	 * Validate question answer
	 * @param Trip $trip
	 * @param Question $question
	 * @param Answer|null $answer
	 * @return Answer
	 * @throws InvalidAnswerException
	 */
	function validate(Trip $trip, Question $question, ?Answer $answer): ?Answer
	{
		if (!$this->isValidationRequired($trip, $question, $answer)) {
			return $answer;
		}

		if (!$question instanceof TextQuestion) {
			throw new InvalidAnswerException("questions.validation.invalid", [
				'name' => $question->getText(null),
				'group' => $question->getGroup()->getId(),
			]);
		}

		if ($answer !== null && !$answer instanceof TextAnswer) {
			throw new InvalidAnswerException("questions.validation.invalid", [
				'name' => $question->getText(null),
				'group' => $question->getGroup()->getId(),
			]);
		}

		if ($question->isRequired() && ($answer === null || $answer->getText() === "")) {
			throw new InvalidAnswerException("questions.validation.required", [
				'name' => $question->getText(null),
				'group' => $question->getGroup()->getId(),
			]);
		}

		if ($question->getValidation() !== null && $answer !== null && $answer->getText() !== "") {
			if (!$answer->match($question->getValidation())) {
				throw new InvalidAnswerException("questions.validation.pattern", [
					'name' => $question->getText(null),
					'group' => $question->getGroup()->getId(),
				]);
			}
		}

		if ($answer !== null && $question->getMinLength() !== null && strlen($answer->getText()) < $question->getMinLength()) {
			throw new InvalidAnswerException("questions.validation.minLength", [
				'name' => $question->getText(null),
				'group' => $question->getGroup()->getId(),
				'count' => $question->getMinLength(),
			]);
		}

		if ($answer !== null && $question->getMaxLength() !== null && strlen($answer->getText()) > $question->getMaxLength()) {
			throw new InvalidAnswerException("questions.validation.maxLength", [
				'name' => $question->getText(null),
				'group' => $question->getGroup()->getId(),
				'count' => $question->getMaxLength(),
			]);
		}

		return $answer;
	}
}
