<?php

declare(strict_types=1);

namespace Cleevio\Answers\Validators;

use Cleevio\Answers\Entities\Answer;
use Cleevio\Answers\Entities\Answers\NumberAnswer;
use Cleevio\Answers\InvalidAnswerException;
use Cleevio\Questions\Entities\Question;
use Cleevio\Questions\Entities\Questions\NumberQuestion;
use Cleevio\Trips\Entities\Trip;

class NumberValidator extends BaseValidator
{

	/**
	 * Validate question answer
	 * @param Trip $trip
	 * @param Question $question
	 * @param Answer|null $answer
	 * @return Answer
	 * @throws InvalidAnswerException
	 */
	function validate(Trip $trip, Question $question, ?Answer $answer): ?Answer
	{
		if (!$this->isValidationRequired($trip, $question, $answer)) {
			return $answer;
		}

		if (!$question instanceof NumberQuestion) {
			throw new InvalidAnswerException("questions.validation.invalid", [
				'name' => $question->getText(null),
				'group' => $question->getGroup()->getId(),
			]);
		}

		if ($answer !== null && !$answer instanceof NumberAnswer) {
			throw new InvalidAnswerException("questions.validation.invalid", [
				'name' => $question->getText(null),
				'group' => $question->getGroup()->getId(),
			]);
		}

		if ($question->isRequired() && $answer === null) {
			throw new InvalidAnswerException("questions.validation.required", [
				'name' => $question->getText(null),
				'group' => $question->getGroup()->getId(),
			]);
		}

		if ($answer !== null && $question->getMinValue() !== null && $answer->getNumber() < $question->getMinValue()) {
			throw new InvalidAnswerException("questions.validation.minValue", [
				'name' => $question->getText(null),
				'group' => $question->getGroup()->getId(),
				'count' => $question->getMinValue(),
			]);
		}

		if ($answer !== null && $question->getMaxValue() !== null && $answer->getNumber() > $question->getMaxValue()) {
			throw new InvalidAnswerException("questions.validation.maxValue", [
				'name' => $question->getText(null),
				'group' => $question->getGroup()->getId(),
				'count' => $question->getMaxValue(),
			]);
		}

		return $answer;
	}
}
