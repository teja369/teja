<?php

declare(strict_types=1);

namespace Cleevio\Answers\Validators;

use Cleevio\Answers\Entities\Answer;
use Cleevio\Answers\InvalidAnswerException;
use Cleevio\Questions\Entities\Question;
use Cleevio\Trips\Entities\Trip;

interface IAnswerValidator
{

	/**
	 * Validate question answer
	 *
	 * @param Trip $trip
	 * @param Question $question
	 * @param Answer|null $answer
	 * @return Answer
	 * @throws InvalidAnswerException
	 */
	function validate(Trip $trip, Question $question, ?Answer $answer): ?Answer;

}
