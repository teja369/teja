<?php

declare(strict_types=1);

namespace Cleevio\Answers\Validators;

use Cleevio\Answers\Entities\Answer;
use Cleevio\Answers\Entities\Answers\DateTimeAnswer;
use Cleevio\Answers\InvalidAnswerException;
use Cleevio\Questions\Entities\Question;
use Cleevio\Questions\Entities\Questions\DateTimeQuestion;
use Cleevio\Trips\Entities\Trip;

class DateTimeValidator extends BaseValidator
{

	/**
	 * Validate question answer
	 * @param Trip $trip
	 * @param Question $question
	 * @param Answer|null $answer
	 * @return Answer
	 * @throws InvalidAnswerException
	 */
	function validate(Trip $trip, Question $question, ?Answer $answer): ?Answer
	{
		if (!$this->isValidationRequired($trip, $question, $answer)) {
			return $answer;
		}

		if (!$question instanceof DateTimeQuestion) {
			throw new InvalidAnswerException("questions.validation.invalid", [
				'name' => $question->getText(null),
				'group' => $question->getGroup()->getId(),
			]);
		}

		if ($answer !== null && !$answer instanceof DateTimeAnswer) {
			throw new InvalidAnswerException("questions.validation.invalid", [
				'name' => $question->getText(null),
				'group' => $question->getGroup()->getId(),
			]);
		}

		if ($question->isRequired() && ($answer === null || $answer->getDatetime()->getTimestamp() === 0)) {
			throw new InvalidAnswerException("questions.validation.required", [
				'name' => $question->getText(null),
				'group' => $question->getGroup()->getId(),
			]);
		}

		if ($question->getValidation() !== null && $answer !== null && $answer->getDatetime()->getTimestamp() !== 0) {
			if (!$answer->match($question->getValidation())) {
				throw new InvalidAnswerException("questions.validation.pattern", [
					'name' => $question->getText(null),
					'group' => $question->getGroup()->getId(),
				]);
			}
		}

		return $answer;
	}
}
