<?php

declare(strict_types=1);

namespace Cleevio\Answers\Validators;

use Cleevio\Answers\Entities\Answer;
use Cleevio\Questions\Entities\Question;
use Cleevio\Trips\Entities\Trip;

abstract class BaseValidator implements IAnswerValidator
{

	/**
	 * Goes up the parent tree and check if the question was even displayed
	 * @param Trip        $trip
	 * @param Question    $question
	 * @param Answer|null $answer
	 * @return bool
	 */
	protected function isValidationRequired(Trip $trip, Question $question, ?Answer $answer): bool
	{
		while ($question->getParent() !== null) {

			$parent = $question->getParent();

			// Check if this question is visible
			$answer = $trip->getAnswer($parent);

			// If parent has no answer than this question is not displayed
			if ($answer === null) {
				return false;
			}

			if (!in_array($answer->getQuestion()->getVisibility(), [
				Question::QUESTION_VISIBILITY_ENABLED, Question::QUESTION_VISIBILITY_PREVIEW,
			], true)) {
				return false;
			}

			// If parent answer does not match parent rule than question is not displayed
			if ($question->getParentRule() !== null && !$answer->match($question->getParentRule())) {
				return false;
			}

			$question = $parent;
		}

		return true;
	}

}
