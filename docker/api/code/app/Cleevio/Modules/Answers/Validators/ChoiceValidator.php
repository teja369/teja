<?php

declare(strict_types=1);

namespace Cleevio\Answers\Validators;

use Cleevio\Answers\Entities\Answer;
use Cleevio\Answers\Entities\Answers\ChoiceAnswer;
use Cleevio\Answers\InvalidAnswerException;
use Cleevio\Questions\Entities\Question;
use Cleevio\Questions\Entities\Questions\ChoiceQuestion;
use Cleevio\Trips\Entities\Trip;

class ChoiceValidator extends BaseValidator
{

	/**
	 * Validate question answer
	 *
	 * @param Trip $trip
	 * @param Question $question
	 * @param Answer|null $answer
	 * @return Answer
	 * @throws InvalidAnswerException
	 */
	function validate(Trip $trip, Question $question, ?Answer $answer): ?Answer
	{
		if (!$this->isValidationRequired($trip, $question, $answer)) {
			return $answer;
		}

		if (!$question instanceof ChoiceQuestion) {
			throw new InvalidAnswerException("questions.validation.invalid", [
				'name' => $question->getText(null),
				'group' => $question->getGroup()->getId(),
			]);
		}

		if ($answer !== null && !$answer instanceof ChoiceAnswer) {
			throw new InvalidAnswerException("questions.validation.invalid", [
				'name' => $question->getText(null),
				'group' => $question->getGroup()->getId(),
			]);
		}

		if ($question->isRequired() && ($answer === null || count($answer->getOptions()) === 0)) {
			throw new InvalidAnswerException("questions.validation.required", [
				'name' => $question->getText(null),
				'group' => $question->getGroup()->getId(),
			]);
		}

		if ($answer !== null && !$question->isMultiChoice() && count($answer->getOptions()) > 1) {
			throw new InvalidAnswerException("questions.validator.choice.single-choice", [
				'name' => $question->getText(null),
				'group' => $question->getGroup()->getId(),
			]);
		}

		return $answer;
	}
}
