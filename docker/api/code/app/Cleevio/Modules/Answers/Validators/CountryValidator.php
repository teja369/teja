<?php

declare(strict_types=1);

namespace Cleevio\Answers\Validators;

use Cleevio\Answers\Entities\Answer;
use Cleevio\Answers\InvalidAnswerException;
use Cleevio\Questions\Entities\Question;
use Cleevio\Questions\Entities\Questions\CountryQuestion;
use Cleevio\Trips\Entities\Trip;

class CountryValidator extends BaseValidator
{

	/**
	 * Validate question answer
	 *
	 * @param Trip $trip
	 * @param Question $question
	 * @param Answer|null $answer
	 * @return Answer
	 * @throws InvalidAnswerException
	 */
	function validate(Trip $trip, Question $question, ?Answer $answer): ?Answer
	{
		if (!$this->isValidationRequired($trip, $question, $answer)) {
			return $answer;
		}

		if (!$question instanceof CountryQuestion) {
			throw new InvalidAnswerException("questions.validation.invalid", [
				'name' => $question->getText(null),
				'group' => $question->getGroup()->getId(),
			]);
		}

		if ($question->isRequired() && ($answer === null)) {
			throw new InvalidAnswerException("questions.validation.required", [
				'name' => $question->getText(null),
				'group' => $question->getGroup()->getId(),
			]);
		}

		return $answer;
	}
}
