<?php

declare(strict_types=1);

namespace Cleevio\Answers\Repositories;

use Cleevio\Repository\IRepository;

interface IAnswerRepository extends IRepository
{

}
