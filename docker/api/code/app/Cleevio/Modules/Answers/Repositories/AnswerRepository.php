<?php

declare(strict_types=1);

namespace Cleevio\Answers\Repositories;

use Cleevio\Answers\Entities\Answer;
use Cleevio\Repository\Repositories\SqlRepository;

class AnswerRepository extends SqlRepository implements IAnswerRepository
{

	/**
	 * Model class type
	 * @return string
	 */
	public function type(): string
	{
		return Answer::class;
	}
}
