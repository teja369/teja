<?php

declare(strict_types=1);

namespace Cleevio\Ansers\Repositories;

use Cleevio\Questions\Entities\Question;
use Cleevio\Repository\ConstraintsBuilder;
use Cleevio\Repository\Expressions\Comparison;
use Cleevio\Trips\Entities\Trip;
use Cleevio\Users\Entities\User;

class AnswerConstraints extends ConstraintsBuilder
{

	/**
	 * @param Trip $trip
	 * @return AnswerConstraints
	 */
	function setTrip(Trip $trip): AnswerConstraints
	{
		$this->addCriteria(new Comparison('trip.id', '=', $trip->getId()));

		return $this;
	}

	/**
	 * @param Question $question
	 * @return AnswerConstraints
	 */
	function setQuestion(Question $question): AnswerConstraints
	{
		$this->addCriteria(new Comparison('question.id', '=', $question->getId()));

		return $this;
	}

	/**
	 * @param User $user
	 * @return AnswerConstraints
	 */
	function setUser(User $user): AnswerConstraints
	{
		$this->addCriteria(new Comparison('trip.user.id', '=', $user->getId()));

		return $this;
	}

}
