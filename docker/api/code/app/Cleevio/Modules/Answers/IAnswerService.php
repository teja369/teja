<?php

declare(strict_types=1);

namespace Cleevio\Answers;

use Cleevio\Answers\Entities\Answer;
use Cleevio\Questions\Entities\Question;
use Cleevio\Trips\Entities\Trip;

interface IAnswerService
{

	/**
	 * @param Trip $trip
	 * @param Question $question
	 * @param array|null $dto
	 * @return Answer|null
	 * @throws InvalidAnswerException
	 */
	function initAnswer(Trip $trip, Question $question, ?array $dto): ?Answer;

	/**
	 * @param Trip $trip
	 * @param Question $question
	 * @param Answer|null $answer
	 * @return Answer|null
	 * @throws InvalidAnswerException
	 */
	function validate(Trip $trip, Question $question, ?Answer $answer): ?Answer;

	/**
	 * Get answers for trip
	 * @param Trip $trip Trip
	 * @param bool $preFilled Include answers for pre-filled questions from previous trip
	 * @return Answer[]
	 */
	function getAnswers(Trip $trip, bool $preFilled): array;
}
