<?php

declare(strict_types=1);

namespace Cleevio\Answers\Entities\Answers;

use Cleevio\Answers\Entities\Answer;
use Cleevio\Questions\Entities\Question;
use Cleevio\Questions\Entities\Questions\DateTimeQuestion;
use Cleevio\Trips\Entities\Trip;
use DateTime;
use Doctrine\ORM\Mapping as ORM;
use InvalidArgumentException;

/**
 * @ORM\Entity()
 */
class DateTimeAnswer extends Answer
{

	/**
	 * @ORM\Column(type="datetime", nullable=false)
	 * @var DateTime
	 */
	private $datetime;

	/**
	 * @param Trip $trip
	 * @param Question $question
	 * @param DateTime $datetime
	 */
	public function __construct(Trip $trip, Question $question, DateTime $datetime)
	{
		parent::__construct($trip, $question);

		$this->datetime = $datetime;
	}

	/**
	 * @return string
	 */
	public function getType(): string
	{
		return Question::QUESTION_TYPE_DATETIME;
	}

	public function getDatetime(): DateTime
	{
		return $this->datetime;
	}

	/**
	 * Check if answer matches provided rule
	 * @param string $rule
	 * @return bool
	 */
	public function match(string $rule): bool
	{
		$question = $this->getQuestion();

		if (!$question instanceof DateTimeQuestion) {
			throw new InvalidArgumentException;
		}

		$match = preg_match('/' . $rule . '/', $this->getDatetime()->format($question->getFormat()));

		return $match !== 0 && $match !== false;
	}
}
