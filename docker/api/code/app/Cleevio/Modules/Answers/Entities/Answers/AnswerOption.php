<?php

declare(strict_types=1);

namespace Cleevio\Answers\Entities\Answers;

use Cleevio\Answers\Entities\Answer;
use Cleevio\Questions\Entities\Questions\QuestionOption;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 * @ORM\Table(name="answers_option")
 */
class AnswerOption
{
	/**
	 * @ORM\Id
	 * @ORM\Column(type="integer")
	 * @ORM\GeneratedValue
	 * @var int
	 */
	private $id;

	/**
	 * @ORM\ManyToOne(targetEntity="Cleevio\Questions\Entities\Questions\QuestionOption", cascade={"persist"})
	 * @ORM\JoinColumn(name="question_option_id", referencedColumnName="id", nullable=false)
	 * @var QuestionOption
	 */
	protected $option;

	/**
	 * @ORM\ManyToOne(targetEntity="Cleevio\Answers\Entities\Answer", cascade={"persist"})
	 * @ORM\JoinColumn(name="answer_id", referencedColumnName="id", nullable=false)
	 * @var Answer
	 */
	protected $answer;

	/**
	 * @param QuestionOption $option
	 * @param Answer $answer
	 */
	public function __construct(QuestionOption $option, Answer $answer)
	{
		$this->option = $option;
		$this->answer = $answer;
	}

	public function getId(): int
	{
		return $this->id;
	}

	public function getOption(): QuestionOption
	{
		return $this->option;
	}

	public function getAnswer(): Answer
	{
		return $this->answer;
	}
}
