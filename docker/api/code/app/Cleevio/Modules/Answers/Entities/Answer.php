<?php

declare(strict_types=1);

namespace Cleevio\Answers\Entities;

use Cleevio\Questions\Entities\Question;
use Cleevio\Trips\Entities\Trip;
use Doctrine\ORM\Mapping as ORM;
use InvalidArgumentException;

/**
 * @ORM\Entity()
 * @ORM\Table(name="answers")
 * @ORM\InheritanceType("SINGLE_TABLE")
 * @ORM\DiscriminatorColumn(name="type", type="string")
 * @ORM\DiscriminatorMap({
 *     "base" = "answer",
 *     "text" = "Cleevio\Answers\Entities\Answers\TextAnswer",
 *     "number" = "Cleevio\Answers\Entities\Answers\NumberAnswer",
 *     "bool" = "Cleevio\Answers\Entities\Answers\BoolAnswer",
 *     "choice" = "Cleevio\Answers\Entities\Answers\ChoiceAnswer",
 *     "datetime" = "Cleevio\Answers\Entities\Answers\DateTimeAnswer",
 *     "country" = "Cleevio\Answers\Entities\Answers\CountryAnswer",
 * })
 */
abstract class Answer
{

	/**
	 * @ORM\Id
	 * @ORM\Column(type="integer")
	 * @ORM\GeneratedValue
	 * @var int|null
	 */
	private $id;

	/**
	 * @ORM\ManyToOne(targetEntity="Cleevio\Trips\Entities\Trip")
	 * @ORM\JoinColumn(name="trip_id", referencedColumnName="id", nullable=false)
	 * @var Trip
	 */
	protected $trip;

	/**
	 * @ORM\ManyToOne(targetEntity="Cleevio\Questions\Entities\Question")
	 * @ORM\JoinColumn(name="question_id", referencedColumnName="id", nullable=false)
	 * @var Question
	 */
	protected $question;

	/**
	 * @param Trip $trip
	 * @param Question $question
	 */
	public function __construct(Trip $trip, Question $question)
	{
		$this->trip = $trip;
		$this->question = $question;
	}

	public function getId(): int
	{
		if ($this->id === null) {
			throw new InvalidArgumentException;
		}

		return $this->id;
	}

	public function getTrip(): Trip
	{
		return $this->trip;
	}

	public function setTrip(Trip $trip): void
	{
		$this->trip = $trip;
	}

	public function getQuestion(): Question
	{
		return $this->question;
	}

	public function setQuestion(Question $question): void
	{
		$this->question = $question;
	}

	/**
	 * Check if answer matches provided rule
	 * @param string $rule
	 * @return bool
	 */
	abstract public function match(string $rule): bool;

	/**
	 * Clone answer
	 */
	public function __clone()
	{
		$this->id = null;
	}

}
