<?php

declare(strict_types=1);

namespace Cleevio\Answers\Entities\Answers;

use Cleevio\Answers\Entities\Answer;
use Cleevio\Questions\Entities\Question;
use Cleevio\Trips\Entities\Trip;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 */
class TextAnswer extends Answer
{

	/**
	 * @ORM\Column(type="text", nullable=false)
	 * @var string
	 */
	private $text;

	/**
	 * @param Trip $trip
	 * @param Question $question
	 * @param string $text
	 */
	public function __construct(Trip $trip, Question $question, string $text)
	{
		parent::__construct($trip, $question);

		$this->text = $text;
	}

	/**
	 * @return string
	 */
	public function getType(): string
	{
		return Question::QUESTION_TYPE_TEXT;
	}

	public function getText(): string
	{
		return $this->text;
	}

	/**
	 * Check if answer matches provided rule
	 * @param string $rule
	 * @return bool
	 */
	public function match(string $rule): bool
	{
		$match = preg_match('/' . $rule . '/', $this->getText());

		return $match !== 0 && $match !== false;
	}
}
