<?php

declare(strict_types=1);

namespace Cleevio\Answers\Entities\Answers;

use Cleevio\Answers\Entities\Answer;
use Cleevio\Countries\Entities\Country;
use Cleevio\Questions\Entities\Question;
use Cleevio\Trips\Entities\Trip;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 */
class CountryAnswer extends Answer
{

	/**
	 * @ORM\ManyToOne(targetEntity="Cleevio\Countries\Entities\Country")
	 * @ORM\JoinColumn(name="country_id", referencedColumnName="id", nullable=false)
	 * @var Country
	 */
	protected $country;


	/**
	 * CountryAnswer constructor.
	 * @param Trip     $trip
	 * @param Question $question
	 * @param Country  $country
	 */
	public function __construct(Trip $trip, Question $question, Country $country)
	{
		parent::__construct($trip, $question);

		$this->country = $country;
	}


	/**
	 * @return string
	 */
	public function getType(): string
	{
		return Question::QUESTION_TYPE_COUNTRY;
	}


	/**
	 * @return Country
	 */
	public function getCountry(): Country
	{
		return $this->country;
	}


	/**
	 * Check if answer matches provided rule
	 * @param string $rule
	 * @return bool
	 */
	public function match(string $rule): bool
	{
		$match = preg_match('/' . $rule . '/', $this->getCountry()->getCode() . "");

		return $match !== 0 && $match !== false;
	}
}
