<?php

declare(strict_types=1);

namespace Cleevio\Answers\Entities\Answers;

use Cleevio\Answers\Entities\Answer;
use Cleevio\Questions\Entities\Question;
use Cleevio\Questions\Entities\Questions\ChoiceQuestion;
use Cleevio\Trips\Entities\Trip;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use InvalidArgumentException;

/**
 * @ORM\Entity()
 */
class ChoiceAnswer extends Answer
{

	/**
	 * @ORM\OneToMany(targetEntity="Cleevio\Answers\Entities\Answers\AnswerOption", mappedBy="answer", cascade={"all"}, orphanRemoval=true)
	 * @var Collection
	 */
	protected $options;

	/**
	 * @param Trip $trip
	 * @param ChoiceQuestion $question
	 * @param array $choices
	 */
	public function __construct(Trip $trip, ChoiceQuestion $question, array $choices)
	{
		parent::__construct($trip, $question);

		$this->options = new ArrayCollection;

		foreach ($choices as $choice) {
			$found = false;

			foreach ($question->getOptions() as $option) {
				if ($choice === $option->getId()) {
					$found = true;
					$this->options->add(new AnswerOption($option, $this));
				}
			}

			if (!$found) {
				throw new InvalidArgumentException;
			}
		}
	}

	/**
	 * @return string
	 */
	public function getType(): string
	{
		return Question::QUESTION_TYPE_CHOICE;
	}

	/**
	 * @return AnswerOption[]
	 */
	public function getOptions(): array
	{
		return $this->options->getValues();
	}

	/**
	 * Check if answer matches provided rule
	 * @param string $rule
	 * @return bool
	 */
	public function match(string $rule): bool
	{
		foreach ($this->getOptions() as $option) {
			$match = preg_match('/' . $rule . '/', $option->getId() . "");

			if ($match !== 0 && $match !== false) {
				return true;
			}
		}

		return false;
	}
}
