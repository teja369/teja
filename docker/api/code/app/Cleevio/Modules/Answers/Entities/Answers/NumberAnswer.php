<?php

declare(strict_types=1);

namespace Cleevio\Answers\Entities\Answers;

use Cleevio\Answers\Entities\Answer;
use Cleevio\Questions\Entities\Question;
use Cleevio\Trips\Entities\Trip;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 */
class NumberAnswer extends Answer
{

	/**
	 * @ORM\Column(type="integer", nullable=false)
	 * @var int
	 */
	private $number;

	/**
	 * @param Trip $trip
	 * @param Question $question
	 * @param int $number
	 */
	public function __construct(Trip $trip, Question $question, int $number)
	{
		parent::__construct($trip, $question);

		$this->number = $number;
	}

	/**
	 * @return string
	 */
	public function getType(): string
	{
		return Question::QUESTION_TYPE_NUMBER;
	}

	public function getNumber(): int
	{
		return $this->number;
	}

	/**
	 * Check if answer matches provided rule
	 * @param string $rule
	 * @return bool
	 */
	public function match(string $rule): bool
	{
		$match = preg_match('/' . $rule . '/', $this->getNumber() . "");

		return $match !== 0 && $match !== false;
	}
}
