<?php

declare(strict_types=1);

namespace Cleevio\Answers\Entities\Answers;

use Cleevio\Answers\Entities\Answer;
use Cleevio\Questions\Entities\Question;
use Cleevio\Trips\Entities\Trip;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 */
class BoolAnswer extends Answer
{

	/**
	 * @ORM\Column(type="boolean", nullable=false)
	 * @var bool
	 */
	private $bool;

	/**
	 * @param Trip $trip
	 * @param Question $question
	 * @param bool $bool
	 */
	public function __construct(Trip $trip, Question $question, bool $bool)
	{
		parent::__construct($trip, $question);

		$this->bool = $bool;
	}

	/**
	 * @return string
	 */
	public function getType(): string
	{
		return Question::QUESTION_TYPE_BOOL;
	}

	public function getBool(): bool
	{
		return $this->bool;
	}

	/**
	 * Check if answer matches provided rule
	 * @param string $rule
	 * @return bool
	 */
	public function match(string $rule): bool
	{
		$match = preg_match('/' . $rule . '/', (int) $this->getBool() . "");

		return $match !== 0 && $match !== false;
	}
}
