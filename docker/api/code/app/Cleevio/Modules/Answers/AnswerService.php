<?php

declare(strict_types=1);

namespace Cleevio\Answers;

use Cleevio\Ansers\Repositories\AnswerConstraints;
use Cleevio\Answers\Entities\Answer;
use Cleevio\Answers\Entities\Answers\BoolAnswer;
use Cleevio\Answers\Entities\Answers\ChoiceAnswer;
use Cleevio\Answers\Entities\Answers\CountryAnswer;
use Cleevio\Answers\Entities\Answers\DateTimeAnswer;
use Cleevio\Answers\Entities\Answers\NumberAnswer;
use Cleevio\Answers\Entities\Answers\TextAnswer;
use Cleevio\Answers\Repositories\IAnswerRepository;
use Cleevio\Answers\Validators\BoolValidator;
use Cleevio\Answers\Validators\ChoiceValidator;
use Cleevio\Answers\Validators\CountryValidator;
use Cleevio\Answers\Validators\DateTimeValidator;
use Cleevio\Answers\Validators\NumberValidator;
use Cleevio\Answers\Validators\TextValidator;
use Cleevio\Countries\ICountryService;
use Cleevio\Questions\Entities\Question;
use Cleevio\Questions\Entities\Questions\BoolQuestion;
use Cleevio\Questions\Entities\Questions\ChoiceQuestion;
use Cleevio\Questions\Entities\Questions\CountryQuestion;
use Cleevio\Questions\Entities\Questions\DateTimeQuestion;
use Cleevio\Questions\Entities\Questions\NumberQuestion;
use Cleevio\Questions\Entities\Questions\TextQuestion;
use Cleevio\Questions\IQuestionService;
use Cleevio\Trips\Entities\Trip;
use DateTime;
use InvalidArgumentException;

class AnswerService implements IAnswerService
{

	/**
	 * @var IQuestionService
	 */
	private $questionService;

	/**
	 * @var IAnswerRepository
	 */
	private $answerRepository;

	/**
	 * @var ICountryService
	 */
	private $countryService;


	/**
	 * @param IQuestionService  $questionService
	 * @param IAnswerRepository $answerRepository
	 * @param ICountryService   $countryService
	 */
	public function __construct(
		IQuestionService $questionService,
		IAnswerRepository $answerRepository,
		ICountryService $countryService
	)
	{
		$this->questionService = $questionService;
		$this->answerRepository = $answerRepository;
		$this->countryService = $countryService;
	}


	/**
	 * @param Trip       $trip
	 * @param Question   $question
	 * @param array|null $dto
	 * @return Answer|null
	 * @throws InvalidAnswerException
	 */
	function initAnswer(Trip $trip, Question $question, ?array $dto): ?Answer
	{
		if ($dto === null) {
			return null;
		}

		if ($question instanceof TextQuestion && $dto['text'] !== null) {
			return new TextAnswer($trip, $question, (string) $dto['text']);
		}

		if ($question instanceof NumberQuestion && $dto['number'] !== null) {
			return new NumberAnswer($trip, $question, (int) $dto['number']);
		}

		if ($question instanceof BoolQuestion && $dto['bool'] !== null) {
			return new BoolAnswer($trip, $question, (bool) $dto['bool']);
		}

		if ($question instanceof ChoiceQuestion && $dto['choice'] !== null) {
			try {
				return new ChoiceAnswer($trip, $question, (array) $dto['choice']);
			} catch (InvalidArgumentException $e) {
				throw new InvalidAnswerException("questions.validation.invalid", [
					'name' => $question->getText(null),
					'group' => $question->getGroup()->getId(),
				]);
			}
		}

		if ($question instanceof DateTimeQuestion && $dto['datetime'] !== null) {
			$datetime = DateTime::createFromFormat($question->getFormat(), $dto['datetime']);

			if ($datetime === false) {
				throw new InvalidAnswerException("questions.validation.invalid", [
					'name' => $question->getText(null),
					'group' => $question->getGroup()->getId(),
				]);
			}

			return new DateTimeAnswer($trip, $question, $datetime);
		}

		if ($question instanceof CountryQuestion && $dto['country'] !== null) {
			$country = $this->countryService->findByCode($dto['country']);

			if ($country === null) {
				throw new InvalidAnswerException("questions.validation.invalid", [
					'name' => $question->getText(null),
					'group' => $question->getGroup()->getId(),
				]);
			}

			return new CountryAnswer($trip, $question, $country);
		}

		return null;
	}


	/**
	 * @param Trip        $trip
	 * @param Question    $question
	 * @param Answer|null $answer
	 * @return Answer|null
	 * @throws InvalidAnswerException
	 */
	function validate(Trip $trip, Question $question, ?Answer $answer): ?Answer
	{
		if ($question instanceof TextQuestion) {
			return (new TextValidator)->validate($trip, $question, $answer instanceof TextAnswer ? $answer : null);
		}

		if ($question instanceof NumberQuestion) {
			return (new NumberValidator)->validate($trip, $question, $answer instanceof NumberAnswer ? $answer : null);
		}

		if ($question instanceof BoolQuestion) {
			return (new BoolValidator)->validate($trip, $question, $answer instanceof BoolAnswer ? $answer : null);
		}

		if ($question instanceof ChoiceQuestion) {
			return (new ChoiceValidator)->validate($trip, $question, $answer instanceof ChoiceAnswer ? $answer : null);
		}

		if ($question instanceof DateTimeQuestion) {
			return (new DateTimeValidator)->validate($trip, $question, $answer instanceof DateTimeAnswer ? $answer : null);
		}

		if ($question instanceof CountryQuestion) {
			return (new CountryValidator)->validate($trip, $question, $answer instanceof CountryAnswer ? $answer : null);
		}

		throw new InvalidArgumentException;
	}


	/**
	 * Get answers for trip
	 * @param Trip $trip      Trip
	 * @param bool $preFilled Include answers for pre-filled questions from previous trip
	 * @return Answer[]
	 */
	function getAnswers(Trip $trip, bool $preFilled): array
	{
		if ($preFilled === false) {
			return $trip->getAnswers();
		}

		$answers = [];

		foreach ($trip->getAnswers() as $answer) {
			$answers[$answer->getQuestion()->getId()] = $answer;
		}

		// Get all required questions for this trip
		$questions = $this->questionService->getQuestionsTrip(null, $trip);

		// Find questions to pre-fill
		$preFill = [];

		foreach ($questions as $question) {
			if (!array_key_exists($question->getId(), $answers) && $question->isPrefilled()) {
				$preFill[] = $question;
			}
		}

		// Pre-fill answers
		foreach ($preFill as $question) {
			$constraints = new AnswerConstraints;
			$constraints->setUser($trip->getUser());
			$constraints->setQuestion($question);
			$constraints->setOrderBy("trip.createdAt");
			$constraints->setOrderDir("desc");

			$answer = $this->answerRepository->findBy($constraints->build());

			if ($answer !== null) {
				$answers[] = $answer;
			}
		}

		return array_values($answers);
	}
}
