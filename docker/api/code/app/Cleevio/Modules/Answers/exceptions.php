<?php

declare(strict_types=1);

namespace Cleevio\Answers;

use Cleevio\Exceptions\ParamException;

class InvalidAnswerException extends ParamException
{

}
