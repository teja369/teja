<?php

declare(strict_types=1);

namespace Cleevio\Questions\Repositories;

use Cleevio\Questions\Entities\QuestionGroup;
use Cleevio\Repository\Repositories\SqlRepository;

class GroupRepository extends SqlRepository implements IGroupRepository
{

	/**
	 * Model class type
	 * @return string
	 */
	public function type(): string
	{
		return QuestionGroup::class;
	}

}
