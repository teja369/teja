<?php

declare(strict_types=1);

namespace Cleevio\Questions\Repositories;

use Cleevio\Questions\Entities\Questions\QuestionOptionCountry;
use Cleevio\Repository\Repositories\SqlRepository;

class QuestionOptionCountryRepository extends SqlRepository implements IQuestionOptionCountryRepository
{

	/**
	 * Model class type
	 * @return string
	 */
	public function type(): string
	{
		return QuestionOptionCountry::class;
	}
}
