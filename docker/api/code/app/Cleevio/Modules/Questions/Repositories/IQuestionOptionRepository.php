<?php

declare(strict_types=1);

namespace Cleevio\Questions\Repositories;

use Cleevio\Questions\Entities\Question;
use Cleevio\Questions\Entities\Questions\QuestionOption;
use Cleevio\Repository\IRepository;

interface IQuestionOptionRepository extends IRepository
{

	/**
	 * @param Question $question
	 * @param string $text
	 * @return QuestionOption|null
	 */
	public function findByText(Question $question, string $text): ?QuestionOption;

}
