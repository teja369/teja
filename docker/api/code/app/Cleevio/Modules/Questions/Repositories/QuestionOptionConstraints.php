<?php

declare(strict_types=1);

namespace Cleevio\Questions\Repositories;

use Cleevio\Countries\Entities\Country;
use Cleevio\Questions\Entities\Question;
use Cleevio\Repository\ConstraintsBuilder;
use Cleevio\Repository\Expressions\Comparison;
use Cleevio\Repository\Expressions\CompositeExpression;
use Cleevio\Repository\Expressions\IsNull;

class QuestionOptionConstraints extends ConstraintsBuilder
{

	/**
	 * @param Country $country
	 * @return QuestionOptionConstraints
	 */
	function setCountry(Country $country): QuestionOptionConstraints
	{
		$this->addCriteria(new CompositeExpression(CompositeExpression::TYPE_OR, [
			new Comparison('countries.country.code', '=', $country->getCode()),
			new IsNull('countries.country'),
		]));

		return $this;
	}


	/**
	 * @param string $search
	 * @return $this
	 */
	function setSearch(string $search): QuestionOptionConstraints
	{
		$this->addCriteria(new Comparison('text', 'CONTAINS', $search));

		return $this;
	}


	/**
	 * @param Question $question
	 * @return QuestionOptionConstraints
	 */
	function setQuestion(Question $question): QuestionOptionConstraints
	{
		$this->addCriteria(new Comparison('question.id', '=', $question->getId()));

		return $this;
	}

}
