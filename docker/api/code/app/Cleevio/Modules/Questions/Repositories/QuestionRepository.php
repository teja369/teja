<?php

declare(strict_types=1);

namespace Cleevio\Questions\Repositories;

use Cleevio\Questions\Entities\Question;
use Cleevio\Repository\Repositories\SqlRepository;
use Doctrine\Common\Collections\Criteria;

class QuestionRepository extends SqlRepository implements IQuestionRepository
{

	/**
	 * Model class type
	 * @return string
	 */
	public function type(): string
	{
		return Question::class;
	}

	/**
	 * @param int[] $ids
	 * @return Question[]
	 */
	public function findByIds(array $ids): array
	{

		$result = $this->getRepository()
			->createQueryBuilder("t")
			->addCriteria(Criteria::create()->where(Criteria::expr()->in("id", $ids)))
			->getQuery()
			->execute();

		return !is_null($result) ? $result : [];
	}
}
