<?php

declare(strict_types=1);

namespace Cleevio\Questions\Repositories;

use Cleevio\Countries\Entities\Country;
use Cleevio\Questions\Entities\QuestionGroup;
use Cleevio\Registrations\Entities\Registration;
use Cleevio\Repository\ConstraintsBuilder;
use Cleevio\Repository\Expressions\Comparison;
use Cleevio\Repository\Expressions\IsNull;

class QuestionConstraints extends ConstraintsBuilder
{

	/**
	 * @param string $text
	 * @return QuestionConstraints
	 */
	function setText(string $text): QuestionConstraints
	{
		$this->addCriteria(new Comparison('text', '=', $text));

		return $this;
	}

	/**
	 * @param string[] $text
	 * @return QuestionConstraints
	 */
	function setTexts(array $text): QuestionConstraints
	{
		$this->addCriteria(new Comparison('text', 'IN', $text));

		return $this;
	}

	/**
	 * @param Registration[] $registrations
	 * @return QuestionConstraints
	 */
	function setRegistrations(array $registrations): QuestionConstraints
	{
		$this->addCriteria(new Comparison('registrations.registration', 'IN', $registrations));

		return $this;
	}

	/**
	 * @param Country $country
	 * @return QuestionConstraints
	 */
	function setCountry(Country $country): QuestionConstraints
	{
		$this->addCriteria(new Comparison('countries.country.code', '=', $country->getCode()));

		return $this;
	}

	/**
	 * @param QuestionGroup $group
	 * @return QuestionConstraints
	 */
	function setGroup(QuestionGroup $group): QuestionConstraints
	{
		$this->addCriteria(new Comparison('group', '=', $group->getId()));

		return $this;
	}

	/**
	 * @param string $search
	 * @return QuestionConstraints
	 */
	function setSearch(string $search): QuestionConstraints
	{
		$this->addCriteria(new Comparison('text', 'CONTAINS', $search));

		return $this;
	}


	/**
	 * @param array $visibility
	 * @return QuestionConstraints
	 */
	function setVisibility(array $visibility): QuestionConstraints
	{
		$this->addCriteria(new Comparison('visibility', 'IN', $visibility));

		return $this;
	}

	/**
	 * @param bool|null $isDeleted
	 * @return QuestionConstraints
	 */
	function setIsDeleted(?bool $isDeleted): QuestionConstraints
	{
		if ($isDeleted !== null) {
			if ($isDeleted) {
				$this->addCriteria(new Comparison('deletedAt', '<>', ''));
			} else {
				$this->addCriteria(new IsNull('deletedAt'));
			}
		}

		return $this;
	}

}
