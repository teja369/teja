<?php

declare(strict_types=1);

namespace Cleevio\Questions\Repositories;

use Cleevio\Repository\IRepository;

interface IQuestionOptionCountryRepository extends IRepository
{

}
