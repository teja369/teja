<?php

declare(strict_types=1);

namespace Cleevio\Questions\Repositories;

use Cleevio\Questions\Entities\Question;
use Cleevio\Repository\IRepository;

interface IQuestionRepository extends IRepository
{

	/**
	 * @param int[] $ids
	 * @return Question[]
	 */
	public function findByIds(array $ids): array;

}
