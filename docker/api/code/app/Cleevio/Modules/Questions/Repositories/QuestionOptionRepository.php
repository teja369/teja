<?php

declare(strict_types=1);

namespace Cleevio\Questions\Repositories;

use Cleevio\Questions\Entities\Question;
use Cleevio\Questions\Entities\Questions\QuestionOption;
use Cleevio\Repository\Repositories\SqlRepository;
use Doctrine\Common\Collections\Criteria;

class QuestionOptionRepository extends SqlRepository implements IQuestionOptionRepository
{

	/**
	 * Model class type
	 * @return string
	 */
	public function type(): string
	{
		return QuestionOption::class;
	}

	/**
	 * @param Question $question
	 * @param string $text
	 * @return QuestionOption|null
	 */
	public function findByText(Question $question, string $text): ?QuestionOption
	{
		$result = $this->getRepository()
			->createQueryBuilder("t")
			->addCriteria(Criteria::create()->where(Criteria::expr()->eq("question", $question->getId())))
			->addCriteria(Criteria::create()->where(Criteria::expr()->eq("text", $text)))
			->setMaxResults(1)
			->getQuery()->execute();

		return is_array($result) && count($result) > 0
			? $result[0]
			: null;
	}
}
