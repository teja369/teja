<?php

declare(strict_types=1);

namespace Cleevio\Questions\Repositories;

use Cleevio\Repository\ConstraintsBuilder;
use Cleevio\Repository\Expressions\Comparison;
use Cleevio\Repository\Expressions\IsNull;

class GroupConstraints extends ConstraintsBuilder
{

	/**
	 * @param string $type
	 * @return GroupConstraints
	 */
	function setType(string $type): GroupConstraints
	{
		$this->addCriteria(new Comparison('type', '=', $type));

		return $this;
	}


	/**
	 * @param bool|null $isDeleted
	 * @return GroupConstraints
	 */
	function setIsDeleted(?bool $isDeleted): GroupConstraints
	{
		if ($isDeleted !== null) {
			if ($isDeleted) {
				$this->addCriteria(new Comparison('deletedAt', '<>', ''));
			} else {
				$this->addCriteria(new IsNull('deletedAt'));
			}
		}

		return $this;
	}
}
