<?php

declare(strict_types=1);

namespace Cleevio\Questions;

use Cleevio\Countries\Entities\Country;
use Cleevio\Hosts\Entities\Host;
use Cleevio\Languages\Entities\Language;
use Cleevio\Questions\Api\DTO\CreateQuestionPostDTO;
use Cleevio\Questions\Api\DTO\QuestionVisibilityPutDTO;
use Cleevio\Questions\Entities\Question;
use Cleevio\Questions\Entities\QuestionGroup;
use Cleevio\Questions\Entities\Questions\QuestionOption;
use Cleevio\Questions\Repositories\QuestionConstraints;
use Cleevio\Registrations\Entities\Registration;
use Cleevio\Trips\Entities\Trip;
use Nette\Http\IRequest;

interface IQuestionService
{

	/**
	 * @param int $id
	 * @return QuestionGroup|null
	 */
	public function getGroup(int $id): ?QuestionGroup;

	/**
	 * @param int $id
	 * @return QuestionOption|null
	 */
	public function getOption(int $id): ?QuestionOption;

	/**
	 * @param Trip|null $trip
	 * @param string|null $type
	 * @return QuestionGroup[]
	 */
	public function getGroups(?Trip $trip, ?string $type): array;

	/**
	 * @param string $name
	 * @param string $type
	 * @return QuestionGroup
	 */
	public function createGroup(string $name, string $type): QuestionGroup;

	/**
	 * @param QuestionGroup $group
	 * @param string $name
	 * @param string $type
	 * @return QuestionGroup
	 */
	public function updateGroup(QuestionGroup $group, string $name, string $type): QuestionGroup;

	/**
	 * Delete group and all of it's questions
	 * @param QuestionGroup $group
	 */
	public function deleteGroup(QuestionGroup $group): void;

	/**
	 * @param int $id
	 * @return Question|null
	 */
	public function getQuestion(int $id): ?Question;

	/**
	 * @param CreateQuestionPostDTO $data
	 * @param QuestionGroup $group
	 * @return Question
	 * @throws QuestionTypeInvalid
	 * @throws QuestionParentNotFound
	 * @throws QuestionParentGroupInvalid
	 * @throws QuestionParentCircular
	 * @throws QuestionTypeInvalid
	 * @throws QuestionUnique
	 * @throws QuestionInvalidText
	 */
	public function createQuestion(CreateQuestionPostDTO $data, QuestionGroup $group): Question;

	/**
	 * @param Question $question
	 * @param CreateQuestionPostDTO $data
	 * @param QuestionGroup $group
	 * @return Question
	 * @throws QuestionTypeInvalid
	 * @throws QuestionParentNotFound
	 * @throws QuestionParentGroupInvalid
	 * @throws QuestionParentCircular
	 * @throws QuestionTypeInvalid
	 * @throws QuestionUnique
	 * @throws QuestionInvalidText
	 */
	public function updateQuestion(Question $question, CreateQuestionPostDTO $data, QuestionGroup $group): Question;

	/**
	 * @param Question $question
	 * @param string $text
	 * @return QuestionOption
	 * @throws QuestionOptionInvalidText
	 */
	public function createOption(Question $question, string $text): QuestionOption;

	/**
	 * @param QuestionOption $option
	 * @param string $text
	 * @param int|null $order
	 * @return QuestionOption
	 * @throws QuestionOptionInvalidText
	 */
	public function updateOption(QuestionOption $option, string $text, ?int $order): QuestionOption;

	/**
	 * @param QuestionOption $option
	 * @return void
	 */
	public function deleteOption(QuestionOption $option): void;

	/**
	 * @param Question $question
	 * @param Country[] $countries
	 * @return Question
	 */
	public function setCountries(Question $question, array $countries): Question;

	/**
	 * @param Question $question
	 * @param array $hosts
	 * @return Question
	 */
	public function setHosts(Question $question, array $hosts): Question;

	/**
	 * @param Question $question
	 * @param Registration[] $registrations
	 * @return Question
	 */
	public function setRegistrations(Question $question, array $registrations): Question;

	/**
	 * @param QuestionOption $option
	 * @param Country[] $countries
	 * @return QuestionOption
	 */
	public function setOptionCountries(QuestionOption $option, array $countries): QuestionOption;

	/**
	 * @param QuestionOption $option
	 * @param Country $country
	 * @return QuestionOption
	 */
	public function deleteOptionCountry(QuestionOption $option, Country $country): QuestionOption;

	/**
	 * @param QuestionGroup|null $group
	 * @param Trip $trip
	 * @return Question[]
	 */
	public function getQuestionsTrip(?QuestionGroup $group, Trip $trip): array;

	/**
	 * @param QuestionGroup|null $group
	 * @param Country|null $country
	 * @param Registration[] $registrations
	 * @param Host|null $host
	 * @param bool $showDisabled
	 * @return Question[]
	 */
	public function getQuestions(?QuestionGroup $group, ?Country $country, array $registrations, ?Host $host, bool $showDisabled): array;

	/**
	 * Retrieve questions by IDs
	 * @param int[] $ids
	 * @return Question[]
	 * @throws QuestionNotFound
	 */
	public function getQuestionsByIds(array $ids): array;

	/**
	 * Delete question and all of it's answers
	 * @param Question $question
	 */
	public function deleteQuestion(Question $question): void;

	/**
	 * Translate question text parameters
	 * @param Question $question
	 * @param Language $language
	 * @param IQuestionTranslation $translation
	 * @return Question
	 */
	public function translateQuestion(Question $question, Language $language, IQuestionTranslation $translation): Question;

	/**
	 * Translate question text parameters
	 * @param QuestionGroup $group
	 * @param Language $language
	 * @param IGroupTranslation $translation
	 * @return QuestionGroup
	 */
	public function translateGroup(QuestionGroup $group, Language $language, IGroupTranslation $translation): QuestionGroup;

	/**
	 * Translate option text parameters
	 * @param QuestionOption $option
	 * @param Language $language
	 * @param IQuestionOptionTranslation $translation
	 * @return QuestionOption
	 */
	public function translateOption(QuestionOption $option, Language $language, IQuestionOptionTranslation $translation): QuestionOption;

	/**
	 * Update constraints depending on query parameters for filtering questions
	 *
	 * @param QuestionConstraints $constraints
	 * @param IRequest $request
	 * @return QuestionConstraints
	 * @throws QuestionGroupNotFound
	 */
	public function queryToConstraints(QuestionConstraints $constraints, IRequest $request): QuestionConstraints;

	/**
	 * Invalidate trips for this question
	 * @param Question $question
	 */
	public function invalidateTrips(Question $question): void;


	/**
	 * Sets visibility for question
	 * @param Question                 $question
	 * @param QuestionVisibilityPutDTO $questionVisibilityPutDTO
	 */
	public function setVisibility(Question $question, QuestionVisibilityPutDTO $questionVisibilityPutDTO): void;

}
