<?php

declare(strict_types=1);

namespace Cleevio\Questions;

/**
 * Interface IQuestionTranslation
 * Getters for all translatable fields in question options
 * @package Cleevio\Questions
 */
interface IQuestionOptionTranslation
{

	function getText(): ?string;

}
