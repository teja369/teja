<?php

declare(strict_types=1);

namespace Cleevio\Questions;

/**
 * Interface IQuestionGroupTranslation
 * Getters for all translatable fields in question groups
 * @package Cleevio\Questions
 */
interface IGroupTranslation
{

	function getName(): ?string;

}
