<?php

declare(strict_types=1);

namespace Cleevio\Questions\Entities\Questions;

use Cleevio\Countries\Entities\Country;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 * @ORM\Table(name="questions_option_country")
 */
class QuestionOptionCountry
{

	/**
	 * @ORM\Id
	 * @ORM\Column(type="integer")
	 * @ORM\GeneratedValue
	 * @var int
	 */
	private $id;

	/**
	 * @ORM\ManyToOne(targetEntity="Cleevio\Countries\Entities\Country", inversedBy="questions")
	 * @ORM\JoinColumn(name="country_id", referencedColumnName="id", nullable=false)
	 * @var Country
	 */
	private $country;

	/**
	 * @ORM\ManyToOne(targetEntity="Cleevio\Questions\Entities\Questions\QuestionOption")
	 * @ORM\JoinColumn(name="option_id", referencedColumnName="id", nullable=false)
	 * @var QuestionOption
	 */
	private $option;

	/**
	 * @param Country $country
	 * @param QuestionOption $option
	 */
	public function __construct(
		Country $country,
		QuestionOption $option
	)
	{
		$this->country = $country;
		$this->option = $option;
	}

	/**
	 * @return int
	 */
	final public function getId(): int
	{
		return $this->id;
	}


	/**
	 * @return Country
	 */
	public function getCountry(): Country
	{
		return $this->country;
	}

	/**
	 * @param Country $country
	 */
	public function setCountry(Country $country): void
	{
		$this->country = $country;
	}

	/**
	 * @return QuestionOption
	 */
	public function getOption(): QuestionOption
	{
		return $this->option;
	}

	/**
	 * @param QuestionOption $option
	 */
	public function setOption(QuestionOption $option): void
	{
		$this->option = $option;
	}

}
