<?php

declare(strict_types=1);

namespace Cleevio\Questions\Entities\Questions;

use Cleevio\Questions\Entities\Question;
use Cleevio\Questions\Entities\QuestionGroup;
use Cleevio\Trips\Entities\Trip;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 */
class ChoiceQuestion extends Question
{
	/**
	 * @ORM\Column(name="multi_choice", type="boolean", nullable=false)
	 * @var bool
	 */
	private $multiChoice;

	/**
	 * @ORM\OneToMany(targetEntity="QuestionOption", mappedBy="question", cascade={"all"}, orphanRemoval=true)
	 * @var Collection
	 */
	protected $options;

	/**
	 * @param string $text
	 * @param QuestionGroup $group
	 */
	public function __construct(string $text, QuestionGroup $group)
	{
		parent::__construct($text, $group);

		$this->multiChoice = false;
		$this->options = new ArrayCollection;
	}

	/**
	 * @return string
	 */
	public function getType(): string
	{
		return self::QUESTION_TYPE_CHOICE;
	}

	/**
	 * @return QuestionOption[]
	 */
	public function getOptions(): array
	{
		return $this->options->getValues();
	}

	/**
	 * @param QuestionOption[] $options
	 */
	public function setOptions(array $options): void
	{
		$this->options = new ArrayCollection($options);
	}

	/**
	 * Get choice question options for provided country
	 * @param Trip|null $trip
	 * @return QuestionOption[]
	 * @deprecated
	 */
	public function getOptionsTrip(?Trip $trip): array
	{
		$filtered = array_filter($this->getOptions(), static function (QuestionOption $option) use ($trip) {
			if ($trip === null) {
				return true;
			}

			if (count($option->getCountries()) === 0) {
				return true;
			}

			foreach ($option->getCountries() as $country) {
				if ($country->getCountry()->getId() === $trip->getCountry()->getId()) {
					return true;
				}
			}

			return false;
		});

		usort($filtered, static function (QuestionOption $o1, QuestionOption $o2) {
			if ($o1->getOrder() === $o2->getOrder()) {
				return 0;
			}

			return $o1->getOrder() < $o2->getOrder()
				? -1
				: 1;
		});

		return $filtered;
	}

	public function isMultiChoice(): bool
	{
		return $this->multiChoice;
	}

	public function setMultiChoice(bool $multiChoice): void
	{
		$this->multiChoice = $multiChoice;
	}
}
