<?php

declare(strict_types=1);

namespace Cleevio\Questions\Entities\Questions;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Translatable\Entity\MappedSuperclass\AbstractPersonalTranslation;

/**
 * @ORM\Entity()
 * @ORM\Table(name="questions_option_translation")
 */
class QuestionOptionTranslation extends AbstractPersonalTranslation
{
	/**
	 * @param string $locale
	 * @param string $field
	 * @param string $value
	 */
	public function __construct($locale, $field, $value)
	{
		$this->setLocale($locale);
		$this->setField($field);
		$this->setContent($value);
	}

	/**
	 * @var QuestionOption
	 * @ORM\ManyToOne(targetEntity="QuestionOption", inversedBy="translations")
	 * @ORM\JoinColumn(name="object_id", referencedColumnName="id", onDelete="CASCADE")
	 */
	protected $object;
}
