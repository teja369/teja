<?php

declare(strict_types=1);

namespace Cleevio\Questions\Entities\Questions;

use Cleevio\Countries\Entities\CountryTranslation;
use Cleevio\Languages\Entities\Language;
use Cleevio\Questions\Entities\Question;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity()
 * @ORM\Table(name="questions_option")
 * @Gedmo\TranslationEntity(class="QuestionOptionTranslation")
 */
class QuestionOption
{
	/**
	 * @ORM\Id
	 * @ORM\Column(type="integer")
	 * @ORM\GeneratedValue
	 * @var int
	 */
	private $id;

	/**
	 * @Gedmo\SortableGroup()
	 * @ORM\ManyToOne(targetEntity="Cleevio\Questions\Entities\Question", cascade={"persist"})
	 * @ORM\JoinColumn(name="question_id", referencedColumnName="id", nullable=false)
	 * @var Question
	 */
	protected $question;

	/**
	 * @Gedmo\Translatable
	 * @ORM\Column(name="text", type="string", nullable=false)
	 * @var string
	 */
	private $text;

	/**
	 * @ORM\OneToMany(targetEntity="QuestionOptionTranslation", mappedBy="object", cascade={"all"}, orphanRemoval=true)
	 * @var ArrayCollection
	 */
	private $translations;

	/**
	 * @ORM\OneToMany(targetEntity="Cleevio\Questions\Entities\Questions\QuestionOptionCountry", mappedBy="option", cascade={"all"}, orphanRemoval=true)
	 * @var Collection
	 */
	protected $countries;

	/**
	 * @Gedmo\SortablePosition()
	 * @ORM\Column(name="`order`", type="integer", nullable=false)
	 * @var int
	 */
	private $order;

	/**
	 * @param Question $question
	 * @param string $text
	 */
	public function __construct(Question $question, string $text)
	{
		$this->question = $question;
		$this->text = $text;
		$this->order = 0;
		$this->translations = new ArrayCollection;
		$this->countries = new ArrayCollection;
	}

	public function getId(): int
	{
		return $this->id;
	}

	public function getText(?string $lang): string
	{
		return $this->getTranslation('text', $lang) ?? $this->text;
	}

	public function setText(string $text): void
	{
		$this->text = $text;
	}

	public function getQuestion(): Question
	{
		return $this->question;
	}

	/**
	 * @return QuestionOptionCountry[]
	 */
	public function getCountries(): array
	{
		return $this->countries->getValues();
	}

	/**
	 * @param QuestionOptionCountry[] $countries
	 */
	public function setCountries(array $countries): void
	{
		$this->countries = new ArrayCollection($countries);
	}

	/**
	 * @return int
	 */
	public function getOrder(): int
	{
		return $this->order;
	}


	/**
	 * @param int $order
	 */
	public function setOrder(int $order): void
	{
		$this->order = $order;
	}

	/**
	 * @param string $name
	 * @param string|null $lang
	 * @return string|null
	 */
	private function getTranslation(string $name, ?string $lang): ?string
	{
		foreach ($this->getTranslations() as $translation) {
			if ($translation->getLocale() === $lang && $translation->getField() === $name) {
				return $translation->getContent();
			}
		}

		return null;
	}

	/**
	 * @return CountryTranslation[]
	 */
	public function getTranslations(): array
	{
		return $this->translations->getValues();
	}

	/**
	 * @param QuestionOptionTranslation $t
	 */
	public function addTranslation(QuestionOptionTranslation $t): void
	{
		foreach ($this->translations as $translation) {
			if ($translation->getLocale() === $t->getLocale() && $translation->getField() === $t->getField()) {
				$translation->setContent($t->getContent());
				$t->setObject($this);

				return;
			}
		}

		$this->translations[] = $t;
		$t->setObject($this);
	}

	/**
	 * @param string $field
	 * @param Language $language
	 */
	public function clearTranslation(string $field, Language $language): void
	{
		foreach ($this->translations as $translation) {
			if ($translation->getLocale() === $language->getCode() && $translation->getField() === $field) {
				$this->translations->removeElement($translation);

				return;
			}
		}
	}
}
