<?php

declare(strict_types=1);

namespace Cleevio\Questions\Entities\Questions;

use Cleevio\Questions\Entities\Question;
use Cleevio\Questions\Entities\QuestionGroup;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 */
class NumberQuestion extends Question
{

	/**
	 * @ORM\Column(type="integer", nullable=true)
	 * @var int|null
	 */
	protected $minValue;

	/**
	 * @ORM\Column(type="integer", nullable=true)
	 * @var int|null
	 */
	protected $maxValue;

	/**
	 * @param string $text
	 * @param QuestionGroup $group
	 */
	public function __construct(string $text, QuestionGroup $group)
	{
		parent::__construct($text, $group);
	}

	/**
	 * @return string
	 */
	public function getType(): string
	{
		return self::QUESTION_TYPE_NUMBER;
	}

	public function getMinValue(): ?int
	{
		return $this->minValue;
	}

	public function setMinValue(?int $minValue): void
	{
		$this->minValue = $minValue;
	}

	public function getMaxValue(): ?int
	{
		return $this->maxValue;
	}

	public function setMaxValue(?int $maxValue): void
	{
		$this->maxValue = $maxValue;
	}

}
