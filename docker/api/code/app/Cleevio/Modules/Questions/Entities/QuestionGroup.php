<?php

declare(strict_types=1);

namespace Cleevio\Questions\Entities;

use Cleevio\Languages\Entities\Language;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\SoftDeleteable\Traits\SoftDeleteableEntity;

/**
 * @ORM\Entity()
 * @ORM\Table(name="questions_group")
 */
class QuestionGroup
{
	public const QUESTION_GROUP_TYPE_PURPOSE = 'purpose';
	public const QUESTION_GROUP_TYPE_DETAIL = 'detail';

	use SoftDeleteableEntity;

	/**
	 * @ORM\Id
	 * @ORM\Column(type="integer")
	 * @ORM\GeneratedValue
	 * @var int
	 */
	private $id;

	/**
	 * @Gedmo\Translatable
	 * @ORM\Column(name="name", type="string", nullable=false)
	 * @var string
	 */
	private $name;

	/**
	 * @ORM\Column(name="type", type="string", nullable=false)
	 * @var string
	 */
	private $type;

	/**
	 * @ORM\OneToMany(targetEntity="Question", mappedBy="group", cascade={"persist", "remove"}, orphanRemoval=true)
	 * @var ArrayCollection
	 */
	private $questions;

	/**
	 * @ORM\OneToMany(targetEntity="QuestionGroupTranslation", mappedBy="object", cascade={"all"}, orphanRemoval=true)
	 * @var ArrayCollection
	 */
	private $translations;

	/**
	 * @param string $name
	 * @param string $type
	 */
	public function __construct(string $name, string $type)
	{
		$this->setName($name);
		$this->setType($type);
		$this->questions = new ArrayCollection;
		$this->translations = new ArrayCollection;
	}

	/**
	 * @return int
	 */
	final public function getId(): int
	{
		return $this->id;
	}

	/**
	 * @param string|null $lang
	 * @return string
	 */
	public function getName(?string $lang): string
	{
		return $this->getTranslation('name', $lang) ?? $this->name;
	}

	/**
	 * @param string $name
	 */
	public function setName(string $name): void
	{
		$this->name = $name;
	}

	/**
	 * @return string
	 */
	public function getType(): string
	{
		return $this->type;
	}

	/**
	 * @param string $type
	 */
	public function setType(string $type): void
	{
		if (!in_array($type, [self::QUESTION_GROUP_TYPE_PURPOSE, self::QUESTION_GROUP_TYPE_DETAIL], true)) {
			throw new \InvalidArgumentException("Invalid group type");
		}

		$this->type = $type;
	}

	/**
	 * @return Question[]
	 */
	public function getQuestions(): array
	{
		return $this->questions->getValues();
	}

	/**
	 * @param Question[] $questions
	 */
	public function setQuestions(array $questions): void
	{
		$this->questions = new ArrayCollection($questions);
	}

	/**
	 * @param string $name
	 * @param string|null $lang
	 * @return string|null
	 */
	private function getTranslation(string $name, ?string $lang): ?string
	{
		foreach ($this->getTranslations() as $translation) {
			if ($translation->getLocale() === $lang && $translation->getField() === $name) {
				return $translation->getContent();
			}
		}

		return null;
	}

	/**
	 * @return QuestionTranslation[]
	 */
	public function getTranslations(): array
	{
		return $this->translations->getValues();
	}

	/**
	 * @param QuestionGroupTranslation $t
	 */
	public function addTranslation(QuestionGroupTranslation $t): void
	{
		foreach ($this->translations as $translation) {
			if ($translation->getLocale() === $t->getLocale() && $translation->getField() === $t->getField()) {
				$translation->setContent($t->getContent());
				$t->setObject($this);

				return;
			}
		}

		$this->translations[] = $t;
		$t->setObject($this);
	}

	/**
	 * @param string $field
	 * @param Language $language
	 */
	public function clearTranslation(string $field, Language $language): void
	{
		foreach ($this->translations as $translation) {
			if ($translation->getLocale() === $language->getCode() && $translation->getField() === $field) {
				$this->translations->removeElement($translation);

				return;
			}
		}
	}

}
