<?php

declare(strict_types=1);

namespace Cleevio\Questions\Entities\Questions;

use Cleevio\Questions\Entities\Question;
use Cleevio\Questions\Entities\QuestionGroup;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 */
class DateTimeQuestion extends Question
{

	public const QUESTION_DATETIME_VARIANT_DATE = "date";
	public const QUESTION_DATETIME_VARIANT_TIME = "time";
	public const QUESTION_DATETIME_VARIANT_DATETIME = "datetime";

	/**
	 * Datepicker variant
	 *
	 * @ORM\Column(name="variant", type="string", nullable=true)
	 * @var string
	 */
	private $variant;

	/**
	 * @param string $text
	 * @param QuestionGroup $group
	 */
	public function __construct(string $text, QuestionGroup $group)
	{
		parent::__construct($text, $group);

		$this->variant = self::QUESTION_DATETIME_VARIANT_DATE;
	}

	/**
	 * @return string
	 */
	public function getType(): string
	{
		return self::QUESTION_TYPE_DATETIME;
	}

	/**
	 * @return string
	 */
	public function getVariant(): string
	{
		return $this->variant;
	}

	/**
	 * @param string $variant
	 */
	public function setVariant(string $variant): void
	{
		$this->variant = $variant;
	}

	/**
	 * Return date format depending on question variant
	 * @return string
	 */
	public function getFormat(): string
	{
		switch ($this->variant) {
			case self::QUESTION_DATETIME_VARIANT_DATE:
				return "Y-m-d";
			case self::QUESTION_DATETIME_VARIANT_TIME:
				return "H:i:s";
			case self::QUESTION_DATETIME_VARIANT_DATETIME:
				return "Y-m-d, H:i:s";
		}

		return "Y-m-d";
	}
}
