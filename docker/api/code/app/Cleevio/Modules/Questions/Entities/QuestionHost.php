<?php

declare(strict_types=1);

namespace Cleevio\Questions\Entities;

use Cleevio\Hosts\Entities\Host;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 * @ORM\Table(name="questions_host")
 */
class QuestionHost
{

	/**
	 * @ORM\Id
	 * @ORM\Column(type="integer")
	 * @ORM\GeneratedValue
	 * @var int
	 */
	private $id;

	/**
	 * @ORM\ManyToOne(targetEntity="Cleevio\Hosts\Entities\Host", inversedBy="questions")
	 * @ORM\JoinColumn(name="host_id", referencedColumnName="id", nullable=false)
	 * @var Host
	 */
	private $host;

	/**
	 * @ORM\ManyToOne(targetEntity="Cleevio\Questions\Entities\Question")
	 * @ORM\JoinColumn(name="question_id", referencedColumnName="id", nullable=false)
	 * @var Question
	 */
	private $question;


	/**
	 * QuestionsHost constructor.
	 * @param Host     $host
	 * @param Question $question
	 */
	public function __construct(
		Host $host,
		Question $question
	)
	{
		$this->host = $host;
		$this->question = $question;
	}


	/**
	 * @return int
	 */
	final public function getId(): int
	{
		return $this->id;
	}


	/**
	 * @return Host
	 */
	public function getHost(): Host
	{
		return $this->host;
	}


	/**
	 * @param Host $host
	 */
	public function setHost(Host $host): void
	{
		$this->host = $host;
	}


	/**
	 * @return Question
	 */
	public function getQuestion(): Question
	{
		return $this->question;
	}


	/**
	 * @param Question $question
	 */
	public function setQuestion(Question $question): void
	{
		$this->question = $question;
	}

}
