<?php

declare(strict_types=1);

namespace Cleevio\Questions\Entities\Questions;

use Cleevio\Questions\Entities\Question;
use Cleevio\Questions\Entities\QuestionGroup;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 */
class BoolQuestion extends Question
{

	/**
	 * @param string $text
	 * @param QuestionGroup $group
	 */
	public function __construct(string $text, QuestionGroup $group)
	{
		parent::__construct($text, $group);
	}

	/**
	 * @return string
	 */
	public function getType(): string
	{
		return self::QUESTION_TYPE_BOOL;
	}

}
