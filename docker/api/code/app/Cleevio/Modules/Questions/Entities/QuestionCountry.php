<?php

declare(strict_types=1);

namespace Cleevio\Questions\Entities;

use Cleevio\Countries\Entities\Country;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 * @ORM\Table(name="questions_country")
 */
class QuestionCountry
{

	/**
	 * @ORM\Id
	 * @ORM\Column(type="integer")
	 * @ORM\GeneratedValue
	 * @var int
	 */
	private $id;

	/**
	 * @ORM\ManyToOne(targetEntity="Cleevio\Countries\Entities\Country", inversedBy="questions")
	 * @ORM\JoinColumn(name="country_id", referencedColumnName="id", nullable=false)
	 * @var Country
	 */
	private $country;

	/**
	 * @ORM\ManyToOne(targetEntity="Cleevio\Questions\Entities\Question")
	 * @ORM\JoinColumn(name="question_id", referencedColumnName="id", nullable=false)
	 * @var Question
	 */
	private $question;



	public function __construct(
		Country $country,
		Question $question
	)
	{
		$this->country = $country;
		$this->question = $question;
	}


	/**
	 * @return int
	 */
	final public function getId(): int
	{
		return $this->id;
	}


	/**
	 * @return Country
	 */
	public function getCountry(): Country
	{
		return $this->country;
	}


	/**
	 * @return Question
	 */
	public function getQuestion(): Question
	{
		return $this->question;
	}


	/**
	 * @param Country $country
	 */
	public function setCountry(Country $country): void
	{
		$this->country = $country;
	}


	/**
	 * @param Question $question
	 */
	public function setQuestion(Question $question): void
	{
		$this->question = $question;
	}

}
