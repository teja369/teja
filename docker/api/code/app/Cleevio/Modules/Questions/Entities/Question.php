<?php

declare(strict_types=1);

namespace Cleevio\Questions\Entities;

use Cleevio\Languages\Entities\Language;
use Cleevio\Registrations\Entities\RegistrationQuestion;
use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\SoftDeleteable\Traits\SoftDeleteableEntity;
use Gedmo\Timestampable\Traits\TimestampableEntity;

/**
 * @ORM\Entity()
 * @ORM\Table(name="questions")
 * @Gedmo\TranslationEntity(class="QuestionTranslation")
 * @ORM\InheritanceType("SINGLE_TABLE")
 * @ORM\DiscriminatorColumn(name="type", type="string")
 * @ORM\DiscriminatorMap({
 *     "base"       = "Question",
 *     "text"       = "Cleevio\Questions\Entities\Questions\TextQuestion",
 *     "bool"       = "Cleevio\Questions\Entities\Questions\BoolQuestion",
 *     "number"     = "Cleevio\Questions\Entities\Questions\NumberQuestion",
 *     "choice"     = "Cleevio\Questions\Entities\Questions\ChoiceQuestion",
 *     "datetime"   = "Cleevio\Questions\Entities\Questions\DateTimeQuestion",
 *     "country"   = "Cleevio\Questions\Entities\Questions\CountryQuestion"
 * })
 */
abstract class Question
{

	public const QUESTION_TYPE_TEXT = "text";
	public const QUESTION_TYPE_BOOL = "bool";
	public const QUESTION_TYPE_NUMBER = "number";
	public const QUESTION_TYPE_CHOICE = "choice";
	public const QUESTION_TYPE_DATETIME = "datetime";
	public const QUESTION_TYPE_COUNTRY = "country";
	public const QUESTION_VISIBILITY_ENABLED = 'enabled';
	public const QUESTION_VISIBILITY_DISABLED = 'disabled';
	public const QUESTION_VISIBILITY_PREVIEW = 'preview';

	use SoftDeleteableEntity;
	use TimestampableEntity;

	/**
	 * @ORM\Id
	 * @ORM\Column(type="integer")
	 * @ORM\GeneratedValue
	 * @var int
	 */
	private $id;

	/**
	 * @Gedmo\Translatable
	 * @ORM\Column(name="text", type="string", nullable=false)
	 * @var string
	 */
	private $text;

	/**
	 * @ORM\Column(name="start_date", type="datetime", nullable=true)
	 * @var DateTime|null
	 */
	private $startDate;

	/**
	 * @ORM\Column(name="end_date", type="datetime", nullable=true)
	 * @var DateTime|null
	 */
	private $endDate;

	/**
	 * @ORM\ManyToOne(targetEntity="Cleevio\Questions\Entities\Question", inversedBy="children")
	 * @ORM\JoinColumn(name="parent_id", referencedColumnName="id", nullable=true)
	 * @var Question|null
	 */
	private $parent;

	/**
	 * Parent REGEXP rule
	 * @ORM\Column(name="parent_rule", type="string", nullable=true)
	 * @var string|null
	 */
	private $parentRule;

	/**
	 * @ORM\Column(name="help", type="string", nullable=true)
	 * @var string|null
	 */
	private $help;

	/**
	 * @ORM\Column(name="placeholder", type="string", nullable=true)
	 * @var string|null
	 */
	private $placeholder;

	/**
	 * @ORM\Column(name="required", type="boolean", nullable=false)
	 * @var bool
	 */
	private $required;

	/**
	 * Validation REGEXP rule
	 * @ORM\Column(name="validation", type="string", nullable=true)
	 * @var string|null
	 */
	private $validation;

	/**
	 * @ORM\Column(name="validation_help", type="string", nullable=true)
	 * @var string|null
	 */
	private $validationHelp;

	/**
	 * @Gedmo\SortablePosition()
	 * @ORM\Column(name="`order`", type="integer", nullable=false)
	 * @var int
	 */
	private $order;

	/**
	 * @ORM\Column(name="is_prefilled", type="boolean", nullable=false)
	 * @var bool
	 */
	private $isPrefilled;


	/**
	 * @ORM\Column(name="visibility", columnDefinition="enum('enabled', 'disabled', 'preview')")
	 * @var string
	 */
	private $visibility;

	/**
	 * @Gedmo\SortableGroup()
	 * @ORM\ManyToOne(targetEntity="Cleevio\Questions\Entities\QuestionGroup", inversedBy="questions")
	 * @ORM\JoinColumn(name="group_id", referencedColumnName="id", nullable=false)
	 * @var QuestionGroup
	 */
	private $group;

	/**
	 * @ORM\OneToMany(targetEntity="QuestionTranslation", mappedBy="object", cascade={"all"}, orphanRemoval=true)
	 * @var ArrayCollection
	 */
	private $translations;

	/**
	 * @ORM\OneToMany(targetEntity="Cleevio\Questions\Entities\QuestionCountry", mappedBy="question", cascade={"all"}, orphanRemoval=true)
	 * @var Collection
	 */
	protected $countries;

	/**
	 * @ORM\OneToMany(targetEntity="Cleevio\Questions\Entities\QuestionHost", mappedBy="question", cascade={"all"}, orphanRemoval=true)
	 * @var Collection
	 */
	protected $hosts;

	/**
	 * @ORM\OneToMany(targetEntity="Cleevio\Registrations\Entities\RegistrationQuestion", mappedBy="question", cascade={"all"}, orphanRemoval=true)
	 * @var Collection
	 */
	protected $registrations;

	/**
	 * @ORM\OneToMany(targetEntity="Cleevio\Questions\Entities\Question", mappedBy="parent", cascade={"all"}, orphanRemoval=true)
	 * @var Collection
	 */
	protected $children;

	/**
	 * Question constructor.
	 * @param string $text
	 * @param QuestionGroup $group
	 */
	public function __construct(string $text, QuestionGroup $group)
	{
		$this->text = $text;
		$this->required = false;
		$this->order = 0;
		$this->isPrefilled = false;
		$this->visibility = self::QUESTION_VISIBILITY_DISABLED;
		$this->group = $group;
		$this->translations = new ArrayCollection;
		$this->countries = new ArrayCollection;
		$this->hosts = new ArrayCollection;
		$this->children = new ArrayCollection;
		$this->registrations = new ArrayCollection;
	}


	/**
	 * @return int
	 */
	final public function getId(): int
	{
		return $this->id;
	}

	/**
	 * Check if the question has been persisted
	 * @return bool
	 */
	public function hasId(): bool
	{
		return $this->id !== null;
	}


	/**
	 * @return string
	 */
	abstract public function getType(): string;

	/**
	 * @param string|null $lang
	 * @return string
	 */
	public function getText(?string $lang): string
	{
		return $this->getTranslation('text', $lang) ?? $this->text;
	}

	/**
	 * @param string $text
	 */
	public function setText(string $text): void
	{
		$this->text = $text;
	}


	/**
	 * @return DateTime|null
	 */
	public function getStartDate(): ?DateTime
	{
		return $this->startDate;
	}


	/**
	 * @param DateTime|null $startDate
	 */
	public function setStartDate(?DateTime $startDate): void
	{
		$this->startDate = $startDate;
	}


	/**
	 * @return DateTime|null
	 */
	public function getEndDate(): ?DateTime
	{
		return $this->endDate;
	}


	/**
	 * @param DateTime|null $endDate
	 */
	public function setEndDate(?DateTime $endDate): void
	{
		$this->endDate = $endDate;
	}

	/**
	 * @return Question
	 */
	public function getParent(): ?Question
	{
		return $this->parent;
	}

	/**
	 * @param Question $parent
	 */
	public function setParent(?Question $parent): void
	{
		$this->parent = $parent;
	}

	/**
	 * @return string|null
	 */
	public function getParentRule(): ?string
	{
		return $this->parentRule;
	}

	/**
	 * @param string|null $parentRule
	 */
	public function setParentRule(?string $parentRule): void
	{
		$this->parentRule = $parentRule;
	}

	/**
	 * @return Question[]
	 */
	public function getChildren(): array
	{
		return $this->children->getValues();
	}

	/**
	 * @param string|null $lang
	 * @return string|null
	 */
	public function getHelp(?string $lang): ?string
	{
		return $this->getTranslation('help', $lang) ?? $this->help;
	}


	/**
	 * @param string|null $help
	 */
	public function setHelp(?string $help): void
	{
		$this->help = $help;
	}

	/**
	 * @param string|null $lang
	 * @return string|null
	 */
	public function getPlaceholder(?string $lang): ?string
	{
		return $this->getTranslation('placeholder', $lang) ?? $this->placeholder;
	}


	/**
	 * @param string|null $placeholder
	 */
	public function setPlaceholder(?string $placeholder): void
	{
		$this->placeholder = $placeholder;
	}

	/**
	 * @return bool
	 */
	public function isRequired(): bool
	{
		return $this->required;
	}

	/**
	 * @param bool $required
	 */
	public function setRequired(bool $required): void
	{
		$this->required = $required;
	}

	/**
	 * @return string|null
	 */
	public function getValidation(): ?string
	{
		return $this->validation;
	}

	/**
	 * @param string|null $validation
	 */
	public function setValidation(?string $validation): void
	{
		$this->validation = $validation;
	}

	/**
	 * @param string|null $lang
	 * @return string|null
	 */
	public function getValidationHelp(?string $lang): ?string
	{
		return $this->getTranslation('validationHelp', $lang) ?? $this->validationHelp;
	}


	/**
	 * @param string|null $help
	 */
	public function setValidationHelp(?string $help): void
	{
		$this->validationHelp = $help;
	}

	/**
	 * @return int
	 */
	public function getOrder(): int
	{
		return $this->order;
	}


	/**
	 * @param int $order
	 */
	public function setOrder(int $order): void
	{
		$this->order = $order;
	}


	/**
	 * @return bool
	 */
	public function isPrefilled(): bool
	{
		return $this->isPrefilled;
	}


	/**
	 * @param bool $isPrefilled
	 */
	public function setIsPrefilled(bool $isPrefilled): void
	{
		$this->isPrefilled = $isPrefilled;
	}

	/**
	 * @param string $name
	 * @param string|null $lang
	 * @return string|null
	 */
	private function getTranslation(string $name, ?string $lang): ?string
	{
		foreach ($this->getTranslations() as $translation) {
			if ($translation->getLocale() === $lang && $translation->getField() === $name) {
				return $translation->getContent();
			}
		}

		return null;
	}

	/**
	 * @return QuestionTranslation[]
	 */
	public function getTranslations(): array
	{
		return $this->translations->getValues();
	}

	/**
	 * @param QuestionTranslation $t
	 */
	public function addTranslation(QuestionTranslation $t): void
	{
		foreach ($this->translations as $translation) {
			if ($translation->getLocale() === $t->getLocale() && $translation->getField() === $t->getField()) {
				$translation->setContent($t->getContent());
				$t->setObject($this);

				return;
			}
		}

		$this->translations[] = $t;
		$t->setObject($this);
	}

	/**
	 * @param string $field
	 * @param Language $language
	 */
	public function clearTranslation(string $field, Language $language): void
	{
		foreach ($this->translations as $translation) {
			if ($translation->getLocale() === $language->getCode() && $translation->getField() === $field) {
				$this->translations->removeElement($translation);

				return;
			}
		}
	}

	/**
	 * @return QuestionCountry[]
	 */
	public function getCountries(): array
	{
		return $this->countries->getValues();
	}

	/**
	 * @param QuestionCountry[] $countries
	 */
	public function setCountries(array $countries): void
	{
		$this->countries = new ArrayCollection($countries);
	}

	/**
	 * @return QuestionHost[]
	 */
	public function getHosts(): array
	{
		return $this->hosts->getValues();
	}

	/**
	 * @param QuestionHost[] $hosts
	 */
	public function setHosts(array $hosts): void
	{
		$this->hosts = new ArrayCollection($hosts);
	}


	/**
	 * @return RegistrationQuestion[]
	 */
	public function getRegistrations(): array
	{
		return $this->registrations->getValues();
	}

	/**
	 * @param RegistrationQuestion[] $registrations
	 */
	public function setRegistrations(array $registrations): void
	{
		$this->registrations = new ArrayCollection($registrations);
	}

	/**
	 * @return QuestionGroup
	 */
	public function getGroup(): QuestionGroup
	{
		return $this->group;
	}

	/**
	 * @param QuestionGroup $group
	 */
	public function setGroup(QuestionGroup $group): void
	{
		$this->group = $group;
	}


	/**
	 * @return string
	 */
	public function getVisibility(): string
	{
		return $this->visibility;
	}


	/**
	 * @param string $visibility
	 */
	public function setVisibility(string $visibility): void
	{
		$this->visibility = $visibility;
	}

}
