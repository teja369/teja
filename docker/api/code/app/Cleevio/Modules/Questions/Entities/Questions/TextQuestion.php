<?php

declare(strict_types=1);

namespace Cleevio\Questions\Entities\Questions;

use Cleevio\Questions\Entities\Question;
use Cleevio\Questions\Entities\QuestionGroup;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 */
class TextQuestion extends Question
{

	/**
	 * @ORM\Column(type="integer", nullable=true)
	 * @var int|null
	 */
	protected $minLength;

	/**
	 * @ORM\Column(type="integer", nullable=true)
	 * @var int|null
	 */
	protected $maxLength;

	/**
	 * @param string $text
	 * @param QuestionGroup $group
	 */
	public function __construct(string $text, QuestionGroup $group)
	{
		parent::__construct($text, $group);
	}

	/**
	 * @return string
	 */
	public function getType(): string
	{
		return self::QUESTION_TYPE_TEXT;
	}

	public function getMinLength(): ?int
	{
		return $this->minLength;
	}

	public function setMinLength(?int $minLength): void
	{
		$this->minLength = $minLength;
	}

	public function getMaxLength(): ?int
	{
		return $this->maxLength;
	}

	public function setMaxLength(?int $maxLength): void
	{
		$this->maxLength = $maxLength;
	}

}
