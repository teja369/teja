<?php

declare(strict_types=1);

namespace Cleevio\Questions;

use Cleevio\Exceptions\ParamException;

class QuestionNotFound extends ParamException
{

}

class QuestionGroupNotFound extends ParamException
{

}

class QuestionTypeInvalid extends ParamException
{

}

class QuestionParentNotFound extends ParamException
{

}

class QuestionParentGroupInvalid extends ParamException
{

}

class QuestionParentCircular extends ParamException
{

}

class QuestionInvalidText extends ParamException
{

}

class QuestionOptionInvalidText extends ParamException
{

}

class QuestionUnique extends ParamException
{

}
