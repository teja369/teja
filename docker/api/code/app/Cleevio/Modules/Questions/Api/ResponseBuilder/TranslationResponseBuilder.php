<?php

declare(strict_types=1);

namespace Cleevio\Questions\Api\Response;

use Cleevio\Api\Translatable\SimpleResponse;
use Cleevio\Questions\Entities\Question;
use Cleevio\Questions\IQuestionTranslation;

final class TranslationResponseBuilder extends SimpleResponse implements IQuestionTranslation
{

	/**
	 * @var Question
	 */
	private $data;

	/**
	 * QuestionResponseBuilder constructor.
	 * @param Question $question
	 * @param string|null $lang
	 */
	public function __construct(Question $question, ?string $lang)
	{
		parent::__construct($lang);

		$this->data = $question;
	}

	/**
	 * @return array
	 */
	protected function data(): array
	{
		return [
			'text' => $this->getText(),
			'help' => $this->getHelp(),
			'placeholder' => $this->getPlaceholder(),
			'validationHelp' => $this->getValidationHelp(),
		];
	}

	function getText(): ?string
	{
		return $this->data->getText($this->getLang());
	}

	function getHelp(): ?string
	{
		return $this->data->getHelp($this->getLang());
	}

	function getPlaceholder(): ?string
	{
		return $this->data->getPlaceholder($this->getLang());
	}

	function getValidationHelp(): ?string
	{
		return $this->data->getValidationHelp($this->getLang());
	}
}
