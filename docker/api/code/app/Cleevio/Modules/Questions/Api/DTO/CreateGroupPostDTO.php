<?php

declare(strict_types=1);

namespace Cleevio\Questions\Api\DTO;

use Cleevio\RestApi\Command\ICommandDTO;

class CreateGroupPostDTO implements ICommandDTO
{

	/**
	 * @var string
	 */
	protected $name;

	/**
	 * @var string
	 */
	protected $type;


	public function __construct(
		string $name,
		string $type
	)
	{
		$this->name = $name;
		$this->type = $type;
	}

	public static function fromRequest($data): ICommandDTO
	{
		return new static(
			$data->name,
			$data->type
		);
	}

	public function getName(): string
	{
		return $this->name;
	}

	public function getType(): string
	{
		return $this->type;
	}

}

