<?php

declare(strict_types=1);

namespace Cleevio\Questions\Api\DTO;

use Cleevio\RestApi\Command\ICommandDTO;

class CreateOptionPutDTO implements ICommandDTO
{

	/**
	 * @var string
	 */
	protected $text;

	/**
	 * @var int|null
	 */
	protected $order;

	/**
	 * @param string $text
	 * @param int|null $order
	 */
	public function __construct(
		string $text,
		?int $order
	)
	{
		$this->text = $text;
		$this->order = $order;
	}

	public static function fromRequest($data): ICommandDTO
	{
		return new static(
			$data->text,
			property_exists($data, 'order') ? $data->order : null
		);
	}

	/**
	 * @return string
	 */
	public function getText(): string
	{
		return $this->text;
	}

	/**
	 * @return int|null
	 */
	public function getOrder(): ?int
	{
		return $this->order;
	}
}

