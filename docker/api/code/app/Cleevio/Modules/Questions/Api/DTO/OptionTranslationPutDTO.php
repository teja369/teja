<?php

declare(strict_types=1);

namespace Cleevio\Questions\Api\DTO;

use Cleevio\Questions\IQuestionOptionTranslation;
use Cleevio\RestApi\Command\ICommandDTO;

class OptionTranslationPutDTO implements ICommandDTO, IQuestionOptionTranslation
{
	/**
	 * @var string|null
	 */
	protected $text;


	public function __construct(
		?string $text
	)
	{
		$this->text = $text;
	}

	public static function fromRequest($data): ICommandDTO
	{
		return new static(
			property_exists($data, 'text') ? $data->text : null
		);
	}

	public function getText(): ?string
	{
		return $this->text;
	}
}
