<?php

declare(strict_types=1);

namespace Cleevio\Questions\Api\DTO;

use Cleevio\RestApi\Command\ICommandDTO;

class QuestionVisibilityPutDTO implements ICommandDTO
{

	/**
	 * @var string
	 */
	private $visibility;


	/**
	 * QuestionVisibilityPutDTO constructor.
	 * @param string $visibility
	 */
	public function __construct(string $visibility)
	{
		$this->visibility = $visibility;
	}

	public static function fromRequest($data): ICommandDTO
	{
		return new static(
			$data->visibility
		);
	}


	/**
	 * @return string
	 */
	public function getVisibility(): string
	{
		return $this->visibility;
	}

}

