<?php

declare(strict_types=1);

namespace Cleevio\Questions\Api\Response\Questions;

use Cleevio\Api\Translatable\SimpleResponse;
use Cleevio\Questions\Entities\Questions\QuestionOption;
use Cleevio\Questions\IQuestionOptionTranslation;

final class QuestionOptionTranslationResponseBuilder extends SimpleResponse implements IQuestionOptionTranslation
{

	/**
	 * @var QuestionOption
	 */
	private $data;

	/**
	 * QuestionResponseBuilder constructor.
	 * @param QuestionOption $option
	 * @param string|null $lang
	 */
	public function __construct(QuestionOption $option, ?string $lang)
	{
		parent::__construct($lang);

		$this->data = $option;
	}

	/**
	 * @return array
	 */
	protected function data(): array
	{
		return [
			'text' => $this->getText(),
		];
	}

	function getText(): ?string
	{
		return $this->data->getText($this->getLang());
	}

}
