<?php

declare(strict_types=1);

namespace Cleevio\Questions\Api\DTO;

use Cleevio\RestApi\Command\ICommandDTO;

class CreateOptionPostDTO implements ICommandDTO
{
	/**
	 * @var array
	 */
	protected $options;

	/**
	 * @param array $options
	 */
	public function __construct(
		array $options
	)
	{
		$this->options = $options;
	}

	public static function fromRequest($data): ICommandDTO
	{
		return new static(
			array_map(static function (object $item) {
				if (!property_exists($item, 'text')) {
					throw new \InvalidArgumentException;
				}

				return [
					'text' => $item->text,
					'order' => property_exists($item, 'order') ? $item->order : null,
				];
			}, $data)
		);
	}

	public function getOptions(): array
	{
		return $this->options;
	}
}

