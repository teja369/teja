<?php

declare(strict_types=1);

namespace Cleevio\Questions\Api\Response;

use Cleevio\Api\Translatable\SimpleResponse;
use Cleevio\Questions\Entities\QuestionGroup;
use Cleevio\Questions\IGroupTranslation;

final class GroupTranslationResponseBuilder extends SimpleResponse implements IGroupTranslation
{

	/**
	 * @var QuestionGroup
	 */
	private $data;

	/**
	 * QuestionResponseBuilder constructor.
	 * @param QuestionGroup $group
	 * @param string|null $lang
	 */
	public function __construct(QuestionGroup $group, ?string $lang)
	{
		parent::__construct($lang);

		$this->data = $group;
	}

	/**
	 * @return array
	 */
	protected function data(): array
	{
		return [
			'name' => $this->getName(),
		];
	}

	function getName(): ?string
	{
		return $this->data->getName($this->getLang());
	}

}
