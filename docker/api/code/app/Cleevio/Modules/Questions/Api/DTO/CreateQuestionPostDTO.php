<?php

declare(strict_types=1);

namespace Cleevio\Questions\Api\DTO;

use Cleevio\RestApi\Command\ICommandDTO;

class CreateQuestionPostDTO implements ICommandDTO
{

	/**
	 * @var string
	 */
	protected $text;

	/**
	 * @var string
	 */
	protected $type;

	/**
	 * @var int
	 */
	protected $group;

	/**
	 * @var int|null
	 */
	protected $parent;

	/**
	 * @var string|null
	 */
	private $parentRule;

	/**
	 * @var bool
	 */
	protected $required;

	/**
	 * @var string|null
	 */
	private $help;

	/**
	 * @var string|null
	 */
	private $placeholder;

	/**
	 * @var string|null
	 */
	private $validation;

	/**
	 * @var string|null
	 */
	private $validationHelp;

	/**
	 * @var int|null
	 */
	private $order;

	/**
	 * @var bool
	 */
	protected $isPrefilled;

	/**
	 * @var int|null
	 */
	protected $minLength;

	/**
	 * @var int|null
	 */
	protected $maxLength;

	/**
	 * @var int|null
	 */
	protected $minValue;

	/**
	 * @var int|null
	 */
	protected $maxValue;

	/**
	 * @var bool|null
	 */
	protected $multiChoice;

	/**
	 * @var string|null
	 */
	protected $variant;


	/**
	 * @param string      $text
	 * @param string      $type
	 * @param int         $group
	 * @param int|null    $parent
	 * @param string|null $parentRule
	 * @param bool        $required
	 * @param string|null $help
	 * @param string|null $placeholder
	 * @param string|null $validation
	 * @param string|null $validationHelp
	 * @param int|null    $order
	 * @param bool        $isPrefilled
	 * @param int|null    $minLength
	 * @param int|null    $maxLength
	 * @param int|null    $minValue
	 * @param int|null    $maxValue
	 * @param bool|null   $multiChoice
	 * @param string|null $variant
	 */
	public function __construct(
		string $text,
		string $type,
		int $group,
		?int $parent,
		?string $parentRule,
		bool $required,
		?string $help,
		?string $placeholder,
		?string $validation,
		?string $validationHelp,
		?int $order,
		bool $isPrefilled,
		?int $minLength,
		?int $maxLength,
		?int $minValue,
		?int $maxValue,
		?bool $multiChoice,
		?string $variant
	)
	{
		$this->text = $text;
		$this->type = $type;
		$this->group = $group;
		$this->parent = $parent;
		$this->parentRule = $parentRule;
		$this->required = $required;
		$this->help = $help;
		$this->placeholder = $placeholder;
		$this->validation = $validation;
		$this->validationHelp = $validationHelp;
		$this->order = $order;
		$this->isPrefilled = $isPrefilled;
		$this->minLength = $minLength;
		$this->maxLength = $maxLength;
		$this->minValue = $minValue;
		$this->maxValue = $maxValue;
		$this->multiChoice = $multiChoice;
		$this->variant = $variant;
	}

	public static function fromRequest($data): ICommandDTO
	{
		return new static(
			$data->text,
			$data->type,
			$data->group,
			property_exists($data, 'parent') ? $data->parent : null,
			property_exists($data, 'parentRule') ? $data->parentRule : null,
			$data->required,
			property_exists($data, 'help') ? $data->help : null,
			property_exists($data, 'placeholder') ? $data->placeholder : null,
			property_exists($data, 'validation') ? $data->validation : null,
			property_exists($data, 'validationHelp') ? $data->validationHelp : null,
			property_exists($data, 'order') ? $data->order : null,
			property_exists($data, 'isPrefilled') ? $data->isPrefilled : false,
			property_exists($data, 'minLength') ? $data->minLength : null,
			property_exists($data, 'maxLength') ? $data->maxLength : null,
			property_exists($data, 'minValue') ? $data->minValue : null,
			property_exists($data, 'maxValue') ? $data->maxValue : null,
			property_exists($data, 'multiChoice') ? $data->multiChoice : null,
			property_exists($data, 'variant') ? $data->variant : null
		);
	}

	/**
	 * @return string
	 */
	public function getText(): string
	{
		return $this->text;
	}

	/**
	 * @return string
	 */
	public function getType(): string
	{
		return $this->type;
	}

	/**
	 * @return int
	 */
	public function getGroup(): int
	{
		return $this->group;
	}

	/**
	 * @return int|null
	 */
	public function getParent(): ?int
	{
		return $this->parent;
	}

	/**
	 * @return string|null
	 */
	public function getParentRule(): ?string
	{
		return $this->parentRule;
	}

	/**
	 * @return bool
	 */
	public function isRequired(): bool
	{
		return $this->required;
	}

	/**
	 * @return string|null
	 */
	public function getHelp(): ?string
	{
		return $this->help;
	}

	/**
	 * @return string|null
	 */
	public function getPlaceholder(): ?string
	{
		return $this->placeholder;
	}

	/**
	 * @return string|null
	 */
	public function getValidation(): ?string
	{
		return $this->validation;
	}

	/**
	 * @return string|null
	 */
	public function getValidationHelp(): ?string
	{
		return $this->validationHelp;
	}

	/**
	 * @return int|null
	 */
	public function getOrder(): ?int
	{
		return $this->order;
	}

	/**
	 * @return bool
	 */
	public function isPrefilled(): bool
	{
		return $this->isPrefilled;
	}

	/**
	 * @return int|null
	 */
	public function getMinLength(): ?int
	{
		return $this->minLength;
	}

	/**
	 * @return int|null
	 */
	public function getMaxLength(): ?int
	{
		return $this->maxLength;
	}

	/**
	 * @return int|null
	 */
	public function getMinValue(): ?int
	{
		return $this->minValue;
	}

	/**
	 * @return int|null
	 */
	public function getMaxValue(): ?int
	{
		return $this->maxValue;
	}

	/**
	 * @return bool|null
	 */
	public function getMultiChoice(): ?bool
	{
		return $this->multiChoice;
	}

	/**
	 * @return string|null
	 */
	public function getVariant(): ?string
	{
		return $this->variant;
	}

}

