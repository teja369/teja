<?php

declare(strict_types=1);

namespace Cleevio\Questions\Api\Response\Questions;

use Cleevio\Questions\Entities\Questions\TextQuestion;
use Cleevio\RestApi\Response\SimpleResponse;

final class TextQuestionResponseBuilder extends SimpleResponse
{

	/**
	 * @var TextQuestion
	 */
	private $data;

	/**
	 * TextQuestionResponseBuilder constructor.
	 * @param TextQuestion $question
	 */
	public function __construct(TextQuestion $question)
	{
		$this->data = $question;
	}

	/**
	 * @return array
	 */
	protected function data(): array
	{
		return [
			'minLength' => $this->data->getMinLength(),
			'maxLength' => $this->data->getMaxLength(),
		];
	}
}
