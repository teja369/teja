<?php

declare(strict_types=1);

namespace Cleevio\Questions\Api\Response\Questions;

use Cleevio\Api\Translatable\SimpleResponse;
use Cleevio\Questions\Entities\Questions\QuestionOption;

final class QuestionOptionResponseBuilder extends SimpleResponse
{

	/**
	 * @var QuestionOption
	 */
	private $data;

	/**
	 * QuestionOptionResponseBuilder constructor.
	 * @param QuestionOption $option
	 * @param string|null $lang
	 */
	public function __construct(QuestionOption $option, ?string $lang)
	{
		parent::__construct($lang);

		$this->data = $option;
	}

	/**
	 * @return array
	 */
	protected function data(): array
	{
		return [
			"id" => $this->data->getId(),
			"text" => $this->data->getText($this->getLang()),
			"order" => $this->data->getOrder(),
		];
	}
}
