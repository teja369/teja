<?php

declare(strict_types=1);

namespace Cleevio\Questions\Api\Response\Questions;

use Cleevio\Api\Translatable\PagedResponse;
use Cleevio\Questions\Entities\Questions\QuestionOption;
use Cleevio\RestApi\Request\PagedRequest;

final class QuestionOptionsPagedResponseBuilder extends PagedResponse
{

	/**
	 * @var array
	 */
	private $data;


	/**
	 * QuestionOptionsPagedResponseBuilder constructor.
	 * @param PagedRequest $request
	 * @param array        $data
	 * @param string|null  $lang
	 */
	public function __construct(PagedRequest $request, array $data, ?string $lang)
	{
		parent::__construct($request, $lang);

		$this->data = $data;
	}

	/**
	 * @return array
	 */
	protected function data(): array
	{
		return array_map(function (QuestionOption $option) {
			return (new QuestionOptionResponseBuilder($option, $this->getLang()))->build();
		}, $this->data);
	}
}
