<?php

declare(strict_types=1);

namespace Cleevio\Questions\Api\DTO;

use Cleevio\Questions\IQuestionTranslation;
use Cleevio\RestApi\Command\ICommandDTO;

class TranslationPutDTO implements ICommandDTO, IQuestionTranslation
{
	/**
	 * @var string|null
	 */
	protected $text;

	/**
	 * @var string|null
	 */
	protected $help;

	/**
	 * @var string|null
	 */
	protected $placeholder;

	/**
	 * @var string|null
	 */
	protected $validationHelp;


	public function __construct(
		?string $text,
		?string $help,
		?string $placeholder,
		?string $validationHelp
	)
	{
		$this->text = $text;
		$this->help = $help;
		$this->placeholder = $placeholder;
		$this->validationHelp = $validationHelp;
	}

	public static function fromRequest($data): ICommandDTO
	{
		return new static(
			property_exists($data, 'text') ? $data->text : null,
			property_exists($data, 'help') ? $data->help : null,
			property_exists($data, 'placeholder') ? $data->placeholder : null,
			property_exists($data, 'validationHelp') ? $data->validationHelp : null
		);
	}

	public function getText(): ?string
	{
		return $this->text;
	}

	public function getHelp(): ?string
	{
		return $this->help;
	}

	public function getPlaceholder(): ?string
	{
		return $this->placeholder;
	}

	public function getValidationHelp(): ?string
	{
		return $this->validationHelp;
	}
}
