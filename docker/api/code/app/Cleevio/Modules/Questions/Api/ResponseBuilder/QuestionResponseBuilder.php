<?php

declare(strict_types=1);

namespace Cleevio\Questions\Api\Response;

use Cleevio\Api\Translatable\SimpleResponse;
use Cleevio\Questions\Api\Response\Questions\BoolQuestionResponseBuilder;
use Cleevio\Questions\Api\Response\Questions\ChoiceQuestionResponseBuilder;
use Cleevio\Questions\Api\Response\Questions\CountryQuestionResponseBuilder;
use Cleevio\Questions\Api\Response\Questions\DateTimeQuestionResponseBuilder;
use Cleevio\Questions\Api\Response\Questions\NumberQuestionResponseBuilder;
use Cleevio\Questions\Api\Response\Questions\TextQuestionResponseBuilder;
use Cleevio\Questions\Entities\Question;
use Cleevio\Questions\Entities\Questions\BoolQuestion;
use Cleevio\Questions\Entities\Questions\ChoiceQuestion;
use Cleevio\Questions\Entities\Questions\CountryQuestion;
use Cleevio\Questions\Entities\Questions\DateTimeQuestion;
use Cleevio\Questions\Entities\Questions\NumberQuestion;
use Cleevio\Questions\Entities\Questions\TextQuestion;
use Cleevio\Trips\Entities\Trip;

final class QuestionResponseBuilder extends SimpleResponse
{

	/**
	 * @var Question
	 */
	private $data;

	/**
	 * @var Trip|null
	 */
	private $trip;

	/**
	 * QuestionResponseBuilder constructor.
	 * @param Question $question
	 * @param Trip|null $trip
	 * @param string|null $lang
	 */
	public function __construct(Question $question, ?Trip $trip, ?string $lang)
	{
		parent::__construct($lang);

		$this->data = $question;
		$this->trip = $trip;
	}

	/**
	 * @return array
	 */
	protected function data(): array
	{
		return [
			'id' => $this->data->getId(),
			'type' => $this->data->getType(),
			'parent' => $this->data->getParent() !== null ? (new QuestionResponseBuilder($this->data->getParent(), $this->trip, $this->getLang()))->build() : null,
			'parentRule' => $this->data->getParentRule() ?? null,
			'question' => $this->data->getText($this->getLang()),
			'group' => (new GroupResponseBuilder($this->data->getGroup(), $this->getLang()))->build(),
			'required' => $this->data->isRequired(),
			'validation' => $this->data->getValidation(),
			'validationHelp' => $this->data->getValidation() !== null ? $this->data->getValidationHelp($this->getLang()) : null,
			'order' => $this->data->getOrder(),
			'help' => $this->data->getHelp($this->getLang()),
			'placeholder' => $this->data->getPlaceholder($this->getLang()),
			'hasChildren' => count($this->data->getChildren()) > 0,
			'visibility' => $this->data->getVisibility(),
			'isPrefilled' => $this->data->isPrefilled(),
			'isDeleted' => $this->data->isDeleted(),
			'text' => $this->data instanceof TextQuestion ? (new TextQuestionResponseBuilder($this->data))->build() : null,
			'bool' => $this->data instanceof BoolQuestion ? (new BoolQuestionResponseBuilder($this->data))->build() : null,
			'number' => $this->data instanceof NumberQuestion ? (new NumberQuestionResponseBuilder($this->data))->build() : null,
			'choice' => $this->data instanceof ChoiceQuestion ? (new ChoiceQuestionResponseBuilder($this->data, $this->trip, $this->getLang()))->build() : null,
			'datetime' => $this->data instanceof DateTimeQuestion ? (new DateTimeQuestionResponseBuilder($this->data))->build() : null,
			'country' => $this->data instanceof CountryQuestion ? (new CountryQuestionResponseBuilder($this->data))->build() : null,
		];
	}
}
