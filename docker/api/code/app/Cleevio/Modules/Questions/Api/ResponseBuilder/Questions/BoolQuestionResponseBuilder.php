<?php

declare(strict_types=1);

namespace Cleevio\Questions\Api\Response\Questions;

use Cleevio\Questions\Entities\Questions\BoolQuestion;
use Cleevio\RestApi\Response\SimpleResponse;

final class BoolQuestionResponseBuilder extends SimpleResponse
{

	/**
	 * @var BoolQuestion
	 */
	private $data;

	/**
	 * BoolQuestionResponseBuilder constructor.
	 * @param BoolQuestion $question
	 */
	public function __construct(BoolQuestion $question)
	{
		$this->data = $question;
	}

	/**
	 * @return array
	 */
	protected function data(): array
	{
		unset($this->data);

		return [];
	}
}
