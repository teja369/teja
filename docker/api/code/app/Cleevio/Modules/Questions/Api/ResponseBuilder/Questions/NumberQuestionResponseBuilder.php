<?php

declare(strict_types=1);

namespace Cleevio\Questions\Api\Response\Questions;

use Cleevio\Questions\Entities\Questions\NumberQuestion;
use Cleevio\RestApi\Response\SimpleResponse;

final class NumberQuestionResponseBuilder extends SimpleResponse
{

	/**
	 * @var NumberQuestion
	 */
	private $data;

	/**
	 * NumberQuestionResponseBuilder constructor.
	 * @param NumberQuestion $question
	 */
	public function __construct(NumberQuestion $question)
	{
		$this->data = $question;
	}

	/**
	 * @return array
	 */
	protected function data(): array
	{
		return [
			'minValue' => $this->data->getMinValue(),
			'maxValue' => $this->data->getMaxValue(),
		];
	}
}
