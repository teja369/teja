<?php

declare(strict_types=1);

namespace Cleevio\Questions\Api\Response;

use Cleevio\Api\Translatable\SimpleResponse;
use Cleevio\Questions\Entities\QuestionGroup;

final class GroupsResponseBuilder extends SimpleResponse
{
	/**
	 * @var array
	 */
	private $data;

	/**
	 * GroupsResponseBuilder constructor.
	 * @param QuestionGroup[] $data
	 * @param string|null $lang
	 */
	public function __construct(array $data, ?string $lang)
	{
		parent::__construct($lang);

		$this->data = $data;
	}

	/**
	 * @return array
	 */
	protected function data(): array
	{
		return array_map(function (QuestionGroup $group) {
			return (new GroupResponseBuilder($group, $this->getLang()))->build();
		}, $this->data);
	}

}
