<?php

declare(strict_types=1);

namespace Cleevio\Questions\Api\Response\Questions;

use Cleevio\Api\Translatable\SimpleResponse;
use Cleevio\Questions\Entities\Questions\QuestionOption;

final class QuestionOptionsResponseBuilder extends SimpleResponse
{

	/**
	 * @var QuestionOption[]
	 */
	private $data;

	/**
	 * SingleChoiceQuestion constructor.
	 * @param QuestionOption[] $options
	 * @param string|null $lang
	 */
	public function __construct(array $options, ?string $lang)
	{
		parent::__construct($lang);

		$this->data = $options;
	}

	/**
	 * @return array
	 */
	protected function data(): array
	{
		return array_map(function (QuestionOption $option) {
			return (new QuestionOptionResponseBuilder($option, $this->getLang()))->build();
		}, $this->data);
	}
}
