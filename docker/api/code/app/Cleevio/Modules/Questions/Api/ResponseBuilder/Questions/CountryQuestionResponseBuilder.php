<?php

declare(strict_types=1);

namespace Cleevio\Questions\Api\Response\Questions;

use Cleevio\Questions\Entities\Questions\CountryQuestion;
use Cleevio\RestApi\Response\SimpleResponse;

final class CountryQuestionResponseBuilder extends SimpleResponse
{

	/**
	 * @var CountryQuestion
	 */
	private $data;


	/**
	 * CountryQuestionResponseBuilder constructor.
	 * @param CountryQuestion $question
	 */
	public function __construct(CountryQuestion $question)
	{
		$this->data = $question;
	}

	/**
	 * @return array
	 */
	protected function data(): array
	{
		unset($this->data);

		return [];
	}
}
