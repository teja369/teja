<?php

declare(strict_types=1);

namespace Cleevio\Countries\Api\DTO;

use Cleevio\RestApi\Command\ICommandDTO;

class CountriesPutDTO implements ICommandDTO
{
	/**
	 * @var array
	 */
	protected $ids;


	public function __construct(
		array $ids
	)
	{
		$this->ids = $ids;
	}


	public static function fromRequest($data): ICommandDTO
	{
		return new static(
			array_map(static function (object $item) {
				$item = (array) $item;

				return $item['id'];
			}, $data)
		);
	}

	public function getIds(): array
	{
		return $this->ids;
	}

}
