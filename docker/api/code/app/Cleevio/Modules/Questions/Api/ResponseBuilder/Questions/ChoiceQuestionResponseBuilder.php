<?php

declare(strict_types=1);

namespace Cleevio\Questions\Api\Response\Questions;

use Cleevio\Api\Translatable\SimpleResponse;
use Cleevio\Questions\Entities\Questions\ChoiceQuestion;
use Cleevio\Trips\Entities\Trip;

final class ChoiceQuestionResponseBuilder extends SimpleResponse
{

	/**
	 * @var ChoiceQuestion
	 */
	private $data;

	/**
	 * @var Trip|null
	 */
	private $trip;

	/**
	 * ChoiceQuestionResponseBuilder constructor.
	 * @param ChoiceQuestion $question
	 * @param Trip|null $trip
	 * @param string|null $lang
	 */
	public function __construct(ChoiceQuestion $question, ?Trip $trip, ?string $lang)
	{
		parent::__construct($lang);

		$this->data = $question;
		$this->trip = $trip;
	}

	/**
	 * @return array
	 */
	protected function data(): array
	{
		return [
			"multiChoice" => $this->data->isMultiChoice(),
			"options" => (new QuestionOptionsResponseBuilder($this->data->getOptionsTrip($this->trip), $this->getLang()))->build(),
		];
	}
}
