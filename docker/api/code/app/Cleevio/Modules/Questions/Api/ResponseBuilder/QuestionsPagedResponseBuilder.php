<?php

declare(strict_types=1);

namespace Cleevio\Questions\Api\Response;

use Cleevio\Api\Translatable\PagedResponse;
use Cleevio\Questions\Entities\Question;
use Cleevio\RestApi\Request\PagedRequest;

final class QuestionsPagedResponseBuilder extends PagedResponse
{
	/**
	 * @var array
	 */
	private $data;

	/**
	 * QuestionsResponseBuilder constructor.
	 * @param PagedRequest $request
	 * @param Question[] $data
	 * @param string|null $lang
	 */
	public function __construct(PagedRequest $request, array $data, ?string $lang)
	{
		parent::__construct($request, $lang);

		$this->data = $data;
	}

	/**
	 * @return array
	 */
	protected function data(): array
	{
		return array_map(function (Question $purpose) {
			return (new QuestionResponseBuilder($purpose, null, $this->getLang()))->build();
		}, $this->data);
	}

}
