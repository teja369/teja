<?php

declare(strict_types=1);

namespace Cleevio\Questions\Api\Response\Questions;

use Cleevio\Questions\Entities\Questions\DateTimeQuestion;
use Cleevio\RestApi\Response\SimpleResponse;

final class DateTimeQuestionResponseBuilder extends SimpleResponse
{

	/**
	 * @var DateTimeQuestion
	 */
	private $data;

	/**
	 * TextQuestionResponseBuilder constructor.
	 * @param DateTimeQuestion $question
	 */
	public function __construct(DateTimeQuestion $question)
	{
		$this->data = $question;
	}

	/**
	 * @return array
	 */
	protected function data(): array
	{
		return [
			'variant' => $this->data->getVariant(),
		];
	}
}
