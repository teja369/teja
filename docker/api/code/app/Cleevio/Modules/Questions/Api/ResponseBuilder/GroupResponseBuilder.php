<?php

declare(strict_types=1);

namespace Cleevio\Questions\Api\Response;

use Cleevio\Api\Translatable\SimpleResponse;
use Cleevio\Questions\Entities\QuestionGroup;

final class GroupResponseBuilder extends SimpleResponse
{

	/**
	 * @var QuestionGroup
	 */
	private $data;

	/**
	 * QuestionResponseBuilder constructor.
	 * @param QuestionGroup $group
	 * @param string|null $lang
	 */
	public function __construct(QuestionGroup $group, ?string $lang)
	{
		parent::__construct($lang);

		$this->data = $group;
	}

	/**
	 * @return array
	 */
	protected function data(): array
	{
		return [
			'id' => $this->data->getId(),
			'type' => $this->data->getType(),
			'name' => $this->data->getName($this->getLang()),
		];
	}
}
