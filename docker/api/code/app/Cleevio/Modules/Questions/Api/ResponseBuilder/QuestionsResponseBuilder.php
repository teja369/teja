<?php

declare(strict_types=1);

namespace Cleevio\Questions\Api\Response;

use Cleevio\Api\Translatable\SimpleResponse;
use Cleevio\Questions\Entities\Question;
use Cleevio\Trips\Entities\Trip;

final class QuestionsResponseBuilder extends SimpleResponse
{
	/**
	 * @var array
	 */
	private $data;

	/**
	 * @var Trip|null
	 */
	private $trip;

	/**
	 * QuestionsResponseBuilder constructor.
	 * @param Question[] $data
	 * @param Trip|null $trip
	 * @param string|null $lang
	 */
	public function __construct(array $data, ?Trip $trip, ?string $lang)
	{
		parent::__construct($lang);

		$this->data = $data;
		$this->trip = $trip;
	}

	/**
	 * @return array
	 */
	protected function data(): array
	{
		return array_map(function (Question $purpose) {
			return (new QuestionResponseBuilder($purpose, $this->trip, $this->getLang()))->build();
		}, $this->data);
	}

}
