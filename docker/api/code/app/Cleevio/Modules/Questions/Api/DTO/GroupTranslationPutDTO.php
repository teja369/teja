<?php

declare(strict_types=1);

namespace Cleevio\Questions\Api\DTO;

use Cleevio\Questions\IGroupTranslation;
use Cleevio\RestApi\Command\ICommandDTO;

class GroupTranslationPutDTO implements ICommandDTO, IGroupTranslation
{
	/**
	 * @var string|null
	 */
	protected $name;


	public function __construct(
		?string $name
	)
	{
		$this->name = $name;
	}

	public static function fromRequest($data): ICommandDTO
	{
		return new static(
			property_exists($data, 'name') ? $data->name : null
		);
	}

	public function getName(): ?string
	{
		return $this->name;
	}
}
