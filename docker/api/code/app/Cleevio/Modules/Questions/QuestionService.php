<?php

declare(strict_types=1);

namespace Cleevio\Questions;

use Cleevio\Countries\CountryNotFound;
use Cleevio\Countries\Entities\Country;
use Cleevio\Countries\Repositories\CountryConstraints;
use Cleevio\Countries\Repositories\ICountryRepository;
use Cleevio\Hosts\Entities\Host;
use Cleevio\Languages\Entities\Language;
use Cleevio\Questions\Api\DTO\CreateQuestionPostDTO;
use Cleevio\Questions\Api\DTO\QuestionVisibilityPutDTO;
use Cleevio\Questions\Entities\Question;
use Cleevio\Questions\Entities\QuestionCountry;
use Cleevio\Questions\Entities\QuestionGroup;
use Cleevio\Questions\Entities\QuestionGroupTranslation;
use Cleevio\Questions\Entities\QuestionHost;
use Cleevio\Questions\Entities\Questions\BoolQuestion;
use Cleevio\Questions\Entities\Questions\ChoiceQuestion;
use Cleevio\Questions\Entities\Questions\CountryQuestion;
use Cleevio\Questions\Entities\Questions\DateTimeQuestion;
use Cleevio\Questions\Entities\Questions\NumberQuestion;
use Cleevio\Questions\Entities\Questions\QuestionOption;
use Cleevio\Questions\Entities\Questions\QuestionOptionCountry;
use Cleevio\Questions\Entities\Questions\QuestionOptionTranslation;
use Cleevio\Questions\Entities\Questions\TextQuestion;
use Cleevio\Questions\Entities\QuestionTranslation;
use Cleevio\Questions\Repositories\GroupConstraints;
use Cleevio\Questions\Repositories\IGroupRepository;
use Cleevio\Questions\Repositories\IQuestionOptionCountryRepository;
use Cleevio\Questions\Repositories\IQuestionOptionRepository;
use Cleevio\Questions\Repositories\IQuestionRepository;
use Cleevio\Questions\Repositories\QuestionConstraints;
use Cleevio\Registrations\Entities\Registration;
use Cleevio\Registrations\Entities\RegistrationQuestion;
use Cleevio\Trips\Entities\Trip;
use Cleevio\Trips\Entities\TripRegistration;
use Cleevio\Trips\Repositories\ITripRepository;
use DateTime;
use Exception;
use Nette\Http\IRequest;
use Nette\Security\User;

class QuestionService implements IQuestionService
{
	private const QUESTION_VISIBILITY_ENABLED = [Question::QUESTION_VISIBILITY_ENABLED];
	private const QUESTION_VISIBILITY_PREVIEW = [Question::QUESTION_VISIBILITY_ENABLED, Question::QUESTION_VISIBILITY_PREVIEW];


	/**
	 * @var IQuestionRepository
	 */
	private $questionRepository;

	/**
	 * @var IGroupRepository
	 */
	private $groupRepository;

	/**
	 * @var IQuestionOptionRepository
	 */
	private $optionRepository;

	/**
	 * @var IQuestionOptionCountryRepository
	 */
	private $questionOptionCountryRepository;

	/**
	 * @var ITripRepository
	 */
	private $tripRepository;

	/**
	 * @var ICountryRepository
	 */
	private $countryRepository;

	/**
	 * @var User
	 */
	private $user;

	/**
	 * @param IQuestionRepository $questionRepository
	 * @param IGroupRepository $groupRepository
	 * @param IQuestionOptionRepository $optionRepository
	 * @param IQuestionOptionCountryRepository $questionOptionCountryRepository
	 * @param ITripRepository $tripRepository
	 * @param User $user
	 * @param ICountryRepository $countryRepository
	 */
	public function __construct(
		IQuestionRepository $questionRepository,
		IGroupRepository $groupRepository,
		IQuestionOptionRepository $optionRepository,
		IQuestionOptionCountryRepository $questionOptionCountryRepository,
		ITripRepository $tripRepository,
		ICountryRepository $countryRepository,
		User $user
	)
	{
		$this->questionRepository = $questionRepository;
		$this->groupRepository = $groupRepository;
		$this->optionRepository = $optionRepository;
		$this->questionOptionCountryRepository = $questionOptionCountryRepository;
		$this->tripRepository = $tripRepository;
		$this->countryRepository = $countryRepository;
		$this->user = $user;
	}


	/**
	 * @param int $id
	 * @return QuestionGroup|null
	 */
	public function getGroup(int $id): ?QuestionGroup
	{
		return $this->groupRepository->find($id);
	}


	/**
	 * @param int $id
	 * @return QuestionOption|null
	 */
	public function getOption(int $id): ?QuestionOption
	{
		return $this->optionRepository->find($id);
	}


	/**
	 * @param Trip|null $trip
	 * @param string|null $type
	 * @return QuestionGroup[]
	 */
	public function getGroups(?Trip $trip, ?string $type): array
	{
		$constraints = new GroupConstraints;
		$constraints->setIsDeleted(false);

		if ($type !== null) {
			$constraints->setType($type);
		}

		$groups = $this->groupRepository->findAll($constraints->build());

		if ($trip === null) {
			return $groups;
		}

		$registrations = array_map(static function (TripRegistration $registration) {
			return $registration->getRegistration();
		}, $trip->getRegistrations());
		$output = [];

		foreach ($groups as $group) {
			if (count($this->getQuestions($group, $trip->getCountry(), $registrations, $trip->getHost(), false)) > 0) {
				$output[] = $group;
			}
		}

		return $output;
	}


	/**
	 * @param string $name
	 * @param string $type
	 * @return QuestionGroup
	 */
	public function createGroup(string $name, string $type): QuestionGroup
	{
		$group = new QuestionGroup($name, $type);

		$this->groupRepository->persist($group);

		return $group;
	}


	/**
	 * @param QuestionGroup $group
	 * @param string $name
	 * @param string $type
	 * @return QuestionGroup
	 */
	public function updateGroup(QuestionGroup $group, string $name, string $type): QuestionGroup
	{
		$group->setName($name);
		$group->setType($type);

		$this->groupRepository->persist($group);

		return $group;
	}


	/**
	 * Delete group and all of it's questions
	 * @param QuestionGroup $group
	 * @throws Exception
	 */
	public function deleteGroup(QuestionGroup $group): void
	{
		$deletedAt = new DateTime;

		foreach ($group->getQuestions() as $question) {
			$this->invalidateTrips($question);
			$question->setDeletedAt($deletedAt);
			$this->questionRepository->persist($question);
		}

		$group->setDeletedAt($deletedAt);
		$this->groupRepository->persist($group);
	}


	/**
	 * @param int $id
	 * @return Question|null
	 */
	public function getQuestion(int $id): ?Question
	{
		return $this->questionRepository->find($id);
	}


	/**
	 * @param CreateQuestionPostDTO $data
	 * @param QuestionGroup $group
	 * @return Question
	 * @throws QuestionInvalidText
	 * @throws QuestionParentCircular
	 * @throws QuestionParentGroupInvalid
	 * @throws QuestionParentNotFound
	 * @throws QuestionTypeInvalid
	 * @throws QuestionUnique
	 */
	public function createQuestion(CreateQuestionPostDTO $data, QuestionGroup $group): Question
	{
		switch ($data->getType()) {

			case Question::QUESTION_TYPE_TEXT:
				$question = new TextQuestion($data->getText(), $group);

				break;
			case Question::QUESTION_TYPE_NUMBER:
				$question = new NumberQuestion($data->getText(), $group);

				break;
			case Question::QUESTION_TYPE_BOOL:
				$question = new BoolQuestion($data->getText(), $group);

				break;
			case Question::QUESTION_TYPE_CHOICE:
				$question = new ChoiceQuestion($data->getText(), $group);

				break;
			case Question::QUESTION_TYPE_DATETIME:
				$question = new DateTimeQuestion($data->getText(), $group);

				break;
			case Question::QUESTION_TYPE_COUNTRY:
				$question = new CountryQuestion($data->getText(), $group);

				break;
			default:
				throw new QuestionTypeInvalid($data->getType());
		}

		return $this->updateQuestion($question, $data, $group);
	}


	/**
	 * @param Question $question
	 * @param CreateQuestionPostDTO $data
	 * @param QuestionGroup $group
	 * @return Question
	 * @throws QuestionInvalidText
	 * @throws QuestionParentCircular
	 * @throws QuestionParentGroupInvalid
	 * @throws QuestionParentNotFound
	 * @throws QuestionTypeInvalid
	 * @throws QuestionUnique
	 */
	public function updateQuestion(Question $question, CreateQuestionPostDTO $data, QuestionGroup $group): Question
	{
		$nameValidation = preg_match('/^[a-z0-9_]*$/', $data->getText());

		if ($nameValidation === 0 || $nameValidation === false) {
			throw new QuestionInvalidText;
		}

		$unique = $this->questionRepository->findBy((new QuestionConstraints)->setText($data->getText())->build());

		if ($unique instanceof Question && !$unique->isDeleted() && (!$question->hasId() || $unique->getId() !== $question->getId())) {
			throw new QuestionUnique;
		}

		if ($question instanceof TextQuestion) {
			$question->setMinLength($data->getMinLength() > 0 ? $data->getMinLength() : null);
			$question->setMaxLength($data->getMaxLength() > 0 ? $data->getMaxLength() : null);
		} elseif ($question instanceof NumberQuestion) {
			$question->setMinValue($data->getMinValue() > 0 ? $data->getMinValue() : null);
			$question->setMaxValue($data->getMaxValue() > 0 ? $data->getMaxValue() : null);
		} elseif ($question instanceof BoolQuestion) {
			// Void
		} elseif ($question instanceof ChoiceQuestion) {
			$question->setMultiChoice($data->getMultiChoice() ?? false);
		} elseif ($question instanceof DateTimeQuestion) {
			if ($data->getVariant() !== null) {
				$question->setVariant($data->getVariant());
			}
		} elseif ($question instanceof CountryQuestion) {
			// Void
		} else {
			throw new QuestionTypeInvalid($data->getType());
		}

		$question->setText($data->getText());
		$question->setGroup($group);
		$question->setRequired($data->isRequired());
		$question->setHelp($data->getHelp() !== '' ? $data->getHelp() : null);
		$question->setPlaceholder($data->getPlaceholder() !== '' ? $data->getPlaceholder() : null);
		$question->setValidation($data->getValidation() !== '' ? $data->getValidation() : null);
		$question->setValidationHelp($data->getValidationHelp() !== '' ? $data->getValidationHelp() : null);
		$question->setIsPrefilled($data->isPrefilled());

		if ($data->getOrder() !== null) {
			$question->setOrder($data->getOrder());
		}

		// Handle parent
		if ($data->getParent() !== null) {
			$parent = $this->questionRepository->find($data->getParent());

			if (!$parent instanceof Question) {
				throw new QuestionParentNotFound;
			}

			if ($parent->getGroup()->getId() !== $group->getId()) {
				throw new QuestionParentGroupInvalid;
			}

			$tree = $parent;

			while ($tree !== null) {
				if ($question->hasId() && $tree->getId() === $question->getId()) {
					throw new QuestionParentCircular;
				}

				$tree = $tree->getParent();
			}

			$question->setParent($parent);
			$question->setParentRule($data->getParentRule() !== '' ? $data->getParentRule() : null);
		} else {
			$question->setParent(null);
			$question->setParentRule(null);
		}

		// Check children group
		foreach ($question->getChildren() as $child) {
			if ($child->getGroup()->getId() !== $question->getGroup()->getId()) {
				throw new QuestionParentGroupInvalid;
			}
		}

		// Invalidate proper drafts
		$this->invalidateTrips($question);

		$this->questionRepository->persist($question);

		return $question;
	}


	/**
	 * @param Question $question
	 * @param string $text
	 * @return QuestionOption
	 * @throws QuestionOptionInvalidText
	 */
	public function createOption(Question $question, string $text): QuestionOption
	{
		$nameValidation = preg_match('/^[a-z0-9_]*$/', $text);

		if ($nameValidation === 0 || $nameValidation === false) {
			throw new QuestionOptionInvalidText;
		}

		$option = $this->optionRepository->findByText($question, $text);

		if ($option !== null) {
			return $option;
		}

		$this->invalidateTrips($question);

		$option = new QuestionOption($question, $text);
		$this->optionRepository->persist($option);

		return $option;
	}


	/**
	 * @param QuestionOption $option
	 * @param string $text
	 * @param int|null $order
	 * @return QuestionOption
	 * @throws QuestionOptionInvalidText
	 */
	public function updateOption(QuestionOption $option, string $text, ?int $order): QuestionOption
	{

		$nameValidation = preg_match('/^[a-z0-9_]*$/', $text);

		if ($nameValidation === 0 || $nameValidation === false) {
			throw new QuestionOptionInvalidText;
		}

		$option->setText($text);

		if ($order !== null) {
			$option->setOrder($order);
		}

		$this->invalidateTrips($option->getQuestion());

		$this->optionRepository->persist($option);

		return $option;
	}


	/**
	 * @param QuestionOption $option
	 * @return void
	 */
	public function deleteOption(QuestionOption $option): void
	{
		$this->invalidateTrips($option->getQuestion());
		$this->optionRepository->delete($option);
	}


	/**
	 * @param Question $question
	 * @param Country[] $countries
	 * @return Question
	 */
	public function setCountries(Question $question, array $countries): Question
	{
		$this->invalidateTrips($question);

		$question->setCountries(
			array_map(static function (Country $country) use ($question) {
				return new QuestionCountry($country, $question);
			}, $countries)
		);

		$this->questionRepository->persist($question);

		return $question;
	}


	/**
	 * @param Question $question
	 * @param array $hosts
	 * @return Question
	 */
	public function setHosts(Question $question, array $hosts): Question
	{
		$this->invalidateTrips($question);

		$question->setHosts(
			array_map(static function (Host $host) use ($question) {
				return new QuestionHost($host, $question);
			}, $hosts)
		);

		$this->questionRepository->persist($question);

		return $question;
	}


	/**
	 * @param Question $question
	 * @param Registration[] $registrations
	 * @return Question
	 */
	public function setRegistrations(Question $question, array $registrations): Question
	{
		$this->invalidateTrips($question);

		$question->setRegistrations(
			array_map(static function (Registration $registration) use ($question) {
				return new RegistrationQuestion($question, $registration);
			}, $registrations)
		);

		$this->questionRepository->persist($question);

		return $question;
	}


	/**
	 * @param QuestionOption $option
	 * @param Country[] $countries
	 * @return QuestionOption
	 */
	public function setOptionCountries(QuestionOption $option, array $countries): QuestionOption
	{
		$this->invalidateTrips($option->getQuestion());

		$option->setCountries(
			array_map(static function (Country $country) use ($option) {
				return new QuestionOptionCountry($country, $option);
			}, $countries)
		);

		$this->optionRepository->persist($option);

		return $option;
	}


	/**
	 * @param QuestionOption $option
	 * @param Country $country
	 * @return QuestionOption
	 */
	public function deleteOptionCountry(QuestionOption $option, Country $country): QuestionOption
	{
		$this->invalidateTrips($option->getQuestion());

		foreach ($option->getCountries() as $c) {
			if ($c->getCountry()->getId() === $country->getId()) {
				$this->questionOptionCountryRepository->delete($c);
			}
		}

		return $option;
	}


	/**
	 * @param QuestionGroup|null $group
	 * Get questions for trip
	 * @param Trip $trip
	 * @return Question[]
	 */
	public function getQuestionsTrip(?QuestionGroup $group, Trip $trip): array
	{
		return $this->getQuestions($group, $trip->getCountry(), array_map(static function (
			TripRegistration $registration
		) {
			return $registration->getRegistration();
		}, $trip->getRegistrations()), $trip->getHost(), false);
	}


	/**
	 * @param QuestionGroup|null $group
	 * @param Country|null $country
	 * @param Registration[] $registrations
	 * @param Host|null $host
	 * @param bool $showDisabled
	 * @return Question[]
	 */
	public function getQuestions(
		?QuestionGroup $group,
		?Country $country,
		array $registrations,
		?Host $host,
		bool $showDisabled
	): array
	{
		// Get all questions for the group
		$constraints = new QuestionConstraints;

		if ($group !== null) {
			$constraints->setGroup($group);
		}

		$visibility = $this->user->isAllowed('admin')
			? self::QUESTION_VISIBILITY_PREVIEW
			: self::QUESTION_VISIBILITY_ENABLED;

		if (!$showDisabled) {
			$constraints->setVisibility($visibility);
		}

		$constraints = $constraints
			->setIsDeleted(false)
			->setOrderBy('order');

		$questions = $this->questionRepository->findAll($constraints->build());
		$output = [];

		foreach ($questions as $question) {
			if (!$question instanceof Question || $question->isDeleted()) {
				continue;
			}

			if (!in_array($question->getVisibility(), $visibility, true) && !$showDisabled) {
				continue;
			}

			// Skip question if parent is disabled
			$parent = $question->getParent();

			while ($parent !== null) {
				if ($parent->isDeleted() || (!in_array($parent->getVisibility(), $visibility, true) && !$showDisabled)) {
					continue 2;
				}

				$parent = $parent->getParent();
			}

			if (!$this->hasCountry($question, $country)) {
				continue;
			}

			if (!$this->hasHost($question, $host)) {
				continue;
			}

			if (!$this->hasRegistration($question, $registrations)) {
				continue;
			}

			$output[] = $question;
		}

		$tree = [];

		// Check tree to show only children that have visible parents
		foreach ($output as $question) {
			if ($question->getParent() === null) {
				$tree[] = $question;
			} else {
				foreach ($output as $parent) {
					if ($parent->getId() === $question->getParent()->getId()) {
						$tree[] = $question;
					}
				}
			}
		}

		return $tree;
	}


	/**
	 * Check if question belongs to provided country
	 * @param Question $question
	 * @param Country|null $country
	 * @return bool
	 */
	private function hasCountry(Question $question, ?Country $country)
	{
		if (count($question->getCountries()) === 0) {
			return true;
		}

		foreach ($question->getCountries() as $questionCountry) {
			if ($country !== null && $questionCountry->getCountry()->getId() === $country->getId()) {
				return true;
			}
		}

		return false;
	}


	/**
	 * Check if question belongs to provided host
	 * @param Question $question
	 * @param Host|null $host
	 * @return bool
	 */
	private function hasHost(Question $question, ?Host $host)
	{
		if (count($question->getHosts()) === 0) {
			return true;
		}

		foreach ($question->getHosts() as $questionHost) {
			if ($host !== null && $questionHost->getHost()->getId() === $host->getId()) {
				return true;
			}
		}

		return false;
	}


	/**
	 * Check if question belongs to provided registration
	 * @param Question $question
	 * @param array $registrations
	 * @return bool
	 */
	private function hasRegistration(Question $question, array $registrations)
	{
		if (count($question->getRegistrations()) === 0 || count($registrations) === 0) {
			return true;
		}

		foreach ($question->getRegistrations() as $questionRegistration) {
			foreach ($registrations as $registration) {
				if ($questionRegistration->getRegistration()->getId() === $registration->getId()) {
					return true;
				}
			}
		}

		return false;
	}


	/**
	 * Retrieve questions by IDs
	 * @param int[] $ids
	 * @return Question[]
	 * @throws QuestionNotFound
	 */
	public function getQuestionsByIds(array $ids): array
	{
		$ids = array_unique($ids);
		$questions = $this->questionRepository->findByIds($ids);

		if (count($questions) !== count($ids)) {
			throw new QuestionNotFound;
		}

		return $questions;
	}


	/**
	 * Delete question and all of it's answers
	 * @param Question $question
	 * @throws Exception
	 */
	public function deleteQuestion(Question $question): void
	{
		$this->invalidateTrips($question);
		$question->setOrder(-1);
		$question->setDeletedAt(new DateTime);
		$this->questionRepository->persist($question);
	}


	/**
	 * Translate question text parameters
	 * @param Question $question
	 * @param Language $language
	 * @param IQuestionTranslation $translation
	 * @return Question
	 */
	public function translateQuestion(
		Question $question,
		Language $language,
		IQuestionTranslation $translation
	): Question
	{
		if ($translation->getText() !== null && $translation->getText() !== '') {
			$question->addTranslation(new QuestionTranslation($language->getCode(), "text", $translation->getText()));
		} else {
			$question->clearTranslation("text", $language);
		}

		if ($translation->getHelp() !== null && $translation->getHelp() !== '') {
			$question->addTranslation(new QuestionTranslation($language->getCode(), "help", $translation->getHelp()));
		} else {
			$question->clearTranslation("help", $language);
		}

		if ($translation->getPlaceholder() !== null && $translation->getPlaceholder() !== '') {
			$question->addTranslation(new QuestionTranslation($language->getCode(), "placeholder", $translation->getPlaceholder()));
		} else {
			$question->clearTranslation("placeholder", $language);
		}

		if ($translation->getValidationHelp() !== null && $translation->getValidationHelp() !== '') {
			$question->addTranslation(new QuestionTranslation($language->getCode(), "validationHelp", $translation->getValidationHelp()));
		} else {
			$question->clearTranslation("validationHelp", $language);
		}

		$this->questionRepository->persist($question);

		return $question;
	}


	/**
	 * Translate question text parameters
	 * @param QuestionGroup $group
	 * @param Language $language
	 * @param IGroupTranslation $translation
	 * @return QuestionGroup
	 */
	public function translateGroup(
		QuestionGroup $group,
		Language $language,
		IGroupTranslation $translation
	): QuestionGroup
	{
		if ($translation->getName() !== null && $translation->getName() !== '') {
			$group->addTranslation(new QuestionGroupTranslation($language->getCode(), "name", $translation->getName()));
		} else {
			$group->clearTranslation("name", $language);
		}

		$this->groupRepository->persist($group);

		return $group;
	}


	/**
	 * @param QuestionOption $option
	 * @param Language $language
	 * @param IQuestionOptionTranslation $translation
	 * @return QuestionOption
	 */
	public function translateOption(
		QuestionOption $option,
		Language $language,
		IQuestionOptionTranslation $translation
	): QuestionOption
	{
		if ($translation->getText() !== null && $translation->getText() !== '') {
			$option->addTranslation(new QuestionOptionTranslation($language->getCode(), "text", $translation->getText()));
		} else {
			$option->clearTranslation("text", $language);
		}

		$this->optionRepository->persist($option);

		return $option;
	}


	/**
	 * @param QuestionConstraints $constraints
	 * @param IRequest $request
	 * @return QuestionConstraints
	 * @throws QuestionGroupNotFound
	 * @throws CountryNotFound
	 */
	public function queryToConstraints(QuestionConstraints $constraints, IRequest $request): QuestionConstraints
	{
		if ($request->getQuery("search") !== null) {
			$constraints->setSearch($request->getQuery("search"));
		}

		if ($request->getQuery("group") !== null) {
			$group = $this->getGroup((int) $request->getQuery("group"));

			if ($group === null || $group->isDeleted()) {
				throw new QuestionGroupNotFound;
			}

			$constraints->setGroup($group);
		}

		if ($request->getQuery('state') !== null) {
			$constraints->setVisibility([$request->getQuery('state')]);
		}

		if ($request->getQuery('country') !== null) {
			$countryConstraints = (new CountryConstraints)->setCode($request->getQuery('country'));

			$country = $this->countryRepository->findBy($countryConstraints->build());

			if ($country === null) {
				throw new CountryNotFound;
			}

			$constraints->setCountry($country);
		}

		return $constraints;
	}


	/**
	 * Invalidate trips for this question
	 * @param Question $question
	 */
	public function invalidateTrips(Question $question): void
	{
		$this->tripRepository->deleteByCountries(array_map(static function (QuestionCountry $country) {
			return $country->getCountry();
		}, $question->getCountries()));
	}


	/**
	 * Sets visibility for question
	 * @param Question $question
	 * @param QuestionVisibilityPutDTO $questionVisibilityPutDTO
	 */
	public function setVisibility(Question $question, QuestionVisibilityPutDTO $questionVisibilityPutDTO): void
	{
		$this->invalidateTrips($question);
		$question->setVisibility($questionVisibilityPutDTO->getVisibility());
		$this->questionRepository->persist($question);
	}
}
