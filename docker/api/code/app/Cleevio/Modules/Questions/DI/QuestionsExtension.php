<?php

declare(strict_types=1);

namespace Cleevio\Questions\DI;

use Cleevio\Modules\Providers\IPresenterMappingProvider;
use Nette\DI\CompilerExtension;
use Nettrine\ORM\DI\Traits\TEntityMapping;

class QuestionsExtension extends CompilerExtension implements IPresenterMappingProvider
{

	use TEntityMapping;

	public function loadConfiguration()
	{
		$this->compiler->loadConfig(__DIR__ . '/services.neon');
		$this->setEntityMappings(
			[
				'Cleevio\Questions\Entities\Question' => __DIR__ . '/..',
			]
		);
	}


	public function getPresenterMapping(): array
	{
		return ['Questions' => 'Cleevio\\Questions\\*Module\\Presenters\\*Presenter'];
	}

}
