<?php

declare(strict_types=1);

namespace Cleevio\Questions\ApiModule\Presenters;

use Cleevio\Api\Helpers\IRequestContext;
use Cleevio\Countries\Api\DTO\CountriesPutDTO;
use Cleevio\Countries\Api\Response\CountriesResponseBuilder;
use Cleevio\Countries\CountryNotFound;
use Cleevio\Countries\ICountryService;
use Cleevio\Questions\Entities\Questions\QuestionOptionCountry;
use Cleevio\Questions\IQuestionService;
use Cleevio\RestApi\Annotations\ApiRoute;
use Cleevio\RestApi\Annotations\Authenticated;
use Cleevio\RestApi\Annotations\DTO;
use Cleevio\RestApi\Annotations\JsonSchema;
use Cleevio\RestApi\Annotations\Response;
use Cleevio\RestApi\Exceptions\AuthenticationException;
use Cleevio\RestApi\Exceptions\NotFoundException;
use Cleevio\RestApi\Presenters\RestPresenter;
use Cleevio\Users\Entities\User;
use InvalidArgumentException;
use Nette\Http\IResponse;

/**
 * Endpoints for choice question countries
 * @ApiRoute("/api/v1/questions/option/<option>/country",presenter="Questions:Api:OptionCountry", tags={"Question - Choice"}, priority=2)
 */
class OptionCountryPresenter extends RestPresenter
{

	/**
	 * @var IQuestionService
	 */
	private $questionService;

	/**
	 * @var IRequestContext
	 */
	private $requestContext;

	/**
	 * @var ICountryService
	 */
	private $countryService;

	/**
	 * @param IQuestionService $questionService
	 * @param IRequestContext $requestContext
	 * @param ICountryService $countryService
	 */
	public function __construct(IQuestionService $questionService,
								IRequestContext $requestContext,
								ICountryService $countryService)
	{
		parent::__construct();

		$this->questionService = $questionService;
		$this->requestContext = $requestContext;
		$this->countryService = $countryService;
	}

	/**
	 * @ApiRoute("/api/v1/questions/option/<option>/country", method="GET", description="Get all countries set for selected question option")
	 * @Response(200, file="response/countries/countries.json")
	 * @Response(404, code="10004", description="Question option with provided ID was not found")
	 * @Authenticated("admin.questions")
	 * @param int $option
	 */
	public function actionOptionCountryList(int $option): void
	{
		$authenticator = $this->getAuthenticator();

		$user = $authenticator->getUser();

		if (!$user instanceof User) {
			throw new AuthenticationException("users.auth.error.user-not-found", 10001);
		}

		$option = $this->questionService->getOption($option);

		if ($option === null) {
			throw new NotFoundException('questions.question.get.error.option-not-found', 10004);
		}

		$countries = array_map(static function (QuestionOptionCountry $country) {
			return $country->getCountry();
		}, $option->getCountries());

		$this->sendSuccessResponse(new CountriesResponseBuilder($countries, $this->requestContext->getLang()), IResponse::S200_OK);
	}

	/**
	 * @ApiRoute("/api/v1/questions/option/<option>/country", method="PUT", description="Set supported countries for provided question option")
	 * @Response(200, file="response/countries/countries.json")
	 * @DTO("Cleevio\Countries\Api\DTO\CountriesPutDTO")
	 * @JsonSchema("request/questions/putCountries.json")
	 * @Response(404, code="10004", description="Country with provided code was not found")
	 * @Response(404, code="10005", description="Question option with provided ID was not found")
	 * @Authenticated("admin.questions")
	 * @param int $option
	 */
	public function actionOptionCountryPut(int $option): void
	{
		$authenticator = $this->getAuthenticator();

		$user = $authenticator->getUser();

		if (!$user instanceof User) {
			throw new AuthenticationException("users.auth.error.user-not-found", 10001);
		}

		$command = $this->getCommandDTO();

		if (!$command instanceof CountriesPutDTO) {
			throw new InvalidArgumentException;
		}

		$option = $this->questionService->getOption($option);

		if ($option === null) {
			throw new NotFoundException('questions.question.get.error.option-not-found', 10005);
		}

		try {
			$countries = $this->countryService->findByIds($command->getIds());

			$this->questionService->setOptionCountries($option, $countries);

			$this->sendSuccessResponse(new CountriesResponseBuilder($countries, $this->requestContext->getLang()), IResponse::S200_OK);
		} catch (CountryNotFound $e) {
			throw new NotFoundException('countries.country.get.error.country-not-found', 10004);
		}
	}

	/**
	 * @ApiRoute("/api/v1/questions/option/<option>/country/<country>", method="DELETE", description="Delete supported countries for provided question option")
	 * @Response(200, file="response/countries/countries.json")
	 * @Response(404, code="10003", description="Field with provided ID was not found")
	 * @Response(404, code="10004", description="Option with provided code was not found")
	 * @Authenticated("admin.questions")
	 * @param int $option
	 * @param string $country
	 */
	public function actionOptionCountryDelete(int $option, string $country): void
	{
		$authenticator = $this->getAuthenticator();

		$user = $authenticator->getUser();

		if (!$user instanceof User) {
			throw new AuthenticationException("users.auth.error.user-not-found", 10001);
		}

		$option = $this->questionService->getOption($option);

		if ($option === null) {
			throw new NotFoundException('questions.question.get.error.option-not-found', 10005);
		}

		$country = $this->countryService->findByCode($country);

		if ($country === null) {
			throw new NotFoundException('countries.country.get.error.country-not-found', 10004);
		}

		$this->questionService->deleteOptionCountry($option, $country);

		$countries = array_map(static function (QuestionOptionCountry $country) {
			return $country->getCountry();
		}, $option->getCountries());

		$this->sendSuccessResponse(new CountriesResponseBuilder($countries, $this->requestContext->getLang()), IResponse::S200_OK);
	}

}
