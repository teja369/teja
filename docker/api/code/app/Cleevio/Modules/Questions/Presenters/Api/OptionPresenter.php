<?php

declare(strict_types=1);

namespace Cleevio\Questions\ApiModule\Presenters;

use Cleevio\Api\Helpers\IRequestContext;
use Cleevio\Questions\Api\DTO\CreateOptionPostDTO;
use Cleevio\Questions\Api\DTO\CreateOptionPutDTO;
use Cleevio\Questions\Api\Response\Questions\QuestionOptionResponseBuilder;
use Cleevio\Questions\Api\Response\Questions\QuestionOptionsPagedResponseBuilder;
use Cleevio\Questions\Entities\Questions\ChoiceQuestion;
use Cleevio\Questions\IQuestionService;
use Cleevio\Questions\QuestionOptionInvalidText;
use Cleevio\Questions\Repositories\IQuestionOptionRepository;
use Cleevio\Questions\Repositories\QuestionOptionConstraints;
use Cleevio\Repository\Constraints;
use Cleevio\RestApi\Annotations\ApiRoute;
use Cleevio\RestApi\Annotations\Authenticated;
use Cleevio\RestApi\Annotations\DTO;
use Cleevio\RestApi\Annotations\JsonSchema;
use Cleevio\RestApi\Annotations\Queries;
use Cleevio\RestApi\Annotations\Response;
use Cleevio\RestApi\Exceptions\AuthenticationException;
use Cleevio\RestApi\Exceptions\BadRequestException;
use Cleevio\RestApi\Exceptions\NotFoundException;
use Cleevio\RestApi\Presenters\RestPresenter;
use Cleevio\RestApi\Request\PagedRequest;
use Cleevio\RestApi\Response\EmptyResponse;
use Cleevio\Trips\Entities\Trip;
use Cleevio\Trips\Repositories\ITripRepository;
use Cleevio\Users\Entities\User;
use InvalidArgumentException;
use Nette\Http\IResponse;

/**
 * Endpoints for choice question countries
 * @ApiRoute("/api/v1/questions/<question>/option",presenter="Questions:Api:Option", tags={"Question - Choice"}, priority=3)
 */
class OptionPresenter extends RestPresenter
{

	/**
	 * @var IQuestionService
	 */
	private $questionService;

	/**
	 * @var IRequestContext
	 */
	private $requestContext;

	/**
	 * @var ITripRepository
	 */
	private $tripRepository;

	/**
	 * @var IQuestionOptionRepository
	 */
	private $optionRepository;

	/**
	 * @param IQuestionService $questionService
	 * @param IRequestContext $requestContext
	 * @param ITripRepository $tripRepository
	 * @param IQuestionOptionRepository $optionRepository
	 */
	public function __construct(IQuestionService $questionService,
								IRequestContext $requestContext,
								ITripRepository $tripRepository,
								IQuestionOptionRepository $optionRepository)
	{
		parent::__construct();

		$this->questionService = $questionService;
		$this->requestContext = $requestContext;
		$this->tripRepository = $tripRepository;
		$this->optionRepository = $optionRepository;
	}

	/**
	 * @ApiRoute("/api/v1/questions/<question>/option", method="GET", description="Get all options for a question")
	 * @Response(200, file="response/questions/options.json")
	 * @Response(404, code="10004", description="Question with provided ID was not found")
	 * @Response(404, code="10002", description="Trip was not found")
	 * @Queries("queries/questions/options.json")
	 * @Authenticated()
	 * @param int $question
	 */
	public function actionOptionList(int $question): void
	{
		$authenticator = $this->getAuthenticator();

		$user = $authenticator->getUser();

		if (!$user instanceof User) {
			throw new AuthenticationException("users.auth.error.user-not-found", 10001);
		}

		$question = $this->questionService->getQuestion($question);

		if ($question === null || $question->isDeleted() || !$question instanceof ChoiceQuestion) {
			throw new NotFoundException('questions.question.get.error.question-not-found', 10004);
		}

		$trip = null;
		$tripId = $this->getHttpRequest()->getQuery("trip");

		if ($tripId !== null && $tripId !== "") {
			$trip = $this->tripRepository->find($tripId);

			if (!$trip instanceof Trip || $trip->getUser()->getId() !== $user->getId()) {
				throw new NotFoundException('trips.trip.get.error.trip-not-found', 10002);
			}
		}

		$constraints = new QuestionOptionConstraints;

		$constraints->setQuestion($question);

		if ($trip !== null) {
			$constraints->setCountry($trip->getCountry());
		}

		$search = $this->getHttpRequest()->getQuery('search');

		if ($search !== '' && $search !== null) {
			$constraints->setSearch($search);
		}

		$constraints = $constraints
			->setOrderBy("order")
			->setOrderDir("asc");

		$total = $this->optionRepository->count(Constraints::empty());
		$filtered = $this->optionRepository->count($constraints->build());

		$request = PagedRequest::of($this->getHttpRequest());

		$constraints = $request->toConstraints($constraints)->build();

		$options = $this->optionRepository->findAll($constraints);

		$response = (new QuestionOptionsPagedResponseBuilder($request, $options, $this->requestContext->getLang()))
			->setTotal($total)
			->setCurrent($filtered);

		$this->sendSuccessResponse($response, IResponse::S200_OK);
	}

	/**
	 * @ApiRoute("/api/v1/questions/<question>/option", method="POST", description="Create a new option for provided question")
	 * @Response(200, file="response/questions/options.json")
	 * @Response(404, code="10004", description="Question with provided ID was not found")
	 * @Response(400, code="10005", description="Invalid question option text format")
	 * @DTO("Cleevio\Questions\Api\DTO\CreateOptionPostDTO")
	 * @JsonSchema("request/questions/createOption.json")
	 * @Authenticated("admin.questions")
	 * @param int $question
	 */
	public function actionOptionCreate(int $question): void
	{
		$authenticator = $this->getAuthenticator();

		$user = $authenticator->getUser();

		if (!$user instanceof User) {
			throw new AuthenticationException("users.auth.error.user-not-found", 10001);
		}

		$command = $this->getCommandDTO();

		if (!$command instanceof CreateOptionPostDTO) {
			throw new InvalidArgumentException;
		}

		$question = $this->questionService->getQuestion($question);

		if ($question === null || $question->isDeleted() || !$question instanceof ChoiceQuestion) {
			throw new NotFoundException('questions.question.get.error.question-not-found', 10004);
		}

		$this->optionRepository->beginTransaction();

		try {
			foreach ($command->getOptions() as $o) {
				$this->questionService->createOption($question, $o['text']);
			}

			$this->optionRepository->commitTransaction();

			$this->sendSuccessResponse(new EmptyResponse, IResponse::S200_OK);

		} catch (QuestionOptionInvalidText $e) {

			$this->optionRepository->rollbackTransaction();

			throw new BadRequestException('questions.question.get.error.option-invalid-text', 10005);
		}
	}

	/**
	 * @ApiRoute("/api/v1/questions/option/<option>", method="PUT", description="Update option details")
	 * @Response(200, file="response/questions/option.json")
	 * @Response(404, code="10004", description="Option with provided ID was not found")
	 * @Response(400, code="10005", description="Invalid question option text format")
	 * @DTO("Cleevio\Questions\Api\DTO\CreateOptionPutDTO")
	 * @JsonSchema("request/questions/putOption.json")
	 * @Authenticated("admin.questions")
	 * @param int $option
	 */
	public function actionOptionPut(int $option): void
	{
		$authenticator = $this->getAuthenticator();

		$user = $authenticator->getUser();

		if (!$user instanceof User) {
			throw new AuthenticationException("users.auth.error.user-not-found", 10001);
		}

		$command = $this->getCommandDTO();

		if (!$command instanceof CreateOptionPutDTO) {
			throw new InvalidArgumentException;
		}

		$option = $this->questionService->getOption($option);

		if ($option === null) {
			throw new NotFoundException('questions.question.get.error.option-not-found', 10004);
		}

		try {
			$this->questionService->updateOption($option, $command->getText(), $command->getOrder());

			$this->sendSuccessResponse(new QuestionOptionResponseBuilder($option, $this->requestContext->getLang()), IResponse::S200_OK);

		} catch (QuestionOptionInvalidText $e) {
			throw new BadRequestException('questions.question.get.error.option-invalid-text', 10005);
		}
	}

	/**
	 * @ApiRoute("/api/v1/questions/option/<option>", method="GET", description="Get option details")
	 * @Response(200, file="response/questions/option.json")
	 * @Response(404, code="10004", description="Option with provided ID was not found")
	 * @Authenticated()
	 * @param int $option
	 */
	public function actionOptionGet(int $option): void
	{
		$authenticator = $this->getAuthenticator();

		$user = $authenticator->getUser();

		if (!$user instanceof User) {
			throw new AuthenticationException("users.auth.error.user-not-found", 10001);
		}

		$option = $this->questionService->getOption($option);

		if ($option === null) {
			throw new NotFoundException('questions.question.get.error.option-not-found', 10004);
		}

		$this->sendSuccessResponse(new QuestionOptionResponseBuilder($option, $this->requestContext->getLang()), IResponse::S200_OK);
	}

	/**
	 * @ApiRoute("/api/v1/questions/option/<option>", method="DELETE", description="Delete option from provided question")
	 * @Response(200)
	 * @Response(404, code="10004", description="Option with provided ID was not found")
	 * @Authenticated("admin.questions")
	 * @param int $option
	 */
	public function actionOptionDelete(int $option): void
	{
		$authenticator = $this->getAuthenticator();

		$user = $authenticator->getUser();

		if (!$user instanceof User) {
			throw new AuthenticationException("users.auth.error.user-not-found", 10001);
		}

		$option = $this->questionService->getOption($option);

		if ($option === null) {
			throw new NotFoundException('questions.question.get.error.option-not-found', 10004);
		}

		$this->questionService->deleteOption($option);

		$this->sendSuccessResponse(new EmptyResponse, IResponse::S200_OK);
	}
}
