<?php

declare(strict_types=1);

namespace Cleevio\Questions\ApiModule\Presenters;

use Cleevio\Api\Helpers\IRequestContext;
use Cleevio\Countries\CountryNotFound;
use Cleevio\Questions\Api\DTO\CreateQuestionPostDTO;
use Cleevio\Questions\Api\DTO\QuestionVisibilityPutDTO;
use Cleevio\Questions\Api\Response\QuestionResponseBuilder;
use Cleevio\Questions\Api\Response\QuestionsPagedResponseBuilder;
use Cleevio\Questions\IQuestionService;
use Cleevio\Questions\QuestionGroupNotFound;
use Cleevio\Questions\QuestionInvalidText;
use Cleevio\Questions\QuestionParentCircular;
use Cleevio\Questions\QuestionParentGroupInvalid;
use Cleevio\Questions\QuestionParentNotFound;
use Cleevio\Questions\QuestionTypeInvalid;
use Cleevio\Questions\QuestionUnique;
use Cleevio\Questions\Repositories\IQuestionRepository;
use Cleevio\Questions\Repositories\QuestionConstraints;
use Cleevio\Repository\Constraints;
use Cleevio\RestApi\Annotations\ApiRoute;
use Cleevio\RestApi\Annotations\Authenticated;
use Cleevio\RestApi\Annotations\DTO;
use Cleevio\RestApi\Annotations\JsonSchema;
use Cleevio\RestApi\Annotations\Queries;
use Cleevio\RestApi\Annotations\Response;
use Cleevio\RestApi\Exceptions\AuthenticationException;
use Cleevio\RestApi\Exceptions\BadRequestException;
use Cleevio\RestApi\Exceptions\NotFoundException;
use Cleevio\RestApi\Presenters\RestPresenter;
use Cleevio\RestApi\Request\PagedRequest;
use Cleevio\RestApi\Response\EmptyResponse;
use Cleevio\Trips\Entities\Trip;
use Cleevio\Trips\Repositories\ITripRepository;
use Cleevio\Users\Entities\User;
use InvalidArgumentException;
use Nette\Http\IResponse;

/**
 * @ApiRoute("/api/v1/questions",presenter="Questions:Api:Question", tags={"Question"}, priority=6)
 */
class QuestionPresenter extends RestPresenter
{

	/**
	 * @var ITripRepository
	 */
	private $tripRepository;

	/**
	 * @var IQuestionService
	 */
	private $questionService;

	/**
	 * @var IRequestContext
	 */
	private $requestContext;

	/**
	 * @var IQuestionRepository
	 */
	private $questionRepository;

	/**
	 * @param ITripRepository $tripRepository
	 * @param IQuestionService $questionService
	 * @param IQuestionRepository $questionRepository
	 * @param IRequestContext $requestContext
	 */
	public function __construct(ITripRepository $tripRepository,
								IQuestionService $questionService,
								IQuestionRepository $questionRepository,
								IRequestContext $requestContext)
	{
		parent::__construct();

		$this->tripRepository = $tripRepository;
		$this->questionService = $questionService;
		$this->requestContext = $requestContext;
		$this->questionRepository = $questionRepository;
	}

	/**
	 * @ApiRoute("/api/v1/questions", method="POST", description="Create a new question")
	 * @Response(200, file="response/questions/question.json")
	 * @Response(404, code="10002", description="Questions group was not found")
	 * @Response(404, code="10003", description="Parent question was not found")
	 * @Response(400, code="10004", description="Question and parent question must be in the same group")
	 * @Response(400, code="10005", description="Question has a circular parent dependency")
	 * @Response(400, code="10006", description="Question text must be unique")
	 * @Response(400, code="10007", description="Question text is invalid")
	 * @DTO("Cleevio\Questions\Api\DTO\CreateQuestionPostDTO")
	 * @JsonSchema("request/questions/createQuestion.json")
	 * @Authenticated("admin.questions")
	 */
	public function actionQuestionsCreate(): void
	{
		$authenticator = $this->getAuthenticator();

		$user = $authenticator->getUser();

		if (!$user instanceof User) {
			throw new AuthenticationException("users.auth.error.user-not-found", 10001);
		}

		$command = $this->getCommandDTO();

		if (!$command instanceof CreateQuestionPostDTO) {
			throw new InvalidArgumentException;
		}

		$group = $this->questionService->getGroup($command->getGroup());

		if ($group === null) {
			throw new NotFoundException('questions.group.get.error.group-not-found', 10002);
		}

		try {
			$question = $this->questionService->createQuestion($command, $group);

			$this->sendSuccessResponse(new QuestionResponseBuilder($question, null, $this->requestContext->getLang()), IResponse::S200_OK);

		} catch (QuestionTypeInvalid $e) {
			throw new BadRequestException($e->getMessage());
		} catch (QuestionParentNotFound $e) {
			throw new BadRequestException('questions.question.put.error.parent-not-found', 10003);
		} catch (QuestionParentGroupInvalid $e) {
			throw new BadRequestException('questions.question.put.error.parent-group-invalid', 10004);
		} catch (QuestionParentCircular $e) {
			throw new BadRequestException('questions.question.put.error.parent-circular', 10005);
		} catch (QuestionUnique $e) {
			throw new BadRequestException('questions.question.put.error.text-unique', 10006);
		} catch (QuestionInvalidText $e) {
			throw new BadRequestException('questions.question.put.error.text-invalid', 10007);
		}
	}

	/**
	 * @ApiRoute("/api/v1/questions", method="GET", description="Fetches all available questions")
	 * @Response(200, file="response/questions/questionsPaged.json")
	 * @Response(404, code="10003", description="Questions group was not found")
	 * @Response(404, code="10002", description="Country was not found")
	 * @Queries("queries/questions/pager.json")
	 * @Authenticated("admin.questions")
	 */
	public function actionQuestionsList(): void
	{
		$authenticator = $this->getAuthenticator();

		$user = $authenticator->getUser();

		if (!$user instanceof User) {
			throw new AuthenticationException("users.auth.error.user-not-found", 10003);
		}

		try {
			$questionConstraints = (new QuestionConstraints)
				->setIsDeleted(false);

			$questionConstraints = $this->questionService->queryToConstraints($questionConstraints, $this->getHttpRequest());

			$questionConstraints
				->setOrderBy("order")
				->setOrderDir("asc");

			$total = $this->questionRepository->count(Constraints::empty());
			$filtered = $this->questionRepository->count($questionConstraints->build());

			$request = PagedRequest::of($this->getHttpRequest());

			$constraints = $request->toConstraints($questionConstraints)->build();

			$questions = $this->questionRepository->findAll($constraints);

			$response = (new QuestionsPagedResponseBuilder($request, $questions, $this->requestContext->getLang()))
				->setCurrent($filtered)
				->setTotal($total);

			$this->sendSuccessResponse($response, IResponse::S200_OK);
		} catch (QuestionGroupNotFound $e) {
			throw new NotFoundException('questions.group.get.error.group-not-found', 10003);
		} catch (CountryNotFound $e) {
			throw new BadRequestException('countries.country.get.error.country-not-found', 10002);
		}
	}

	/**
	 * @ApiRoute("/api/v1/questions/<question>", method="PUT", description="Update question information")
	 * @Response(200, file="response/questions/question.json")
	 * @Response(404, code="10001", description="Question was not found")
	 * @Response(404, code="10002", description="Questions group was not found")
	 * @Response(404, code="10003", description="Parent question was not found")
	 * @Response(400, code="10004", description="Question and parent question must be in the same group")
	 * @Response(400, code="10005", description="Question has a circular parent dependency")
	 * @Response(400, code="10006", description="Question text must be unique")
	 * @Response(400, code="10007", description="Question text is invalid")
	 * @DTO("Cleevio\Questions\Api\DTO\CreateQuestionPostDTO")
	 * @JsonSchema("request/questions/createQuestion.json")
	 * @Authenticated("admin.questions")
	 * @param int $question
	 */
	public function actionQuestionsPut(int $question): void
	{
		$authenticator = $this->getAuthenticator();

		$user = $authenticator->getUser();

		if (!$user instanceof User) {
			throw new AuthenticationException("users.auth.error.user-not-found", 10003);
		}

		$question = $this->questionService->getQuestion($question);

		if ($question === null || $question->isDeleted()) {
			throw new NotFoundException('questions.question.get.error.question-not-found', 10001);
		}

		$command = $this->getCommandDTO();

		if (!$command instanceof CreateQuestionPostDTO) {
			throw new InvalidArgumentException;
		}

		$group = $this->questionService->getGroup($command->getGroup());

		if ($group === null) {
			throw new NotFoundException('questions.group.get.error.group-not-found', 10002);
		}

		try {
			$question = $this->questionService->updateQuestion($question, $command, $group);

			$this->sendSuccessResponse(new QuestionResponseBuilder($question, null, $this->requestContext->getLang()), IResponse::S200_OK);

		} catch (QuestionTypeInvalid $e) {
			throw new BadRequestException($e->getMessage());
		} catch (QuestionParentNotFound $e) {
			throw new BadRequestException('questions.question.put.error.parent-not-found', 10003);
		} catch (QuestionParentGroupInvalid $e) {
			throw new BadRequestException('questions.question.put.error.parent-group-invalid', 10004);
		} catch (QuestionParentCircular $e) {
			throw new BadRequestException('questions.question.put.error.parent-circular', 10005);
		} catch (QuestionUnique $e) {
			throw new BadRequestException('questions.question.put.error.text-unique', 10006);
		} catch (QuestionInvalidText $e) {
			throw new BadRequestException('questions.question.put.error.text-invalid', 10007);
		}
	}

	/**
	 * @ApiRoute("/api/v1/questions/<question>", method="GET", description="Get single question")
	 * @Response(200, file="response/questions/question.json")
	 * @Response(404, code="10001", description="Question was not found")
	 * @Response(404, code="10002", description="Trip was not found")
	 * @Queries("queries/questions/question.json")
	 * @Authenticated("admin.questions")
	 * @param int $question
	 */
	public function actionQuestionsGet(int $question): void
	{
		$authenticator = $this->getAuthenticator();

		$user = $authenticator->getUser();

		if (!$user instanceof User) {
			throw new AuthenticationException("users.auth.error.user-not-found", 10003);
		}

		$trip = null;
		$tripId = $this->getHttpRequest()->getQuery("trip");

		if ($tripId !== null && $tripId !== "") {
			$trip = $this->tripRepository->find($tripId);

			if (!$trip instanceof Trip || $trip->getUser()->getId() !== $user->getId()) {
				throw new NotFoundException('trips.trip.get.error.trip-not-found', 10002);
			}
		}

		$question = $this->questionService->getQuestion($question);

		if ($question === null || $question->isDeleted()) {
			throw new NotFoundException('questions.question.get.error.question-not-found', 10001);
		}

		$this->sendSuccessResponse(new QuestionResponseBuilder($question, $trip, $this->requestContext->getLang()), IResponse::S200_OK);
	}

	/**
	 * @ApiRoute("/api/v1/questions/<question>", method="DELETE", description="Delete question and its answers and depending questions")
	 * @Response(200)
	 * @Response(404, code="10001", description="Question was not found")
	 * @Authenticated("admin.questions")
	 * @param int $question
	 */
	public function actionQuestionsDelete(int $question): void
	{
		$authenticator = $this->getAuthenticator();

		$user = $authenticator->getUser();

		if (!$user instanceof User) {
			throw new AuthenticationException("users.auth.error.user-not-found", 10003);
		}

		$question = $this->questionService->getQuestion($question);

		if ($question === null || $question->isDeleted()) {
			throw new NotFoundException('questions.question.get.error.question-not-found', 10001);
		}

		$this->questionService->deleteQuestion($question);

		$this->sendSuccessResponse(new EmptyResponse, IResponse::S200_OK);
	}

	/**
	 * @ApiRoute("/api/v1/questions/<question>/visibility", method="PUT", description="Sets visibility of the question")
	 * @Response(200)
	 * @Response(404, code="10001", description="Question was not found")
	 * @DTO("Cleevio\Questions\Api\DTO\QuestionVisibilityPutDTO")
	 * @JsonSchema("request/questions/putQuestionVisibility.json")
	 * @Authenticated()
	 * @param int $question
	 */
	public function actionQuestionVisibility(int $question): void
	{
		$authenticator = $this->getAuthenticator();

		$user = $authenticator->getUser();

		if (!$user instanceof User) {
			throw new AuthenticationException("users.auth.error.user-not-found", 10003);
		}

		$question = $this->questionService->getQuestion($question);

		if ($question === null || $question->isDeleted()) {
			throw new NotFoundException('questions.question.get.error.question-not-found', 10001);
		}

		$command = $this->getCommandDTO();

		if (!$command instanceof QuestionVisibilityPutDTO) {
			throw new InvalidArgumentException;
		}

		$this->questionService->setVisibility($question, $command);

		$this->sendSuccessResponse(new EmptyResponse, IResponse::S200_OK);
	}

}
