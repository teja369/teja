<?php

declare(strict_types=1);

namespace Cleevio\Questions\ApiModule\Presenters;

use Cleevio\Languages\ILanguageService;
use Cleevio\Questions\Api\DTO\TranslationPutDTO;
use Cleevio\Questions\Api\Response\TranslationResponseBuilder;
use Cleevio\Questions\IQuestionService;
use Cleevio\RestApi\Annotations\ApiRoute;
use Cleevio\RestApi\Annotations\Authenticated;
use Cleevio\RestApi\Annotations\DTO;
use Cleevio\RestApi\Annotations\JsonSchema;
use Cleevio\RestApi\Annotations\Response;
use Cleevio\RestApi\Exceptions\AuthenticationException;
use Cleevio\RestApi\Exceptions\NotFoundException;
use Cleevio\RestApi\Presenters\RestPresenter;
use Cleevio\Users\Entities\User;
use InvalidArgumentException;
use Nette\Http\IResponse;

/**
 * @ApiRoute("/api/v1/questions",presenter="Questions:Api:Translation", tags={"Question"}, priority=3)
 */
class TranslationPresenter extends RestPresenter
{

	/**
	 * @var IQuestionService
	 */
	private $questionService;

	/**
	 * @var ILanguageService
	 */
	private $languageService;

	/**
	 * @param IQuestionService $questionService
	 * @param ILanguageService $languageService
	 */
	public function __construct(IQuestionService $questionService,
								ILanguageService $languageService)
	{
		parent::__construct();

		$this->questionService = $questionService;
		$this->languageService = $languageService;
	}

	/**
	 * @ApiRoute("/api/v1/questions/<question>/translation/<lang>", method="GET", description="Get all translations set for selected question")
	 * @Response(200, file="response/questions/translation.json")
	 * @Response(404, code="10002", description="Langauge with provided code was not found")
	 * @Response(404, code="10003", description="Question with provided ID was not found")
	 * @Authenticated("admin.questions")
	 * @param int $question
	 * @param string $lang
	 */
	public function actionList(int $question, string $lang): void
	{
		$authenticator = $this->getAuthenticator();

		$user = $authenticator->getUser();

		if (!$user instanceof User) {
			throw new AuthenticationException("users.auth.error.user-not-found", 10001);
		}

		$question = $this->questionService->getQuestion($question);

		if ($question === null || $question->isDeleted()) {
			throw new NotFoundException('questions.question.get.error.question-not-found', 10003);
		}

		$language = $this->languageService->getLanguage($lang);

		if ($language === null) {
			throw new NotFoundException('languages.language.get.not-found', 10002);
		}

		$this->sendSuccessResponse(new TranslationResponseBuilder($question, $lang), IResponse::S200_OK);
	}

	/**
	 * @ApiRoute("/api/v1/questions/<question>/translation/<lang>", method="PUT", description="Update translations for a provided questions")
	 * @Response(200, file="response/questions/translation.json")
	 * @DTO("Cleevio\Questions\Api\DTO\TranslationPutDTO")
	 * @JsonSchema("request/questions/putTranslation.json")
	 * @Response(404, code="10002", description="Langauge with provided code was not found")
	 * @Response(404, code="10003", description="Question with provided ID was not found")
	 * @Authenticated("admin.questions")
	 * @param int $question
	 * @param string $lang
	 */
	public function actionPut(int $question, string $lang): void
	{
		$authenticator = $this->getAuthenticator();

		$user = $authenticator->getUser();

		if (!$user instanceof User) {
			throw new AuthenticationException("users.auth.error.user-not-found", 10001);
		}

		$command = $this->getCommandDTO();

		if (!$command instanceof TranslationPutDTO) {
			throw new InvalidArgumentException;
		}

		$question = $this->questionService->getQuestion($question);

		if ($question === null || $question->isDeleted()) {
			throw new NotFoundException('questions.question.get.error.question-not-found', 10003);
		}

		$language = $this->languageService->getLanguage($lang);

		if ($language === null) {
			throw new NotFoundException('languages.language.get.not-found', 10002);
		}

		$this->questionService->translateQuestion($question, $language, $command);

		$this->sendSuccessResponse(new TranslationResponseBuilder($question, $lang), IResponse::S200_OK);
	}

}
