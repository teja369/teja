<?php

declare(strict_types=1);

namespace Cleevio\Questions\ApiModule\Presenters;

use Cleevio\Questions\Api\DTO\RegistrationsPutDTO;
use Cleevio\Questions\IQuestionService;
use Cleevio\Questions\RegistrationNotFound;
use Cleevio\Registrations\Api\Response\RegistrationsResponseBuilder;
use Cleevio\Registrations\Entities\RegistrationQuestion;
use Cleevio\Registrations\IRegistrationService;
use Cleevio\RestApi\Annotations\ApiRoute;
use Cleevio\RestApi\Annotations\Authenticated;
use Cleevio\RestApi\Annotations\DTO;
use Cleevio\RestApi\Annotations\JsonSchema;
use Cleevio\RestApi\Annotations\Response;
use Cleevio\RestApi\Exceptions\AuthenticationException;
use Cleevio\RestApi\Exceptions\NotFoundException;
use Cleevio\RestApi\Presenters\RestPresenter;
use Cleevio\Users\Entities\User;
use InvalidArgumentException;
use Nette\Http\IResponse;

/**
 * @ApiRoute("/api/v1/questions/<question>/registration",presenter="Questions:Api:Registration", tags={"Question"}, priority=4)
 */
class RegistrationPresenter extends RestPresenter
{

	/**
	 * @var IQuestionService
	 */
	private $questionService;

	/**
	 * @var IRegistrationService
	 */
	private $registrationService;

	/**
	 * @param IQuestionService $questionService
	 * @param IRegistrationService $registrationService
	 */
	public function __construct(IQuestionService $questionService,
								IRegistrationService $registrationService)
	{
		parent::__construct();

		$this->questionService = $questionService;
		$this->registrationService = $registrationService;
	}

	/**
	 * @ApiRoute("/api/v1/questions/<question>/registration", method="GET", description="Get all registrations set for selected question")
	 * @Response(200, file="response/registrations/registrations.json")
	 * @Response(404, code="10003", description="Question with provided ID was not found")
	 * @Authenticated("admin.questions")
	 * @param int $question
	 */
	public function actionRegistrationList(int $question): void
	{
		$authenticator = $this->getAuthenticator();

		$user = $authenticator->getUser();

		if (!$user instanceof User) {
			throw new AuthenticationException("users.auth.error.user-not-found", 10001);
		}

		$question = $this->questionService->getQuestion($question);

		if ($question === null || $question->isDeleted()) {
			throw new NotFoundException('questions.question.get.error.question-not-found', 10003);
		}

		$registrations = array_map(static function (RegistrationQuestion $registration) {
			return $registration->getRegistration();
		}, $question->getRegistrations());

		$this->sendSuccessResponse(new RegistrationsResponseBuilder($registrations), IResponse::S200_OK);
	}

	/**
	 * @ApiRoute("/api/v1/questions/<question>/registration", method="PUT", description="Set supported registrations for provided question")
	 * @Response(200, file="response/registrations/registrations.json")
	 * @DTO("Cleevio\Questions\Api\DTO\RegistrationsPutDTO")
	 * @JsonSchema("request/questions/putRegistrations.json")
	 * @Response(404, code="10003", description="Question with provided ID was not found")
	 * @Response(404, code="10004", description="Registrations with provided id was not found")
	 * @Authenticated("admin.questions")
	 * @param int $question
	 */
	public function actionRegistrationPut(int $question): void
	{
		$authenticator = $this->getAuthenticator();

		$user = $authenticator->getUser();

		if (!$user instanceof User) {
			throw new AuthenticationException("users.auth.error.user-not-found", 10001);
		}

		$command = $this->getCommandDTO();

		if (!$command instanceof RegistrationsPutDTO) {
			throw new InvalidArgumentException;
		}

		$question = $this->questionService->getQuestion($question);

		if ($question === null || $question->isDeleted()) {
			throw new NotFoundException('questions.question.get.error.question-not-found', 10003);
		}

		try {
			$registrations = $this->registrationService->findByIds($command->getIds());

			$this->questionService->setRegistrations($question, $registrations);

			$this->sendSuccessResponse(new RegistrationsResponseBuilder($registrations), IResponse::S200_OK);
		} catch (RegistrationNotFound $e) {
			throw new NotFoundException('registrations.registration.get.error.registration-not-found', 10004);
		}
	}

}
