<?php

declare(strict_types=1);

namespace Cleevio\Questions\ApiModule\Presenters;

use Cleevio\Languages\ILanguageService;
use Cleevio\Questions\Api\DTO\GroupTranslationPutDTO;
use Cleevio\Questions\Api\Response\GroupTranslationResponseBuilder;
use Cleevio\Questions\IQuestionService;
use Cleevio\RestApi\Annotations\ApiRoute;
use Cleevio\RestApi\Annotations\Authenticated;
use Cleevio\RestApi\Annotations\DTO;
use Cleevio\RestApi\Annotations\JsonSchema;
use Cleevio\RestApi\Annotations\Response;
use Cleevio\RestApi\Exceptions\AuthenticationException;
use Cleevio\RestApi\Exceptions\NotFoundException;
use Cleevio\RestApi\Presenters\RestPresenter;
use Cleevio\Users\Entities\User;
use InvalidArgumentException;
use Nette\Http\IResponse;

/**
 * @ApiRoute("/api/v1/questions/groups",presenter="Questions:Api:GroupTranslation", tags={"Question - Group"})
 */
class GroupTranslationPresenter extends RestPresenter
{

	/**
	 * @var IQuestionService
	 */
	private $questionService;

	/**
	 * @var ILanguageService
	 */
	private $languageService;

	/**
	 * @param IQuestionService $questionService
	 * @param ILanguageService $languageService
	 */
	public function __construct(IQuestionService $questionService,
								ILanguageService $languageService)
	{
		parent::__construct();

		$this->questionService = $questionService;
		$this->languageService = $languageService;
	}

	/**
	 * @ApiRoute("/api/v1/questions/groups/<group>/translation/<lang>", method="GET", description="Get all translations set for selected question group")
	 * @Response(200, file="response/questions/groupTranslation.json")
	 * @Response(404, code="10002", description="Langauge with provided code was not found")
	 * @Response(404, code="10003", description="Question group with provided ID was not found")
	 * @Authenticated("admin.questions")
	 * @param int $group
	 * @param string $lang
	 */
	public function actionList(int $group, string $lang): void
	{
		$authenticator = $this->getAuthenticator();

		$user = $authenticator->getUser();

		if (!$user instanceof User) {
			throw new AuthenticationException("users.auth.error.user-not-found", 10001);
		}

		$group = $this->questionService->getGroup($group);

		if ($group === null) {
			throw new NotFoundException('questions.question.get.error.group-not-found', 10003);
		}

		$language = $this->languageService->getLanguage($lang);

		if ($language === null) {
			throw new NotFoundException('languages.language.get.not-found', 10002);
		}

		$this->sendSuccessResponse(new GroupTranslationResponseBuilder($group, $lang), IResponse::S200_OK);
	}

	/**
	 * @ApiRoute("/api/v1/questions/groups/<group>/translation/<lang>", method="PUT", description="Update translations for a provided question group")
	 * @Response(200, file="response/questions/groupTranslation.json")
	 * @DTO("Cleevio\Questions\Api\DTO\GroupTranslationPutDTO")
	 * @JsonSchema("request/questions/putGroupTranslation.json")
	 * @Response(404, code="10002", description="Langauge with provided code was not found")
	 * @Response(404, code="10003", description="Question group with provided ID was not found")
	 * @Authenticated("admin.questions")
	 * @param int $group
	 * @param string $lang
	 */
	public function actionPut(int $group, string $lang): void
	{
		$authenticator = $this->getAuthenticator();

		$user = $authenticator->getUser();

		if (!$user instanceof User) {
			throw new AuthenticationException("users.auth.error.user-not-found", 10001);
		}

		$command = $this->getCommandDTO();

		if (!$command instanceof GroupTranslationPutDTO) {
			throw new InvalidArgumentException;
		}

		$group = $this->questionService->getGroup($group);

		if ($group === null) {
			throw new NotFoundException('questions.question.get.error.group-not-found', 10003);
		}

		$language = $this->languageService->getLanguage($lang);

		if ($language === null) {
			throw new NotFoundException('languages.language.get.not-found', 10002);
		}

		$this->questionService->translateGroup($group, $language, $command);

		$this->sendSuccessResponse(new GroupTranslationResponseBuilder($group, $lang), IResponse::S200_OK);
	}

}
