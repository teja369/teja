<?php

declare(strict_types=1);

namespace Cleevio\Questions\ApiModule\Presenters;

use Cleevio\Languages\ILanguageService;
use Cleevio\Questions\Api\DTO\OptionTranslationPutDTO;
use Cleevio\Questions\Api\Response\Questions\QuestionOptionTranslationResponseBuilder;
use Cleevio\Questions\IQuestionService;
use Cleevio\RestApi\Annotations\ApiRoute;
use Cleevio\RestApi\Annotations\Authenticated;
use Cleevio\RestApi\Annotations\DTO;
use Cleevio\RestApi\Annotations\JsonSchema;
use Cleevio\RestApi\Annotations\Response;
use Cleevio\RestApi\Exceptions\AuthenticationException;
use Cleevio\RestApi\Exceptions\NotFoundException;
use Cleevio\RestApi\Presenters\RestPresenter;
use Cleevio\Users\Entities\User;
use InvalidArgumentException;
use Nette\Http\IResponse;

/**
 * @ApiRoute("/api/v1/questions/option/<option>/translation",presenter="Questions:Api:OptionTranslation", tags={"Question - Choice"})
 */
class OptionTranslationPresenter extends RestPresenter
{

	/**
	 * @var IQuestionService
	 */
	private $questionService;

	/**
	 * @var ILanguageService
	 */
	private $languageService;

	/**
	 * @param IQuestionService $questionService
	 * @param ILanguageService $languageService
	 */
	public function __construct(IQuestionService $questionService,
								ILanguageService $languageService)
	{
		parent::__construct();

		$this->questionService = $questionService;
		$this->languageService = $languageService;
	}

	/**
	 * @ApiRoute("/api/v1/questions/option/<option>/translation/<lang>", method="GET", description="Get all translations set for selected question option")
	 * @Response(200, file="response/questions/optionTranslation.json")
	 * @Response(404, code="10002", description="Langauge with provided code was not found")
	 * @Response(404, code="10003", description="Question option with provided ID was not found")
	 * @Authenticated("admin.questions")
	 * @param int $option
	 * @param string $lang
	 */
	public function actionList(int $option, string $lang): void
	{
		$authenticator = $this->getAuthenticator();

		$user = $authenticator->getUser();

		if (!$user instanceof User) {
			throw new AuthenticationException("users.auth.error.user-not-found", 10001);
		}

		$option = $this->questionService->getOption($option);

		if ($option === null) {
			throw new NotFoundException('questions.question.get.error.option-not-found', 10003);
		}

		$language = $this->languageService->getLanguage($lang);

		if ($language === null) {
			throw new NotFoundException('languages.language.get.not-found', 10002);
		}

		$this->sendSuccessResponse(new QuestionOptionTranslationResponseBuilder($option, $lang), IResponse::S200_OK);
	}

	/**
	 * @ApiRoute("/api/v1/questions/option/<option>/translation/<lang>", method="PUT", description="Update translations for a provided question option")
	 * @Response(200, file="response/questions/optionTranslation.json")
	 * @DTO("Cleevio\Questions\Api\DTO\OptionTranslationPutDTO")
	 * @JsonSchema("request/questions/putOptionTranslation.json")
	 * @Response(404, code="10002", description="Langauge with provided code was not found")
	 * @Response(404, code="10003", description="Question option with provided ID was not found")
	 * @Authenticated("admin.questions")
	 * @param int $option
	 * @param string $lang
	 */
	public function actionPut(int $option, string $lang): void
	{
		$authenticator = $this->getAuthenticator();

		$user = $authenticator->getUser();

		if (!$user instanceof User) {
			throw new AuthenticationException("users.auth.error.user-not-found", 10001);
		}

		$command = $this->getCommandDTO();

		if (!$command instanceof OptionTranslationPutDTO) {
			throw new InvalidArgumentException;
		}

		$option = $this->questionService->getOption($option);

		if ($option === null) {
			throw new NotFoundException('questions.question.get.error.option-not-found', 10003);
		}

		$language = $this->languageService->getLanguage($lang);

		if ($language === null) {
			throw new NotFoundException('languages.language.get.not-found', 10002);
		}

		$this->questionService->translateOption($option, $language, $command);

		$this->sendSuccessResponse(new QuestionOptionTranslationResponseBuilder($option, $lang), IResponse::S200_OK);
	}

}
