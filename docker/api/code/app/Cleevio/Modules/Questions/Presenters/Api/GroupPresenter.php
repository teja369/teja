<?php

declare(strict_types=1);

namespace Cleevio\Questions\ApiModule\Presenters;

use Cleevio\Api\Helpers\IRequestContext;
use Cleevio\Questions\Api\DTO\CreateGroupPostDTO;
use Cleevio\Questions\Api\Response\GroupResponseBuilder;
use Cleevio\Questions\Api\Response\GroupsResponseBuilder;
use Cleevio\Questions\Api\Response\QuestionsResponseBuilder;
use Cleevio\Questions\Entities\QuestionGroup;
use Cleevio\Questions\IQuestionService;
use Cleevio\RestApi\Annotations\ApiRoute;
use Cleevio\RestApi\Annotations\Authenticated;
use Cleevio\RestApi\Annotations\DTO;
use Cleevio\RestApi\Annotations\JsonSchema;
use Cleevio\RestApi\Annotations\Queries;
use Cleevio\RestApi\Annotations\Response;
use Cleevio\RestApi\Exceptions\AuthenticationException;
use Cleevio\RestApi\Exceptions\BadRequestException;
use Cleevio\RestApi\Exceptions\NotFoundException;
use Cleevio\RestApi\Presenters\RestPresenter;
use Cleevio\RestApi\Response\EmptyResponse;
use Cleevio\Trips\Entities\Trip;
use Cleevio\Trips\ITripService;
use Cleevio\Trips\Repositories\ITripRepository;
use Cleevio\Trips\TripService;
use Cleevio\Trips\ValidationException;
use Cleevio\Users\Entities\User;
use InvalidArgumentException;
use Nette\Http\IResponse;

/**
 * @ApiRoute("/api/v1/questions/groups",presenter="Questions:Api:Group", tags={"Question - Group"})
 */
class GroupPresenter extends RestPresenter
{

	/**
	 * @var ITripRepository
	 */
	private $tripRepository;

	/**
	 * @var IQuestionService
	 */
	private $questionService;

	/**
	 * @var IRequestContext
	 */
	private $requestContext;
	/**
	 * @var ITripService
	 */
	private $tripService;

	/**
	 * @param ITripRepository $tripRepository
	 * @param IQuestionService $questionService
	 * @param ITripService $tripService
	 * @param IRequestContext $requestContext
	 */
	public function __construct(ITripRepository $tripRepository,
								IQuestionService $questionService,
								ITripService $tripService,
								IRequestContext $requestContext)
	{
		parent::__construct();

		$this->tripRepository = $tripRepository;
		$this->questionService = $questionService;
		$this->requestContext = $requestContext;
		$this->tripService = $tripService;
	}

	/**
	 * @ApiRoute("/api/v1/questions/groups", method="GET", description="Fetches all available question groups")
	 * @Response(200, file="response/questions/groups.json")
	 * @Response(404, code="10002", description="Trip was not found or belongs to another user")
	 * @Response(400, code="10005", description="Trip validation failed")
	 * @Queries("queries/questions/groups.json")
	 * @Authenticated()
	 */
	public function actionGroupsList(): void
	{
		$authenticator = $this->getAuthenticator();

		$user = $authenticator->getUser();

		if (!$user instanceof User) {
			throw new AuthenticationException("users.auth.error.user-not-found", 10003);
		}

		$trip = null;
		$tripId = $this->getHttpRequest()->getQuery("trip");

		if ($tripId !== null) {
			$trip = $this->tripRepository->find($tripId);

			if (!$trip instanceof Trip || $trip->getUser()->getId() !== $user->getId()) {
				throw new NotFoundException('trips.trip.get.error.trip-not-found', 10002);
			}
		}

		$groups = $this->questionService->getGroups($trip, $this->getHttpRequest()->getQuery('type'));

		$this->sendSuccessResponse(new GroupsResponseBuilder($groups, $this->requestContext->getLang()), IResponse::S200_OK);
	}

	/**
	 * @ApiRoute("/api/v1/questions/groups", method="POST", description="Create a new question group")
	 * @Response(200, file="response/questions/group.json")
	 * @DTO("Cleevio\Questions\Api\DTO\CreateGroupPostDTO")
	 * @JsonSchema("request/questions/createGroup.json")
	 * @Authenticated("admin.questions")
	 */
	public function actionGroupsCreate(): void
	{
		$authenticator = $this->getAuthenticator();

		$user = $authenticator->getUser();

		if (!$user instanceof User) {
			throw new AuthenticationException("users.auth.error.user-not-found", 10003);
		}

		$command = $this->getCommandDTO();

		if (!$command instanceof CreateGroupPostDTO) {
			throw new InvalidArgumentException;
		}

		try {
			$group = $this->questionService->createGroup($command->getName(), $command->getType());
		} catch (InvalidArgumentException $e) {
			throw new BadRequestException($e->getMessage());
		}

		$this->sendSuccessResponse(new GroupResponseBuilder($group, $this->requestContext->getLang()), IResponse::S200_OK);
	}

	/**
	 * @ApiRoute("/api/v1/questions/groups/<group>", method="PUT", description="Edit group")
	 * @Response(200, file="response/questions/group.json")
	 * @Response(404, code="10003", description="Questions group was not found")
	 * @DTO("Cleevio\Questions\Api\DTO\CreateGroupPostDTO")
	 * @JsonSchema("request/questions/createGroup.json")
	 * @Authenticated("admin.questions")
	 * @param int $group
	 */
	public function actionGroupsPut(int $group): void
	{
		$authenticator = $this->getAuthenticator();

		$user = $authenticator->getUser();

		if (!$user instanceof User) {
			throw new AuthenticationException("users.auth.error.user-not-found", 10002);
		}

		$command = $this->getCommandDTO();

		if (!$command instanceof CreateGroupPostDTO) {
			throw new InvalidArgumentException;
		}

		$group = $this->questionService->getGroup($group);

		if ($group === null || $group->isDeleted()) {
			throw new NotFoundException('questions.group.get.error.group-not-found', 10003);
		}

		try {
			$group = $this->questionService->updateGroup($group, $command->getName(), $command->getType());

		} catch (InvalidArgumentException $e) {
			throw new BadRequestException($e->getMessage());
		}

		$this->sendSuccessResponse(new GroupResponseBuilder($group, $this->requestContext->getLang()), IResponse::S200_OK);
	}

	/**
	 * @ApiRoute("/api/v1/questions/groups/<group>", method="GET", description="Get group by ID")
	 * @Response(200, file="response/questions/group.json")
	 * @Response(404, code="10003", description="Questions group was not found")
	 * @Queries("queries/questions/groups.json")
	 * @Authenticated("admin.questions")
	 * @param int $group
	 */
	public function actionGroupsGet(int $group): void
	{
		$authenticator = $this->getAuthenticator();

		$user = $authenticator->getUser();

		if (!$user instanceof User) {
			throw new AuthenticationException("users.auth.error.user-not-found", 10002);
		}

		$group = $this->questionService->getGroup($group);

		if ($group === null || $group->isDeleted()) {
			throw new NotFoundException('questions.group.get.error.group-not-found', 10003);
		}

		$this->sendSuccessResponse(new GroupResponseBuilder($group, $this->requestContext->getLang()), IResponse::S200_OK);

	}

	/**
	 * @ApiRoute("/api/v1/questions/groups/<group>", method="DELETE", description="Delete group and all of its questions")
	 * @Response(404, code="10003", description="Questions group was not found")
	 * @Authenticated("admin.questions")
	 * @param int $group
	 */
	public function actionGroupsDelete(int $group): void
	{
		$authenticator = $this->getAuthenticator();

		$user = $authenticator->getUser();

		if (!$user instanceof User) {
			throw new AuthenticationException("users.auth.error.user-not-found", 10002);
		}

		$group = $this->questionService->getGroup($group);

		if ($group === null || $group->isDeleted()) {
			throw new NotFoundException('questions.group.get.error.group-not-found', 10003);
		}

		$this->questionService->deleteGroup($group);

		$this->sendSuccessResponse(new EmptyResponse, IResponse::S200_OK);
	}

	/**
	 * @ApiRoute("/api/v1/questions/groups/<group>/questions", method="GET", description="Fetches all available questions in a group relevant for a trip")
	 * @Response(200, file="response/questions/questions.json")
	 * @Response(404, code="10002", description="Trip was not found or belongs to another user")
	 * @Response(404, code="10003", description="Questions group was not found")
	 * @Response(400, code="10005", description="Trip validation has failed")
	 * @Queries("queries/questions/questionsGroup.json")
	 * @Authenticated()
	 * @param int $group
	 */
	public function actionQuestionsList(int $group): void
	{
		$authenticator = $this->getAuthenticator();

		$user = $authenticator->getUser();

		if (!$user instanceof User) {
			throw new AuthenticationException("users.auth.error.user-not-found", 10001);
		}

		$group = $this->questionService->getGroup($group);

		if ($group === null || $group->isDeleted()) {
			throw new NotFoundException('questions.group.get.error.group-not-found', 10003);
		}

		$trip = null;
		$tripId = $this->getHttpRequest()->getQuery("trip");

		if ($tripId !== null && $tripId !== "") {
			$trip = $this->tripRepository->find($tripId);

			if (!$trip instanceof Trip || $trip->getUser()->getId() !== $user->getId()) {
				throw new NotFoundException('trips.trip.get.error.trip-not-found', 10002);
			}

			try {
				$this->tripService->validateTrip($trip, $group->getType() === QuestionGroup::QUESTION_GROUP_TYPE_PURPOSE
					? TripService::TRIP_VALIDATION_PURPOSE
					: TripService::TRIP_VALIDATION_DETAILS);
			} catch (ValidationException $e) {
				throw new BadRequestException($e->getMessage(), 10005, $e->getParams());
			}
		}

		if ($trip !== null) {
			$questions = $this->questionService->getQuestionsTrip($group, $trip);
		} else {
			$showDisabled = $this->getHttpRequest()->getQuery("showDisabled") !== null
				? (bool) $this->getHttpRequest()->getQuery("showDisabled")
				: false;

			$questions = $this->questionService->getQuestions($group, null, [], null, $showDisabled);
		}

		$this->sendSuccessResponse(new QuestionsResponseBuilder($questions, $trip, $this->requestContext->getLang()), IResponse::S200_OK);
	}
}
