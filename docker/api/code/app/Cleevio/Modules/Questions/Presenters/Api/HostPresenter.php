<?php

declare(strict_types=1);

namespace Cleevio\Questions\ApiModule\Presenters;

use Cleevio\Api\Helpers\IRequestContext;
use Cleevio\Hosts\Api\Response\HostsResponseBuilder;
use Cleevio\Hosts\HostNotFound;
use Cleevio\Hosts\IHostsService;
use Cleevio\Questions\Api\DTO\HostsPutDTO;
use Cleevio\Questions\Entities\QuestionHost;
use Cleevio\Questions\IQuestionService;
use Cleevio\RestApi\Annotations\ApiRoute;
use Cleevio\RestApi\Annotations\Authenticated;
use Cleevio\RestApi\Annotations\DTO;
use Cleevio\RestApi\Annotations\JsonSchema;
use Cleevio\RestApi\Annotations\Response;
use Cleevio\RestApi\Exceptions\AuthenticationException;
use Cleevio\RestApi\Exceptions\NotFoundException;
use Cleevio\RestApi\Presenters\RestPresenter;
use Cleevio\Users\Entities\User;
use InvalidArgumentException;
use Nette\Http\IResponse;

/**
 * @ApiRoute("/api/v1/questions/<question>/host",presenter="Questions:Api:Host", tags={"Question"}, priority=4)
 */
class HostPresenter extends RestPresenter
{

	/**
	 * @var IQuestionService
	 */
	private $questionService;

	/**
	 * @var IRequestContext
	 */
	private $requestContext;
	/**
	 * @var IHostsService
	 */
	private $hostsService;

	/**
	 * @param IQuestionService $questionService
	 * @param IHostsService $hostsService
	 * @param IRequestContext $requestContext
	 */
	public function __construct(IQuestionService $questionService,
								IHostsService $hostsService,
								IRequestContext $requestContext)
	{
		parent::__construct();

		$this->questionService = $questionService;
		$this->requestContext = $requestContext;
		$this->hostsService = $hostsService;
	}

	/**
	 * @ApiRoute("/api/v1/questions/<question>/host", method="GET", description="Get all hosts set for selected question")
	 * @Response(200, file="response/hosts/hosts.json")
	 * @Response(404, code="10003", description="Question with provided ID was not found")
	 * @Authenticated("admin.questions")
	 * @param int $question
	 */
	public function actionCountryList(int $question): void
	{
		$authenticator = $this->getAuthenticator();

		$user = $authenticator->getUser();

		if (!$user instanceof User) {
			throw new AuthenticationException("users.auth.error.user-not-found", 10001);
		}

		$question = $this->questionService->getQuestion($question);

		if ($question === null || $question->isDeleted()) {
			throw new NotFoundException('questions.question.get.error.question-not-found', 10003);
		}

		$hosts = array_map(static function (QuestionHost $host) {
			return $host->getHost();
		}, $question->getHosts());

		$this->sendSuccessResponse(new HostsResponseBuilder($hosts, $this->requestContext->getLang()), IResponse::S200_OK);
	}

	/**
	 * @ApiRoute("/api/v1/questions/<question>/host", method="PUT", description="Set supported hosts for provided question")
	 * @Response(200, file="response/hosts/hosts.json")
	 * @DTO("Cleevio\Questions\Api\DTO\HostsPutDTO")
	 * @JsonSchema("request/questions/putHosts.json")
	 * @Response(404, code="10003", description="Question with provided ID was not found")
	 * @Response(404, code="10004", description="Host with provided code was not found")
	 * @Authenticated("admin.questions")
	 * @param int $question
	 */
	public function actionCountryPut(int $question): void
	{
		$authenticator = $this->getAuthenticator();

		$user = $authenticator->getUser();

		if (!$user instanceof User) {
			throw new AuthenticationException("users.auth.error.user-not-found", 10001);
		}

		$command = $this->getCommandDTO();

		if (!$command instanceof HostsPutDTO) {
			throw new InvalidArgumentException;
		}

		$question = $this->questionService->getQuestion($question);

		if ($question === null || $question->isDeleted()) {
			throw new NotFoundException('questions.question.get.error.question-not-found', 10003);
		}

		try {
			$hosts = $this->hostsService->findByIds($command->getIds());

			$this->questionService->setHosts($question, $hosts);

			$this->sendSuccessResponse(new HostsResponseBuilder($hosts, $this->requestContext->getLang()), IResponse::S200_OK);
		} catch (HostNotFound $e) {
			throw new NotFoundException('hosts.host.get.error.host-not-found', 10004);
		}
	}

}
