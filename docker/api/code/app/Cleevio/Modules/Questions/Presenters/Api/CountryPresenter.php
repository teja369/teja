<?php

declare(strict_types=1);

namespace Cleevio\Questions\ApiModule\Presenters;

use Cleevio\Api\Helpers\IRequestContext;
use Cleevio\Countries\Api\DTO\CountriesPutDTO;
use Cleevio\Countries\Api\Response\CountriesResponseBuilder;
use Cleevio\Countries\CountryNotFound;
use Cleevio\Countries\ICountryService;
use Cleevio\Questions\Entities\QuestionCountry;
use Cleevio\Questions\IQuestionService;
use Cleevio\RestApi\Annotations\ApiRoute;
use Cleevio\RestApi\Annotations\Authenticated;
use Cleevio\RestApi\Annotations\DTO;
use Cleevio\RestApi\Annotations\JsonSchema;
use Cleevio\RestApi\Annotations\Response;
use Cleevio\RestApi\Exceptions\AuthenticationException;
use Cleevio\RestApi\Exceptions\NotFoundException;
use Cleevio\RestApi\Presenters\RestPresenter;
use Cleevio\Users\Entities\User;
use InvalidArgumentException;
use Nette\Http\IResponse;

/**
 * @ApiRoute("/api/v1/questions/<question>/country",presenter="Questions:Api:Country", tags={"Question"}, priority=4)
 */
class CountryPresenter extends RestPresenter
{

	/**
	 * @var IQuestionService
	 */
	private $questionService;

	/**
	 * @var IRequestContext
	 */
	private $requestContext;

	/**
	 * @var ICountryService
	 */
	private $countryService;

	/**
	 * @param IQuestionService $questionService
	 * @param IRequestContext $requestContext
	 * @param ICountryService $countryService
	 */
	public function __construct(IQuestionService $questionService,
								IRequestContext $requestContext,
								ICountryService $countryService)
	{
		parent::__construct();

		$this->questionService = $questionService;
		$this->requestContext = $requestContext;
		$this->countryService = $countryService;
	}

	/**
	 * @ApiRoute("/api/v1/questions/<question>/country", method="GET", description="Get all countries set for selected question")
	 * @Response(200, file="response/countries/countries.json")
	 * @Response(404, code="10003", description="Question with provided ID was not found")
	 * @Authenticated("admin.questions")
	 * @param int $question
	 */
	public function actionCountryList(int $question): void
	{
		$authenticator = $this->getAuthenticator();

		$user = $authenticator->getUser();

		if (!$user instanceof User) {
			throw new AuthenticationException("users.auth.error.user-not-found", 10001);
		}

		$question = $this->questionService->getQuestion($question);

		if ($question === null || $question->isDeleted()) {
			throw new NotFoundException('questions.question.get.error.question-not-found', 10003);
		}

		$countries = array_map(static function (QuestionCountry $country) {
			return $country->getCountry();
		}, $question->getCountries());

		$this->sendSuccessResponse(new CountriesResponseBuilder($countries, $this->requestContext->getLang()), IResponse::S200_OK);
	}

	/**
	 * @ApiRoute("/api/v1/questions/<question>/country", method="PUT", description="Set supported countries for provided question")
	 * @Response(200, file="response/countries/countries.json")
	 * @DTO("Cleevio\Countries\Api\DTO\CountriesPutDTO")
	 * @JsonSchema("request/questions/putCountries.json")
	 * @Response(404, code="10003", description="Question with provided ID was not found")
	 * @Response(404, code="10004", description="Country with provided code was not found")
	 * @Authenticated("admin.questions")
	 * @param int $question
	 */
	public function actionCountryPut(int $question): void
	{
		$authenticator = $this->getAuthenticator();

		$user = $authenticator->getUser();

		if (!$user instanceof User) {
			throw new AuthenticationException("users.auth.error.user-not-found", 10001);
		}

		$command = $this->getCommandDTO();

		if (!$command instanceof CountriesPutDTO) {
			throw new InvalidArgumentException;
		}

		$question = $this->questionService->getQuestion($question);

		if ($question === null || $question->isDeleted()) {
			throw new NotFoundException('questions.question.get.error.question-not-found', 10003);
		}

		try {
			$countries = $this->countryService->findByIds($command->getIds());

			$this->questionService->setCountries($question, $countries);

			$this->sendSuccessResponse(new CountriesResponseBuilder($countries, $this->requestContext->getLang()), IResponse::S200_OK);
		} catch (CountryNotFound $e) {
			throw new NotFoundException('countries.country.get.error.country-not-found', 10004);
		}
	}

}
