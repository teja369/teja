<?php

declare(strict_types=1);

namespace Cleevio\Questions;

/**
 * Interface IQuestionTranslation
 * Getters for all translatable fields in questions
 * @package Cleevio\Questions
 */
interface IQuestionTranslation
{

	function getText(): ?string;

	function getHelp(): ?string;

	function getPlaceholder(): ?string;

	function getValidationHelp(): ?string;

}
