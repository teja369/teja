<?php

declare(strict_types=1);

namespace Cleevio\Trips\Entities;

use Cleevio\Answers\Entities\Answer;
use Cleevio\Countries\Entities\Country;
use Cleevio\Hosts\Entities\Host;
use Cleevio\Questions\Entities\Question;
use Cleevio\Registrations\Entities\Registration;
use Cleevio\Users\Entities\User;
use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use InvalidArgumentException;

/**
 * @ORM\Entity()
 * @ORM\Table(name="trips")
 */
class Trip
{

	use TimestampableEntity;

	public const TRIP_STATE_DRAFT = 'draft';

	public const TRIP_STATE_DELETED = 'deleted';

	public const TRIP_STATE_SUBMITTED = 'submitted';

	public const TRIP_STATE_PROCESSED = 'processed';

	public const TRIP_STATE_ON_HOLD = 'on-hold';

	public const TRIP_STATE_CANCELLED = 'cancelled';

	/**
	 * @ORM\Id
	 * @ORM\Column(type="integer")
	 * @ORM\GeneratedValue
	 * @var int|null
	 */
	private $id;

	/**
	 * @ORM\ManyToOne(targetEntity="Cleevio\Countries\Entities\Country", cascade={"persist"})
	 * @ORM\JoinColumn(name="country_id", referencedColumnName="id", nullable=false)
	 * @var Country
	 */
	protected $country;

	/**
	 * @ORM\Column(type="datetime", nullable=false)
	 * @var DateTime
	 */
	protected $start;

	/**
	 * @ORM\Column(type="datetime", nullable=false)
	 * @var DateTime
	 */
	protected $end;

	/**
	 * @ORM\Column(type="string", nullable=false)
	 * @var string
	 */
	protected $state = self::TRIP_STATE_DRAFT;

	/**
	 * @ORM\Column(type="text", nullable=true)
	 * @var string|null
	 */
	protected $stateMessage;

	/**
	 * @ORM\ManyToOne(targetEntity="Cleevio\Users\Entities\User", inversedBy="trips")
	 * @var User
	 */
	protected $user;

	/**
	 * @var ArrayCollection
	 * @ORM\OneToMany(targetEntity="Cleevio\Trips\Entities\TripRegistration", mappedBy="trip", cascade={"all"}, orphanRemoval=true)
	 */
	protected $registrations;

	/**
	 * @var ArrayCollection
	 * @ORM\OneToMany(targetEntity="Cleevio\Trips\Entities\TripFavorite", mappedBy="trip", cascade={"all"}, orphanRemoval=true)
	 */
	protected $favorites;

	/**
	 * @ORM\Column(type="datetime", nullable=true)
	 * @var DateTime|null
	 */
	protected $registrationCheck;

	/**
	 * @ORM\ManyToOne(targetEntity="Cleevio\Hosts\Entities\Host", inversedBy="trip")
	 * @ORM\JoinColumn(name="host_id", referencedColumnName="id", nullable=true)
	 * @var Host|null
	 */
	protected $host;

	/**
	 * @ORM\Column(type="datetime", nullable=true)
	 * @var DateTime|null
	 */
	protected $hostSet;

	/**
	 * @var \DateTime|null
	 * @ORM\Column(type="datetime", nullable=true)
	 */
	protected $submittedAt;

	/**
	 * @var ArrayCollection
	 * @ORM\OneToMany(targetEntity="Cleevio\Answers\Entities\Answer", mappedBy="trip", cascade={"all"}, orphanRemoval=true)
	 */
	protected $answers;


	/**
	 * @param Country $country
	 * @param User $user
	 * @param DateTime $start
	 * @param DateTime $end
	 * @param string $state
	 */
	public function __construct(
		Country $country,
		User $user,
		DateTime $start,
		DateTime $end,
		string $state = self::TRIP_STATE_DRAFT
	)
	{
		$this->country = $country;
		$this->user = $user;
		$this->start = $start;
		$this->end = $end;
		$this->state = $state;
		$this->registrations = new ArrayCollection;
		$this->answers = new ArrayCollection;
		$this->favorites = new ArrayCollection;
	}


	final public function getId(): int
	{
		if ($this->id === null) {
			throw new InvalidArgumentException;
		}

		return $this->id;
	}

	public function hasId(): bool
	{
		return $this->id !== null;
	}

	public function getCountry(): Country
	{
		return $this->country;
	}


	public function setCountry(Country $country): void
	{
		$this->country = $country;
	}


	public function getStart(): DateTime
	{
		return $this->start;
	}


	public function setStart(DateTime $start): void
	{
		$this->start = $start;
	}


	public function getEnd(): DateTime
	{
		return $this->end;
	}


	public function setEnd(DateTime $end): void
	{
		$this->end = $end;
	}


	public function getState(): string
	{
		return $this->state;
	}


	public function setState(string $state): void
	{
		if (!in_array($state, [
			self::TRIP_STATE_DRAFT,
			self::TRIP_STATE_SUBMITTED,
			self::TRIP_STATE_PROCESSED,
			self::TRIP_STATE_ON_HOLD,
			self::TRIP_STATE_DELETED,
			self::TRIP_STATE_CANCELLED,
		], true)) {
			throw new InvalidArgumentException("Invalid state");
		}

		if ($state === Trip::TRIP_STATE_CANCELLED && $this->getState() !== Trip::TRIP_STATE_SUBMITTED) {
			throw new InvalidArgumentException('Cannot cancel trip');
		}

		if ($state === Trip::TRIP_STATE_PROCESSED && $this->getState() !== Trip::TRIP_STATE_SUBMITTED) {
			throw new InvalidArgumentException('Cannot process trip');
		}

		if ($this->submittedAt === null && $state === self::TRIP_STATE_SUBMITTED) {
			$this->submittedAt = new DateTime;
		} elseif ($state === self::TRIP_STATE_DRAFT) {
			$this->submittedAt = null;
		}

		$this->state = $state;
	}


	/**
	 * @param User $user
	 * @return bool
	 */
	public function isFavorite(User $user): bool
	{
		foreach ($this->getFavorites() as $favorite) {
			if ($favorite->getUser()->getId() === $user->getId()) {
				return true;
			}
		}

		return false;
	}


	/**
	 * @param User $user
	 */
	public function setFavorite(User $user): void
	{
		if (!$this->isFavorite($user)) {
			$this->favorites[] = new TripFavorite($this, $user);
		}
	}


	public function getUser(): User
	{
		return $this->user;
	}


	public function setUser(User $user): void
	{
		$this->user = $user;
	}


	public function canDelete(): bool
	{
		return $this->state === Trip::TRIP_STATE_DRAFT;
	}


	/**
	 * @return TripRegistration[]
	 */
	public function getRegistrations(): array
	{
		return $this->registrations->getValues();
	}


	/**
	 * Check if trip requires registration
	 * @param Registration $registration
	 * @return bool
	 */
	public function requiresRegistration(Registration $registration)
	{
		foreach ($this->getRegistrations() as $tripRegistration) {
			if ($tripRegistration->getRegistration()->getId() === $registration->getId()) {
				return true;
			}
		}

		return false;
	}


	/**
	 * @param TripRegistration[] $registrations
	 * @throws \Exception
	 */
	public function setRegistrations(array $registrations): void
	{
		$this->registrations = new ArrayCollection($registrations);
		$this->registrationCheck = new DateTime;
	}


	/**
	 * @return TripFavorite[]
	 */
	public function getFavorites(): array
	{
		return $this->favorites->getValues();
	}


	/**
	 * @param TripFavorite[] $favorites
	 */
	public function setFavorites(array $favorites): void
	{
		$this->favorites = new ArrayCollection($favorites);
	}


	public function getHost(): ?Host
	{
		return $this->host;
	}


	public function setHost(?Host $host): void
	{
		$this->host = $host;
		$this->hostSet = new DateTime;
	}


	public function getHostSet(): ?DateTime
	{
		return $this->hostSet;
	}


	public function getRegistrationCheck(): ?DateTime
	{
		return $this->registrationCheck;
	}


	public function setRegistrationCheck(?DateTime $registrationCheck): void
	{
		$this->registrationCheck = $registrationCheck;
	}


	/**
	 * @return DateTime|null
	 */
	public function getSubmittedAt(): ?DateTime
	{
		return $this->submittedAt;
	}


	/**
	 * @return Answer[]
	 */
	public function getAnswers(): array
	{
		return $this->answers->getValues();
	}


	/**
	 * @param Question $question
	 * @return Answer|null
	 */
	public function getAnswer(Question $question): ?Answer
	{
		foreach ($this->getAnswers() as $answer) {
			if ($answer->getQuestion()->getId() === $question->getId()) {
				return $answer;
			}
		}

		return null;
	}


	/**
	 * @param Answer $answers
	 */
	public function addAnswer(Answer $answers): void
	{
		foreach ($this->answers as $value) {
			if ($value->getQuestion()->getId() === $answers->getQuestion()->getId()) {
				$this->answers->removeElement($value);
				$this->answers->add($answers);

				return;
			}
		}

		$this->answers->add($answers);
	}


	/**
	 * @param Question $question
	 */
	public function removeAnswer(Question $question): void
	{
		foreach ($this->answers as $value) {
			if ($value->getQuestion()->getId() === $question->getId()) {
				$this->answers->removeElement($value);

				break;
			}
		}
	}


	/**
	 * @param Answer[] $answers
	 */
	public function setAnswers(array $answers): void
	{
		$this->answers = new ArrayCollection($answers);
	}


	/**
	 * CLone trip
	 * @throws \Exception
	 */
	public function __clone()
	{
		$this->id = null;
		$this->state = self::TRIP_STATE_DRAFT;
		$this->start = new DateTime;
		$this->end = new DateTime;
		$this->registrations = new ArrayCollection;
		$this->registrationCheck = null;
		$this->favorites = new ArrayCollection;

		$this->createdAt = new DateTime;
		$this->updatedAt = new DateTime;
		$this->submittedAt = null;

		$answers = $this->getAnswers();
		$this->answers = new ArrayCollection;

		foreach ($answers as $answer) {
			$answer = clone $answer;
			$answer->setTrip($this);
			$this->addAnswer($answer);
		}
	}


	public static function submittedStates(): array
	{
		return [
			self::TRIP_STATE_SUBMITTED,
			self::TRIP_STATE_PROCESSED,
			self::TRIP_STATE_ON_HOLD,
		];
	}


	public static function overlapStates(): array
	{
		return [
			self::TRIP_STATE_DRAFT,
			self::TRIP_STATE_SUBMITTED,
			self::TRIP_STATE_PROCESSED,
			self::TRIP_STATE_ON_HOLD,
		];
	}


	/**
	 * @return string|null
	 */
	public function getStateMessage(): ?string
	{
		return $this->stateMessage;
	}


	/**
	 * @param string|null $stateMessage
	 */
	public function setStateMessage(?string $stateMessage): void
	{
		$this->stateMessage = $stateMessage;
	}

}
