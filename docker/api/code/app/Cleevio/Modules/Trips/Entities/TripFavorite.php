<?php

declare(strict_types=1);

namespace Cleevio\Trips\Entities;

use Cleevio\Users\Entities\User;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 * @ORM\Table(name="trips_favorite")
 */
class TripFavorite
{

	/**
	 * @ORM\Id
	 * @ORM\Column(type="integer")
	 * @ORM\GeneratedValue
	 * @var int
	 */
	private $id;

	/**
	 * @ORM\ManyToOne(targetEntity="Cleevio\Users\Entities\User")
	 * @ORM\JoinColumn(name="user_id", referencedColumnName="id", nullable=false)
	 * @var User
	 */
	protected $user;

	/**
	 * @ORM\ManyToOne(targetEntity="Cleevio\Trips\Entities\Trip")
	 * @ORM\JoinColumn(name="trip_id", referencedColumnName="id", nullable=false)
	 * @var Trip
	 */
	protected $trip;


	/**
	 * TripFavorite constructor.
	 * @param Trip $trip
	 * @param User $user
	 */
	public function __construct(
		Trip $trip,
		User $user
	)
	{
		$this->trip = $trip;
		$this->user = $user;
	}

	/**
	 * @return int
	 */
	final public function getId(): int
	{
		return $this->id;
	}

	/**
	 * @return Trip
	 */
	public function getTrip(): Trip
	{
		return $this->trip;
	}

	/**
	 * @return User
	 */
	public function getUser(): User
	{
		return $this->user;
	}

}
