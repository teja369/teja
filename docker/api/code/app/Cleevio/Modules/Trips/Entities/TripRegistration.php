<?php

declare(strict_types=1);

namespace Cleevio\Trips\Entities;

use Cleevio\Registrations\Entities\Registration;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 * @ORM\Table(name="trips_registration")
 */
class TripRegistration
{

	/**
	 * @ORM\Id
	 * @ORM\Column(type="integer")
	 * @ORM\GeneratedValue
	 * @var int
	 */
	private $id;

	/**
	 * @ORM\ManyToOne(targetEntity="Cleevio\Registrations\Entities\Registration")
	 * @ORM\JoinColumn(name="registration_id", referencedColumnName="id", nullable=false)
	 * @var Registration
	 */
	protected $registration;

	/**
	 * @ORM\ManyToOne(targetEntity="Cleevio\Trips\Entities\Trip")
	 * @ORM\JoinColumn(name="trip_id", referencedColumnName="id", nullable=false)
	 * @var Trip
	 */
	protected $trip;


	public function __construct(
		Trip $trip,
		Registration $registration
	)
	{
		$this->trip = $trip;
		$this->registration = $registration;
	}

	/**
	 * @return int
	 */
	final public function getId(): int
	{
		return $this->id;
	}

	public function getRegistration(): Registration
	{
		return $this->registration;
	}

}
