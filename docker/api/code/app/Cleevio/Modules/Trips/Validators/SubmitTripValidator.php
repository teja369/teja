<?php

declare(strict_types=1);

namespace Cleevio\Trips\Validators;

use Cleevio\Trips\Entities\Trip;

class SubmitTripValidator implements ITripValidator
{
	/**
	 * Validate trip against this rules
	 *
	 * @param Trip $trip
	 */
	public function validate(Trip $trip): void
	{
		unset($trip);
	}
}
