<?php

declare(strict_types=1);

namespace Cleevio\Trips\Validators;

use Cleevio\Answers\IAnswerService;
use Cleevio\Answers\InvalidAnswerException;
use Cleevio\Questions\Entities\QuestionGroup;
use Cleevio\Questions\IQuestionService;
use Cleevio\Trips\Entities\Trip;
use Cleevio\Trips\Entities\TripRegistration;
use Cleevio\Trips\ValidationException;

class QuestionsTripValidator implements ITripValidator
{

	/**
	 * @var IQuestionService
	 */
	private $questionService;

	/**
	 * @var IAnswerService
	 */
	private $answerService;

	/**
	 * @var string
	 */
	private $group;


	public function __construct(
		IQuestionService $questionService,
		IAnswerService $answerService,
		string $group
	)
	{
		$this->questionService = $questionService;
		$this->answerService = $answerService;
		$this->group = $group;
	}


	/**
	 * @param Trip $trip
	 * @throws ValidationException
	 */
	public function validate(Trip $trip): void
	{
		try {

			$groups = $this->questionService->getGroups($trip, $this->group);
			$registrations = [];

			if ($this->group !== QuestionGroup::QUESTION_GROUP_TYPE_PURPOSE) {
				$registrations = array_map(static function (TripRegistration $registration) {
					return $registration->getRegistration();
				}, $trip->getRegistrations());
			}

			foreach ($groups as $group) {
				foreach ($this->questionService->getQuestions($group, $trip->getCountry(), $registrations, $trip->getHost(), false) as $question) {
					$this->answerService->validate($trip, $question, $trip->getAnswer($question));
				}
			}
		} catch (InvalidAnswerException $e) {
			throw new ValidationException($e->getMessage(), $e->getParams());
		}
	}
}
