<?php

declare(strict_types=1);

namespace Cleevio\Trips\Validators;

use Cleevio\Fields\FieldTypeInvalid;
use Cleevio\Fields\IFieldService;
use Cleevio\Hosts\Entities\Host;
use Cleevio\Hosts\IHostsService;
use Cleevio\Hosts\InvalidFieldException;
use Cleevio\Hosts\MissingHostException;
use Cleevio\Trips\Entities\Trip;
use Cleevio\Trips\ValidationException;

class HostTripValidator implements ITripValidator
{

	/**
	 * @var IFieldService
	 */
	private $fieldService;

	/**
	 * @var IHostsService
	 */
	private $hostsService;


	public function __construct(
		IFieldService $fieldService,
		IHostsService $hostsService
	)
	{
		$this->fieldService = $fieldService;
		$this->hostsService = $hostsService;
	}


	/**
	 * Validate trip against this rules
	 * @param Trip $trip
	 * @throws ValidationException
	 */
	public function validate(Trip $trip): void
	{
		try {
			if ($trip->getHost() === null && $trip->getHostSet() === null) {
				throw new MissingHostException;
			}

			if ($trip->getHost() !== null && $trip->getHost()->getType() === Host::HOST_TYPE_NON_CLIENT) {
				$fields = $this->fieldService->getFields($trip->getCountry(), null);

				foreach ($fields as $field) {
					$params = $trip->getHost()->getParam($field);

					$this->hostsService->validate(
						$trip->getHost(),
						$field,
						$params === null ? null : $params->getValue()
					);
				}
			}
		} catch (InvalidFieldException | FieldTypeInvalid |  MissingHostException $e) {
			throw new ValidationException($e->getMessage(), $e->getParams());
		}
	}
}
