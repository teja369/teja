<?php

declare(strict_types=1);

namespace Cleevio\Trips\Validators;

use Cleevio\Trips\Entities\Trip;
use Cleevio\Trips\Repositories\ITripRepository;
use Cleevio\Trips\Repositories\TripConstraints;
use Cleevio\Trips\ValidationException;
use DateTime;

class BaseTripValidator implements ITripValidator
{

	private const DATE_FORMAT = 'Y-m-d';

	/**
	 * @var ITripRepository
	 */
	private $tripRepository;

	/**
	 * @param ITripRepository $tripRepository
	 */
	public function __construct(ITripRepository $tripRepository)
	{
		$this->tripRepository = $tripRepository;
	}

	/**
	 * Validate trip against this rules
	 * @param Trip $trip
	 * @throws ValidationException
	 */
	public function validate(Trip $trip): void
	{
		if (!$trip->getCountry()->isEnabled()) {
			throw new ValidationException("trips.trip.validation.invalid.country");
		}

		if ($trip->getCountry()->isHome()) {
			throw new ValidationException("trips.trip.validation.invalid.country.home");
		}

		$today = (new DateTime)->setTime(0, 0);

		if ($trip->getState() !== Trip::TRIP_STATE_DRAFT && $trip->getState() !== Trip::TRIP_STATE_SUBMITTED) {
			throw new ValidationException("trips.trip.validation.required.state");
		}

		if ($trip->getStart() < $today || $trip->getEnd() < $today) {
			throw new ValidationException("trips.trip.validation.required.date.retrospective");
		}

		if ($trip->getStart() > $trip->getEnd()) {
			throw new ValidationException("trips.trip.validation.required.date.end-before-start");
		}

		$constraints = (new TripConstraints)
			->setUser($trip->getUser())
			->setState(implode("|", Trip::overlapStates()))
			->setMinEndDate($trip->getStart()->format(self::DATE_FORMAT))
			->setMaxStartDate($trip->getEnd()->format(self::DATE_FORMAT))
			->build();

		$overlapped = $this->tripRepository->findAll($constraints);

		foreach ($overlapped as $overlappedTrip) {
			if ($overlappedTrip instanceof Trip && (!$trip->hasId() || $trip->getId() !== $overlappedTrip->getId())) {
				throw new ValidationException("trips.create.error.date-overlap");
			}
		}
	}
}
