<?php

declare(strict_types=1);

namespace Cleevio\Trips\Validators;

use Cleevio\Trips\Entities\Trip;
use Cleevio\Trips\ValidationException;

interface ITripValidator
{
	/**
	 * Validate trip against this rules
	 *
	 * @param Trip $trip
	 * @throws ValidationException
	 */
	public function validate(Trip $trip): void;

}
