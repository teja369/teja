<?php

declare(strict_types=1);

namespace Cleevio\Trips\Validators;

use Cleevio\Trips\Entities\Trip;
use Cleevio\Trips\ValidationException;

class RegistrationTripValidator implements ITripValidator
{

	/**
	 * Validate trip against this rules
	 *
	 * @param Trip $trip
	 * @throws ValidationException
	 */
	public function validate(Trip $trip): void
	{
		if ($trip->getRegistrationCheck() === null) {
			throw new ValidationException("trips.trip.validation.required.registration");
		}
	}
}
