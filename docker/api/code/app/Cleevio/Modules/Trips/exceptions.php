<?php

declare(strict_types=1);

namespace Cleevio\Trips;

use Cleevio\Exceptions\ParamException;

class ValidationException extends ParamException
{

}

class InvalidOrderByParameter extends ParamException
{

}

class TripStateInvalid extends ParamException
{

}

class TripQueryInvalidDate extends ParamException
{

}

class TripQueryInvalidDateTime extends ParamException
{

}
