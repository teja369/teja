<?php

declare(strict_types=1);

namespace Cleevio\Trips\ApiModule\Presenters;

use Cleevio\Answers\IAnswerService;
use Cleevio\Api\Helpers\IRequestContext;
use Cleevio\Hosts\Entities\Host;
use Cleevio\Hosts\Repositories\IHostRepository;
use Cleevio\RestApi\Annotations\ApiRoute;
use Cleevio\RestApi\Annotations\Authenticated;
use Cleevio\RestApi\Annotations\Response;
use Cleevio\RestApi\Exceptions\AuthenticationException;
use Cleevio\RestApi\Exceptions\BadRequestException;
use Cleevio\RestApi\Exceptions\NotFoundException;
use Cleevio\RestApi\Presenters\RestPresenter;
use Cleevio\Trips\Api\Response\TripResponseBuilder;
use Cleevio\Trips\Entities\Trip;
use Cleevio\Trips\ITripService;
use Cleevio\Trips\Repositories\ITripRepository;
use Cleevio\Trips\TripService;
use Cleevio\Trips\ValidationException;
use Cleevio\Users\Entities\User;
use Nette\Http\IResponse;

/**
 * @ApiRoute("/api/v1/trips",presenter="Trips:Api:TripHost", tags={"Trip"}, priority=1)
 */
class TripHostPresenter extends RestPresenter
{
	/**
	 * @var ITripRepository
	 */
	private $tripRepository;

	/**
	 * @var IHostRepository
	 */
	private $hostRepository;

	/**
	 * @var ITripService
	 */
	private $tripService;
	/**
	 * @var IRequestContext
	 */
	private $requestContext;
	/**
	 * @var IAnswerService
	 */
	private $answerService;

	/**
	 * @param ITripRepository $tripRepository
	 * @param ITripService $tripService
	 * @param IHostRepository $hostRepository
	 * @param IAnswerService $answerService
	 * @param IRequestContext $requestContext
	 */
	public function __construct(
		ITripRepository $tripRepository,
		ITripService $tripService,
		IHostRepository $hostRepository,
		IAnswerService $answerService,
		IRequestContext $requestContext
	)
	{
		parent::__construct();

		$this->tripRepository = $tripRepository;
		$this->hostRepository = $hostRepository;
		$this->tripService = $tripService;
		$this->requestContext = $requestContext;
		$this->answerService = $answerService;
	}

	/**
	 * @ApiRoute("/api/v1/trips/<trip>/host/<host>", method="PUT", description="Set host for a trip")
	 * @Response(200, file="response/trips/trip.json")
	 * @Response(404, code="10002", description="Trip was not found or belongs to another user")
	 * @Response(404, code="10003", description="Provided host was not found")
	 * @Response(400, code="10005", description="Trip validation failed")
	 * @Authenticated()
	 * @param int $trip
	 * @param int $host
	 */
	public function actionPut(int $trip, int $host): void
	{
		$authenticator = $this->getAuthenticator();

		$user = $authenticator->getUser();

		if (!$user instanceof User) {
			throw new AuthenticationException("users.auth.error.user-not-found", 10001);
		}

		$trip = $this->tripRepository->find($trip);

		if (!$trip instanceof Trip || $trip->getState() === Trip::TRIP_STATE_DELETED || $trip->getUser()->getId() !== $user->getId()) {
			throw new NotFoundException("trips.trip.get.error.trip-not-found", 10002);
		}

		$host = $this->hostRepository->find($host);

		if (!$host instanceof Host) {
			throw new NotFoundException("hosts.host.get.error.host-not-found", 10003);
		}

		try {
			$this->tripService->validateTrip($trip, TripService::TRIP_VALIDATION_HOST);
		} catch (ValidationException $e) {
			throw new BadRequestException($e->getMessage(), 10005, $e->getParams());
		}

		$trip->setState(Trip::TRIP_STATE_DRAFT);
		$trip->setHost($host);
		$this->tripRepository->persist($trip);

		$this->sendSuccessResponse(new TripResponseBuilder($trip, $this->answerService, $this->requestContext->getLang()), IResponse::S200_OK);
	}

	/**
	 * @ApiRoute("/api/v1/trips/<trip>/host", method="DELETE", description="Remove host from a trip")
	 * @Response(200, file="response/trips/trip.json")
	 * @Response(404, code="10002", description="Trip was not found or belongs to another user")
	 * @Response(404, code="10003", description="Provided host was not found")
	 * @Response(400, code="10005", description="Trip validation failed")
	 * @Authenticated()
	 * @param int $trip
	 */
	public function actionDeleteHost(int $trip): void
	{
		$authenticator = $this->getAuthenticator();

		$user = $authenticator->getUser();

		if (!$user instanceof User) {
			throw new AuthenticationException("users.auth.error.user-not-found", 10001);
		}

		$trip = $this->tripRepository->find($trip);

		if (!$trip instanceof Trip || $trip->getState() === Trip::TRIP_STATE_DELETED || $trip->getUser()->getId() !== $user->getId()) {
			throw new NotFoundException("trips.trip.get.error.trip-not-found", 10002);
		}

		try {
			$this->tripService->validateTrip($trip, TripService::TRIP_VALIDATION_HOST);
		} catch (ValidationException $e) {
			throw new BadRequestException($e->getMessage(), 10005, $e->getParams());
		}

		$trip->setState(Trip::TRIP_STATE_DRAFT);
		$trip->setHost(null);
		$this->tripRepository->persist($trip);

		$this->sendSuccessResponse(new TripResponseBuilder($trip, $this->answerService, $this->requestContext->getLang()), IResponse::S200_OK);
	}

}
