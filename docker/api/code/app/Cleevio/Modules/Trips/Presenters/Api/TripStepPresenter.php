<?php

declare(strict_types=1);

namespace Cleevio\Trips\ApiModule\Presenters;

use Cleevio\Api\Helpers\IRequestContext;
use Cleevio\RestApi\Annotations\ApiRoute;
use Cleevio\RestApi\Annotations\Authenticated;
use Cleevio\RestApi\Annotations\Response;
use Cleevio\RestApi\Exceptions\AuthenticationException;
use Cleevio\RestApi\Exceptions\NotFoundException;
use Cleevio\RestApi\Presenters\RestPresenter;
use Cleevio\Trips\Api\Response\TripStepResponseBuilder;
use Cleevio\Trips\Entities\Trip;
use Cleevio\Trips\ITripService;
use Cleevio\Users\Entities\User;
use Nette\Http\IResponse;

/**
 * @ApiRoute("/api/v1/trips",presenter="Trips:Api:TripStep", tags={"Trip"}, priority=1)
 */
class TripStepPresenter extends RestPresenter
{

	/**
	 * @var ITripService
	 */
	private $tripService;

	/**
	 * @var IRequestContext
	 */
	private $requestContext;


	/**
	 * TripStepPresenter constructor.
	 * @param ITripService $tripService
	 * @param IRequestContext $requestContext
	 */
	public function __construct(
		ITripService $tripService,
		IRequestContext $requestContext
	)
	{
		parent::__construct();

		$this->tripService = $tripService;
		$this->requestContext = $requestContext;
	}


	/**
	 * @ApiRoute("/api/v1/trips/<trip>/step", method="GET", description="Returns trip current step")
	 * @Response(404, code="10002", description="Trip was not found or belongs to another user")
	 * @Response(200, file="response/trips/step.json")
	 * @Authenticated()
	 * @param int $trip
	 */
	public function actionGetStep(int $trip): void
	{
		$authenticator = $this->getAuthenticator();

		$user = $authenticator->getUser();

		if (!$user instanceof User) {
			throw new AuthenticationException("users.auth.error.user-not-found", 10001);
		}

		$trip = $this->tripService->findById($trip);

		if (!$trip instanceof Trip || $trip->getState() === Trip::TRIP_STATE_DELETED || $trip->getUser()
				->getId() !== $user->getId()) {
			throw new NotFoundException("trips.trip.get.error.trip-not-found", 10002);
		}

		$step = $this->tripService->getCurrentStep($trip);

		$this->sendSuccessResponse(new TripStepResponseBuilder($step, $this->requestContext->getLang()), IResponse::S200_OK);
	}

}
