<?php

declare(strict_types=1);

namespace Cleevio\Trips\ApiModule\Presenters;

use Cleevio\Answers\IAnswerService;
use Cleevio\Api\Helpers\IRequestContext;
use Cleevio\Registrations\Entities\Registration;
use Cleevio\Registrations\Repositories\IRegistrationRepository;
use Cleevio\Repository\Constraints;
use Cleevio\RestApi\Annotations\ApiRoute;
use Cleevio\RestApi\Annotations\Authenticated;
use Cleevio\RestApi\Annotations\Response;
use Cleevio\RestApi\Exceptions\AuthenticationException;
use Cleevio\RestApi\Exceptions\BadRequestException;
use Cleevio\RestApi\Exceptions\NotFoundException;
use Cleevio\RestApi\Presenters\RestPresenter;
use Cleevio\Trips\Api\Response\TripResponseBuilder;
use Cleevio\Trips\Entities\Trip;
use Cleevio\Trips\Entities\TripRegistration;
use Cleevio\Trips\ITripService;
use Cleevio\Trips\Repositories\ITripRepository;
use Cleevio\Trips\TripService;
use Cleevio\Trips\ValidationException;
use Cleevio\Users\Entities\User;
use Exception;
use Nette\Http\IResponse;

/**
 * @ApiRoute("/api/v1/trips",presenter="Trips:Api:TripRegistration", tags={"Trip"}, priority=1)
 */
class TripRegistrationPresenter extends RestPresenter
{

	/**
	 * @var ITripService
	 */
	private $tripService;

	/**
	 * @var ITripRepository
	 */
	private $tripRepository;

	/**
	 * @var IRegistrationRepository
	 */
	private $registrationRepository;

	/**
	 * @var IRequestContext
	 */
	private $requestContext;
	/**
	 * @var IAnswerService
	 */
	private $answerService;


	/**
	 * @param ITripService $tripService
	 * @param ITripRepository $tripRepository
	 * @param IRegistrationRepository $registrationRepository
	 * @param IAnswerService $answerService
	 * @param IRequestContext $requestContext
	 */
	public function __construct(
		ITripService $tripService,
		ITripRepository $tripRepository,
		IRegistrationRepository $registrationRepository,
		IAnswerService $answerService,
		IRequestContext $requestContext
	)
	{
		parent::__construct();

		$this->tripService = $tripService;
		$this->tripRepository = $tripRepository;
		$this->registrationRepository = $registrationRepository;
		$this->requestContext = $requestContext;
		$this->answerService = $answerService;
	}

	/**
	 * @ApiRoute("/api/v1/trips/<trip>/registration", method="GET", description="Get required registrations for this trip")
	 * @Response(200, file="response/trips/trip.json")
	 * @Response(404, code="10002", description="Trip was not found or belongs to another user")
	 * @Response(404, code="10003", description="No registrations exists")
	 * @Response(400, code="10004", description="Trip validation failed")
	 * @Authenticated()
	 * @param int $trip
	 * @throws Exception
	 */
	public function actionGetRegistration(int $trip): void
	{
		$authenticator = $this->getAuthenticator();

		$user = $authenticator->getUser();

		if (!$user instanceof User) {
			throw new AuthenticationException("users.auth.error.user-not-found", 10001);
		}

		$trip = $this->tripRepository->find($trip);

		if (!$trip instanceof Trip || $trip->getState() === Trip::TRIP_STATE_DELETED || $trip->getUser()->getId() !== $user->getId()) {
			throw new NotFoundException("trips.trip.get.error.trip-not-found", 10002);
		}

		$registrations = $this->registrationRepository->findAll(Constraints::empty());

		if (count($registrations) === 0) {
			throw new NotFoundException("trips.trip.registration.error.registrations-not-found", 10003);
		}

		try {
			$this->tripService->validateTrip($trip, TripService::TRIP_VALIDATION_REGISTRATION);
		} catch (ValidationException $e) {
			throw new BadRequestException($e->getMessage(), 10004, $e->getParams());
		}

		$tripRegistrations = [];

		foreach ($registrations as $registration) {
			if (!$registration instanceof Registration) {
				continue;
			}

			switch ($registration->getName()) {
				case "A1":
					$tripRegistrations[] = new TripRegistration($trip, $registration);

					break;
				case "PWD":
					if ((bool) random_int(0, 1)) {
						$tripRegistrations[] = new TripRegistration($trip, $registration);
					}

					break;
			}
		}

		$trip->setState(Trip::TRIP_STATE_DRAFT);
		$trip->setRegistrations($tripRegistrations);
		$this->tripRepository->persist($trip);

		$this->sendSuccessResponse(new TripResponseBuilder($trip, $this->answerService, $this->requestContext->getLang()), IResponse::S200_OK);
	}

}
