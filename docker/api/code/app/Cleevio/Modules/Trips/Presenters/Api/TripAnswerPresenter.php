<?php

declare(strict_types=1);

namespace Cleevio\Trips\ApiModule\Presenters;

use Cleevio\Answers\IAnswerService;
use Cleevio\Answers\InvalidAnswerException;
use Cleevio\Api\Helpers\IRequestContext;
use Cleevio\Questions\IQuestionService;
use Cleevio\Questions\QuestionNotFound;
use Cleevio\RestApi\Annotations\ApiRoute;
use Cleevio\RestApi\Annotations\Authenticated;
use Cleevio\RestApi\Annotations\DTO;
use Cleevio\RestApi\Annotations\JsonSchema;
use Cleevio\RestApi\Annotations\Response;
use Cleevio\RestApi\Exceptions\AuthenticationException;
use Cleevio\RestApi\Exceptions\BadRequestException;
use Cleevio\RestApi\Exceptions\NotFoundException;
use Cleevio\RestApi\Presenters\RestPresenter;
use Cleevio\Trips\Api\DTO\AnswersPutDTO;
use Cleevio\Trips\Api\Response\TripResponseBuilder;
use Cleevio\Trips\Entities\Trip;
use Cleevio\Trips\ITripService;
use Cleevio\Trips\Repositories\ITripRepository;
use Cleevio\Trips\TripService;
use Cleevio\Trips\ValidationException;
use Cleevio\Users\Entities\User;
use InvalidArgumentException;
use Nette\Http\IResponse;

/**
 * @ApiRoute("/api/v1/trips",presenter="Trips:Api:TripAnswer", tags={"Trip"}, priority=1)
 */
class TripAnswerPresenter extends RestPresenter
{

	/**
	 * @var ITripService
	 */
	private $tripService;

	/**
	 * @var ITripRepository
	 */
	private $tripRepository;

	/**
	 * @var IQuestionService
	 */
	private $questionService;
	/**
	 * @var IAnswerService
	 */
	private $answerService;
	/**
	 * @var IRequestContext
	 */
	private $requestContext;


	/**
	 * @param ITripService $tripService
	 * @param ITripRepository $tripRepository
	 * @param IQuestionService $questionService
	 * @param IAnswerService $answerService
	 * @param IRequestContext $requestContext
	 */
	public function __construct(
		ITripService $tripService,
		ITripRepository $tripRepository,
		IQuestionService $questionService,
		IAnswerService $answerService,
		IRequestContext $requestContext
	)
	{
		parent::__construct();

		$this->tripService = $tripService;
		$this->tripRepository = $tripRepository;
		$this->questionService = $questionService;
		$this->answerService = $answerService;
		$this->requestContext = $requestContext;
	}

	/**
	 * @ApiRoute("/api/v1/trips/<trip>/answer", method="PUT", description="Save answers for trip questions")
	 * @DTO("Cleevio\Trips\Api\DTO\AnswersPutDTO")
	 * @JsonSchema("request/trips/answer.json")
	 * @Response(200, file="response/trips/trip.json")
	 * @Response(404, code="10002", description="Trip was not found or belongs to another user")
	 * @Response(404, code="10003", description="Provided question was not found")
	 * @Response(400, code="10005", description="Trip validation failed")
	 * @Response(400, code="10006", description="Invalid answer was provided to one of the questions")
	 * @Authenticated()
	 * @param int $trip
	 */
	public function actionPutAnswer(int $trip): void
	{
		$authenticator = $this->getAuthenticator();

		$user = $authenticator->getUser();

		if (!$user instanceof User) {
			throw new AuthenticationException("users.auth.error.user-not-found", 10001);
		}

		$trip = $this->tripRepository->find($trip);

		if (!$trip instanceof Trip || $trip->getState() === Trip::TRIP_STATE_DELETED || $trip->getUser()->getId() !== $user->getId()) {
			throw new NotFoundException("trips.trip.get.error.trip-not-found", 10002);
		}

		try {
			$this->tripService->validateTrip($trip, TripService::TRIP_VALIDATION_PURPOSE);
		} catch (ValidationException $e) {
			throw new BadRequestException($e->getMessage(), 10005, $e->getParams());
		}

		$command = $this->getCommandDTO();

		if (!$command instanceof AnswersPutDTO) {
			throw new InvalidArgumentException;
		}

		try {
			$questions = $this->questionService->getQuestionsByIds(array_keys($command->getQuestions()));

			foreach ($questions as $question) {
				$answer = $this->answerService->initAnswer($trip, $question, $command->getQuestion($question->getId()));
				$answer = $this->answerService->validate($trip, $question, $answer);

				if ($answer !== null) {
					$trip->addAnswer($answer);
				} else {
					$trip->removeAnswer($question);
				}
			}

			$trip->setState(Trip::TRIP_STATE_DRAFT);
			$this->tripRepository->persist($trip);

		} catch (QuestionNotFound $e) {
			throw new NotFoundException("questions.question.get.error.question-not-found", 10003);
		} catch (InvalidAnswerException $e) {
			throw new BadRequestException($e->getMessage(), 10006, $e->getParams());
		}

		$this->sendSuccessResponse(new TripResponseBuilder($trip, $this->answerService, $this->requestContext->getLang()), IResponse::S200_OK);
	}

}
