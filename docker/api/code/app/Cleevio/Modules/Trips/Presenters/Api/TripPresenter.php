<?php

declare(strict_types=1);

namespace Cleevio\Trips\ApiModule\Presenters;

use Cleevio\Answers\IAnswerService;
use Cleevio\Api\Helpers\IRequestContext;
use Cleevio\Countries\CountryNotFound;
use Cleevio\Countries\ICountryService;
use Cleevio\Hosts\HostNotFound;
use Cleevio\Languages\ITranslationService;
use Cleevio\Mailer\Context;
use Cleevio\Mailer\EmailEvent;
use Cleevio\Questions\RegistrationNotFound;
use Cleevio\Repository\Constraints;
use Cleevio\RestApi\Annotations\ApiRoute;
use Cleevio\RestApi\Annotations\Authenticated;
use Cleevio\RestApi\Annotations\DTO;
use Cleevio\RestApi\Annotations\JsonSchema;
use Cleevio\RestApi\Annotations\Queries;
use Cleevio\RestApi\Annotations\Response;
use Cleevio\RestApi\Exceptions\AuthenticationException;
use Cleevio\RestApi\Exceptions\BadRequestException;
use Cleevio\RestApi\Exceptions\ForbiddenException;
use Cleevio\RestApi\Exceptions\NotFoundException;
use Cleevio\RestApi\Presenters\RestPresenter;
use Cleevio\RestApi\Request\PagedRequest;
use Cleevio\RestApi\Response\EmptyResponse;
use Cleevio\Trips\Api\DTO\CreatePostDTO;
use Cleevio\Trips\Api\DTO\StateMessagePutDTO;
use Cleevio\Trips\Api\Response\TripResponseBuilder;
use Cleevio\Trips\Api\Response\TripsResponseBuilder;
use Cleevio\Trips\Entities\Trip;
use Cleevio\Trips\Entities\TripFavorite;
use Cleevio\Trips\InvalidOrderByParameter;
use Cleevio\Trips\ITripService;
use Cleevio\Trips\Repositories\ITripRepository;
use Cleevio\Trips\Repositories\TripConstraints;
use Cleevio\Trips\TripQueryInvalidDate;
use Cleevio\Trips\TripQueryInvalidDateTime;
use Cleevio\Trips\TripService;
use Cleevio\Trips\TripStateInvalid;
use Cleevio\Trips\ValidationException;
use Cleevio\Users\Entities\User;
use Cleevio\Users\UserNotFound;
use Exception;
use InvalidArgumentException;
use Nette\Http\IResponse;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

/**
 * @ApiRoute("/api/v1/trips",presenter="Trips:Api:Trip", tags={"Trip"}, priority=2)
 */
class TripPresenter extends RestPresenter
{
	/**
	 * @var ITripService
	 */
	private $tripService;

	/**
	 * @var ICountryService
	 */
	private $countryService;

	/**
	 * @var ITripRepository
	 */
	private $tripRepository;

	/**
	 * @var IRequestContext
	 */
	private $requestContext;
	/**
	 * @var IAnswerService
	 */
	private $answerService;
	/**
	 * @var EventDispatcherInterface
	 */
	private $eventDispatcher;
	/**
	 * @var Context
	 */
	private $mailContext;
	/**
	 * @var ITranslationService
	 */
	private $translationService;

	/**
	 * @param ITripService $tripService
	 * @param ITripRepository $tripRepository
	 * @param ICountryService $countryService
	 * @param IAnswerService $answerService
	 * @param ITranslationService $translationService
	 * @param IRequestContext $requestContext
	 * @param EventDispatcherInterface $eventDispatcher
	 * @param Context $mailContext
	 */
	public function __construct(
		ITripService $tripService,
		ITripRepository $tripRepository,
		ICountryService $countryService,
		IAnswerService $answerService,
		ITranslationService $translationService,
		IRequestContext $requestContext,
		EventDispatcherInterface $eventDispatcher,
		Context $mailContext
	)
	{
		parent::__construct();

		$this->tripService = $tripService;
		$this->countryService = $countryService;
		$this->tripRepository = $tripRepository;
		$this->requestContext = $requestContext;
		$this->answerService = $answerService;
		$this->eventDispatcher = $eventDispatcher;
		$this->mailContext = $mailContext;
		$this->translationService = $translationService;
	}

	/**
	 * @ApiRoute("/api/v1/trips", method="GET", description="Get list of trips")
	 * @Queries("queries/trips/trips.json")
	 * @Response(200, file="response/trips/trips.json")
	 * @Response(400, code="10002", description="Provided country was not found")
	 * @Response(400, code="10003", description="Provided host was not found")
	 * @Response(400, code="10004", description="Invalid order by argument")
	 * @Response(400, code="10005", description="Invalid datetime format in query parameter")
	 * @Response(400, code="10006", description="Invalid date format in query parameter")
	 * @Response(400, code="10007", description="Provided user was not found")
	 * @Response(400, code="10008", description="Provided registration was not found")
	 * @Authenticated()
	 */
	public function actionList(): void
	{
		$authenticator = $this->getAuthenticator();

		$user = $authenticator->getUser();

		if (!$user instanceof User) {
			throw new AuthenticationException("users.auth.error.user-not-found", 10001);
		}

		$request = PagedRequest::of($this->getHttpRequest());

		try {
			$tripConstraints = new TripConstraints;

			if ($this->getHttpRequest()->getQuery('showAll') === null) {
				$tripConstraints = $tripConstraints->setUser($user);
			} elseif (!$this->getUser()->isAllowed("admin.trips")) {
				throw new ForbiddenException;
			}

			$tripConstraints = $this->tripService->queryToConstraints($tripConstraints, $this->getHttpRequest());
			$constraints = $request->toConstraints($tripConstraints)->build();

			$trips = $this->tripRepository->findAll($constraints);
			$current = $this->tripRepository->count($constraints);
			$total = $this->tripRepository->count(Constraints::empty());

			$response = (new TripsResponseBuilder($request, $trips, $this->answerService, $this->requestContext->getLang()))
				->setCurrent($current)
				->setTotal($total);

			$this->sendSuccessResponse($response, IResponse::S200_OK);

		} catch (CountryNotFound $e) {
			throw new BadRequestException('countries.country.get.error.country-not-found', 10002);
		} catch (HostNotFound $e) {
			throw new BadRequestException('hosts.host.get.error.host-not-found', 10003);
		} catch (InvalidOrderByParameter $e) {
			throw new BadRequestException('trips.trip.get.error.orderBy-not-found', 10004);
		} catch (TripQueryInvalidDateTime $e) {
			throw new BadRequestException('trips.trip.get.error.query-datetime-invalid', 10005);
		} catch (TripQueryInvalidDate $e) {
			throw new BadRequestException('trips.trip.get.error.query-date-invalid', 10006);
		} catch (UserNotFound $e) {
			throw new BadRequestException('users.user.get.error.user-not-found', 10007);
		} catch (RegistrationNotFound $e) {
			throw new BadRequestException('registrations.registration.get.error.registration-not-found', 10008);
		}
	}


	/**
	 * @ApiRoute("/api/v1/trips", method="POST", description="Creates new trip")
	 * @Response(400, code="10002", description="Country does not exist")
	 * @Response(400, code="10003", description="This user has another trip in this time period")
	 * @DTO("Cleevio\Trips\Api\DTO\CreatePostDTO")
	 * @JsonSchema("request/trips/create.json")
	 * @Authenticated()
	 */
	public function actionCreate(): void
	{
		$authenticator = $this->getAuthenticator();

		$user = $authenticator->getUser();

		if (!$user instanceof User) {
			throw new AuthenticationException("users.auth.error.user-not-found", 10001);
		}

		$command = $this->getCommandDTO();

		if (!$command instanceof CreatePostDTO) {
			throw new InvalidArgumentException;
		}

		$country = $this->countryService->findByCode($command->getCountry());

		if ($country === null) {
			throw new BadRequestException("countries.country.get.error.country-not-found", 10002);
		}

		try {
			$trip = $this->tripService->createTrip($country, $user, $command->getStart(), $command->getEnd());
		} catch (ValidationException $e) {
			throw new BadRequestException($e->getMessage(), 10005, $e->getParams());
		}

		$this->sendSuccessResponse(new TripResponseBuilder($trip, $this->answerService, $this->requestContext->getLang()), IResponse::S201_CREATED);
	}

	/**
	 * @ApiRoute("/api/v1/trips/<trip>", method="PUT", description="Update trip basic info")
	 * @Response(404, code="10002", description="Trip was not found or belongs to another user")
	 * @Response(400, code="10003", description="Country does not exist")
	 * @Response(400, code="10004", description="This user has another trip in this time period")
	 * @DTO("Cleevio\Trips\Api\DTO\CreatePostDTO")
	 * @JsonSchema("request/trips/create.json")
	 * @Authenticated()
	 * @param int $trip
	 */
	public function actionPut(int $trip): void
	{
		$authenticator = $this->getAuthenticator();

		$user = $authenticator->getUser();

		if (!$user instanceof User) {
			throw new AuthenticationException("users.auth.error.user-not-found", 10001);
		}

		$trip = $this->tripRepository->find($trip);

		if (!$trip instanceof Trip || $trip->getState() === Trip::TRIP_STATE_DELETED || $trip->getUser()->getId() !== $user->getId()) {
			throw new NotFoundException("trips.trip.get.error.trip-not-found", 10002);
		}

		$command = $this->getCommandDTO();

		if (!$command instanceof CreatePostDTO) {
			throw new InvalidArgumentException;
		}

		$country = $this->countryService->findByCode($command->getCountry());

		if ($country === null) {
			throw new BadRequestException("countries.country.get.error.country-not-found", 10003);
		}

		$trip->setState(Trip::TRIP_STATE_DRAFT);
		$trip->setCountry($country);
		$trip->setStart($command->getStart());
		$trip->setEnd($command->getEnd());

		try {
			$this->tripService->validateTrip($trip, TripService::TRIP_VALIDATION_PURPOSE);
		} catch (ValidationException $e) {
			throw new BadRequestException($e->getMessage(), 10005, $e->getParams());
		}

		$this->tripRepository->persist($trip);

		$this->sendSuccessResponse(new TripResponseBuilder($trip, $this->answerService, $this->requestContext->getLang()), IResponse::S201_CREATED);
	}

	/**
	 * @ApiRoute("/api/v1/trips/<trip>", method="GET", description="Get trip by id")
	 * @Response(200, file="response/trips/trip.json")
	 * @Response(404, code="10002", description="Trip was not found or belongs to another user")
	 * @Authenticated()
	 * @param int $trip
	 */
	public function actionGet(int $trip): void
	{
		$authenticator = $this->getAuthenticator();

		$user = $authenticator->getUser();

		if (!$user instanceof User) {
			throw new AuthenticationException("users.auth.error.user-not-found", 10001);
		}

		$trip = $this->tripRepository->find($trip);

		if (!$trip instanceof Trip || $trip->getState() === Trip::TRIP_STATE_DELETED ||
			(!$this->getUser()->isAllowed("admin.trips") && $trip->getUser()->getId() !== $user->getId())) {
			throw new NotFoundException("trips.trip.get.error.trip-not-found", 10002);
		}

		$this->sendSuccessResponse(new TripResponseBuilder($trip, $this->answerService, $this->requestContext->getLang()), IResponse::S200_OK);
	}

	/**
	 * @ApiRoute("/api/v1/trips/<id>/copy", method="GET", description="Copy trip")
	 * @Response(200, file="response/trips/trip.json")
	 * @Response(400, code="10003", description="Trip cannot be deleted")
	 * @Response(404, code="10002", description="Trip was not found or belongs to another user")
	 * @param int $id
	 * @Authenticated()
	 */
	public function actionCopy(int $id): void
	{
		$authenticator = $this->getAuthenticator();

		$user = $authenticator->getUser();

		if (!$user instanceof User) {
			throw new AuthenticationException("users.auth.error.user-not-found", 10001);
		}

		$trip = $this->tripService->findById($id);

		if (!$trip instanceof Trip || $trip->getUser()->getId() !== $user->getId()) {
			throw new NotFoundException("trips.trip.get.error.trip-not-found", 10002);
		}

		$trip = $this->tripService->copyTrip($trip);

		$this->sendSuccessResponse(new TripResponseBuilder($trip, $this->answerService, $this->requestContext->getLang()), IResponse::S200_OK);
	}

	/**
	 * @ApiRoute("/api/v1/trips/<id>", method="DELETE", description="Deletes a trip")
	 * @Response(400, code="10003", description="Trip cannot be deleted")
	 * @Response(404, code="10002", description="Trip was not found or belongs to another user")
	 * @param int $id
	 * @Authenticated()
	 */
	public function actionDelete(int $id): void
	{
		$authenticator = $this->getAuthenticator();

		$user = $authenticator->getUser();

		if (!$user instanceof User) {
			throw new AuthenticationException("users.auth.error.user-not-found", 10001);
		}

		$trip = $this->tripService->findById($id);

		if (!$trip instanceof Trip || $trip->getUser()->getId() !== $user->getId()) {
			throw new NotFoundException("trips.trip.get.error.trip-not-found", 10002);
		}

		$result = $this->tripService->deleteTrip($trip);

		if ($result === false) {
			throw new BadRequestException("trips.trip.delete.error.cannot-delete", 10003);
		}

		$this->sendSuccessResponse(new EmptyResponse);
	}

	/**
	 * @ApiRoute("/api/v1/trips/<id>/submit", method="PUT", description="Submit trip")
	 * @Response(404, code="10002", description="Trip was not found or belongs to another user")
	 * @Response(400, code="10005", description="Trip validatiuon has failed and cannot be submitted")
	 * @param int $id
	 * @Authenticated()
	 */
	public function actionSubmit(int $id): void
	{
		$authenticator = $this->getAuthenticator();

		$user = $authenticator->getUser();

		if (!$user instanceof User) {
			throw new AuthenticationException("users.auth.error.user-not-found", 10001);
		}

		$trip = $this->tripService->findById($id);

		if (!$trip instanceof Trip || $trip->getUser()->getId() !== $user->getId()) {
			throw new NotFoundException("trips.trip.get.error.trip-not-found", 10002);
		}

		try {
			$this->tripService->setTripState($trip, Trip::TRIP_STATE_SUBMITTED, true);
		} catch (ValidationException | TripStateInvalid $e) {
			throw new BadRequestException($e->getMessage(), 10005, $e->getParams());
		}

		$this->sendSuccessResponse(new TripResponseBuilder($trip, $this->answerService, $this->requestContext->getLang()));
	}

	/**
	 * @ApiRoute("/api/v1/trips/<id>/cancel", method="PUT", description="Cancel trip")
	 * @Response(404, code="10002", description="Trip was not found or belongs to another user")
	 * @Response(400, code="10003", description="Trip cannot be cancelled")
	 * @param int $id
	 * @Authenticated()
	 */
	public function actionCancel(int $id): void
	{
		$authenticator = $this->getAuthenticator();

		$user = $authenticator->getUser();

		if (!$user instanceof User) {
			throw new AuthenticationException("users.auth.error.user-not-found", 10001);
		}

		$trip = $this->tripService->findById($id);

		if (!$trip instanceof Trip || $trip->getUser()->getId() !== $user->getId()) {
			throw new NotFoundException("trips.trip.get.error.trip-not-found", 10002);
		}

		try {
			$this->tripService->setTripState($trip, Trip::TRIP_STATE_CANCELLED, false);
		} catch (ValidationException | TripStateInvalid $e) {
			throw new BadRequestException($e->getMessage(), 10003, $e->getParams());
		}

		$this->sendSuccessResponse(new TripResponseBuilder($trip, $this->answerService, $this->requestContext->getLang()));
	}

	/**
	 * @ApiRoute("/api/v1/trips/<id>/process", method="PUT", description="Set trip as processed")
	 * @Response(404, code="10002", description="Trip was not found")
	 * @Response(400, code="10002", description="Trip cannot be processed")
	 * @param int $id
	 * @Authenticated("admin.trips")
	 */
	public function actionProcess(int $id): void
	{
		$authenticator = $this->getAuthenticator();

		$user = $authenticator->getUser();

		if (!$user instanceof User) {
			throw new AuthenticationException("users.auth.error.user-not-found", 10001);
		}

		$trip = $this->tripService->findById($id);

		if (!$trip instanceof Trip) {
			throw new NotFoundException("trips.trip.get.error.trip-not-found", 10002);
		}

		try {
			$this->tripService->setTripState($trip, Trip::TRIP_STATE_PROCESSED, false);
		} catch (ValidationException | TripStateInvalid $e) {
			throw new BadRequestException($e->getMessage(), 10003, $e->getParams());
		}

		$this->sendSuccessResponse(new TripResponseBuilder($trip, $this->answerService, $this->requestContext->getLang()));
	}

	/**
	 * @ApiRoute("/api/v1/trips/<id>/favorite", method="PUT", description="Set trip as user favorite")
	 * @Response(404, code="10002", description="Trip was not found or belongs to another user")
	 * @param int $id
	 * @Authenticated()
	 */
	public function actionFavoritePut(int $id): void
	{
		$authenticator = $this->getAuthenticator();

		$user = $authenticator->getUser();

		if (!$user instanceof User) {
			throw new AuthenticationException("users.auth.error.user-not-found", 10001);
		}

		$trip = $this->tripService->findById($id);

		if (!$trip instanceof Trip || $trip->getUser()->getId() !== $user->getId()) {
			throw new NotFoundException("trips.trip.get.error.trip-not-found", 10002);
		}

		$trip->setFavorite($user);
		$this->tripRepository->persist($trip);

		$this->sendSuccessResponse(new TripResponseBuilder($trip, $this->answerService, $this->requestContext->getLang()));
	}

	/**
	 * @ApiRoute("/api/v1/trips/<id>/favorite", method="DELETE", description="Remove trip as user favorite")
	 * @Response(404, code="10002", description="Trip was not found or belongs to another user")
	 * @param int $id
	 * @Authenticated()
	 */
	public function actionFavoriteDelete(int $id): void
	{
		$authenticator = $this->getAuthenticator();

		$user = $authenticator->getUser();

		if (!$user instanceof User) {
			throw new AuthenticationException("users.auth.error.user-not-found", 10001);
		}

		$trip = $this->tripService->findById($id);

		if (!$trip instanceof Trip || $trip->getUser()->getId() !== $user->getId()) {
			throw new NotFoundException("trips.trip.get.error.trip-not-found", 10002);
		}

		$trip->setFavorites(array_filter($trip->getFavorites(), static function (TripFavorite $favorite) use ($user) {
			return $favorite->getUser()->getId() !== $user->getId();
		}));

		$this->tripRepository->persist($trip);

		$this->sendSuccessResponse(new TripResponseBuilder($trip, $this->answerService, $this->requestContext->getLang()));
	}

	/**
	 * @ApiRoute("/api/v1/trips/<id>/onhold", method="PUT", description="Puts trip on hold")
	 * @Response(404, code="10002", description="Trip was not found or belongs to another user")
	 * @Response(400, code="10003", description="Sending user does not have an email address")
	 * @Response(400, code="10004", description="Email could not be send")
	 * @DTO("Cleevio\Trips\Api\DTO\StateMessagePutDTO")
	 * @param int $id
	 * @JsonSchema("request/trips/onhold.json")
	 * @Authenticated("admin.trips")
	 */
	public function actionOnHoldPut(int $id): void
	{
		$authenticator = $this->getAuthenticator();

		$user = $authenticator->getUser();

		if (!$user instanceof User) {
			throw new AuthenticationException("users.auth.error.user-not-found", 10001);
		}

		$trip = $this->tripService->findById($id);

		if (!$trip instanceof Trip) {
			throw new NotFoundException("trips.trip.get.error.trip-not-found", 10002);
		}

		$command = $this->getCommandDTO();

		if (!$command instanceof StateMessagePutDTO) {
			throw new InvalidArgumentException;
		}

		if ($trip->getUser()->getEmail() === null) {
			throw new BadRequestException("users.auth.error.user-no-email", 10003);
		}

		$this->tripRepository->beginTransaction();

		try {

			$this->tripService->setTripState($trip, Trip::TRIP_STATE_ON_HOLD, false, $command->getStateMessage());

			$this->eventDispatcher->dispatch(new EmailEvent(
				$this->translationService->translate("trips.trip.onHold.email.subject", $trip->getUser()->getLanguage()),
				$this->translationService->translate("trips.trip.onHold.email.message", $trip->getUser()->getLanguage(), [
					'message' => $command->getStateMessage(),
				]),
				$trip->getUser()->getEmail(),
				$this->mailContext->getAddressSystem()
			));

			$this->tripRepository->commitTransaction();

		} catch (Exception $ex) {

			$this->tripRepository->rollbackTransaction();

			throw new BadRequestException("trips.trip.onHold.error.email", 10004);
		}

		$this->sendSuccessResponse(new TripResponseBuilder($trip, $this->answerService, $this->requestContext->getLang()));
	}

	/**
	 * @ApiRoute("/api/v1/trips/<id>/onhold", method="DELETE", description="Releases trip")
	 * @Response(404, code="10002", description="Trip was not found or belongs to another user")
	 * @param int $id
	 * @throws TripStateInvalid
	 * @throws ValidationException
	 * @Authenticated("admin.trips")
	 */
	public function actionOnHoldDelete(int $id): void
	{
		$authenticator = $this->getAuthenticator();

		$user = $authenticator->getUser();

		if (!$user instanceof User) {
			throw new AuthenticationException("users.auth.error.user-not-found", 10001);
		}

		$trip = $this->tripService->findById($id);

		if (!$trip instanceof Trip) {
			throw new NotFoundException("trips.trip.get.error.trip-not-found", 10002);
		}

		$this->tripService->setTripState($trip, Trip::TRIP_STATE_SUBMITTED, false);

		$this->sendSuccessResponse(new TripResponseBuilder($trip, $this->answerService, $this->requestContext->getLang()));
	}

}
