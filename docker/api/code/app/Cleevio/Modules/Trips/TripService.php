<?php

declare(strict_types=1);

namespace Cleevio\Trips;

use Cleevio\Answers\IAnswerService;
use Cleevio\Countries\CountryNotFound;
use Cleevio\Countries\Entities\Country;
use Cleevio\Countries\ICountryService;
use Cleevio\Fields\IFieldService;
use Cleevio\Hosts\HostNotFound;
use Cleevio\Hosts\IHostsService;
use Cleevio\Questions\Entities\QuestionGroup;
use Cleevio\Questions\IQuestionService;
use Cleevio\Questions\RegistrationNotFound;
use Cleevio\Registrations\IRegistrationService;
use Cleevio\Trips\Entities\Trip;
use Cleevio\Trips\Repositories\ITripRepository;
use Cleevio\Trips\Repositories\TripConstraints;
use Cleevio\Trips\Validators\BaseTripValidator;
use Cleevio\Trips\Validators\HostTripValidator;
use Cleevio\Trips\Validators\ITripValidator;
use Cleevio\Trips\Validators\QuestionsTripValidator;
use Cleevio\Trips\Validators\RegistrationTripValidator;
use Cleevio\Trips\Validators\SubmitTripValidator;
use Cleevio\Users\Entities\User;
use Cleevio\Users\IUserService;
use Cleevio\Users\UserNotFound;
use DateTime;
use InvalidArgumentException;
use Nette\Http\IRequest;
use Nette\SmartObject;

class TripService implements ITripService
{

	private const DATE_FORMAT = 'Y-m-d';

	private const DATETIME_FORMAT = 'Y-m-d H:i:s';

	use SmartObject;

	/**
	 * @var ITripRepository
	 */
	private $tripRepository;

	/**
	 * @var ITripValidator[]
	 */
	private $tripValidators;

	/**
	 * @var ICountryService
	 */
	private $countryService;

	/**
	 * @var IHostsService
	 */
	private $hostsService;

	/**
	 * @var IUserService
	 */
	private $userService;

	/**
	 * @var IRegistrationService
	 */
	private $registrationService;


	/**
	 * @param ITripRepository $tripRepository
	 * @param IAnswerService $answerService
	 * @param IFieldService $fieldService
	 * @param ICountryService $countryService
	 * @param IHostsService $hostsService
	 * @param IQuestionService $questionService
	 * @param IUserService $userService
	 * @param IRegistrationService $registrationService
	 */
	public function __construct(
		ITripRepository $tripRepository,
		IAnswerService $answerService,
		IFieldService $fieldService,
		ICountryService $countryService,
		IHostsService $hostsService,
		IQuestionService $questionService,
		IUserService $userService,
		IRegistrationService $registrationService
	)
	{
		$this->tripRepository = $tripRepository;
		$this->tripValidators = [
			self::TRIP_VALIDATION_BASIC => new BaseTripValidator($this->tripRepository),
			self::TRIP_VALIDATION_PURPOSE => new QuestionsTripValidator($questionService, $answerService, QuestionGroup::QUESTION_GROUP_TYPE_PURPOSE),
			self::TRIP_VALIDATION_HOST => new HostTripValidator($fieldService, $hostsService),
			self::TRIP_VALIDATION_REGISTRATION => new RegistrationTripValidator,
			self::TRIP_VALIDATION_DETAILS => new QuestionsTripValidator($questionService, $answerService, QuestionGroup::QUESTION_GROUP_TYPE_DETAIL),
			self::TRIP_VALIDATION_SUBMITTED => new SubmitTripValidator,
		];

		$this->countryService = $countryService;
		$this->hostsService = $hostsService;
		$this->userService = $userService;
		$this->registrationService = $registrationService;
	}


	/**
	 * @param Country $country
	 * @param User $user
	 * @param DateTime $start
	 * @param DateTime $end
	 * @param string $state
	 * @return Trip
	 * @throws ValidationException
	 */
	public function createTrip(
		Country $country,
		User $user,
		DateTime $start,
		DateTime $end,
		string $state = Trip::TRIP_STATE_DRAFT
	): Trip
	{
		$trip = new Trip($country, $user, $start, $end, $state);

		$this->validateTrip($trip, TripService::TRIP_VALIDATION_PURPOSE);

		$this->tripRepository->persist($trip);

		return $trip;
	}


	/**
	 * @param int $id
	 * @return Trip|null
	 */
	public function findById(int $id): ?Trip
	{
		return $this->tripRepository->find($id);
	}


	/**
	 * @param Trip $trip
	 * @return bool
	 */
	public function deleteTrip(Trip $trip): bool
	{
		if ($trip->canDelete()) {
			$trip->setState(Trip::TRIP_STATE_DELETED);
			$this->tripRepository->persist($trip);

			return true;
		}

		return false;
	}


	/**
	 * @param Trip $old
	 * @return Trip
	 */
	public function copyTrip(Trip $old): Trip
	{
		$trip = clone $old;
		$this->tripRepository->persist($trip);

		return $trip;
	}


	/**
	 * Find the lowest validation error step for trip
	 * Returns null if trip is correct
	 * @param Trip $trip Trip to validate
	 * @param string|null $max Maximum validation step
	 * @throws ValidationException
	 */
	public function validateTrip(Trip $trip, ?string $max): void
	{
		foreach ($this->tripValidators as $key => $validator) {
			if ($max === $key) {
				return;
			}

			$validator->validate($trip);
		}

		return;
	}


	/**
	 * @param Trip $trip
	 * @param string $state
	 * @param bool $validate
	 * @param string|null $stateMessage
	 * @throws TripStateInvalid
	 * @throws ValidationException
	 */
	public function setTripState(Trip $trip, string $state, bool $validate, ?string $stateMessage = null): void
	{
		if ($validate) {
			$this->validateTrip($trip, null);
		}

		try {
			$trip->setState($state);
			$trip->setStateMessage($stateMessage);

		} catch (InvalidArgumentException $e) {
			throw new TripStateInvalid;
		}

		$this->tripRepository->persist($trip);
	}


	/**
	 * @param Trip $trip
	 * @return array|null
	 */
	public function getCurrentStep(Trip $trip): ?array
	{
		foreach ($this->tripValidators as $key => $validator) {
			try {
				$validator->validate($trip);
			} catch (ValidationException $e) {
				return [$key => [
					'error' => $e->getMessage(),
					'params' => $e->getParams(),
				]];
			}
		}

		return [self::TRIP_VALIDATION_SUMMARY => []];
	}


	/**
	 * Update constraints depending on query parameters for filtering trips
	 * @param TripConstraints $tripConstraints
	 * @param IRequest $request
	 * @return TripConstraints
	 * @throws CountryNotFound
	 * @throws HostNotFound
	 * @throws InvalidOrderByParameter
	 * @throws TripQueryInvalidDate
	 * @throws TripQueryInvalidDateTime
	 * @throws UserNotFound
	 * @throws RegistrationNotFound
	 */
	public function queryToConstraints(TripConstraints $tripConstraints, IRequest $request): TripConstraints
	{
		if ($request->getQuery("state") !== null) {
			$tripConstraints->setState($request->getQuery("state"));
		}

		if ($request->getQuery("isFavorite") !== null) {
			$tripConstraints->isFavorite((bool) $request->getQuery("isFavorite"));
		}

		if ($request->getQuery("minStartDate") !== null) {
			if (DateTime::createFromFormat(self::DATE_FORMAT, $request->getQuery("minStartDate")) === false) {
				throw new TripQueryInvalidDate;
			}

			$tripConstraints->setMinStartDate($request->getQuery("minStartDate"));
		}

		if ($request->getQuery("maxStartDate") !== null) {
			if (DateTime::createFromFormat(self::DATE_FORMAT, $request->getQuery("maxStartDate")) === false) {
				throw new TripQueryInvalidDate;
			}

			$tripConstraints->setMaxStartDate($request->getQuery("maxStartDate"));
		}

		if ($request->getQuery("minEndDate") !== null) {
			if (DateTime::createFromFormat(self::DATE_FORMAT, $request->getQuery("minEndDate")) === false) {
				throw new TripQueryInvalidDate;
			}

			$tripConstraints->setMinEndDate($request->getQuery("minEndDate"));
		}

		if ($request->getQuery("maxEndDate") !== null) {
			if (DateTime::createFromFormat(self::DATE_FORMAT, $request->getQuery("maxEndDate")) === false) {
				throw new TripQueryInvalidDate;
			}

			$tripConstraints->setMaxEndDate($request->getQuery("maxEndDate"));
		}

		if ($request->getQuery("minSubmittedDateTime") !== null) {
			if (DateTime::createFromFormat(self::DATETIME_FORMAT, $request->getQuery("minSubmittedDateTime")) === false) {
				throw new TripQueryInvalidDateTime;
			}

			$tripConstraints->setMinSubmittedDateTime($request->getQuery("minSubmittedDateTime"));
		}

		if ($request->getQuery("maxSubmittedDateTime") !== null) {
			if (DateTime::createFromFormat(self::DATETIME_FORMAT, $request->getQuery("maxSubmittedDateTime")) === false) {
				throw new TripQueryInvalidDateTime;
			}

			$tripConstraints->setMaxSubmittedDateTime($request->getQuery("maxSubmittedDateTime"));
		}

		if ($request->getQuery('search') !== null) {
			$tripConstraints->setSearch($request->getQuery('search'));
		}

		if ($request->getQuery("country") !== null) {
			$country = $this->countryService->findByCode($request->getQuery("country"));

			if ($country === null) {
				throw new CountryNotFound;
			}

			$tripConstraints->setCountry($country);
		}

		if ($request->getQuery("host") !== null) {
			$host = $this->hostsService->getHost((int) $request->getQuery("host"));

			if ($host === null) {
				throw new HostNotFound;
			}

			$tripConstraints->setHost($host);
		}

		if ($request->getQuery("registration") !== null) {
			$host = $this->registrationService->getRegistration((int) $request->getQuery("registration"));

			if ($host === null) {
				throw new RegistrationNotFound;
			}

			$tripConstraints->setRegistration($host);
		}

		if ($request->getQuery("user") !== null) {
			$user = $this->userService->getUser((int) $request->getQuery("user"));

			if ($user === null) {
				throw new UserNotFound;
			}

			$tripConstraints->setUser($user);
		}

		if ($request->getQuery("orderBy") !== null) {
			try {
				$tripConstraints->setOrderBy($request->getQuery("orderBy"));
			} catch (InvalidArgumentException $e) {
				throw new InvalidOrderByParameter;
			}
		}

		if ($request->getQuery("orderDir") !== null) {
			$tripConstraints->setOrderDir($request->getQuery("orderDir"));
		}

		return $tripConstraints;
	}

}
