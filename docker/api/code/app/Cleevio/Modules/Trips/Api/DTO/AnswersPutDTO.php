<?php

declare(strict_types=1);

namespace Cleevio\Trips\Api\DTO;

use Cleevio\RestApi\Command\ICommandDTO;

class AnswersPutDTO implements ICommandDTO
{
	/**
	 * @var array
	 */
	protected $questions;


	public function __construct(
		array $questions
	)
	{
		$this->questions = $questions;
	}

	public static function fromRequest($data): ICommandDTO
	{
		$answers = [];

		foreach ($data as $answer) {
			$answers[(int) $answer->id] = ((array) $answer);
		}

		return new static($answers);
	}

	public function getQuestions(): array
	{
		return $this->questions;
	}

	public function getQuestion(int $id): ?array
	{
		foreach ($this->questions as $key => $question) {
			if ($key === $id) {
				return $question;
			}
		}

		return null;
	}

}
