<?php

declare(strict_types=1);

namespace Cleevio\Trips\Api\Response;

use Cleevio\Registrations\Api\Response\RegistrationsResponseBuilder;
use Cleevio\RestApi\Response\SimpleResponse;
use Cleevio\Trips\Entities\Trip;
use Cleevio\Trips\Entities\TripRegistration;

final class TripRegistrationResponseBuilder extends SimpleResponse
{

	/**
	 * @var Trip
	 */
	private $data;


	/**
	 * TripResponseBuilder constructor.
	 * @param Trip $trip
	 */
	public function __construct(Trip $trip)
	{
		$this->data = $trip;
	}


	/**
	 * @return array
	 */
	protected function data(): array
	{
		return [
			'registrations' => (new RegistrationsResponseBuilder(array_map(static function (TripRegistration $tripRegistration) {
				return $tripRegistration->getRegistration();
			}, $this->data->getRegistrations())))->build(),
			'checked' => $this->data->getRegistrationCheck() !== null ? $this->data->getRegistrationCheck()->format("Y-m-d H:i:s") : null,
		];
	}
}
