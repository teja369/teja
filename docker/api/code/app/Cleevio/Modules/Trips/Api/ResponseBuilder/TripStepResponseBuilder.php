<?php

declare(strict_types=1);

namespace Cleevio\Trips\Api\Response;

use Cleevio\Api\Translatable\SimpleResponse;

final class TripStepResponseBuilder extends SimpleResponse
{

	/**
	 * @var array|null
	 */
	private $data;


	/**
	 * TripStepResponseBuilder constructor.
	 * @param array|null $step
	 * @param string|null $lang
	 */
	public function __construct(?array $step, ?string $lang)
	{
		parent::__construct($lang);

		$this->data = $step;
	}


	/**
	 * @return array
	 */
	protected function data(): array
	{
		$key = $this->data !== null
			? key($this->data)
			: null;

		return [
			'step' => $key,
			'params' => $key !== null && array_key_exists('params', $this->data[$key]) ? $this->data[$key]['params'] : [],
			'error' => $key !== null && array_key_exists('error', $this->data[$key])  ? $this->data[$key]['error'] : null,
		];
	}
}
