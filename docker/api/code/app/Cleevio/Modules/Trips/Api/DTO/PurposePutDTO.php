<?php

declare(strict_types=1);

namespace Cleevio\Trips\Api\DTO;

use Cleevio\RestApi\Command\ICommandDTO;

class PurposePutDTO implements ICommandDTO
{
	/**
	 * @var int[]
	 */
	protected $purposes;


	public function __construct(
		array $purposes
	)
	{
		$this->purposes = $purposes;
	}


	public static function fromRequest($data): ICommandDTO
	{
		return new static(
			array_map(static function ($purpose) {
				return $purpose->id;
			}, $data)
		);
	}

	/**
	 * @return int[]
	 */
	public function getPurposes(): array
	{
		return $this->purposes;
	}

}
