<?php

declare(strict_types=1);

namespace Cleevio\Trips\Api\Response;

use Cleevio\Answers\Api\Response\AnswersResponseBuilder;
use Cleevio\Answers\IAnswerService;
use Cleevio\Api\Translatable\SimpleResponse;
use Cleevio\Countries\Api\Response\CountryResponseBuilder;
use Cleevio\Hosts\Api\Response\HostResponseBuilder;
use Cleevio\Trips\Entities\Trip;
use Cleevio\Users\Api\Response\UserResponseBuilder;

final class TripResponseBuilder extends SimpleResponse
{

	/**
	 * @var Trip
	 */
	private $data;

	/**
	 * @var IAnswerService
	 */
	private $answerService;


	/**
	 * TripResponseBuilder constructor.
	 * @param Trip $trip
	 * @param IAnswerService $answerService
	 * @param string|null $lang
	 */
	public function __construct(Trip $trip, IAnswerService $answerService, ?string $lang)
	{
		parent::__construct($lang);

		$this->data = $trip;
		$this->answerService = $answerService;
	}


	/**
	 * @return array
	 */
	protected function data(): array
	{
		return [
			'id' => $this->data->getId(),
			'start' => $this->data->getStart()->format("Y-m-d"),
			'end' => $this->data->getEnd()->format("Y-m-d"),
			'country' => (new CountryResponseBuilder($this->data->getCountry(), $this->getLang()))->build(),
			'host' => $this->data->getHost() !== null ? (new HostResponseBuilder($this->data->getHost(), $this->getLang()))->build() : null,
			'hostSet' => $this->data->getHostSet() !== null ? $this->data->getHostSet()->format("Y-m-d H:i:s") : null,
			'registration' => (new TripRegistrationResponseBuilder($this->data))->build(),
			'answers' => (new AnswersResponseBuilder($this->answerService->getAnswers($this->data, true), $this->getLang()))->build(),
			'state' => $this->data->getState(),
			'stateMessage' => $this->data->getStateMessage(),
			'user' => (new UserResponseBuilder($this->data->getUser(), $this->getLang()))->build(),
			'isFavorite' => $this->data->isFavorite($this->data->getUser()),
			'updatedAt' => $this->data->getUpdatedAt()->format("Y-m-d H:i:s"),
			'createdAt' => $this->data->getCreatedAt()->format("Y-m-d H:i:s"),
			'submittedAt' => $this->data->getSubmittedAt() !== null
				? $this->data->getSubmittedAt()->format("Y-m-d H:i:s")
				: null,
		];
	}
}
