<?php

declare(strict_types=1);

namespace Cleevio\Trips\Api\Response;

use Cleevio\Answers\IAnswerService;
use Cleevio\Api\Translatable\PagedResponse;
use Cleevio\RestApi\Request\PagedRequest;
use Cleevio\Trips\Entities\Trip;

final class TripsResponseBuilder extends PagedResponse
{

	/**
	 * @var Trip[]
	 */
	private $data;

	/**
	 * @var IAnswerService
	 */
	private $answerService;

	/**
	 * TripResponseBuilder constructor.
	 * @param PagedRequest $request
	 * @param Trip[] $trip
	 * @param IAnswerService $answerService
	 * @param string|null $lang
	 */
	public function __construct(PagedRequest $request, array $trip, IAnswerService $answerService, ?string $lang)
	{
		parent::__construct($request, $lang);

		$this->data = $trip;
		$this->answerService = $answerService;
	}

	/**
	 * @return Trip[]
	 */
	protected function data(): array
	{
		return array_map(function (Trip $user) {
			return (new TripResponseBuilder($user, $this->answerService, $this->getLang()))->build();
		}, $this->data);
	}
}
