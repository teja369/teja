<?php

declare(strict_types=1);

namespace Cleevio\Trips\Api\DTO;

use Cleevio\RestApi\Command\ICommandDTO;
use Cleevio\RestApi\Exceptions\BadRequestException;
use DateTime;

class CreatePostDTO implements ICommandDTO
{

	private const DATE_FORMAT = 'Y-m-d';

	/**
	 * @var DateTime
	 */
	protected $start;

	/**
	 * @var DateTime
	 */
	protected $end;

	/**
	 * @var string
	 */
	protected $country;


	public function __construct(
		DateTime $start,
		DateTime $end,
		string $country
	)
	{
		$this->start = $start;
		$this->end = $end;
		$this->country = $country;
	}


	public static function fromRequest($data): ICommandDTO
	{
		$start = DateTime::createFromFormat(self::DATE_FORMAT, $data->start);
		$end = DateTime::createFromFormat(self::DATE_FORMAT, $data->end);
		$today = (new DateTime)->setTime(0, 0);

		if ($start === false || $end === false) {
			throw new BadRequestException('trips.create.error.invalid-date');
		}

		if ($start < $today || $end < $today) {
			throw new BadRequestException('trips.create.error.past-date');
		}

		if ($end < $start) {
			throw new BadRequestException('trips.create.error.start-greater');
		}

		return new static(
			$start,
			$end,
			$data->country
		);
	}


	/**
	 * @return DateTime
	 */
	public function getStart(): DateTime
	{
		return $this->start;
	}


	/**
	 * @return DateTime
	 */
	public function getEnd(): DateTime
	{
		return $this->end;
	}


	/**
	 * @return string
	 */
	public function getCountry(): string
	{
		return $this->country;
	}

}
