<?php

declare(strict_types=1);

namespace Cleevio\Trips\Api\DTO;

use Cleevio\RestApi\Command\ICommandDTO;

class StateMessagePutDTO implements ICommandDTO
{

	/**
	 * @var string
	 */
	protected $message;


	public function __construct(string $message)
	{
		$this->message = $message;
	}


	public static function fromRequest($data): ICommandDTO
	{
		return new static($data->message);
	}


	/**
	 * @return string
	 */
	public function getStateMessage(): string
	{
		return $this->message;
	}

}
