<?php

declare(strict_types=1);

namespace Cleevio\Trips\Repositories;

use Cleevio\Countries\Entities\Country;
use Cleevio\Repository\Repositories\SqlRepository;
use Cleevio\Trips\Entities\Trip;
use Doctrine\Common\Collections\Criteria;

class TripRepository extends SqlRepository implements ITripRepository
{

	/**
	 * Model class type
	 * @return string
	 */
	public function type(): string
	{
		return Trip::class;
	}

	/**
	 * @param Country[] $countries
	 */
	public function deleteByCountries(array $countries): void
	{
		$this->getRepository()
			->createQueryBuilder("t")
			->addCriteria(Criteria::create()->where(Criteria::expr()->eq("state", Trip::TRIP_STATE_DRAFT)))
			->addCriteria(Criteria::create()->where(Criteria::expr()->in("country", array_map(static function (Country $country) {
				return $country->getId();
			}, $countries))))
			->delete()
			->getQuery()
			->execute();

	}
}
