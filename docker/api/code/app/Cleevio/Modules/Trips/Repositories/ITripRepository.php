<?php

declare(strict_types=1);

namespace Cleevio\Trips\Repositories;

use Cleevio\Countries\Entities\Country;
use Cleevio\Repository\IRepository;

interface ITripRepository extends IRepository
{

	/**
	 * @param Country[] $countries
	 */
	public function deleteByCountries(array $countries): void;

}
