<?php

declare(strict_types=1);

namespace Cleevio\Trips\Repositories;

use Cleevio\Countries\Entities\Country;
use Cleevio\Hosts\Entities\Host;
use Cleevio\Registrations\Entities\Registration;
use Cleevio\Repository\ConstraintsBuilder;
use Cleevio\Repository\Expressions\Comparison;
use Cleevio\Repository\Expressions\CompositeExpression;
use Cleevio\Repository\Expressions\IsNull;
use Cleevio\Users\Entities\User;

class TripConstraints extends ConstraintsBuilder
{

	public function __construct()
	{
		$this->addCriteria(new Comparison("state", "<>", 'deleted'));
	}

	function setState(string $state): TripConstraints
	{
		$states = explode('|', $state);
		$constraints = [];

		foreach ($states as $s) {
			$constraints[] = new Comparison("state", "=", $s);
		}

		$this->addCriteria(new CompositeExpression(CompositeExpression::TYPE_OR, $constraints));

		return $this;
	}

	function setMinStartDate(string $date): TripConstraints
	{
		$this->addCriteria(new Comparison("start", ">=", $date));

		return $this;
	}

	function setMaxStartDate(string $date): TripConstraints
	{
		$this->addCriteria(new Comparison("start", "<=", $date));

		return $this;
	}

	function setMinEndDate(string $date): TripConstraints
	{
		$this->addCriteria(new Comparison("end", ">=", $date));

		return $this;
	}

	function setMaxEndDate(string $date): TripConstraints
	{
		$this->addCriteria(new Comparison("end", "<=", $date));

		return $this;
	}

	function setMinSubmittedDateTime(string $date): TripConstraints
	{
		$this->addCriteria(new Comparison("submittedAt", ">=", $date));

		return $this;
	}

	function setMaxSubmittedDateTime(string $date): TripConstraints
	{
		$this->addCriteria(new Comparison("submittedAt", "<=", $date));

		return $this;
	}

	function isFavorite(bool $isFavorite): TripConstraints
	{
		if ($isFavorite) {
			$this->addCriteria(new Comparison("favorites.user", ">", "0"));
		} else {
			$this->addCriteria(new IsNull("favorites.user"));
		}

		return $this;
	}

	function setUser(User $user): TripConstraints
	{
		$this->addCriteria(new Comparison("user.id", "=", $user->getId() . ""));

		return $this;
	}

	function setCountry(Country $country): TripConstraints
	{
		$this->addCriteria(new Comparison("country.code", "=", $country->getCode()));

		return $this;
	}

	function setHost(Host $host): TripConstraints
	{
		$this->addCriteria(new Comparison("host", "=", $host->getId()));

		return $this;
	}

	function setRegistration(Registration $registration): TripConstraints
	{
		$this->addCriteria(new Comparison("registrations.registration.id", "=", $registration->getId()));

		return $this;
	}

	function setSearch(string $search): TripConstraints
	{
		$this->addCriteria(new CompositeExpression(CompositeExpression::TYPE_OR, [
			new Comparison('user.name', 'CONTAINS', $search),
			new Comparison('user.email', 'CONTAINS', $search),
			new Comparison('user.username', 'CONTAINS', $search),
			new Comparison('country.name', 'CONTAINS', $search),
			new Comparison('host.name', 'CONTAINS', $search),
		]));

		return $this;
	}

	public function setOrderBy(?string $orderBy): ConstraintsBuilder
	{
		if ($orderBy !== null && !in_array($orderBy, ['start'], true)) {
			throw new \InvalidArgumentException;
		}

		return parent::setOrderBy($orderBy);
	}

}
