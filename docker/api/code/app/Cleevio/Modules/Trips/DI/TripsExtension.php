<?php

declare(strict_types=1);

namespace Cleevio\Trips\DI;

use Cleevio\Modules\Providers\IPresenterMappingProvider;
use Nette\DI\CompilerExtension;
use Nettrine\ORM\DI\Traits\TEntityMapping;

class TripsExtension extends CompilerExtension implements IPresenterMappingProvider
{

	use TEntityMapping;

	public function loadConfiguration()
	{
		$this->compiler->loadConfig(__DIR__ . '/services.neon');
		$this->setEntityMappings(
			[
				'Cleevio\Trips\Entities\Trip' => __DIR__ . '/..',
			]
		);
	}

	public function getPresenterMapping(): array
	{
		return ['Trips' => 'Cleevio\\Trips\\*Module\\Presenters\\*Presenter'];
	}

}
