<?php

declare(strict_types=1);

namespace Cleevio\Trips;

use Cleevio\Countries\CountryNotFound;
use Cleevio\Countries\Entities\Country;
use Cleevio\Hosts\HostNotFound;
use Cleevio\Questions\RegistrationNotFound;
use Cleevio\Trips\Entities\Trip;
use Cleevio\Trips\Repositories\TripConstraints;
use Cleevio\Users\Entities\User;
use Cleevio\Users\UserNotFound;
use DateTime;
use Nette\Http\IRequest;

interface ITripService
{

	public const TRIP_VALIDATION_BASIC = 'basic';

	public const TRIP_VALIDATION_PURPOSE = 'purpose';

	public const TRIP_VALIDATION_HOST = 'host';

	public const TRIP_VALIDATION_REGISTRATION = 'registration';

	public const TRIP_VALIDATION_DETAILS = 'details';

	public const TRIP_VALIDATION_SUMMARY = 'summary';

	public const TRIP_VALIDATION_SUBMITTED = 'submitted';


	/**
	 * @param Country $country
	 * @param User $user
	 * @param DateTime $start
	 * @param DateTime $end
	 * @param string $state
	 * @return Trip
	 * @throws ValidationException
	 */
	public function createTrip(
		Country $country,
		User $user,
		DateTime $start,
		DateTime $end,
		string $state = Trip::TRIP_STATE_DRAFT
	): Trip;


	/**
	 * @param int $id
	 * @return Trip|null
	 */
	public function findById(int $id): ?Trip;


	/**
	 * @param Trip $old
	 * @return Trip
	 */
	public function copyTrip(Trip $old): Trip;


	/**
	 * @param Trip $trip
	 * @return bool
	 */
	public function deleteTrip(Trip $trip): bool;


	/**
	 * Find the lowest validation error step for trip
	 * Returns null if trip is correct
	 * @param Trip $trip Trip to validate
	 * @param string|null $max Maximum validation step
	 * @throws ValidationException
	 */
	public function validateTrip(Trip $trip, ?string $max): void;


	/**
	 * @param Trip $trip
	 * @param string $state
	 * @param bool $validate
	 * @param string|null $stateMessage
	 * @throws TripStateInvalid
	 * @throws ValidationException
	 */
	public function setTripState(Trip $trip, string $state, bool $validate, ?string $stateMessage = null): void;


	/**
	 * @param Trip $trip
	 * @return array|null
	 */
	public function getCurrentStep(Trip $trip): ?array;


	/**
	 * Update constraints depending on query parameters for filtering trips
	 * @param TripConstraints $constraints
	 * @param IRequest $request
	 * @return TripConstraints
	 * @throws CountryNotFound
	 * @throws HostNotFound
	 * @throws InvalidOrderByParameter
	 * @throws TripQueryInvalidDate
	 * @throws TripQueryInvalidDateTime
	 * @throws UserNotFound
	 * @throws RegistrationNotFound
	 */
	public function queryToConstraints(TripConstraints $constraints, IRequest $request): TripConstraints;
}
