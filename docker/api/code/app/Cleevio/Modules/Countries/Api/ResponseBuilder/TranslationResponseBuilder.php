<?php

declare(strict_types=1);

namespace Cleevio\Countries\Api\Response;

use Cleevio\Api\Translatable\SimpleResponse;
use Cleevio\Countries\Entities\Country;
use Cleevio\Countries\ICountryTranslation;

final class TranslationResponseBuilder extends SimpleResponse implements ICountryTranslation
{

	/**
	 * @var Country
	 */
	private $data;

	/**
	 * QuestionResponseBuilder constructor.
	 * @param Country $question
	 * @param string|null $lang
	 */
	public function __construct(Country $question, ?string $lang)
	{
		parent::__construct($lang);

		$this->data = $question;
	}

	/**
	 * @return array
	 */
	protected function data(): array
	{
		return [
			'name' => $this->getName(),
		];
	}

	function getName(): ?string
	{
		return $this->data->getName($this->getLang());
	}

}
