<?php

declare(strict_types=1);

namespace Cleevio\Countries\Api\Response;

use Cleevio\Api\Translatable\PagedResponse;
use Cleevio\Countries\Entities\Country;
use Cleevio\RestApi\Request\PagedRequest;

final class CountriesPagedResponseBuilder extends PagedResponse
{
	/**
	 * @var array
	 */
	private $data;


	/**
	 * CountriesPagedResponseBuilder constructor.
	 * @param PagedRequest $request
	 * @param array        $data
	 * @param string|null  $lang
	 */
	public function __construct(PagedRequest $request, array $data, ?string $lang)
	{
		parent::__construct($request, $lang);

		$this->data = $data;
	}

	/**
	 * @return array
	 */
	protected function data(): array
	{
		return array_map(function (Country $country) {
			return (new CountryResponseBuilder($country, $this->getLang()))->build();
		}, $this->data);
	}

}
