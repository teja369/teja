<?php

declare(strict_types=1);

namespace Cleevio\Countries\Api\Response;

use Cleevio\Api\Translatable\SimpleResponse;
use Cleevio\Countries\Entities\Country;

final class CountriesResponseBuilder extends SimpleResponse
{
	/**
	 * @var array
	 */
	private $data;

	/**
	 * CountriesResponseBuilder constructor.
	 * @param array $data
	 * @param string|null $lang
	 */
	public function __construct(array $data, ?string $lang)
	{
		parent::__construct($lang);

		$this->data = $data;
	}

	/**
	 * @return array
	 */
	protected function data(): array
	{
		return array_map(function (Country $country) {
			return (new CountryResponseBuilder($country, $this->getLang()))->build();
		}, $this->data);
	}

}
