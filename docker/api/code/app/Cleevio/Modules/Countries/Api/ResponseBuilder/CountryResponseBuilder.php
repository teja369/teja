<?php

declare(strict_types=1);

namespace Cleevio\Countries\Api\Response;

use Cleevio\Api\Translatable\SimpleResponse;
use Cleevio\Countries\Entities\Country;

final class CountryResponseBuilder extends SimpleResponse
{
	/**
	 * @var Country
	 */
	private $data;

	/**
	 * CountryResponseBuilder constructor.
	 * @param Country $country
	 * @param string|null $lang
	 */
	public function __construct(Country $country, ?string $lang)
	{
		parent::__construct($lang);

		$this->data = $country;
	}

	/**
	 * @return array
	 */
	protected function data(): array
	{
		return [
			'id' => $this->data->getId(),
			'code' => $this->data->getCode(),
			'name' => $this->data->getName($this->getLang()),
			'isEnabled' => $this->data->isEnabled(),
			'isHome' => $this->data->isHome(),
		];
	}
}
