<?php

declare(strict_types=1);

namespace Cleevio\Countries\Repositories;

use Cleevio\Countries\Entities\Country;
use Cleevio\Repository\Repositories\SqlRepository;
use Doctrine\Common\Collections\Criteria;

class CountryRepository extends SqlRepository implements ICountryRepository
{

	public function type(): string
	{
		return Country::class;
	}

	/**
	 * @param int[] $ids
	 * @return Country[]
	 */
	public function findByIds(array $ids): array
	{
		$result = $this->getRepository()
			->createQueryBuilder("t")
			->addCriteria(Criteria::create()->where(Criteria::expr()->in("id", $ids)))
			->getQuery()
			->execute();

		return !is_null($result) ? $result : [];
	}

	/**
	 * @param string[] $codes
	 * @return Country[]
	 */
	public function findByCodes(array $codes): array
	{
		$result = $this->getRepository()
			->createQueryBuilder("t")
			->addCriteria(Criteria::create()->where(Criteria::expr()->in("code", $codes)))
			->getQuery()
			->execute();

		return !is_null($result) ? $result : [];
	}
}
