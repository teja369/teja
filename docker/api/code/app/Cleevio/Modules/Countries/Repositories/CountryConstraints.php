<?php

declare(strict_types=1);

namespace Cleevio\Countries\Repositories;

use Cleevio\Repository\ConstraintsBuilder;
use Cleevio\Repository\Expressions\Comparison;
use Cleevio\Repository\Expressions\CompositeExpression;

class CountryConstraints extends ConstraintsBuilder
{

	function setCode(string $code): CountryConstraints
	{
		$this->addCriteria(new Comparison('code', '=', $code));

		return $this;
	}

	/**
	 * @param bool|null $isEnabled
	 * @return CountryConstraints
	 */
	function setIsEnabled(?bool $isEnabled): CountryConstraints
	{
		if ($isEnabled !== null) {
			$this->addCriteria(new Comparison('isEnabled', '=', $isEnabled));
		}

		return $this;
	}

	/**
	 * @param bool|null $isHome
	 * @return CountryConstraints
	 */
	function setIsHome(?bool $isHome): CountryConstraints
	{
		if ($isHome !== null) {
			$this->addCriteria(new Comparison('isHome', '=', $isHome));
		}

		return $this;
	}

	function setSearch(string $search, ?string $lang): CountryConstraints
	{
		if ($lang !== null) {
			$this->addCriteria(New CompositeExpression(CompositeExpression::TYPE_OR, [
				new Comparison('name', 'STARTS_WITH', $search),
				new CompositeExpression(CompositeExpression::TYPE_AND, [
					new Comparison('translations.content', 'STARTS_WITH', $search),
					new Comparison('translations.locale', '=', $lang),
					new Comparison('translations.field', '=', 'name'),
				]),
			]));
		} else {
			$this->addCriteria(new Comparison('name', 'STARTS_WITH', $search));
		}

		return $this;
	}
}
