<?php

declare(strict_types=1);

namespace Cleevio\Countries\Repositories;

use Cleevio\Countries\Entities\Country;
use Cleevio\Repository\IRepository;

interface ICountryRepository extends IRepository
{

	/**
	 * @param int[] $ids
	 * @return Country[]
	 */
	public function findByIds(array $ids): array;

	/**
	 * @param string[] $codes
	 * @return Country[]
	 */
	public function findByCodes(array $codes): array;

}
