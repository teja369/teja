<?php

declare(strict_types=1);

namespace Cleevio\Countries;

use Cleevio\Countries\Entities\Country;
use Cleevio\Countries\Entities\CountryTranslation;
use Cleevio\Countries\Repositories\CountryConstraints;
use Cleevio\Countries\Repositories\ICountryRepository;
use Cleevio\Languages\Entities\Language;

class CountryService implements ICountryService
{

	/**
	 * @var ICountryRepository
	 */
	private $countryRepository;


	/**
	 * @param ICountryRepository $countryRepository
	 */
	public function __construct(ICountryRepository $countryRepository)
	{
		$this->countryRepository = $countryRepository;
	}


	/**
	 * @param int $id
	 * @return Country|null
	 */
	public function getCountry(int $id): ?Country
	{
		return $this->countryRepository->find($id);
	}

	/**
	 * @param bool $showDisabled
	 * @param string|null $search
	 * @param string|null $language
	 * @return array
	 */
	public function getCountries(bool $showDisabled, ?string $search = null, ?string $language = null): array
	{
		$constraints = (new CountryConstraints)
			->setIsEnabled(!$showDisabled ? true : null);

		if ($search !== null) {
			$constraints->setSearch($search, $language);
		}

		return $this->countryRepository->findAll($constraints->build());
	}


	public function findByCode(string $code): ?Country
	{
		$constraints = (new CountryConstraints)->setCode($code)->build();

		return $this->countryRepository->findBy($constraints);
	}


	/**
	 * @param array $ids
	 * @return Country[]
	 * @throws CountryNotFound
	 */
	public function findByIds(array $ids): array
	{
		$ids = array_unique($ids);
		$countries = $this->countryRepository->findByIds($ids);

		if (count($countries) !== count($ids)) {
			throw new CountryNotFound;
		}

		return $countries;
	}



	/**
	 * @param string[] $codes
	 * @return Country[]
	 * @throws CountryNotFound
	 */
	public function findByCodes(array $codes): array
	{
		$codes = array_unique($codes);
		$countries = $this->countryRepository->findByCodes($codes);

		if (count($countries) !== count($codes)) {
			throw new CountryNotFound;
		}

		return $countries;
	}


	/**
	 * @param Country $country
	 */
	public function enableCountry(Country $country): void
	{
		$country->setIsEnabled(true);
		$this->countryRepository->persist($country);
	}


	/**
	 * @param Country $country
	 */
	public function disableCountry(Country $country): void
	{
		$country->setIsEnabled(false);
		$this->countryRepository->persist($country);
	}

	/**
	 * Translate question text parameters
	 * @param Country $country
	 * @param Language $language
	 * @param ICountryTranslation $translation
	 * @return Country
	 */
	public function translateCountry(Country $country, Language $language, ICountryTranslation $translation): Country
	{
		if ($translation->getName() !== null && $translation->getName() !== '') {
			$country->addTranslation(new CountryTranslation($language->getCode(), "name", $translation->getName()));
		} else {
			$country->clearTranslation("name", $language);
		}

		$this->countryRepository->persist($country);

		return $country;
	}
}
