<?php

declare(strict_types=1);

namespace Cleevio\Countries\ApiModule\Presenters;

use Cleevio\Api\Helpers\IRequestContext;
use Cleevio\Countries\Api\Response\CountriesPagedResponseBuilder;
use Cleevio\Countries\Api\Response\CountryResponseBuilder;
use Cleevio\Countries\ICountryService;
use Cleevio\Countries\Repositories\CountryConstraints;
use Cleevio\Countries\Repositories\ICountryRepository;
use Cleevio\Repository\Constraints;
use Cleevio\RestApi\Annotations\ApiRoute;
use Cleevio\RestApi\Annotations\Authenticated;
use Cleevio\RestApi\Annotations\Queries;
use Cleevio\RestApi\Annotations\Response;
use Cleevio\RestApi\Exceptions\AuthenticationException;
use Cleevio\RestApi\Exceptions\NotFoundException;
use Cleevio\RestApi\Presenters\RestPresenter;
use Cleevio\RestApi\Request\PagedRequest;
use Cleevio\RestApi\Response\EmptyResponse;
use Cleevio\Users\Entities\User;
use Nette\Http\IResponse;

/**
 * @ApiRoute("/api/v1/countries",presenter="Countries:Api:Country")
 */
class CountryPresenter extends RestPresenter
{

	/**
	 * @var ICountryService
	 */
	private $countryService;

	/**
	 * @var IRequestContext
	 */
	private $requestContext;

	/**
	 * @var ICountryRepository
	 */
	private $countryRepository;


	/**
	 * CountryPresenter constructor.
	 * @param ICountryService    $countryService
	 * @param IRequestContext    $requestContext
	 * @param ICountryRepository $countryRepository
	 */
	public function __construct(
		ICountryService $countryService,
		IRequestContext $requestContext,
		ICountryRepository $countryRepository
	)
	{
		parent::__construct();

		$this->countryService = $countryService;
		$this->requestContext = $requestContext;
		$this->countryRepository = $countryRepository;
	}


	/**
	 * @ApiRoute("/api/v1/countries", method="GET", description="Fetches all available countries")
	 * @Response(200, file="response/countries/countries.json")
	 * @Queries("queries/countries/countries.json")
	 * @Authenticated()
	 */
	public function actionCountriesGet(): void
	{
		$search = $this->getHttpRequest()->getQuery('search');

		$showDisabled = $this->getHttpRequest()->getQuery("showDisabled") !== null
			? (bool) $this->getHttpRequest()->getQuery("showDisabled")
			: false;

		$countryConstraints = new CountryConstraints;

		$countryConstraints->setIsEnabled(!$showDisabled ? true : null);
		$countryConstraints->setOrderBy("name");

		if ($search !== null) {
			$countryConstraints->setSearch($search, $this->requestContext->getLang());
		}

		$total = $this->countryRepository->count(Constraints::empty());
		$filtered = $this->countryRepository->count($countryConstraints->build());

		$request = PagedRequest::of($this->getHttpRequest());

		$countryConstraints = $request->toConstraints($countryConstraints)->build();

		$countries = $this->countryRepository->findAll($countryConstraints);

		$response = (new CountriesPagedResponseBuilder($request, $countries, $this->requestContext->getLang()))
			->setCurrent($filtered)
			->setTotal($total);

		$this->sendSuccessResponse($response, IResponse::S200_OK);
	}

	/**
	 * @ApiRoute("/api/v1/countries/home", method="GET", description="Get home country")
	 * @Response(200, file="response/countries/country.json")
	 * @Response(404, code="10001", description="Country was not found")
	 * @Authenticated()
	 */
	public function actionCountriesHomeGet(): void
	{
		$constraints = new CountryConstraints;
		$constraints->setIsHome(true);
		$country = $this->countryRepository->findBy($constraints->build());

		if ($country === null) {
			throw new NotFoundException('countries.country.get.error.country-not-found', 10001);
		}

		$this->sendSuccessResponse(new CountryResponseBuilder($country, $this->requestContext->getLang()), IResponse::S200_OK);
	}


	/**
	 * @ApiRoute("/api/v1/countries/<code>/enable", method="PUT", description="Enable country for users")
	 * @Response(200)
	 * @Response(404, code="10001", description="Country was not found")
	 * @Authenticated("admin.countries")
	 * @param string $code
	 */
	public function actionCountryEnable(string $code): void
	{

		$authenticator = $this->getAuthenticator();

		$user = $authenticator->getUser();

		if (!$user instanceof User) {
			throw new AuthenticationException("users.auth.error.user-not-found", 10003);
		}

		$country = $this->countryService->findByCode($code);

		if ($country === null) {
			throw new NotFoundException('countries.country.get.error.country-not-found', 10001);
		}

		$this->countryService->enableCountry($country);

		$this->sendSuccessResponse(new EmptyResponse, IResponse::S200_OK);
	}


	/**
	 * @ApiRoute("/api/v1/countries/<code>/enable", method="DELETE", description="Disable country for users")
	 * @Response(200)
	 * @Response(404, code="10001", description="Country was not found")
	 * @Authenticated("admin.countries")
	 * @param string $code
	 */
	public function actionCountryDisable(string $code): void
	{
		$authenticator = $this->getAuthenticator();

		$user = $authenticator->getUser();

		if (!$user instanceof User) {
			throw new AuthenticationException("users.auth.error.user-not-found", 10003);
		}

		$country = $this->countryService->findByCode($code);

		if ($country === null) {
			throw new NotFoundException('countries.country.get.error.country-not-found', 10001);
		}

		$this->countryService->disableCountry($country);

		$this->sendSuccessResponse(new EmptyResponse, IResponse::S200_OK);
	}

}
