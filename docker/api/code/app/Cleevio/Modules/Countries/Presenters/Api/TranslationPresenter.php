<?php

declare(strict_types=1);

namespace Cleevio\Countries\ApiModule\Presenters;

use Cleevio\Countries\Api\DTO\TranslationPutDTO;
use Cleevio\Countries\Api\Response\TranslationResponseBuilder;
use Cleevio\Countries\ICountryService;
use Cleevio\Languages\ILanguageService;
use Cleevio\RestApi\Annotations\ApiRoute;
use Cleevio\RestApi\Annotations\Authenticated;
use Cleevio\RestApi\Annotations\DTO;
use Cleevio\RestApi\Annotations\JsonSchema;
use Cleevio\RestApi\Annotations\Response;
use Cleevio\RestApi\Exceptions\AuthenticationException;
use Cleevio\RestApi\Exceptions\NotFoundException;
use Cleevio\RestApi\Presenters\RestPresenter;
use Cleevio\Users\Entities\User;
use InvalidArgumentException;
use Nette\Http\IResponse;

/**
 * @ApiRoute("/api/v1/questions",presenter="Countries:Api:Translation", tags={"Country"})
 */
class TranslationPresenter extends RestPresenter
{

	/**
	 * @var ICountryService
	 */
	private $countryService;

	/**
	 * @var ILanguageService
	 */
	private $languageService;

	/**
	 * @param ICountryService $countryService
	 * @param ILanguageService $languageService
	 */
	public function __construct(ICountryService $countryService,
								ILanguageService $languageService)
	{
		parent::__construct();

		$this->languageService = $languageService;
		$this->countryService = $countryService;
	}

	/**
	 * @ApiRoute("/api/v1/countries/<id>/translation/<lang>", method="GET", description="Get all translations set for selected country")
	 * @Response(200, file="response/countries/translation.json")
	 * @Response(404, code="10002", description="Langauge with provided code was not found")
	 * @Response(404, code="10003", description="Country with provided ID was not found")
	 * @Authenticated("admin.countries")
	 * @param int $id
	 * @param string $lang
	 */
	public function actionList(int $id, string $lang): void
	{
		$authenticator = $this->getAuthenticator();

		$user = $authenticator->getUser();

		if (!$user instanceof User) {
			throw new AuthenticationException("users.auth.error.user-not-found", 10001);
		}

		$country = $this->countryService->getCountry($id);

		if ($country === null) {
			throw new NotFoundException('countries.country.get.error.country-not-found', 10003);
		}

		$language = $this->languageService->getLanguage($lang);

		if ($language === null) {
			throw new NotFoundException('languages.language.get.not-found', 10002);
		}

		$this->sendSuccessResponse(new TranslationResponseBuilder($country, $lang), IResponse::S200_OK);
	}

	/**
	 * @ApiRoute("/api/v1/countries/<id>/translation/<lang>", method="PUT", description="Update translations for a provided country")
	 * @Response(200, file="response/countries/translation.json")
	 * @DTO("Cleevio\Countries\Api\DTO\TranslationPutDTO")
	 * @JsonSchema("request/countries/putTranslation.json")
	 * @Response(404, code="10002", description="Langauge with provided code was not found")
	 * @Response(404, code="10003", description="Country with provided ID was not found")
	 * @Authenticated("admin.countries")
	 * @param int $id
	 * @param string $lang
	 */
	public function actionPut(int $id, string $lang): void
	{
		$authenticator = $this->getAuthenticator();

		$user = $authenticator->getUser();

		if (!$user instanceof User) {
			throw new AuthenticationException("users.auth.error.user-not-found", 10001);
		}

		$command = $this->getCommandDTO();

		if (!$command instanceof TranslationPutDTO) {
			throw new InvalidArgumentException;
		}

		$country = $this->countryService->getCountry($id);

		if ($country === null) {
			throw new NotFoundException('countries.country.get.error.country-not-found', 10003);
		}

		$language = $this->languageService->getLanguage($lang);

		if ($language === null) {
			throw new NotFoundException('languages.language.get.not-found', 10002);
		}

		$this->countryService->translateCountry($country, $language, $command);

		$this->sendSuccessResponse(new TranslationResponseBuilder($country, $lang), IResponse::S200_OK);
	}

}
