<?php

declare(strict_types=1);

namespace Cleevio\Countries\Entities;

use Cleevio\Languages\Entities\Language;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\Translatable\Translatable;

/**
 * @ORM\Entity()
 * @ORM\Table(name="countries")
 * @Gedmo\TranslationEntity(class="CountryTranslation")
 */
class Country implements Translatable
{

	/**
	 * @ORM\Id
	 * @ORM\Column(type="integer")
	 * @ORM\GeneratedValue
	 * @var int
	 */
	private $id;

	/**
	 * @ORM\Column(name="code", type="string", length=2, nullable=false)
	 * @var string
	 */
	private $code;

	/**
	 * @Gedmo\Translatable
	 * @ORM\Column(name="name", type="string", nullable=false)
	 * @var string
	 */
	private $name;

	/**
	 * @ORM\Column(name="is_enabled", type="boolean", nullable=false, options={"default": 0})
	 * @var bool
	 */
	private $isEnabled;

	/**
	 * @ORM\Column(name="is_home", type="boolean", nullable=false, options={"default": 0})
	 * @var bool
	 */
	private $isHome;

	/**
	 * @ORM\OneToMany(targetEntity="CountryTranslation", mappedBy="object", cascade={"all"}, orphanRemoval=true)
	 * @var ArrayCollection
	 */
	private $translations;

	/**
	 * @ORM\OneToMany(targetEntity="Cleevio\Trips\Entities\Trip", mappedBy="country")
	 * @ORM\JoinColumn(name="country_id", unique=false)
	 * @var ArrayCollection
	 */
	private $trips;

	/**
	 * @ORM\OneToMany(targetEntity="Cleevio\Questions\Entities\Question", mappedBy="country")
	 * @ORM\JoinColumn(name="country_id", unique=false, nullable=false)
	 * @var ArrayCollection
	 */
	protected $questions;


	/**
	 * @param string $code
	 * @param string $name
	 */
	public function __construct(string $code, string $name)
	{
		$this->code = $code;
		$this->name = $name;
		$this->isEnabled = false;
		$this->isHome = false;
		$this->translations = new ArrayCollection;
		$this->trips = new ArrayCollection;
		$this->questions = new ArrayCollection;
	}

	/**
	 * @return int
	 */
	final public function getId(): int
	{
		return $this->id;
	}

	public function getCode(): string
	{
		return $this->code;
	}

	public function setCode(string $code): void
	{
		$this->code = $code;
	}

	/**
	 * @param string|null $lang
	 * @return string
	 */
	public function getName(?string $lang): string
	{
		return $this->getTranslation('name', $lang) ?? $this->name;
	}

	/**
	 * @param string $name
	 */
	public function setName(string $name): void
	{
		$this->name = $name;
	}

	/**
	 * @return bool
	 */
	public function isEnabled(): bool
	{
		return $this->isEnabled;
	}


	/**
	 * @param bool $isEnabled
	 */
	public function setIsEnabled(bool $isEnabled): void
	{
		$this->isEnabled = $isEnabled;
	}

	/**
	 * @return bool
	 */
	public function isHome(): bool
	{
		return $this->isHome;
	}

	/**
	 * @param bool $isHome
	 */
	public function setIsHome(bool $isHome): void
	{
		$this->isHome = $isHome;
	}

	/**
	 * @param string $name
	 * @param string|null $lang
	 * @return string|null
	 */
	private function getTranslation(string $name, ?string $lang): ?string
	{
		foreach ($this->getTranslations() as $translation) {
			if ($translation->getLocale() === $lang && $translation->getField() === $name) {
				return $translation->getContent();
			}
		}

		return null;
	}

	/**
	 * @return CountryTranslation[]
	 */
	public function getTranslations(): array
	{
		return $this->translations->getValues();
	}

	/**
	 * @param CountryTranslation $t
	 */
	public function addTranslation(CountryTranslation $t): void
	{
		foreach ($this->translations as $translation) {
			if ($translation->getLocale() === $t->getLocale() && $translation->getField() === $t->getField()) {
				$translation->setContent($t->getContent());
				$t->setObject($this);

				return;
			}
		}

		$this->translations[] = $t;
		$t->setObject($this);
	}

	/**
	 * @param string $field
	 * @param Language $language
	 */
	public function clearTranslation(string $field, Language $language): void
	{
		foreach ($this->translations as $translation) {
			if ($translation->getLocale() === $language->getCode() && $translation->getField() === $field) {
				$this->translations->removeElement($translation);

				return;
			}
		}
	}
}
