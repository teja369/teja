<?php

declare(strict_types=1);

namespace Cleevio\Countries;

/**
 * Interface ICountryTranslation
 * Getters for all translatable fields in countries
 * @package Cleevio\Countries
 */
interface ICountryTranslation
{

	function getName(): ?string;

}
