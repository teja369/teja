<?php

declare(strict_types=1);

namespace Cleevio\Countries;

use Cleevio\Countries\Entities\Country;
use Cleevio\Languages\Entities\Language;

interface ICountryService
{

	/**
	 * @param int $id
	 * @return Country|null
	 */
	public function getCountry(int $id): ?Country;

	/**
	 * @param bool $showDisabled
	 * @param string|null $search
	 * @param string|null $language
	 * @return array
	 */
	public function getCountries(bool $showDisabled, ?string $search = null, ?string $language = null): array;


	/**
	 * @param string $code
	 * @return Country|null
	 */
	public function findByCode(string $code): ?Country;


	/**
	 * @param int[] $ids
	 * @return Country[]
	 * @throws CountryNotFound
	 */
	public function findByIds(array $ids): array;


	/**
	 * @param string[] $codes
	 * @return Country[]
	 * @throws CountryNotFound
	 */
	public function findByCodes(array $codes): array;


	/**
	 * @param Country $country
	 */
	public function enableCountry(Country $country): void;


	/**
	 * @param Country $country
	 */
	public function disableCountry(Country $country): void;

	/**
	 * Translate question text parameters
	 * @param Country $country
	 * @param Language $language
	 * @param ICountryTranslation $translation
	 * @return Country
	 */
	public function translateCountry(Country $country, Language $language, ICountryTranslation $translation): Country;

}
