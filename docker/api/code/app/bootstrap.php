<?php

declare(strict_types=1);

use Dotenv\Dotenv;
use Tracy\Debugger;

require __DIR__ . '/helpers.php';
require __DIR__ . '/../vendor/autoload.php';

$configurator = new Nette\Configurator;

/**
 * Load environment variables, it MUST be set.
 * Take a look on .env.example, edit and save it as .env in application root.
 */
(new Dotenv(__DIR__ . '/..'))->load();

$configurator->setDebugMode(env('DEBUG_MODE', false));
$configurator->enableTracy(__DIR__ . '/../log');

if ((bool) env('DEBUG_HIDE_BAR', false)) {
	Debugger::$showBar = false;
}

$configurator->setTimeZone('Europe/Prague');
$configurator->setTempDirectory(__DIR__ . '/../temp');

$configurator->createRobotLoader()
	->addDirectory(__DIR__)
	->register();

$configurator->addConfig(__DIR__ . '/config/config.neon');
$configurator->addConfig(__DIR__ . '/config/config.local.neon');

$configurator->addParameters([
	'PROXY_ADDRESS' => getenv("PROXY_ADDRESS") === "" ? null : getenv("PROXY_ADDRESS"),
	'SMTP_HOST' => env("SMTP_HOST"),
	'SMTP_PORT' => (int) getenv("SMTP_PORT"),
	'SMTP_SECURE' => getenv("SMTP_SECURE") === "" ? null : getenv("SMTP_SECURE") ,
	'SMTP_USERNAME' => getenv("SMTP_USERNAME"),
	'SMTP_PASSWORD' => getenv("SMTP_PASSWORD"),
	'EMAIL_ADDRESS_SYSTEM' => getenv("EMAIL_ADDRESS_SYSTEM"),
	'EMAIL_ADDRESS_SUPPORT' => getenv("EMAIL_ADDRESS_SUPPORT"),
	'LOGGER_SENTRY_DSN' => getenv("LOGGER_SENTRY_DSN"),
	'LOGGER_SENTRY_ENABLED' => getenv("LOGGER_SENTRY_ENABLED") === 'true',
	'LOGGER_MAILER_ENABLED' => getenv("LOGGER_MAILER_ENABLED") === 'true',
	'LOGGER_MAILER_ADDRESS' => getenv("LOGGER_MAILER_ADDRESS"),
]);

$container = $configurator->createContainer();

define('APP_DIR', __DIR__);
define('CACHE_DIR', __DIR__ . '/../temp');

return $container;
