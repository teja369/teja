<?php

error_reporting(E_ALL ^ (E_NOTICE | E_WARNING));

require __DIR__ . '/../vendor/autoload.php';

$configurator = new Nette\Configurator;

/**
 * Load environment variables, it MUST be set.
 * Take a look on .env.example, edit and save it as .env in application root.
 */
$environmentLoader = (new Dotenv\Dotenv(__DIR__ . '/..'))->load();

if (getenv(Tester\Environment::RUNNER)) {
	$configurator->setDebugMode(false);
} elseif (PHP_SAPI === 'cli') {
	$configurator->setDebugMode(false);
} else {
	$configurator->setDebugMode(true);
	$configurator->enableDebugger();
}

$configurator->setTempDirectory(__DIR__ . '/../temp');

$configurator->createRobotLoader()
    ->addDirectory(__DIR__ . '/../app')
    ->addDirectory(__DIR__ . '/../tests')
    ->register();

$configurator->addParameters([
    'appDir' => __DIR__ . '/../app',
    'wwwDir' => __DIR__ . '/../www',
]);

$configurator->addConfig(__DIR__ . '/../app/config/config.neon');
$configurator->addConfig(__DIR__ . '/../app/config/config.test.neon');

$configurator->addParameters([
    'PROXY_ADDRESS' => null,
    'SMTP_HOST' => env("SMTP_HOST"),
    'SMTP_PORT' => (int) env("SMTP_PORT"),
    'SMTP_SECURE' => env("SMTP_SECURE") === "" ? null : getenv("SMTP_SECURE") ,
    'SMTP_USERNAME' => env("SMTP_USERNAME"),
    'SMTP_PASSWORD' => env("SMTP_PASSWORD"),
    'EMAIL_ADDRESS_SYSTEM' => env("EMAIL_ADDRESS_SYSTEM"),
    'EMAIL_ADDRESS_SUPPORT' => env("EMAIL_ADDRESS_SUPPORT"),
    'LOGGER_SENTRY_DSN' => getenv("LOGGER_SENTRY_DSN"),
    'LOGGER_SENTRY_ENABLED' => false,
    'LOGGER_MAILER_ENABLED' => false,
	'LOGGER_MAILER_ADDRESS' => getenv("LOGGER_MAILER_ADDRESS"),
]);

define('APP_DIR', __DIR__ . '/../app');
define('WWW_DIR', __DIR__ . '/../www');
define('TEST_DIR', __DIR__ . '/../tests');
define('CACHE_DIR', __DIR__ . '/../temp');
define('NETTE_TESTER', true);

return $configurator->createContainer();
