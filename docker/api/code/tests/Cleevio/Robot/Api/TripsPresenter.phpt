<?php
declare(strict_types=1);

namespace Test\Cleevio\Robot\Api;

$container = require __DIR__ . '/../../../bootstrap.php';

use Cleevio\Countries\Entities\Country;
use Cleevio\Countries\Repositories\ICountryRepository;
use Cleevio\Fields\Entities\Fields\TextField;
use Cleevio\Fields\Repositories\IFieldRepository;
use Cleevio\Hosts\Entities\Host;
use Cleevio\Hosts\Entities\HostParam;
use Cleevio\Hosts\Repositories\IHostRepository;
use Cleevio\Questions\Repositories\IQuestionRepository;
use Cleevio\Registrations\Repositories\IRegistrationRepository;
use Cleevio\RestApi\Auth\ITokenProvider;
use Cleevio\RestApi\Testing\RestTest;
use Cleevio\Trips\Entities\Trip;
use Cleevio\Trips\Repositories\ITripRepository;
use Cleevio\Users\Entities\User;
use Cleevio\Users\Repositories\IUserRepository;
use DateTime;
use Nette;
use Symfony\Component\Console\Application;
use Test\Cleevio\BaseFixture;
use Test\Cleevio\TestContext;
use Tester\Assert;

class TripsPresenter extends RestTest
{

    /**
     * @var ICountryRepository|object|null
     */
    private $countryRepository;

    /**
     * @var IUserRepository|object|null
     */
    private $userRepository;

    /**
     * @var ITokenProvider
     */
    private $tokenProvider;

    /**
     * @var ITripRepository|object|null
     */
    private $tripRepository;

    /**
     * @var IHostRepository|object|null
     */
    private $hostRepository;

    /**
     * @var IRegistrationRepository|object|null
     */
    private $registrationRepository;

    /**
     * @var IQuestionRepository|object|null
     */
    private $questionRepository;

    /**
     * @var IFieldRepository|object|null
     */
    private $fieldRepository;

    /**
     * @var Country
     */
    private $country;

    /**
     * @var User
     */
    private $user;

    /**
     * @var string
     */
    private $accessToken;

    /**
     * @var Trip
     */
    private $trip;

    /**
     * @var TextField
     */
    private $field;

    /**
     * @var Host
     */
    private $host1;

    /**
     * LoginPresenter constructor.
     * @param Nette\DI\Container $container
     */
    function __construct(Nette\DI\Container $container)
    {
        parent::__construct(new TestContext($container->getByType(Application::class)));

        $this->countryRepository = $container->getByType(ICountryRepository::class);
        $this->userRepository = $container->getByType(IUserRepository::class);
        $this->tokenProvider = $container->getByType(ITokenProvider::class);
        $this->tripRepository = $container->getByType(ITripRepository::class);
        $this->hostRepository = $container->getByType(IHostRepository::class);
        $this->registrationRepository = $container->getByType(IRegistrationRepository::class);
        $this->fieldRepository = $container->getByType(IFieldRepository::class);
        $this->questionRepository = $container->getByType(IQuestionRepository::class);
    }

    function testTripGet()
    {
        $response = $this->getRequest('api/v1/robot/trips/' . $this->trip->getId())
            ->withMethod('POST')
            ->withHeader('Authorization', 'Bearer ' . $this->accessToken)
            ->withBody([])
            ->execute(200);

        Assert::notNull($response);
        Assert::same($this->trip->getId(), $response->id);
        Assert::notNull(((array)$response)['fields_' . $this->field->getName(null)]);
    }

    function testTripGetKnownField()
    {
        $response = $this->getRequest('api/v1/robot/trips/' . $this->trip->getId())
            ->withMethod('POST')
            ->withHeader('Authorization', 'Bearer ' . $this->accessToken)
            ->withBody([
                ['field' => $this->field->getName(null)]
            ])
            ->execute(200);

        Assert::notNull($response);
        Assert::same($this->trip->getId(), $response->id);
        Assert::notNull(((array)$response)['fields_' . $this->field->getName(null)]);
    }

    function testTripGetUnknownField()
    {
        $response = $this->getRequest('api/v1/robot/trips/' . $this->trip->getId())
            ->withMethod('POST')
            ->withHeader('Authorization', 'Bearer ' . $this->accessToken)
            ->withBody([
                ['field' => uniqid()]
            ])
            ->execute(200);

        Assert::notNull($response);
        Assert::same($this->trip->getId(), $response->id);
        Assert::null(((array)$response)['fields_' . $this->field->getName(null)]);
    }

    // Todo: csv output
    /*function testTripList()
    {
        $response = $this->getRequest('api/v1/robot/trips' )
            ->withMethod('GET')
            ->withHeader('Authorization', 'Bearer ' . $this->accessToken)
            ->execute(200);

        Assert::notNull($response);
    }*/


    function testTripPut()
    {
        $response = $this->getRequest('api/v1/robot/trips')
            ->withMethod('PUT')
            ->withHeader('Authorization', 'Bearer ' . $this->accessToken)
            ->withBody([
                [
                    'id' => $this->trip->getId(),
                    'state' => Trip::TRIP_STATE_PROCESSED,
                    'comment' => 'test'
                ]
            ])
            ->execute(200);

        Assert::notNull($response);
    }

    function testTripPutNoComment()
    {
        $response = $this->getRequest('api/v1/robot/trips')
            ->withMethod('PUT')
            ->withHeader('Authorization', 'Bearer ' . $this->accessToken)
            ->withBody([
                [
                    'id' => $this->trip->getId(),
                    'state' => Trip::TRIP_STATE_PROCESSED
                ]
            ])
            ->execute(200);

        Assert::notNull($response);
    }

    function testTripPutInvalidState()
    {
        $this->getRequest('api/v1/robot/trips')
            ->withMethod('PUT')
            ->withHeader('Authorization', 'Bearer ' . $this->accessToken)
            ->withBody([
                [
                    'id' => $this->trip->getId(),
                    'state' => uniqid()
                ]
            ])
            ->execute(400);
    }


    protected function setUp()
    {
        parent::setUp();
        $this->user = $this->userRepository->findByUsername(BaseFixture::USER_NAME);
        $this->accessToken = $this->tokenProvider->getToken($this->user);

        $this->country = new Country('cz', 'Czech Republic', "icon");
        $this->country->setIsEnabled(true);
        $this->countryRepository->persist($this->country);

        $this->field = new TextField(uniqid());
        $this->fieldRepository->persist($this->field);

        $this->host1 = new Host(uniqid(), $this->country, Host::HOST_TYPE_CLIENT);
        $this->host1->setParams([new HostParam($this->host1, $this->field, uniqid())]);
        $this->hostRepository->persist($this->host1);

        $this->trip = new Trip(
            $this->country,
            $this->user,
            (new DateTime())->modify('+2 days'),
            (new DateTime())->modify('+3 days'),
            Trip::TRIP_STATE_SUBMITTED
        );
        $this->trip->setHost($this->host1);
        $this->tripRepository->persist($this->trip);

    }
}

$test = new TripsPresenter($container);
$test->run();
