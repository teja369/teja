<?php
declare(strict_types=1);

namespace Test\Cleevio\Registrations\Api;

$container = require __DIR__ . '/../../../bootstrap.php';

use Cleevio\Registrations\Entities\Registration;
use Cleevio\Registrations\Repositories\IRegistrationRepository;
use Cleevio\RestApi\Auth\ITokenProvider;
use Cleevio\RestApi\Testing\RestTest;
use Cleevio\Users\Entities\User;
use Cleevio\Users\Repositories\IUserRepository;
use Nette;
use Symfony\Component\Console\Application;
use Test\Cleevio\BaseFixture;
use Test\Cleevio\TestContext;
use Tester\Assert;

class RegistrationPresenter extends RestTest
{
    /**
     * @var User
     */
    private $user;

    /**
     * @var string
     */
    private $accessToken;

    /**
     * @var IUserRepository|object|null
     */
    private $userRepository;

    /**
     * @var ITokenProvider|object|null
     */
    private $tokenProvider;

    /**
     * @var IRegistrationRepository|object|null
     */
    private $registrationRepository;

    /**
     * CountryPresenter constructor.
     * @param Nette\DI\Container $container
     */
    function __construct(Nette\DI\Container $container)
    {
        parent::__construct(new TestContext($container->getByType(Application::class)));

        $this->userRepository = $container->getByType(IUserRepository::class);
        $this->tokenProvider = $container->getByType(ITokenProvider::class);
        $this->registrationRepository = $container->getByType(IRegistrationRepository::class);
    }

    function testRegistrationsGet()
    {
        $response = $this->getRequest('api/v1/registrations')
            ->withMethod('GET')
            ->withHeader('Authorization', 'Bearer ' . $this->accessToken)
            ->execute(200);

        Assert::notNull($response);
        Assert::count(1, $response->items);
    }

    function testRegistrationsUnauthorized()
    {
        $this->getRequest('api/v1/registrations')
            ->withMethod('GET')
            ->execute(401);
    }

    protected function setUp()
    {
        parent::setUp();

        $this->user = $this->userRepository->findByUsername(BaseFixture::USER_NAME);
        $this->accessToken = $this->tokenProvider->getToken($this->user);

        $registration = new Registration(uniqid());
        $this->registrationRepository->persist($registration);
    }


}

$test = new RegistrationPresenter($container);
$test->run();
