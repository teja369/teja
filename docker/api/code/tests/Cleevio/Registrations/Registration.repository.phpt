<?php
declare(strict_types=1);

namespace Test\Cleevio\Registrations;

$container = require __DIR__ . '/../../bootstrap.php';

use Cleevio\Registrations\Entities\Registration;
use Cleevio\Registrations\Repositories\IRegistrationRepository;
use Cleevio\Repository\RepositoryTest;
use Nette;
use Symfony\Component\Console\Application;
use Test\Cleevio\TestContext;

class RegistrationRepositoryTest extends RepositoryTest
{

    function __construct(Nette\DI\Container $container)
    {
        parent::__construct($container, new TestContext($container->getByType(Application::class)));
    }

    protected function getType(): string
    {
        return IRegistrationRepository::class;
    }


    /**
     * Create and return new entity
     * @return Registration
     */
    protected function getEntity(): Registration
    {
        return new Registration(uniqid());
    }
}

$test = new RegistrationRepositoryTest($container);
$test->run();
