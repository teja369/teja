<?php

declare(strict_types=1);

namespace Test\Cleevio;

use Cleevio\Identity\Identities\ApiIdentity;
use Cleevio\Identity\Identities\PasswordIdentity;
use Cleevio\Users\Entities\Resource;
use Cleevio\Users\Entities\Role;
use Cleevio\Users\Entities\User;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class BaseFixture implements FixtureInterface
{

    private const PASSWORD = "test";

    public const USER_NAME = "test";

    private const ROLES = [
        'traveller',
        'admin',
        'admin-external'
    ];

    private const RESOURCES = [
        'traveller',
        'admin',
        'admin.hosts',
        'admin.questions',
        'admin.trips',
        'admin.countries',
        'admin.localizations'
    ];

    /**
     * Load data fixtures with the passed EntityManager
     * @param ObjectManager $manager
     * @throws \Exception
     */
    public function load(ObjectManager $manager)
    {
        $user = new User(self::USER_NAME);
        $user->setEmail(sprintf("%s@test.com", uniqid()));
        $manager->persist($user);
        $manager->flush();

        $identity = new PasswordIdentity($user, self::PASSWORD);
        $manager->persist($identity);
        $manager->flush();

        $apiIdentity = new ApiIdentity($user);
        $manager->persist($apiIdentity);
        $manager->flush();

        foreach (self::ROLES as $role) {
            $r = new Role($role);
            $manager->persist($r);
            $manager->flush();
        }

        foreach (self::RESOURCES as $resource) {
            $r = new Resource($resource);
            $manager->persist($r);
            $manager->flush();
        }

    }
}
