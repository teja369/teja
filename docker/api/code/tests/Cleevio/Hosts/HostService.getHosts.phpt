<?php
declare(strict_types=1);

namespace Test\Cleevio\Hosts;

$container = require __DIR__ . '/../../bootstrap.php';

use Cleevio\Countries\Entities\Country;
use Cleevio\Countries\Repositories\ICountryRepository;
use Cleevio\Hosts\Entities\Host;
use Cleevio\Hosts\IHostsService;
use Cleevio\Hosts\Repositories\IHostRepository;
use Nette;
use Symfony\Component\Console\Application;
use Test\Cleevio\BaseTest;
use Test\Cleevio\TestContext;
use Tester\Assert;

class HostServiceGetHostsTest extends BaseTest
{

    /**
     * @var IHostsService|object|null
     */
    private $hostService;

    /**
     * @var ICountryRepository
     */
    private $countryRepository;

    /**
     * @var IHostRepository|object|null
     */
    private $hostRepository;
    /**
     * @var Country
     */
    private $countryCz;

    /**
     * @var Host
     */
    private $host;

    function __construct(Nette\DI\Container $container)
    {
        parent::__construct(new TestContext($container->getByType(Application::class)));

        $this->hostService = $container->getByType(IHostsService::class);
        $this->hostRepository = $container->getByType(IHostRepository::class);
        $this->countryRepository = $container->getByType(ICountryRepository::class);
    }

    function testGetHosts()
    {
        $hosts = $this->hostService->getHosts($this->countryCz, Host::HOST_TYPE_CLIENT, null);
        Assert::count(1, $hosts);
        Assert::same($this->host->getId(), $hosts[0]->getId());
    }

    protected function setUp()
    {
        parent::setUp();

        $this->countryCz = new Country('CZ', uniqid(), "icon");
        $this->countryRepository->persist($this->countryCz);

        $this->host = new Host(uniqid(), $this->countryCz, Host::HOST_TYPE_CLIENT);
        $this->host->setCountry($this->countryCz);
        $this->hostRepository->persist($this->host);
    }
}

$test = new HostServiceGetHostsTest($container);
$test->run();
