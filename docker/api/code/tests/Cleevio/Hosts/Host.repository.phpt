<?php
declare(strict_types=1);

namespace Test\Cleevio\Hosts;

$container = require __DIR__ . '/../../bootstrap.php';

use Cleevio\Countries\Entities\Country;
use Cleevio\Countries\Repositories\ICountryRepository;
use Cleevio\Hosts\Entities\Host;
use Cleevio\Hosts\Repositories\IHostRepository;
use Cleevio\Repository\RepositoryTest;
use Nette;
use Symfony\Component\Console\Application;
use Test\Cleevio\TestContext;
use Tester\Assert;

class HostRepositoryTest extends RepositoryTest
{

    /**
     * @var ICountryRepository|object|null
     */
    private $countryRepository;

    function __construct(Nette\DI\Container $container)
    {
        parent::__construct($container, new TestContext($container->getByType(Application::class)));

        $this->countryRepository = $container->getByType(ICountryRepository::class);
    }

    protected function getType(): string
    {
        return IHostRepository::class;
    }

    public function testFindByIds()
    {
        $entity1 = $this->getEntity();
        $entity2 = $this->getEntity();

        Assert::notNull($entity1);
        Assert::notNull($entity2);

        $this->repository->persist($entity1);
        $this->repository->persist($entity2);

        Assert::count(2, $this->repository->findByIds([$entity1->getId(), $entity2->getId()]));
    }

    /**
     * Create and return new entity
     * @return Host
     */
    protected function getEntity(): Host
    {
        $country = new Country('CZ', uniqid(), "icon");
        $this->countryRepository->persist($country);

        return new Host(uniqid(), $country, Host::HOST_TYPE_CLIENT);
    }
}

$test = new HostRepositoryTest($container);
$test->run();
