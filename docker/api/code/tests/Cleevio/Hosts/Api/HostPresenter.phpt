<?php
declare(strict_types=1);

namespace Test\Cleevio\Hosts\Api;

$container = require __DIR__ . '/../../../bootstrap.php';

use Cleevio\Countries\Entities\Country;
use Cleevio\Countries\Repositories\ICountryRepository;
use Cleevio\Fields\Entities\Field;
use Cleevio\Fields\Entities\Fields\TextField;
use Cleevio\Fields\Repositories\IFieldRepository;
use Cleevio\Hosts\Entities\Host;
use Cleevio\Hosts\Repositories\IHostRepository;
use Cleevio\RestApi\Auth\ITokenProvider;
use Cleevio\RestApi\Testing\RestTest;
use Cleevio\Trips\Entities\Trip;
use Cleevio\Trips\Repositories\ITripRepository;
use Cleevio\Users\Entities\User;
use Cleevio\Users\Repositories\IUserRepository;
use DateTime;
use Nette;
use Symfony\Component\Console\Application;
use Test\Cleevio\BaseFixture;
use Test\Cleevio\TestContext;
use Tester\Assert;

class HostPresenter extends RestTest
{

    /**
     * @var ITripRepository|null
     */
    private $tripRepository;

    /**
     * @var ICountryRepository|null
     */
    private $countryRepository;

    /**
     * @var IUserRepository|null
     */
    private $userRepository;

    /**
     * @var ITokenProvider|null
     */
    private $tokenProvider;

    /**
     * @var IHostRepository|object|null
     */
    private $hostRepository;

    /**
     * @var IFieldRepository|object|null
     */
    private $fieldRepository;

    /**
     * @var Trip to us
     */
    private $tripUs;

    /**
     * @var Trip
     */
    private $tripCz;

    /**
     * @var User
     */
    private $user;

    /**
     * @var string
     */
    private $accessToken;

    /**
     * @var Country
     */
    private $countryUs;

    /**
     * @var Host
     */
    private $host;


    /**
     * CountryPresenter constructor.
     * @param Nette\DI\Container $container
     */
    function __construct(Nette\DI\Container $container)
    {
        parent::__construct(new TestContext($container->getByType(Application::class)));

        $this->countryRepository = $container->getByType(ICountryRepository::class);
        $this->tripRepository = $container->getByType(ITripRepository::class);
        $this->hostRepository = $container->getByType(IHostRepository::class);
        $this->userRepository = $container->getByType(IUserRepository::class);
        $this->tokenProvider = $container->getByType(ITokenProvider::class);
        $this->fieldRepository = $container->getByType(IFieldRepository::class);
    }


    function testHostsList()
    {
        $response = $this->getRequest('api/v1/hosts?trip=' . $this->tripUs->getId())
            ->withMethod('GET')
            ->withHeader('Authorization', 'Bearer ' . $this->accessToken)
            ->execute(200);

        Assert::notNull($response->items);
        Assert::count(2, $response->items);
        Assert::same("Default", $response->items[0]->name);
        Assert::same("Non-Client", $response->items[1]->name);
    }


    function testHostListSearch()
    {
        $response = $this->getRequest('api/v1/hosts?trip=' . $this->tripUs->getId() . '&search=Default')
            ->withMethod('GET')
            ->withHeader('Authorization', 'Bearer ' . $this->accessToken)
            ->execute(200);

        Assert::notNull($response->items);
        Assert::count(1, $response->items);
        Assert::same("Default", $response->items[0]->name);
    }


    function testHostsGet()
    {
        $response = $this->getRequest('api/v1/hosts/' . $this->host->getId())
            ->withMethod('GET')
            ->withHeader('Authorization', 'Bearer ' . $this->accessToken)
            ->execute(200);

        Assert::notNull($response);
    }


    function testHostsListType()
    {
        $response = $this->getRequest('api/v1/hosts?trip=' . $this->tripUs->getId() . "&type=" . Host::HOST_TYPE_CLIENT)
            ->withMethod('GET')
            ->withHeader('Authorization', 'Bearer ' . $this->accessToken)
            ->execute(200);

        Assert::notNull($response->items);
        Assert::count(1, $response->items);
        Assert::same("Default", $response->items[0]->name);
    }


    function testHostsListEmptyForCountry()
    {
        $response = $this->getRequest('api/v1/hosts?trip=' . $this->tripCz->getId())
            ->withMethod('GET')
            ->withHeader('Authorization', 'Bearer ' . $this->accessToken)
            ->execute(200);

        Assert::notNull($response->items);
        Assert::count(0, $response->items);
    }


    function testHostsListWithoutCountry()
    {
        $this->getRequest('api/v1/hosts')
            ->withMethod('GET')
            ->withHeader('Authorization', 'Bearer ' . $this->accessToken)
            ->execute(200);
    }


    function testHostsListNonExistingTrip()
    {
        $this->getRequest('api/v1/hosts?trip=' . 99)
            ->withMethod('GET')
            ->withHeader('Authorization', 'Bearer ' . $this->accessToken)
            ->execute(404);
    }


    function testHostsPost()
    {
        $name = uniqid();
        $value = uniqid();
        $field = new TextField(uniqid());
        $this->fieldRepository->persist($field);

        $response = $this->getRequest('api/v1/hosts')
            ->withMethod('POST')
            ->withBody([
                'name' => $name,
                'country' => $this->countryUs->getCode(),
                'params' => [
                    [
                        "id" => $field->getId(),
                        "value" => $value,
                    ],
                ],
            ])
            ->withHeader('Authorization', 'Bearer ' . $this->accessToken)
            ->execute(200);

        Assert::equal($name, $response->name);
        Assert::equal($field->getId(), $response->params[0]->field->id);
        Assert::equal($value, $response->params[0]->value);
    }


    function testHostsPostNoParams()
    {
        $name = uniqid();

        $response = $this->getRequest('api/v1/hosts')
            ->withMethod('POST')
            ->withBody([
                'name' => $name,
                'country' => $this->countryUs->getCode(),
                'params' => [],
            ])
            ->withHeader('Authorization', 'Bearer ' . $this->accessToken)
            ->execute(200);

        Assert::equal($name, $response->name);
        Assert::count(0, $response->params);
    }


    function testHostsPostUnauthorized()
    {
        $this->getRequest('api/v1/hosts')
            ->withMethod('POST')
            ->withBody([
                'name' => uniqid(),
                'country' => $this->countryUs->getCode(),
                'params' => [

                ],
            ])
            ->execute(401);
    }


    function testHostsDelete()
    {
        $this->getRequest('api/v1/hosts/' . $this->host->getId())
            ->withMethod('DELETE')
            ->withHeader('Authorization', 'Bearer ' . $this->accessToken)
            ->execute(200);

        $this->getRequest('api/v1/hosts/' . $this->host->getId())
            ->withMethod('DELETE')
            ->withHeader('Authorization', 'Bearer ' . $this->accessToken)
            ->execute(404);
    }


    protected function setUp()
    {
        parent::setUp();

        $this->user = $this->userRepository->findByUsername(BaseFixture::USER_NAME);
        $this->accessToken = $this->tokenProvider->getToken($this->user);

        $this->countryUs = new Country('US', 'United states', "icon");
        $this->countryRepository->persist($this->countryUs);

        $countryCz = new Country('CZ', 'Czechia', "icon");
        $this->countryRepository->persist($countryCz);

        $this->tripUs = new Trip($this->countryUs, $this->user, new DateTime(), new DateTime());
        $this->tripRepository->persist($this->tripUs);

        $this->tripCz = new Trip($countryCz, $this->user, new DateTime(), new DateTime());
        $this->tripRepository->persist($this->tripCz);

        $this->host = new Host("Default", $this->countryUs, Host::HOST_TYPE_CLIENT);
        $this->hostRepository->persist($this->host);

        $host = new Host("Non-Client", $this->countryUs, Host::HOST_TYPE_NON_CLIENT);
        $this->hostRepository->persist($host);
    }

}

$test = new HostPresenter($container);
$test->run();
