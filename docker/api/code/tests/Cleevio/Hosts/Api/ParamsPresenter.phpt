<?php
declare(strict_types=1);

namespace Test\Cleevio\Hosts\Api;

$container = require __DIR__ . '/../../../bootstrap.php';

use Cleevio\Countries\Entities\Country;
use Cleevio\Countries\Repositories\ICountryRepository;
use Cleevio\Fields\Entities\Field;
use Cleevio\Fields\Entities\Fields\TextField;
use Cleevio\Fields\Repositories\IFieldRepository;
use Cleevio\Hosts\Entities\Host;
use Cleevio\Hosts\Entities\HostParam;
use Cleevio\Hosts\Repositories\IHostParamRepository;
use Cleevio\Hosts\Repositories\IHostRepository;
use Cleevio\RestApi\Auth\ITokenProvider;
use Cleevio\RestApi\Testing\RestTest;
use Cleevio\Trips\Entities\Trip;
use Cleevio\Trips\Repositories\ITripRepository;
use Cleevio\Users\Entities\User;
use Cleevio\Users\Repositories\IUserRepository;
use DateTime;
use Nette;
use Symfony\Component\Console\Application;
use Test\Cleevio\BaseFixture;
use Test\Cleevio\TestContext;
use Tester\Assert;

class ParamsPresenter extends RestTest
{
    /**
     * @var ICountryRepository|null
     */
    private $countryRepository;

    /**
     * @var IUserRepository|null
     */
    private $userRepository;

    /**
     * @var ITokenProvider|null
     */
    private $tokenProvider;

    /**
     * @var IHostRepository|object|null
     */
    private $hostRepository;

    /**
     * @var IFieldRepository|object|null
     */
    private $fieldRepository;

    /**
     * @var IHostParamRepository|object|null
     */
    private $hostParamRepository;

    /**
     * @var ITripRepository|object|null
     */
    private $tripRepository;

    /**
     * @var User
     */
    private $user;

    /**
     * @var string
     */
    private $accessToken;

    /**
     * @var Country
     */
    private $countryUs;

    /**
     * @var Host
     */
    private $host;

    /**
     * @var Field
     */
    private $field;

    /**
     * @var HostParam
     */
    private $hostParam;

    /**
     * CountryPresenter constructor.
     * @param Nette\DI\Container $container
     */
    function __construct(Nette\DI\Container $container)
    {
        parent::__construct(new TestContext($container->getByType(Application::class)));

        $this->countryRepository = $container->getByType(ICountryRepository::class);
        $this->hostRepository = $container->getByType(IHostRepository::class);
        $this->userRepository = $container->getByType(IUserRepository::class);
        $this->tokenProvider = $container->getByType(ITokenProvider::class);
        $this->fieldRepository = $container->getByType(IFieldRepository::class);
        $this->hostParamRepository = $container->getByType(IHostParamRepository::class);
        $this->tripRepository = $container->getByType(ITripRepository::class);
    }

    function testParamsList()
    {
        $response = $this->getRequest('api/v1/hosts/' . $this->host->getId() . '/param')
            ->withMethod('GET')
            ->withHeader('Authorization', 'Bearer ' . $this->accessToken)
            ->execute(200);

        Assert::notNull($response);
        Assert::count(1, $response);
    }

    function testParamsPut()
    {
        $value = uniqid();

        $response = $this->getRequest('api/v1/hosts/' . $this->host->getId() . '/param/' . $this->field->getId())
            ->withMethod('PUT')
            ->withBody([
                'value' => $value
            ])
            ->withHeader('Authorization', 'Bearer ' . $this->accessToken)
            ->execute(200);

        Assert::notNull($response);
        Assert::same($this->host->getId(), $response->id);
    }

    function testParamsPutSoftDeleteHost()
    {
        $trip = new Trip($this->countryUs, $this->user, new DateTime(), new DateTime());
        $trip->setHost($this->host);
        $this->tripRepository->persist($trip);

        $value = uniqid();

        $response = $this->getRequest('api/v1/hosts/' . $this->host->getId() . '/param/' . $this->field->getId())
            ->withMethod('PUT')
            ->withBody([
                'value' => $value
            ])
            ->withHeader('Authorization', 'Bearer ' . $this->accessToken)
            ->execute(200);

        Assert::notNull($response);
        Assert::notSame($this->host->getId(), $response->id);
    }

    function testParamsDelete()
    {
        $response = $this->getRequest('api/v1/hosts/' . $this->host->getId() . '/param/' . $this->field->getId())
            ->withMethod('DELETE')
            ->withHeader('Authorization', 'Bearer ' . $this->accessToken)
            ->execute(200);

        Assert::notNull($response);
        Assert::same($this->host->getId(), $response->id);
    }

    function testParamsDeleteSoftDeleteHost()
    {
        $trip = new Trip($this->countryUs, $this->user, new DateTime(), new DateTime());
        $trip->setHost($this->host);
        $this->tripRepository->persist($trip);

        $response = $this->getRequest('api/v1/hosts/' . $this->host->getId() . '/param/' . $this->field->getId())
            ->withMethod('DELETE')
            ->withHeader('Authorization', 'Bearer ' . $this->accessToken)
            ->execute(200);

        Assert::notNull($response);
        Assert::notSame($this->host->getId(), $response->id);
    }

    protected function setUp()
    {
        parent::setUp();

        $this->user = $this->userRepository->findByUsername(BaseFixture::USER_NAME);
        $this->accessToken = $this->tokenProvider->getToken($this->user);

        $this->countryUs = new Country('US', 'United states', "icon");
        $this->countryRepository->persist($this->countryUs);

        $this->host = new Host("Non-Client", $this->countryUs, Host::HOST_TYPE_NON_CLIENT);
        $this->hostRepository->persist($this->host);

        $this->field = new TextField(uniqid());
        $this->fieldRepository->persist($this->field);

        $this->hostParam = new HostParam($this->host, $this->field, uniqid());
        $this->hostParamRepository->persist($this->hostParam);
    }


}

$test = new ParamsPresenter($container);
$test->run();
