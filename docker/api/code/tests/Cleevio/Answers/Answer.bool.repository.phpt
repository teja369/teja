<?php
declare(strict_types=1);

namespace Test\Cleevio\Answers;

$container = require __DIR__ . '/../../bootstrap.php';

use Cleevio\Answers\Entities\Answer;
use Cleevio\Answers\Entities\Answers\BoolAnswer;
use Cleevio\Answers\Entities\Answers\NumberAnswer;
use Cleevio\Answers\Entities\Answers\TextAnswer;
use Cleevio\Answers\Repositories\IAnswerRepository;
use Cleevio\Countries\Entities\Country;
use Cleevio\Countries\Repositories\ICountryRepository;
use Cleevio\Questions\Entities\QuestionGroup;
use Cleevio\Questions\Entities\Questions\TextQuestion;
use Cleevio\Questions\Repositories\IGroupRepository;
use Cleevio\Questions\Repositories\IQuestionRepository;
use Cleevio\Questions\Repositories\QuestionRepository;
use Cleevio\Repository\RepositoryTest;
use Cleevio\Trips\Entities\Trip;
use Cleevio\Trips\Repositories\ITripRepository;
use Cleevio\Users\Entities\User;
use Cleevio\Users\Repositories\IUserRepository;
use DateTime;
use Nette;
use Symfony\Component\Console\Application;
use Test\Cleevio\TestContext;

class AnswerBoolRepositoryTest extends RepositoryTest
{

    /**
     * @var ICountryRepository|object|null
     */
    private $countryRepository;

    /**
     * @var IUserRepository|object|null
     */
    private $userRepository;

    /**
     * @var ITripRepository|object|null
     */
    private $tripRepository;

    /**
     * @var IQuestionRepository|object|null
     */
    private $questionRepository;

    /**
     * @var IQuestionRepository|object|null
     */
    private $groupRepository;

    function __construct(Nette\DI\Container $container)
    {
        parent::__construct($container, new TestContext($container->getByType(Application::class)));

        $this->countryRepository = $container->getByType(ICountryRepository::class);
        $this->userRepository = $container->getByType(IUserRepository::class);
        $this->tripRepository = $container->getByType(ITripRepository::class);
        $this->questionRepository = $container->getByType(IQuestionRepository::class);
        $this->groupRepository = $container->getByType(IGroupRepository::class);
    }

    protected function getType(): string
    {
        return IAnswerRepository::class;
    }

    /**
     * Create and return new entity
     * @return Answer
     * @throws \Exception
     */
    protected function getEntity(): Answer
    {
        $country = new Country("SK", uniqid(), "icon");
        $this->countryRepository->persist($country);

        $user = new User(uniqid());
        $this->userRepository->persist($user);

        $trip = new Trip($country, $user, new DateTime(), new DateTime());
        $this->tripRepository->persist($trip);

        $group = new QuestionGroup(uniqid(), QuestionGroup::QUESTION_GROUP_TYPE_DETAIL);
        $this->groupRepository->persist($group);

        $question = new TextQuestion(uniqid(), $group);
        $this->questionRepository->persist($question);

        return new BoolAnswer($trip, $question, true);
    }
}

$test = new AnswerBoolRepositoryTest($container);
$test->run();
