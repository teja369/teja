<?php

declare(strict_types=1);

namespace Test\Cleevio;

require __DIR__ . '/../bootstrap.php';

use Cleevio\Testing\ITestContext;
use Exception;
use Tester\TestCase;

/**
 * Class BaseTest
 * @package Test\Cleevio
 */
class BaseTest extends TestCase
{

    /**
     * @var ITestContext
     */
    private $context;

    /**
     * @param ITestContext $context
     */
    function __construct(ITestContext $context)
    {
        $this->context = $context;
    }

    /**
     * @throws Exception
     */
    protected function setUp()
    {
        parent::setUp();

        $this->context->setUp();
    }
}