<?php
declare(strict_types=1);

namespace Test\Cleevio\Fields;

$container = require __DIR__ . '/../../bootstrap.php';

use Cleevio\Fields\Entities\Field;
use Cleevio\Fields\Entities\Fields\TextField;
use Cleevio\Fields\Repositories\IFieldRepository;
use Cleevio\Repository\RepositoryTest;
use Nette;
use Symfony\Component\Console\Application;
use Test\Cleevio\TestContext;
use Tester\Assert;

class FieldRepositoryTest extends RepositoryTest
{

    /**
     * @var IFieldRepository
     */
    private $fieldRepository;


    function __construct(Nette\DI\Container $container)
    {
        parent::__construct($container, new TestContext($container->getByType(Application::class)));
        $this->fieldRepository = $container->getByType(IFieldRepository::class);
    }

    protected function getType(): string
    {
        return IFieldRepository::class;
    }


    public function testFindByIds()
    {
        $entity1 = $this->getEntity();
        $entity2 = $this->getEntity();

        Assert::notNull($entity1);
        Assert::notNull($entity2);

        $this->repository->persist($entity1);
        $this->repository->persist($entity2);

        Assert::count(2, $this->repository->findByIds([$entity1->getId(), $entity2->getId()]));
    }

    /**
     * @return Field
     */
    protected function getEntity(): Field
    {
        return new TextField(uniqid());
    }
}

$test = new FieldRepositoryTest($container);
$test->run();
