<?php
declare(strict_types=1);

namespace Test\Cleevio\Fields;

$container = require __DIR__ . '/../../bootstrap.php';

use Cleevio\Fields\Entities\Address;
use Cleevio\Fields\Entities\Fields\TextField;
use Cleevio\Fields\Repositories\IAddressRepository;
use Cleevio\Fields\Repositories\IFieldRepository;
use Cleevio\Repository\RepositoryTest;
use Nette;
use Symfony\Component\Console\Application;
use Test\Cleevio\TestContext;
use Tester\Assert;

class AddressRepositoryTest extends RepositoryTest
{

    /**
     * @var IFieldRepository
     */
    private $fieldRepository;


    function __construct(Nette\DI\Container $container)
    {
        parent::__construct($container, new TestContext($container->getByType(Application::class)));
        $this->fieldRepository = $container->getByType(IFieldRepository::class);
    }

    protected function getType(): string
    {
        return IAddressRepository::class;
    }

    /**
     * @return Address
     */
    protected function getEntity(): Address
    {
        $field = new TextField(uniqid());
        $this->fieldRepository->persist($field);

        return new Address($field, 0);
    }
}

$test = new AddressRepositoryTest($container);
$test->run();
