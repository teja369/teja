<?php
declare(strict_types=1);

namespace Test\Cleevio\Fields\Api;

$container = require __DIR__ . '/../../../bootstrap.php';

use Cleevio\Countries\Entities\Country;
use Cleevio\Countries\Repositories\ICountryRepository;
use Cleevio\Fields\Entities\Field;
use Cleevio\Fields\Entities\Fields\TextField;
use Cleevio\Fields\Repositories\IFieldRepository;
use Cleevio\RestApi\Auth\ITokenProvider;
use Cleevio\RestApi\Testing\RestTest;
use Cleevio\Trips\Repositories\ITripRepository;
use Cleevio\Users\Entities\User;
use Cleevio\Users\Repositories\IUserRepository;
use Nette;
use Symfony\Component\Console\Application;
use Test\Cleevio\BaseFixture;
use Test\Cleevio\TestContext;
use Tester\Assert;

class FieldPresenter extends RestTest
{
    /**
     * @var ITripRepository|null
     */
    private $tripRepository;

    /**
     * @var ICountryRepository|null
     */
    private $countryRepository;

    /**
     * @var IUserRepository|null
     */
    private $userRepository;

    /**
     * @var ITokenProvider|null
     */
    private $tokenProvider;

    /**
     * @var IFieldRepository|object|null
     */
    private $fieldRepository;

    /**
     * @var User
     */
    private $user;

    /**
     * @var string
     */
    private $accessToken;

    /**
     * @var Country
     */
    private $countryUs;

    /**
     * @var Field
     */
    private $field;

    /**
     * @var Field
     */
    private $field2;

    /**
     * CountryPresenter constructor.
     * @param Nette\DI\Container $container
     */
    function __construct(Nette\DI\Container $container)
    {
        parent::__construct(new TestContext($container->getByType(Application::class)));

        $this->countryRepository = $container->getByType(ICountryRepository::class);
        $this->tripRepository = $container->getByType(ITripRepository::class);
        $this->userRepository = $container->getByType(IUserRepository::class);
        $this->tokenProvider = $container->getByType(ITokenProvider::class);
        $this->fieldRepository = $container->getByType(IFieldRepository::class);
    }

    function testFieldGet()
    {
        $response = $this->getRequest('api/v1/fields/' . $this->field->getId())
            ->withMethod('GET')
            ->withHeader('Authorization', 'Bearer ' . $this->accessToken)
            ->execute(200);

        Assert::notNull($response);
        Assert::same($this->field->getId(), $response->id);
    }

    function testFieldsGet()
    {
        $response = $this->getRequest('api/v1/fields')
            ->withMethod('GET')
            ->withHeader('Authorization', 'Bearer ' . $this->accessToken)
            ->execute(200);

        Assert::notNull($response);
        Assert::count(2, $response);
    }

    function testFieldsPost()
    {
        $name = uniqid();

        $response = $this->getRequest('api/v1/fields')
            ->withMethod('POST')
            ->withBody([
                'name' => $name,
                'type' => Field::FIELD_TYPE_TEXT,
                'placeholder' => null,
                'required' => true
            ])
            ->withHeader('Authorization', 'Bearer ' . $this->accessToken)
            ->execute(200);

        Assert::notNull($response);
        Assert::notNull($name, $response->name);
    }

    function testFieldsPostUnique()
    {
        $this->getRequest('api/v1/fields')
            ->withMethod('POST')
            ->withBody([
                'name' => $this->field->getName(null),
                'type' => Field::FIELD_TYPE_TEXT,
                'placeholder' => null,
                'required' => true
            ])
            ->withHeader('Authorization', 'Bearer ' . $this->accessToken)
            ->execute(400);
    }

    function testFieldsPut()
    {
        $name = uniqid();

        $response = $this->getRequest('api/v1/fields/' . $this->field->getId())
            ->withMethod('PUT')
            ->withBody([
                'name' => $name,
                'type' => Field::FIELD_TYPE_TEXT,
                'placeholder' => null,
                'required' => true
            ])
            ->withHeader('Authorization', 'Bearer ' . $this->accessToken)
            ->execute(200);

        Assert::notNull($response);
        Assert::notNull($name, $response->name);
    }

    function testFieldsPutSameUnique()
    {
        $this->getRequest('api/v1/fields/' . $this->field->getId())
            ->withMethod('PUT')
            ->withBody([
                'name' => $this->field->getName(null),
                'type' => Field::FIELD_TYPE_TEXT,
                'placeholder' => null,
                'required' => true
            ])
            ->withHeader('Authorization', 'Bearer ' . $this->accessToken)
            ->execute(200);
    }

    function testFieldsPutUnique()
    {
        $this->getRequest('api/v1/fields/' . $this->field->getId())
            ->withMethod('PUT')
            ->withBody([
                'name' => $this->field2->getName(null),
                'type' => Field::FIELD_TYPE_TEXT,
                'placeholder' => null,
                'required' => true
            ])
            ->withHeader('Authorization', 'Bearer ' . $this->accessToken)
            ->execute(400);
    }

    function testFieldsDelete()
    {
        $this->getRequest('api/v1/fields/' . $this->field->getId())
            ->withMethod('DELETE')
            ->withHeader('Authorization', 'Bearer ' . $this->accessToken)
            ->execute(200);

        $this->getRequest('api/v1/fields/' . $this->field->getId())
            ->withMethod('DELETE')
            ->withHeader('Authorization', 'Bearer ' . $this->accessToken)
            ->execute(404);
    }


    protected function setUp()
    {
        parent::setUp();

        $this->user = $this->userRepository->findByUsername(BaseFixture::USER_NAME);
        $this->accessToken = $this->tokenProvider->getToken($this->user);

        $this->countryUs = new Country('US', 'United states', "icon");
        $this->countryRepository->persist($this->countryUs);

        $this->field = new TextField(uniqid());
        $this->fieldRepository->persist($this->field);

        $this->field2 = new TextField(uniqid());
        $this->fieldRepository->persist($this->field2);
    }


}

$test = new FieldPresenter($container);
$test->run();
