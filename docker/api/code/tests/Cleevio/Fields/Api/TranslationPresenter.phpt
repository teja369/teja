<?php
declare(strict_types=1);

namespace Test\Cleevio\Fields\Api;

$container = require __DIR__ . '/../../../bootstrap.php';

use Cleevio\Countries\Entities\Country;
use Cleevio\Countries\Repositories\ICountryRepository;
use Cleevio\Fields\Entities\Fields\TextField;
use Cleevio\Fields\Repositories\IFieldRepository;
use Cleevio\Languages\Entities\Language;
use Cleevio\Languages\Repositories\ILanguageRepository;
use Cleevio\RestApi\Auth\ITokenProvider;
use Cleevio\RestApi\Testing\RestTest;
use Cleevio\Users\Entities\User;
use Cleevio\Users\Repositories\IUserRepository;
use Nette;
use Symfony\Component\Console\Application;
use Test\Cleevio\BaseFixture;
use Test\Cleevio\TestContext;
use Tester\Assert;

class TranslationPresenter extends RestTest
{
    /**
     * @var IUserRepository|null
     */
    private $userRepository;

    /**
     * @var ITokenProvider|null
     */
    private $tokenProvider;

    /**
     * @var User
     */
    private $user;

    /**
     * @var string
     */
    private $accessToken;

    /**
     * @var ILanguageRepository|object|null
     */
    private $languageRepository;

    /**
     * @var Language
     */
    private $language;

    /**
     * @var TextField
     */
    private $field;

    /**
     * @var IFieldRepository|object|null
     */
    private $fieldRepository;

    /**
     * @var ICountryRepository|object|null
     */
    private $countryRepository;

    /**
     * CountryPresenter constructor.
     * @param Nette\DI\Container $container
     */
    function __construct(Nette\DI\Container $container)
    {
        parent::__construct(new TestContext($container->getByType(Application::class)));

        $this->userRepository = $container->getByType(IUserRepository::class);
        $this->tokenProvider = $container->getByType(ITokenProvider::class);
        $this->fieldRepository = $container->getByType(IFieldRepository::class);
        $this->languageRepository = $container->getByType(ILanguageRepository::class);
        $this->countryRepository = $container->getByType(ICountryRepository::class);
    }

    function testGet()
    {
        $response = $this->getRequest(sprintf('api/v1/fields/%s/translation/%s', $this->field->getId(), $this->language->getCode()))
            ->withMethod('GET')
            ->withHeader('Authorization', 'Bearer ' . $this->accessToken)
            ->execute(200);

        Assert::notNull($response);
        Assert::same($this->field->getName($this->language->getCode()), $response->name);
    }

    function testPut()
    {
        $name = uniqid();
        $placeholder = uniqid();

        $response = $this->getRequest(sprintf('api/v1/fields/%s/translation/%s', $this->field->getId(), $this->language->getCode()))
            ->withMethod('PUT')
            ->withBody([
                'name' => $name,
                'placeholder' => $placeholder,
            ])
            ->withHeader('Authorization', 'Bearer ' . $this->accessToken)
            ->execute(200);

        Assert::notNull($response);
        Assert::same($name, $response->name);
        //Assert::same($placeholder, $response->placeholder);
    }

    protected function setUp()
    {
        parent::setUp();

        $this->user = $this->userRepository->findByUsername(BaseFixture::USER_NAME);
        $this->accessToken = $this->tokenProvider->getToken($this->user);

        $country = new Country('US', uniqid(), uniqid());
        $this->countryRepository->persist($country);

        $this->language = new Language(uniqid(), "en", $country);
        $this->languageRepository->persist($this->language);

        $this->field = new TextField(uniqid());
        $this->fieldRepository->persist($this->field);
    }


}

$test = new TranslationPresenter($container);
$test->run();
