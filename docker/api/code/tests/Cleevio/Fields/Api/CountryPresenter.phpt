<?php
declare(strict_types=1);

namespace Test\Cleevio\Fields\Api;

$container = require __DIR__ . '/../../../bootstrap.php';

use Cleevio\Countries\Entities\Country;
use Cleevio\Countries\Repositories\ICountryRepository;
use Cleevio\Fields\Entities\Field;
use Cleevio\Fields\Entities\FieldCountry;
use Cleevio\Fields\Entities\Fields\TextField;
use Cleevio\Fields\Repositories\IFieldRepository;
use Cleevio\RestApi\Auth\ITokenProvider;
use Cleevio\RestApi\Testing\RestTest;
use Cleevio\Users\Entities\User;
use Cleevio\Users\Repositories\IUserRepository;
use Nette;
use Symfony\Component\Console\Application;
use Test\Cleevio\BaseFixture;
use Test\Cleevio\TestContext;
use Tester\Assert;

class CountryPresenter extends RestTest
{
    /**
     * @var ICountryRepository|null
     */
    private $countryRepository;

    /**
     * @var IUserRepository|null
     */
    private $userRepository;

    /**
     * @var IFieldRepository|object|null
     */
    private $fieldRepository;

    /**
     * @var ITokenProvider|null
     */
    private $tokenProvider;

    /**
     * @var User
     */
    private $user;

    /**
     * @var string
     */
    private $accessToken;

    /**
     * @var Country
     */
    private $countryUs;

    /**
     * @var Country
     */
    private $countryCz;

    /**
     * @var Field
     */
    private $field;

    /**
     * CountryPresenter constructor.
     * @param Nette\DI\Container $container
     */
    function __construct(Nette\DI\Container $container)
    {
        parent::__construct(new TestContext($container->getByType(Application::class)));

        $this->countryRepository = $container->getByType(ICountryRepository::class);
        $this->userRepository = $container->getByType(IUserRepository::class);
        $this->tokenProvider = $container->getByType(ITokenProvider::class);
        $this->fieldRepository = $container->getByType(IFieldRepository::class);
    }

    function testList()
    {
        $response = $this->getRequest(sprintf('api/v1/fields/%s/country', $this->field->getId()))
            ->withMethod('GET')
            ->withHeader('Authorization', 'Bearer ' . $this->accessToken)
            ->execute(200);

        Assert::notNull($response);
        Assert::count(2, $response);
    }

    function testPut()
    {
        $response = $this->getRequest(sprintf('api/v1/fields/%s/country', $this->field->getId()))
            ->withMethod('PUT')
            ->withBody([
                [
                    'id' => $this->countryCz->getId()
                ]
            ])
            ->withHeader('Authorization', 'Bearer ' . $this->accessToken)
            ->execute(200);

        Assert::notNull($response);
        Assert::count(1, $response);
    }

    function testPutEmpty()
    {
        $response = $this->getRequest(sprintf('api/v1/fields/%s/country', $this->field->getId()))
            ->withMethod('PUT')
            ->withBody([])
            ->withHeader('Authorization', 'Bearer ' . $this->accessToken)
            ->execute(200);

        Assert::notNull($response);
        Assert::count(0, $response);
    }

    function testDelete()
    {
        $response = $this->getRequest(sprintf('api/v1/fields/%s/country/%s', $this->field->getId(), $this->countryCz->getCode()))
            ->withMethod('DELETE')
            ->withHeader('Authorization', 'Bearer ' . $this->accessToken)
            ->execute(200);

        Assert::notNull($response);
        Assert::count(1, $response);
    }

    protected function setUp()
    {
        parent::setUp();

        $this->user = $this->userRepository->findByUsername(BaseFixture::USER_NAME);
        $this->accessToken = $this->tokenProvider->getToken($this->user);

        $this->countryUs = new Country('US', 'United states', "icon");
        $this->countryRepository->persist($this->countryUs);

        $this->countryCz = new Country('CZ', 'Czechia', "icon");
        $this->countryRepository->persist($this->countryCz);

        $this->field = new TextField(uniqid());
        $this->field->setCountries([
            new FieldCountry($this->field, $this->countryUs),
            new FieldCountry($this->field, $this->countryCz)
        ]);
        $this->fieldRepository->persist($this->field);
    }


}

$test = new CountryPresenter($container);
$test->run();
