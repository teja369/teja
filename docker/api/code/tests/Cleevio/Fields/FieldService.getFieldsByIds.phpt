<?php
declare(strict_types=1);

namespace Test\Cleevio\Fields;

$container = require __DIR__ . '/../../bootstrap.php';

use Cleevio\Fields\Entities\Field;
use Cleevio\Fields\Entities\Fields\TextField;
use Cleevio\Fields\FieldNotFound;
use Cleevio\Fields\IFieldService;
use Cleevio\Fields\Repositories\IFieldRepository;
use Nette;
use Symfony\Component\Console\Application;
use Test\Cleevio\BaseTest;
use Test\Cleevio\TestContext;
use Tester\Assert;

class FieldServiceGetPurposesByIdsTest extends BaseTest
{

    /**
     * @var IFieldService|object|null
     */
    private $fieldService;

    /**
     * @var IFieldRepository|object|null
     */
    private $fieldRepository;

    function __construct(Nette\DI\Container $container)
    {
        parent::__construct(new TestContext($container->getByType(Application::class)));

        $this->fieldService = $container->getByType(IFieldService::class);
        $this->fieldRepository = $container->getByType(IFieldRepository::class);
    }

    /**
     * Service should return fields
     */
    function testGetFieldsByIds()
    {
        try {

            $fields = [];
            for ($i = 0; $i < 10; $i++) {
                $field = new TextField(uniqid());
                $this->fieldRepository->persist($field);
                $fields[] = $field->getId();
            }

            $fields = $this->fieldService->getFieldsByIds($fields);
            Assert::count(10, $fields);
        } catch (FieldNotFound $e) {
            Assert::null($e);
        }
    }

    function testGetFieldsByIdsNotFound()
    {
        Assert::exception(function () {

            $fields = [];
            for ($i = 0; $i < 10; $i++) {
                $purpose = new TextField(uniqid());
                $this->fieldRepository->persist($purpose);
                $fields[] = $purpose->getId();
            }

            $this->fieldService->getFieldsByIds([99]);
        }, FieldNotFound::class);
    }


    protected function setUp()
    {
        parent::setUp();

        $this->fieldRepository->persist(new TextField(uniqid()));
        $this->fieldRepository->persist(new TextField(uniqid()));
    }
}

$test = new FieldServiceGetPurposesByIdsTest($container);
$test->run();
