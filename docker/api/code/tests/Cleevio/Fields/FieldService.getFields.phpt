<?php
declare(strict_types=1);

namespace Test\Cleevio\Fields;

$container = require __DIR__ . '/../../bootstrap.php';

use Cleevio\Countries\Entities\Country;
use Cleevio\Countries\Repositories\ICountryRepository;
use Cleevio\Fields\Entities\Field;
use Cleevio\Fields\Entities\FieldCountry;
use Cleevio\Fields\Entities\Fields\TextField;
use Cleevio\Fields\IFieldService;
use Cleevio\Fields\Repositories\IFieldRepository;
use Cleevio\Hosts\Entities\HostField;
use Cleevio\Hosts\Entities\HostFieldCountry;
use Cleevio\Hosts\IHostFieldService;
use Cleevio\Hosts\Repositories\IHostFieldRepository;
use Nette;
use Symfony\Component\Console\Application;
use Test\Cleevio\BaseTest;
use Test\Cleevio\TestContext;
use Tester\Assert;

class FieldServiceGetFields extends BaseTest
{

    /**
     * @var IFieldRepository
     */
    private $fieldRepository;

    /**
     * @var Field
     */
    private $defaultField;

    /**
     * @var Field
     */
    private $countryField;

    /**
     * @var IFieldService|object|null
     */
    private $fieldService;

    /**
     * @var Country
     */
    private $country;

    /**
     * @var ICountryRepository|object|null
     */
    private $countryRepository;

    /**
     * @var Country
     */
    private $countrySk;


    function __construct(Nette\DI\Container $container)
    {
        parent::__construct(new TestContext($container->getByType(Application::class)));
        $this->fieldRepository = $container->getByType(IFieldRepository::class);
        $this->fieldService = $container->getByType(IFieldService::class);
        $this->countryRepository = $container->getByType(ICountryRepository::class);
    }

    function testFieldsForCountry()
    {
        $countryField = $this->fieldService->getFields($this->country, null);

        Assert::notNull($countryField);
        Assert::count(2, $countryField);
    }

    function testFieldsForCountryDifferent()
    {
        $countryField = $this->fieldService->getFields($this->countrySk, null);

        Assert::notNull($countryField);
        Assert::count(1, $countryField);
        Assert::same('Name', $countryField[0]->getName(null));
    }

    function testFieldsDefault()
    {
        $countryField = $this->fieldService->getFields(null, null);

        Assert::notNull($countryField);
        Assert::count(2, $countryField);
        Assert::same('Name', $countryField[0]->getName(null));
    }


    protected function setUp()
    {
        parent::setUp();

        $this->country = new Country('CZ', 'Czech Republic', "icon");
        $this->countryRepository->persist($this->country);

        $this->countrySk = new Country('SK', 'Slovakia', "icon");
        $this->countryRepository->persist($this->countrySk);

        $this->defaultField = new TextField('Name');

        $this->countryField = new TextField('VAT number');
        $this->countryField->setCountries([new FieldCountry($this->countryField, $this->country)]);

        $this->fieldRepository->persist($this->defaultField);
        $this->fieldRepository->persist($this->countryField);

    }
}

$test = new FieldServiceGetFields($container);
$test->run();
