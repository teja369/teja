<?php

declare(strict_types=1);

namespace Test\Cleevio;

use Cleevio\Testing\ITestContext;
use Symfony\Component\Console\Application;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Output\NullOutput;

class TestContext implements ITestContext
{

    /**
     * @var Application
     */
    private $application;

    /**
     * @param Application $application
     */
    public function __construct(Application $application)
    {
        $this->application = $application;
    }

    /**
     * Custom setUp
     * @throws \Throwable
     */
    public function setUp(): void
    {
        $this->migrations();
        $this->fixtures();
    }

    /**
     * Custom tearDown
     */
    public function tearDown(): void
    {

    }

    /**
     * Run database migrations command
     * @throws \Throwable
     */
    protected function migrations()
    {
        $arguments = ['command' => 'migrations:migrate'];

        $input = new ArrayInput($arguments);
        $input->setInteractive(false);

        $this->application->doRun($input, new NullOutput());
    }

    /**
     * Purge database and load new fixtures
     * @throws \Throwable
     */
    protected function fixtures()
    {
        $arguments = ['command' => 'doctrine:fixtures:load', '--fixtures' => TEST_DIR.'/Cleevio/BaseFixture.php'];

        $input = new ArrayInput($arguments);
        $input->setInteractive(false);

        $this->application->doRun($input, new NullOutput());
    }

	/**
	 * Path where tests are located
	 * @return string
	 */
	public function getTestPath(): string
	{
		return TEST_DIR;
	}

	/**
	 * Path where temp files are stored
	 * @return string
	 */
	public function getTempPath(): string
	{
		return CACHE_DIR;
	}
}
