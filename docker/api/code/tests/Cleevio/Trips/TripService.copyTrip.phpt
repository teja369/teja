<?php
declare(strict_types=1);

namespace Test\Cleevio\Trips;

$container = require __DIR__ . '/../../bootstrap.php';

use Cleevio\Countries\Entities\Country;
use Cleevio\Countries\Repositories\ICountryRepository;
use Cleevio\Hosts\Entities\Host;
use Cleevio\Hosts\Repositories\IHostRepository;
use Cleevio\Trips\Entities\Trip;
use Cleevio\Trips\ITripService;
use Cleevio\Trips\Repositories\ITripRepository;
use Cleevio\Users\Entities\User;
use Cleevio\Users\Repositories\IUserRepository;
use DateTime;
use Nette;
use Symfony\Component\Console\Application;
use Test\Cleevio\BaseFixture;
use Test\Cleevio\BaseTest;
use Test\Cleevio\TestContext;
use Tester\Assert;

class TripServiceCopyTrip extends BaseTest
{

    /**
     * @var ITripRepository|object|null
     */
    private $tripRepository;

    /**
     * @var ICountryRepository|object|null
     */
    private $countryRepository;

    /**
     * @var IUserRepository|object|null
     */
    private $userRepository;

    /**
     * @var ITripService|object|null
     */
    private $tripService;

    /**
     * @var IHostRepository|object|null
     */
    private $hostRepository;

    /**
     * @var Country
     */
    private $country;

    /**
     * @var User
     */
    private $user;

    /**
     * @var Trip
     */
    private $trip;


    /**
     * @param Nette\DI\Container $container
     */
    function __construct(Nette\DI\Container $container)
    {
        parent::__construct(new TestContext($container->getByType(Application::class)));

        $this->tripRepository = $container->getByType(ITripRepository::class);
        $this->countryRepository = $container->getByType(ICountryRepository::class);
        $this->userRepository = $container->getByType(IUserRepository::class);
        $this->tripService = $container->getByType(ITripService::class);
        $this->hostRepository = $container->getByType(IHostRepository::class);
    }


    function testCopyTrip()
    {
        $tripId = $this->trip->getId();
        $response = $this->tripService->copyTrip($this->trip);

        Assert::notSame($tripId, $response->getId());
        Assert::same(Trip::TRIP_STATE_DRAFT, $response->getState());
        Assert::same($this->trip->getCountry()->getId(), $response->getCountry()->getId());
        Assert::same($this->trip->getHost()->getId(), $response->getHost()->getId());
        Assert::same($this->trip->getHostSet(), $response->getHostSet());
    }

    protected function setUp()
    {
        parent::setUp();

        $this->user = $this->userRepository->findByUsername(BaseFixture::USER_NAME);

        $this->country = new Country('DE', 'Germany', "icon");
        $this->countryRepository->persist($this->country);

        $host = new Host(uniqid(), $this->country, Host::HOST_TYPE_CLIENT);
        $this->hostRepository->persist($host);

        $this->trip = new Trip(
            $this->country,
            $this->user,
            new DateTime(),
            new DateTime(),
            Trip::TRIP_STATE_SUBMITTED
        );
        $this->trip->setHost($host);

        $this->tripRepository->persist($this->trip);
    }
}

$test = new TripServiceCopyTrip($container);
$test->run();
