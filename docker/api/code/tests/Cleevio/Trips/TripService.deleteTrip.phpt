<?php
declare(strict_types=1);

namespace Test\Cleevio\Trips;

$container = require __DIR__ . '/../../bootstrap.php';

use Cleevio\Countries\Entities\Country;
use Cleevio\Countries\Repositories\ICountryRepository;
use Cleevio\Trips\Entities\Trip;
use Cleevio\Trips\ITripService;
use Cleevio\Trips\Repositories\ITripRepository;
use Cleevio\Users\Entities\User;
use Cleevio\Users\Repositories\IUserRepository;
use DateTime;
use Nette;
use Symfony\Component\Console\Application;
use Test\Cleevio\BaseFixture;
use Test\Cleevio\BaseTest;
use Test\Cleevio\TestContext;
use Tester\Assert;

class TripServiceDeleteTrip extends BaseTest
{

    /**
     * @var ITripRepository|object|null
     */
    private $tripRepository;

    /**
     * @var ICountryRepository|object|null
     */
    private $countryRepository;

    /**
     * @var IUserRepository|object|null
     */
    private $userRepository;

    /**
     * @var ITripService|object|null
     */
    private $tripService;

    /**
     * @var Country
     */
    private $country;

    /**
     * @var User
     */
    private $user;

    /**
     * @var Trip
     */
    private $tripDraft;

    /**
     * @var Trip
     */
    private $tripNotDraft;


    /**
     * @param Nette\DI\Container $container
     */
    function __construct(Nette\DI\Container $container)
    {
        parent::__construct(new TestContext($container->getByType(Application::class)));

        $this->tripRepository = $container->getByType(ITripRepository::class);
        $this->countryRepository = $container->getByType(ICountryRepository::class);
        $this->userRepository = $container->getByType(IUserRepository::class);
        $this->tripService = $container->getByType(ITripService::class);
    }


    function testDeleteTrip()
    {
        $tripId = $this->tripDraft->getId();
        $response = $this->tripService->deleteTrip($this->tripDraft);
        $result = $this->tripRepository->find($tripId);

        Assert::true($response);
        Assert::same(Trip::TRIP_STATE_DELETED, $result->getState());
    }


    function testDeleteTripNotDraft()
    {
        $response = $this->tripService->deleteTrip($this->tripNotDraft);

        Assert::false($response);
    }


    protected function setUp()
    {
        parent::setUp();

        $this->user = $this->userRepository->findByUsername(BaseFixture::USER_NAME);
        $this->country = new Country('DE', 'Germany', "icon");
        $this->countryRepository->persist($this->country);
        $this->tripDraft = new Trip(
            $this->country,
            $this->user,
            new DateTime(),
            new DateTime(),
            Trip::TRIP_STATE_DRAFT
        );

        $this->tripRepository->persist($this->tripDraft);

        $this->tripNotDraft = new Trip(
            $this->country,
            $this->user,
            new DateTime(),
            new DateTime(),
            Trip::TRIP_STATE_SUBMITTED
        );

        $this->tripRepository->persist($this->tripNotDraft);
    }
}

$test = new TripServiceDeleteTrip($container);
$test->run();
