<?php
declare(strict_types=1);

namespace Test\Cleevio\Trips\Api;

$container = require __DIR__ . '/../../../bootstrap.php';

use Cleevio\Answers\Entities\Answers\TextAnswer;
use Cleevio\Countries\Entities\Country;
use Cleevio\Countries\Repositories\ICountryRepository;
use Cleevio\Questions\Entities\Question;
use Cleevio\Questions\Entities\QuestionCountry;
use Cleevio\Questions\Entities\QuestionGroup;
use Cleevio\Questions\Entities\Questions\TextQuestion;
use Cleevio\Questions\Repositories\IGroupRepository;
use Cleevio\Questions\Repositories\IQuestionRepository;
use Cleevio\Registrations\Entities\Registration;
use Cleevio\Registrations\Repositories\IRegistrationRepository;
use Cleevio\RestApi\Auth\ITokenProvider;
use Cleevio\RestApi\Testing\RestTest;
use Cleevio\Trips\Entities\Trip;
use Cleevio\Trips\Entities\TripRegistration;
use Cleevio\Trips\Repositories\ITripRepository;
use Cleevio\Trips\TripService;
use Cleevio\Users\Entities\User;
use Cleevio\Users\Repositories\IUserRepository;
use DateTime;
use Nette;
use Symfony\Component\Console\Application;
use Test\Cleevio\BaseFixture;
use Test\Cleevio\TestContext;
use Tester\Assert;

class TripStepPresenter extends RestTest
{

    /**
     * @var ICountryRepository|object|null
     */
    private $countryRepository;

    /**
     * @var IUserRepository|object|null
     */
    private $userRepository;

    /**
     * @var ITokenProvider
     */
    private $tokenProvider;

    /**
     * @var ITripRepository|object|null
     */
    private $tripRepository;
    /**
     * @var Country
     */
    private $country;

    /**
     * @var User
     */
    private $user;

    /**
     * @var string
     */
    private $accessToken;

    /**
     * @var IGroupRepository|object|null
     */
    private $groupRepository;

    /**
     * @var IQuestionRepository|object|null
     */
    private $questionRepository;

    /**
     * @var TextQuestion
     */
    private $questionPurpose;

    /**
     * @var Registration
     */
    private $registration;

    /**
     * @var IRegistrationRepository|object|null
     */
    private $registrationRepository;

    /**
     * @var TextQuestion
     */
    private $questionDetail;

    /**
     * LoginPresenter constructor.
     * @param Nette\DI\Container $container
     */
    function __construct(Nette\DI\Container $container)
    {
        parent::__construct(new TestContext($container->getByType(Application::class)));

        $this->countryRepository = $container->getByType(ICountryRepository::class);
        $this->userRepository = $container->getByType(IUserRepository::class);
        $this->tokenProvider = $container->getByType(ITokenProvider::class);
        $this->tripRepository = $container->getByType(ITripRepository::class);
        $this->groupRepository = $container->getByType(IGroupRepository::class);
        $this->questionRepository = $container->getByType(IQuestionRepository::class);
        $this->registrationRepository = $container->getByType(IRegistrationRepository::class);
    }

    function testGetPurpose()
    {
        $trip = new Trip($this->country, $this->user,
            (new DateTime())->modify('+2 days'),
            (new DateTime())->modify('+3 days')
        );
        $this->tripRepository->persist($trip);

        $response = $this->getRequest('api/v1/trips/' . $trip->getId() . '/step')
            ->withHeader('Authorization', 'Bearer ' . $this->accessToken)
            ->execute(200);

        Assert::notNull($response);
        Assert::equal(TripService::TRIP_VALIDATION_PURPOSE, $response->step);
    }

    function testGetHost()
    {
        $trip = new Trip($this->country, $this->user,
            (new DateTime())->modify('+2 days'),
            (new DateTime())->modify('+3 days')
        );

        $answer = new TextAnswer($trip, $this->questionPurpose, uniqid());
        $trip->addAnswer($answer);

        $this->tripRepository->persist($trip);

        $response = $this->getRequest('api/v1/trips/' . $trip->getId() . '/step')
            ->withHeader('Authorization', 'Bearer ' . $this->accessToken)
            ->execute(200);

        Assert::notNull($response);
        Assert::equal(TripService::TRIP_VALIDATION_HOST, $response->step);
    }

    function testGetRegistration()
    {
        $trip = new Trip($this->country, $this->user,
            (new DateTime())->modify('+2 days'),
            (new DateTime())->modify('+3 days')
        );

        $answer = new TextAnswer($trip, $this->questionPurpose, uniqid());
        $trip->addAnswer($answer);
        $trip->setHost(null);
        $this->tripRepository->persist($trip);

        $response = $this->getRequest('api/v1/trips/' . $trip->getId() . '/step')
            ->withHeader('Authorization', 'Bearer ' . $this->accessToken)
            ->execute(200);

        Assert::notNull($response);
        Assert::equal(TripService::TRIP_VALIDATION_REGISTRATION, $response->step);
    }

    function testGetDetails()
    {
        $trip = new Trip($this->country, $this->user,
            (new DateTime())->modify('+2 days'),
            (new DateTime())->modify('+3 days')
        );

        $answer = new TextAnswer($trip, $this->questionPurpose, uniqid());
        $trip->addAnswer($answer);

        $trip->setHost(null);
        $trip->setRegistrations([new TripRegistration($trip, $this->registration)]);

        $this->tripRepository->persist($trip);

        $response = $this->getRequest('api/v1/trips/' . $trip->getId() . '/step')
            ->withHeader('Authorization', 'Bearer ' . $this->accessToken)
            ->execute(200);

        Assert::notNull($response);
        Assert::equal(TripService::TRIP_VALIDATION_DETAILS, $response->step);
    }

    protected function setUp()
    {
        parent::setUp();
        $this->user = $this->userRepository->findByUsername(BaseFixture::USER_NAME);
        $this->accessToken = $this->tokenProvider->getToken($this->user);

        $this->country = new Country('cz', 'Czech Republic', "icon");
        $this->country->setIsEnabled(true);
        $this->countryRepository->persist($this->country);

        // Prepare purpose step
        $group = new QuestionGroup(uniqid(), QuestionGroup::QUESTION_GROUP_TYPE_PURPOSE);
        $this->groupRepository->persist($group);

        $this->questionPurpose = new TextQuestion(uniqid(), $group);
        $this->questionPurpose->setVisibility(Question::QUESTION_VISIBILITY_ENABLED);
        $this->questionPurpose->setRequired(true);
        $this->questionPurpose->setCountries([new QuestionCountry($this->country, $this->questionPurpose)]);
        $this->questionRepository->persist($this->questionPurpose);

        $this->registration = new Registration(uniqid());
        $this->registrationRepository->persist($this->registration);

        // Prepare question step
        $group = new QuestionGroup(uniqid(), QuestionGroup::QUESTION_GROUP_TYPE_DETAIL);
        $this->groupRepository->persist($group);

        $this->questionDetail = new TextQuestion(uniqid(), $group);
        $this->questionDetail->setVisibility(Question::QUESTION_VISIBILITY_ENABLED);
        $this->questionDetail->setRequired(true);
        $this->questionDetail->setCountries([new QuestionCountry($this->country, $this->questionDetail)]);
        $this->questionRepository->persist($this->questionDetail);
    }
}

$test = new TripStepPresenter($container);
$test->run();
