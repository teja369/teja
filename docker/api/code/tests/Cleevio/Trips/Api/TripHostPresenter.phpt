<?php
declare(strict_types=1);

namespace Test\Cleevio\Trips\Api;

$container = require __DIR__ . '/../../../bootstrap.php';

use Cleevio\Countries\Entities\Country;
use Cleevio\Countries\Repositories\ICountryRepository;
use Cleevio\Hosts\Entities\Host;
use Cleevio\Hosts\Repositories\IHostRepository;
use Cleevio\Questions\Repositories\IQuestionRepository;
use Cleevio\Registrations\Repositories\IRegistrationRepository;
use Cleevio\RestApi\Auth\ITokenProvider;
use Cleevio\RestApi\Testing\RestTest;
use Cleevio\Trips\Entities\Trip;
use Cleevio\Trips\Repositories\ITripRepository;
use Cleevio\Users\Entities\User;
use Cleevio\Users\Repositories\IUserRepository;
use DateTime;
use Nette;
use Symfony\Component\Console\Application;
use Test\Cleevio\BaseFixture;
use Test\Cleevio\TestContext;
use Tester\Assert;

class TripHostPresenter extends RestTest
{

    /**
     * @var ICountryRepository|object|null
     */
    private $countryRepository;

    /**
     * @var IUserRepository|object|null
     */
    private $userRepository;

    /**
     * @var ITokenProvider
     */
    private $tokenProvider;

    /**
     * @var ITripRepository|object|null
     */
    private $tripRepository;

    /**
     * @var IHostRepository|object|null
     */
    private $hostRepository;

    /**
     * @var IRegistrationRepository|object|null
     */
    private $registrationRepository;

    /**
     * @var IQuestionRepository|object|null
     */
    private $questionRepository;

    /**
     * @var Country
     */
    private $country;

    /**
     * @var User
     */
    private $user;

    /**
     * @var string
     */
    private $accessToken;

    /**
     * LoginPresenter constructor.
     * @param Nette\DI\Container $container
     */
    function __construct(Nette\DI\Container $container)
    {
        parent::__construct(new TestContext($container->getByType(Application::class)));

        $this->countryRepository = $container->getByType(ICountryRepository::class);
        $this->userRepository = $container->getByType(IUserRepository::class);
        $this->tokenProvider = $container->getByType(ITokenProvider::class);
        $this->tripRepository = $container->getByType(ITripRepository::class);
        $this->hostRepository = $container->getByType(IHostRepository::class);
        $this->registrationRepository = $container->getByType(IRegistrationRepository::class);
        $this->questionRepository = $container->getByType(IQuestionRepository::class);
    }

    function testTripPutHost()
    {
        $trip = new Trip($this->country, $this->user, (new DateTime())->modify('+1 day'), (new DateTime())->modify('+2 days'));
        $this->tripRepository->persist($trip);

        $host = new Host(uniqid(), $this->country, 'client');;
        $this->hostRepository->persist($host);

        $response = $this->getRequest(sprintf('api/v1/trips/%s/host/%s', $trip->getId(), $host->getId()))
            ->withMethod('PUT')
            ->withHeader('Authorization', 'Bearer ' . $this->accessToken)
            ->execute(200);

        Assert::notNull($response);
        Assert::notNull($response->host);
        Assert::equal($host->getId(), $response->host->id);
    }

    function testTripDeleteHost()
    {
        $host = new Host(uniqid(), $this->country, 'client');;
        $this->hostRepository->persist($host);

        $trip = new Trip($this->country, $this->user, new DateTime(), new DateTime());
        $trip->setHost($host);
        $this->tripRepository->persist($trip);

        $response = $this->getRequest(sprintf('api/v1/trips/%s/host', $trip->getId()))
            ->withMethod('DELETE')
            ->withHeader('Authorization', 'Bearer ' . $this->accessToken)
            ->execute(200);

        Assert::notNull($response);
        Assert::null($response->host);
    }

    protected function setUp()
    {
        parent::setUp();
        $this->user = $this->userRepository->findByUsername(BaseFixture::USER_NAME);
        $this->accessToken = $this->tokenProvider->getToken($this->user);
        $this->country = new Country('cz', 'Czech Republic', "icon");
        $this->country->setIsEnabled(true);
        $this->countryRepository->persist($this->country);

    }
}

$test = new TripHostPresenter($container);
$test->run();
