<?php
declare(strict_types=1);

namespace Test\Cleevio\Trips\Api;

$container = require __DIR__ . '/../../../bootstrap.php';

use Cleevio\Countries\Entities\Country;
use Cleevio\Countries\Repositories\ICountryRepository;
use Cleevio\Hosts\Entities\Host;
use Cleevio\Hosts\Repositories\IHostRepository;
use Cleevio\Questions\Repositories\IQuestionRepository;
use Cleevio\Registrations\Repositories\IRegistrationRepository;
use Cleevio\RestApi\Auth\ITokenProvider;
use Cleevio\RestApi\Testing\RestTest;
use Cleevio\Trips\Entities\Trip;
use Cleevio\Trips\Repositories\ITripRepository;
use Cleevio\Users\Entities\User;
use Cleevio\Users\Repositories\IUserRepository;
use DateTime;
use Nette;
use Symfony\Component\Console\Application;
use Test\Cleevio\BaseFixture;
use Test\Cleevio\TestContext;
use Tester\Assert;

class TripPresenter extends RestTest
{

    private const ON_HOLD_MESSAGE = 'On hold message';

    /**
     * @var ICountryRepository|object|null
     */
    private $countryRepository;

    /**
     * @var IUserRepository|object|null
     */
    private $userRepository;

    /**
     * @var ITokenProvider
     */
    private $tokenProvider;

    /**
     * @var ITripRepository|object|null
     */
    private $tripRepository;

    /**
     * @var IHostRepository|object|null
     */
    private $hostRepository;

    /**
     * @var IRegistrationRepository|object|null
     */
    private $registrationRepository;

    /**
     * @var IQuestionRepository|object|null
     */
    private $questionRepository;

    /**
     * @var Country
     */
    private $country;

    /**
     * @var Country
     */
    private $countryCz;

    /**
     * @var User
     */
    private $user;

    /**
     * @var string
     */
    private $accessToken;

    /**
     * @var Trip
     */
    private $trip;

    /**
     * @var Host
     */
    private $host1;

    /**
     * @var Host
     */
    private $host2;


    /**
     * LoginPresenter constructor.
     * @param Nette\DI\Container $container
     */
    function __construct(Nette\DI\Container $container)
    {
        parent::__construct(new TestContext($container->getByType(Application::class)));

        $this->countryRepository = $container->getByType(ICountryRepository::class);
        $this->userRepository = $container->getByType(IUserRepository::class);
        $this->tokenProvider = $container->getByType(ITokenProvider::class);
        $this->tripRepository = $container->getByType(ITripRepository::class);
        $this->hostRepository = $container->getByType(IHostRepository::class);
        $this->registrationRepository = $container->getByType(IRegistrationRepository::class);
        $this->questionRepository = $container->getByType(IQuestionRepository::class);
    }

    function testTripGet()
    {
        $response = $this->getRequest('api/v1/trips/' . $this->trip->getId())
            ->withHeader('Authorization', 'Bearer ' . $this->accessToken)
            ->execute(200);

        Assert::notNull($response);
    }


    function testTripGetUnauthorized()
    {
        $this->getRequest('api/v1/trips/' . $this->trip->getId())
            ->execute(401);
    }


    function testTripGetNotFound()
    {
        $this->getRequest('api/v1/trips/' . 99)
            ->withHeader('Authorization', 'Bearer ' . $this->accessToken)
            ->execute(404);
    }


    function testTripList()
    {
        $response = $this->getRequest('api/v1/trips')
            ->withHeader('Authorization', 'Bearer ' . $this->accessToken)
            ->execute(200);

        Assert::notNull($response);
        Assert::count(1, $response->items);
    }


    function testTripListByState()
    {
        $trip = new Trip(
            $this->country,
            $this->user,
            (new DateTime())->modify('+2 days'),
            (new DateTime())->modify('+3 days')
        );
        $trip->setState(Trip::TRIP_STATE_SUBMITTED);
        $this->tripRepository->persist($trip);

        $response = $this->getRequest('api/v1/trips?state=' . Trip::TRIP_STATE_SUBMITTED)
            ->withHeader('Authorization', 'Bearer ' . $this->accessToken)
            ->execute(200);

        Assert::notNull($response);
        Assert::count(1, $response->items);
    }


    function testTripListByStates()
    {
        $trip = new Trip(
            $this->country,
            $this->user,
            (new DateTime())->modify('+2 days'),
            (new DateTime())->modify('+3 days')
        );
        $trip->setState(Trip::TRIP_STATE_SUBMITTED);
        $this->tripRepository->persist($trip);

        $response = $this->getRequest('api/v1/trips?state=' . Trip::TRIP_STATE_SUBMITTED . '|' . Trip::TRIP_STATE_DRAFT)
            ->withHeader('Authorization', 'Bearer ' . $this->accessToken)
            ->execute(200);

        Assert::notNull($response);
        Assert::count(2, $response->items);
    }


    function testTripListByCountry()
    {
        $response = $this->getRequest('api/v1/trips?country=' . $this->country->getCode())
            ->withHeader('Authorization', 'Bearer ' . $this->accessToken)
            ->execute(200);

        Assert::notNull($response);
        Assert::count(1, $response->items);
    }

    function testTripListByCountryNotFound()
    {
        $response = $this->getRequest('api/v1/trips?country=' . $this->countryCz->getCode())
            ->withHeader('Authorization', 'Bearer ' . $this->accessToken)
            ->execute(200);

        Assert::notNull($response);
        Assert::count(0, $response->items);
    }

    function testTripListByHost()
    {
        $response = $this->getRequest('api/v1/trips?host=' . $this->host1->getId())
            ->withHeader('Authorization', 'Bearer ' . $this->accessToken)
            ->execute(200);

        Assert::notNull($response);
        Assert::count(1, $response->items);
    }


    function testTripListByHostNotFound()
    {
        $response = $this->getRequest('api/v1/trips?host=' . $this->host2->getId())
            ->withHeader('Authorization', 'Bearer ' . $this->accessToken)
            ->execute(200);

        Assert::notNull($response);
        Assert::count(0, $response->items);
    }


    function testTripListByMinStartDate()
    {
        $response = $this->getRequest('api/v1/trips?minStartDate=' . (new DateTime())->modify('+1 days')
                ->format("Y-m-d"))
            ->withHeader('Authorization', 'Bearer ' . $this->accessToken)
            ->execute(200);

        Assert::notNull($response);
        Assert::count(1, $response->items);
    }


    function testTripListByMinStartDateNotFound()
    {
        $response = $this->getRequest('api/v1/trips?minStartDate=' . (new DateTime())->modify('+3 days')
                ->format("Y-m-d"))
            ->withHeader('Authorization', 'Bearer ' . $this->accessToken)
            ->execute(200);

        Assert::notNull($response);
        Assert::count(0, $response->items);
    }


    function testTripListByMaxStartDate()
    {
        $response = $this->getRequest('api/v1/trips?maxStartDate=' . (new DateTime())->modify('+3 days')
                ->format("Y-m-d"))
            ->withHeader('Authorization', 'Bearer ' . $this->accessToken)
            ->execute(200);

        Assert::notNull($response);
        Assert::count(1, $response->items);
    }


    function testTripListByMaxStartDateNotFound()
    {
        $response = $this->getRequest('api/v1/trips?maxStartDate=' . (new DateTime())->modify('+1 days')
                ->format("Y-m-d"))
            ->withHeader('Authorization', 'Bearer ' . $this->accessToken)
            ->execute(200);

        Assert::notNull($response);
        Assert::count(0, $response->items);
    }


    function testTripListByMinEndDate()
    {
        $response = $this->getRequest('api/v1/trips?minEndDate=' . (new DateTime())->modify('+3 days')->format("Y-m-d"))
            ->withHeader('Authorization', 'Bearer ' . $this->accessToken)
            ->execute(200);

        Assert::notNull($response);
        Assert::count(1, $response->items);
    }


    function testTripListByMinEndDateNotFound()
    {
        $response = $this->getRequest('api/v1/trips?minEndDate=' . (new DateTime())->modify('+4 days')->format("Y-m-d"))
            ->withHeader('Authorization', 'Bearer ' . $this->accessToken)
            ->execute(200);

        Assert::notNull($response);
        Assert::count(0, $response->items);
    }


    function testTripListByMaxEndDate()
    {
        $response = $this->getRequest('api/v1/trips?maxEndDate=' . (new DateTime())->modify('+4 days')->format("Y-m-d"))
            ->withHeader('Authorization', 'Bearer ' . $this->accessToken)
            ->execute(200);

        Assert::notNull($response);
        Assert::count(1, $response->items);
    }


    function testTripListByMaxEndDateNotFound()
    {
        $response = $this->getRequest('api/v1/trips?maxEndDate=' . (new DateTime())->modify('+2 days')->format("Y-m-d"))
            ->withHeader('Authorization', 'Bearer ' . $this->accessToken)
            ->execute(200);

        Assert::notNull($response);
        Assert::count(0, $response->items);
    }

    function testTripListSortUnknown()
    {
        $this->getRequest('api/v1/trips?orderBy=' . uniqid())
            ->withHeader('Authorization', 'Bearer ' . $this->accessToken)
            ->execute(400);
    }

    function testTripListSortStartAsc()
    {
        $trip = new Trip(
            $this->country,
            $this->user,
            (new DateTime())->modify('+3 days'),
            (new DateTime())->modify('+4 days')
        );
        $this->tripRepository->persist($trip);

        $response = $this->getRequest('api/v1/trips?orderBy=start&orderDir=asc')
            ->withHeader('Authorization', 'Bearer ' . $this->accessToken)
            ->execute(200);

        Assert::notNull($response);
        Assert::count(2, $response->items);
        Assert::same($this->trip->getId(), $response->items[0]->id);
    }


    function testTripListSortStartDesc()
    {
        $trip = new Trip(
            $this->country,
            $this->user,
            (new DateTime())->modify('+3 days'),
            (new DateTime())->modify('+4 days')
        );
        $this->tripRepository->persist($trip);

        $response = $this->getRequest('api/v1/trips?orderBy=start&orderDir=desc')
            ->withHeader('Authorization', 'Bearer ' . $this->accessToken)
            ->execute(200);

        Assert::notNull($response);
        Assert::count(2, $response->items);
        Assert::same($trip->getId(), $response->items[0]->id);
    }


    function testTripPost()
    {
        $trip_start = (new DateTime)->modify('+4 days')->format("Y-m-d");
        $trip_end = (new DateTime)->modify('+5 days')->format("Y-m-d");

        $response = $this->getRequest('api/v1/trips')
            ->withMethod('POST')
            ->withBody([
                'start' => $trip_start,
                'end' => $trip_end,
                'country' => $this->country->getCode(),
            ])
            ->withHeader('Authorization', 'Bearer ' . $this->accessToken)
            ->execute(201);

        Assert::equal($this->country->getCode(), $response->country->code);
        Assert::equal(Trip::TRIP_STATE_DRAFT, $response->state);
        Assert::equal($trip_start, $response->start);
        Assert::equal($trip_end, $response->end);
    }


    function testTripPostUnauthorized()
    {
        $trip_start = (new DateTime)->modify('+1 days')->format("Y-m-d");
        $trip_end = (new DateTime)->modify('+2 days')->format("Y-m-d");

        $this->getRequest('api/v1/trips')
            ->withMethod('POST')
            ->withBody([
                'start' => $trip_start,
                'end' => $trip_end,
                'country' => $this->country->getCode(),
            ])
            ->execute(401);
    }


    function testTripPostTodayDate()
    {
        $trip_start = (new DateTime)->format("Y-m-d");
        $trip_end = (new DateTime)->modify('+1 days')->format("Y-m-d");

        $this->getRequest('api/v1/trips')
            ->withMethod('POST')
            ->withBody([
                'start' => $trip_start,
                'end' => $trip_end,
                'country' => $this->country->getCode(),
            ])
            ->withHeader('Authorization', 'Bearer ' . $this->accessToken)
            ->execute(201);
    }


    function testTripPostInvalidDateFormat()
    {
        $trip_start = (new DateTime)->modify('+1 days')->format("Y-md");
        $trip_end = (new DateTime)->modify('+2 days')->format("Ym-d");

        $this->getRequest('api/v1/trips')
            ->withMethod('POST')
            ->withBody([
                'start' => $trip_start,
                'end' => $trip_end,
                'country' => $this->country->getCode(),
            ])
            ->withHeader('Authorization', 'Bearer ' . $this->accessToken)
            ->execute(400);
    }


    function testTripPostInPast()
    {
        $trip_start = (new DateTime)->modify('-2 days')->format("Y-m-d");
        $trip_end = (new DateTime)->modify('-1 days')->format("Y-m-d");

        $this->getRequest('api/v1/trips')
            ->withMethod('POST')
            ->withBody([
                'start' => $trip_start,
                'end' => $trip_end,
                'country' => $this->country->getCode(),
            ])
            ->withHeader('Authorization', 'Bearer ' . $this->accessToken)
            ->execute(400);
    }


    function testTripPostStartGreater()
    {
        $trip_start = (new DateTime)->modify('+3 days')->format("Y-m-d");
        $trip_end = (new DateTime)->modify('+2 days')->format("Y-m-d");

        $this->getRequest('api/v1/trips')
            ->withMethod('POST')
            ->withBody([
                'start' => $trip_start,
                'end' => $trip_end,
                'country' => $this->country->getCode(),
            ])
            ->withHeader('Authorization', 'Bearer ' . $this->accessToken)
            ->execute(400);
    }

    function testTripPostOverlap()
    {
        $trip_start = (new DateTime)->modify('+1 days')->format("Y-m-d");
        $trip_end = (new DateTime)->modify('+5 days')->format("Y-m-d");

        $this->getRequest('api/v1/trips')
            ->withMethod('POST')
            ->withBody([
                'start' => $trip_start,
                'end' => $trip_end,
                'country' => $this->country->getCode(),
            ])
            ->withHeader('Authorization', 'Bearer ' . $this->accessToken)
            ->execute(400);
    }

    function testTripDelete()
    {
        $this->getRequest('api/v1/trips/' . $this->trip->getId())
            ->withMethod('DELETE')
            ->withHeader('Authorization', 'Bearer ' . $this->accessToken)
            ->execute(200);
    }


    function testTripCopy()
    {
        $response = $this->getRequest('api/v1/trips/' . $this->trip->getId() . '/copy')
            ->withMethod('GET')
            ->withHeader('Authorization', 'Bearer ' . $this->accessToken)
            ->execute(200);

        Assert::notNull($response);
        Assert::notSame($this->trip->getId(), $response->id);
    }


    function testTripCancel()
    {
        $trip = new Trip(
            $this->country,
            $this->user,
            (new DateTime())->modify('+2 days'),
            (new DateTime())->modify('+3 days')
        );
        $trip->setHost($this->host1);
        $trip->setRegistrationCheck(new DateTime());
        $trip->setState(Trip::TRIP_STATE_SUBMITTED);
        $this->tripRepository->persist($trip);

        $this->getRequest('api/v1/trips/' . $trip->getId() . '/cancel')
            ->withMethod('PUT')
            ->withHeader('Authorization', 'Bearer ' . $this->accessToken)
            ->execute(200);
    }

    function testTripCancelProcessed()
    {
        $trip = new Trip(
            $this->country,
            $this->user,
            (new DateTime())->modify('+2 days'),
            (new DateTime())->modify('+3 days'),
            Trip::TRIP_STATE_PROCESSED
        );
        $this->tripRepository->persist($trip);

        $this->getRequest('api/v1/trips/' . $trip->getId() . '/cancel')
            ->withMethod('PUT')
            ->withHeader('Authorization', 'Bearer ' . $this->accessToken)
            ->execute(400);
    }

    function testTripProcess()
    {
        $trip = new Trip(
            $this->country,
            $this->user,
            (new DateTime())->modify('+2 days'),
            (new DateTime())->modify('+3 days')
        );
        $trip->setHost($this->host1);
        $trip->setRegistrationCheck(new DateTime());
        $trip->setState(Trip::TRIP_STATE_SUBMITTED);
        $this->tripRepository->persist($trip);

        $this->getRequest('api/v1/trips/' . $trip->getId() . '/process')
            ->withMethod('PUT')
            ->withHeader('Authorization', 'Bearer ' . $this->accessToken)
            ->execute(200);
    }

    function testTripProcessProcessed()
    {
        $trip = new Trip(
            $this->country,
            $this->user,
            (new DateTime())->modify('+2 days'),
            (new DateTime())->modify('+3 days'),
            Trip::TRIP_STATE_PROCESSED
        );
        $trip->setHost($this->host1);
        $trip->setRegistrationCheck(new DateTime());
        $this->tripRepository->persist($trip);

        $this->getRequest('api/v1/trips/' . $trip->getId() . '/process')
            ->withMethod('PUT')
            ->withHeader('Authorization', 'Bearer ' . $this->accessToken)
            ->execute(400);
    }


    function testTripFavoritePut()
    {
        $trip = new Trip(
            $this->country,
            $this->user,
            (new DateTime())->modify('+2 days'),
            (new DateTime())->modify('+3 days')
        );
        $trip->setState(Trip::TRIP_STATE_SUBMITTED);
        $this->tripRepository->persist($trip);

        $this->getRequest('api/v1/trips/' . $trip->getId() . '/favorite')
            ->withMethod('PUT')
            ->withHeader('Authorization', 'Bearer ' . $this->accessToken)
            ->execute(200);

        $response = $this->getRequest('api/v1/trips/' . $trip->getId())
            ->withHeader('Authorization', 'Bearer ' . $this->accessToken)
            ->execute(200);

        Assert::notNull($response);
        Assert::true($response->isFavorite);
    }

    function testTripFavoriteDelete()
    {
        $trip = new Trip(
            $this->country,
            $this->user,
            (new DateTime())->modify('+2 days'),
            (new DateTime())->modify('+3 days')
        );
        $trip->setState(Trip::TRIP_STATE_SUBMITTED);
        $this->tripRepository->persist($trip);

        $this->getRequest('api/v1/trips/' . $trip->getId() . '/favorite')
            ->withMethod('DELETE')
            ->withHeader('Authorization', 'Bearer ' . $this->accessToken)
            ->execute(200);

        $response = $this->getRequest('api/v1/trips/' . $trip->getId())
            ->withHeader('Authorization', 'Bearer ' . $this->accessToken)
            ->execute(200);

        Assert::notNull($response);
        Assert::false($response->isFavorite);
    }


    function testTripOnHoldPut()
    {
        $trip = new Trip(
            $this->country,
            $this->user,
            (new DateTime())->modify('+2 days'),
            (new DateTime())->modify('+3 days')
        );
        $trip->setState(Trip::TRIP_STATE_SUBMITTED);
        $this->tripRepository->persist($trip);

        $response = $this->getRequest('api/v1/trips/' . $trip->getId() . '/onhold')
            ->withMethod('PUT')
            ->withBody(['message' => self::ON_HOLD_MESSAGE])
            ->withHeader('Authorization', 'Bearer ' . $this->accessToken)
            ->execute(200);

        Assert::notNull($response);
        Assert::equal($response->state, Trip::TRIP_STATE_ON_HOLD);
        Assert::equal($response->stateMessage, self::ON_HOLD_MESSAGE);
    }

    function testTripOnHoldDelete()
    {
        $trip = new Trip(
            $this->country,
            $this->user,
            (new DateTime())->modify('+2 days'),
            (new DateTime())->modify('+3 days')
        );
        $trip->setState(Trip::TRIP_STATE_ON_HOLD);
        $this->tripRepository->persist($trip);

        $response = $this->getRequest('api/v1/trips/' . $trip->getId() . '/onhold')
            ->withMethod('DELETE')
            ->withHeader('Authorization', 'Bearer ' . $this->accessToken)
            ->execute(200);

        Assert::notNull($response);
        Assert::equal($response->state, Trip::TRIP_STATE_SUBMITTED);
        Assert::null($response->stateMessage);
    }


    protected function setUp()
    {
        parent::setUp();
        $this->user = $this->userRepository->findByUsername(BaseFixture::USER_NAME);
        $this->accessToken = $this->tokenProvider->getToken($this->user);

        $this->country = new Country('cz', 'Czech Republic', "icon");
        $this->country->setIsEnabled(true);
        $this->countryRepository->persist($this->country);

        $this->countryCz = new Country('us', 'United states', "icon");
        $this->countryRepository->persist($this->countryCz);

        $this->host1 = new Host(uniqid(), $this->country, Host::HOST_TYPE_CLIENT);
        $this->hostRepository->persist($this->host1);

        $this->host2 = new Host(uniqid(), $this->country, Host::HOST_TYPE_CLIENT);
        $this->hostRepository->persist($this->host2);

        $this->trip = new Trip(
            $this->country,
            $this->user,
            (new DateTime())->modify('+2 days'),
            (new DateTime())->modify('+3 days')
        );
        $this->trip->setHost($this->host1);
        $this->tripRepository->persist($this->trip);

    }
}

$test = new TripPresenter($container);
$test->run();
