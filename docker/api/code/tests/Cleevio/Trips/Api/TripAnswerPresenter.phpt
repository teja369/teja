<?php
declare(strict_types=1);

namespace Test\Cleevio\Trips\Api;

$container = require __DIR__ . '/../../../bootstrap.php';

use Cleevio\Answers\Entities\Answers\TextAnswer;
use Cleevio\Countries\Entities\Country;
use Cleevio\Countries\Repositories\ICountryRepository;
use Cleevio\Hosts\Repositories\IHostRepository;
use Cleevio\Questions\Entities\Question;
use Cleevio\Questions\Entities\QuestionGroup;
use Cleevio\Questions\Entities\Questions\TextQuestion;
use Cleevio\Questions\Repositories\IGroupRepository;
use Cleevio\Questions\Repositories\IQuestionRepository;
use Cleevio\Registrations\Entities\Registration;
use Cleevio\Registrations\Entities\RegistrationQuestion;
use Cleevio\Registrations\Repositories\IRegistrationRepository;
use Cleevio\RestApi\Auth\ITokenProvider;
use Cleevio\RestApi\Testing\RestTest;
use Cleevio\Trips\Entities\Trip;
use Cleevio\Trips\Entities\TripRegistration;
use Cleevio\Trips\Repositories\ITripRepository;
use Cleevio\Users\Entities\User;
use Cleevio\Users\Repositories\IUserRepository;
use DateTime;
use Nette;
use Symfony\Component\Console\Application;
use Test\Cleevio\BaseFixture;
use Test\Cleevio\TestContext;
use Tester\Assert;

class TripAnswerPresenter extends RestTest
{

    /**
     * @var ICountryRepository|object|null
     */
    private $countryRepository;

    /**
     * @var IUserRepository|object|null
     */
    private $userRepository;

    /**
     * @var ITokenProvider
     */
    private $tokenProvider;

    /**
     * @var ITripRepository|object|null
     */
    private $tripRepository;

    /**
     * @var IHostRepository|object|null
     */
    private $hostRepository;

    /**
     * @var IRegistrationRepository|object|null
     */
    private $registrationRepository;

    /**
     * @var IQuestionRepository|object|null
     */
    private $questionRepository;

    /**
     * @var IGroupRepository|object|null
     */
    private $groupRepository;

    /**
     * @var Country
     */
    private $country;

    /**
     * @var User
     */
    private $user;

    /**
     * @var string
     */
    private $accessToken;

    /**
     * @var QuestionGroup
     */
    private $group;

    /**
     * LoginPresenter constructor.
     * @param Nette\DI\Container $container
     */
    function __construct(Nette\DI\Container $container)
    {
        parent::__construct(new TestContext($container->getByType(Application::class)));

        $this->countryRepository = $container->getByType(ICountryRepository::class);
        $this->userRepository = $container->getByType(IUserRepository::class);
        $this->tokenProvider = $container->getByType(ITokenProvider::class);
        $this->tripRepository = $container->getByType(ITripRepository::class);
        $this->hostRepository = $container->getByType(IHostRepository::class);
        $this->registrationRepository = $container->getByType(IRegistrationRepository::class);
        $this->questionRepository = $container->getByType(IQuestionRepository::class);
        $this->groupRepository = $container->getByType(IGroupRepository::class);
    }

    function testTripPutAnswer()
    {
        $registration = new Registration(uniqid());
        $this->registrationRepository->persist($registration);

        $question = new TextQuestion(uniqid(), $this->group);
        $question->setRegistrations([new RegistrationQuestion($question, $registration)]);
        $this->questionRepository->persist($question);

        $trip = new Trip($this->country, $this->user, (new DateTime())->modify('+1 day'), (new DateTime())->modify('+2 days'));
        $trip->setRegistrations([new TripRegistration($trip, $registration)]);
        $trip->setRegistrationCheck(new DateTime());
        $this->tripRepository->persist($trip);

        $response = $this->getRequest(sprintf('api/v1/trips/%s/answer', $trip->getId()))
            ->withMethod('PUT')
            ->withBody([[
                'id' => $question->getId(),
                'text' => uniqid(),
            ]])
            ->withHeader('Authorization', 'Bearer ' . $this->accessToken)
            ->execute(200);

        Assert::notNull($response);
    }

    function testTripGetPreFilledAnswer()
    {
        $registration = new Registration(uniqid());
        $this->registrationRepository->persist($registration);

        $question = new TextQuestion(uniqid(), $this->group);
        $question->setVisibility(Question::QUESTION_VISIBILITY_ENABLED);
        $question->setIsPrefilled(true);
        $question->setRegistrations([new RegistrationQuestion($question, $registration)]);
        $this->questionRepository->persist($question);

        $trip = new Trip($this->country, $this->user, (new DateTime())->modify('+1 day'), (new DateTime())->modify('+2 days'));
        $trip->setRegistrations([new TripRegistration($trip, $registration)]);
        $trip->setRegistrationCheck(new DateTime());
        $trip->addAnswer(new TextAnswer($trip, $question, uniqid()));
        $this->tripRepository->persist($trip);

        // Another trip without answer
        $trip2 = new Trip($this->country, $this->user, (new DateTime())->modify('+1 day'), (new DateTime())->modify('+2 days'));
        $trip2->setRegistrations([new TripRegistration($trip2, $registration)]);
        $trip2->setRegistrationCheck(new DateTime());
        $this->tripRepository->persist($trip2);

        $response = $this->getRequest(sprintf('api/v1/trips/%s', $trip2->getId()))
            ->withMethod('GET')
            ->withHeader('Authorization', 'Bearer ' . $this->accessToken)
            ->execute(200);

        Assert::notNull($response);
        Assert::count(1, $response->answers);
    }

    function testTripGetNotPreFilledAnswer()
    {
        $registration = new Registration(uniqid());
        $this->registrationRepository->persist($registration);

        $question = new TextQuestion(uniqid(), $this->group);
        $question->setRegistrations([new RegistrationQuestion($question, $registration)]);
        $this->questionRepository->persist($question);

        $trip = new Trip($this->country, $this->user, (new DateTime())->modify('+1 day'), (new DateTime())->modify('+2 days'));
        $trip->setRegistrations([new TripRegistration($trip, $registration)]);
        $trip->setRegistrationCheck(new DateTime());
        $trip->addAnswer(new TextAnswer($trip, $question, uniqid()));
        $this->tripRepository->persist($trip);

        $response = $this->getRequest(sprintf('api/v1/trips/%s', $trip->getId()))
            ->withMethod('GET')
            ->withHeader('Authorization', 'Bearer ' . $this->accessToken)
            ->execute(200);

        Assert::notNull($response);
        Assert::count(1, $response->answers);

        // Another trip without answer
        $trip2 = new Trip($this->country, $this->user, (new DateTime())->modify('+1 day'), (new DateTime())->modify('+2 days'));
        $trip2->setRegistrations([new TripRegistration($trip2, $registration)]);
        $trip2->setRegistrationCheck(new DateTime());
        $this->tripRepository->persist($trip2);

        $response = $this->getRequest(sprintf('api/v1/trips/%s', $trip2->getId()))
            ->withMethod('GET')
            ->withHeader('Authorization', 'Bearer ' . $this->accessToken)
            ->execute(200);

        Assert::notNull($response);
        Assert::count(0, $response->answers);
    }

    protected function setUp()
    {
        parent::setUp();
        $this->user = $this->userRepository->findByUsername(BaseFixture::USER_NAME);
        $this->accessToken = $this->tokenProvider->getToken($this->user);

        $this->country = new Country('cz', 'Czech Republic', "icon");
        $this->country->setIsEnabled(true);
        $this->countryRepository->persist($this->country);

        $this->group = new QuestionGroup(uniqid(), QuestionGroup::QUESTION_GROUP_TYPE_DETAIL);
        $this->groupRepository->persist($this->group);
    }
}

$test = new TripAnswerPresenter($container);
$test->run();
