<?php
declare(strict_types=1);

namespace Test\Cleevio\Trips\Api;

$container = require __DIR__ . '/../../../bootstrap.php';

use Cleevio\Countries\Entities\Country;
use Cleevio\Countries\Repositories\ICountryRepository;
use Cleevio\Hosts\Repositories\IHostRepository;
use Cleevio\Registrations\Entities\Registration;
use Cleevio\Registrations\Repositories\IRegistrationRepository;
use Cleevio\RestApi\Auth\ITokenProvider;
use Cleevio\RestApi\Testing\RestTest;
use Cleevio\Trips\Entities\Trip;
use Cleevio\Trips\Repositories\ITripRepository;
use Cleevio\Users\Entities\User;
use Cleevio\Users\Repositories\IUserRepository;
use DateTime;
use Nette;
use Symfony\Component\Console\Application;
use Test\Cleevio\BaseFixture;
use Test\Cleevio\TestContext;
use Tester\Assert;

class TripRegistrationPresenter extends RestTest
{

    /**
     * @var ICountryRepository|object|null
     */
    private $countryRepository;

    /**
     * @var IUserRepository|object|null
     */
    private $userRepository;

    /**
     * @var ITokenProvider
     */
    private $tokenProvider;

    /**
     * @var ITripRepository|object|null
     */
    private $tripRepository;

    /**
     * @var IHostRepository|object|null
     */
    private $hostRepository;

    /**
     * @var IRegistrationRepository|object|null
     */
    private $registrationRepository;

    /**
     * @var Country
     */
    private $country;

    /**
     * @var User
     */
    private $user;

    /**
     * @var string
     */
    private $accessToken;

    /**
     * LoginPresenter constructor.
     * @param Nette\DI\Container $container
     */
    function __construct(Nette\DI\Container $container)
    {
        parent::__construct(new TestContext($container->getByType(Application::class)));

        $this->countryRepository = $container->getByType(ICountryRepository::class);
        $this->userRepository = $container->getByType(IUserRepository::class);
        $this->tokenProvider = $container->getByType(ITokenProvider::class);
        $this->tripRepository = $container->getByType(ITripRepository::class);
        $this->hostRepository = $container->getByType(IHostRepository::class);
        $this->registrationRepository = $container->getByType(IRegistrationRepository::class);
    }

    function testTripRegistrationGet()
    {
        $trip = new Trip(
            $this->country,
            $this->user,
            (new DateTime())->modify('+2 days'),
            (new DateTime())->modify('+3 days')
        );
        $trip->setHost(null);
        $this->tripRepository->persist($trip);

        $registration = new Registration("A1");
        $this->registrationRepository->persist($registration);

        $response = $this->getRequest('api/v1/trips/' . $trip->getId() . '/registration')
            ->withHeader('Authorization', 'Bearer ' . $this->accessToken)
            ->execute(200);

        Assert::notNull($response);
        Assert::notNull($response->registration->checked);
        Assert::count(1, $response->registration->registrations);
    }

    protected function setUp()
    {
        parent::setUp();
        $this->user = $this->userRepository->findByUsername(BaseFixture::USER_NAME);
        $this->accessToken = $this->tokenProvider->getToken($this->user);
        $this->country = new Country('cz', 'Czech Republic', "icon");
        $this->country->setIsEnabled(true);
        $this->countryRepository->persist($this->country);

    }
}

$test = new TripRegistrationPresenter($container);
$test->run();
