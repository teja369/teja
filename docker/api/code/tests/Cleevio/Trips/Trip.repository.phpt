<?php
declare(strict_types=1);

namespace Test\Cleevio\Trips;

$container = require __DIR__ . '/../../bootstrap.php';

use Cleevio\Countries\Entities\Country;
use Cleevio\Repository\RepositoryTest;
use Cleevio\Trips\Entities\Trip;
use Cleevio\Trips\Repositories\ITripRepository;
use Cleevio\Users\Repositories\IUserRepository;
use DateTime;
use Exception;
use Nette;
use Symfony\Component\Console\Application;
use Test\Cleevio\BaseFixture;
use Test\Cleevio\TestContext;

class TripRepositoryTest extends RepositoryTest
{

    /**
     * @var IUserRepository
     */
    private $userRepository;

    function __construct(Nette\DI\Container $container)
    {
        parent::__construct($container, new TestContext($container->getByType(Application::class)));
        $this->userRepository = $container->getByType(IUserRepository::class);
    }

    protected function getType(): string
    {
        return ITripRepository::class;
    }


    /**
     * Create and return new entity
     * @return Trip
     * @throws Exception
     */
    protected function getEntity(): Trip
    {
        $user = $this->userRepository->findByUsername(BaseFixture::USER_NAME);

        return new Trip(
            new Country('us', 'USA', "icon"),
            $user,
            new DateTime,
            new DateTime,
            Trip::TRIP_STATE_SUBMITTED
        );
    }
}

$test = new TripRepositoryTest($container);
$test->run();
