<?php
declare(strict_types=1);

namespace Test\Cleevio\Trips;

$container = require __DIR__ . '/../../bootstrap.php';

use Cleevio\Countries\Entities\Country;
use Cleevio\Countries\Repositories\ICountryRepository;
use Cleevio\Trips\Entities\Trip;
use Cleevio\Trips\ITripService;
use Cleevio\Trips\Repositories\ITripRepository;
use Cleevio\Users\Entities\User;
use Cleevio\Users\Repositories\IUserRepository;
use DateTime;
use Nette;
use Symfony\Component\Console\Application;
use Test\Cleevio\BaseFixture;
use Test\Cleevio\BaseTest;
use Test\Cleevio\TestContext;
use Tester\Assert;

class TripServiceCreateTrip extends BaseTest
{

    /**
     * @var ITripRepository|object|null
     */
    private $tripRepository;

    /**
     * @var ICountryRepository|object|null
     */
    private $countryRepository;

    /**
     * @var IUserRepository|object|null
     */
    private $userRepository;

    /**
     * @var ITripService|object|null
     */
    private $tripService;

    /**
     * @var Country
     */
    private $country;

    /**
     * @var User
     */
    private $user;


    /**
     * @param Nette\DI\Container $container
     */
    function __construct(Nette\DI\Container $container)
    {
        parent::__construct(new TestContext($container->getByType(Application::class)));

        $this->tripRepository = $container->getByType(ITripRepository::class);
        $this->countryRepository = $container->getByType(ICountryRepository::class);
        $this->userRepository = $container->getByType(IUserRepository::class);
        $this->tripService = $container->getByType(ITripService::class);
    }


    function testCreateTrip()
    {
        $trip = $this->tripService->createTrip(
            $this->country,
            $this->user,
            new DateTime(),
            (new DateTime())->modify('+ 2 days')
        );

        Assert::true($trip instanceof Trip);
        Assert::equal('DE', $trip->getCountry()->getCode());
        Assert::equal(Trip::TRIP_STATE_DRAFT, $trip->getState());
    }


    protected function setUp()
    {
        parent::setUp();

        $this->user = $this->userRepository->findByUsername(BaseFixture::USER_NAME);

        $this->country = new Country('DE', 'Germany', "icon");
        $this->country->setIsEnabled(true);
        $this->countryRepository->persist($this->country);

    }
}

$test = new TripServiceCreateTrip($container);
$test->run();
