<?php
declare(strict_types=1);

namespace Test\Cleevio\Countries;

$container = require __DIR__ . '/../../bootstrap.php';

use Cleevio\Countries\Entities\Country;
use Cleevio\Countries\ICountryService;
use Cleevio\Countries\Repositories\ICountryRepository;
use Nette;
use Symfony\Component\Console\Application;
use Test\Cleevio\BaseTest;
use Test\Cleevio\TestContext;
use Tester\Assert;

class CountryServiceFindByCode extends BaseTest
{

    /**
     * @var ICountryRepository|object|null
     */
    private $countryRepository;

    /**
     * @var ICountryService|object|null
     */
    private $countryService;

    /**
     * @var Country[]
     */
    private $country;

    /**
     * @param Nette\DI\Container $container
     */
    function __construct(Nette\DI\Container $container)
	{
		parent::__construct(new TestContext($container->getByType(Application::class)));

		$this->countryService = $container->getByType(ICountryService::class);
		$this->countryRepository = $container->getByType(ICountryRepository::class);
	}


	function testFindByCode()
	{
		$countryExists = $this->countryService->findByCode('DE');
		$countryNotExists = $this->countryService->findByCode('XY');
		Assert::true($countryExists instanceof Country);
		Assert::notNull($countryExists);
		Assert::null($countryNotExists);

	}

	protected function setUp()
	{
		parent::setUp();

		$this->countryRepository->beginTransaction();

		$this->country[] = new Country('DE', 'Germany', "icon");
		$this->country[] = new Country('US', 'United States', "icon");

		foreach ($this->country as $country) {
			$this->countryRepository->persist($country);
		}

	}


	protected function tearDown()
	{
		parent::tearDown();
		$this->countryRepository->rollbackTransaction();
	}
}

$test = new CountryServiceFindByCode($container);
$test->run();
