<?php
declare(strict_types=1);

namespace Test\Cleevio\Countries;

$container = require __DIR__ . '/../../bootstrap.php';

use Cleevio\Countries\Entities\Country;
use Cleevio\Countries\Entities\CountryTranslation;
use Cleevio\Countries\ICountryService;
use Cleevio\Countries\Repositories\ICountryRepository;
use Nette;
use Symfony\Component\Console\Application;
use Test\Cleevio\BaseTest;
use Test\Cleevio\TestContext;
use Tester\Assert;

class CountryServiceEnableCountry extends BaseTest
{

    /**
     * @var ICountryRepository|object|null
     */
    private $countryRepository;

    /**
     * @var ICountryService|object|null
     */
    private $countryService;

    /**
     * @var Country
     */
    private $countryDe;

    /**
     * @var Country
     */
    private $countryUs;

    /**
     * @param Nette\DI\Container $container
     */
    function __construct(Nette\DI\Container $container)
    {
        parent::__construct(new TestContext($container->getByType(Application::class)));

        $this->countryService = $container->getByType(ICountryService::class);
        $this->countryRepository = $container->getByType(ICountryRepository::class);
    }


    function testEnableCountry()
    {
        $this->countryService->enableCountry($this->countryDe);
        Assert::true($this->countryDe->isEnabled());
    }

    function testDisableCountry()
    {
        $this->countryService->disableCountry($this->countryUs);
        Assert::false($this->countryUs->isEnabled());
    }

    protected function setUp()
    {
        parent::setUp();

        $this->countryRepository->beginTransaction();

        $this->countryDe = new Country('DE', 'Germany');
        $this->countryDe->setIsEnabled(false);

        $this->countryUs = new Country('US', 'United States');

        $this->countryDe->addTranslation(new CountryTranslation('en', 'name', 'Germany'));

        $this->countryRepository->persist($this->countryDe);
        $this->countryRepository->persist($this->countryUs);
    }


    protected function tearDown()
    {
        parent::tearDown();
        $this->countryRepository->rollbackTransaction();
    }
}

$test = new CountryServiceEnableCountry($container);
$test->run();
