<?php
declare(strict_types=1);

namespace Test\Cleevio\Countries\Api;

$container = require __DIR__ . '/../../../bootstrap.php';

use Cleevio\Countries\Entities\Country;
use Cleevio\Countries\Entities\CountryTranslation;
use Cleevio\Countries\Repositories\ICountryRepository;
use Cleevio\RestApi\Auth\ITokenProvider;
use Cleevio\RestApi\Testing\RestTest;
use Cleevio\Users\Entities\User;
use Cleevio\Users\Repositories\IUserRepository;
use Nette;
use Symfony\Component\Console\Application;
use Test\Cleevio\BaseFixture;
use Test\Cleevio\TestContext;
use Tester\Assert;

class CountryPresenter extends RestTest
{

	/**
	 * @var ICountryRepository|object|null
	 */
	private $countryRepository;

	/**
	 * @var IUserRepository|null
	 */
	private $userRepository;

	/**
	 * @var ITokenProvider|null
	 */
	private $tokenProvider;

	/**
	 * @var Country[]
	 */
	private $countries = [];

	/**
	 * @var User|null
	 */
	private $user;

	/**
	 * @var string
	 */
	private $accessToken;

	/**
	 * @var string
	 */
	private $existingCountry;

	/**
	 * @var string
	 */
	private $translation;


	/**
	 * CountryPresenter constructor.
	 * @param Nette\DI\Container $container
	 */
	function __construct(Nette\DI\Container $container)
	{
		parent::__construct(new TestContext($container->getByType(Application::class)));

		$this->countryRepository = $container->getByType(ICountryRepository::class);
		$this->userRepository = $container->getByType(IUserRepository::class);
		$this->tokenProvider = $container->getByType(ITokenProvider::class);
	}


	function testCountriesGet()
	{
		$this->getRequest('api/v1/countries')
			->withMethod('GET')
			->withHeader('Authorization', 'Bearer ' . $this->accessToken)
			->execute(200);
	}


	function testCountriesGetSearchExists()
	{
		$responseExists = $this->getRequest(sprintf('api/v1/countries?search=%s', $this->translation))
			->withMethod('GET')
			->withHeader('Accept-Language', 'en')
			->withHeader('Authorization', 'Bearer ' . $this->accessToken)
			->execute(200);

		Assert::count(1, $responseExists->items);

	}


	function testCountriesGetSearchNotExists()
	{
		$responseNotExists = $this->getRequest('api/v1/countries?search=xfange')
			->withMethod('GET')
			->withHeader('Accept-Language', 'en')
			->withHeader('Authorization', 'Bearer ' . $this->accessToken)
			->execute(200);

		Assert::count(0, $responseNotExists->items);
	}

	function testCountriesEnable()
	{
		$response = $this->getRequest('api/v1/countries/CG/enable')
			->withMethod('PUT')
			->withHeader('Accept-Language', 'en')
			->withHeader('Authorization', 'Bearer ' . $this->accessToken)
			->execute(200);
	}


	function testCountriesDisable()
	{
		$response = $this->getRequest('api/v1/countries/AN/enable')
			->withMethod('DELETE')
			->withHeader('Accept-Language', 'en')
			->withHeader('Authorization', 'Bearer ' . $this->accessToken)
			->execute(200);
	}


	protected function setUp()
	{
		parent::setUp();

		$this->user = $this->userRepository->findByUsername(BaseFixture::USER_NAME);
		$this->accessToken = $this->tokenProvider->getToken($this->user);
		$this->existingCountry = uniqid();
		$this->translation = uniqid();

		$angola = new Country('AN', $this->existingCountry, "icon");
        $angola->setIsEnabled(true);
		$angola->addTranslation(new CountryTranslation('en', 'name', $this->translation));

		$disabledCongo = new Country('CG', uniqid(), 'icon');
		$disabledCongo->setIsEnabled(false);

		$this->countries[] = $angola;
		$this->countries[] = $disabledCongo;
		$this->countries[] = new Country('US', uniqid(), "icon");
		$this->countries[] = new Country('DE', uniqid(), "icon");

		foreach ($this->countries as $country) {
			$this->countryRepository->persist($country);
		}

	}

}

$test = new CountryPresenter($container);
$test->run();
