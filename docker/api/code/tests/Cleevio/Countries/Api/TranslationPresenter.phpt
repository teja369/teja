<?php
declare(strict_types=1);

namespace Test\Cleevio\Countries\Api;

$container = require __DIR__ . '/../../../bootstrap.php';

use Cleevio\Countries\Entities\Country;
use Cleevio\Countries\Repositories\ICountryRepository;
use Cleevio\Languages\Entities\Language;
use Cleevio\Languages\Repositories\ILanguageRepository;
use Cleevio\RestApi\Auth\ITokenProvider;
use Cleevio\RestApi\Testing\RestTest;
use Cleevio\Users\Entities\User;
use Cleevio\Users\Repositories\IUserRepository;
use Nette;
use Symfony\Component\Console\Application;
use Test\Cleevio\BaseFixture;
use Test\Cleevio\TestContext;
use Tester\Assert;

class TranslationPresenter extends RestTest
{
    /**
     * @var IUserRepository|null
     */
    private $userRepository;

    /**
     * @var ITokenProvider|null
     */
    private $tokenProvider;

    /**
     * @var User
     */
    private $user;

    /**
     * @var string
     */
    private $accessToken;

    /**
     * @var ILanguageRepository|object|null
     */
    private $languageRepository;

    /**
     * @var Language
     */
    private $language;

    /**
     * @var ICountryRepository|object|null
     */
    private $countryRepository;

    /**
     * @var Country
     */
    private $country;

    /**
     * CountryPresenter constructor.
     * @param Nette\DI\Container $container
     */
    function __construct(Nette\DI\Container $container)
    {
        parent::__construct(new TestContext($container->getByType(Application::class)));

        $this->userRepository = $container->getByType(IUserRepository::class);
        $this->tokenProvider = $container->getByType(ITokenProvider::class);
        $this->countryRepository = $container->getByType(ICountryRepository::class);
        $this->languageRepository = $container->getByType(ILanguageRepository::class);
    }

    function testGet()
    {
        $response = $this->getRequest(sprintf('api/v1/countries/%s/translation/%s', $this->country->getId(), $this->language->getCode()))
            ->withMethod('GET')
            ->withHeader('Authorization', 'Bearer ' . $this->accessToken)
            ->execute(200);

        Assert::notNull($response);
        Assert::same($this->country->getName($this->language->getCode()), $response->name);
    }

    function testPut()
    {
        $name = uniqid();

        $response = $this->getRequest(sprintf('api/v1/countries/%s/translation/%s', $this->country->getId(), $this->language->getCode()))
            ->withMethod('PUT')
            ->withBody([
                'name' => $name
            ])
            ->withHeader('Authorization', 'Bearer ' . $this->accessToken)
            ->execute(200);

        Assert::notNull($response);
        Assert::same($name, $response->name);
    }

    protected function setUp()
    {
        parent::setUp();

        $this->user = $this->userRepository->findByUsername(BaseFixture::USER_NAME);
        $this->accessToken = $this->tokenProvider->getToken($this->user);

        $this->country = new Country("US", uniqid(), uniqid());
        $this->countryRepository->persist($this->country);

        $this->language = new Language(uniqid(), "en", $this->country);
        $this->languageRepository->persist($this->language);
    }


}

$test = new TranslationPresenter($container);
$test->run();
