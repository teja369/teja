<?php
declare(strict_types=1);

namespace Test\Cleevio\Countries;

$container = require __DIR__ . '/../../bootstrap.php';

use Cleevio\Countries\Entities\Country;
use Cleevio\Countries\Repositories\ICountryRepository;
use Cleevio\Repository\RepositoryTest;
use Nette;
use Symfony\Component\Console\Application;
use Test\Cleevio\TestContext;
use Tester\Assert;

class CountryRepositoryTest extends RepositoryTest
{

    function __construct(Nette\DI\Container $container)
    {
        parent::__construct($container, new TestContext($container->getByType(Application::class)));
    }

    protected function getType(): string
    {
        return ICountryRepository::class;
    }

    public function testFindByCodes()
    {
        $entity1 = $this->getEntity();
        $entity2 = $this->getEntity();

        Assert::notNull($entity1);
        Assert::notNull($entity2);

        $this->repository->persist($entity1);
        $this->repository->persist($entity2);

        Assert::count(2, $this->repository->findByCodes([$entity1->getCode(), $entity2->getCode()]));
    }

    /**
     * Create and return new entity
     * @return Country
     */
    protected function getEntity(): Country
    {
        return new Country(substr(uniqid(), 0, 2), uniqid());
    }
}

$test = new CountryRepositoryTest($container);
$test->run();
