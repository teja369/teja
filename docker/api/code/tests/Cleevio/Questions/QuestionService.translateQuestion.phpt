<?php
declare(strict_types=1);

namespace Test\Cleevio\Questions;

$container = require __DIR__ . '/../../bootstrap.php';

use Cleevio\Countries\Entities\Country;
use Cleevio\Countries\Repositories\ICountryRepository;
use Cleevio\Languages\Entities\Language;
use Cleevio\Languages\Repositories\ILanguageRepository;
use Cleevio\Questions\Api\DTO\TranslationPutDTO;
use Cleevio\Questions\Entities\QuestionGroup;
use Cleevio\Questions\Entities\Questions\TextQuestion;
use Cleevio\Questions\IQuestionService;
use Cleevio\Questions\Repositories\IGroupRepository;
use Cleevio\Questions\Repositories\IQuestionRepository;
use Cleevio\Trips\Entities\Trip;
use Cleevio\Trips\ITripService;
use Cleevio\Trips\Repositories\ITripRepository;
use Cleevio\Users\Entities\User;
use Cleevio\Users\Repositories\IUserRepository;
use DateTime;
use Nette;
use Symfony\Component\Console\Application;
use Test\Cleevio\BaseFixture;
use Test\Cleevio\BaseTest;
use Test\Cleevio\TestContext;
use Tester\Assert;

class QuestionServiceTranslateQuestion extends BaseTest
{

    /**
     * @var IQuestionService|object|null
     */
    private $questionService;

    /**
     * @var QuestionGroup
     */
    private $group;

    /**
     * @var IGroupRepository|object|null
     */
    private $groupRepository;

    /**
     * @var IQuestionRepository|object|null
     */
    private $questionRepository;

    /**
     * @var ILanguageRepository|object|null
     */
    private $languageRepository;

    /**
     * @var TextQuestion
     */
    private $question;

    /**
     * @var Language
     */
    private $language;

    /**
     * @var ICountryRepository|object|null
     */
    private $countryRepository;

    /**
     * @param Nette\DI\Container $container
     */
    function __construct(Nette\DI\Container $container)
    {
        parent::__construct(new TestContext($container->getByType(Application::class)));

        $this->questionService = $container->getByType(IQuestionService::class);
        $this->groupRepository = $container->getByType(IGroupRepository::class);
        $this->questionRepository = $container->getByType(IQuestionRepository::class);
        $this->languageRepository = $container->getByType(ILanguageRepository::class);
        $this->countryRepository = $container->getByType(ICountryRepository::class);
    }

    function testTranslateQuestion()
    {
        $translation = new TranslationPutDTO(uniqid(), uniqid(), uniqid(), uniqid());

        $this->question = $this->questionService->translateQuestion($this->question, $this->language, $translation);

        Assert::same($translation->getText(), $this->question->getText($this->language->getCode()));
        Assert::same($translation->getHelp(), $this->question->getHelp($this->language->getCode()));
        Assert::same($translation->getPlaceholder(), $this->question->getPlaceholder($this->language->getCode()));
        Assert::same($translation->getValidationHelp(), $this->question->getValidationHelp($this->language->getCode()));
    }

    protected function setUp()
    {
        parent::setUp();

        $country = new Country('US', uniqid(), uniqid());
        $this->countryRepository->persist($country);

        $this->language = new Language(uniqid(), "en", $country);
        $this->languageRepository->persist($this->language);

        $this->group = new QuestionGroup(uniqid(), QuestionGroup::QUESTION_GROUP_TYPE_DETAIL);
        $this->groupRepository->persist($this->group);

        $this->question = new TextQuestion(uniqid(), $this->group);
        $this->questionRepository->persist($this->question);
    }
}

$test = new QuestionServiceTranslateQuestion($container);
$test->run();
