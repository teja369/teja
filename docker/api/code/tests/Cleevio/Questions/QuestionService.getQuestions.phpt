<?php
declare(strict_types=1);

namespace Test\Cleevio\Questions;

$container = require __DIR__ . '/../../bootstrap.php';

use Cleevio\Countries\Entities\Country;
use Cleevio\Countries\Repositories\ICountryRepository;
use Cleevio\Hosts\Entities\Host;
use Cleevio\Hosts\Repositories\IHostRepository;
use Cleevio\Questions\Entities\QuestionCountry;
use Cleevio\Questions\Entities\QuestionGroup;
use Cleevio\Questions\Entities\Questions\TextQuestion;
use Cleevio\Questions\IQuestionService;
use Cleevio\Questions\Repositories\IGroupRepository;
use Cleevio\Questions\Repositories\IQuestionRepository;
use Cleevio\Trips\Entities\Trip;
use Cleevio\Trips\ITripService;
use Cleevio\Trips\Repositories\ITripRepository;
use Cleevio\Users\Entities\User;
use Cleevio\Users\Repositories\IUserRepository;
use DateTime;
use Nette;
use Symfony\Component\Console\Application;
use Test\Cleevio\BaseFixture;
use Test\Cleevio\BaseTest;
use Test\Cleevio\TestContext;
use Tester\Assert;

class QuestionServiceGetQuestions extends BaseTest
{

    /**
     * @var IQuestionService|object|null
     */
    private $questionService;

    /**
     * @var IGroupRepository|object|null
     */
    private $groupRepository;

    /**
     * @var IQuestionRepository|object|null
     */
    private $questionRepository;

    /**
     * @var ICountryRepository|object|null
     */
    private $countryRepository;

    /**
     * @var IHostRepository|object|null
     */
    private $hostRepository;

    /**
     * @var QuestionGroup
     */
    private $group;

    /**
     * @var TextQuestion
     */
    private $question;

    /**
     * @var Country
     */
    private $country;

    /**
     * @var Country
     */
    private $countrySk;

    /**
     * @var Host
     */
    private $host;

    /**
     * @var Host
     */
    private $host2;

    /**
     * @param Nette\DI\Container $container
     */
    function __construct(Nette\DI\Container $container)
    {
        parent::__construct(new TestContext($container->getByType(Application::class)));

        $this->questionService = $container->getByType(IQuestionService::class);
        $this->groupRepository = $container->getByType(IGroupRepository::class);
        $this->questionRepository = $container->getByType(IQuestionRepository::class);
        $this->countryRepository = $container->getByType(ICountryRepository::class);
        $this->hostRepository = $container->getByType(IHostRepository::class);
    }

    function testGetQuestions()
    {
        $questions = $this->questionService->getQuestions($this->group, $this->country, [], $this->host, true);

        Assert::notNull($questions);
        Assert::count(1, $questions);
    }

    function testGetQuestionsNoEnabled()
    {
        $questions = $this->questionService->getQuestions($this->group, $this->country, [], $this->host, false);

        Assert::notNull($questions);
        Assert::count(0, $questions);
    }

    function testGetQuestionsNoCountry()
    {
        $questions = $this->questionService->getQuestions($this->group, $this->countrySk, [], $this->host, true);

        Assert::notNull($questions);
        Assert::count(0, $questions);
    }

    function testGetQuestionsNoHost()
    {
        $questions = $this->questionService->getQuestions($this->group, $this->countrySk, [], $this->host2, true);

        Assert::notNull($questions);
        Assert::count(0, $questions);
    }

    protected function setUp()
    {
        parent::setUp();

        $this->country = new Country('DE', 'Germany', "icon");
        $this->countryRepository->persist($this->country);

        $this->host = new Host(uniqid(), $this->country, Host::HOST_TYPE_NON_CLIENT);
        $this->hostRepository->persist($this->host);

        $this->host2 = new Host(uniqid(), $this->country, Host::HOST_TYPE_NON_CLIENT);
        $this->hostRepository->persist($this->host2);

        $this->countrySk = new Country('SK', 'Slovakia', "icon");
        $this->countryRepository->persist($this->countrySk);

        $this->group = new QuestionGroup(uniqid(), QuestionGroup::QUESTION_GROUP_TYPE_DETAIL);
        $this->groupRepository->persist($this->group);

        $this->question = new TextQuestion(uniqid(), $this->group);
        $this->question->setCountries([new QuestionCountry($this->country, $this->question)]);
        $this->questionRepository->persist($this->question);

    }
}

$test = new QuestionServiceGetQuestions($container);
$test->run();
