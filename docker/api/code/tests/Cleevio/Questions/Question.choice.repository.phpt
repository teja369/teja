<?php
declare(strict_types=1);

namespace Test\Cleevio\Questions;

$container = require __DIR__ . '/../../bootstrap.php';

use Cleevio\Questions\Entities\Question;
use Cleevio\Questions\Entities\QuestionGroup;
use Cleevio\Questions\Entities\Questions\ChoiceQuestion;
use Cleevio\Questions\Repositories\IGroupRepository;
use Cleevio\Questions\Repositories\IQuestionRepository;
use Cleevio\Repository\RepositoryTest;
use Nette;
use Symfony\Component\Console\Application;
use Test\Cleevio\TestContext;

class QuestionChoiceRepositoryTest extends RepositoryTest
{

    /**
     * @var IGroupRepository|object|null
     */
    private $groupRepository;

    function __construct(Nette\DI\Container $container)
    {
        parent::__construct($container, new TestContext($container->getByType(Application::class)));

        $this->groupRepository = $container->getByType(IGroupRepository::class);
    }

    protected function getType(): string
    {
        return IQuestionRepository::class;
    }


    /**
     * Create and return new entity
     * @return Question
     */
    protected function getEntity(): Question
    {
        $group = new QuestionGroup(uniqid(), QuestionGroup::QUESTION_GROUP_TYPE_DETAIL);
        $this->groupRepository->persist($group);

        return new ChoiceQuestion(uniqid(), $group);
    }
}

$test = new QuestionChoiceRepositoryTest($container);
$test->run();
