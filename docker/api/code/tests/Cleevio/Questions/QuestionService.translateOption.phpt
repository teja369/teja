<?php
declare(strict_types=1);

namespace Test\Cleevio\Questions;

$container = require __DIR__ . '/../../bootstrap.php';

use Cleevio\Countries\Entities\Country;
use Cleevio\Countries\Repositories\ICountryRepository;
use Cleevio\Languages\Entities\Language;
use Cleevio\Languages\Repositories\ILanguageRepository;
use Cleevio\Questions\Api\DTO\OptionTranslationPutDTO;
use Cleevio\Questions\Api\DTO\TranslationPutDTO;
use Cleevio\Questions\Entities\QuestionGroup;
use Cleevio\Questions\Entities\Questions\ChoiceQuestion;
use Cleevio\Questions\Entities\Questions\QuestionOption;
use Cleevio\Questions\Entities\Questions\TextQuestion;
use Cleevio\Questions\IQuestionService;
use Cleevio\Questions\Repositories\IGroupRepository;
use Cleevio\Questions\Repositories\IQuestionRepository;
use Cleevio\Trips\Entities\Trip;
use Cleevio\Trips\ITripService;
use Cleevio\Trips\Repositories\ITripRepository;
use Cleevio\Users\Entities\User;
use Cleevio\Users\Repositories\IUserRepository;
use DateTime;
use Nette;
use Symfony\Component\Console\Application;
use Test\Cleevio\BaseFixture;
use Test\Cleevio\BaseTest;
use Test\Cleevio\TestContext;
use Tester\Assert;

class QuestionServiceTranslateOption extends BaseTest
{

    /**
     * @var IQuestionService|object|null
     */
    private $questionService;

    /**
     * @var QuestionGroup
     */
    private $group;

    /**
     * @var IGroupRepository|object|null
     */
    private $groupRepository;

    /**
     * @var IQuestionRepository|object|null
     */
    private $questionRepository;

    /**
     * @var ILanguageRepository|object|null
     */
    private $languageRepository;

    /**
     * @var TextQuestion
     */
    private $question;

    /**
     * @var Language
     */
    private $language;

    /**
     * @var QuestionOption
     */
    private $option;

    /**
     * @var ICountryRepository|object|null
     */
    private $countryRepository;

    /**
     * @param Nette\DI\Container $container
     */
    function __construct(Nette\DI\Container $container)
    {
        parent::__construct(new TestContext($container->getByType(Application::class)));

        $this->questionService = $container->getByType(IQuestionService::class);
        $this->groupRepository = $container->getByType(IGroupRepository::class);
        $this->questionRepository = $container->getByType(IQuestionRepository::class);
        $this->languageRepository = $container->getByType(ILanguageRepository::class);
        $this->countryRepository = $container->getByType(ICountryRepository::class);
    }

    function testTranslateOption()
    {
        $translation = new OptionTranslationPutDTO(uniqid());

        $this->option = $this->questionService->translateOption($this->option, $this->language, $translation);

        Assert::same($translation->getText(), $this->option->getText($this->language->getCode()));
    }

    protected function setUp()
    {
        parent::setUp();

        $country = new Country('US', uniqid(), uniqid());
        $this->countryRepository->persist($country);

        $this->language = new Language(uniqid(), "en", $country);
        $this->languageRepository->persist($this->language);

        $this->group = new QuestionGroup(uniqid(), QuestionGroup::QUESTION_GROUP_TYPE_DETAIL);
        $this->groupRepository->persist($this->group);

        $this->question = new ChoiceQuestion(uniqid(), $this->group);
        $this->option = new QuestionOption($this->question, uniqid());
        $this->question->setOptions([
            $this->option,
            new QuestionOption($this->question, uniqid()),
            new QuestionOption($this->question, uniqid()),
        ]);
        $this->questionRepository->persist($this->question);
    }
}

$test = new QuestionServiceTranslateOption($container);
$test->run();
