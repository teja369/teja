<?php
declare(strict_types=1);

namespace Test\Cleevio\Questions;

$container = require __DIR__ . '/../../bootstrap.php';

use Cleevio\Countries\Entities\Country;
use Cleevio\Countries\Repositories\ICountryRepository;
use Cleevio\Questions\Api\DTO\QuestionVisibilityPutDTO;
use Cleevio\Questions\Entities\Question;
use Cleevio\Questions\Entities\QuestionCountry;
use Cleevio\Questions\Entities\QuestionGroup;
use Cleevio\Questions\Entities\Questions\TextQuestion;
use Cleevio\Questions\IQuestionService;
use Cleevio\Questions\Repositories\IGroupRepository;
use Cleevio\Questions\Repositories\IQuestionRepository;
use Cleevio\Trips\Entities\Trip;
use Cleevio\Trips\ITripService;
use Cleevio\Trips\Repositories\ITripRepository;
use Cleevio\Users\Entities\User;
use Cleevio\Users\Repositories\IUserRepository;
use DateTime;
use Nette;
use Symfony\Component\Console\Application;
use Test\Cleevio\BaseFixture;
use Test\Cleevio\BaseTest;
use Test\Cleevio\TestContext;
use Tester\Assert;

class QuestionServiceDisableQuestion extends BaseTest
{

    /**
     * @var IQuestionService|object|null
     */
    private $questionService;

    /**
     * @var IGroupRepository|object|null
     */
    private $groupRepository;

    /**
     * @var IQuestionRepository|object|null
     */
    private $questionRepository;

    /**
     * @var ICountryRepository|object|null
     */
    private $countryRepository;

    /**
     * @var QuestionGroup
     */
    private $group;

    /**
     * @var TextQuestion
     */
    private $question;

    /**
     * @var Country
     */
    private $country;

	/**
	 * @var QuestionVisibilityPutDTO
	 */
	private $questionVisibilityDTO;


	/**
     * @param Nette\DI\Container $container
     */
    function __construct(Nette\DI\Container $container)
    {
        parent::__construct(new TestContext($container->getByType(Application::class)));

        $this->questionService = $container->getByType(IQuestionService::class);
        $this->groupRepository = $container->getByType(IGroupRepository::class);
        $this->questionRepository = $container->getByType(IQuestionRepository::class);
        $this->countryRepository = $container->getByType(ICountryRepository::class);
    }

    function testDisableQuestion()
    {
        Assert::true($this->question->getVisibility() === Question::QUESTION_VISIBILITY_DISABLED);
        $this->questionService->setVisibility($this->question, $this->questionVisibilityDTO);

        $this->question = $this->questionRepository->find($this->question->getId());

        Assert::notNull($this->question);
        Assert::equal($this->question->getVisibility(), Question::QUESTION_VISIBILITY_ENABLED);
        Assert::notEqual($this->question->getVisibility(), Question::QUESTION_VISIBILITY_PREVIEW);

    }	

    protected function setUp()
    {
        parent::setUp();

        $this->country = new Country('DE', 'Germany', "icon");
        $this->countryRepository->persist($this->country);

        $this->group = new QuestionGroup(uniqid(), QuestionGroup::QUESTION_GROUP_TYPE_DETAIL);
        $this->groupRepository->persist($this->group);

        $this->questionVisibilityDTO = new QuestionVisibilityPutDTO(Question::QUESTION_VISIBILITY_ENABLED);

        $this->question = new TextQuestion(uniqid(), $this->group);
        $this->question->setCountries([new QuestionCountry($this->country, $this->question)]);
        $this->questionRepository->persist($this->question);

    }

}

$test = new QuestionServiceDisableQuestion($container);
$test->run();
