<?php
declare(strict_types=1);

namespace Test\Cleevio\Questions\Api;

$container = require __DIR__ . '/../../../bootstrap.php';

use Cleevio\Countries\Entities\Country;
use Cleevio\Countries\Repositories\ICountryRepository;
use Cleevio\Languages\Entities\Language;
use Cleevio\Languages\Repositories\ILanguageRepository;
use Cleevio\Questions\Entities\Question;
use Cleevio\Questions\Entities\QuestionCountry;
use Cleevio\Questions\Entities\QuestionGroup;
use Cleevio\Questions\Entities\Questions\BoolQuestion;
use Cleevio\Questions\Entities\Questions\ChoiceQuestion;
use Cleevio\Questions\Entities\Questions\NumberQuestion;
use Cleevio\Questions\Entities\Questions\TextQuestion;
use Cleevio\Questions\Repositories\IGroupRepository;
use Cleevio\Questions\Repositories\IQuestionRepository;
use Cleevio\Registrations\Entities\Registration;
use Cleevio\Registrations\Entities\RegistrationQuestion;
use Cleevio\Registrations\Repositories\IRegistrationRepository;
use Cleevio\RestApi\Auth\ITokenProvider;
use Cleevio\RestApi\Testing\RestTest;
use Cleevio\Trips\Entities\Trip;
use Cleevio\Trips\Entities\TripRegistration;
use Cleevio\Trips\Repositories\ITripRepository;
use Cleevio\Users\Entities\User;
use Cleevio\Users\Repositories\IUserRepository;
use DateTime;
use Nette;
use Symfony\Component\Console\Application;
use Test\Cleevio\BaseFixture;
use Test\Cleevio\TestContext;
use Tester\Assert;

class GroupTranslationPresenter extends RestTest
{
    /**
     * @var IUserRepository|null
     */
    private $userRepository;

    /**
     * @var ITokenProvider|null
     */
    private $tokenProvider;

    /**
     * @var User
     */
    private $user;

    /**
     * @var string
     */
    private $accessToken;

    /**
     * @var IGroupRepository|object|null
     */
    private $groupRepository;

    /**
     * @var ILanguageRepository|object|null
     */
    private $languageRepository;

    /**
     * @var QuestionGroup
     */
    private $group;


    /**
     * @var Language
     */
    private $language;

    /**
     * @var ICountryRepository|object|null
     */
    private $countryRepository;

    /**
     * CountryPresenter constructor.
     * @param Nette\DI\Container $container
     */
    function __construct(Nette\DI\Container $container)
    {
        parent::__construct(new TestContext($container->getByType(Application::class)));

        $this->userRepository = $container->getByType(IUserRepository::class);
        $this->tokenProvider = $container->getByType(ITokenProvider::class);
        $this->groupRepository = $container->getByType(IGroupRepository::class);
        $this->languageRepository = $container->getByType(ILanguageRepository::class);
        $this->countryRepository = $container->getByType(ICountryRepository::class);
    }

    function testGet()
    {
        $response = $this->getRequest(sprintf('api/v1/questions/groups/%s/translation/%s', $this->group->getId(), $this->language->getCode()))
            ->withMethod('GET')
            ->withHeader('Authorization', 'Bearer ' . $this->accessToken)
            ->execute(200);

        Assert::notNull($response);
        Assert::same($this->group->getName($this->language->getCode()), $response->name);
    }

    function testPut()
    {
        $name = uniqid();

        $response = $this->getRequest(sprintf('api/v1/questions/groups/%s/translation/%s', $this->group->getId(), $this->language->getCode()))
            ->withMethod('PUT')
            ->withBody([
                'name' => $name
            ])
            ->withHeader('Authorization', 'Bearer ' . $this->accessToken)
            ->execute(200);

        Assert::notNull($response);
        Assert::same($name, $response->name);
    }

    protected function setUp()
    {
        parent::setUp();

        $this->user = $this->userRepository->findByUsername(BaseFixture::USER_NAME);
        $this->accessToken = $this->tokenProvider->getToken($this->user);

        $country = new Country('US', uniqid(), uniqid());
        $this->countryRepository->persist($country);

        $this->language = new Language(uniqid(), "en", $country);
        $this->languageRepository->persist($this->language);

        $this->group = new QuestionGroup(uniqid(), QuestionGroup::QUESTION_GROUP_TYPE_DETAIL);
        $this->groupRepository->persist($this->group);
    }


}

$test = new GroupTranslationPresenter($container);
$test->run();
