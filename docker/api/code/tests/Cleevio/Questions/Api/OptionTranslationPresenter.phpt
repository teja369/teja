<?php
declare(strict_types=1);

namespace Test\Cleevio\Questions\Api;

$container = require __DIR__ . '/../../../bootstrap.php';

use Cleevio\Countries\Entities\Country;
use Cleevio\Countries\Repositories\ICountryRepository;
use Cleevio\Languages\Entities\Language;
use Cleevio\Languages\Repositories\ILanguageRepository;
use Cleevio\Questions\Entities\Question;
use Cleevio\Questions\Entities\QuestionCountry;
use Cleevio\Questions\Entities\QuestionGroup;
use Cleevio\Questions\Entities\Questions\BoolQuestion;
use Cleevio\Questions\Entities\Questions\ChoiceQuestion;
use Cleevio\Questions\Entities\Questions\NumberQuestion;
use Cleevio\Questions\Entities\Questions\QuestionOption;
use Cleevio\Questions\Entities\Questions\TextQuestion;
use Cleevio\Questions\Repositories\IGroupRepository;
use Cleevio\Questions\Repositories\IQuestionRepository;
use Cleevio\Registrations\Entities\Registration;
use Cleevio\Registrations\Entities\RegistrationQuestion;
use Cleevio\Registrations\Repositories\IRegistrationRepository;
use Cleevio\RestApi\Auth\ITokenProvider;
use Cleevio\RestApi\Testing\RestTest;
use Cleevio\Trips\Entities\Trip;
use Cleevio\Trips\Entities\TripRegistration;
use Cleevio\Trips\Repositories\ITripRepository;
use Cleevio\Users\Entities\User;
use Cleevio\Users\Repositories\IUserRepository;
use DateTime;
use Nette;
use Symfony\Component\Console\Application;
use Test\Cleevio\BaseFixture;
use Test\Cleevio\TestContext;
use Tester\Assert;

class OptionTranslationPresenter extends RestTest
{
    /**
     * @var IUserRepository|null
     */
    private $userRepository;

    /**
     * @var ITokenProvider|null
     */
    private $tokenProvider;

    /**
     * @var User
     */
    private $user;

    /**
     * @var string
     */
    private $accessToken;

    /**
     * @var IGroupRepository|object|null
     */
    private $groupRepository;

    /**
     * @var ILanguageRepository|object|null
     */
    private $languageRepository;

    /**
     * @var IQuestionRepository|object|null
     */
    private $questionRepository;

    /**
     * @var QuestionGroup
     */
    private $group;


    /**
     * @var Language
     */
    private $language;

    /**
     * @var ChoiceQuestion
     */
    private $question;

    /**
     * @var QuestionOption
     */
    private $option;

    /**
     * @var ICountryRepository|object|null
     */
    private $countryRepository;

    /**
     * CountryPresenter constructor.
     * @param Nette\DI\Container $container
     */
    function __construct(Nette\DI\Container $container)
    {
        parent::__construct(new TestContext($container->getByType(Application::class)));

        $this->userRepository = $container->getByType(IUserRepository::class);
        $this->tokenProvider = $container->getByType(ITokenProvider::class);
        $this->groupRepository = $container->getByType(IGroupRepository::class);
        $this->languageRepository = $container->getByType(ILanguageRepository::class);
        $this->questionRepository = $container->getByType(IQuestionRepository::class);
        $this->countryRepository = $container->getByType(ICountryRepository::class);
    }

    function testGet()
    {
        $response = $this->getRequest(sprintf('api/v1/questions/option/%s/translation/%s', $this->option->getId(), $this->language->getCode()))
            ->withMethod('GET')
            ->withHeader('Authorization', 'Bearer ' . $this->accessToken)
            ->execute(200);

        Assert::notNull($response);
        Assert::same($this->option->getText($this->language->getCode()), $response->text);
    }

    function testPut()
    {
        $text = uniqid();

        $response = $this->getRequest(sprintf('api/v1/questions/option/%s/translation/%s', $this->option->getId(), $this->language->getCode()))
            ->withMethod('PUT')
            ->withBody([
                'text' => $text
            ])
            ->withHeader('Authorization', 'Bearer ' . $this->accessToken)
            ->execute(200);

        Assert::notNull($response);
        Assert::same($text, $response->text);
    }

    protected function setUp()
    {
        parent::setUp();

        $this->user = $this->userRepository->findByUsername(BaseFixture::USER_NAME);
        $this->accessToken = $this->tokenProvider->getToken($this->user);

        $country = new Country('US', uniqid(), uniqid());
        $this->countryRepository->persist($country);

        $this->language = new Language(uniqid(), "en", $country);
        $this->languageRepository->persist($this->language);

        $this->group = new QuestionGroup(uniqid(), QuestionGroup::QUESTION_GROUP_TYPE_DETAIL);
        $this->groupRepository->persist($this->group);

        $this->question = new ChoiceQuestion(uniqid(), $this->group);
        $this->option = new QuestionOption($this->question, uniqid());
        $this->question->setOptions([
            $this->option,
            new QuestionOption($this->question, uniqid()),
            new QuestionOption($this->question, uniqid()),
        ]);
        $this->questionRepository->persist($this->question);
    }


}

$test = new OptionTranslationPresenter($container);
$test->run();
