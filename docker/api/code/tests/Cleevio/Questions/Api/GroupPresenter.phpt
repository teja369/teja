<?php
declare(strict_types=1);

namespace Test\Cleevio\Questions\Api;

$container = require __DIR__ . '/../../../bootstrap.php';

use Cleevio\Questions\Entities\QuestionGroup;
use Cleevio\Questions\Repositories\IGroupRepository;
use Cleevio\RestApi\Auth\ITokenProvider;
use Cleevio\RestApi\Testing\RestTest;
use Cleevio\Users\Entities\User;
use Cleevio\Users\Repositories\IUserRepository;
use Nette;
use Symfony\Component\Console\Application;
use Test\Cleevio\BaseFixture;
use Test\Cleevio\TestContext;
use Tester\Assert;

class GroupPresenter extends RestTest
{
    /**
     * @var IUserRepository|null
     */
    private $userRepository;

    /**
     * @var ITokenProvider|null
     */
    private $tokenProvider;

    /**
     * @var User
     */
    private $user;

    /**
     * @var string
     */
    private $accessToken;


    /**
     * @var IGroupRepository|object|null
     */
    private $groupRepository;

    /**
     * @var QuestionGroup
     */
    private $group;

    /**
     * CountryPresenter constructor.
     * @param Nette\DI\Container $container
     */
    function __construct(Nette\DI\Container $container)
    {
        parent::__construct(new TestContext($container->getByType(Application::class)));

        $this->userRepository = $container->getByType(IUserRepository::class);
        $this->tokenProvider = $container->getByType(ITokenProvider::class);
        $this->groupRepository = $container->getByType(IGroupRepository::class);
    }

    function testGroupsGet()
    {
        $response = $this->getRequest('api/v1/questions/groups/' . $this->group->getId())
            ->withMethod('GET')
            ->withHeader('Authorization', 'Bearer ' . $this->accessToken)
            ->execute(200);

        Assert::notNull($response);
        Assert::same($this->group->getName(null), $response->name);
    }

    function testGroupsList()
    {
        $response = $this->getRequest('api/v1/questions/groups')
            ->withMethod('GET')
            ->withHeader('Authorization', 'Bearer ' . $this->accessToken)
            ->execute(200);

        Assert::notNull($response);
        Assert::count(1, $response);
    }

    function testGroupsCreate()
    {
        $name = uniqid();

        $response = $this->getRequest('api/v1/questions/groups')
            ->withMethod('POST')
            ->withBody([
                'name' => $name,
                'type' => QuestionGroup::QUESTION_GROUP_TYPE_PURPOSE,
            ])
            ->withHeader('Authorization', 'Bearer ' . $this->accessToken)
            ->execute(200);

        Assert::notNull($response);
        Assert::same($name, $response->name);
    }

    function testGroupsPut()
    {
        $name = uniqid();

        $response = $this->getRequest('api/v1/questions/groups/' . $this->group->getId())
            ->withMethod('PUT')
            ->withBody([
                'name' => $name,
                'type' => QuestionGroup::QUESTION_GROUP_TYPE_PURPOSE,
            ])
            ->withHeader('Authorization', 'Bearer ' . $this->accessToken)
            ->execute(200);

        Assert::notNull($response);
        Assert::same($name, $response->name);
    }

    function testGroupsDelete()
    {
        $this->getRequest('api/v1/questions/groups/' . $this->group->getId())
            ->withMethod('DELETE')
            ->withHeader('Authorization', 'Bearer ' . $this->accessToken)
            ->execute(200);
    }

    protected function setUp()
    {
        parent::setUp();

        $this->user = $this->userRepository->findByUsername(BaseFixture::USER_NAME);
        $this->accessToken = $this->tokenProvider->getToken($this->user);

        $this->group = new QuestionGroup(uniqid(), QuestionGroup::QUESTION_GROUP_TYPE_DETAIL);
        $this->groupRepository->persist($this->group);
    }


}

$test = new GroupPresenter($container);
$test->run();
