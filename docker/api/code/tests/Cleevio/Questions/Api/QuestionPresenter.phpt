<?php
declare(strict_types=1);

namespace Test\Cleevio\Questions\Api;

$container = require __DIR__ . '/../../../bootstrap.php';

use Cleevio\Countries\Entities\Country;
use Cleevio\Countries\Repositories\ICountryRepository;
use Cleevio\Questions\Entities\Question;
use Cleevio\Questions\Entities\QuestionCountry;
use Cleevio\Questions\Entities\QuestionGroup;
use Cleevio\Questions\Entities\Questions\BoolQuestion;
use Cleevio\Questions\Entities\Questions\ChoiceQuestion;
use Cleevio\Questions\Entities\Questions\DateTimeQuestion;
use Cleevio\Questions\Entities\Questions\NumberQuestion;
use Cleevio\Questions\Entities\Questions\QuestionOption;
use Cleevio\Questions\Entities\Questions\QuestionOptionCountry;
use Cleevio\Questions\Entities\Questions\TextQuestion;
use Cleevio\Questions\Repositories\IGroupRepository;
use Cleevio\Questions\Repositories\IQuestionRepository;
use Cleevio\Registrations\Entities\Registration;
use Cleevio\Registrations\Entities\RegistrationQuestion;
use Cleevio\Registrations\Repositories\IRegistrationRepository;
use Cleevio\RestApi\Auth\ITokenProvider;
use Cleevio\RestApi\Testing\RestTest;
use Cleevio\Trips\Entities\Trip;
use Cleevio\Trips\Entities\TripRegistration;
use Cleevio\Trips\Repositories\ITripRepository;
use Cleevio\Users\Entities\User;
use Cleevio\Users\Repositories\IUserRepository;
use DateTime;
use Nette;
use Symfony\Component\Console\Application;
use Test\Cleevio\BaseFixture;
use Test\Cleevio\TestContext;
use Tester\Assert;

class QuestionPresenter extends RestTest
{
    /**
     * @var ITripRepository|null
     */
    private $tripRepository;

    /**
     * @var ICountryRepository|null
     */
    private $countryRepository;

    /**
     * @var IUserRepository|null
     */
    private $userRepository;

    /**
     * @var ITokenProvider|null
     */
    private $tokenProvider;

    /**
     * @var User
     */
    private $user;

    /**
     * @var string
     */
    private $accessToken;

    /**
     * @var Country
     */
    private $countryUs;

    /**
     * @var Country
     */
    private $countryCz;

    /**
     * @var object|IQuestionRepository|null
     */
    private $questionRepository;

    /**
     * @var IRegistrationRepository|object|null
     */
    private $registrationRepository;

    /**
     * @var Registration
     */
    private $registration;

    /**
     * @var IGroupRepository|object|null
     */
    private $groupRepository;

    /**
     * @var QuestionGroup
     */
    private $group;

    /**
     * @var QuestionGroup
     */
    private $group2;

    /**
     * @var TextQuestion
     */
    private $questionText;

    /**
     * @var ChoiceQuestion
     */
    private $choiceQuestion;

    /**
     * CountryPresenter constructor.
     * @param Nette\DI\Container $container
     */
    function __construct(Nette\DI\Container $container)
    {
        parent::__construct(new TestContext($container->getByType(Application::class)));

        $this->countryRepository = $container->getByType(ICountryRepository::class);
        $this->tripRepository = $container->getByType(ITripRepository::class);
        $this->userRepository = $container->getByType(IUserRepository::class);
        $this->tokenProvider = $container->getByType(ITokenProvider::class);
        $this->questionRepository = $container->getByType(IQuestionRepository::class);
        $this->registrationRepository = $container->getByType(IRegistrationRepository::class);
        $this->groupRepository = $container->getByType(IGroupRepository::class);
    }

    function testQuestionsGet()
    {
        $response = $this->getRequest('api/v1/questions/' . $this->questionText->getId())
            ->withMethod('GET')
            ->withHeader('Authorization', 'Bearer ' . $this->accessToken)
            ->execute(200);

        Assert::notNull($response);
        Assert::same($this->questionText->getId(), $response->id);
    }

    /**
     * Test getting multi choice question
     */
    function testQuestionOptionsGet()
    {
        $response = $this->getRequest('api/v1/questions/' . $this->choiceQuestion->getId())
            ->withMethod('GET')
            ->withHeader('Authorization', 'Bearer ' . $this->accessToken)
            ->execute(200);

        Assert::notNull($response);
        Assert::same($this->choiceQuestion->getId(), $response->id);
        Assert::count(4, (array)$response->choice->options);
    }

    function testQuestionsList()
    {
        $response = $this->getRequest('api/v1/questions')
            ->withMethod('GET')
            ->withHeader('Authorization', 'Bearer ' . $this->accessToken)
            ->execute(200);

        Assert::notNull($response);
        Assert::count(5, $response->items);
    }

    function testQuestionsListGroup()
    {
        $response = $this->getRequest('api/v1/questions?group=' . $this->group->getId())
            ->withMethod('GET')
            ->withHeader('Authorization', 'Bearer ' . $this->accessToken)
            ->execute(200);

        Assert::notNull($response);
        Assert::count(5, $response->items);
    }

    /**
     * Test that multi choice question returns filtered options for country
     */
    function testQuestionOptionsCountryGet()
    {
        $trip = new Trip($this->countryUs, $this->user, (new DateTime())->modify('+1 day'), (new DateTime())->modify('+2 days'));
        $trip->setRegistrationCheck(new DateTime());
        $trip->setRegistrations([new TripRegistration($trip, $this->registration)]);
        $trip->setHost(null);
        $this->tripRepository->persist($trip);

        $response = $this->getRequest('api/v1/questions/' . $this->choiceQuestion->getId() . '?trip=' . $trip->getId())
            ->withMethod('GET')
            ->withHeader('Authorization', 'Bearer ' . $this->accessToken)
            ->execute(200);

        Assert::notNull($response);
        Assert::same($this->choiceQuestion->getId(), $response->id);
        Assert::count(3, (array)$response->choice->options);
    }

    function testGroupQuestionsList()
    {
        $trip = new Trip($this->countryCz, $this->user, (new DateTime())->modify('+1 day'), (new DateTime())->modify('+2 days'));
        $trip->setRegistrationCheck(new DateTime());
        $trip->setRegistrations([new TripRegistration($trip, $this->registration)]);
        $trip->setHost(null);
        $this->tripRepository->persist($trip);

        $response = $this->getRequest('api/v1/questions/groups/' . $this->group->getId() . '/questions?trip=' . $trip->getId())
            ->withMethod('GET')
            ->withHeader('Authorization', 'Bearer ' . $this->accessToken)
            ->execute(200);

        Assert::notNull($response);
        Assert::count(3, $response);
    }

    function testGroupQuestionsListUnauthorized()
    {
        $trip = new Trip($this->countryCz, $this->user, new DateTime(), new DateTime());
        $this->tripRepository->persist($trip);

        $this->getRequest('api/v1/questions/groups/' . $this->group->getId() . '/questions?trip=' . $trip->getId())
            ->withMethod('GET')
            ->execute(401);
    }

    function testGroupQuestionsListWithoutRegistration()
    {
        $trip = new Trip($this->countryCz, $this->user, new DateTime(), new DateTime());
        $this->tripRepository->persist($trip);

        $this->getRequest('api/v1/questions/groups/' . $this->group->getId() . '/questions?trip=' . $trip->getId())
            ->withMethod('GET')
            ->withHeader('Authorization', 'Bearer ' . $this->accessToken)
            ->execute(400);
    }

    function testQuestionsCreate()
    {
        $response = $this->getRequest('api/v1/questions')
            ->withMethod('POST')
            ->withBody([
                'text' => uniqid(),
                'parent' => $this->questionText->getId(),
                'type' => Question::QUESTION_TYPE_TEXT,
                'group' => $this->group->getId(),
                'required' => true
            ])
            ->withHeader('Authorization', 'Bearer ' . $this->accessToken)
            ->execute(200);

        Assert::notNull($response);
        Assert::equal($response->visibility, Question::QUESTION_VISIBILITY_DISABLED);
    }

    function testQuestionsCreateUnique()
    {
        $this->getRequest('api/v1/questions')
            ->withMethod('POST')
            ->withBody([
                'text' => $this->questionText->getText(null),
                'parent' => $this->questionText->getId(),
                'type' => Question::QUESTION_TYPE_TEXT,
                'group' => $this->group->getId(),
                'required' => true
            ])
            ->withHeader('Authorization', 'Bearer ' . $this->accessToken)
            ->execute(400);
    }

    function testQuestionsCreateParentDifferentGroup()
    {
        $group = new QuestionGroup(uniqid(), QuestionGroup::QUESTION_GROUP_TYPE_DETAIL);
        $this->groupRepository->persist($group);

        $this->getRequest('api/v1/questions')
            ->withMethod('POST')
            ->withBody([
                'text' => uniqid(),
                'parent' => $this->questionText->getId(),
                'type' => Question::QUESTION_TYPE_TEXT,
                'group' => $group->getId(),
                'required' => true
            ])
            ->withHeader('Authorization', 'Bearer ' . $this->accessToken)
            ->execute(400);
    }

    function testQuestionsPut()
    {
        $text = uniqid();
        $response = $this->getRequest('api/v1/questions/' . $this->questionText->getId())
            ->withMethod('PUT')
            ->withBody([
                'text' => $text,
                'type' => Question::QUESTION_TYPE_TEXT,
                'group' => $this->group2->getId(),
                'required' => true
            ])
            ->withHeader('Authorization', 'Bearer ' . $this->accessToken)
            ->execute(200);

        Assert::notNull($response);
        Assert::same($text, $response->question);
        Assert::same($this->group2->getId(), $response->group->id);
    }

    function testQuestionsPutUnique()
    {
        $this->getRequest('api/v1/questions/' . $this->questionText->getId())
            ->withMethod('PUT')
            ->withBody([
                'text' => $this->choiceQuestion->getText(null),
                'type' => Question::QUESTION_TYPE_TEXT,
                'group' => $this->group->getId(),
                'required' => true
            ])
            ->withHeader('Authorization', 'Bearer ' . $this->accessToken)
            ->execute(400);
    }

    function testQuestionsPutParentCircular()
    {
        $this->getRequest('api/v1/questions/' . $this->questionText->getId())
            ->withMethod('PUT')
            ->withBody([
                'text' => uniqid(),
                'parent' => $this->questionText->getId(),
                'type' => Question::QUESTION_TYPE_TEXT,
                'group' => $this->group->getId(),
                'required' => true
            ])
            ->withHeader('Authorization', 'Bearer ' . $this->accessToken)
            ->execute(400);
    }

    function testQuestionsDelete()
    {
        $response = $this->getRequest('api/v1/questions/' . $this->questionText->getId())
            ->withMethod('DELETE')
            ->withHeader('Authorization', 'Bearer ' . $this->accessToken)
            ->execute(200);

        Assert::notNull($response);

        $this->getRequest('api/v1/questions/' . $this->questionText->getId())
            ->withMethod('DELETE')
            ->withHeader('Authorization', 'Bearer ' . $this->accessToken)
            ->execute(404);
    }

    function testQuestionsSetVisibility()
    {
        Assert::equal($this->questionText->getVisibility(), Question::QUESTION_VISIBILITY_ENABLED);

        $this->getRequest('api/v1/questions/' . $this->questionText->getId() . '/visibility')
            ->withMethod('PUT')
			->withBody(['visibility' => Question::QUESTION_VISIBILITY_DISABLED])
            ->withHeader('Authorization', 'Bearer ' . $this->accessToken)
            ->execute(200);
    }


    protected function setUp()
    {
        parent::setUp();

        $this->user = $this->userRepository->findByUsername(BaseFixture::USER_NAME);
        $this->accessToken = $this->tokenProvider->getToken($this->user);

        $this->countryUs = new Country('US', 'United states', "icon");
        $this->countryUs->setIsEnabled(true);
        $this->countryRepository->persist($this->countryUs);

        $this->countryCz = new Country('CZ', 'Czechia', "icon");
        $this->countryCz->setIsEnabled(true);
        $this->countryRepository->persist($this->countryCz);

        $this->group = new QuestionGroup(uniqid(), QuestionGroup::QUESTION_GROUP_TYPE_DETAIL);
        $this->groupRepository->persist($this->group);

        $this->group2 = new QuestionGroup(uniqid(), QuestionGroup::QUESTION_GROUP_TYPE_PURPOSE);
        $this->groupRepository->persist($this->group2);

        $this->questionText = new TextQuestion(uniqid(), $this->group);
        $this->questionText->setVisibility(Question::QUESTION_VISIBILITY_ENABLED);
        $this->questionText->setCountries([new QuestionCountry($this->countryCz, $this->questionText)]);
        $this->questionRepository->persist($this->questionText);

        $questionBool = new BoolQuestion(uniqid(), $this->group);
        $questionBool->setVisibility(Question::QUESTION_VISIBILITY_ENABLED);
        $questionBool->setCountries([new QuestionCountry($this->countryCz, $questionBool)]);
        $this->questionRepository->persist($questionBool);

        $numberQuestion = new NumberQuestion(uniqid(), $this->group);
        $numberQuestion->setVisibility(Question::QUESTION_VISIBILITY_ENABLED);
        $numberQuestion->setCountries([new QuestionCountry($this->countryUs, $numberQuestion)]);
        $this->questionRepository->persist($numberQuestion);

        $this->choiceQuestion = new ChoiceQuestion(uniqid(), $this->group);
        $this->choiceQuestion->setVisibility(Question::QUESTION_VISIBILITY_ENABLED);

        $questionOptionCz = new QuestionOption($this->choiceQuestion, uniqid());
        $questionOptionCz->setCountries([new QuestionOptionCountry($this->countryCz, $questionOptionCz)]);

        $this->choiceQuestion->setOptions([
            $questionOptionCz, // Option only for CZ
            new QuestionOption($this->choiceQuestion, uniqid()),
            new QuestionOption($this->choiceQuestion, uniqid()),
            new QuestionOption($this->choiceQuestion, uniqid()),
        ]);
        $this->choiceQuestion->setCountries([new QuestionCountry($this->countryUs, $this->choiceQuestion)]);
        $this->questionRepository->persist($this->choiceQuestion);

        $questionDatetime = new DateTimeQuestion(uniqid(), $this->group);
        $questionDatetime->setVisibility(Question::QUESTION_VISIBILITY_ENABLED);
        $questionDatetime->setCountries([new QuestionCountry($this->countryCz, $questionDatetime)]);
        $this->questionRepository->persist($questionDatetime);

        $this->registration = new Registration(uniqid());
        $this->registration->setQuestions([
            new RegistrationQuestion($this->questionText, $this->registration),
            new RegistrationQuestion($questionBool, $this->registration),
            new RegistrationQuestion($numberQuestion, $this->registration),
            new RegistrationQuestion($this->choiceQuestion, $this->registration),
        ]);
        $this->registrationRepository->persist($this->registration);
    }


}

$test = new QuestionPresenter($container);
$test->run();
