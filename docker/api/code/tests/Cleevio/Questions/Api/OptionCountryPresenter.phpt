<?php
declare(strict_types=1);

namespace Test\Cleevio\Questions\Api;

$container = require __DIR__ . '/../../../bootstrap.php';

use Cleevio\Countries\Entities\Country;
use Cleevio\Countries\Repositories\ICountryRepository;
use Cleevio\Questions\Entities\QuestionCountry;
use Cleevio\Questions\Entities\QuestionGroup;
use Cleevio\Questions\Entities\Questions\ChoiceQuestion;
use Cleevio\Questions\Entities\Questions\QuestionOption;
use Cleevio\Questions\Entities\Questions\QuestionOptionCountry;
use Cleevio\Questions\Entities\Questions\TextQuestion;
use Cleevio\Questions\Repositories\IGroupRepository;
use Cleevio\Questions\Repositories\IQuestionRepository;
use Cleevio\RestApi\Auth\ITokenProvider;
use Cleevio\RestApi\Testing\RestTest;
use Cleevio\Users\Entities\User;
use Cleevio\Users\Repositories\IUserRepository;
use Nette;
use Symfony\Component\Console\Application;
use Test\Cleevio\BaseFixture;
use Test\Cleevio\TestContext;
use Tester\Assert;

class OptionCountryPresenter extends RestTest
{
    /**
     * @var ICountryRepository|null
     */
    private $countryRepository;

    /**
     * @var IUserRepository|null
     */
    private $userRepository;

    /**
     * @var ITokenProvider|null
     */
    private $tokenProvider;

    /**
     * @var User
     */
    private $user;

    /**
     * @var string
     */
    private $accessToken;

    /**
     * @var Country
     */
    private $countryUs;

    /**
     * @var Country
     */
    private $countryCz;

    /**
     * @var object|IQuestionRepository|null
     */
    private $questionRepository;

    /**
     * @var IGroupRepository|object|null
     */
    private $groupRepository;

    /**
     * @var QuestionGroup
     */
    private $group;

    /**
     * @var TextQuestion
     */
    private $question;

    /**
     * @var QuestionOption
     */
    private $option;

    /**
     * CountryPresenter constructor.
     * @param Nette\DI\Container $container
     */
    function __construct(Nette\DI\Container $container)
    {
        parent::__construct(new TestContext($container->getByType(Application::class)));

        $this->countryRepository = $container->getByType(ICountryRepository::class);
        $this->userRepository = $container->getByType(IUserRepository::class);
        $this->tokenProvider = $container->getByType(ITokenProvider::class);
        $this->questionRepository = $container->getByType(IQuestionRepository::class);
        $this->groupRepository = $container->getByType(IGroupRepository::class);
    }

    function testList()
    {
        $response = $this->getRequest(sprintf('api/v1/questions/option/%s/country', $this->option->getId()))
            ->withMethod('GET')
            ->withHeader('Authorization', 'Bearer ' . $this->accessToken)
            ->execute(200);

        Assert::notNull($response);
        Assert::count(2, $response);
    }

    function testPut()
    {
        $response = $this->getRequest(sprintf('api/v1/questions/option/%s/country', $this->option->getId()))
            ->withMethod('PUT')
            ->withBody([
                [
                    'id' => $this->countryCz->getId()
                ]
            ])
            ->withHeader('Authorization', 'Bearer ' . $this->accessToken)
            ->execute(200);

        Assert::notNull($response);
        Assert::count(1, $response);
    }

    function testPutEmpty()
    {
        $response = $this->getRequest(sprintf('api/v1/questions/option/%s/country', $this->option->getId()))
            ->withMethod('PUT')
            ->withBody([])
            ->withHeader('Authorization', 'Bearer ' . $this->accessToken)
            ->execute(200);

        Assert::notNull($response);
        Assert::count(0, $response);
    }

    function testDelete()
    {
        $response = $this->getRequest(sprintf('api/v1/questions/option/%s/country/%s', $this->option->getId(), $this->countryCz->getCode()))
            ->withMethod('DELETE')
            ->withHeader('Authorization', 'Bearer ' . $this->accessToken)
            ->execute(200);

        Assert::notNull($response);
        Assert::count(1, $response);
    }

    function testDeleteNonExisting()
    {
        $this->getRequest(sprintf('api/v1/questions/option/%s/country/%s', $this->option->getId(), uniqid()))
            ->withMethod('DELETE')
            ->withHeader('Authorization', 'Bearer ' . $this->accessToken)
            ->execute(404);
    }

    protected function setUp()
    {
        parent::setUp();

        $this->user = $this->userRepository->findByUsername(BaseFixture::USER_NAME);
        $this->accessToken = $this->tokenProvider->getToken($this->user);

        $this->countryUs = new Country('US', 'United states', "icon");
        $this->countryRepository->persist($this->countryUs);

        $this->countryCz = new Country('CZ', 'Czechia', "icon");
        $this->countryRepository->persist($this->countryCz);

        $this->group = new QuestionGroup(uniqid(), QuestionGroup::QUESTION_GROUP_TYPE_DETAIL);
        $this->groupRepository->persist($this->group);

        $this->question = new ChoiceQuestion(uniqid(), $this->group);

        $this->option = new QuestionOption($this->question, uniqid());
        $this->option->setCountries([
            new QuestionOptionCountry($this->countryUs, $this->option),
            new QuestionOptionCountry($this->countryCz, $this->option)
        ]);
        $this->question->setOptions([$this->option]);

        $this->questionRepository->persist($this->question);
    }


}

$test = new OptionCountryPresenter($container);
$test->run();
