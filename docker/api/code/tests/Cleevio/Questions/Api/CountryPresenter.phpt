<?php
declare(strict_types=1);

namespace Test\Cleevio\Questions\Api;

$container = require __DIR__ . '/../../../bootstrap.php';

use Cleevio\Countries\Entities\Country;
use Cleevio\Countries\Repositories\ICountryRepository;
use Cleevio\Questions\Entities\QuestionCountry;
use Cleevio\Questions\Entities\QuestionGroup;
use Cleevio\Questions\Entities\Questions\TextQuestion;
use Cleevio\Questions\Repositories\IGroupRepository;
use Cleevio\Questions\Repositories\IQuestionRepository;
use Cleevio\RestApi\Auth\ITokenProvider;
use Cleevio\RestApi\Testing\RestTest;
use Cleevio\Users\Entities\User;
use Cleevio\Users\Repositories\IUserRepository;
use Nette;
use Symfony\Component\Console\Application;
use Test\Cleevio\BaseFixture;
use Test\Cleevio\TestContext;
use Tester\Assert;

class CountryPresenter extends RestTest
{
    /**
     * @var ICountryRepository|null
     */
    private $countryRepository;

    /**
     * @var IUserRepository|null
     */
    private $userRepository;

    /**
     * @var ITokenProvider|null
     */
    private $tokenProvider;

    /**
     * @var User
     */
    private $user;

    /**
     * @var string
     */
    private $accessToken;

    /**
     * @var Country
     */
    private $countryUs;

    /**
     * @var Country
     */
    private $countryCz;

    /**
     * @var object|IQuestionRepository|null
     */
    private $questionRepository;

    /**
     * @var IGroupRepository|object|null
     */
    private $groupRepository;

    /**
     * @var QuestionGroup
     */
    private $group;

    /**
     * @var TextQuestion
     */
    private $question;

    /**
     * CountryPresenter constructor.
     * @param Nette\DI\Container $container
     */
    function __construct(Nette\DI\Container $container)
    {
        parent::__construct(new TestContext($container->getByType(Application::class)));

        $this->countryRepository = $container->getByType(ICountryRepository::class);
        $this->userRepository = $container->getByType(IUserRepository::class);
        $this->tokenProvider = $container->getByType(ITokenProvider::class);
        $this->questionRepository = $container->getByType(IQuestionRepository::class);
        $this->groupRepository = $container->getByType(IGroupRepository::class);
    }

    function testList()
    {
        $response = $this->getRequest(sprintf('api/v1/questions/%s/country', $this->question->getId()))
            ->withMethod('GET')
            ->withHeader('Authorization', 'Bearer ' . $this->accessToken)
            ->execute(200);

        Assert::notNull($response);
        Assert::count(2, $response);
    }

    function testPut()
    {
        $response = $this->getRequest(sprintf('api/v1/questions/%s/country', $this->question->getId()))
            ->withMethod('PUT')
            ->withBody([
                [
                    'id' => $this->countryCz->getId()
                ]
            ])
            ->withHeader('Authorization', 'Bearer ' . $this->accessToken)
            ->execute(200);

        Assert::notNull($response);
        Assert::count(1, $response);
    }

    function testPutEmpty()
    {
        $response = $this->getRequest(sprintf('api/v1/questions/%s/country', $this->question->getId()))
            ->withMethod('PUT')
            ->withBody([])
            ->withHeader('Authorization', 'Bearer ' . $this->accessToken)
            ->execute(200);

        Assert::notNull($response);
        Assert::count(0, $response);
    }

    protected function setUp()
    {
        parent::setUp();

        $this->user = $this->userRepository->findByUsername(BaseFixture::USER_NAME);
        $this->accessToken = $this->tokenProvider->getToken($this->user);

        $this->countryUs = new Country('US', 'United states', "icon");
        $this->countryRepository->persist($this->countryUs);

        $this->countryCz = new Country('CZ', 'Czechia', "icon");
        $this->countryRepository->persist($this->countryCz);

        $this->group = new QuestionGroup(uniqid(), QuestionGroup::QUESTION_GROUP_TYPE_DETAIL);
        $this->groupRepository->persist($this->group);

        $this->question = new TextQuestion(uniqid(), $this->group);
        $this->question->setCountries([
            new QuestionCountry($this->countryUs, $this->question),
            new QuestionCountry($this->countryCz, $this->question)
        ]);
        $this->questionRepository->persist($this->question);
    }


}

$test = new CountryPresenter($container);
$test->run();
