<?php
declare(strict_types=1);

namespace Test\Cleevio\Questions;

$container = require __DIR__ . '/../../bootstrap.php';

use Cleevio\Countries\Entities\Country;
use Cleevio\Countries\Repositories\ICountryRepository;
use Cleevio\Questions\Entities\QuestionGroup;
use Cleevio\Questions\IQuestionService;
use Cleevio\Questions\Repositories\IGroupRepository;
use Cleevio\Trips\Entities\Trip;
use Cleevio\Trips\ITripService;
use Cleevio\Trips\Repositories\ITripRepository;
use Cleevio\Users\Entities\User;
use Cleevio\Users\Repositories\IUserRepository;
use DateTime;
use Nette;
use Symfony\Component\Console\Application;
use Test\Cleevio\BaseFixture;
use Test\Cleevio\BaseTest;
use Test\Cleevio\TestContext;
use Tester\Assert;

class QuestionServiceCreateGroup extends BaseTest
{

    /**
     * @var IQuestionService|object|null
     */
    private $questionService;

    /**
     * @var IGroupRepository|object|null
     */
    private $groupRepository;

    /**
     * @param Nette\DI\Container $container
     */
    function __construct(Nette\DI\Container $container)
    {
        parent::__construct(new TestContext($container->getByType(Application::class)));

        $this->questionService = $container->getByType(IQuestionService::class);
        $this->groupRepository = $container->getByType(IGroupRepository::class);
    }

    function testGetGroup()
    {
        $name = uniqid();
        $group = $this->questionService->createGroup($name, QuestionGroup::QUESTION_GROUP_TYPE_DETAIL);

        Assert::true($group instanceof QuestionGroup);
        Assert::equal($name, $group->getName(null));
    }

    function testGetGroupInvalidType()
    {
        Assert::exception(function () {
            $this->questionService->createGroup(uniqid(), uniqid());
        }, \InvalidArgumentException::class);
    }
}

$test = new QuestionServiceCreateGroup($container);
$test->run();
