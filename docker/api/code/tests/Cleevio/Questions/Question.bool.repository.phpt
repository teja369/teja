<?php
declare(strict_types=1);

namespace Test\Cleevio\Questions;

$container = require __DIR__ . '/../../bootstrap.php';

use Cleevio\Questions\Entities\Question;
use Cleevio\Questions\Entities\QuestionGroup;
use Cleevio\Questions\Entities\Questions\BoolQuestion;
use Cleevio\Questions\Entities\Questions\TextQuestion;
use Cleevio\Questions\Repositories\IGroupRepository;
use Cleevio\Questions\Repositories\IQuestionRepository;
use Cleevio\Repository\RepositoryTest;
use Nette;
use Symfony\Component\Console\Application;
use Test\Cleevio\TestContext;
use Tester\Assert;

class QuestionBoolRepositoryTest extends RepositoryTest
{

    /**
     * @var object|Application|null
     */
    private $groupRepository;

    function __construct(Nette\DI\Container $container)
    {
        parent::__construct($container, new TestContext($container->getByType(Application::class)));

        $this->groupRepository = $container->getByType(IGroupRepository::class);
    }

    protected function getType(): string
    {
        return IQuestionRepository::class;
    }


    public function testFindByIds()
    {
        $entity1 = $this->getEntity();
        $entity2 = $this->getEntity();

        Assert::notNull($entity1);
        Assert::notNull($entity2);

        $this->repository->persist($entity1);
        $this->repository->persist($entity2);

        Assert::count(2, $this->repository->findByIds([$entity1->getId(), $entity2->getId()]));
    }

    /**
     * Create and return new entity
     * @return Question
     */
    protected function getEntity(): Question
    {
        $group = new QuestionGroup(uniqid(), QuestionGroup::QUESTION_GROUP_TYPE_DETAIL);
        $this->groupRepository->persist($group);

        return new BoolQuestion(uniqid(), $group);
    }
}

$test = new QuestionBoolRepositoryTest($container);
$test->run();
