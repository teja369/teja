<?php
declare(strict_types=1);

namespace Test\Cleevio\Questions;

$container = require __DIR__ . '/../../bootstrap.php';

use Cleevio\Countries\Entities\Country;
use Cleevio\Countries\Repositories\ICountryRepository;
use Cleevio\Questions\Entities\QuestionGroup;
use Cleevio\Questions\IQuestionService;
use Cleevio\Questions\Repositories\IGroupRepository;
use Cleevio\Trips\Entities\Trip;
use Cleevio\Trips\ITripService;
use Cleevio\Trips\Repositories\ITripRepository;
use Cleevio\Users\Entities\User;
use Cleevio\Users\Repositories\IUserRepository;
use DateTime;
use Nette;
use Symfony\Component\Console\Application;
use Test\Cleevio\BaseFixture;
use Test\Cleevio\BaseTest;
use Test\Cleevio\TestContext;
use Tester\Assert;

class QuestionServiceGetGroups extends BaseTest
{

    /**
     * @var IQuestionService|object|null
     */
    private $questionService;

    /**
     * @var QuestionGroup
     */
    private $group;

    /**
     * @var IGroupRepository|object|null
     */
    private $groupRepository;

    /**
     * @param Nette\DI\Container $container
     */
    function __construct(Nette\DI\Container $container)
    {
        parent::__construct(new TestContext($container->getByType(Application::class)));

        $this->questionService = $container->getByType(IQuestionService::class);
        $this->groupRepository = $container->getByType(IGroupRepository::class);
    }

    function testGetGroups()
    {
        $groups = $this->questionService->getGroups(null, QuestionGroup::QUESTION_GROUP_TYPE_DETAIL);

        Assert::notNull($groups);
        Assert::count(1, $groups);
    }

    function testGetGroupsEmpty()
    {
        $groups = $this->questionService->getGroups(null, QuestionGroup::QUESTION_GROUP_TYPE_PURPOSE);

        Assert::notNull($groups);
        Assert::count(0, $groups);
    }

    protected function setUp()
    {
        parent::setUp();

        $this->group = new QuestionGroup(uniqid(), QuestionGroup::QUESTION_GROUP_TYPE_DETAIL);
        $this->groupRepository->persist($this->group);
    }
}

$test = new QuestionServiceGetGroups($container);
$test->run();
