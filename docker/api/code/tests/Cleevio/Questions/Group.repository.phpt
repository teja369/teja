<?php
declare(strict_types=1);

namespace Test\Cleevio\Questions;

$container = require __DIR__ . '/../../bootstrap.php';

use Cleevio\Questions\Entities\Question;
use Cleevio\Questions\Entities\QuestionGroup;
use Cleevio\Questions\Entities\Questions\BoolQuestion;
use Cleevio\Questions\Entities\Questions\TextQuestion;
use Cleevio\Questions\Repositories\IGroupRepository;
use Cleevio\Questions\Repositories\IQuestionRepository;
use Cleevio\Repository\RepositoryTest;
use Nette;
use Symfony\Component\Console\Application;
use Test\Cleevio\TestContext;
use Tester\Assert;

class GroupRepositoryTest extends RepositoryTest
{

    function __construct(Nette\DI\Container $container)
    {
        parent::__construct($container, new TestContext($container->getByType(Application::class)));
    }

    protected function getType(): string
    {
        return IGroupRepository::class;
    }

    /**
     * Create and return new entity
     * @return QuestionGroup
     */
    protected function getEntity(): QuestionGroup
    {
        return new QuestionGroup(uniqid(), QuestionGroup::QUESTION_GROUP_TYPE_DETAIL);
    }
}

$test = new GroupRepositoryTest($container);
$test->run();
