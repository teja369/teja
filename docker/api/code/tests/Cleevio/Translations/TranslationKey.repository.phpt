<?php
declare(strict_types=1);

namespace Test\Cleevio\Translations;

$container = require __DIR__ . '/../../bootstrap.php';

use Cleevio\Languages\Entities\TranslationKey;
use Cleevio\Languages\Repositories\TranslationKeyRepository;
use Cleevio\Repository\RepositoryTest;
use Nette;
use Symfony\Component\Console\Application;
use Test\Cleevio\TestContext;

class TranslationKeyRepositoryTest extends RepositoryTest
{

    function __construct(Nette\DI\Container $container)
    {
        parent::__construct($container, new TestContext($container->getByType(Application::class)));
    }

    protected function getType(): string
    {
        return TranslationKeyRepository::class;
    }

    protected function getEntity()
    {
        return new TranslationKey(uniqid());
    }

}

$test = new TranslationKeyRepositoryTest($container);
$test->run();
