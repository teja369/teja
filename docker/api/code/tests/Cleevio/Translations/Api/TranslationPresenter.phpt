<?php
declare(strict_types=1);

namespace Test\Cleevio\Translations\Api;

$container = require __DIR__ . '/../../../bootstrap.php';

use Cleevio\Countries\Entities\Country;
use Cleevio\Countries\Repositories\ICountryRepository;
use Cleevio\Languages\Entities\Language;
use Cleevio\Languages\Entities\Translation;
use Cleevio\Languages\Entities\TranslationKey;
use Cleevio\Languages\Repositories\ILanguageRepository;
use Cleevio\Languages\Repositories\ITranslationKeyRepository;
use Cleevio\Languages\Repositories\ITranslationRepository;
use Cleevio\RestApi\Auth\ITokenProvider;
use Cleevio\RestApi\Testing\RestTest;
use Cleevio\Users\Entities\User;
use Cleevio\Users\Repositories\IUserRepository;
use Nette;
use Symfony\Component\Console\Application;
use Test\Cleevio\BaseFixture;
use Test\Cleevio\TestContext;
use Tester\Assert;

class TranslationPresenter extends RestTest
{

    /**
     * @var ITranslationRepository
     */
    private $translationRepository;

    /**
     * @var ILanguageRepository
     */
    private $languageRepository;

    /**
     * @var Translation
     */
    private $translation;

    /**
     * @var Language
     */
    private $language;

    /**
     * @var ITranslationKeyRepository|object|null
     */
    private $translationKeyRepository;

    /**
     * @var TranslationKey
     */
    private $translationKey;

    /**
     * @var IUserRepository|object|null
     */
    private $userRepository;

    /**
     * @var ITokenProvider|object|null
     */
    private $tokenProvider;


    /**
     * @var User|null
     */
    private $user;

    /**
     * @var string
     */
    private $accessToken;

    /**
     * @var ICountryRepository|object|null
     */
    private $countryRepository;

    /**
     * @var Country
     */
    private $country;

    /**
     * LoginPresenter constructor.
     * @param Nette\DI\Container $container
     */
    function __construct(Nette\DI\Container $container)
    {
        parent::__construct(new TestContext($container->getByType(Application::class)));

        $this->userRepository = $container->getByType(IUserRepository::class);
        $this->tokenProvider = $container->getByType(ITokenProvider::class);
        $this->translationRepository = $container->getByType(ITranslationRepository::class);
        $this->languageRepository = $container->getByType(ILanguageRepository::class);
        $this->translationKeyRepository = $container->getByType(ITranslationKeyRepository::class);
        $this->countryRepository = $container->getByType(ICountryRepository::class);
    }


    function testTranslationsGet()
    {
        $response = $this->getRequest('api/v1/translations/' . $this->language->getCode())
            ->execute(200);

        Assert::notNull($response);
        Assert::count(1, $response);
    }

    function testTranslationKeysGet()
    {
        $response = $this->getRequest('api/v1/translations/keys')
            ->withHeader('Authorization', 'Bearer ' . $this->accessToken)
            ->execute(200);

        Assert::notNull($response);
        Assert::count(1, $response);
    }

    function testTranslationKeysGetUnauthorized()
    {
        $this->getRequest('api/v1/translations/keys')
            ->execute(401);
    }

    function testTranslationsCreate()
    {
        $this->getRequest('api/v1/translations/' . $this->language->getCode())
            ->withMethod('POST')
            ->withHeader('Authorization', 'Bearer ' . $this->accessToken)
            ->withBody([
                [
                    'key' => $this->translationKey->getKey(),
                    'value' => uniqid()
                ]
            ])
            ->execute(200);
    }

    function testTranslationsCreateUnauthorized()
    {
        $this->getRequest('api/v1/translations/' . $this->language->getCode())
            ->withMethod('POST')
            ->withBody([
                [
                    'key' => $this->translationKey->getKey(),
                    'value' => uniqid()
                ]
            ])
            ->execute(401);
    }

    function testTranslationsCreateInvalidKey()
    {
        $this->getRequest('api/v1/translations/' . $this->language->getCode())
            ->withHeader('Authorization', 'Bearer ' . $this->accessToken)
            ->withMethod('POST')
            ->withBody([
                [
                    'key' => uniqid(),
                    'value' => uniqid()
                ]
            ])
            ->execute(400);
    }

    function testTranslationsPut()
    {
        $this->getRequest('api/v1/translations/' . $this->language->getCode())
            ->withMethod('PUT')
            ->withHeader('Authorization', 'Bearer ' . $this->accessToken)
            ->withBody([
                [
                    'key' => $this->translationKey->getKey(),
                    'value' => uniqid()
                ]
            ])
            ->execute(200);
    }

    function testTranslationsPutUnauthorized()
    {
        $this->getRequest('api/v1/translations/' . $this->language->getCode())
            ->withMethod('PUT')
            ->withBody([
                [
                    'key' => $this->translationKey->getKey(),
                    'value' => uniqid()
                ]
            ])
            ->execute(401);
    }

    function testTranslationsPutInvalidKey()
    {
        $this->getRequest('api/v1/translations/' . $this->language->getCode())
            ->withHeader('Authorization', 'Bearer ' . $this->accessToken)
            ->withMethod('PUT')
            ->withBody([
                [
                    'key' => uniqid(),
                    'value' => uniqid()
                ]
            ])
            ->execute(400);
    }


    protected function setUp()
    {
        parent::setUp();

        $this->user = $this->userRepository->findByUsername(BaseFixture::USER_NAME);
        $this->accessToken = $this->tokenProvider->getToken($this->user);

        $this->country = new Country('US', uniqid(), uniqid());
        $this->countryRepository->persist($this->country);

        $this->language = new Language(uniqid(), uniqid(), $this->country);

        $this->translationKey = new TranslationKey(uniqid());
        $this->translationKeyRepository->persist($this->translationKey);

        $this->translation = new Translation($this->translationKey, $this->language, uniqid());

        $this->translationRepository->persist($this->translation);
    }
}

$test = new TranslationPresenter($container);
$test->run();
