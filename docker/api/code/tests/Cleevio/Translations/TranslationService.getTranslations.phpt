<?php
declare(strict_types=1);

namespace Test\Cleevio\Translations;

$container = require __DIR__ . '/../../bootstrap.php';

use Cleevio\Countries\Entities\Country;
use Cleevio\Countries\Repositories\ICountryRepository;
use Cleevio\Languages\Entities\Language;
use Cleevio\Languages\Entities\Translation;
use Cleevio\Languages\Entities\TranslationKey;
use Cleevio\Languages\ITranslationService;
use Cleevio\Languages\Repositories\ILanguageRepository;
use Cleevio\Languages\Repositories\ITranslationRepository;
use Cleevio\Languages\Repositories\TranslationConstraints;
use Nette;
use Symfony\Component\Console\Application;
use Test\Cleevio\BaseTest;
use Test\Cleevio\TestContext;
use Tester\Assert;

class TranslationsServiceGetTranslations extends BaseTest
{

    /**
     * @var ITranslationService|null
     */
    private $translationService;

    /**
     * @var ITranslationRepository
     */
    private $translationRepository;

    /**
     * @var ILanguageRepository
     */
    private $languageRepository;

    /**
     * @var ICountryRepository|object|null
     */
    private $countryRepository;

    /**
     * @var Translation
     */
    private $translationEn;

    /**
     * @var Language
     */
    private $languageEn;

    /**
     * @var Translation
     */
    private $translationCz;

    /**
     * @var Language
     */
    private $languageCz;

    /**
     * @var Country
     */
    private $country;


    function __construct(Nette\DI\Container $container)
    {
        parent::__construct(new TestContext($container->getByType(Application::class)));

        $this->translationService = $container->getByType(ITranslationService::class);
        $this->translationRepository = $container->getByType(ITranslationRepository::class);
        $this->languageRepository = $container->getByType(ILanguageRepository::class);
        $this->countryRepository = $container->getByType(ICountryRepository::class);
    }


    function testGetTranslations()
    {
        $constraints = new TranslationConstraints;
        $constraints->setCode('cz');

        $translations = $this->translationService->getTranslations($constraints);
        Assert::count(1, $translations);
    }


    function testGetTranslationNonExistingCode()
    {
        $constraints = new TranslationConstraints;
        $constraints->setCode('xy');

        $translations = $this->translationService->getTranslations($constraints);
        Assert::count(0, $translations);
    }


    protected function setUp()
    {
        parent::setUp();

        $this->country = new Country('US', uniqid(), uniqid());
        $this->countryRepository->persist($this->country);

        $this->languageEn = new Language('English', 'en', $this->country);
        $this->languageCz = new Language('Czech', 'cz', $this->country);

        $this->translationEn = new Translation(new TranslationKey(uniqid()), $this->languageEn, uniqid());
        $this->translationCz = new Translation(new TranslationKey(uniqid()), $this->languageCz, uniqid());

        $this->translationRepository->persist($this->translationEn);
        $this->translationRepository->persist($this->translationCz);
    }
}

$test = new TranslationsServiceGetTranslations($container);
$test->run();
