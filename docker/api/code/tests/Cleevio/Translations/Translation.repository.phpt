<?php
declare(strict_types=1);

namespace Test\Cleevio\Translations;

$container = require __DIR__ . '/../../bootstrap.php';

use Cleevio\Countries\Entities\Country;
use Cleevio\Countries\Repositories\ICountryRepository;
use Cleevio\Languages\Entities\Language;
use Cleevio\Languages\Entities\Translation;
use Cleevio\Languages\Entities\TranslationKey;
use Cleevio\Languages\Repositories\TranslationRepository;
use Cleevio\Repository\RepositoryTest;
use Nette;
use Symfony\Component\Console\Application;
use Test\Cleevio\TestContext;

class TranslationRepositoryTest extends RepositoryTest
{

    /**
     * @var ICountryRepository|object|null
     */
    private $countryRepository;

    function __construct(Nette\DI\Container $container)
    {
        parent::__construct($container, new TestContext($container->getByType(Application::class)));

        $this->countryRepository = $container->getByType(ICountryRepository::class);
    }

    protected function getType(): string
    {
        return TranslationRepository::class;
    }

    protected function getEntity()
    {
        $country = new Country('US', uniqid(), uniqid());
        $this->countryRepository->persist($country);

        return new Translation(new TranslationKey(uniqid()), new Language(uniqid(), uniqid(), $country), uniqid());
    }

}

$test = new TranslationRepositoryTest($container);
$test->run();
