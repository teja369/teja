<?php
declare(strict_types=1);

namespace Test\Cleevio\Languages\Api;

$container = require __DIR__ . '/../../../bootstrap.php';

use Cleevio\Countries\Entities\Country;
use Cleevio\Countries\Repositories\ICountryRepository;
use Cleevio\Languages\Entities\Language;
use Cleevio\Languages\Repositories\ILanguageRepository;
use Cleevio\RestApi\Auth\ITokenProvider;
use Cleevio\RestApi\Testing\RestTest;
use Cleevio\Users\Entities\User;
use Cleevio\Users\Repositories\IUserRepository;
use Nette;
use Symfony\Component\Console\Application;
use Test\Cleevio\BaseFixture;
use Test\Cleevio\TestContext;
use Tester\Assert;

class LanguagePresenter extends RestTest
{
    /**
     * @var IUserRepository|object|null
     */
    private $userRepository;

    /**
     * @var ITokenProvider|object|null
     */
    private $tokenProvider;

    /**
     * @var ILanguageRepository|object|null
     */
    private $languageRepository;

    /**
     * @var User|null
     */
    private $user;

    /**
     * @var string
     */
    private $accessToken;

    /**
     * @var Language
     */
    private $language;

    /**
     * @var Language
     */
    private $language2;

    /**
     * @var ICountryRepository|object|null
     */
    private $countryRepository;

    /**
     * @var Country
     */
    private $country;

    /**
     * LoginPresenter constructor.
     * @param Nette\DI\Container $container
     */
    function __construct(Nette\DI\Container $container)
    {
        parent::__construct(new TestContext($container->getByType(Application::class)));

        $this->userRepository = $container->getByType(IUserRepository::class);
        $this->tokenProvider = $container->getByType(ITokenProvider::class);
        $this->languageRepository = $container->getByType(ILanguageRepository::class);
        $this->countryRepository = $container->getByType(ICountryRepository::class);
    }

    function testLanguagesGet()
    {
        $response = $this->getRequest('api/v1/languages/' . $this->language->getCode())
            ->withMethod('GET')
            ->execute(200);

        Assert::notNull($response);
    }

    function testLanguagesGetNotFound()
    {
        $this->getRequest('api/v1/languages/' . uniqid())
            ->withMethod('GET')
            ->execute(404);
    }

    function testLanguagesGetDefault()
    {
        $response = $this->getRequest('api/v1/languages/default')
            ->withMethod('GET')
            ->execute(200);

        Assert::notNull($response);
        Assert::same($this->language2->getCode(), $response->code);
    }

    function testLanguagesPutDefault()
    {
        $response = $this->getRequest('api/v1/languages/' . $this->language->getCode() . "/default")
            ->withMethod('PUT')
            ->withHeader('Authorization', 'Bearer ' . $this->accessToken)
            ->execute(200);

        Assert::notNull($response);
        Assert::same($this->language->getCode(), $response->code);

        $response = $this->getRequest('api/v1/languages/' . $this->language2->getCode() . "/default")
            ->withMethod('PUT')
            ->withHeader('Authorization', 'Bearer ' . $this->accessToken)
            ->execute(200);

        Assert::notNull($response);
        Assert::same($this->language2->getCode(), $response->code);
    }

    function testLanguagesList()
    {
        $response = $this->getRequest('api/v1/languages')
            ->withMethod('GET')
            ->execute(200);

        Assert::notNull($response);
        Assert::count(2, $response);
    }

    function testLanguagesCreate()
    {
        $code = 'cs';
        $name = uniqid();

        $response = $this->getRequest('api/v1/languages')
            ->withMethod('POST')
            ->withHeader('Authorization', 'Bearer ' . $this->accessToken)
            ->withBody([
                "code" => $code,
                "name" => $name,
                "country" => $this->country->getCode(),
            ])
            ->execute(200);

        Assert::notNull($response);
        Assert::equal($code, $response->code);
        Assert::equal($name, $response->name);
        Assert::equal($this->country->getId(), $response->country->id);
    }

    function testLanguagesCreateConflict()
    {
        $name = uniqid();

        $this->getRequest('api/v1/languages')
            ->withMethod('POST')
            ->withHeader('Authorization', 'Bearer ' . $this->accessToken)
            ->withBody([
                "code" => 'en',
                "name" => $name,
                "country" => $this->country->getCode(),
            ])
            ->execute(409);
    }

    function testLanguagesUpdate()
    {
        $name = uniqid();

        $response = $this->getRequest('api/v1/languages/' . $this->language->getCode())
            ->withMethod('PUT')
            ->withHeader('Authorization', 'Bearer ' . $this->accessToken)
            ->withBody([
                "name" => $name,
                "country" => $this->country->getCode(),
            ])
            ->execute(200);

        Assert::notNull($response);
        Assert::equal($name, $response->name);
        Assert::equal($this->country->getId(), $response->country->id);
    }

    function testLanguagesDelete()
    {
        $this->getRequest('api/v1/languages/' . $this->language->getCode())
            ->withMethod('DELETE')
            ->withHeader('Authorization', 'Bearer ' . $this->accessToken)
            ->execute(200);
    }

    function testLanguagesDeleteNotFound()
    {
        $this->getRequest('api/v1/languages/' . uniqid())
            ->withMethod('DELETE')
            ->withHeader('Authorization', 'Bearer ' . $this->accessToken)
            ->execute(404);
    }

    function testLanguagesDeleteDefault()
    {
        $this->getRequest('api/v1/languages/' . $this->language2->getCode())
            ->withMethod('DELETE')
            ->withHeader('Authorization', 'Bearer ' . $this->accessToken)
            ->execute(400);
    }

    protected function setUp()
    {
        parent::setUp();

        $this->user = $this->userRepository->findByUsername(BaseFixture::USER_NAME);
        $this->accessToken = $this->tokenProvider->getToken($this->user);

        $this->country = new Country('US', uniqid());
        $this->countryRepository->persist($this->country);

        $this->language = new Language(uniqid(), 'en', $this->country);
        $this->languageRepository->persist($this->language);

        $this->language2 = new Language(uniqid(), 'sk', $this->country);
        $this->language2->setIsDefault(true);
        $this->languageRepository->persist($this->language2);
    }


}

$test = new LanguagePresenter($container);
$test->run();
