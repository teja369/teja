<?php
declare(strict_types=1);

namespace Test\Cleevio\Languages;

$container = require __DIR__ . '/../../bootstrap.php';

use Cleevio\Countries\Entities\Country;
use Cleevio\Countries\Repositories\ICountryRepository;
use Cleevio\Languages\Entities\Language;
use Cleevio\Languages\Repositories\ILanguageRepository;
use Cleevio\Repository\RepositoryTest;
use Nette;
use Symfony\Component\Console\Application;
use Test\Cleevio\TestContext;

class LanguageRepositoryTest extends RepositoryTest
{

    /**
     * @var ICountryRepository|object|null
     */
    private $countryRepository;

    function __construct(Nette\DI\Container $container)
    {
        parent::__construct($container, new TestContext($container->getByType(Application::class)));

        $this->countryRepository = $container->getByType(ICountryRepository::class);
    }


    protected function getType(): string
    {
        return ILanguageRepository::class;
    }

    /**
     * Create and return new entity
     * @return Language
     */
    protected function getEntity(): Language
    {
        $country = new Country('US', uniqid(), uniqid());
        $this->countryRepository->persist($country);

        return new Language(uniqid(), uniqid(), $country);
    }
}

$test = new LanguageRepositoryTest($container);
$test->run();
