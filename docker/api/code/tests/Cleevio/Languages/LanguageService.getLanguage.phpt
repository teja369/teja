<?php
declare(strict_types=1);

namespace Test\Cleevio\Languages;

$container = require __DIR__ . '/../../bootstrap.php';

use Cleevio\Countries\Entities\Country;
use Cleevio\Countries\Repositories\ICountryRepository;
use Cleevio\Languages\Entities\Language;
use Cleevio\Languages\ILanguageService;
use Cleevio\Languages\Repositories\ILanguageRepository;
use Nette;
use Symfony\Component\Console\Application;
use Test\Cleevio\BaseTest;
use Test\Cleevio\TestContext;
use Tester\Assert;

class LanguageServiceGetLanguageTest extends BaseTest
{

    /**
     * @var ILanguageService
     */
    private $languageService;

    /**
     * @var ILanguageRepository
     */
    private $languageRepository;

    /**
     * @var ICountryRepository|object|null
     */
    private $countryRepository;

    /**
     * @var Language
     */
    private $language;

    /**
     * @var string
     */
    private $code;

    /**
     * @var Country
     */
    private $country;

    function __construct(Nette\DI\Container $container)
    {
        parent::__construct(new TestContext($container->getByType(Application::class)));

        $this->languageService = $container->getByType(ILanguageService::class);
        $this->languageRepository = $container->getByType(ILanguageRepository::class);
        $this->countryRepository = $container->getByType(ICountryRepository::class);
    }

    function testGetLanguages()
    {
        $languages = $this->languageService->getLanguages();
        Assert::true(count($languages) > 0);
    }

    function testGetLanguage()
    {
        $language = $this->languageService->getLanguage($this->code);
        Assert::notNull($language);
    }

    function testCreateLanguage()
    {
        $name = uniqid();
        $code = uniqid();

        $language = $this->languageService->createLanguage($code, $name, $this->country);

        Assert::notNull($language);
        Assert::equal($code, $language->getCode());
        Assert::equal($name, $language->getName());
        Assert::equal($this->country->getId(), $language->getCountry()->getId());
    }

    protected function setUp()
    {
        parent::setUp();

        $this->code = uniqid();

        $this->country = new Country('US', uniqid(), uniqid());
        $this->countryRepository->persist($this->country);

        $this->language = new Language(uniqid(), $this->code, $this->country);
        $this->languageRepository->persist($this->language);
    }
}

$test = new LanguageServiceGetLanguageTest($container);
$test->run();
