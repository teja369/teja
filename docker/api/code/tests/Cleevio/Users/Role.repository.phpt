<?php
declare(strict_types=1);

namespace Test\Cleevio\Users;

$container = require __DIR__ . '/../../bootstrap.php';

use Cleevio\Repository\RepositoryTest;
use Cleevio\Users\Entities\Role;
use Cleevio\Users\Repositories\IRoleRepository;
use Nette;
use Symfony\Component\Console\Application;
use Test\Cleevio\TestContext;

class RoleRepositoryTest extends RepositoryTest
{

    function __construct(Nette\DI\Container $container)
    {
        parent::__construct($container, new TestContext($container->getByType(Application::class)));
    }

    protected function getType(): string
    {
        return IRoleRepository::class;
    }

    /**
     * Create and return new entity
     * @return Role
     */
    protected function getEntity(): Role
    {
        return new Role(uniqid());
    }

}

$test = new RoleRepositoryTest($container);
$test->run();
