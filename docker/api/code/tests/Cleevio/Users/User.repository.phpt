<?php
declare(strict_types=1);

namespace Test\Cleevio\Users;

$container = require __DIR__ . '/../../bootstrap.php';

use Cleevio\Repository\RepositoryTest;
use Cleevio\Users\Entities\User;
use Cleevio\Users\Repositories\IUserRepository;
use Nette;
use Symfony\Component\Console\Application;
use Test\Cleevio\TestContext;
use Tester\Assert;

class UserRepositoryTest extends RepositoryTest
{


    /**
     * @var string
     */
    private $email;

    /**
     * @var string
     */
    private $username;

    function __construct(Nette\DI\Container $container)
    {
        parent::__construct($container, new TestContext($container->getByType(Application::class)));
    }

    function testFindByEmail()
    {
        $entity = $this->getEntity();
        $this->repository->persist($entity);
        Assert::false(is_null($this->repository->findByEmail($entity->getEmail())));
    }

    function testFindByUsername()
    {
        $entity = $this->getEntity();
        $this->repository->persist($entity);
        Assert::false(is_null($this->repository->findByUsername($entity->getUsername())));
    }

    protected function getType(): string
    {
        return IUserRepository::class;
    }

    /**
     * Create and return new entity
     * @return User
     */
    protected function getEntity(): User
    {
        $user = new User($this->username);
        $user->setEmail($this->email);

        return $user;
    }

    protected function setUp()
    {
        parent::setUp();
        $this->username = uniqid();
        $this->email = uniqid() . '@test.com';
    }

}

$test = new UserRepositoryTest($container);
$test->run();
