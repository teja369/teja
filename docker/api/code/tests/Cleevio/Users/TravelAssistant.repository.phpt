<?php
declare(strict_types=1);

namespace Test\Cleevio\Users;

$container = require __DIR__ . '/../../bootstrap.php';

use Cleevio\Languages\Repositories\ITranslationRepository;
use Cleevio\Repository\RepositoryTest;
use Cleevio\Users\Entities\TravelAssistant;
use Cleevio\Users\Entities\User;
use Cleevio\Users\Repositories\ITravelAssistantRepository;
use Cleevio\Users\Repositories\IUserRepository;
use Nette;
use Symfony\Component\Console\Application;
use Test\Cleevio\TestContext;

class TravelAssistantRepositoryTest extends RepositoryTest
{

	private const TRAVELLER_USERNAME = 'traveller';

	private const ASSISTANT_USERNAME = 'assistant';

	/**
	 * @var User
	 */
	private $assistant;

	/**
	 * @var User
	 */
	private $traveller;

	/**
	 * @var IUserRepository|object|null
	 */
	private $userRepository;


	function __construct(Nette\DI\Container $container)
    {
        parent::__construct($container, new TestContext($container->getByType(Application::class)));

        $this->userRepository = $container->getByType(IUserRepository::class);
    }


    protected function getType(): string
    {
        return ITravelAssistantRepository::class;
    }


	/**
	 * Create and return new entity
	 * @return TravelAssistant
	 */
    protected function getEntity(): TravelAssistant
    {
		$travelAssistant = new TravelAssistant(
			$this->traveller,
			$this->assistant
		);

        return $travelAssistant;
    }

    protected function setUp()
    {
        parent::setUp();
        $this->traveller = new User(self::TRAVELLER_USERNAME);
        $this->assistant = new User(self::ASSISTANT_USERNAME);

        $this->userRepository->persist($this->traveller);
        $this->userRepository->persist($this->assistant);
    }

}

$test = new TravelAssistantRepositoryTest($container);
$test->run();
