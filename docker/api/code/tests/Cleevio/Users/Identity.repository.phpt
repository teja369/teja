<?php
declare(strict_types=1);

namespace Test\Cleevio\Users;

$container = require __DIR__ . '/../../bootstrap.php';

use Cleevio\Identity\Identities\AccountKitIdentity;
use Cleevio\Identity\Identities\ApiIdentity;
use Cleevio\Identity\Identities\FacebookIdentity;
use Cleevio\Identity\Identities\PasswordIdentity;
use Cleevio\Identity\Identity;
use Cleevio\Identity\IdentityRepository;
use Cleevio\Repository\RepositoryTest;
use Cleevio\Users\Entities\User;
use Nette;
use Symfony\Component\Console\Application;
use Test\Cleevio\TestContext;
use Tester\Assert;

class IdentityRepositoryTest extends RepositoryTest
{

    function __construct(Nette\DI\Container $container)
    {
        parent::__construct($container, new TestContext($container->getByType(Application::class)));
    }

    function testFindByRefreshToken()
    {
        $entity = new ApiIdentity(new User(uniqid()));
        $this->repository->persist($entity);

        Assert::false(is_null($this->repository->findByRefreshToken($entity->getRefreshToken())));
    }

    function testFindByFacebookId()
    {
        $facebookId = uniqid();
        $entity = new FacebookIdentity(new User(uniqid()), $facebookId);
        $this->repository->persist($entity);

        Assert::false(is_null($this->repository->findByFacebookId($facebookId)));
    }

    function testFindByAccountKitId()
    {
        $accountKitId = uniqid();
        $entity = new AccountKitIdentity(new User(uniqid()), $accountKitId);
        $this->repository->persist($entity);

        Assert::false(is_null($this->repository->findByAccountKitId($accountKitId)));
    }

    protected function getType(): string
    {
        return IdentityRepository::class;
    }

    /**
     * Create and return new entity
     * @return Identity
     */
    protected function getEntity(): Identity
    {
        return new PasswordIdentity(new User(uniqid()), "test");
    }
}

$test = new IdentityRepositoryTest($container);
$test->run();
