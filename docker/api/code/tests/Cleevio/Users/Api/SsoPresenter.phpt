<?php
declare(strict_types=1);

namespace Test\Cleevio\Users\Back;

$container = require __DIR__ . '/../../../bootstrap.php';

use Cleevio\Identity\Identities\ApiIdentity;
use Cleevio\Identity\Identities\PasswordIdentity;
use Cleevio\Identity\Identities\SsoIdentity;
use Cleevio\Identity\IIdentityRepository;
use Cleevio\RestApi\Testing\RestTest;
use Cleevio\Users\Entities\User;
use Cleevio\Users\Repositories\IUserRepository;
use Nette;
use Symfony\Component\Console\Application;
use Test\Cleevio\TestContext;
use Tester\Assert;

class SsoPresenter extends RestTest
{

    /**
     * @var IUserRepository|null
     */
    private $userRepository;

    /**
     * @var IIdentityRepository|null
     */
    private $identityRepository;

    /**
     * @var User
     */
    private $user;

    /**
     * @var SsoIdentity
     */
    private $identity;


    /**
     * LoginPresenter constructor.
     * @param Nette\DI\Container $container
     */
    function __construct(Nette\DI\Container $container)
    {
        parent::__construct(new TestContext($container->getByType(Application::class)));

        $this->userRepository = $container->getByType(IUserRepository::class);
        $this->identityRepository = $container->getByType(IIdentityRepository::class);
    }


    function testLogin()
    {
        $response = $this->getRequest('api/v1/sso/login')
            ->withMethod('POST')
            ->withHeader('Authorization', 'Bearer ' . $this->identity->getToken())
            ->execute(200);

        Assert::notNull($response->accessToken);
    }


    protected function setUp()
    {
        parent::setUp();
        $username = uniqid();
        $this->user = new User($username);
        $this->userRepository->persist($this->user);

        $this->identity = new SsoIdentity($this->user, uniqid());
        $this->identityRepository->persist($this->identity);
    }


    protected function tearDown()
    {
        parent::tearDown();
        $this->userRepository->delete($this->user);
    }
}

$test = new SsoPresenter($container);
$test->run();
