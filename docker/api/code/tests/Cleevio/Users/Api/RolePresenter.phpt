<?php
declare(strict_types=1);

namespace Test\Cleevio\Users\Back;

$container = require __DIR__ . '/../../../bootstrap.php';

use Cleevio\RestApi\Testing\RestTest;
use Cleevio\Users\Entities\Resource;
use Cleevio\Users\Entities\Role;
use Cleevio\Users\Repositories\IResourceRepository;
use Cleevio\Users\Repositories\IRoleRepository;
use Nette;
use Symfony\Component\Console\Application;
use Test\Cleevio\TestContext;
use Tester\Assert;

class RolePresenter extends RestTest
{

    /**
     * @var IRoleRepository|object|null
     */
    private $roleRepository;

    /**
     * @var IResourceRepository|object|null
     */
    private $resourceRepository;

    /**
     * LoginPresenter constructor.
     * @param Nette\DI\Container $container
     */
    function __construct(Nette\DI\Container $container)
    {
        parent::__construct(new TestContext($container->getByType(Application::class)));

        $this->roleRepository = $container->getByType(IRoleRepository::class);
        $this->resourceRepository = $container->getByType(IResourceRepository::class);
    }

    function testGetRoles()
    {
        $response = $this->getRequest('api/v1/user/role')
            ->withMethod('GET')
            ->execute(200);

        Assert::count(3, $response);
    }

    function testGetResources()
    {
        $response = $this->getRequest('api/v1/user/resource')
            ->withMethod('GET')
            ->execute(200);

        Assert::count(7, $response);
    }
}

$test = new RolePresenter($container);
$test->run();
