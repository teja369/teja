<?php
declare(strict_types=1);

namespace Test\Cleevio\Users\Back;

$container = require __DIR__ . '/../../../bootstrap.php';

use Cleevio\Identity\Identities\ApiIdentity;
use Cleevio\Identity\Identities\PasswordIdentity;
use Cleevio\Identity\IIdentityRepository;
use Cleevio\RestApi\Testing\RestTest;
use Cleevio\Users\Entities\User;
use Cleevio\Users\Repositories\IUserRepository;
use Nette;
use Symfony\Component\Console\Application;
use Test\Cleevio\TestContext;
use Tester\Assert;

class AuthPresenter extends RestTest
{

	private const PASSWORD = "test";

	/**
	 * @var IUserRepository|null
	 */
	private $userRepository;

	/**
	 * @var IIdentityRepository|null
	 */
	private $identityRepository;

	/**
	 * @var User
	 */
	private $user;

	/**
	 * @var ApiIdentity
	 */
	private $apiIdentity;


	/**
	 * LoginPresenter constructor.
	 * @param Nette\DI\Container $container
	 */
	function __construct(Nette\DI\Container $container)
	{
		parent::__construct(new TestContext($container->getByType(Application::class)));

		$this->userRepository = $container->getByType(IUserRepository::class);
		$this->identityRepository = $container->getByType(IIdentityRepository::class);
	}


	function testLoginBasicPost()
	{
		$response = $this->getRequest('api/v1/auth/login')
			->withMethod('POST')
			->withAuth($this->user->getUsername(), self::PASSWORD)
			->execute(200);

		Assert::notNull($response->accessToken);
	}


	function testRefreshPost()
	{
		$response = $this->getRequest('/api/v1/auth/refresh')
			->withMethod('POST')
			->withHeader("Authorization", 'Bearer ' . $this->apiIdentity->getRefreshToken())
			->execute(200);

		Assert::notNull($response->accessToken);
	}


	protected function setUp()
	{
		parent::setUp();
		$username = uniqid();
		$this->user = new User($username);
		$this->userRepository->persist($this->user);

		$identity = new PasswordIdentity($this->user, self::PASSWORD);
		$this->identityRepository->persist($identity);

		$this->apiIdentity = new ApiIdentity($this->user);
		$this->identityRepository->persist($identity);
	}


	protected function tearDown()
	{
		parent::tearDown();
		$this->userRepository->delete($this->user);
	}
}

$test = new AuthPresenter($container);
$test->run();
