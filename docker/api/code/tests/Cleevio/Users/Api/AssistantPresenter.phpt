<?php
declare(strict_types=1);

namespace Test\Cleevio\Users\Back;

$container = require __DIR__ . '/../../../bootstrap.php';

use Cleevio\RestApi\Auth\ITokenProvider;
use Cleevio\RestApi\Testing\RestTest;
use Cleevio\Users\Entities\TravelAssistant;
use Cleevio\Users\Entities\User;
use Cleevio\Users\Repositories\ITravelAssistantRepository;
use Cleevio\Users\Repositories\IUserRepository;
use Nette;
use Symfony\Component\Console\Application;
use Test\Cleevio\BaseFixture;
use Test\Cleevio\TestContext;
use Tester\Assert;

class AssistantPresenter extends RestTest
{

	private const ASSISTANT_USERNAME = 'assistant';

	/**
	 * @var IUserRepository|null
	 */
	private $userRepository;

	/**
	 * @var User
	 */
	private $user;

	/**
	 * @var ITokenProvider|object|null
	 */
	private $tokenProvider;

	/**
	 * @var string
	 */
	private $accessToken;

	/**
	 * @var User
	 */
	private $assistant;

	/**
	 * @var ITravelAssistantRepository|object|null
	 */
	private $travelAssistantRepository;

	/**
	 * @var string
	 */
	private $assistantAccessToken;


	/**
	 * LoginPresenter constructor.
	 * @param Nette\DI\Container $container
	 */
	function __construct(Nette\DI\Container $container)
	{
		parent::__construct(new TestContext($container->getByType(Application::class)));

		$this->userRepository = $container->getByType(IUserRepository::class);
		$this->travelAssistantRepository = $container->getByType(ITravelAssistantRepository::class);
		$this->tokenProvider = $container->getByType(ITokenProvider::class);
	}


	function testGetUserAssistants()
	{
		$response = $this->getRequest('api/v1/user/assistant')
			->withMethod('GET')
			->withHeader('Authorization', 'Bearer ' . $this->accessToken)
			->execute(200);

		Assert::notNull($response);
		Assert::same($response->items[0]->username, self::ASSISTANT_USERNAME);
		Assert::notSame($response->items[0]->username, $this->user->getUsername());
	}


	function testGetAssistantUsers()
	{
		$response = $this->getRequest('api/v1/assistant/user')
			->withMethod('GET')
			->withHeader('Authorization', 'Bearer ' . $this->assistantAccessToken)
			->execute(200);

		Assert::notNull($response);
		Assert::same($response->items[0]->username, $this->user->getUsername());
		Assert::notSame($response->items[0]->username, self::ASSISTANT_USERNAME);
	}


	protected function setUp()
	{
		parent::setUp();

		$this->user = $this->userRepository->findByUsername(BaseFixture::USER_NAME);
		$this->assistant = new User(self::ASSISTANT_USERNAME);
		$this->userRepository->persist($this->assistant);
		$this->accessToken = $this->tokenProvider->getToken($this->user);
		$this->assistantAccessToken = $this->tokenProvider->getToken($this->assistant);

		$travelAssistant = new TravelAssistant(
			$this->user,
			$this->assistant
		);

		$this->travelAssistantRepository->persist($travelAssistant);
	}

}

$test = new AssistantPresenter($container);
$test->run();
