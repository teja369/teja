<?php
declare(strict_types=1);

namespace Test\Cleevio\Users\Back;

$container = require __DIR__ . '/../../../bootstrap.php';

use Cleevio\Countries\Entities\Country;
use Cleevio\Countries\Repositories\ICountryRepository;
use Cleevio\Identity\Identities\ApiIdentity;
use Cleevio\Identity\Identities\PasswordIdentity;
use Cleevio\Identity\IIdentityRepository;
use Cleevio\Languages\Entities\Language;
use Cleevio\Languages\Repositories\ILanguageRepository;
use Cleevio\RestApi\Auth\ITokenProvider;
use Cleevio\RestApi\Testing\RestTest;
use Cleevio\Users\Entities\User;
use Cleevio\Users\Repositories\IUserRepository;
use Nette;
use Symfony\Component\Console\Application;
use Test\Cleevio\BaseFixture;
use Test\Cleevio\TestContext;
use Tester\Assert;

class LanguagePresenter extends RestTest
{


    /**
     * @var IUserRepository|null
     */
    private $userRepository;

    /**
     * @var User
     */
    private $user;

    /**
     * @var ITokenProvider|object|null
     */
    private $tokenProvider;

    /**
     * @var string
     */
    private $accessToken;

    /**
     * @var Language
     */
    private $language;

    /**
     * @var ILanguageRepository
     */
    private $languageRepository;

    /**
     * @var ICountryRepository|object|null
     */
    private $countryRepository;

    /**
     * LoginPresenter constructor.
     * @param Nette\DI\Container $container
     */
    function __construct(Nette\DI\Container $container)
    {
        parent::__construct(new TestContext($container->getByType(Application::class)));

        $this->userRepository = $container->getByType(IUserRepository::class);
        $this->tokenProvider = $container->getByType(ITokenProvider::class);
        $this->languageRepository = $container->getByType(ILanguageRepository::class);
        $this->countryRepository = $container->getByType(ICountryRepository::class);
    }


    function testPut()
    {
        $response = $this->getRequest(sprintf('api/v1/user/language/%s', $this->language->getCode()))
            ->withMethod('PUT')
            ->withHeader('Authorization', 'Bearer ' . $this->accessToken)
            ->execute(200);

        Assert::notNull($response->id);
        Assert::notNull($response->language);
    }

    protected function setUp()
    {
        parent::setUp();

        $this->user = $this->userRepository->findByUsername(BaseFixture::USER_NAME);
        $this->accessToken = $this->tokenProvider->getToken($this->user);

        $country = new Country('US', uniqid(), uniqid());
        $this->countryRepository->persist($country);

        $this->language = new Language(uniqid(), "cs", $country);
        $this->languageRepository->persist($this->language);
    }


    protected function tearDown()
    {
        parent::tearDown();
        $this->userRepository->delete($this->user);
    }
}

$test = new LanguagePresenter($container);
$test->run();
