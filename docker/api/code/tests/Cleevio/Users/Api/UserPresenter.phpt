<?php
declare(strict_types=1);

namespace Test\Cleevio\Users\Back;

$container = require __DIR__ . '/../../../bootstrap.php';

use Cleevio\Identity\Identities\ApiIdentity;
use Cleevio\Identity\Identities\PasswordIdentity;
use Cleevio\Identity\IIdentityRepository;
use Cleevio\RestApi\Auth\ITokenProvider;
use Cleevio\RestApi\Testing\RestTest;
use Cleevio\Users\Entities\User;
use Cleevio\Users\Repositories\IUserRepository;
use Nette;
use Symfony\Component\Console\Application;
use Test\Cleevio\BaseFixture;
use Test\Cleevio\TestContext;
use Tester\Assert;

class UserPresenter extends RestTest
{


    /**
     * @var IUserRepository|null
     */
    private $userRepository;

    /**
     * @var User
     */
    private $user;

    /**
     * @var ITokenProvider|object|null
     */
    private $tokenProvider;

    /**
     * @var string
     */
    private $accessToken;


    /**
     * LoginPresenter constructor.
     * @param Nette\DI\Container $container
     */
    function __construct(Nette\DI\Container $container)
    {
        parent::__construct(new TestContext($container->getByType(Application::class)));

        $this->userRepository = $container->getByType(IUserRepository::class);
        $this->tokenProvider = $container->getByType(ITokenProvider::class);
    }


    function testGet()
    {
        $response = $this->getRequest('api/v1/user')
            ->withMethod('GET')
            ->withHeader('Authorization', 'Bearer ' . $this->accessToken)
            ->execute(200);

        Assert::notNull($response->id);
        Assert::notNull($response->username);
        Assert::equal(BaseFixture::USER_NAME, $response->username);
		Assert::null($response->language);
    }

    protected function setUp()
    {
        parent::setUp();

        $this->user = $this->userRepository->findByUsername(BaseFixture::USER_NAME);
        $this->accessToken = $this->tokenProvider->getToken($this->user);
    }


    protected function tearDown()
    {
        parent::tearDown();
        $this->userRepository->delete($this->user);
    }
}

$test = new UserPresenter($container);
$test->run();
