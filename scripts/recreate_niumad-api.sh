#!/bin/bash

docker stop niumad-api-nginx niumad-api
docker rm niumad-api-nginx niumad-api

docker create \
    --restart unless-stopped \
    --name niumad-api \
    --network niumad-bridge \
    -v /data/niumad/niumad-api/.env:/var/www/html/.env:ro \
    -v /data/niumad/niumad-api/saml.pem:/var/www/html/app/cert/saml.pem:ro \
    app3038acrdev.azurecr.io/niumad-api
    
docker create \
    --restart unless-stopped \
    --name niumad-api-nginx \
    --network niumad-bridge \
    -v /data/niumad/niumad-api/nginx_conf/default.conf:/etc/nginx/conf.d/default.conf:ro \
    -v /data/niumad/niumad-api/nginx_conf/nginx.conf:/etc/nginx/nginx.conf:ro \
    -v /data/niumad/niumad-api/nginx_log/:/var/log/nginx \
    --volumes-from niumad-api \
    nginx

docker start niumad-api
docker exec niumad-api php bin/console migrations:migrate --no-interaction

docker start niumad-api-nginx

docker exec niumad-api chmod a+w -R temp
