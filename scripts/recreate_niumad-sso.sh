#!/bin/bash

docker stop niumad-sso-nginx niumad-sso
docker rm niumad-sso-nginx niumad-sso

docker create \
    --restart unless-stopped \
    --name niumad-sso \
    --network niumad-bridge \
    -v /data/niumad/niumad-sso/.env:/var/www/html/.env:ro \
    niumad-sso:uat
    
docker create \
    --restart unless-stopped \
    --name niumad-sso-nginx \
    --network niumad-bridge \
    -v /data/niumad/niumad-sso/nginx_conf/default.conf:/etc/nginx/conf.d/default.conf:ro \
    -v /data/niumad/niumad-sso/nginx_conf/nginx.conf:/etc/nginx/nginx.conf:ro \
    -v /data/niumad/niumad-sso/nginx_log/:/var/log/nginx \
    --volumes-from niumad-sso \
    nginx

docker start niumad-sso
docker exec niumad-sso php bin/console migrations:migrate --no-interaction

docker start niumad-sso-nginx
docker exec niumad-sso chmod a+w -R temp
