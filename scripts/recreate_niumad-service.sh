#!/bin/bash

# Exit on failures
set -e

HomeDir="/data/niumad/"
# Allowed services
NiumadServices=("api" "frontend" "admin" "sso")
declare -A Ports=( )
MigrationServices=("api" "sso")
Env="uat"

# Check if service name passed as parameter
if [ -z "$1" ]
then
    echo "No service name specified. Allowed services are '${NiumadServices[@]}'."
    exit 1
fi

if [[ ! " ${NiumadServices[@]} " =~ " $1 " ]]; then
    echo "Service $1 not configured. Allowed services are '${NiumadServices[@]}'."
    exit 1
fi

# Check if containers are running, stop them if yes
echo "Stopping containers ..."
if [ "$(docker ps | grep niumad-${1}-nginx$)" ]; then
    docker stop niumad-$1-nginx
    echo "Container niumad-$1-nginx stopped."
else
    echo "Container niumad-$1-nginx is not running."
fi

if [ "$(docker ps | grep niumad-${1}$)" ]; then
    docker stop niumad-$1
    echo "Container niumad-$1 stopped."
else
    echo "Container niumad-$1 is not running."
fi

# Remove old containers if they are present
echo "Removing old containers ..."

if [ "$(docker container ls -f 'status=exited' -f 'status=dead' -f 'status=created' | grep niumad-${1}-nginx$)" ]; then
    docker rm niumad-$1-nginx
    echo "Container niumad-$1-nginx removed."
else
    echo "Container niumad-$1-nginx not found."
fi

if [ "$(docker container ls -f 'status=exited' -f 'status=dead' -f 'status=created' | grep niumad-${1}$)" ]; then
    docker rm niumad-$1
    echo "Container niumad-$1 removed."
else
    echo "Container niumad-$1 is not found."
fi

echo "Creating new containers"
docker create \
    --restart unless-stopped \
    --name niumad-$1\
    --network niumad-bridge \
    -v ${HomeDir}niumad-$1/.env:/var/www/html/.env:ro \
    niumad-${1}:${Env}


if [ ${Ports[$1]} ]
then
    BindPort="-p ${Ports[$1]}:80"
    echo "Webserver: binding port 80 to host port ${Ports[$1]}."
else
    BindPort=
fi

docker create \
    --restart unless-stopped \
    --name niumad-${1}-nginx \
    --network niumad-bridge \
    -v ${HomeDir}niumad-${1}/nginx_conf/default.conf:/etc/nginx/conf.d/default.conf:ro \
    -v ${HomeDir}niumad-${1}/nginx_conf/nginx.conf:/etc/nginx/nginx.conf:ro \
    -v ${HomeDir}niumad-${1}/nginx_log/:/var/log/nginx \
    --volumes-from niumad-${1} \
    $BindPort \
    nginx

echo "Startnig new containers ..."
docker start niumad-${1} && echo "Service $1 was started"
if [[ " ${MigrationServices[@]} " =~ " $1 " ]]; then
    echo "Running migrations for service $1 .."
    docker exec niumad-${1} php bin/console migrations:migrate --no-interaction
fi


docker start niumad-${1}-nginx && echo "Service $1-nginx was started."
docker exec niumad-${1} chmod a+w -R temp
