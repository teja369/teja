#!/bin/bash

docker stop niumad-frontend-nginx niumad-frontend
docker rm niumad-frontend-nginx niumad-frontend

docker create \
    --restart unless-stopped \
    --name niumad-frontend \
    --network niumad-bridge \
    -v /data/niumad/niumad-frontend/.env:/var/www/html/.env:ro \
    niumad-frontend:uat
    
docker create \
    --restart unless-stopped \
    --name niumad-frontend-nginx \
    --network niumad-bridge \
    -v /data/niumad/niumad-frontend/nginx_conf/default.conf:/etc/nginx/conf.d/default.conf:ro \
    -v /data/niumad/niumad-frontend/nginx_conf/nginx.conf:/etc/nginx/nginx.conf:ro \
    -v /data/niumad/niumad-frontend/nginx_log/:/var/log/nginx \
    --volumes-from niumad-frontend \
    nginx

docker start niumad-frontend

docker start niumad-frontend-nginx
docker exec niumad-frontend chmod a+w -R temp
