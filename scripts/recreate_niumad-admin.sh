#!/bin/bash

docker stop niumad-admin-nginx niumad-admin
docker rm niumad-admin-nginx niumad-admin

docker create \
    --restart unless-stopped \
    --name niumad-admin \
    --network niumad-bridge \
    -v /data/niumad/niumad-admin/.env:/var/www/html/.env:ro \
    app3038acrdev.azurecr.io/niumad-admin
    
docker create \
    --restart unless-stopped \
    --name niumad-admin-nginx \
    --network niumad-bridge \
    -v /data/niumad/niumad-admin/nginx_conf/default.conf:/etc/nginx/conf.d/default.conf:ro \
    -v /data/niumad/niumad-admin/nginx_conf/nginx.conf:/etc/nginx/nginx.conf:ro \
    -v /data/niumad/niumad-admin/nginx_log/:/var/log/nginx \
    --volumes-from niumad-admin \
    nginx

docker start niumad-admin
#docker exec niumad-admin php bin/console translations:create


docker start niumad-admin-nginx
docker exec niumad-admin chmod a+w -R temp
