#!/bin/bash

docker stop niumad-proxy
docker rm niumad-proxy

docker create \
    --restart unless-stopped \
    --name niumad-proxy \
    --network niumad-bridge \
    --ip 172.18.0.100 \
    -v /data/niumad/niumad-proxy/nginx_conf/default.conf:/etc/nginx/conf.d/default.conf:ro \
    -v /data/niumad/niumad-proxy/nginx_conf/nginx.conf:/etc/nginx/nginx.conf:ro \
    -v /data/niumad/niumad-proxy/nginx_log/:/var/log/nginx \
    -v /data/niumad/site-certificates/niumad-uat-cz.deloittece.com_old/:/etc/nginx/site-certificates/niumad-uat.deloittece.com/:ro \
    -p 80:80 \
    -p 443:443 \
    -p 8080:8080 \
    nginx:1.15-alpine

docker start niumad-proxy
