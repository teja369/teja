#!/bin/bash
# CHECK FOR ROOT PRIVILEGES.
if [ "$(id -u)" != "0" ]; then
	echo "CylancePROTECT Linux Support Collection Tool must be run as root." 1>&2
	exit 1
fi
# CLEANUP TEMPORARY FILES IF SPECIFIC SIGNALS ARE CAUGHT.
trap cleanup SIGHUP SIGINT SIGQUIT SIGTERM
function cleanup {
	echo ""
	echo Cleaning up...
	rm -rf /tmp/cylanceLogs 2>&1 >/dev/null
	echo "Collection incomplete - please run the CylancePROTECT Linux Support Collection Tool again." 1>&2
	exit 1
}
# DISPLAY ASCII SPLASH.
cat Cylance
echo Collection started, please wait...
# CREATE SCRATCH DIRECTORIES.
mkdir /tmp/cylanceLogs 2>/dev/null
# DON'T CREATE CYDATA FOLDERS IF CYLANCEPROTECT IS NOT INSTALLED.
if [ -x "$(command -v /opt/cylance/desktop/cylancesvc)" ]; then
	mkdir /tmp/cylanceLogs/Cydata 2>>/tmp/cylanceLogs/CaptureError.txt
	mkdir /tmp/cylanceLogs/Cydata/HostCache 2>>/tmp/cylanceLogs/CaptureError.txt
fi
# PROCESS LIST.
ps auxww 2>>/tmp/cylanceLogs/CaptureError.txt > /tmp/cylanceLogs/ps.txt
# CHECK IF PSTREE EXISTS.
if [ -x "$(command -v pstree)" ]; then
	# PROCESS TREE LIST.
	pstree -pGA 2>>/tmp/cylanceLogs/CaptureError.txt > /tmp/cylanceLogs/pstree.txt
fi
# FREE AND USED PHYSICAL AND SWAP MEMORY, KERNEL BUFFERS AND CACHES.
free -t -l 2>>/tmp/cylanceLogs/CaptureError.txt > /tmp/cylanceLogs/mem_free.txt
# KERNEL SLAB CACHE INFORMATION.
slabtop -o -s l 2>>/tmp/cylanceLogs/CaptureError.txt > /tmp/cylanceLogs/slabtop.txt
# CHECK IF VMSTAT EXISTS.
if [ -x "$(command -v vmstat)" ]; then
	# VIRTUAL MEMORY STATISTICS. "BUFF" AND "CACHE" COLUMNS DEPLETE AS FREE MEMORY DEPLETES, "SO" COLUMN WILL INCREASE AS KERNEL THRASHES PAGES TO DISK.
	vmstat 1 10 2>>/tmp/cylanceLogs/CaptureError.txt > /tmp/cylanceLogs/vmstat.txt
fi
# PERFORMANCE MEASUREMENTS.
# CHECK IF TOP EXISTS.
if [ -x "$(command -v top)" ]; then
	# CAPTURE TOP STATISTICS IN BATCH MODE. SHOW COMMAND LINES, 10 SAMPLES, ONCE PER SECOND.
	top -bc -d 1 -n 10 2>>/tmp/cylanceLogs/CaptureError.txt > /tmp/cylanceLogs/top.txt
fi
# CHECK IF IOTOP EXISTS.
if [ -x "$(command -v iotop)" ]; then
	# CAPTURE IOTOP STATISTICS IN BATCH MODE. 10 SAMPLES, ONCE PER SECOND.
	iotop -b -d 1 -n 10 2>>/tmp/cylanceLogs/CaptureError.txt > /tmp/cylanceLogs/iotop.txt
fi
# CHECK IF IOSTAT EXISTS.
if [ -x "$(command -v iostat)" ]; then
	# CAPTURE IOSTAT FOR BLOCK DEVICES AND PARTITIONS. 10 SAMPLES, ONCE PER SECOND.
	iostat -p ALL 1 10 2>>/tmp/cylanceLogs/CaptureError.txt > /tmp/cylanceLogs/iostat.txt
	# CAPTURE EXTENDED STATISTICS AND DEVICE UTILIZATION REPORT. 10 SAMPLES, ONCE PER SECOND.
	iostat -p ALL -dx 1 10 2>>/tmp/cylanceLogs/CaptureError.txt >> /tmp/cylanceLogs/iostat.txt
fi
# OS, KERNEL INFORMATION.
# PRINT ALL SYSTEM INFORMATION.
uname -a 2>>/tmp/cylanceLogs/CaptureError.txt > /tmp/cylanceLogs/Kernelinfo.txt
# DISPLAY KERNEL PARAMETERS.
sysctl -a 2>/dev/null > /tmp/cylanceLogs/sysctl.txt
# COLLECT OS INFORMATION.
cat $(find /etc/ -maxdepth 1 -type f -name '*release*') 2>>/tmp/cylanceLogs/CaptureError.txt > /tmp/cylanceLogs/osinfo.txt
# COLLECT PID MAX VALUE.
cat /proc/sys/kernel/pid_max 2>>/tmp/cylanceLogs/CaptureError.txt > /tmp/cylanceLogs/pid_max.txt
# COLLECT MEMORY OVERCOMMIT SETTINGS
echo overcommit_memory > /tmp/cylanceLogs/mem-overCommit.txt
cat /proc/sys/vm/overcommit_memory 2>>/tmp/cylanceLogs/CaptureError.txt >> /tmp/cylanceLogs/mem-overCommit.txt
echo overcommit_ratio >> /tmp/cylanceLogs/mem-overCommit.txt
cat /proc/sys/vm/overcommit_ratio 2>>/tmp/cylanceLogs/CaptureError.txt >> /tmp/cylanceLogs/mem-overCommit.txt
# STATUS OF KERNEL MODULES.
lsmod 2>>/tmp/cylanceLogs/CaptureError.txt > /tmp/cylanceLogs/lsmod.txt
# ENVIRONMENT VARIABLES.
env 2>>/tmp/cylanceLogs/CaptureError.txt > /tmp/cylanceLogs/env-vars.txt
# KERNEL VERSIONS, SHA256 HASHES, AND FILE TYPES
sha256sum /boot/vmlinuz-* 2>>/tmp/cylanceLogs/CaptureError.txt 1>> /tmp/cylanceLogs/Kernelinfo.txt
file /boot/vmlinuz-* 2>>/tmp/cylanceLogs/CaptureError.txt 1>> /tmp/cylanceLogs/Kernelinfo.txt
# PROCESS LIST WITH NUMBER OF MINOR AND MAJOR PAGE FAULTS.
ps axww -o pid,min_flt,maj_flt,cmd 2>>/tmp/cylanceLogs/CaptureError.txt > /tmp/cylanceLogs/mem-pageFaults.txt
# CHECK FOR UBUNTU.
UBUNTUver=`grep DISTRIB_ID /etc/*-release 2>/dev/null | awk -F '=' '{print $2}'`
if [ "$UBUNTUver" == "Ubuntu" ]; then
	# LIST ALL INSTALLED PACKAGES. CHECK FOR PREREQUISITES AND POSSIBLE MEMDEF CONFLICTS.
	apt list --installed 2>/dev/null > /tmp/cylanceLogs/packageList.txt
	# COLLECT RECENT PACKAGE INSTALLATION HISTORY. CHECK FOR PREREQUISITES AND POSSIBLE MEMDEF CONFLICTS.
	cat /var/log/dpkg.log > /tmp/cylanceLogs/packageHistory.txt 2>&1
	cat /var/log/dpkg.log.1 >> /tmp/cylanceLogs/packageHistory.txt 2>&1
	zgrep : /var/log/dpkg.log.*.gz >> /tmp/cylanceLogs/packageHistory.txt 2>&1
	# CHECK IF CYLANCE IS INSTALLED.
	if [ -x "$(command -v /opt/cylance/desktop/cylancesvc)" ]; then
		# LIST INSTALLED CYLANCE PACKAGES.
		apt list --installed 2>/dev/null | grep -i cylance 1> /tmp/cylanceLogs/packageCylance.txt
		# COLLECT CYLANCE PACKAGE INSTALLATION HISTORY.
		grep cylance /var/log/dpkg.log > /tmp/cylanceLogs/packageHistoryCylance.txt 2>&1
		grep cylance /var/log/dpkg.log.1 >> /tmp/cylanceLogs/packageHistoryCylance.txt 2>&1
		zgrep cylance /var/log/dpkg.log.*.gz >> /tmp/cylanceLogs/packageHistoryCylance.txt 2>&1
		# STATUS AND LIST OF FILES ASSOCIATED WITH CYLANCE PACKAGES.
		dpkg -s cylance-protect >> /tmp/cylanceLogs/packageCylance.txt 2>&1
		dpkg-query -L cylance-protect >> /tmp/cylanceLogs/packageCylance.txt 2>&1
		dpkg -s cylance-protect-ui >> /tmp/cylanceLogs/packageCylance.txt 2>&1
		dpkg-query -L cylance-protect-ui >> /tmp/cylanceLogs/packageCylance.txt 2>&1
		# CHECK IF UBUNTU 16.
		UBUNTUrelease=`grep DISTRIB_RELEASE /etc/*-release 2>/dev/null | awk -F '=' '{print $2}' | awk -F '.' '{print $1}'`
		if [ "$UBUNTUrelease" == "16" ]; then
			# CHECK IF JOURNALCTL EXISTS
			if [ -x "$(command -v journalctl)" ]; then
				# CHECK IF GREP FOR LOADMODULE HAS NON BLANK RESULTS
				JctlLoadModule=`journalctl | grep loadmodule.sh`
				if [ "$JctlLoadModule" != "" ]; then
					# COLLECT LOADMODULE MESSAGES. CHECK FOR KERNEL MODULE BUILD FAILURES.
					journalctl | grep loadmodule.sh 2>>/tmp/cylanceLogs/CaptureError.txt > /tmp/cylanceLogs/Cydata/journalctl-loadmodule.log
				fi
				# CHECK IF GREP FOR DRIVER MESSAGES HAS NON BLANK RESULTS
				JctlDriver=`journalctl | grep CyProtectDrv`
				if [ "$JctlDriver" != "" ]; then
					# COLLECT CYLANCE DRIVER MESSAGES.
					journalctl | grep CyProtectDrv 2>>/tmp/cylanceLogs/CaptureError.txt > /tmp/cylanceLogs/Cydata/journalctl-CyProtectDrv.txt
				fi
			fi
		fi
	fi
fi
# CHECK FOR SUSE ENTERPRISE LINUX 11 OR 12.
SUSEver=`cat /etc/SuSE-release 2>/dev/null | grep "SUSE" | awk -F 'Linux Enterprise Server ' '{print $2}' | awk -F ' ' '{print $1}'`
if [ "$SUSEver" == "11" ] || [ "$SUSEver" == "12" ]; then
	# LIST ALL INSTALLED PACKAGES VIA RPM. CHECK FOR PREREQUISITES AND POSSIBLE MEMDEF CONFLICTS.
	rpm -qa 2>>/tmp/cylanceLogs/CaptureError.txt > /tmp/cylanceLogs/packageList.txt
	# LIST ALL INSTALLED PACKAGES VIA ZYPPER. CHECK FOR PREREQUISITES AND POSSIBLE MEMDEF CONFLICTS.
	zypper pa --installed-only | grep "i  |" 2>>/tmp/cylanceLogs/CaptureError.txt >> /tmp/cylanceLogs/packageList.txt
	# COLLECT PACKAGE INSTALLATION HISTORY. CHECK FOR PREREQUISITES AND POSSIBLE MEMDEF CONFLICTS.
	cp /var/log/zypp/history /tmp/cylanceLogs/packageHistory.txt >> /tmp/cylanceLogs/CaptureError.txt 2>&1 >/dev/null
	# COLLECT KERNEL PACKAGE INFORMATION.
	rpm -qa | grep -i kernel 2>>/tmp/cylanceLogs/CaptureError.txt 1>> /tmp/cylanceLogs/Kernelinfo.txt
	# CHECK IF CYLANCE IS INSTALLED.
	if [ -x "$(command -v /opt/cylance/desktop/cylancesvc)" ]; then
		# LIST INSTALLED CYLANCE PACKAGES.
		rpm -qa | grep -i Cylance > /tmp/cylanceLogs/packageCylance.txt 2>&1
		zypper pa -i | grep -i Cylance >> /tmp/cylanceLogs/packageCylance.txt 2>&1
		# LIST HISTORY OF CYLANCE PACKAGES.
		rpm -q CylancePROTECT --last > /tmp/cylanceLogs/packageHistoryCylance.txt 2>&1
		rpm -q CylancePROTECTUI --last >> /tmp/cylanceLogs/packageHistoryCylance.txt 2>&1
		# COLLECT INFORMATION AND FILE LIST FOR CYLANCE PACKAGES.
		rpm -qi CylancePROTECT >> /tmp/cylanceLogs/packageCylance.txt 2>&1
		rpm -ql CylancePROTECT >> /tmp/cylanceLogs/packageCylance.txt 2>&1
		rpm -qi CylancePROTECTUI >> /tmp/cylanceLogs/packageCylance.txt 2>&1
		rpm -ql CylancePROTECTUI >> /tmp/cylanceLogs/packageCylance.txt 2>&1
		# CHECK IF JOURNALCTL EXISTS
		if [ -x "$(command -v journalctl)" ]; then
			# CHECK IF GREP FOR LOADMODULE HAS NON BLANK RESULTS
			JctlLoadModule=`journalctl | grep loadmodule.sh`
			if [ "$JctlLoadModule" != "" ]; then
				# COLLECT LOADMODULE MESSAGES. CHECK FOR KERNEL MODULE BUILD FAILURES.
				journalctl | grep loadmodule.sh 2>>/tmp/cylanceLogs/CaptureError.txt > /tmp/cylanceLogs/Cydata/journalctl-loadmodule.log
			fi
			# CHECK IF GREP FOR DRIVER MESSAGES HAS NON BLANK RESULTS
			JctlDriver=`journalctl | grep CyProtectDrv`
			if [ "$JctlDriver" != "" ]; then
				# COLLECT CYLANCE DRIVER MESSAGES.
				journalctl | grep CyProtectDrv 2>>/tmp/cylanceLogs/CaptureError.txt > /tmp/cylanceLogs/Cydata/journalctl-CyProtectDrv.txt
			fi
		fi
	fi
# ELSE CHECK FOR NON-UBUNTU AND NON-SUSE LINUX.
elif [ "$UBUNTUver" != "Ubuntu" ] && [ "$SUSEver" != "11" ] && [ "$SUSEver" != "12" ]; then
	# MUST BE AMAZON LINUX, RHEL OR CENTOS.
	# LIST ALL INSTALLED PACKAGES VIA RPM. CHECK FOR PREREQUISITES AND POSSIBLE MEMDEF CONFLICTS.
	rpm -qa 2>>/tmp/cylanceLogs/CaptureError.txt > /tmp/cylanceLogs/packageList.txt
	# COLLECT PACKAGE INSTALLATION HISTORY. CHECK FOR PREREQUISITES AND POSSIBLE MEMDEF CONFLICTS.
	rpm -qa --last 2>>/tmp/cylanceLogs/CaptureError.txt > /tmp/cylanceLogs/packageHistory.txt
	# COLLECT KERNEL PACKAGE INFORMATION.
	rpm -qi 'kernel-*' 2>>/tmp/cylanceLogs/CaptureError.txt 1>> /tmp/cylanceLogs/Kernelinfo.txt
	# CHECK IF CYLANCE IS INSTALLED.
	if [ -x "$(command -v /opt/cylance/desktop/cylancesvc)" ]; then
		# LIST INSTALLED CYLANCE PACKAGES.
		rpm -qa | grep -i Cylance > /tmp/cylanceLogs/packageCylance.txt 2>&1
		# LIST HISTORY OF CYLANCE PACKAGES.
		rpm -q CylancePROTECT --last > /tmp/cylanceLogs/packageHistoryCylance.txt 2>&1
		rpm -q CylancePROTECTUI --last >> /tmp/cylanceLogs/packageHistoryCylance.txt 2>&1
		# COLLECT INFORMATION AND FILE LIST FOR CYLANCE PACKAGES.
		rpm -qi CylancePROTECT >> /tmp/cylanceLogs/packageCylance.txt 2>&1
		rpm -ql CylancePROTECT >> /tmp/cylanceLogs/packageCylance.txt 2>&1
		rpm -qi CylancePROTECTUI >> /tmp/cylanceLogs/packageCylance.txt 2>&1
		rpm -ql CylancePROTECTUI >> /tmp/cylanceLogs/packageCylance.txt 2>&1
	fi
	# CHECK FOR AMAZON LINUX, RHEL/CENTOS 7.
	OSver=`cat /etc/system-release 2>/dev/null | grep "release" | awk -F 'release ' '{print $2}' | awk -F ' ' '{print $1}' | awk -F '.' '{print $1}'`
	if [ "$OSver" == "7" ] || [ "$OSver" == "2" ] || [ "$OSver" == "2017" ] || [ "$OSver" == "2018" ]; then
		# CHECK IF CYLANCE IS INSTALLED.
		if [ -x "$(command -v /opt/cylance/desktop/cylancesvc)" ]; then
			# LIST CYLANCE PACKAGE HISTORY VIA YUM.
			yum history summary CylancePROTECT >> /tmp/cylanceLogs/packageHistoryCylance.txt 2>&1
			yum history summary CylancePROTECTUI >> /tmp/cylanceLogs/packageHistoryCylance.txt 2>&1
			yum history info CylancePROTECT >> /tmp/cylanceLogs/packageHistoryCylance.txt 2>&1
			yum history info CylancePROTECTUI >> /tmp/cylanceLogs/packageHistoryCylance.txt 2>&1
			# CHECK IF JOURNALCTL EXISTS
			if [ -x "$(command -v journalctl)" ]; then
				# CHECK IF GREP FOR LOADMODULE HAS NON BLANK RESULTS
				JctlLoadModule=`journalctl | grep loadmodule.sh`
				if [ "$JctlLoadModule" != "" ]; then
					# COLLECT LOADMODULE MESSAGES. CHECK FOR KERNEL MODULE BUILD FAILURES.
					journalctl | grep loadmodule.sh 2>>/tmp/cylanceLogs/CaptureError.txt > /tmp/cylanceLogs/Cydata/journalctl-loadmodule.log
				fi
				# CHECK IF GREP FOR DRIVER MESSAGES HAS NON BLANK RESULTS
				JctlDriver=`journalctl | grep CyProtectDrv`
				if [ "$JctlDriver" != "" ]; then
					# COLLECT CYLANCE DRIVER MESSAGES.
					journalctl | grep CyProtectDrv 2>>/tmp/cylanceLogs/CaptureError.txt > /tmp/cylanceLogs/Cydata/journalctl-CyProtectDrv.txt
				fi
			fi
		fi
	fi
fi
# CHECK IF DMIDECODE EXISTS
if [ -x "$(command -v dmidecode)" ]; then
	# DEVICE FINGERPRINT SOURCES TO DETERMINE UNIQUENESS.
	dmidecode -t system 2>>/tmp/cylanceLogs/CaptureError.txt | grep Serial 2>>/tmp/cylanceLogs/CaptureError.txt > /tmp/cylanceLogs/fingerprintBIOS.txt
fi
# CHECK IF FINGERPRINT SOURCE DBUS MACHINE-ID IS PRESENT.
if [ -f "/var/lib/dbus/machine-id" ]; then
	cp /var/lib/dbus/machine-id /tmp/cylanceLogs/fingerprintMachineID.txt >> /tmp/cylanceLogs/CaptureError.txt 2>&1 >/dev/null
fi
# CHECK IF FINGERPRINT SOURCE MACHINE-ID IS PRESENT
if [ -f "/etc/machine-id" ]; then
	cp /etc/machine-id /tmp/cylanceLogs/fingerprintMachineID.txt >> /tmp/cylanceLogs/CaptureError.txt 2>&1 >/dev/null
fi
# CHECK IF LSOF EXISTS.
if [ -x "$(command -v lsof)" ]; then
	# INFORMATION ABOUT FILES OPENED BY ALL PROCESSES.
	lsof +c 0 2>/dev/null 1> /tmp/cylanceLogs/openFiles-All.txt
fi
# CHECK IF CYLANCE IS INSTALLED AND LSOF EXISTS.
if [ -x "$(command -v /opt/cylance/desktop/cylancesvc)" ] && [ -x "$(command -v lsof)" ]; then
	# CHECK IF CYLANCESVC IS RUNNING
	CySvcRunning=`pgrep cylancesvc`
	if [ "$CySvcRunning" != "" ]; then
		# INFORMATION ABOUT FILES OPENED BY CYLANCE.
		lsof +c 0 -p $(pgrep cylancesvc) 2>/dev/null 1> /tmp/cylanceLogs/openFiles-Cylance.txt
	fi
fi
# SYSTEM INFORMATION.
# PROCESSOR TYPE.
cp /proc/cpuinfo /tmp/cylanceLogs/cpu.txt >> /tmp/cylanceLogs/CaptureError.txt 2>&1 >/dev/null
# SYSTEM RAM USAGE.
cp /proc/meminfo /tmp/cylanceLogs/mem.txt >> /tmp/cylanceLogs/CaptureError.txt 2>&1 >/dev/null
# MOUNTED FILE SYSTEMS.
cp /proc/mounts /tmp/cylanceLogs/mounts.txt >> /tmp/cylanceLogs/CaptureError.txt 2>&1 >/dev/null
# LOADED KERNEL MODULES.
cp /proc/modules /tmp/cylanceLogs/modules.txt >> /tmp/cylanceLogs/CaptureError.txt 2>&1 >/dev/null
# CHECK MEMORY FRAGMENTATION.
cp /proc/buddyinfo /tmp/cylanceLogs/buddyinfo.txt >> /tmp/cylanceLogs/CaptureError.txt 2>&1 >/dev/null
# KERNEL SLAB ALLOCATOR STATISTICS.
cp /proc/slabinfo /tmp/cylanceLogs/slabinfo.txt >> /tmp/cylanceLogs/CaptureError.txt 2>&1 >/dev/null
# RESOURCE MANAGER PROCESS LIMITATIONS
cp /etc/security/limits.conf /tmp/cylanceLogs/limits.txt >> /tmp/cylanceLogs/CaptureError.txt 2>&1 >/dev/null
# RUNTIME KERNEL PARAMETERS
cp /etc/sysctl.conf /tmp/cylanceLogs/sysctl.txt >> /tmp/cylanceLogs/CaptureError.txt 2>&1 >/dev/null
# CYLANCE INFORMATION AND SUPPORT FILES
# CHECK IF CYLANCE IS INSTALLED.
if [ -x "$(command -v /opt/cylance/desktop/cylancesvc)" ]; then
	# CYLANCE DIRECTORY LISTING.
	ls -alR /opt/cylance 2>> /tmp/cylanceLogs/CaptureError.txt 1> /tmp/cylanceLogs/directoryCylance.txt
	# CHECK IF DRIVER IS RUNNING
	CyDrvRunning=`modinfo CyProtectDrv`
	if [ "$CyDrvRunning" != "" ]; then
		# CYLANCE DRIVER INFO.
		modinfo CyProtectDrv 2>> /tmp/cylanceLogs/CaptureError.txt 1> /tmp/cylanceLogs/Cydata/CyProtectDrv-modinfo.txt
	fi
	# CHECK IF CYLANCESVC IS RUNNING
	CySvcRunning=`pgrep cylancesvc`
	if [ "$CySvcRunning" != "" ]; then
		# CYLANCE SERVICE PROCESS MEMORY MAP.
		pmap -x $(pgrep cylancesvc 2>> /tmp/cylanceLogs/CaptureError.txt) 2>>/tmp/cylanceLogs/CaptureError.txt > /tmp/cylanceLogs/maps.txt
	fi
	# CHECK IF KERNEL OBJECT EXISTS
	CyKObjRunning=`modinfo -n CyProtectDrv`
	if [ "$CyKObjRunning" != "" ]; then
		# COLLECT CYLANCE DRIVER KERNEL OBJECT.
		cp $(modinfo -n CyProtectDrv 2>> /tmp/cylanceLogs/CaptureError.txt) /tmp/cylanceLogs/Cydata/CyProtectDrv.ko >> /tmp/cylanceLogs/CaptureError.txt 2>&1 >/dev/null
	fi
	# CHECK IF INIT CYLANCE SERVICE CONF EXISTS.
	if [ -f "/etc/init/cylancesvc.conf" ]; then
		# COLLECT INIT CYLANCE SERVICE CONF.
		cp /etc/init/cylancesvc.conf /tmp/cylanceLogs/Cydata/Upstart-cylancesvc.conf >> /tmp/cylanceLogs/CaptureError.txt 2>&1 >/dev/null
	fi
	# CHECK IF CYLANCE DRIVER BUILD LOG EXISTS.
	if [ -f "/usr/src/CyProtectDrv"*"/loadmodule.log" ]; then
		# CYLANCE DRIVER BUILD LOG.
		cp /usr/src/CyProtectDrv*/loadmodule.log /tmp/cylanceLogs/Cydata/CyProtectDrv-loadmodule.log >> /tmp/cylanceLogs/CaptureError.txt 2>&1 >/dev/null
	fi
	# CYLANCE DRIVER CACHE.
	cp /opt/cylance/desktop/CyProtectDrv.bin /tmp/cylanceLogs/Cydata/ >> /tmp/cylanceLogs/CaptureError.txt 2>&1 >/dev/null
	# CYLANCE LOCAL DATABASE.
	cp /opt/cylance/desktop/chp.d* /tmp/cylanceLogs/Cydata/ >> /tmp/cylanceLogs/CaptureError.txt 2>&1 >/dev/null
	# CYLANCE CONFIGURATION XML.
	cp /opt/cylance/desktop/registry/LocalMachine/software/cylance/desktop/values.xml /tmp/cylanceLogs/Cydata/ >> /tmp/cylanceLogs/CaptureError.txt 2>&1 >/dev/null
	# CYLANCE STATUS INFORMATION FILE.
	cp /opt/cylance/desktop/Status.json /tmp/cylanceLogs/Cydata/ >> /tmp/cylanceLogs/CaptureError.txt 2>&1 >/dev/null
	# CYLANCE PROXY CHECK.
	# CHECK IF INIT PROXY CONF EXISTS.
	if [ -f "/etc/init/cylancesvc.override" ]; then
		cp /etc/init/cylancesvc.override /tmp/cylanceLogs/Cydata/proxy-cylancesvc.override >> /tmp/cylanceLogs/CaptureError.txt 2>&1 >/dev/null
	fi
	# CHECK IF SYSTEMD PROXY CONF EXISTS.
	if [ -f "/etc/systemd/system/cylancesvc.service.d/proxy.conf" ]; then
		cp /etc/systemd/system/cylancesvc.service.d/proxy.conf /tmp/cylanceLogs/Cydata/proxy-proxy.conf >> /tmp/cylanceLogs/CaptureError.txt 2>&1 >/dev/null
	fi
	# CYLANCE ANALYZER STATUS, COUNT, AND SCANNER STATE.
	cp /opt/cylance/HostCache/* /tmp/cylanceLogs/Cydata/HostCache/ >> /tmp/cylanceLogs/CaptureError.txt 2>&1 >/dev/null
	# CHECK IF CYLANCESVC IS RUNNING
	CySvcRunning=`pgrep cylancesvc`
	if [ "$CySvcRunning" != "" ]; then
		# CYLANCE SERVICE WITH NUMBER OF MINOR AND MAJOR PAGE FAULTS.
		ps ww -o pid,min_flt,maj_flt,cmd $(pgrep cylancesvc) 2>>/tmp/cylanceLogs/CaptureError.txt > /tmp/cylanceLogs/mem-cyPageFaults.txt
	fi
else
	# CYLANCE NOT INSTALLED.
	echo CylancePROTECT service not found, skipping CylancePROTECT data collection.
	echo CylancePROTECT service not found, skipping CylancePROTECT data collection. > '/tmp/cylanceLogs/!CylanceNotDetected!.txt'
fi
# RECORD TOOL VERSION USED.
echo CylancePROTECT Linux Support Collection Tool v3.5 > '/tmp/cylanceLogs/!Version!.txt'
# ARCHIVE TEMP DATA.
echo Archiving collection...
tar --gzip -cf cylancelogs-$(hostname)-$(date "+%Y.%m.%d-%H.%M.%S").tgz /var/log/messages* /var/log/syslog* /opt/cylance/desktop/log /tmp/cylanceLogs > /dev/null 2>&1
# CLEAN UP TEMP DATA.
echo Cleaning up...
rm -rf /tmp/cylanceLogs 2>&1 >/dev/null
# FINISHED.
echo Collection complete.
exit 0